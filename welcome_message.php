<!DOCTYPE html>
<html lang="en">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
  <meta name="viewport" content="width=device-width, initial-scale=1"/>
  <title>DIGIDOC | Digital Document management </title>

  <!-- CSS  -->
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  <link href="assets/css/materialize.css" type="text/css" rel="stylesheet" media="screen,projection"/>
  <link href="assets/css/style.css" type="text/css" rel="stylesheet" media="screen,projection"/>
</head>
<body>
<!--   <nav class="white" role="navigation">
    <div class="nav-wrapper container">
      <a id="logo-container" href="#" class="brand-logo">Logo</a>
      <ul class="right hide-on-med-and-down">
        <li><a href="#">Login</a></li>
      </ul>

      <ul id="nav-mobile" class="side-nav">
        <li><a href="#">Navbar Link</a></li>
      </ul>
      <a href="#" data-activates="nav-mobile" class="button-collapse"><i class="material-icons">menu</i></a>
    </div>
  </nav>
 -->
  <div id="index-banner" class="parallax-container">
    <div class="section no-pad-bot">
      <div class="container" style="height: 750px;">
        <br><br>
        <h1 class="header center  text-lighten-2" style="font-size: 5rem;color:#1D2731">DIGIDOC</h1>
        <div class="row center">
          <h5 class="header col s12 hide-on-med-and-down light" style="font-size: 2.5rem;color:#0B3C5D">An online document management <br>
          Make your paperless..</h5>

        </div>
        <div class="row center-align">
          <a href="http://www.digidoc.asia" id="download-button" class="btn-large waves-effect waves-light " style="background-color: #0B3C5D">Get Started</a>
        </div>
        <br><br>

      </div>
    </div>
    <div class="parallax headertop" ><img src="assets/images/background.jpg" alt="Unsplashed background img 1"></div>
  </div>


  <div class="container">
    <div class="section">

      <!--   Icon Section   -->
      <div class="row">
        <div class="col s12 m3">
          <div class="icon-block">
            <h2 class="center ifeature"><i class="material-icons">folder</i></h2>
            <h5 class="center">My Document</h5>

            <p class="light">Document search does not waste any more time because you can create indexes to track specific documents and create document versions for edited documents. The original file will not be lost.</p>
          </div>
        </div>

        <div class="col s12 m3">
          <div class="icon-block">
            <h2 class="center  ifeature"><i class="material-icons">folder_shared</i></h2>
            <h5 class="center">Sharing with other</h5>

            <p class="light">It makes it easier to collaborate with others by sharing documents. You can set permissions to access files manually. And there are notifications as soon as the document is shared.</p>
          </div>
        </div>

        <div class="col s12 m3">
          <div class="icon-block">
            <h2 class="center ifeature"><i class="material-icons">storage</i></h2>
            <h5 class="center">Workflow</h5>

            <p class="light">Dynamic flow can be defined as needed. With real-time status and alerting in both the system and email.</p>
          </div>
        </div>

        <div class="col s12 m3">
          <div class="icon-block">
            <h2 class="center ifeature"><i class="material-icons">group</i></h2>
            <h5 class="center">Users Management</h5>

            <p class="light">Assigns privileges and space to users to host events in the system.</p>
          </div>
        </div>
      </div>

    </div>
  </div>


  <div class="parallax-container valign-wrapper">
    <div class="section no-pad-bot">
      <div class="container">
        <div class="row col s12 center">
            <div class="row col m6 s12">
              <img src="assets/images/mobile.png" alt="Unsplashed background img 1" style="width: 80%">
            </div>
            <div class="row col m6 s12 ">
              <h4 class="header light"><b>Easy to approve</b></h4>
            </div>
        </div>
      </div>
    </div>
    <div class="parallax"><img src="assets/images/paperlesss.jpg" alt="Unsplashed background img 2"></div>
  </div>

<!--   <div class="container">
    <div class="section">

      <div class="row">
        <div class="col s12 center">
          <h3><i class="mdi-content-send brown-text"></i></h3>
          <h4>Contact Us</h4>
          <p class="left-align light">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam scelerisque id nunc nec volutpat. Etiam pellentesque tristique arcu, non consequat magna fermentum ac. Cras ut ultricies eros. Maecenas eros justo, ullamcorper a sapien id, viverra ultrices eros. Morbi sem neque, posuere et pretium eget, bibendum sollicitudin lacus. Aliquam eleifend sollicitudin diam, eu mattis nisl maximus sed. Nulla imperdiet semper molestie. Morbi massa odio, condimentum sed ipsum ac, gravida ultrices erat. Nullam eget dignissim mauris, non tristique erat. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae;</p>
        </div>
      </div>

    </div>
  </div> -->


<!--   <div class="parallax-container valign-wrapper">
    <div class="section no-pad-bot">
      <div class="container">
        <div class="row center">
          <h5 class="header col s12 light">A modern responsive front-end framework based on Material Design</h5>
        </div>
      </div>
    </div>
    <div class="parallax"><img src="assets/images/background3.jpg" alt="Unsplashed background img 3"></div>
  </div> -->

  <footer class="page-footer footer" >
    <div class="container">
      <div class="row">
        <div class="col l6 s12">
          <h5 class="white-text">Contact Us</h5>
          <p class="grey-text text-lighten-4">Siamrajathanee Co.,Ltd 289/9 Moo 10, Old Railway Road. Samrong Phrapadaeng, Samutprakarn 10130
Tel. 0-2743-5240 Fax 0-2743-5261 E-mail info@siamrajathanee.com</p>


        </div>
<!--         <div class="col l3 s12">
          <h5 class="white-text">Settings</h5>
          <ul>
            <li><a class="white-text" href="#!">Link 1</a></li>
            <li><a class="white-text" href="#!">Link 2</a></li>
            <li><a class="white-text" href="#!">Link 3</a></li>
            <li><a class="white-text" href="#!">Link 4</a></li>
          </ul>
        </div> -->
<!--         <div class="col l3 s12">
          <h5 class="white-text">Connect</h5>
          <ul>
            <li><a class="white-text" href="#!">Link 1</a></li>
            <li><a class="white-text" href="#!">Link 2</a></li>
            <li><a class="white-text" href="#!">Link 3</a></li>
            <li><a class="white-text" href="#!">Link 4</a></li>
          </ul>
        </div> -->
      </div>
    </div>
    <div class="footer-copyright">
      <div class="container">
       <a class="brown-text text-lighten-3 right" href="http://www.siamrajathanee.com/index.php">© 2010 Siam Rajathanee Co.,Ltd</a>
      </div>
    </div>
  </footer>


  <!--  Scripts-->
  <script src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
  <script src="assets/js/materialize.js"></script>
  <script src="assets/js/init.js"></script>

  </body>
</html>
