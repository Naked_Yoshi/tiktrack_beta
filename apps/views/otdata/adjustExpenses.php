<div id="AdjustExpenses" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title" id="myModalLabel">ปรับปรุงอื่น ๆ</h4>
            </div>
            <div class="modal-body">
              <div class="row">
                <div class="col-md-6">
                  <label for="txt_code_site">เบิกค่าใช้จ่าย</label>
                </div>
                <div class="col-md-6" align="right">
                  <span id="txt_code_site">xxxxxxxxxxxx</span>
                </div>
              </div>
              <br/>
              <div class="row">
                <div class="col-md-6">
                  <label for="txt_expenses">ค่าใช้จ่ายเหมา</label>
                </div>
                <div class="col-md-6" align="right">
                  <span id="txt_expenses">xxx บาท</span>
                </div>
              </div>
              <br/>
              <hr>
              <div class="row">
                <div class="col-md-6" style="padding-top:7px;">
                  <label for="">ปรับปรุงค่าใช้จ่ายไซต์ / แผนก</label>
                </div>
                <div class="col-md-6">
                  <input type="text" class='form-control' id="adjustCodeSite" name="" value="" placeholder="รหัสไซต์ / ชื่อแผนก">
                  <span style="color:red;font-size:12px;">* หากเป็นค่าใช้จ่ายแผนกตัวเองให้ปล่อยเป็นช่องว่าง</span>
                </div>
              </div>
              <br>
              <div class="row">
                <div class="col-md-6" style="padding-top:7px;">
                  <label for="">ปรับปรุงค่าใช้จ่ายเหมา</label>
                </div>
                <div class="col-md-6">
                  <input type="text" class='form-control' id="adjustExpenses" name="" value="" placeholder="บาท">
                  <!-- <span>* หากเป็นค่าใช้จ่ายแผนกตัวเองให้ปล่อยเป็นช่องว่าง</span> -->
                </div>
              </div>
              <hr>
              <div class="row">
                <div class="col-md-12" style="padding:0px">
                    <button class="btn btn-block btn-success btn-adjust-ot">บันทึก</button>
                </div>
              </div>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
