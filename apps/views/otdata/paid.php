<div class="white-box">
  <div class='col-lg-6 col-sm-6col-md-6 col-xs-6' style='padding-bottom:20px'>
      <!-- <button class='ot-all-btn ot-all-approve btn btn-success btn-rounded disabled' data-position='top' data-delay='50' data-tooltip='อนุมัติ' disabled>
        <span class='fa fa-check'></span> อนุมัติ<span class='approve_num'></span> </button> &nbsp;
      <button class='ot-all-btn ot-all-reject btn btn-danger btn-rounded disabled' data-position='top' data-delay='50' data-tooltip='ไม่อนุมัติ' disabled>
        <span class='fa fa-remove'></span> ไม่อนุมัติ<span class='reject_num'></span></button> -->
  </div>
  <div class='col-lg-6 col-sm-6 col-md-6 col-xs-6' align='right'>
    <button class='btn btn-info btn_filter' data-position='top' data-delay='50' data-tooltip='ค้นหา'>
      <span class='fa fa-filter'></span> ค้นหา</button>

    <button class='btn btn-info btn_print btn-report-otrequest' data-position='top' data-delay='50' data-tooltip='พิมพ์รายงาน'>
      <span class='fa fa-print'></span> พิมพ์รายงาน</button>
  </div>
  <br><br><br>
    <table class="table table-bordered " id="otdata_table" data-segment="users">
        <thead>
            <tr>
                <!-- <th scope="col" class='text-center' ><div class='checkbox checkbox-inverse'><input id="select-all" type='checkbox'/><label></label></div></th> -->
                <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="4" class='text-center'>no</th>
                <th scope="col" style="width:200px;" data-tablesaw-sortable-col data-tablesaw-sortable-default-col data-tablesaw-priority="persist">ชื่อ - นามสกุล</th>
                <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="4" class='text-center'>วันที่ขอ</th>
                <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="4" class='text-center'>รายละเอียด</th>
                <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="2" class='text-center'>เริ่ม</th>
                <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="2" class='text-center'>สิ้นสุด</th>
                <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="4" class='text-center'>ตัวคูณ</th>
                <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="4" class='text-center'>ชั่วโมงเบิกจริง</th>
                <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="4" class='text-center'>ค่าใช้จ่ายแผนก / ไซต์</th>
                <!-- <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="4" class='text-center'>อนุมัติทำโดย</th> -->
                <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="4" class='text-center'>อนุมัติเบิกโดย</th>

                <th scope="col" style="width:80px;" data-tablesaw-sortable-col data-tablesaw-priority="4" class='text-center'>ตัวเลือก</th>
            </tr>
        </thead>

        <tbody id='otdata_body'>

        </tbody>

    </table>

</div>
