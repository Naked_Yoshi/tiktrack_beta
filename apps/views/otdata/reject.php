<div class="white-box">
<table class="table table-bordered " id="otdata_table" data-segment="users">
<thead>
  <tr>
      <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="4" class='text-center'>no</th>
      <th scope="col" data-tablesaw-sortable-col data-tablesaw-sortable-default-col data-tablesaw-priority="persist">ชื่อ - นามสกุล</th>
      <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="4" class='text-center'>วันที่ขอ</th>
      <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="4" class='text-center'>รายละเอียด</th>
      <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="2" class='text-center'>เริ่ม</th>
      <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="2" class='text-center'>สิ้นสุด</th>
      <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="4" class='text-center'>ตัวคูณ</th>
      <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="4" class='text-center'>ไซต์</th>
      <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="4" class='text-center'>สาขา</th>
      <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="4" class='text-center'>รวม(ชั่วโมง)</th>
      <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="4" class='text-center'>สถานะ</th>
  </tr>
</thead>
<tbody id='otdata_body'>

</tbody>
</table>
</div>
