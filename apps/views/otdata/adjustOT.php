<div id="AdjustOTModel" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title" id="myModalLabel">ปรับปรุงเวลาการลา</h4>
            </div>
            <div class="modal-body">
              <div class="row">
                <div class="col-md-6">
                  <label for="reqTotalHours">รวมขั่วโมงที่ขอ</label>
                </div>
                <div class="col-md-6" align="right">
                  <span id="reqTotalHours">xx ชั่วโมง xx นาที</span>
                </div>
              </div>
              <br/>
              <div class="row">
                <div class="col-md-6">
                  <label for="actualWork">รวมชั่วโมงที่ทำจริง</label>
                </div>
                <div class="col-md-6" align="right">
                  <span id="actualWork">xx ชั่วโมง xx นาที</span>
                </div>
              </div>
              <br/>
              <div class="row">
                <div class="col-md-6">
                  <label for="excessWork">ชั่วโมงที่ปัดเศษ</label>
                </div>
                <div class="col-md-6" align="right">
                  <span id="excessWork">xx ชั่วโมง xx นาท</span>
                </div>
              </div>
              <hr>
              <div class="row">
                <div class="col-md-6" style="padding-top:7px;">
                  <label for="excessWork">ปรับปรุงเวลาจ่ายจริง</label>
                </div>
                <div class="col-md-3">
                  <input type="number" class='form-control' id="adjustHour" name="" value="" placeholder="ชั่วโมง">
                </div>
                <div class="col-md-3">
                  <input type="number" class='form-control' id="adjustMinute" name="" value="" placeholder="นาที">
                </div>
              </div>
              <hr>
              <div class="row">
                <div class="col-md-12" style="padding:0px">
                    <button class="btn btn-block btn-success btn-adjust-ot">บันทึก</button>
                </div>
              </div>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
