<div id="OTrequest" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="depCreateLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="clearModals();">×</button>
          <h4 class="modal-title" id="OTrequest"><span class='fa fa-print'></span>  พิมพ์รายงานการขอ OT</h4>
      </div>
      <div class="modal-body">

        <!-- <form id="OTrequest"> -->
          <form class='report_form' method="post" target="_blank" action='<?php echo base_url('OTdata/previewReport'); ?>'>
            <div class="row">
              <div class="type_ofsearch form-group col-md-4" id="date_start1">
                <div class="input-group has-feedback">
                  <label class="col-md-12">วันที่เริ่มต้น</label>
                  <input type="text" class="form-control mydatepicker" id="date_start" name="date_start" placeholder="วัน/เดือน/ปี">
                  <span class="form-control-feedback"><i class="icon-calender"></i></span>
                </div>
                  </div>
              <!-- <?php $month = array("มกราคม","กุมภาพันธ์","มีนาคม","เมษายน","พฤษภาคม","มิถุนายน","กรกฎาคม","สิงหาคม","กันยายน","ตุลาคม","พฤศจิกายน","ธันวาคม"); ?> -->

              <div class="type_ofsearch form-group col-md-4" id="date_end1">
                <div class="input-group has-feedback">
                  <label class="col-md-12">วันที่สิ้นสุด</label>
                  <input type="text" class="form-control mydatepicker" id="date_ended" name="date_ended" placeholder="วัน/เดือน/ปี">
                  <span class="form-control-feedback"><i class="icon-calender"></i></span>
                </div>
                  </div>

              <div class="col-md-4">
                <div class="form-group has-feedback" id="selectType1">
                      <label>ประเภทการพิมพ์รายงาน</label>
                      <select name="selectType" id = "selectType" class="form-control validate">
                        <option value="0">พิมพ์รายบุคคล</option>
                        <option value="1">พิมพ์ตามไซต์งาน</option>
                      </select>
                  <!-- <span class="form-control-feedback" id="depcreateName_feedback"></span> -->
                </div>
              </div>
            </div>
            <div class="row" id="by_person">
              <div class="col s12">
                 <label for="search_user_report">ค้นหาชื่อพนักงาน</label>
                 <form onsubmit="$('#search_user_report').blur();return false;" class="pure-form" style="padding-bottom:10px;">
                   <input id="search_user_report" type="text" name="q" placeholder="ชื่อ หรือนามสกุล" class="form-control">
                 </form>
              </div>
              <input type="hidden" id='index_emp' name="index_emp" value="">
              <input type="hidden" id='site' name="site" value="">
              <input type="hidden" id='branche' name="branche" value="">
            </div>

            <div class="row" id="by_site" style="display:none;">

              <div class="col-md-6">
                <div class="input-field custom sm"  id="report_site">
                    <select class="form-control validate" name="cb_site" id="cb_site">
                    </select>
                </div>
                <label class='feedback' id="feedback-site"></label>
              </div>
              <div class="col-md-6">
                <div class="input-field custom sm"  id="report_branch">
                  <select class="form-control validate" name="cb_branche" id="cb_branche">
                    <option value="" disabled selected>เลือกรายการ </option>
                  </select>
                </div>
                <label class='feedback' id="feedback-branch"></label>
              </div>
            </div>
          </form>

      </div>
      <div class="modal-footer">
      <button type="button" class="btn btn-info waves-effect  submit-report-print-ot-complete">พิมพ์</button>

      </div>
    </div>
  </div>
</div>
