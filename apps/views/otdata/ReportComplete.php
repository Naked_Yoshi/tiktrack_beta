<div id="OTcomplete" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="depCreateLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="clearModals();">×</button>
          <h4 class="modal-title" id="OTcomplete"><span class='fa fa-print'></span>  พิมพ์รายงาน OT ที่ได้รับการอนุมัติเเล้ว</h4>
      </div>
      <div class="modal-body">

        <form id="OTcomplete" class='report_form' method="post" target="_blank" action='<?php echo base_url('report/preview'); ?>' >

            <div class="row">
              <div class="type_ofsearch form-group col-md-4" id="date_start1">
                <div class="input-group has-feedback">
                  <label class="col-md-12">วันที่เริ่มต้น</label>
                  <input type="text" class="form-control mydatepicker" id="date_start" name="date_start" placeholder="วัน/เดือน/ปี">
                  <span class="form-control-feedback"><i class="icon-calender"></i></span>
                </div>
                  </div>
              <div class="type_ofsearch form-group col-md-4" id="date_end1">
                <div class="input-group has-feedback">
                  <label class="col-md-12">วันที่สิ้นสุด</label>
                  <input type="text" class="form-control mydatepicker" id="date_ended" name="date_ended" placeholder="วัน/เดือน/ปี">
                  <span class="form-control-feedback"><i class="icon-calender"></i></span>
                </div>
                  </div>
              <div class="col-md-4">
                <div class="form-group has-feedback" id="selectType">
                      <label>ประเภทการพิมพ์รายงาน</label>
                      <select name="selectedType" id="selectedType" class="form-control validate">
                        <option value="0">โปรดเลือก</option>
                        <option value="1">พิมพ์ตามไซต์งาน</option>
                      </select>
                  <!-- <span class="form-control-feedback" id="depcreateName_feedback"></span> -->
                </div>
              </div>



            </div>

            <!-- <div class="row form-group has-feedback" id="by_person">
              <input type="text" class="form-control validate" name="print_input" id="" placeholder="ค้นหารายชื่อพนักงาน e-mail ชื่อจริง นามสกุล" data-id='print_input'>
              <span class="form-control-feedback" id="feedback-print_input"></span>
              <div class="col-md-12">
                <br>
                  <label>ข้อมูลที่เลือก</label>
                  <p id='getname'> - </p>
                  <label>ไซต์</label>
                  <p id='getsite'> - </p>
                  <label>สาขา</label>
                  <p id='getbranch'> - </p>
                  <hr>
              </div>
              <input type="hidden" id='index' name="index" value="">
            </div> -->

            <div class="row" id="by_site" style="display:none;">
              <div class="col-md-6">
                <div class="form-group has-feedback" id="">
                  <label for="positionDep">ไซต์ <span id="posdep">   </span>    </label>
                    <!-- <input type="hidden"  name="site" id="site" > -->
                  <select class="form-control validate" name="site" id="site"  >

                  </select>
                <span class="form-control-feedback" id="sitecreateName_feedback"></span>
                </div>
              </div>
              <!-- <div class="col s12 m6">
                <label>ไซต์ *</label>
                <div class="input-field custom sm"  id="site">
                    <select name="site">
                    </select>
                </div>
                <label class='feedback' id="feedback-site"></label>
              </div> -->
              <div class="col-md-6">
                <div class="form-group has-feedback" id="positionDep_group">
                  <label for="positionDep">สาขา<span id="posdep">   </span>    </label>
                    <input type="hidden"  name="posdep" id="posdep" >
                  <select class="form-control validate" name="dep" id="Report_branche"  >

                  </select>
                  <span class="form-control-feedback" id="positionDep_feedback"></span>
                </div>
              </div>
              <!-- <div class="col s12 m6">
                <label>สาขา *</label>
                <div class="input-field custom sm"  id="branch">
                  <select name="branch">
                    <option value="" disabled selected>เลือกรายการ </option>
                  </select>
                </div>
                <label class='feedback' id="feedback-branch"></label>
              </div> -->
            </div>



      </div>
        </form>
        <div class="modal-footer">
        <button type="submit" class="btn btn-info waves-effect  submit-report-print-ot-complete">พิมพ์</button>
        </div>
    </div>
  </div>
</div>
