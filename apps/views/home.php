<!DOCTYPE html>
<html lang="en" class="bg">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<!-- <link rel="icon" type="image/png" sizes="16x16" href="../plugins/images/favicon.png"> -->
<title>#tiktrack Best service for time attendance solutions.</title>
<style>
    .bg {
    /* The image used */
    background-image: url("<?php echo base_url("assets/img/ui_tt-07.jpg"); ?>");

    /* Full height */
    height: 100%;

    /* Center and scale the image nicely */
    background-position: center;
    background-repeat: no-repeat;
    background-size: cover;
}
</style>
<!-- Bootstrap Core CSS -->
<link href="<?php echo base_url("assets/bootstrap/dist/css/bootstrap.min.css"); ?>" rel="stylesheet">
<!-- animation CSS -->
<link href="<?php echo base_url("assets/css/animate.css"); ?>" rel="stylesheet">
<!-- Custom CSS -->
<link href="<?php echo base_url("assets/css/style.css"); ?>" rel="stylesheet">
<!-- color CSS -->
<link href="<?php echo base_url("assets/css/colors/default.css"); ?>" id="theme"  rel="stylesheet">
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
<link rel="stylesheet" href="<?php echo base_url("assets/css/sweetalert2/dist/sweetalert2.min.js"); ?>">
</head>
<body>
<!-- Preloader -->
<!-- <div class="preloader">
  <div class="cssload-speeding-wheel"></div>
</div> -->
<section id="wrapper" class="bg">
  <div class="login-box">
  <div class="white-box">
    <h1 class="text-center" style="font-size:80px;">TIKTRACK</h1>
    <h1 class="text-center" style="font-size:27px;">TIME ATTENDANCE SYSTEM</h1>
  </div>
  <div class="row" style="background-color: #b71c1c;">
    <h1 class="text-center" style="font-size:40px;color:#ffffff;">Customer Login</h1>
  </div>
  <br>
  <div class="row">
    <div class="input-group"> <span class="input-group-addon">@</span>
        <input type="text" placeholder="Username" id="username" class="form-control">
    </div>
    <br>
    <div class="input-group"> <span class="input-group-addon"><span class="fa fa-key"></span></span>
        <input type="password" placeholder="Password" id="password" class="form-control">        
    </div>
    
    <div class="row" style="margin:15px 0px 0px 20px">
    <span id="txt_response" style="color:red;font-size:12px;"></span>
    </div>

  </div>
  <br>
  <div class="row" style="background-color: #b71c1c; padding:5px; ">
      <div class="col-sm-12" style="text-align:center;">
          <button class="btn btn-outline btn-rounded btn-default btn-login" style="width:150px;">Login</button>         
        </div>
  </div>
  <br>
  <div class="row" style="padding:5px;text-align:center;">
    <p>
      <span class="fa fa-info-circle"></span>&nbsp; Forget Password ? &nbsp;
      <button class="btn  btn-warning " alt="default" data-toggle="modal" data-target="#myModal" class="model_img img-responsive" style="height:40px;">click Here!!!</button>
    </p>
  </div>

                        <div id="myModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                              <div class="modal-dialog">
                                  <div class="modal-content">
                                      <div class="modal-header">
                                          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                          <h4 class="modal-title" id="myModalLabel">Forget Password</h4>
                                      </div>
                                      <div class="modal-body">
                                        <label for="">โปรดกรอก E-mail ของท่าน</label>
                                        <div class="input-group m-t-10">
                                          <span class="input-group-addon"> <i class="fa fa-envelope-o"></i></span><input type="email" id="email" name="example-input2-group1" class="form-control" placeholder="Email"></div>

                                      </div>
                                      <div class="modal-footer">
                                          <button type="button" class="btn btn-info waves-effect btn-forget-password">Send to Email.</button>
                                      </div>
                                  </div>
                                  <!-- /.modal-content -->
                              </div>
                              <!-- /.modal-dialog -->
                          </div>
  </div>
</section>
<!-- jQuery -->
<script src="<?php echo base_url("assets/plugins/bower_components/jquery/dist/jquery.min.js"); ?>"></script>
<!-- Bootstrap Core JavaScript -->
<script src="<?php echo base_url("assets/bootstrap/dist/js/bootstrap.min.js");?>"></script>
<!-- Menu Plugin JavaScript -->
<script src="<?php echo base_url("assets/plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.js"); ?>"></script>

<!--slimscroll JavaScript -->
<script src="<?php echo base_url("assets/js/jquery.slimscroll.js");?>"></script>
<!--Wave Effects -->
<script src="<?php echo base_url("assets/js/waves.js");?>"></script>
<!-- Custom Theme JavaScript -->
<script src="<?php echo base_url("assets/js/custom.min.js"); ?>"></script>
<!--Style Switcher -->
<script src="<?php echo base_url("assets/plugins/bower_components/styleswitcher/jQuery.style.switcher.js"); ?>"></script>

<script src="<?php echo base_url("assets/js/apps/JLogin.js"); ?>"></script>

<!-- <script src="<?php echo base_url("assets/js/apps/JHelpers.js"); ?>"></script> -->

<script src="<?php echo base_url("assets/js/jwt-decode.min.js"); ?>"></script>

<script type="text/javascript" src='<?php echo base_url('assets/css/sweetalert2/dist/sweetalert2.all.min.js');?>'></script>
</body>
</html>
