<div class="white-box">
  <div class="row">

                      <div class="col-md-4 col-sm-12 click01">
                          <div class="white-box text-center" style="background-color:#689f38;">
                              <h1 class="text-white counter" id="checkin"></h1>
                                  <p class="text-white">เข้างาน</p>
                                    </div>
                        </div>

                          <div class="col-md-4 col-sm-12 click02">
                                <div class="white-box text-center" style="background-color:#ffb300;">
                                    <h1 class="counter text-white" id="late"></h1>
                                    <p class="text-white">สาย</p>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-12 click03">
                                <div class="white-box text-center" style="background-color:#e53935;">
                                    <h1 class="text-white counter" id="absense"></h1>
                                    <p class="text-white">ขาด</p>
                                </div>
                            </div>
      </div>
</div>

<div class="white-box">
      <section>
        <div class="row">
          <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <button class="btn btn-info" data-toggle="modal" data-target="#exampleModal"><span class='fa fa-search'></span> ค้นหา</button>
          </div>
            <div class='col-lg-9 col-sm-8 col-md-8 col-xs-12' align="right">
              <button class="btn btn-info" data-toggle="modal" data-target="#reportModal"><span class='fa fa-print'></span> ปริ้นรายงาน</button>
              </div>
        </div>

      <div class="modal fade" id="reportModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1">

          <div class="modal-dialog" role="document">
              <div class="modal-content">
                  <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                      <h4 class="modal-title" id="exampleModalLabel1">พิมพ์รายงาน</h4>
                  </div>
                  <div class="modal-body">
                    <!-- <form class='report_form' method="post" action='<?php echo base_url('attendance/previewReport'); ?>'> -->
                      <form class='report_form' method="post" target="_blank" action='<?php echo base_url('attendance/previewReport'); ?>'>
                      <input type="hidden" name="company_id" id="company_id" value="">
                      <input type="hidden" name="index_emp" id="index_emp" value="">
                      <input type="hidden" name="index_site" id="index_site" value="">
                      <input type="hidden" name="index_branche" id="index_branche" value="">
                      <input type="hidden" name="index_dep" id="index_dep" value="">
                        <div class="row">
                          <?php $month = array("มกราคม","กุมภาพันธ์","มีนาคม","เมษายน","พฤษภาคม","มิถุนายน","กรกฎาคม","สิงหาคม","กันยายน","ตุลาคม","พฤศจิกายน","ธันวาคม"); ?>
                          <div class='form-group col-md-6' id="report_month_type">
                          <label>เดือน</label>
                          <div class="input-field custom sm" >
                          <select class="form-control" name="report_searchmonth" id="report_selectsearchmonth">
                            <?php for ($i=0; $i < count($month); $i++) { $num = $i+1; ?>
                              <option value="<?php echo $num ?>"><?php echo $month[$i]; ?></option>
                            <?php } ?>
                          </select>
                          </div>
                          </div>
                          <div class='form-group col-md-6' >
                            <label>ปี</label>
                            <div class="input-field custom sm"  id="report_searchyear">
                            <select class="form-control" name="report_searchyear" id="report_selectsearchyear">
                              <?php for ($i=0; $i < 10; $i++) { $year = 2018+$i; ?>
                                <option value="<?php echo $year; ?>"><?php echo $year; ?></option>
                              <?php } ?>
                            </select>
                            </div>
                          </div>

                        </div>

                        <div class="row">
                          <div class="col s12">
                            <hr>
                          </div>
                          <div class="col s6">
                            <p><span class='fa fa-file-o'></span> ประเภทการพิมพ์รายงาน : </p>
                          </div>
                          <div class='form-group col-md-6' id="report_year_type">
                            <div class="input-field custom sm"  id="report_selectType">
                            <select class="form-control" name="report_selectType" id="report_Type">
                                <option value="0">พิมพ์รายบุคคล</option>
                                <option value="1">พิมพ์ตามไซต์งาน</option>
                                <option value="2">พิมพ์ตามแผนก</option>
                            </select>
                            </div>
                          </div>
                        </div>

                        <div class="row" id="by_person">
                          <div class="col s12">
                             <label for="search_user_report">ค้นหาชื่อพนักงาน</label>
                             <form onsubmit="$('#search_user_report').blur();return false;" class="pure-form" style="padding-bottom:10px;">
                               <input id="search_user_report" type="text" name="q" placeholder="ชื่อ หรือนามสกุล" class="form-control">
                             </form>
                          </div>
                          <!-- <div class="col s12">
                              <br>
                              <label>ข้อมูลที่เลือก</label>
                              <p id='getname'> - </p>
                              <label>ไซต์</label>
                              <p id='getsite'> - </p>
                              <label>สาขา</label>
                              <p id='getbranch'> - </p>
                              <hr>
                          </div> -->
                          <input type="hidden" id='index' name="index" value="">
                        </div>

                        <div class="row" id="by_site" style="display:none;">
                          <div class="col-md-6">
                            <label>ไซต์ *</label>
                            <div class="input-field custom sm"  id="report_site">
                                <select class="form-control" name="r_site" id="r_site">
                                </select>
                            </div>
                            <label class='feedback' id="feedback-site"></label>
                          </div>
                          <div class="col-md-6">
                            <label>สาขา *</label>
                            <div class="input-field custom sm"  id="report_branch">
                              <select class="form-control" name="r_branche" id="r_branche">
                                <option value="" disabled selected>เลือกรายการ </option>
                              </select>
                            </div>
                            <label class='feedback' id="feedback-branch"></label>
                          </div>
                        </div>

                        <div class="row" id="by_dep" style="display:none;">
                          <div class="col s12">
                            <div class="input-field custom sm"  id="report_dep">
                            <select class="form-control" name="r_dep" id="r_dep">
                                <option value="">เลือกแผนก</option>
                            </select>
                            </div>
                          </div>
                        </div>
                  </form>
                  </div>
                  <div class="modal-footer">
                      <button type="button" class="btn btn-primary btn-report-attendance">พิมพ์ข้อมูล</button>
                  </div>
              </div>
          </div>

      </div>


        <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="exampleModalLabel1">ค้นหา</h4>
                    </div>
                    <div class="modal-body">
                      <div class="row">
                        <div class="form-group col-md-6 " id="searchtype">
                              <label class="col-md-12">ประเภทการค้นหา</label>
                              <div class="col-md-12">
                                <select class="form-control" name="searchtype" id="search_type">
                                  <option value="day" selected>รายวัน </option>
                                  <option value="month">รายเดือน </option>
                                  <option value="year">รายปี </option>
                                </select>
                              </div>
                            </div>
                            <div class="type_ofsearch form-group col-md-6" id="day_type">
                              <div class="input-group has-feedback">
                                <label class="col-md-12">วันที่</label>
                                <input type="text" class="form-control mydatepicker" id="datepicker" name="datepicker" placeholder="วัน/เดือน/ปี">
                                <span class="form-control-feedback"><i class="icon-calender"></i></span>
                              </div>
                                </div>
                                <?php $month = array("มกราคม","กุมภาพันธ์","มีนาคม","เมษายน","พฤษภาคม","มิถุนายน","กรกฎาคม","สิงหาคม","กันยายน","ตุลาคม","พฤศจิกายน","ธันวาคม"); ?>
                              <div class='type_ofsearch form-group col-md-6' id="month_type" style="display:none;">
                                <label>เดือน</label>
                                <div class="input-field custom sm"  id="searchmonth">
                                <select class="form-control" name="searchmonth" id="selectsearchmonth">
                                  <?php for ($i=0; $i < count($month); $i++) { $num = $i+1; ?>
                                    <option value="<?php echo $num ?>"><?php echo $month[$i]; ?></option>
                                  <?php } ?>
                                </select>
                                </div>
                              </div>
                              <div class='type_ofsearch form-group col-md-6' id="year_type" style="display:none;">
                                <label>ปี</label>
                                <div class="input-field custom sm"  id="searchyear">
                                <select class="form-control" name="searchyear" id="selectsearchyear">
                                  <?php for ($i=0; $i < 10; $i++) { $year = 2018+$i; ?>
                                    <option value="<?php echo $year; ?>"><?php echo $year; ?></option>
                                  <?php } ?>
                                </select>
                                </div>
                              </div>
                      </div>

                      <div class="row">
                        <div class="form-group col-md-6">
                          <label class="col-md-12">ไซต์</label>
                          <div class="col-md-12">
                            <select class="form-control" name="site" id="site">

                            </select>
                          </div>
                          </div>
                          <div class="form-group col-md-6">
                            <label class="col-md-12">สาขา</label>
                            <div class="col-md-12">
                              <select class="form-control" name="branch" id="branch">

                              </select>
                            </div>
                            </div>
                      </div>
                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-primary btnsearchall" data-dismiss="modal">ค้นหา</button>
                    </div>
                </div>
            </div>
        </div>










</section>
</div>

<div class="white-box">
  <div class="row">
    <div class="col-lg-12">
        <div class="white-box">
              <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <div class='checkbox checkbox-success'><input class='select-all' data-size="large" id='select-all' onclick='chk_all()' type='checkbox'/>&nbsp;<label for="select-all">เลือกทั้งหมด</label></div>
              </div>
            <div class='col-lg-9 col-sm-8 col-md-8 col-xs-12' align='right'>
                <button class='btn btn-success btn-rounded all-approve disabled' data-position='top' data-delay='50' data-tooltip='อนุมัติ' disabled><span class='fa fa-check'></span> อนุมัติ<span class='approve_num'></span> </button> &nbsp;
                <button class='btn btn-danger btn-rounded all-not-approve disabled' data-position='top' data-delay='50' data-tooltip='ไม่อนุมัติ' disabled><span class='fa fa-remove'></span> ไม่อนุมัติ<span class='reject_num'></span></button>
          </div>
            <table class="table table-bordered " id="attendance_table" data-segment="users">
                <thead id='attendance_head'>
                    <tr id='headcolumn'>
                        <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="4"><div class='checkbox'><input class='select-all' id='select-all' type='checkbox'/><label></label></div></th>
                        <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="4"></th>
                        <th scope="col" data-tablesaw-sortable-col data-tablesaw-sortable-default-col data-tablesaw-priority="persist">ชื่อ - นามสกุล</th>
                        <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="2">ช่วงเวลา</th>
                        <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="2">เวลาเข้า</th>
                        <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="2">เวลาออก</th>
                        <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="4">สาย</th>
                        <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="4">เวลาทำงาน</th>
                        <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="4">สถานะ</th>
                        <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="4">ช่องทางเข้า</th>
                        <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="4">ช่องทางออก</th>
                        <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="4">ตัวเลือก</th>
                    </tr>
                </thead>

                <tbody id='attendance_body'>

                </tbody>

            </table>
        </div>
    </div>
</div>
</div>
