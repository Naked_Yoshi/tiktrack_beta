<!-- sample modal content -->
                        <div id="AdjustModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                              <div class="modal-dialog">
                                  <div class="modal-content">
                                      <div class="modal-header">
                                          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                          <h4 class="modal-title" id="myModalLabel">แก้ไขการลงเวลา เข้า-ออก</h4>
                                      </div>
                                      <div class="modal-body">
                                        <div class="row">
                                          <div class="col-md-12">
                                            ชื่อ - นามสกุลพนักงาน : <span id="adjust_emp_name"></span>
                                          </div>
                                        </div>
                                        <br>

                                        <div class="row">
                                          <div class="col-md-6">
                                            <div class="form-group">
                                              <label for="usrcreateTitle">วันที่ลงเวลาเข้า :</label>
                                              <input type="text" class="form-control" value="" id="tat_date_checkin" name="tat_date_checkin" readonly>
                                            </div>
                                          </div>
                                          <div class="col-md-6">
                                            <label for="break_end_group">เวลาเข้า</label>
                                            <div class="form-group has-feedback" id="tat_time_checkin_group">
                                                <div class="input-group">
                                                  <input type="text" class="form-control clockpicker" name="tat_time_checkin" id="tat_time_checkin" placeholder="เวลาเลิกพัก">
                                                  <span class="form-control-feedback" id="break_start_feedback"></span>
                                                  <span class="input-group-btn">
                                                    <button class="btn btn-default btn-outline btn-cancel-time" data-id="tat_time_checkin" type="button"><span class='fa fa-remove'></span></button>
                                                  </span>
                                                </div>
                                            </div>
                                          </div>
                                        </div>

                                        <div class="row">
                                          <div class="col-md-6">
                                            <div class="form-group">
                                              <label for="usrcreateTitle">วันที่ลงเวลาออก :</label>
                                              <input type="text" class="form-control" value="" id="tat_date_checkout" name="tat_date_checkout" readonly>
                                            </div>
                                          </div>
                                          <div class="col-md-6">
                                            <label for="break_end_group">เวลาออก :</label>
                                            <div class="form-group has-feedback" id="tat_time_checkout_group">
                                                <div class="input-group">
                                                  <input type="text" class="form-control clockpicker" name="tat_time_checkout" id="tat_time_checkout" placeholder="เวลาเลิกพัก">
                                                  <span class="form-control-feedback" id="break_start_feedback"></span>
                                                  <span class="input-group-btn">
                                                    <button class="btn btn-default btn-outline btn-cancel-time" data-id="tat_time_checkout" type="button"><span class='fa fa-remove'></span></button>
                                                  </span>
                                                </div>
                                            </div>

                                          </div>
                                        </div>

                                        <hr>

                                        <div class="row">
                                          <div class="">
                                          <h4>ประวัติการแก้ไข :</h4>
                                          </div>
                                          <div class="">
                                            <table class="table table-bordered " id="adjust_table" data-segment="users">
                                                <thead>
                                                    <tr>
                                                        <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="4" class='text-center'>ลำดับ</th>
                                                        <th scope="col" data-tablesaw-sortable-col data-tablesaw-sortable-default-col data-tablesaw-priority="4">เวลาเข้า</th>
                                                        <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="4" class='text-center'>เวลาออก</th>
                                                        <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="4" class='text-center'>ผู้แก้ไข</th>
                                                        <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="4" class='text-center'>วันที่แก้ไข</th>
                                                    </tr>
                                                </thead>

                                                <tbody id='adjust_body'>

                                                </tbody>

                                            </table>
                                          </div>
                                        </div>



                                      </div> <!-- <div class="modal-body"> -->
                                      <div class="modal-footer">
                                        <input type="hidden" id="pre_change_check_in_time" name="" value="">
                                        <input type="hidden" id="pre_change_check_out_time" name="" value="">
                                          <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">ปิด</button>
                                          <button type="button" class="btn btn-success waves-effect btn-save-adjust">บันทึก</button>
                                      </div>
                                  </div>
                                  <!-- /.modal-content -->
                              </div>
                              <!-- /.modal-dialog -->
                          </div>
