<!-- sample modal content -->
                          <div id="DetailModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                              <div class="modal-dialog">
                                  <div class="modal-content">
                                      <div class="modal-header">
                                          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                          <h4 class="modal-title" id="myModalLabel">รายละเอียดการลงเวลา เข้า-ออก</h4>
                                      </div>
                                      <div class="modal-body">
                                          <!-- <h4>Overflowing text to show scroll behavior</h4>
                                          <p>Praesent commodo cursus magna, vel scelerisque nisl consectetur et. Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor.</p>
                                          <p>Aenean lacinia bibendum nulla sed consectetur. Praesent commodo cursus magna, vel scelerisque nisl consectetur et. Donec sed odio dui. Donec ullamcorper nulla non metus auctor fringilla.</p>
                                        -->

                        <!--<ul class="tabs tabs-fixed-width">-->
                        <ul class="nav nav-tabs" role="tablist">
                          <!--<li class="tab col s3"><a class="map-in active"><span class='fa fa-angle-double-up'></span> สถานที่การลงเวลาเข้า</a></li>
                          <li class="tab col s3"><a class='map-out'><span class='fa fa-angle-double-down'></span> สถานที่การลงเวลาออก</a></li>-->
                          <li role="presentation" ><a href="#chekin" aria-controls="home" role="tab" data-toggle="tab" aria-expanded="false" class="map_in"><span class="visible-xs"><i class="ti-home"></i></span><span class="hidden-xs"><i class="fa fa-location-arrow"></i> &nbsp; สถานที่การลงเวลาเข้า</span></a></li>
                          <li role="presentation" ><a href="#checkout" aria-controls="profile" role="tab" data-toggle="tab" aria-expanded="false" class="map_out"><span class="visible-xs"><i class="ti-user"></i></span> <span class="hidden-xs"><i class="fa fa-location-arrow"></i> &nbsp; สถานที่การลงเวลาออก</span></a></li>
                          <!--<li role="presentation" class=""><a href="#messages" aria-controls="messages" role="tab" data-toggle="tab" aria-expanded="false"><span class="visible-xs"><i class="ti-email"></i></span> <span class="hidden-xs">Messages</span></a></li>-->
                        </ul>

                            <div class="tab-content">

                                <div role="tabpanel" class="tab-pane active" id="chekin">
                                    <div class='col s12' id="map_in" style="height:250px;"></div>

                                </div> <!-- tab ที่ 1 -->

                                <div role="tabpanel" class="tab-pane" id="checkout">
                                    <div class='col s12' id="map_out" style="height:250px;"></div>

                                </div> <!-- tab ที่ 2 -->

                            </div> <!-- tab content -->





                                        <div class="panel panel-default block1">
                                          <div class="panel-heading" style="font-size:160%"><i class="fa fa-chevron-circle-left"></i> &nbsp; ข้อมูล</div>
                                          <div class="panel-wrapper collapse in">

                                            <div class="panel-body">



                                                <div class="col s12">
                                                  <label>ชื่อ - นามสกุล </label>
                                                  <p class ="label_name" id="label_name" style="margin-left:5%; font-size:140%"></p>
                                                </div>

                                                  <hr>



                                                <!--<div class="panel-wrapper collapse in">-->

                                                    <div class="col s12 m6">
                                                      <label>ไซต์ </label>
                                                        <p class="label_site" id="label_site" style="margin-left:5%; font-size:140%"></p>
                                                      </div>
                                                    <!--</div>--> <!-- panel-wrapper collapse in -->

                                                      <hr>



                                                <div class="col s12 m6">
                                                  <label>สาขา </label>
                                                  <p class="label_branch" id="label_branch" style="margin-left:5%; font-size:140%"></p>
                                                </div>

                                                  <hr>






                                                <div class="col s12 m6">
                                                  <p><span class='fa fa-clock-o'></span> การลงเวลา</p>
                                                  <!--<li class="divider"></li><br>-->

                                                </div>

                                                  <hr>


                                                <div class="col s12">
                                                  <label> <i class="fa fa-calendar"></i> &nbsp; วันที่ </label>
                                                  <p class="label_check_date" id="label_check_date" style="margin-left:5%; font-size:140%"></p>
                                                </div>

                                                  <hr>

                                                <div class="col s12 m6">
                                                  <label> <i class="fa fa-map-marker"></i> &nbsp; เวลาเข้า </label>
                                                  <b><p class="label_checkin_time" id="label_checkin_time" style="margin-left:5%; font-size:140%"></p></b>
                                                  <p> <i class="fa fa-tags"></i> &nbsp; หมายเหตุ : <span id="tat_message_in"></span></p>
                                                </div>

                                                  <hr>

                                                <div class="col s12 m6">
                                                  <label> <i class="fa fa-power-off"></i> &nbsp; เวลาออก </label>
                                                    <p class="label_checkout_time" id="label_checkout_time" style="margin-left:5%; font-size:140%"></p>
                                                    <p> <i class="fa fa-tags"></i> &nbsp; หมายเหตุ : <span id="tat_message_out"></span></p>
                                                </div>

                                                  <hr>

                                                <div class="col s12 m6">
                                                  <label>สาย </label>
                                                <p class="label_late_time" id="label_late_time" style="margin-left:5%; font-size:140%"></p>
                                                </div>



                                            </div> <!-- panel body -->



                                          <div class="panel-heading" style="font-size:160%"><i class="fa fa-chevron-circle-left"></i> &nbsp; ช่องทางการลงเวลา</div>
                                          <div class="panel-body">

                                                <div class="col s12 m6">
                                                  <label><i class="fa fa-sign-in"></i> &nbsp; ช่องทางการลงเวลาเข้า </label>
                                                  <p class="label_checkin_route_id" id="label_checkin_route_id" style="margin-left:5%; font-size:140%"></p>
                                                </div>



                                                <hr>


                                                <div class="col s12 m6">
                                                  <label> <i class="fa fa-sign-out"></i> &nbsp; ช่องทางการลงเวลาออก </label>
                                                  <p class="label_checkout_route_id" id="label_checkout_route_id" style="margin-left:5%; font-size:140%"></p>
                                                </div>
                                            <!--  </div>--> <!-- div class row -->

                                              <hr>

                                              <div class="col s12">
                                                <!--<li class="divider"></li>-->
                                                <p> <i class="fa fa-check-square-o"></i> &nbsp; การอนุมัติ</p>

                                              </div>

                                              <div class="col s12">
                                                <p class="label_approveStatus" id="label_approveStatus" style="margin-left:5%; font-size:140%"></p>
                                              </div>


                                            </div> <!--  class="panel-body"  -->

                                            <!-- <div class="panel-footer">
                                              <div class="col s12">

                                                <p> <i class="fa fa-tags"></i> &nbsp; หมายเหตุ</p>
                                                <div class="col s12">
                                                  <p style="margin-left:5%; font-size:140%"></p>
                                                </div>
                                                <span id="att_detail"></span>
                                              </div>
                                            </div> <!-- class="panel-footer" -->

                                          </div>
                                        </div>




                                      </div> <!-- <div class="modal-body"> -->








                                      <div class="modal-footer">
                                          <button type="button" class="btn btn-info waves-effect" data-dismiss="modal">ปิด</button>
                                      </div>
                                  </div>
                                  <!-- /.modal-content -->
                              </div>
                              <!-- /.modal-dialog -->
                          </div>
