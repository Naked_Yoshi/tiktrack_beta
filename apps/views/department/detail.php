		<!-- <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script> -->
    <div id="DetailDev" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="depCreateLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="clearModals();">×</button>
            <h5 class="modal-title" id=""> แผนผังองค์กรแผนก  : <span id="dep_name_label"></span></h5>
        </div>
        <div class="modal-body">

          <div class="row">
              <div class="col s12">
                <h5> <?php/* echo $detail["Department"][0]["depName"]*/ ?></h5>
                <h6 class='grey-text text-lighten-1'><?php/* echo $detail["Department"][0]["depCaption"] */?></h6>
              </div>

              <div class="col s12">
                <br>
                <span class='fa fa-sitemap'></span> แผนผังแผนก
                <hr>
                <div class="container-chart" id="detail">
                  <center>
                  <div id="chart_detail"></div>
                </center>
                </div>
              </div>
          </div>


        </div>
        <div class="modal-footer">

        </div>
      </div>
    </div>
  </div>
