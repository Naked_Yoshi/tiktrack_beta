<div id="depCreate" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="depCreateLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header modal-header-success">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="clearModals();">×</button>
          <h4 class="modal-title" id="depCreateLabel"> เพิ่มแผนก</h4>
      </div>
      <div class="modal-body">

        <form id="depCreateForm">
          <div class='row'>
            <div class="col-md-12">
              <div class="form-group has-feedback" id="depcreateName_group">
                <label for="depcreateName">ชื่อแผนก</label>
                <input type="text" class="form-control validate" name="depName" id="depcreateName" placeholder="กรอกชื่อแผนก">
                <span class="form-control-feedback" id="depcreateName_feedback"></span>
              </div>
            </div>
            <div class="col-md-12">
              <div class="form-group">
                <label for="depcreateDesc">คำอธิบาย</label>
                <textarea class="form-control" name="depDesc" id="depcreateDesc" rows="5" placeholder="กรอกคำอธิบาย"></textarea>
              </div>
            </div>
          </div>
        </form>

      </div>
      <div class="modal-footer">
      <button type="button" class="btn btn-success waves-effect submit-create-dep">บันทึกข้อมูล</button>
      <button type="button" class="btn btn-info waves-effect" data-dismiss="modal" onclick="clearModals();">ปิดหน้าต่าง</button>
      </div>
    </div>
  </div>
</div>
