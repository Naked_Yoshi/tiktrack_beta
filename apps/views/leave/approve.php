
<div class="white-box">
  <div class='col-lg-6 col-sm-6 col-md-6 col-xs-6' align='left'>
    <button class='btn btn-info btn_filter' data-position='top' data-delay='50' data-tooltip='ค้นหา'>
      <span class='fa fa-filter'></span> ค้นหา</button>
  </div>
    <div class='col-lg-6 col-sm-6 col-md-6 col-xs-6' align='right' style='padding-bottom:20px'>
      <button class='btn btn-info btn_print' data-position='top' data-delay='50' data-tooltip='พิมพ์รายงาน'>
        <span class='fa fa-print'></span> พิมพ์รายงาน</button>
    </div>
<table class="table table-bordered " id="leave_table" data-segment="users">
<thead>
  <tr>
      <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="4" class='text-center'>no</th>
      <th scope="col" data-tablesaw-sortable-col data-tablesaw-sortable-default-col data-tablesaw-priority="persist">ชื่อ - นามสกุล</th>
      <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="4" class='text-center'>วันที่ขอ</th>
      <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="2" class='text-center'>ประเภท</th>
      <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="4" class='text-center'>รายละเอียด</th>
      <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="2" class='text-center'>เริ่ม</th>
      <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="2" class='text-center'>สิ้นสุด</th>
      <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="4" class='text-center'>ไซต์</th>
      <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="4" class='text-center'>สาขา</th>
      <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="4" class='text-center'>สถานะ</th>
      <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="4" class='text-center'>รวม</th>
      <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="4" class='text-center'>ตัวเลือก</th>

  </tr>
</thead>
<tbody id='leave_body'>

</tbody>
</table>
</div>
