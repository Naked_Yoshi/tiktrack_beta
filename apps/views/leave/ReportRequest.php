<div id="LeaveRequestReport" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="depCreateLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="clearModals();">×</button>
          <h4 class="modal-title" id="OTrequest"><span class='fa fa-print'></span>  พิมพ์รายงานการขอลา</h4>
      </div>
      <div class="modal-body">

        <!-- <form id="OTrequest"> -->
          <form class='report_form' method="post" target="_blank" action='<?php echo base_url('vacation/previewReport'); ?>'>
            <div class="row">
              <div class="col-md-6">
                <label>ปี</label>
                <div class="input-field custom sm" name="searchyear">
                <select name="searchyear" id="searchyear" class="form-control validate">
                  <?php for ($i=0; $i < 10; $i++) { $year = 2017+$i; ?>
                    <option value="<?php echo $year; ?>"><?php echo $year; ?></option>
                  <?php } ?>
                </select>
                </div>

              </div>
              <?php $month = array("มกราคม","กุมภาพันธ์","มีนาคม","เมษายน","พฤษภาคม","มิถุนายน","กรกฎาคม","สิงหาคม","กันยายน","ตุลาคม","พฤศจิกายน","ธันวาคม"); ?>
              <div class="col-md-6">
                <div class="form-group has-feedback" id="depcreateName_group">
                      <label>เดือน</label>
                      <select name="searchmonth" id="searchmonth" class="form-control validate">
                        <?php for ($i=0; $i < count($month); $i++) { $num = $i+1; ?>
                          <option value="<?php echo $num ?>"><?php echo $month[$i]; ?></option>
                        <?php } ?>
                      </select>
                  <!-- <span class="form-control-feedback" id="depcreateName_feedback"></span> -->
                </div>
              </div>
            </div>
            <input type="hidden" id="leave_request" name="leave_request" value="">
            <input type="hidden" id="auth" name="auth" value="">
            <input type="hidden" id="company_id" name="company_id" value="">
          </form>

      </div>
      <div class="modal-footer">
      <button type="button" class="btn btn-info waves-effect  submit-report-print-leave">พิมพ์</button>

      </div>
    </div>
  </div>
</div>
