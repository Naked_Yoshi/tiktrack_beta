<div id="LeaveApproveModel" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title" id="myModalLabel">การอนุมัติการลา</h4>
            </div>
            <div class="modal-body">
              <div class="row">
                <div class="col-md-4">
                  <h2>สถานะการอนุมัติ : </h2>
                </div>
                <div class="col-md-8" style='font-size:20px;padding-top:12px;' id="tag_approve_status">
                  <!-- <span class='label label-danger'>ไม่อนุมัติ</span> -->
                </div>
              </div>
              <hr>
              <div class="row">
                <div class="form-group">
                    <label class="col-md-12"><h5>วันที่มีผล</h4></label>
                    <div class="col-md-12">
                      <p id="txt_sdate_edate"></p>
                      <p id="txt_stime_etime"></p>
                    </div>
                </div>
              </div>
              <div class="row">
                <div class="form-group">
                    <label class="col-md-12"><h5>ประเภทการลา</h4></label>
                    <div class="col-md-12">
                      <p id="txt_leave_type"></p>
                    </div>
                </div>
              </div>
              <div class="row">
                <div class="form-group">
                    <label class="col-md-12"><h5>รายละเอียดการลา</h4></label>
                    <div class="col-md-12">
                      <p id="txt_detail"></p>
                    </div>
                </div>
              </div>
              <div class="row">
                <div class="form-group">
                    <label class="col-md-12"><h5>รวม</h4></label>
                    <div class="col-md-12">
                      <p id="txt_sum_leave"></p>
                    </div>
                </div>
              </div>
            <hr>
              <div class="row">
                <div class="form-group">
                  <label class="col-md-12"><h5>การอนุมัติ</h4></label>
                  <table class="table table-responsive ">
                    <thead>
                        <tr>
                            <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="4" class='text-left' width='100'>Lv.</span></th>
                            <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="4" class='text-left'>สถานะ</th>
                        </tr>
                    </thead>
                    <tbody id="leave_approve_body">

                    </tbody>
                  </table>
                </div>
              </div>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
