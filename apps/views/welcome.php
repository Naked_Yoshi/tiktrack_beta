<!DOCTYPE html>
<html lang='en'>
  <head>
    <meta charset="utf-8">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" type="image/png" sizes="16x16" href="<?php echo base_url('assets/plugins/material/favicon.png'); ?>">
    <title>#TIKTRACK Best service for time attendance solution.</title>
    <!-- Bootstrap Core CSS -->
    <link href="<?php echo base_url("assets/bootstrap/dist/css/bootstrap.min.css"); ?>" rel="stylesheet">
    <!-- Menu CSS -->
    <link href="<?php echo base_url("assets/plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.css"); ?>" rel="stylesheet">
    <!-- toast CSS -->
    <link href="<?php echo base_url("assets/plugins/bower_components/toast-master/css/jquery.toast.css"); ?>" rel="stylesheet">
    <!-- morris CSS -->
    <link href="<?php echo base_url("assets/css/animate.css"); ?>" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="<?php echo base_url("assets/css/style.css"); ?>" rel="stylesheet">
    <!-- color CSS -->
    <link href="<?php echo base_url("assets/css/colors/default.css"); ?>" id="theme" rel="stylesheet">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
  </head>
  <body>
    <style media="screen">

    </style>
    <div class="preloader">
        <div class="cssload-speeding-wheel"></div>
    </div>
    <div id="wrapper">
        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top m-b-0">
            <div class="navbar-header">
                <a class="navbar-toggle hidden-sm hidden-md hidden-lg " href="javascript:void(0)" data-toggle="collapse" data-target=".navbar-collapse"><i class="ti-menu"></i></a>
                <div class="top-left-part">
                    <a class="logo" href="index.html">
                        <b>
                        <!--This is dark logo icon--><img src="<?php echo base_url("assets/plugins/material/tiktrack-logo.png"); ?>" alt="home" class="dark-logo" /><!--This is light logo icon--><img src="<?php echo base_url("assets/plugins/material/tiktrack-logo.png"); ?>" alt="home" class="light-logo" />
                     </b>
                        <span class="hidden-xs">
                        <!--This is dark logo text--><img src="<?php echo base_url("assets/plugins/material/tiktrack-logo-text.png"); ?>" alt="home" class="dark-logo" /><!--This is light logo text--><img src="<?php echo base_url("assets/plugins/material/tiktrack-logo-text.png"); ?>" alt="home" class="light-logo" />
                     </span>
                    </a>
                </div>
                <ul class="nav navbar-top-links navbar-left hidden-xs">
                    <li><a href="javascript:void(0)" class="open-close hidden-xs waves-effect waves-light"><i class="icon-arrow-left-circle ti-menu"></i></a></li>
                </ul>
                <ul class="nav navbar-top-links navbar-right pull-right">
                    <!-- /.Megamenu -->
                    <li class="right-side-toggle"> <a class="waves-effect waves-light" href="javascript:void(0)"><i class="ti-settings"></i></a></li>
                    <!-- /.dropdown -->
                </ul>
            </div>
            <!-- /.navbar-header -->
          </nav>
        <!-- Left navbar-header -->
        <div class="navbar-default sidebar" role="navigation">
            <div class="sidebar-nav navbar-collapse slimscrollsidebar">
                <div class="user-profile">
                    <div class="dropdown user-pro-body">
                        <div><img src="<?php echo base_url("assets/plugins/images/users/varun.jpg"); ?>" alt="user-img" class="img-circle"></div>
                        <a href="#" class="dropdown-toggle u-dropdown" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Steave Gection <span class="caret"></span></a>
                        <ul class="dropdown-menu animated flipInY">
                            <li><a href="#"><i class="ti-user"></i> My Profile</a></li>
                            <li><a href="#"><i class="ti-wallet"></i> My Balance</a></li>
                            <li><a href="#"><i class="ti-email"></i> Inbox</a></li>
                            <li role="separator" class="divider"></li>
                            <li><a href="#"><i class="ti-settings"></i> Account Setting</a></li>
                            <li role="separator" class="divider"></li>
                            <li><a href="login.html"><i class="fa fa-power-off"></i> Logout</a></li>
                        </ul>
                    </div>
                </div>
                <ul class="nav" id="side-menu">
                    <li class="sidebar-search hidden-sm hidden-md hidden-lg">
                        <!-- input-group -->
                        <div class="input-group custom-search-form">
                            <input type="text" class="form-control" placeholder="Search..."> <span class="input-group-btn">
                        <button class="btn btn-default" type="button"> <i class="fa fa-search"></i> </button>
                        </span>
                        </div>
                        <!-- /input-group -->
                    </li>
                    <li>
                        <a href="inbox.html" class="waves-effect"><i class=" icon-notebook"></i> <span class="hide-menu">บันทึกลงเวลา<span class="fa arrow"></span><span class="label label-rouded label-danger pull-right">New</span></span></a>
                        <ul class="nav nav-second-level">
                            <li><a href="chat.html">รายวัน</a></li>
                            <li><a href="chat.html">รายบุคคล</a></li>
                        </ul>
                    </li>
                    <li>
                        <a href="inbox.html" class="waves-effect"><i class="icon-clock"></i> <span class="hide-menu">บันทึกลงเวลา</a>
                    </li>
                    <li>
                        <a href="inbox.html" class="waves-effect"><i class="icon-location-pin"></i> <span class="hide-menu">ไซต์และสาขา</a>
                    </li>
                    <li>
                        <a href="inbox.html" class="waves-effect"><i class="fa fa-sitemap"></i> <span class="hide-menu">องค์กร</a>
                    </li>
                    <li>
                        <a href="inbox.html" class="waves-effect"><i class="icon-people"></i> <span class="hide-menu">จัดการพนักงาน</a>
                    </li>
                </ul>
            </div>
        </div>
        <!-- Left navbar-header end -->
        <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row bg-title">
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h4 class="page-title">Dashboard 1</h4>
                    </div>
                    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                        <!-- <a href="http://wrappixel.com/templates/pixeladmin/" target="_blank" class="btn btn-danger pull-right m-l-20 btn-rounded btn-outline hidden-xs hidden-sm waves-effect waves-light">Buy Now</a> -->
                        <ol class="breadcrumb">
                            <li><a href="#">Dashboard</a></li>
                            <li class="active">Dashboard 1</li>
                        </ol>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- /.row -->


            <!-- /.container-fluid -->
            <footer class="footer text-center"> 2018 &copy; #TIKTRACK Best service for time attendance solution. </footer>
        </div>
        <!-- /#page-wrapper -->
    </div>
  </div>
  </body>
  <!-- /#wrapper -->
  <!-- jQuery -->
  <script src="<?php echo base_url("assets/plugins/bower_components/jquery/dist/jquery.min.js"); ?>"></script>
  <!-- Bootstrap Core JavaScript -->
  <script src="<?php echo base_url("assets/bootstrap/dist/js/bootstrap.min.js");?>"></script>
  <!-- Menu Plugin JavaScript -->
  <script src="<?php echo base_url("assets/plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.js"); ?>"></script>
  <!--slimscroll JavaScript -->
  <script src="<?php echo base_url("assets/js/jquery.slimscroll.js");?>"></script>
  <!--Wave Effects -->
  <script src="<?php echo base_url("assets/js/waves.js");?>"></script>
  <!--Counter js -->
  <script src="<?php echo base_url("assets/plugins/bower_components/waypoints/lib/jquery.waypoints.js"); ?>"></script>
  <script src="<?php echo base_url("assets/plugins/bower_components/counterup/jquery.counterup.min.js"); ?>"></script>
  <!--Morris JavaScript -->
  <script src="<?php echo base_url("assets/plugins/bower_components/raphael/raphael-min.js"); ?>"></script>
  <!-- Custom Theme JavaScript -->
  <script src="<?php echo base_url("assets/js/custom.min.js"); ?>"></script>
  <!-- Sparkline chart JavaScript -->
  <script src="<?php echo base_url("assets/plugins/bower_components/jquery-sparkline/jquery.sparkline.min.js"); ?>"></script>
  <script src="<?php echo base_url("assets/plugins/bower_components/jquery-sparkline/jquery.charts-sparkline.js"); ?>"></script>
  <script src="<?php echo base_url("assets/plugins/bower_components/toast-master/js/jquery.toast.js"); ?>"></script>
  <script src="<?php echo base_url("assets/plugins/bower_components/styleswitcher/jQuery.style.switcher.js"); ?>"></script>
</html>
