<div class="row bg-title">
    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
        <h4 class="page-title"><?php echo $pageName ?></h4>
    </div>
    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
        <?php if (isset($button_create))  echo $button_create; ?>
        <ol class="breadcrumb">
            <li><a href="#">สยามราชธานี</a></li>
            <?php echo $pageactive ?>
        </ol>
    </div>
    <!-- /.col-lg-12 -->
</div>
