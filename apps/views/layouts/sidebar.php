  <div class="navbar-default sidebar" role="navigation">
    <div class="sidebar-nav navbar-collapse slimscrollsidebar">
        <div class="user-profile">
            <div class="dropdown user-pro-body">
                <div><img src="<?php echo base_url("assets/plugins/images/users/varun.jpg"); ?>" alt="user-img" class="img-circle"></div>
                <a href="#" class="dropdown-toggle u-dropdown" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><span id="fullname"></span><span class="caret"></span></a>
                <ul class="dropdown-menu animated flipInY">
                    <li class="btn-change-pass"><a href="#"><i class="fa fa-key"></i> Change Password</a></li>
                    <li role="separator" class="divider"></li>
                    <li class="btn-logout"><a href="#"><i class="fa fa-power-off"></i> Logout</a></li>
                </ul>
            </div>
        </div>
        <ul class="nav" id="side-menu">
            <li class="sidebar-search hidden-sm hidden-md hidden-lg">
                <!-- input-group -->
                <div class="input-group custom-search-form">
                    <input type="text" class="form-control" placeholder="Search..."> <span class="input-group-btn">
                <button class="btn btn-default" type="button"> <i class="fa fa-search"></i> </button>
                </span>
                </div>
                <!-- /input-group -->
            </li>
            <li>
                <a href="<?php echo base_url("attendance/users"); ?>" class="waves-effect"><i class=" icon-notebook"></i> <span class="hide-menu">บันทึกลงเวลา</span></a>
                <!-- <span class="label label-rouded label-danger pull-right">New</span> -->
            </li>
            <li class="nav nav-first-level collapse">
                <a href="#" class="waves-effect"><i class="icon-clock"></i> <span class="hide-menu">ล่วงเวลา OT<span class="fa arrow"></span></a>
                  <ul class="nav nav-second-level collapse ">
                    <li>
                      <a href="<?php echo base_url("otdata/requestLoad"); ?>" class="waves-effect"><span class="hide-menu">ขออนุมัติ</a>
                    </li>
                    <li>
                      <a href="<?php echo base_url("otdata/completeLoad"); ?>" class="waves-effect"><span class="hide-menu">อนุมัติ</a>
                    </li>
                    <li>
                      <a href="<?php echo base_url("otdata/paidLoad"); ?>" class="waves-effect"><span class="hide-menu">เบิกอนุมัติ</a>
                    </li>
                    <li>
                      <a href="<?php echo base_url("otdata/rejectLoad"); ?>" class="waves-effect"><span class="hide-menu">ยกเลิก</a>
                    </li>
                  </ul>
            </li>
            <li class="nav nav-first-level collapse">
                <a href="#" class="waves-effect"><i class="fa fa-plane"></i> <span class="hide-menu">บันทึกการลา<span class="fa arrow"></span></a>
                  <ul class="nav nav-second-level collapse ">
                    <li>
                      <a href="<?php echo base_url("vacation/LeaveRequest"); ?>" class="waves-effect"><span class="hide-menu">ขออนุมัติ</a>
                    </li>
                    <li>
                      <a href="<?php echo base_url("vacation/LeaveApprove"); ?>" class="waves-effect"><span class="hide-menu">อนุมัติ</a>
                    </li>
                    <li>
                      <a href="<?php echo base_url("vacation/LeaveReject"); ?>" class="waves-effect"><span class="hide-menu">ยกเลิก</a>
                    </li>
                  </ul>
            </li>
            <li class="nav nav-first-level collapse">
                <a href="#" class="waves-effect"><i class="fa fa-file-text-o"></i> <span class="hide-menu">รายงาน<span class="fa arrow"></span></a>
                  <ul class="nav nav-second-level collapse ">
                    <li>
                      <a href="<?php echo base_url("ReportForm/report_attendance"); ?>" class="waves-effect"><span class="hide-menu">บันทึกลงเวลา</a>
                    </li>
                    <li>
                      <a href="<?php echo base_url("vacation/LeaveApprove"); ?>" class="waves-effect"><span class="hide-menu">ล่วงเวลา</a>
                    </li>
                    <!-- <li>
                      <a href="<?php echo base_url("vacation/LeaveReject"); ?>" class="waves-effect"><span class="hide-menu">การลา</a>
                    </li> -->
                  </ul>
            </li>
            <!-- <li class="isAdmin">
                <a href="<?php echo base_url("exportreport"); ?>" class="waves-effect"><i class="fa fa-file-text-o"></i> <span class="hide-menu">รายงาน & ส่งออกข้อมูล</a>
            </li> -->
            <li class="isAdmin-org">
                <a href="<?php echo base_url("organization"); ?>" class="waves-effect"><i class="fa fa-sitemap"></i> <span class="hide-menu">องค์กร</a>
            </li>
            <li class="isAdmin">
                <a href="<?php echo base_url("users"); ?>" class="waves-effect"><i class="icon-people"></i> <span class="hide-menu">จัดการพนักงาน</a>
            </li >
            <li class="nav nav-first-level collapse isAdmin">
                <a href="#" class="waves-effect"><i class="fa fa-gear"></i> <span class="hide-menu">จัดการข้อมูลบริษัท<span class="fa arrow"></span></a>
                  <ul class="nav nav-second-level collapse ">
                    <li>
                      <a href="<?php echo base_url("companysetting"); ?>" class="waves-effect"><span class="hide-menu">ตั้งค่าทั่วไป</a>
                    </li>
                    <li>
                      <a href="<?php echo base_url("companysetting/working_setting"); ?>" class="waves-effect"><span class="hide-menu">ตั้งค่าวันทำการ</a>
                    </li>
                    <li>
                      <a href="<?php echo base_url("companysetting/holiday_setting"); ?>" class="waves-effect"><span class="hide-menu">ตั้งค่าวันหยุดประจำปี</a>
                    </li>
                    <li>
                      <a href="<?php echo base_url("vacation"); ?>" class="waves-effect"><span class="hide-menu">ตั้งค่าวันลา</a>
                    </li>

                  </ul>
            </li>
            <li class="isAdmin">
                <a href="<?php echo base_url("sites"); ?>" class="waves-effect"><i class="icon-location-pin"></i> <span class="hide-menu">ไซต์และสาขา</a>
            </li>
        </ul>
    </div>
</div>
