<!DOCTYPE html>
<html lang='en'>
  <head>
    <meta charset="utf-8">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" type="image/png" sizes="16x16" href="<?php echo base_url('assets/plugins/material/favicon.png'); ?>">
    <title>#TIKTRACK Best service for time attendance solution.</title>
    <!-- Bootstrap Core CSS -->
    <link href="<?php echo base_url("assets/bootstrap/dist/css/bootstrap.min.css"); ?>" rel="stylesheet">
    <!-- Menu CSS -->
    <link href="<?php echo base_url("assets/plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.css"); ?>" rel="stylesheet">
    <!-- toast CSS -->
    <link href="<?php echo base_url("assets/plugins/bower_components/toast-master/css/jquery.toast.css"); ?>" rel="stylesheet">

    <link href="<?php echo base_url("assets/plugins/bower_components/switchery/dist/switchery.min.css"); ?>" rel="stylesheet">

    <!-- FCM -->
    <link rel="manifest" href="<?php echo base_url("/manifest.json"); ?>">

    <link href="<?php echo base_url("assets/plugins/bower_components/datatables/jquery.dataTables.min.css");?>" rel="stylesheet" type="text/css" />
    <link href="https://cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="<?php echo base_url("assets/css/sweetalert2/dist/sweetalert2.min.js"); ?>">

    <?php if (isset($CSS_asset)){echo $CSS_asset;} ?>
    <link href="<?php echo base_url("assets/css/style.css"); ?>" rel="stylesheet">
    <link href="<?php echo base_url("assets/css/animate.css"); ?>" rel="stylesheet">
    <link href="<?php echo base_url("assets/css/custom.css"); ?>" rel="stylesheet">
    <!-- color CSS -->
    <link href="<?php echo base_url("assets/css/colors/default.css"); ?>" id="theme" rel="stylesheet">
    <link href="<?php echo base_url("assets/plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css");?>"  rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url("assets/plugins/bower_components/custom-select/custom-select.css"); ?>" rel="stylesheet" type="text/css" />


    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!-- <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script> -->

  </head>
  <body class="fix-sidebar">
    <div class="preloader">
        <div class="cssload-speeding-wheel"></div>
    </div>
    <div id="wrapper">
        <?php if ($navbar) echo $navbar; ?>
        <?php if ($sidebar) echo $sidebar; ?>
        <div id="page-wrapper">
            <div class="container-fluid">
                <?php if ($bread) echo $bread; ?>
                <?php if ($middle) echo $middle; ?>
            <!-- /.container-fluid -->
            <?php if ($footer) echo $footer; ?>
          </div>
        <!-- /#page-wrapper -->
      </div>
    </div>
    <div class="modal-area"></div>
    <div class="modal-area2"></div>
  </body>
  <!-- /#wrapper -->
  <!-- jQuery -->
  <script src="<?php echo base_url("assets/plugins/bower_components/jquery/dist/jquery.min.js"); ?>"></script>
  <!-- Bootstrap Core JavaScript -->
  <script src="<?php echo base_url("assets/bootstrap/dist/js/bootstrap.min.js");?>"></script>
  <!-- Menu Plugin JavaScript -->
  <script src="<?php echo base_url("assets/plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.js"); ?>"></script>
  <!--slimscroll JavaScript -->
  <script src="<?php echo base_url("assets/js/jquery.slimscroll.js");?>"></script>
  <!--Wave Effects -->
  <script src="<?php echo base_url("assets/js/waves.js");?>"></script>
  <!--Counter js -->
  <script src="<?php echo base_url("assets/plugins/bower_components/waypoints/lib/jquery.waypoints.js"); ?>"></script>
  <script src="<?php echo base_url("assets/plugins/bower_components/counterup/jquery.counterup.min.js"); ?>"></script>
  <!--Morris JavaScript -->
  <script src="<?php echo base_url("assets/plugins/bower_components/raphael/raphael-min.js"); ?>"></script>
  <!-- Custom Theme JavaScript -->
  <script src="<?php echo base_url("assets/js/custom.min.js"); ?>"></script>
  <script src="<?php echo base_url("assets/js/apps/JHelpers.js"); ?>"></script>
  <?php if (isset($JS_asset)){echo $JS_asset;} ?>
  <!-- Sparkline chart JavaScript -->
  <script src="<?php echo base_url("assets/plugins/bower_components/jquery-sparkline/jquery.sparkline.min.js"); ?>"></script>
  <script src="<?php echo base_url("assets/plugins/bower_components/jquery-sparkline/jquery.charts-sparkline.js"); ?>"></script>
  <script src="<?php echo base_url("assets/plugins/bower_components/toast-master/js/jquery.toast.js"); ?>"></script>
  <script src="<?php echo base_url("assets/plugins/bower_components/styleswitcher/jQuery.style.switcher.js"); ?>"></script>
  <script src="<?php echo base_url("assets/plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js");?>"></script>
  <script src="<?php echo base_url("assets/plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.th.js");?>"></script>
  <script type="text/javascript" src='<?php echo base_url('assets/css/sweetalert2/dist/sweetalert2.all.min.js');?>'></script>
  <script src="<?php echo base_url("assets/plugins/bower_components/datatables/jquery.dataTables.min.js");?>"></script>
    <!-- start - This is for export functionality only -->
  <script src="<?php echo base_url("assets/plugins/bower_components/custom-select/custom-select.min.js" );?>" type="text/javascript"></script>

  <script src="https://cdn.datatables.net/buttons/1.2.2/js/dataTables.buttons.min.js"></script>
  <script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.flash.min.js"></script>
  <script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.html5.min.js"></script>
  <script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.print.min.js"></script>

  <!-- <script src="https://cdn.datatables.net/plug-ins/1.10.16/i18n/Thai.json"></script> -->

  <script src="https://cdn.datatables.net/buttons/1.2.2/js/dataTables.buttons.min.js"></script>
  <script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.flash.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
  <script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js"></script>
  <script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js"></script>
  <script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.html5.min.js"></script>
  <script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.print.min.js"></script>

  <!-- noticication -->
  <script src="https://js.pusher.com/4.3/pusher.min.js"></script>

  <!-- firebase -->
  <script src="https://www.gstatic.com/firebasejs/5.4.1/firebase-app.js"></script>
  <script src="https://www.gstatic.com/firebasejs/5.4.1/firebase-messaging.js"></script>
  <!-- <script src="https://www.gstatic.com/firebasejs/5.4.1/firebase.js"></script> -->

  <script src="<?php echo base_url("assets/plugins/bower_components/switchery/dist/switchery.min.js"); ?>"></script>


  <!-- JWT DECODE -->
  <script src="<?php echo base_url("assets/js/jwt-decode.min.js");?>"></script>
