<div class="white-box">
    <div class="row">
      <div class="col-md-12">
            <div class="sttabs tabs-style-iconbox">
                <nav>
                    <ul>
                        <li><a href="#section-iconbox-1" class="sticon fa fa-file-pdf-o"><span>รายงาน</span></a></li>
                        <li><a href="#section-iconbox-2" class="sticon fa fa-save"><span>ส่งออกข้อมูล</span></a></li>
                    </ul>
                </nav>
                <div class="content-wrap">
                    <section id="section-iconbox-1">
                      <div class="table-responsive">
                        <table class='table table-bordered   table-striped report_list_table'>
                          <thead>
                            <th  style="width:50px;" class='text-center' >ลำดับ</th>
                            <th style="width:600px;" >ชื่อรายงาน</th>
                            <th style="width:200px;" class='text-center' >ตัวเลือก</th>
                          </thead>
                          <tbody id='report_list_body'>

                          </tbody>
                        </table>
                      </div>
                    </section>
                    <section id="section-iconbox-2">
                      <!-- <div class="table-responsive">
                        <table class='table table-bordered   table-striped export_table'>
                          <thead>
                            <th  style="width:50px;" class='text-center' >ลำดับ</th>
                            <th style="width:600px;" >ชื่อรายงาน</th>
                            <th style="width:200px;" class='text-center' >ตัวเลือก</th>
                          </thead>
                          <tbody id='export_body'>

                          </tbody>
                        </table>
                      </div> -->
                </div>
                <!-- /content -->
            </div>
            <!-- /tabs -->
      </div>
    </div>
</div>
