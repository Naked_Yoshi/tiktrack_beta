<!-- <div class="white-box">
  <section id="wrapper" class="">
    <div class="login-box">
    <div class="row" style="background-color: #b71c1c;">
      <h1 class="text-center" style="font-size:30px;color:#ffffff;">Change Password</h1>
    </div>
    <br>
    <div class="row">
      <div class="input-group"> <span class="input-group-addon"><span class="fa fa-key"></span></span>
          <input type="password" placeholder="Password เดิมจากระบบ" id="generate_password" class="form-control">
      </div>
      <br>
      <div class="input-group"> <span class="input-group-addon"><span class="fa fa-key"></span></span>
          <input type="password" placeholder="Password ใหม่" id="new_password" class="form-control">
      </div>
      <br>
      <div class="input-group"> <span class="input-group-addon"><span class="fa fa-key"></span></span>
          <input type="password" placeholder="ยืนยัน Password" id="renew_password" class="form-control">
      </div>
      <span id="labelchk"></span>
    </div>
    <br>
    <div class="row" style="background-color: #b71c1c; padding:5px; ">
        <div class="col-sm-12" style="text-align:center;">
            <button class="btn btn-outline btn-rounded btn-default btn-change-pass" style="width:150px;">Save</button>
        </div>
    </div>
    <br>
    </div>
  </section>
</div> -->
<div id="changePass" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                            <h4 class="modal-title" id="myModalLabel">Change Password</h4>
                                        </div>
                                        <div class="modal-body" style="padding-left: 50px;padding-right :50px;">
                                            <div class="row">
                                              <div class="input-group"> <span class="input-group-addon"><span class="fa fa-key"></span></span>
                                                  <input type="password" placeholder="Password เดิมจากระบบ" id="generate_password" class="form-control">
                                              </div>
                                            </div>
                                            <br>
                                            <div class="row">
                                              <div class="input-group"> <span class="input-group-addon"><span class="fa fa-key"></span></span>
                                                  <input type="password" placeholder="Password ใหม่" id="new_password" class="form-control">
                                              </div>
                                            </div>
                                            <br>
                                            <div class="row">
                                              <div class="input-group"> <span class="input-group-addon"><span class="fa fa-key"></span></span>
                                                  <input type="password" placeholder="ยืนยัน Password" id="renew_password" class="form-control">
                                              </div>
                                            </div>
                                            <span id="labelchk"></span>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-info waves-effect btn-save-change-pass" data-dismiss="modal">Save</button>
                                        </div>
                                    </div>
                                    <!-- /.modal-content -->
                                </div>
                                <!-- /.modal-dialog -->
                            </div>
