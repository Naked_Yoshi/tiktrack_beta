<div class="white-box">
  <div class="row">
    <div class="col-md-12">
      <a class='btn btn-success pull-right m-l-20 btn-rounded btn-outline hidden-xs hidden-sm waves-effect waves-light btn-create-holiday'><span class="fa fa-plus"></span> เพิ่มกลุ่มวันหยุดประจำปี</a>
      <h4>ข้อมูลวันหยุดประจำปี</h4>
      <hr>
    </div>
  </div>
  <div class="row">
      <div class="col-md-12">
          <!-- <div class="table-responsive"> -->
            <table class='table table-bordered  table-striped holiday_table'>
              <thead>
                <th  width='50' class='text-center' >ลำดับ</th>
                <th>ชื่อกลุ่มวันหยุดประจำปี</th>
                <th width='150'>ตัวเลือก</th>
              </thead>
              <tbody id='holiday_body'>

              </tbody>
            </table>
          <!-- </div> -->
      </div>
  </div>
</div>
