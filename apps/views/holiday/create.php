<div id="holidayCreate" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="holidayCreateLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="clearModals();">×</button>
          <h4 class="modal-title" id="holidayCreateLabel"> เพิ่มหยุดประจำปี</h4>
      </div>
      <div class="modal-body">
        <form id='form-holiday'>
           <div class='row'>
              <div class="col-md-12">
                  <div class="form-group has-feedback" id="holyday_name_group">
                      <label for="holyday_name">ชื่อกลุ่มวันหยุด</label>
                      <input type="text" class='form-control' id="holyday_name" name="holyday_name" value="" placeholder="กรอกชื่อกลุ่มวันหยุด">
                      <span class="form-control-feedback" id="holyday_name_feedback"></span>
                  </div>
                  <hr>
              </div>
              <div class="col-md-12">
                  <a class='btn btn-success pull-right m-l-20  btn-outline waves-effect waves-light btn-create-hd-row'><span class='fa fa-plus'></span></a>
                  <h5>
                    <span class='icon-calender'></span> รายการวันหยุด
                  </h5>
                  <hr>
              </div>
           </div>
           <div class="row">
              <div class="col-md-12">
                  <table class='table'>
                      <thead>
                        <th>วันหยุด</th>
                        <th colspan="2">คำอธิบาย</th>
                      </thead>
                      <tbody id='HolidayList_body'>
                        <tr  class='text-center no-list' data-row='0' >
                          <td colspan="3">
                            <h5>ไม่พบรายการวันหยุด</h5>
                          </td>
                        </tr>
                      </tbody>
                  </table>
              </div>
           </div>
        </form>
      </div>
      <div class="modal-footer">
          <button type="button" class='btn btn-success btn-outline btn-save-hd'>บันทึกข้อมูล</button>
          <button type="button" class='btn btn-default btn-outline' data-dismiss='modal'>ปิดหน้าต่าง</button>
      </div>
    </div>
  </div>
</div>
