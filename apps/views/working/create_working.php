<div id="create_wk_Modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title" id="myModalLabel">เพิ่มวันทำการ</h4>
            </div>
            <form align='left' id='create_form_wk'>
            <div class="modal-body">
                <div class="row" style="padding:10px">
                  <div class="form-group has-feedback" id="wk_name_group">
                      <label for="wk_name">ชื่อวันทำการ</label>
                      <input type="text" class="form-control" name="wk_name" id="wk_name" placeholder="ชื่อวันทำการ">
                      <span class="form-control-feedback" id="wk_name_feedback"></span>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-3" style="padding-top:10px;padding-left:15px;">
                      <label for="working_group">วันทำการ</label>
                  </div>
                  <div class="col-md-2">
                        <div class="checkbox checkbox-primary checkbox-circle">
                          <input type="checkbox" id="wk_chk_every" name="wk_chk_every" value="" align="">
                          <label for="wk_monday">Every Day</label>
                        </div>
                  </div>
                  <div class="col-md-1">
                        <div class="checkbox checkbox-primary checkbox-circle">
                          <input type="checkbox" class="chk-day" id="wk_chk_monday" name="wk_chk_monday" value="monday" align="">
                          <label for="wk_monday">(Mon)</label>
                        </div>
                  </div>
                  <div class="col-md-1">
                        <div class="checkbox checkbox-primary checkbox-circle">
                          <input type="checkbox" class="chk-day" id="wk_chk_tuesday" name="wk_chk_tuesday" value="tuesday" align="">
                          <label for="wk_monday">(Tue)</label>
                        </div>
                  </div>
                  <div class="col-md-1">
                        <div class="checkbox checkbox-primary checkbox-circle" style="text-align:center;display:block;">
                          <input type="checkbox" class="chk-day" id="wk_chk_wednesday" name="wk_chk_wednesday" value="wednesday" align="">
                          <label for="wk_monday">(Wed)</label>
                        </div>
                  </div>
                  <div class="col-md-1">
                        <div class="checkbox checkbox-primary checkbox-circle" style="text-align:center;display:block;">
                          <input type="checkbox" class="chk-day" id="wk_chk_thuesday" name="wk_chk_thuesday" value="thuesday" align="">
                          <label for="wk_monday">(Thu)</label>
                        </div>
                  </div>
                  <div class="col-md-1">
                        <div class="checkbox checkbox-primary checkbox-circle" style="text-align:center;display:block;">
                          <input type="checkbox" class="chk-day" id="wk_chk_friday" name="wk_chk_friday" value="friday" align="">
                          <label for="wk_monday">(Fri)</label>
                        </div>
                  </div>
                  <div class="col-md-1">
                        <div class="checkbox checkbox-primary checkbox-circle" style="text-align:center;display:block;">
                          <input type="checkbox" class="chk-day" id="wk_chk_saturday" name="wk_chk_saturday" value="saturday" align="">
                          <label for="wk_monday">(Sat)</label>
                        </div>
                  </div>
                  <div class="col-md-1">
                        <div class="checkbox checkbox-primary checkbox-circle" style="text-align:center;display:block;">
                          <input type="checkbox" class="chk-day" id="wk_chk_sunday" name="wk_chk_sunday" value="sunday" align="">
                          <label for="wk_monday">(Sun)</label>
                        </div>
                  </div>
                </div>
                <hr>
                <div class="row">
                  <div class="col-md-12" style="padding-left:15px;">
                  <div class="checkbox checkbox-success">
                    <input type="checkbox" id="chk_time_used_everyday" name="chk_time_used_everyday" value="" checked="checked">
                    <label for="chk_time_used_everyday">ใช้เวลาเดี่ยวกันทั้งหมด</label>
                  </div>
                </div>
                </div>
                <hr>
                <div class="row chk-time-eve">
                  <div class="col-md-12" style="padding-left:15px;">
                    <div class="col-md-3">
                      <label for="work_start_group">เวลาเข้างาน</label>
                      <div class="form-group has-feedback" id="work_start_group">
                          <div class="input-group">
                            <input type="text" class="form-control clockpicker" name="work_start" id="work_start" placeholder="เวลาเข้างาน">
                            <span class="form-control-feedback" id="work_start_feedback"></span>
                            <span class="input-group-btn">
                              <button class="btn btn-default btn-outline btn-cancel-time" data-id="work_start" type="button"><span class='fa fa-remove'></span></button>
                            </span>
                          </div>
                      </div>
                    </div>

                    <div class="col-md-3">
                      <label for="break_start_group">เวลาเริ่มพัก</label>
                      <div class="form-group has-feedback" id="break_start_group">
                          <div class="input-group">
                            <input type="text" class="form-control clockpicker" name="break_start" id="break_start" placeholder="เวลาเริ่มพัก">
                            <span class="form-control-feedback" id="break_start_feedback"></span>
                            <span class="input-group-btn">
                              <button class="btn btn-default btn-outline btn-cancel-time" data-id="break_start" type="button"><span class='fa fa-remove'></span></button>
                            </span>
                          </div>
                      </div>
                    </div>

                    <div class="col-md-3">
                      <label for="break_end_group">เวลาเลิกพัก</label>
                      <div class="form-group has-feedback" id="break_start_group">
                          <div class="input-group">
                            <input type="text" class="form-control clockpicker" name="break_end" id="break_end" placeholder="เวลาเลิกพัก">
                            <span class="form-control-feedback" id="break_start_feedback"></span>
                            <span class="input-group-btn">
                              <button class="btn btn-default btn-outline btn-cancel-time" data-id="break_end" type="button"><span class='fa fa-remove'></span></button>
                            </span>
                          </div>
                      </div>
                    </div>

                    <div class="col-md-3">
                      <label for="work_end_group">เวลาเลิกงาน</label>
                      <div class="form-group has-feedback" id="work_end_group">
                          <div class="input-group">
                            <input type="text" class="form-control clockpicker" name="work_end" id="work_end" placeholder="เวลาเลิกงาน">
                            <span class="form-control-feedback" id="work_end_feedback"></span>
                            <span class="input-group-btn">
                              <button class="btn btn-default btn-outline btn-cancel-time" data-id="work_end" type="button"><span class='fa fa-remove'></span></button>
                            </span>
                          </div>
                      </div>
                    </div>


                  </div>
                </div>
                <div class="row chk-time-custom" style="display:none;">
                  <div class="col-md-12 wk-all chk-wk-monday" style="padding-left:15px;display:none;">

                    <div class="col-md-12">
                        <label for="monday_group">วันจันทร์</label>
                    </div>

                    <div class="col-md-3">
                      <!-- <label for="work_start_group">เวลาเข้างาน</label> -->
                      <div class="form-group has-feedback" id="monday_work_start_group">
                          <div class="input-group">
                            <input type="text" class="form-control clockpicker" name="monday_work_start" id="monday_work_start" placeholder="เวลาเข้างาน">
                            <span class="form-control-feedback" id="monday_work_start_feedback"></span>
                            <span class="input-group-btn">
                              <button class="btn btn-default btn-outline btn-cancel-time" data-id="monday_work_start" type="button"><span class='fa fa-remove'></span></button>
                            </span>
                          </div>
                      </div>
                    </div>

                    <div class="col-md-3">
                      <!-- <label for="break_start_group">เวลาเริ่มพัก</label> -->
                      <div class="form-group has-feedback" id="monday_break_start_group">
                          <div class="input-group">
                            <input type="text" class="form-control clockpicker" name="monday_break_start" id="monday_break_start" placeholder="เวลาเริ่มพัก">
                            <span class="form-control-feedback" id="monday_break_start_feedback"></span>
                            <span class="input-group-btn">
                              <button class="btn btn-default btn-outline btn-cancel-time" data-id="monday_break_start" type="button"><span class='fa fa-remove'></span></button>
                            </span>
                          </div>
                      </div>
                    </div>

                    <div class="col-md-3">
                      <!-- <label for="break_end_group">เวลาเลิกพัก</label> -->
                      <div class="form-group has-feedback" id="monday_break_start_group">
                          <div class="input-group">
                            <input type="text" class="form-control clockpicker" name="monday_break_end" id="monday_break_end" placeholder="เวลาเลิกพัก">
                            <span class="form-control-feedback" id="monday_break_start_feedback"></span>
                            <span class="input-group-btn">
                              <button class="btn btn-default btn-outline btn-cancel-time" data-id="monday_break_end" type="button"><span class='fa fa-remove'></span></button>
                            </span>
                          </div>
                      </div>
                    </div>

                    <div class="col-md-3">
                      <!-- <label for="work_end_group">เวลาเลิกงาน</label> -->
                      <div class="form-group has-feedback" id="monday_work_end_group">
                          <div class="input-group">
                            <input type="text" class="form-control clockpicker" name="monday_work_end" id="monday_work_end" placeholder="เวลาเลิกงาน">
                            <span class="form-control-feedback" id="monday_work_end_feedback"></span>
                            <span class="input-group-btn">
                              <button class="btn btn-default btn-outline btn-cancel-time" data-id="monday_work_end" type="button"><span class='fa fa-remove'></span></button>
                            </span>
                          </div>
                      </div>
                    </div>

                  </div>

                  <!-- ///////////วันอังคาร -->
                  <div class="col-md-12 wk-all chk-wk-tuesday" style="padding-left:15px;display:none;">

                    <div class="col-md-12">
                        <label for="tuesday_group">วันอังคาร</label>
                    </div>

                    <div class="col-md-3">
                      <!-- <label for="work_start_group">เวลาเข้างาน</label> -->
                      <div class="form-group has-feedback" id="tuesday_work_start_group">
                          <div class="input-group">
                            <input type="text" class="form-control clockpicker" name="tuesday_work_start" id="tuesday_work_start" placeholder="เวลาเข้างาน">
                            <span class="form-control-feedback" id="tuesday_work_start_feedback"></span>
                            <span class="input-group-btn">
                              <button class="btn btn-default btn-outline btn-cancel-time" data-id="tuesday_work_start" type="button"><span class='fa fa-remove'></span></button>
                            </span>
                          </div>
                      </div>
                    </div>

                    <div class="col-md-3">
                      <!-- <label for="break_start_group">เวลาเริ่มพัก</label> -->
                      <div class="form-group has-feedback" id="tuesday_break_start_group">
                          <div class="input-group">
                            <input type="text" class="form-control clockpicker" name="tuesday_break_start" id="tuesday_break_start" placeholder="เวลาเริ่มพัก">
                            <span class="form-control-feedback" id="tuesday_break_start_feedback"></span>
                            <span class="input-group-btn">
                              <button class="btn btn-default btn-outline btn-cancel-time" data-id="tuesday_break_start" type="button"><span class='fa fa-remove'></span></button>
                            </span>
                          </div>
                      </div>
                    </div>

                    <div class="col-md-3">
                      <!-- <label for="break_end_group">เวลาเลิกพัก</label> -->
                      <div class="form-group has-feedback" id="tuesday_break_start_group">
                          <div class="input-group">
                            <input type="text" class="form-control clockpicker" name="tuesday_break_end" id="tuesday_break_end" placeholder="เวลาเลิกพัก">
                            <span class="form-control-feedback" id="tuesday_break_start_feedback"></span>
                            <span class="input-group-btn">
                              <button class="btn btn-default btn-outline btn-cancel-time" data-id="tuesday_break_end" type="button"><span class='fa fa-remove'></span></button>
                            </span>
                          </div>
                      </div>
                    </div>

                    <div class="col-md-3">
                      <!-- <label for="work_end_group">เวลาเลิกงาน</label> -->
                      <div class="form-group has-feedback" id="tuesday_work_end_group">
                          <div class="input-group">
                            <input type="text" class="form-control clockpicker" name="tuesday_work_end" id="tuesday_work_end" placeholder="เวลาเลิกงาน">
                            <span class="form-control-feedback" id="tuesday_work_end_feedback"></span>
                            <span class="input-group-btn">
                              <button class="btn btn-default btn-outline btn-cancel-time" data-id="tuesday_work_end" type="button"><span class='fa fa-remove'></span></button>
                            </span>
                          </div>
                      </div>
                    </div>


                  </div>
                  <!-- //////ปิดเงื่อนไข -->

                  <!-- ///////////วันพุธ -->
                  <div class="col-md-12 wk-all chk-wk-wednesday" style="padding-left:15px;display:none;">

                    <div class="col-md-12">
                        <label for="wednesday_group">วันพุธ</label>
                    </div>

                    <div class="col-md-3">
                      <!-- <label for="work_start_group">เวลาเข้างาน</label> -->
                      <div class="form-group has-feedback" id="wednesday_work_start_group">
                          <div class="input-group">
                            <input type="text" class="form-control clockpicker" name="wednesday_work_start" id="wednesday_work_start" placeholder="เวลาเข้างาน">
                            <span class="form-control-feedback" id="wednesday_work_start_feedback"></span>
                            <span class="input-group-btn">
                              <button class="btn btn-default btn-outline btn-cancel-time" data-id="wednesday_work_start" type="button"><span class='fa fa-remove'></span></button>
                            </span>
                          </div>
                      </div>
                    </div>

                    <div class="col-md-3">
                      <!-- <label for="break_start_group">เวลาเริ่มพัก</label> -->
                      <div class="form-group has-feedback" id="wednesday_break_start_group">
                          <div class="input-group">
                            <input type="text" class="form-control clockpicker" name="wednesday_break_start" id="wednesday_break_start" placeholder="เวลาเริ่มพัก">
                            <span class="form-control-feedback" id="wednesday_break_start_feedback"></span>
                            <span class="input-group-btn">
                              <button class="btn btn-default btn-outline btn-cancel-time" data-id="wednesday_break_start" type="button"><span class='fa fa-remove'></span></button>
                            </span>
                          </div>
                      </div>
                    </div>

                    <div class="col-md-3">
                      <!-- <label for="break_end_group">เวลาเลิกพัก</label> -->
                      <div class="form-group has-feedback" id="wednesday_break_start_group">
                          <div class="input-group">
                            <input type="text" class="form-control clockpicker" name="wednesday_break_end" id="wednesday_break_end" placeholder="เวลาเลิกพัก">
                            <span class="form-control-feedback" id="wednesday_break_start_feedback"></span>
                            <span class="input-group-btn">
                              <button class="btn btn-default btn-outline btn-cancel-time" data-id="wednesday_break_end" type="button"><span class='fa fa-remove'></span></button>
                            </span>
                          </div>
                      </div>
                    </div>

                    <div class="col-md-3">
                      <!-- <label for="work_end_group">เวลาเลิกงาน</label> -->
                      <div class="form-group has-feedback" id="wednesday_work_end_group">
                          <div class="input-group">
                            <input type="text" class="form-control clockpicker" name="wednesday_work_end" id="wednesday_work_end" placeholder="เวลาเลิกงาน">
                            <span class="form-control-feedback" id="wednesday_work_end_feedback"></span>
                            <span class="input-group-btn">
                              <button class="btn btn-default btn-outline btn-cancel-time" data-id="wednesday_work_end" type="button"><span class='fa fa-remove'></span></button>
                            </span>
                          </div>
                      </div>
                    </div>


                  </div>
                  <!-- //////ปิดเงื่อนไข -->

                  <!-- ///////////วันพฤหัสบดี -->
                  <div class="col-md-12 wk-all chk-wk-thuesday" style="padding-left:15px;display:none;">

                    <div class="col-md-12">
                        <label for="thuesday_group">วันพฤหัสบดี</label>
                    </div>

                    <div class="col-md-3">
                      <!-- <label for="work_start_group">เวลาเข้างาน</label> -->
                      <div class="form-group has-feedback" id="thuesday_work_start_group">
                          <div class="input-group">
                            <input type="text" class="form-control clockpicker" name="thuesday_work_start" id="thuesday_work_start" placeholder="เวลาเข้างาน">
                            <span class="form-control-feedback" id="thuesday_work_start_feedback"></span>
                            <span class="input-group-btn">
                              <button class="btn btn-default btn-outline btn-cancel-time" data-id="thuesday_work_start" type="button"><span class='fa fa-remove'></span></button>
                            </span>
                          </div>
                      </div>
                    </div>

                    <div class="col-md-3">
                      <!-- <label for="break_start_group">เวลาเริ่มพัก</label> -->
                      <div class="form-group has-feedback" id="thuesday_break_start_group">
                          <div class="input-group">
                            <input type="text" class="form-control clockpicker" name="thuesday_break_start" id="thuesday_break_start" placeholder="เวลาเริ่มพัก">
                            <span class="form-control-feedback" id="thuesday_break_start_feedback"></span>
                            <span class="input-group-btn">
                              <button class="btn btn-default btn-outline btn-cancel-time" data-id="thuesday_break_start" type="button"><span class='fa fa-remove'></span></button>
                            </span>
                          </div>
                      </div>
                    </div>

                    <div class="col-md-3">
                      <!-- <label for="break_end_group">เวลาเลิกพัก</label> -->
                      <div class="form-group has-feedback" id="thuesday_break_start_group">
                          <div class="input-group">
                            <input type="text" class="form-control clockpicker" name="thuesday_break_end" id="thuesday_break_end" placeholder="เวลาเลิกพัก">
                            <span class="form-control-feedback" id="thuesday_break_start_feedback"></span>
                            <span class="input-group-btn">
                              <button class="btn btn-default btn-outline btn-cancel-time" data-id="thuesday_break_end" type="button"><span class='fa fa-remove'></span></button>
                            </span>
                          </div>
                      </div>
                    </div>

                    <div class="col-md-3">
                      <!-- <label for="work_end_group">เวลาเลิกงาน</label> -->
                      <div class="form-group has-feedback" id="wednesday_work_end_group">
                          <div class="input-group">
                            <input type="text" class="form-control clockpicker" name="thuesday_work_end" id="thuesday_work_end" placeholder="เวลาเลิกงาน">
                            <span class="form-control-feedback" id="thuesday_work_end_feedback"></span>
                            <span class="input-group-btn">
                              <button class="btn btn-default btn-outline btn-cancel-time" data-id="thuesday_work_end" type="button"><span class='fa fa-remove'></span></button>
                            </span>
                          </div>
                      </div>
                    </div>


                  </div>
                  <!-- //////ปิดเงื่อนไข -->

                  <!-- ///////////วันศุกร์ -->
                  <div class="col-md-12 wk-all chk-wk-friday" style="padding-left:15px;display:none;">

                    <div class="col-md-12">
                        <label for="thuesday_group">วันศุกร์</label>
                    </div>

                    <div class="col-md-3">
                      <!-- <label for="work_start_group">เวลาเข้างาน</label> -->
                      <div class="form-group has-feedback" id="friday_work_start_group">
                          <div class="input-group">
                            <input type="text" class="form-control clockpicker" name="friday_work_start" id="friday_work_start" placeholder="เวลาเข้างาน">
                            <span class="form-control-feedback" id="friday_work_start_feedback"></span>
                            <span class="input-group-btn">
                              <button class="btn btn-default btn-outline btn-cancel-time" data-id="friday_work_start" type="button"><span class='fa fa-remove'></span></button>
                            </span>
                          </div>
                      </div>
                    </div>

                    <div class="col-md-3">
                      <!-- <label for="break_start_group">เวลาเริ่มพัก</label> -->
                      <div class="form-group has-feedback" id="friday_break_start_group">
                          <div class="input-group">
                            <input type="text" class="form-control clockpicker" name="friday_break_start" id="friday_break_start" placeholder="เวลาเริ่มพัก">
                            <span class="form-control-feedback" id="friday_break_start_feedback"></span>
                            <span class="input-group-btn">
                              <button class="btn btn-default btn-outline btn-cancel-time" data-id="friday_break_start" type="button"><span class='fa fa-remove'></span></button>
                            </span>
                          </div>
                      </div>
                    </div>

                    <div class="col-md-3">
                      <!-- <label for="break_end_group">เวลาเลิกพัก</label> -->
                      <div class="form-group has-feedback" id="friday_break_start_group">
                          <div class="input-group">
                            <input type="text" class="form-control clockpicker" name="friday_break_end" id="friday_break_end" placeholder="เวลาเลิกพัก">
                            <span class="form-control-feedback" id="friday_break_start_feedback"></span>
                            <span class="input-group-btn">
                              <button class="btn btn-default btn-outline btn-cancel-time" data-id="friday_break_end" type="button"><span class='fa fa-remove'></span></button>
                            </span>
                          </div>
                      </div>
                    </div>

                    <div class="col-md-3">
                      <!-- <label for="work_end_group">เวลาเลิกงาน</label> -->
                      <div class="form-group has-feedback" id="friday_work_end_group">
                          <div class="input-group">
                            <input type="text" class="form-control clockpicker" name="friday_work_end" id="friday_work_end" placeholder="เวลาเลิกงาน">
                            <span class="form-control-feedback" id="friday_work_end_feedback"></span>
                            <span class="input-group-btn">
                              <button class="btn btn-default btn-outline btn-cancel-time" data-id="friday_work_end" type="button"><span class='fa fa-remove'></span></button>
                            </span>
                          </div>
                      </div>
                    </div>


                  </div>
                  <!-- //////ปิดเงื่อนไข -->

                  <!-- ///////////วันเสาร์ -->
                  <div class="col-md-12 wk-all chk-wk-saturday" style="padding-left:15px;display:none;">

                    <div class="col-md-12">
                        <label for="saturday_group">วันเสาร์</label>
                    </div>

                    <div class="col-md-3">
                      <!-- <label for="work_start_group">เวลาเข้างาน</label> -->
                      <div class="form-group has-feedback" id="saturday_work_start_group">
                          <div class="input-group">
                            <input type="text" class="form-control clockpicker" name="saturday_work_start" id="saturday_work_start" placeholder="เวลาเข้างาน">
                            <span class="form-control-feedback" id="saturday_work_start_feedback"></span>
                            <span class="input-group-btn">
                              <button class="btn btn-default btn-outline btn-cancel-time" data-id="saturday_work_start" type="button"><span class='fa fa-remove'></span></button>
                            </span>
                          </div>
                      </div>
                    </div>

                    <div class="col-md-3">
                      <!-- <label for="break_start_group">เวลาเริ่มพัก</label> -->
                      <div class="form-group has-feedback" id="saturday_break_start_group">
                          <div class="input-group">
                            <input type="text" class="form-control clockpicker" name="saturday_break_start" id="saturday_break_start" placeholder="เวลาเริ่มพัก">
                            <span class="form-control-feedback" id="saturday_break_start_feedback"></span>
                            <span class="input-group-btn">
                              <button class="btn btn-default btn-outline btn-cancel-time" data-id="saturday_break_start" type="button"><span class='fa fa-remove'></span></button>
                            </span>
                          </div>
                      </div>
                    </div>

                    <div class="col-md-3">
                      <!-- <label for="break_end_group">เวลาเลิกพัก</label> -->
                      <div class="form-group has-feedback" id="saturday_break_start_group">
                          <div class="input-group">
                            <input type="text" class="form-control clockpicker" name="saturday_break_end" id="saturday_break_end" placeholder="เวลาเลิกพัก">
                            <span class="form-control-feedback" id="saturday_break_start_feedback"></span>
                            <span class="input-group-btn">
                              <button class="btn btn-default btn-outline btn-cancel-time" data-id="saturday_break_end" type="button"><span class='fa fa-remove'></span></button>
                            </span>
                          </div>
                      </div>
                    </div>

                    <div class="col-md-3">
                      <!-- <label for="work_end_group">เวลาเลิกงาน</label> -->
                      <div class="form-group has-feedback" id="saturday_work_end_group">
                          <div class="input-group">
                            <input type="text" class="form-control clockpicker" name="saturday_work_end" id="saturday_work_end" placeholder="เวลาเลิกงาน">
                            <span class="form-control-feedback" id="saturday_work_end_feedback"></span>
                            <span class="input-group-btn">
                              <button class="btn btn-default btn-outline btn-cancel-time" data-id="saturday_work_end" type="button"><span class='fa fa-remove'></span></button>
                            </span>
                          </div>
                      </div>
                    </div>


                  </div>
                  <!-- //////ปิดเงื่อนไข -->

                  <!-- ///////////วันอาทิตย์ -->
                  <div class="col-md-12 wk-all chk-wk-sunday" style="padding-left:15px;display:none;">

                    <div class="col-md-12">
                        <label for="saturday_group">วันอาทิตย์</label>
                    </div>

                    <div class="col-md-3">
                      <!-- <label for="work_start_group">เวลาเข้างาน</label> -->
                      <div class="form-group has-feedback" id="sunday_work_start_group">
                          <div class="input-group">
                            <input type="text" class="form-control clockpicker" name="sunday_work_start" id="sunday_work_start" placeholder="เวลาเข้างาน">
                            <span class="form-control-feedback" id="sunday_work_start_feedback"></span>
                            <span class="input-group-btn">
                              <button class="btn btn-default btn-outline btn-cancel-time" data-id="sunday_work_start" type="button"><span class='fa fa-remove'></span></button>
                            </span>
                          </div>
                      </div>
                    </div>

                    <div class="col-md-3">
                      <!-- <label for="break_start_group">เวลาเริ่มพัก</label> -->
                      <div class="form-group has-feedback" id="sunday_break_start_group">
                          <div class="input-group">
                            <input type="text" class="form-control clockpicker" name="sunday_break_start" id="sunday_break_start" placeholder="เวลาเริ่มพัก">
                            <span class="form-control-feedback" id="sunday_break_start_feedback"></span>
                            <span class="input-group-btn">
                              <button class="btn btn-default btn-outline btn-cancel-time" data-id="sunday_break_start" type="button"><span class='fa fa-remove'></span></button>
                            </span>
                          </div>
                      </div>
                    </div>

                    <div class="col-md-3">
                      <!-- <label for="break_end_group">เวลาเลิกพัก</label> -->
                      <div class="form-group has-feedback" id="sunday_break_start_group">
                          <div class="input-group">
                            <input type="text" class="form-control clockpicker" name="sunday_break_end" id="sunday_break_end" placeholder="เวลาเลิกพัก">
                            <span class="form-control-feedback" id="sunday_break_start_feedback"></span>
                            <span class="input-group-btn">
                              <button class="btn btn-default btn-outline btn-cancel-time" data-id="sunday_break_end" type="button"><span class='fa fa-remove'></span></button>
                            </span>
                          </div>
                      </div>
                    </div>

                    <div class="col-md-3">
                      <!-- <label for="work_end_group">เวลาเลิกงาน</label> -->
                      <div class="form-group has-feedback" id="sunday_work_end_group">
                          <div class="input-group">
                            <input type="text" class="form-control clockpicker" name="sunday_work_end" id="sunday_work_end" placeholder="เวลาเลิกงาน">
                            <span class="form-control-feedback" id="sunday_work_end_feedback"></span>
                            <span class="input-group-btn">
                              <button class="btn btn-default btn-outline btn-cancel-time" data-id="sunday_work_end" type="button"><span class='fa fa-remove'></span></button>
                            </span>
                          </div>
                      </div>
                    </div>
                  </div>
                  <!-- //////ปิดเงื่อนไข -->
                </div>
            </div>
            <div class="modal-footer">
              <div class="col-md-12 text-left">
                <button type="button" class="fcbtn btn btn-outline btn-success btn-1d btn-create-wk">สร้างวันทำการ</button>
                <button type="reset" class="fcbtn btn btn-outline btn-default btn-1d">เคลียร์</button>
              </div>
            </div>
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
