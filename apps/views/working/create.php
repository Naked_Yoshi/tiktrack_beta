<div id="workingCreate" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="siteCreateLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="clearModals();">×</button>
          <h4 class="modal-title" id="siteCreateLabel"> เพิ่มวันทำการ</h4>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-md-12">

                    <div class="row">
                      <div class="col-md-12 text-left">
                        <h4><span class='fa fa-list-alt'></span> วันทำการประจำไซต์</h4>
                        <table class='table'>
                          <thead>
                              <th class='text-left' ><b>รายการวันทำการประจำไซต์</b></th>
                              <th class='text-right' colspan="2">
                                <!-- <a data-toggle="tab" href="#createnew"  class='btn btn-outline btn-success wk-new-create' aria-expanded="false"><span class='fa fa-file'></span> สร้างใหม่ </a> -->
                                <!-- <a data-toggle="tab" href="#list_wk"  class='btn btn-outline btn-info wk-search' aria-expanded="false"><span class='fa fa-search'></span> ค้นหา </a> -->
                              </th>
                          </thead>
                          <tbody id='wk_select_table'>
                            <tr class='noSelect'>
                              <td colspan="3" class='text-center '><b> --- เลือกวันทำการ --- </b></td>
                            </tr>
                          </tbody>
                        </table>
                      </div>
                    </div>

                    <div class="row">
                      <div class='col-md-12' id="list_wk">
                        <div class="row">
                          <div class="col-md-12 text-left">
                            <h4><span class='fa fa-calendar'></span> รายการวันทำการ
                                <!-- <a class="close close-wk-list"><span class="fa fa-remove"></span></a> -->
                            </h4>
                            <hr>
                          </div>
                        </div>
                        <div class="row">
                          <div class="col-md-12">
                            <div class="vtabs">
                                    <ul class="nav tabs-vertical tabs-working-list"></ul>
                                    <div class="tab-content tabs-working-content"></div>
                                </div>
                          </div>
                        </div>
                      </div>
                      <div class="col-md-12" id="create_new_wk">
                        <!-- create form -->
                        <form align='left' id='create_form_wk'>
                          <input type="hidden" id="company_id" name="company_id" value="1">
                          <input type="hidden" id="createBy" name="createBy" value="22">
                          <div class="row">
                            <div class="col-md-12">
                              <h4>
                                สร้างใหม่
                                <a class="close close-new"><span class="fa fa-remove"></span></a>
                              </h4>
                              <hr>
                            </div>
                          </div>
                          <div class="row">
                            <div class="col-md-12">
                              <div class="form-group has-feedback" id="wk_name_group">
                                  <label for="wk_name">ชื่อวันทำการ</label>
                                  <input type="text" class="form-control" name="wk_name" id="wk_name" placeholder="เวลาเข้างาน">
                                  <span class="form-control-feedback" id="wk_name_feedback"></span>
                              </div>
                            </div>
                          </div>

                          <div class="row">
                            <div class="col-md-12">
                                <label for="monday_group">วันจันทร์</label>
                            </div>
                            <div class="col-md-6">
                              <div class="form-group has-feedback" id="monday_start_group">
                                  <div class="input-group">
                                    <input type="text" class="form-control clockpicker" name="monday_start" id="monday_start" placeholder="เวลาเข้างาน">
                                    <span class="form-control-feedback" id="monday_start_feedback"></span>
                                    <span class="input-group-btn">
                                      <button class="btn btn-default btn-outline btn-cancel-time" data-id="monday_start" type="button"><span class='fa fa-remove'></span></button>
                                    </span>
                                  </div>
                              </div>

                            </div>
                            <div class="col-md-6">
                              <div class="form-group has-feedback clockpicker" id="monday_end_group">
                                  <div class="input-group">
                                    <input type="text" class="form-control" name="monday_end" id="monday_end" placeholder="เวลาออกงาน">
                                    <span class="form-control-feedback" id="monday_end_feedback"></span>
                                    <span class="input-group-btn">
                                      <button class="btn btn-default btn-outline btn-cancel-time" data-id="monday_end" type="button"><span class='fa fa-remove'></span></button>
                                    </span>
                                  </div>
                              </div>
                            </div>
                            <div class="col-md-12">
                                <a class='apply-all'><span class='fa fa-check-circle'></span> นำไปใช้ทั้งหมด</a>
                            </div>
                          </div>
                          <br>
                          <div class="row">
                            <div class="col-md-12">
                              <label for="tuesday_group">วันอังคาร</label>
                            </div>
                            <div class="col-md-6">
                              <div class="form-group has-feedback clockpicker" id="tuesday_start_group">
                                <div class="input-group">
                                  <input type="text" class="form-control" name="tuesday_start" id="tuesday_start" placeholder="เวลาเข้างาน">
                                  <span class="form-control-feedback" id="tuesday_start_feedback"></span>
                                  <span class="input-group-btn">
                                    <button class="btn btn-default btn-outline btn-cancel-time" data-id="tuesday_start" type="button"><span class='fa fa-remove'></span></button>
                                  </span>
                                </div>
                              </div>
                            </div>
                            <div class="col-md-6">
                              <div class="form-group has-feedback clockpicker" id="tuesday_end_group">
                                <div class="input-group">
                                  <input type="text" class="form-control" name="tuesday_end" id="tuesday_end" placeholder="เวลาออกงาน">
                                  <span class="form-control-feedback" id="tuesday_end_feedback"></span>
                                  <span class="input-group-btn">
                                    <button class="btn btn-default btn-outline btn-cancel-time" data-id="tuesday_end" type="button"><span class='fa fa-remove'></span></button>
                                  </span>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div class="row">
                            <div class="col-md-12">
                              <label for="wednesday_group">วันพุธ</label>
                            </div>
                            <div class="col-md-6">
                              <div class="form-group has-feedback clockpicker" id="wednesday_start_group">
                                <div class="input-group">
                                  <input type="text" class="form-control" name="wednesday_start" id="wednesday_start" placeholder="เวลาเข้างาน">
                                  <span class="form-control-feedback" id="wednesday_start_feedback"></span>
                                  <span class="input-group-btn">
                                    <button class="btn btn-default btn-outline btn-cancel-time" data-id="wednesday_start" type="button"><span class='fa fa-remove'></span></button>
                                  </span>
                                </div>
                              </div>
                            </div>
                            <div class="col-md-6">
                              <div class="form-group has-feedback clockpicker" id="wednesday_end_group">
                                <div class="input-group">
                                  <input type="text" class="form-control" name="wednesday_end" id="wednesday_end" placeholder="เวลาออกงาน">
                                  <span class="form-control-feedback" id="wednesday_end_feedback"></span>
                                  <span class="input-group-btn">
                                    <button class="btn btn-default btn-outline btn-cancel-time" data-id="wednesday_end" type="button"><span class='fa fa-remove'></span></button>
                                  </span>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div class="row">
                            <div class="col-md-12">
                              <label for="thursday_group">วันพฤหัสบดี</label>
                            </div>
                            <div class="col-md-6">
                              <div class="form-group has-feedback clockpicker" id="thursday_start_group">
                                <div class="input-group">
                                  <input type="text" class="form-control" name="thursday_start" id="thursday_start" placeholder="เวลาเข้างาน">
                                  <span class="form-control-feedback" id="thursday_start_feedback"></span>
                                  <span class="input-group-btn">
                                    <button class="btn btn-default btn-outline btn-cancel-time" data-id="thursday_start" type="button"><span class='fa fa-remove'></span></button>
                                  </span>
                                </div>
                              </div>
                            </div>
                            <div class="col-md-6">
                              <div class="form-group has-feedback clockpicker" id="thursday_end_group">
                                <div class="input-group">
                                  <input type="text" class="form-control" name="thursday_end" id="thursday_end" placeholder="เวลาออกงาน">
                                  <span class="form-control-feedback" id="thursday_end_feedback"></span>
                                  <span class="input-group-btn">
                                    <button class="btn btn-default btn-outline btn-cancel-time" data-id="thursday_end" type="button"><span class='fa fa-remove'></span></button>
                                  </span>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div class="row">
                            <div class="col-md-12">
                              <label for="friday_group">วันศุกร์</label>
                            </div>
                            <div class="col-md-6">
                              <div class="form-group has-feedback clockpicker" id="friday_start_group">
                                <div class="input-group">
                                  <input type="text" class="form-control" name="friday_start" id="friday_start" placeholder="เวลาเข้างาน">
                                  <span class="form-control-feedback" id="friday_start_feedback"></span>
                                  <span class="input-group-btn">
                                    <button class="btn btn-default btn-outline btn-cancel-time" data-id="friday_start" type="button"><span class='fa fa-remove'></span></button>
                                  </span>
                                </div>
                              </div>
                            </div>
                            <div class="col-md-6">
                              <div class="form-group has-feedback clockpicker" id="friday_end_group">
                                <div class="input-group">
                                  <input type="text" class="form-control" name="friday_end" id="friday_end" placeholder="เวลาออกงาน">
                                  <span class="form-control-feedback" id="friday_end_feedback"></span>
                                  <span class="input-group-btn">
                                    <button class="btn btn-default btn-outline btn-cancel-time" data-id="friday_end" type="button"><span class='fa fa-remove'></span></button>
                                  </span>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div class="row">
                            <div class="col-md-12">
                              <label for="saturday_group">วันเสาร์</label>
                            </div>
                            <div class="col-md-6">
                              <div class="form-group has-feedback clockpicker" id="saturday_start_group">
                                  <div class="input-group">
                                  <input type="text" class="form-control" name="saturday_start" id="saturday_start" placeholder="เวลาเข้างาน">
                                  <span class="form-control-feedback" id="saturday_start_feedback"></span>
                                  <span class="input-group-btn">
                                    <button class="btn btn-default btn-outline btn-cancel-time" data-id="saturday_start" type="button"><span class='fa fa-remove'></span></button>
                                  </span>
                                </div>
                              </div>
                            </div>
                            <div class="col-md-6">
                              <div class="form-group has-feedback clockpicker" id="saturday_end_group">
                                <div class="input-group">
                                  <input type="text" class="form-control" name="saturday_end" id="saturday_end" placeholder="เวลาออกงาน">
                                  <span class="form-control-feedback" id="saturday_end_feedback"></span>
                                  <span class="input-group-btn">
                                    <button class="btn btn-default btn-outline btn-cancel-time" data-id="saturday_end" type="button"><span class='fa fa-remove'></span></button>
                                  </span>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div class="row">
                            <div class="col-md-12">
                              <label for="sunday_group">วันอาทิตย์</label>
                            </div>
                            <div class="col-md-6">
                              <div class="form-group has-feedback clockpicker" id="sunday_start_group">
                                <div class="input-group">
                                  <input type="text" class="form-control" name="sunday_start" id="sunday_start" placeholder="เวลาเข้างาน">
                                  <span class="form-control-feedback" id="sunday_start_feedback"></span>
                                  <span class="input-group-btn">
                                    <button class="btn btn-default btn-outline btn-cancel-time" data-id="sunday_start" type="button"><span class='fa fa-remove'></span></button>
                                  </span>
                                </div>
                              </div>
                            </div>
                            <div class="col-md-6">
                              <div class="form-group has-feedback clockpicker" id="sunday_end_group">
                                <div class="input-group">
                                  <input type="text" class="form-control" name="sunday_end" id="sunday_end" placeholder="เวลาออกงาน">
                                  <span class="form-control-feedback" id="sunday_end_feedback"></span>
                                  <span class="input-group-btn">
                                    <button class="btn btn-default btn-outline btn-cancel-time" data-id="sunday_end" type="button"><span class='fa fa-remove'></span></button>
                                  </span>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div class="row">
                            <div class="col-md-12 text-left">
                              <button type="button" class="fcbtn btn btn-outline btn-success btn-1d btn-create-wk">สร้างทำการ</button>
                              <button type="reset" class="fcbtn btn btn-outline btn-default btn-1d">เคลียร์</button>
                            </div>
                          </div>
                        </form>
                        <!-- end form -->
                      </div>
                    </div>
                    <div class="row">
                      <hr>
                      <div class="col-md-12" align='right'>
                          <button class="fcbtn btn btn-outline btn-success btn-1d btn-save-wk">บันทึก</button>
                          <button class="fcbtn btn btn-outline btn-default btn-1d" data-dismiss="modal" >ยกเลิก</button>

                      </div>
                    </div>

            <!-- /content -->
          </div>

        </div>

      </div>
    </div>
  </div>
</div>
