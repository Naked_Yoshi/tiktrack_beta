<style media="screen">
  .table-otDetail tr td{
    vertical-align:middle !important;
  }
  .check-use{
    color: #43a047;
  }
</style>
<div id="wkLink" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="wkLinkLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="clearModals();">×</button>
          <h4 class="modal-title" id="wkLinkLabel"> กลุ่มเวลาทำการ</h4>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-md-12">
            <div class="sttabs tabs-style-circle">
              <nav>
                <ul class='ot_tabs'>
                  <li><a href="#section-wklist" class="sticon ti-list"><span class='tabs-txt'>สรุปเวลาทำการ</span></a></li>
                  <li><a href="#section-circle-2" class="sticon ti-alarm-clock"><span class='tabs-txt'>ล่วงเวลา OT</span></a></li>
                  <li><a href="#section-circle-3" class="sticon ti-calendar"><span class='tabs-txt'>วันหยุดประจำปี</span></a></li>
                  <!-- <li><a href="#section-circle-4" class="sticon ti-calendar"><span class='tabs-txt'>เงื่อนไขการลา</span></a></li> -->
                </ul>
              </nav>
              <div class="content-wrap text-center">
              <section id="section-wklist">
                  <div class="text-left">
                    <div class="row">
                      <div class="col-md-12">
                        <table class='table'>
                          <thead>
                            <th colspan='4'>
                              <h4><span class='fa fa-list'></span> เวลาทำการ</h4>
                            </th>
                          </thead>
                          <tbody>
                            <tr>
                              <td width='100'>วันจันทร์ </td>
                              <td class='text-center'><span id="mon_start"></span></td>
                              <td class='text-center'width='30'>ถึง</td>
                              <td class='text-center'><span id="mon_end"></span></td>
                            </tr>
                            <tr>
                              <td width='100'>วันอังคาร </td>
                              <td class='text-center'<span id="tue_start"></td>
                              <td class='text-center'width='30'>ถึง</td>
                              <td class='text-center'><span id="tue_end"></td>
                            </tr>
                            <tr>
                              <td width='100'>วันพุธ </td>
                              <td class='text-center'><span id="wed_start"></td>
                              <td class='text-center' width='30'>ถึง</td>
                              <td class='text-center'><span id="wed_end"></td>
                            </tr>
                            <tr>
                              <td width='100'>วันพฤหัสบดี </td>
                              <td class='text-center'><span id="thu_start"></td>
                              <td class='text-center' width='30'>ถึง</td>
                              <td class='text-center'><span id="thu_end"></td>
                            </tr>
                            <tr>
                              <td width='100'>วันศุกร์ </td>
                              <td class='text-center'><span id="fri_start"></td>
                              <td class='text-center' width='30'>ถึง</td>
                              <td class='text-center'><span id="fri_end"></td>
                            </tr>
                            <tr>
                              <td width='100'>วันเสาร์ </td>
                              <td class='text-center'><span id="sat_start"></td>
                              <td class='text-center' width='30'>ถึง</td>
                              <td class='text-center'><span id="sat_end"></td>
                            </tr>
                            <tr>
                              <td width='100'>วันอาทิตย์ </td>
                              <td class='text-center'><span id="sun_start"></td>
                              <td class='text-center' width='30'>ถึง</td>
                              <td class='text-center'><span id="sun_end"></td>
                            </tr>
                          </tbody>
                        </table>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-12">
                        <table class='table'>
                          <thead>
                            <th colspan='4'>
                              <h4><span class='fa fa-clock-o'></span> ค่าล่วงเวลา OT</h4>
                            </th>
                          </thead>
                          <tbody>
                            <tr>
                                  <td class='text-left' id='td_wk_ot'>
                                    <span id="text_wk_ot"></span>
                                  </td>
                            </tr>
                          </tbody>
                        </table>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-12">
                        <table class='table'>
                          <thead>
                            <th colspan='4'>
                              <h4><span class='fa fa-calendar'></span> วันหยุดประจำปี</h4>
                            </th>
                          </thead>
                          <tbody>
                            <tr>
                                  <td class='text-left' id='td_wk_hd'>
                                    <span id="text_wk_hd"></span>
                                  </td>
                            </tr>
                          </tbody>
                        </table>
                      </div>
                    </div>
                  </div>
              </section>
              <section id="section-circle-2">
                <div class="row text-left">
                  <div class="col-md-12">
                    <div class="form-group">
                      <label for="selectOt">เลือกกลุ่มล่วงเวลา</label>
                      <select class="form-control" name="selectOt" id="selectOt">

                      </select>
                    </div>
                  </div>
                </div>
                <div class="ot-detail-dialog"></div>
              </section>
              <section id="section-circle-3">
                <div class="text-left">
                    <div class="row">
                      <div class="col-md-12">
                        <div class="form-group">
                          <label for="selectHd">เลือกกลุ่มวันหยุดประจำปี</label>
                          <select class="form-control" name="" id="selectHd">

                          </select>
                        </div>
                      </div>
                      <div class="col-md-12">
                        <div class="hd-detail-dialog"></div>
                      </div>
                    </div>
                </div>
                </section>
                <!-- <section id="section-circle-4">
                  <div class="text-left">
                      <div class="row">
                        <div class="col-md-12">
                          <div class="form-group">
                            <label for="selectLev">เลือกกลุ่มวันลา</label>
                            <select class="form-control" name="" id="selectLev">

                            </select>
                          </div>
                        </div>
                        <div class="col-md-12">
                          <div class="Lev-detail-dialog"></div>
                        </div>
                      </div>
                  </div>
                  </section> -->
              </div>
                                                  <!-- /content -->
            </div>
          </div>

        </div>

      </div>
    </div>
  </div>
</div>
