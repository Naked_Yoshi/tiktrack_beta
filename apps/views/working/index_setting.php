<div class="white-box">
  <div class="row">
    <div class="col-md-12">
      <a class='btn btn-success pull-right m-l-20 btn-rounded btn-outline hidden-xs hidden-sm waves-effect waves-light btn-create-working'><span class="fa fa-plus"></span> เพิ่มวันทำการ</a>
      <h4>ข้อมูลวันทำการ</h4>
      <hr>
    </div>
  </div>
  <div class="row">
      <div class="col-md-12">
          <!-- <div class="table-responsive"> -->
            <table class='table table-bordered  table-striped working_table'>
              <thead>
                <th  width='50' class='text-center' >ลำดับ</th>
                <th>ชื่อวันทำการ</th>
                <th width='150'>ตัวเลือก</th>
              </thead>
              <tbody id='working_body'>

              </tbody>
            </table>
          <!-- </div> -->
      </div>
  </div>
</div>
