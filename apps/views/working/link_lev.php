<div class="row ">
  <div class="col-md-12 text-center" id="link_lev_head">
  </div>
  <div class="col-md-12">
    <table class='table'>
      <thead>
        <th width='30%'>เงื่อนไข</th>
        <th>แจ้งให้ทราบล่วงหน้า</th>
        <th>สิทธิตามกฏหมาย</th>
        <th align="center" style="text-align:center">หักเงิน</th>
        <th align="center" style="text-align:center">ลาฉุกเฉิน</th>
        <th align="center" style="text-align:center">ลาย้อนหลัง</th>
      </thead>
      <tbody id="link_lev_body">
      </tbody>
    </table>
  </div>
  <div class="col-md-12" align='right'>
    <hr>
    <div class="row">
      <div class="col-md-8 text-left" style="padding-top:0px;">
      <h4>ผู้อนุมัติการลา</h>
      </div>
      <div class="col-md-4" style="padding-top:5px">
      <a class='btn btn-info pull-right m-l-20 btn-outline hidden-xs hidden-sm waves-effect waves-light btn-add-lev-approve'><span class='fa fa-plus'></span> เพิ่มผู้อนุมัติการลา</a>
      </div>
    </div>
    <div class="row lev-approve" align='left' style="font-size:18px;">
      <span class='text-center no-list-lev-approve' data-row='0'>
        <h5>ไม่พบรายการ โปรดเพิ่มผู้อนุมัติ</h5>
      </span>
    </div>
    <hr>
    <button type="button" class='btn btn-success btn-outline btn-link-lev' name="button">
      เชื่อมต่อกลุ่มวันลา
    </button>
  </div>
</div>
