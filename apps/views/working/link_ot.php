    <div class="row text-left">
      <!-- <?php //if (!$detail["config"]["Has"]){ ?> -->
      <!-- <div class="col-md-12 text-center">
        <p><span class='fa fa-info-circle'></span> กรุณาเลือกรายการล่วงเวลา</p>
      </div> -->
      <?php //}//else { ?>
      <div class="col-md-12">
          <!-- <?php
            // $config =  $detail["config"]["data"];
            // // print_r($config);
            // $excess = $detail["excess"];
            // $approve = $detail["approve"];
          ?> -->
          <h4>กลุ่มล่วงเวลา <span id="title_ot_name"></span></h4>
          <br>
          <table class='table table-otDetail'>
              <tr>
                <td  width='170' rowspan="2" class='text-center'>
                  กำหนดการเริ่มล่วงเวลา
                </td>
                <!-- <?php
                  // $before_minute = "เริ่มทันที";
                  // if ($config["ot_before_minute"] > 0){
                  //       $before_minute = $config["ot_before_minute"]." นาที";
                  // }
                  // $after_minute = "เริ่มทันที";
                  // if ($config["ot_after_minute"] > 0){
                  //       $after_minute = $config["ot_after_minute"]." นาที";
                  //}
                  ?> -->
                <td><h5>ก่อนเข้างาน (นาที)</h5> <small> <span id="before_minute"></span> </small></td>
              </tr>
              <tr>
                <td><h5>หลังเลิกงาน (นาที)</h5> <small> <span id="after_minute"> </small></td>
              </tr>
          </table>

          <!-- <table class='table table-otDetail'>
              <tr>
                <td width='170' rowspan="2" class='text-center'>
                  จำนวนการขอล่วงเวลา
                </td>
                <td>
                  <!-- <?php
                    // $limit_date = "สามารถขอทำงานล่วงเวลาได้ไม่จำกัด";
                    // if ($config["ot_limit_date"] > 0){
                    //       $limit_date = $config["ot_limit_date"]." ชม. / วัน";
                    // }
                    // $limit_month = "สามารถขอทำงานล่วงเวลาได้ไม่จำกัด";
                    // if ($config["ot_limit_month"] > 0){
                    //       $limit_month = $config["ot_limit_month"]." ชม. / เดือน";
                    //}
                    ?> -->
                  <!-- <h5>จำนวนที่ขอได้ต่อวัน (ชม)</h5> <small> <?php //echo $limit_date; ?> </small>
                </td>
              </tr>
              <tr>
                <td><h5>จำนวนที่ขอได้ต่อเดือน (ชม)</h5> <small> <?php //echo $limit_month; ?></small></td>
              </tr>
          </table> -->

          <!-- <table class='table table-otDetail'>
            <?php
              // $icon_request = "<span class='fa fa-check check-use'></span>";
              // $msg_request = "<h5>ขออนุมัติล่วงหน้า </h5><small>ขอล่วงเวลาก่อน ".$config["ot_avaliable_time_before"]." นาที </small>    ";
              // if ($config["ot_request"] < 1){
              //     $icon_request  = "<span class='fa fa-remove'></span>";
              //     $msg_request = "<h5>ไม่ต้องขอการขออนุมัติ</h5>";
              // }
              // $icon_urgent = "<span class='fa fa-check check-use'></span>";
              // $msg_urgent = "<h5>อนุญาตขอเร่งด่วน</small>    ";
              // if ($config["ot_urgent_request"] < 1){
              //     $icon_urgent  = "<span class='fa fa-remove'></span>";
              //     $msg_urgent = "<h5>ไม่อนุญาตขอเร่งด่วน</h5>";
              // }
              // $icon_approve = "<span class='fa fa-check check-use'></span>";
              // $msg_approve = "<h5> ขออนุมัติจ่าย </h5><small>กำนดจ่ายทุก ๆ วันที่ ".$config["ot_paid_date"]." ของทุกๆ เดือน </small>    ";
              // if ($config["ot_approve_paid"] < 1){
              //     $icon_approve  = "<span class='fa fa-remove'></span>";
              //     $msg_approve = "<h5>ไม่ต้องขออนุมัติจ่าย</h5>";
              // }
              // $icon_package = "<span class='fa fa-check check-use'></span>";
              // $msg_package = "<h5> อนุญาตคำนวนค่าล่วงเวลาแบบเหมาจ่าย </h5> ";
              // if ($config["ot_package"] < 1){
              //     $icon_package  = "<span class='fa fa-remove'></span>";
              //     $msg_package = "<h5>ไม่อนุญาตคำนวนแบบเหมาจ่าย</h5>";
              // }
            ?>
              <tr>
                <td width='50'>
                  <?php echo $icon_request; ?>
                </td>
                <td>
                  <?php echo $msg_request; ?>
                </td>
              </tr>
              <tr>
                <td width='50'>
                  <?php echo $icon_urgent; ?>
                </td>
                <td><h5><?php echo $msg_urgent; ?></h5> </td>
              </tr>
              <tr>
                <td width='50'>
                  <?php echo $icon_approve; ?>
                </td>
                <td> <?php echo $msg_approve; ?></td>
              </tr>
              <tr>
                <td width='50'>
                  <?php echo $icon_package; ?>
                </td>
                <td> <?php echo $msg_package; ?> </td>
              </tr>
          </table> -->

          <table class='table table-otDetail'>
              <tr>
                <td>
                  <h5>เงื่อนไขการกำหนดเศษนาที</h5>
                    <p id="excess_detail"></p>
                </td>
              </tr>
          </table>
          <table class='table table-otDetail'>
              <tr>
                <td>
                  <h5>อัตราการคำนวณค่าล่วงเวลา</h5>
                  <p> วันทำงานปกติ (นอกเวลาทำการ) <span id="ot_working_ex"></span> เท่า
                      <br>
                      <p id="otp_working"></p>
                  </p>
                  <p> วันหยุดประจำสัปดาห์ (เวลาทำการ) <span id="ot_weekend"></span> เท่า & วันหยุดประจำสัปดาห์ (นอกเวลาทำการ) <span id="ot_weekend_ex"></span> เท่า
                      <br>
                      <p id="otp_weekend"></p>
                  </p>
                  <p> วันหยุดประจำปี (เวลาทำการ) <span id="ot_holiday"></span> เท่า & วันหยุดประจำปี (นอกเวลาทำการ) <span id="ot_holiday_ex"></span> เท่า
                      <br>
                      <p id="otp_holiday"></p>
                  </p>
                </td>
              </tr>
          </table>
      </div>
      <div class="col-md-12" align='right'>
        <hr>
        <button type="button" class='btn btn-success btn-outline btn-link-ot' name="button">
          เชื่อมต่อกลุ่มล่วงเวลา
        </button>
      </div>
      <?php //} ?>
    </div>
