<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<link rel="icon" type="image/png" sizes="16x16" href="../plugins/images/favicon.png">
<title>#tiktrack Best service for time attendance solution.</title>
<!-- Bootstrap Core CSS -->
<link href="<?php echo base_url("assets/bootstrap/dist/css/bootstrap.min.css"); ?>" rel="stylesheet">
<!-- animation CSS -->
<link href="<?php echo base_url("assets/css/animate.css"); ?>" rel="stylesheet">
<!-- Custom CSS -->
<link href="<?php echo base_url("assets/css/style.css"); ?>" rel="stylesheet">
<!-- color CSS -->
<link href="<?php echo base_url("assets/css/colors/default.css"); ?>" id="theme"  rel="stylesheet">
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
<link rel="stylesheet" href="<?php echo base_url("assets/css/sweetalert2/dist/sweetalert2.min.js"); ?>">
</head>
<body bgcolor="#fbe9e7">
<!-- Preloader -->
<div class="preloader">
  <div class="cssload-speeding-wheel"></div>
</div>
<section id="wrapper" class="">
  <div class="login-box">
  <div class="row" style="background-color: #b71c1c;">
    <h1 class="text-center" style="font-size:30px;color:#ffffff;">Reset Password</h1>
  </div>
  <br>
  <div class="row">
    <div class="input-group"> <span class="input-group-addon"><span class="fa fa-key"></span></span>
        <input type="password" placeholder="Password ใหม่" id="new_password" class="form-control">
    </div>
    <br>
    <div class="input-group"> <span class="input-group-addon"><span class="fa fa-key"></span></span>
        <input type="password" placeholder="ยืนยัน Password" id="renew_password" class="form-control">
    </div>
    <span id="labelchk"></span>
  </div>
  <br>
  <div class="row" style="background-color: #b71c1c; padding:5px; ">
      <div class="col-sm-12" style="text-align:center;">
          <button class="btn btn-outline btn-rounded btn-default btn-resetpass" style="width:150px;">Save</button>
      </div>
  </div>
  <br>
  </div>
</section>
<!-- jQuery -->
<script src="<?php echo base_url("assets/plugins/bower_components/jquery/dist/jquery.min.js"); ?>"></script>
<!-- Bootstrap Core JavaScript -->
<script src="<?php echo base_url("assets/bootstrap/dist/js/bootstrap.min.js");?>"></script>
<!-- Menu Plugin JavaScript -->
<script src="<?php echo base_url("assets/plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.js"); ?>"></script>

<!--slimscroll JavaScript -->
<script src="<?php echo base_url("assets/js/jquery.slimscroll.js");?>"></script>
<!--Wave Effects -->
<script src="<?php echo base_url("assets/js/waves.js");?>"></script>
<!-- Custom Theme JavaScript -->
<script src="<?php echo base_url("assets/js/custom.min.js"); ?>"></script>
<!--Style Switcher -->
<script src="<?php echo base_url("assets/plugins/bower_components/styleswitcher/jQuery.style.switcher.js"); ?>"></script>

<script src="<?php echo base_url("assets/js/apps/JLogin.js"); ?>"></script>

<!-- <script src="<?php echo base_url("assets/js/apps/JHelpers.js"); ?>"></script> -->

<script src="<?php echo base_url("assets/js/jwt-decode.min.js"); ?>"></script>

<script type="text/javascript" src='<?php echo base_url('assets/css/sweetalert2/dist/sweetalert2.all.min.js');?>'></script>
</body>
</html>
