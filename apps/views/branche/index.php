<div class="white-box">
      <section>
        <div class="row">
          <div class="col-md-12">
            <a class='btn btn-success pull-right m-l-20 btn-rounded btn-outline hidden-xs hidden-sm waves-effect waves-light btn-create-site'>เพิ่มไซต์งาน</a>
            <h4>ข้อมูลไซต์งาน</h4>
            <hr>
          </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <!-- <div class="table-responsive"> -->
                  <table class='table table-bordered  table-striped site_table'>
                    <thead>
                      <th  width='50' class='text-center' >ลำดับ</th>
                      <th>ชื่อไซต์</th>
                      <th width='75'>ตัวเลือก</th>
                    </thead>
                    <tbody id='site_body'>

                    </tbody>
                  </table>
                <!-- </div> -->
            </div>
        </div>
        </section>
</div>
