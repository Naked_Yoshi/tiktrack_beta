<!-- <style media="screen">
  .vtabs{
    width: 100% !important;
  }
  #tabSite{
    width: 15% !important;
  }
  #tabSite_content{
    width: 85% !important;
    padding: 2.5px 20px !important;
  }
</style> -->
<div class="white-box">
    <div class="vtabs">
    <ul class="nav tabs-vertical" id="tabSite">
        <li class="tab active">
          <a data-toggle="tab" href="#detail" aria-expanded="true"> <span class="visible-xs"><i class="ti-home"></i></span> <span class="hidden-xs">ข้อมูลสาขา</span> </a>
        </li>
        <li class="tab tab-BrancheEditlocation">
          <a data-toggle="tab" href="#location" aria-expanded="false"> <span class="visible-xs"><i class="ti-user"></i></span> <span class="hidden-xs">สถานที่</span> </a>
        </li>
        <li class="tab">
          <a aria-expanded="false" data-toggle="tab" href="#ot"> <span class="visible-xs"><i class="ti-email"></i></span> <span class="hidden-xs">ล่วงเวลา (OT)</span> </a>
        </li>
        <!-- <li class="tab">
          <a aria-expanded="false" data-toggle="tab" href="#holiday"> <span class="visible-xs"><i class="ti-email"></i></span> <span class="hidden-xs">วันหยุดประจำปี</span> </a>
        </li> -->
        <li class="tab">
          <a aria-expanded="false" data-toggle="tab" href="#working"> <span class="visible-xs"><i class="ti-email"></i></span> <span class="hidden-xs">วันทำการ</span> </a>
        </li>
        <li class="tab">
          <a aria-expanded="false" data-toggle="tab" href="#employee"> <span class="visible-xs"><i class="ti-email"></i></span> <span class="hidden-xs">สมาชิก</span> </a>
        </li>
        <li class="tab">
          <a aria-expanded="false" data-toggle="tab" href="#grant"> <span class="visible-xs"><i class="ti-email"></i></span> <span class="hidden-xs">ผู้ดูแลและอนุมัติ</span> </a>
        </li>
        <li class="tab">
          <a aria-expanded="false" data-toggle="tab" href="#leave"> <span class="visible-xs"><i class="ti-email"></i></span> <span class="hidden-xs">วันลา</span> </a>
        </li>
    </ul>
    <div class="tab-content" id="tabSite_content">
      <div id="detail" class="tab-pane active">
        <div class="row">
          <div class="col-md-12">
              <h3>ข้อมูลทั่วไปของสาขา</h3><hr>
          </div>
        </div>
        <div class="row">
          <div class="col-md-12">
            <div class="form-group has-feedback" id="sitecreateName_group">
                <h5><label for="sitecreateName"><b>ชื่อสาขา</b> </label></h5>
                <div class="input-group">
                <input type="text" class="form-control validate" name="branchename" id="branchecreateName" placeholder="กรอกชื่อไซต์" value="<?php //echo $detail["brancheName"]; ?>">
                <span class="input-group-btn">
                  <button class="btn btn-outline btn-success btn-save-siteName" type="button"><span class='fa fa-save'></span> บันทึก</button>
                </span>
                </div>
                <span class="form-control-feedback" id="sitecreateName_feedback"></span>
            </div>
          </div>
          <div class="col-md-12">
            <div class="form-group has-feedback" id="branchecreateName_group">
                <h5><label><b>สร้างโดย</b> </label></h5>
                <p><a><span id="createName"></span></a></p>
                <p><small>สร้างเมื่อ : <span id="createAt"></span></small></p>
            </div>
          </div>
          <div class="col-md-12">
            <div class="form-group has-feedback" id="branchecreateName_group">
                <h5><label><b>แก้ไขล่าสุด</b> </label></h5>
                <p><a><span id="updateName"></span></a></p>
                <p><small>สร้างเมื่อ : <span id="updateAt"></span></small></p>
            </div>
          </div>
        </div>
        <div class="clearfix"></div>
      </div>
      <div id="employee" class="tab-pane">
        <div class="row">
          <div class="col-md-12">
              <h3>พนักงานสาขา
              <a class='btn btn-warning pull-right m-l-20 btn-outline hidden-xs hidden-sm waves-effect waves-light btn-link_wk_all_branche' data-toggle="modal" data-target="#link_wk_all_branche"><span class='fa fa-plus'></span> เลือกกลุ่มวันทำการทั้งสาขา</a>
              <a class='btn btn-success pull-right m-l-20 btn-outline hidden-xs hidden-sm waves-effect waves-light btn-clear-emp' data-toggle="modal" data-target="#addbrancheemployees"><span class='fa fa-plus'></span> เพิ่มพนักงาน (จากไซต์งาน)</a>
              <a class='btn btn-info pull-right m-l-20 btn-outline hidden-xs hidden-sm waves-effect waves-light btn-clear-emp-2' data-toggle="modal" data-target="#addfrom_waiting"><span class='fa fa-plus'></span> เพิ่มพนักงาน</a>
            </h3><p>คุณสามารถจัดการพนักงานสาขาได้</p><hr>
          </div>
        </div>
          <div id="link_wk_all_branche" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                  <div class="modal-dialog">
                      <div class="modal-content">
                          <div class="modal-header">
                              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                              <h4 class="modal-title" id="myModalLabel">เลือกวันทำการสาขา</h4>
                          </div>
                          <div class="modal-body">
                              <label for="usr_branche_wk">เลือกวันทำการ</label>
                              <select class="form-control" name="married" id="usr_branche_wk">

                               </select>
                          </div>
                          <div class="modal-footer">
                              <button type="button" class="btn btn-success waves-effect btn-link_wk_all_branche-save">Save</button>
                              <button type="button" class="btn btn-danger waves-effect btn-link_wk_all_branche-close" data-dismiss="modal">Close</button>
                          </div>
                      </div>
                      <!-- /.modal-content -->
                  </div>
                  <!-- /.modal-dialog -->
              </div>

                <div id="addbrancheemployees" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                          <div class="modal-dialog">
                              <div class="modal-content">
                                  <div class="modal-header">
                                      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                      <h4 class="modal-title" id="myModalLabel">เพิ่มพนักงานสาขา (จากไซต์งาน)</h4>
                                  </div>
                                  <div class="modal-body">
                                      <label for="emp-branche-waiting-list">พนักงาน</label>
                                      <form onsubmit="$('#emp-branche-waiting-list').blur();return false;" class="pure-form" style="padding-bottom:10px;">
                                        <input id="emp-branche-waiting-list" type="text" name="q" placeholder="ชื่อ หรือนามสกุล" class="form-control">
                                      </form>
                                      <span id="branche_emp_txt_label"></span>
                                      <div class="" style="padding-top:10px;">
                                        <span id="branche-add-emp-chip">


                                        </span>
                                      </div>
                                  </div>
                                  <div class="modal-footer">
                                      <button type="button" class="btn btn-success waves-effect btn-adduser-branche">Save</button>
                                      <button type="button" class="btn btn-danger waves-effect btn-adduser-branche-close" data-dismiss="modal">Close</button>
                                  </div>
                              </div>
                              <!-- /.modal-content -->
                          </div>
                          <!-- /.modal-dialog -->
                      </div>

                      <div id="addfrom_waiting" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                            <h4 class="modal-title" id="myModalLabel">เพิ่มพนักงาน</h4>
                                        </div>
                                        <div class="modal-body">
                                            <label for="emp-branche-waiting-list_2">พนักงาน</label>
                                            <form onsubmit="$('#emp-branche-waiting-list_2').blur();return false;" class="pure-form" style="padding-bottom:10px;">
                                              <input id="emp-branche-waiting-list_2" type="text" name="q" placeholder="ชื่อ หรือนามสกุล" class="form-control">
                                            </form>
                                            <span id="branche_emp_txt_label_2"></span>
                                            <div class="" style="padding-top:10px;">
                                              <span id="branche-add-emp-chip_2">


                                              </span>
                                            </div>
                                            <br>
                                            <label for="usr_wk">เลือกวันทำการ</label>
                                            <select class="form-control" name="married" id="usr_wk">

                                             </select>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-success waves-effect btn-adduser-branche-waiting">Save</button>
                                            <button type="button" class="btn btn-danger waves-effect btn-adduser-branche-waiting-close" data-dismiss="modal">Close</button>
                                        </div>
                                    </div>
                                    <!-- /.modal-content -->
                                </div>
                                <!-- /.modal-dialog -->
                            </div>
        <div class="row">
          <div class="col-md-12">
            <table class="table table-bordered " id="employee_branche_table" data-segment="<?php echo $pageSegment; ?>">
                <thead>
                  <th width="50">ลำดับ</th>
                  <th>ชื่อ - นามสกุล</th>
                  <th width='150'>วันทำการ</th>
                  <th width='100'>ตัวเลือก</th>
                </thead>
                <tbody id='employees_branche_body'>

                </tbody>
            </table>
          </div>
        </div>
        <div class="clearfix"></div>
      </div>
      <div id="branche" class="tab-pane">
        <div class="row">
          <div class="col-md-12">
            <h4>สาขา <a class='btn btn-success pull-right m-l-20 btn-outline hidden-xs hidden-sm waves-effect waves-light btn-create-branche'><span class='fa fa-plus'></span> เพิ่มสาขา</a></h4>
             <p>คุณสามารถจัดการสาขาประจำไซต์งานของคุณได้</p>
          </div>
        </div>
      </div>
      <!-- branche -->
      <div id="location" class="tab-pane">
        <form id='form_location'>
          <div class="form-group" id="sitecreateLoname_group">
                <input type="text" class="form-control" id="loname" placeholder="ค้นหาสถานที่ ... " value="">
          </div>
          <div class="row">
            <div class="col-md-12">
                <div class='map' id="map"></div>
            </div>
          </div>
          <br>
          <div class="row">
            <div class="col-md-12">
              <div class="form-group has-feedback" id="sitecreateArea_group">
                  <label for="sitecreateArea">พื้นที่รัศมี : </label> <span id="txt_area"></span><span> เมตร</span>
                  <input type="range" min="10" max="200" name="area" value="<?php echo $detail["area"]?>" id="CircleArea_site">
                  <input type="hidden"name="branche_segment" value="<?php echo $pageSegment; ?>">
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group has-feedback" id="sitecreateLat_group">
                  <label for="sitecreateName">ละติจูด</label>
                  <input type="text" class="form-control" name="lat" id="sitecreateLat" value="<?php echo $detail["lat"]?>" readonly>
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group has-feedback" id="sitecreateLng_group">
                  <label for="sitecreateName">ลองติจูด</label>
                  <input type="text" class="form-control" name="lng" id="sitecreateLng"  value="<?php echo $detail["lng"]?>" readonly>
              </div>
            </div>

          </div>
          <br>
          <div class="row">
            <div class="col-md-12"  align='right'>
                <button type="button" class="fcbtn btn btn-outline btn-info btn-1d btn-save-location">บันทึกตำแหน่ง</button>
            </div>
          </div>
        </form>
      </div>
      <!-- location -->
      <div id="working" class="tab-pane">
        <div class="row">
          <div class="col-md-12">
            <h4>วันทำการประจำไซต์ <a class='btn btn-success pull-right m-l-20 btn-outline hidden-xs hidden-sm waves-effect waves-light btn-add-wk'><span class='fa fa-plus'></span> เพิ่มวันทำการ</a></h4>
              <p>คุณสามารถจัดการวันทำการประจำไซต์งานของคุณได้</p>
          </div>
        </div>
        <div class="row">
          <div class="col-md-12">
              <hr>
              <table class="table table-bordered " id="working_table" data-segment="<?php echo $pageSegment; ?>">
                  <thead>
                    <th class='text-center' width='70'>ลำดับ</th>
                    <th>ชื่อวันทำการ</th>
                    <th width='100'>ตัวเลือก</th>
                  </thead>
                  <tbody id='working_body'>

                  </tbody>
              </table>
          </div>
        </div>
      </div>
      <div id="holiday" class="tab-pane">
        <div class="row">
          <div class="col-md-12">
            <h4>วันหยุดประจำปี
              <a class='btn btn-info pull-right m-l-20 btn-outline hidden-xs hidden-sm waves-effect waves-light btn-create-site'><span class='fa fa-file-o'></span> ดึงข้อมูล</a>
              <a class='btn btn-success pull-right m-l-20 btn-outline hidden-xs hidden-sm waves-effect waves-light btn-create-holiday'><span class='fa fa-plus'></span> เพิ่มวันหยุด</a>
            </h4>
            <p>คุณสามารถกำหนดวันหยุดประจำปีของสาขาของคุณได้</p>
          </div>
        </div>
        <div class="row">
          <div class="col-md-12">
              <hr>
              <table class="table table-bordered " id="holiday_table" data-segment="<?php echo $pageSegment; ?>">
                  <thead>
                    <th width="50">ลำดับ</th>
                    <th>กลุ่มวัหยุด</th>
                    <th width='100'>ตัวเลือก</th>
                  </thead>
                  <tbody id='holiday_body'>

                  </tbody>
              </table>
          </div>
        </div>
      </div>
      <div id="ot" class="tab-pane">
        <div class="row">
          <div class="col-md-12">
            <h4>ล่วงเวลา (OT)
              <a class='btn btn-success pull-right m-l-20 btn-outline hidden-xs hidden-sm waves-effect waves-light btn-ot-creategroup'><span class='fa fa-plus'></span> เพิ่มกลุ่มล่วงเวลา</a>
            </h4>
            <p>คุณสามารถกำหนดค่าล่วงเวลาประจำปีของไซต์งานของคุณได้</p>
          </div>
        </div>
        <div class="row">
          <div class="col-md-12">
            <hr>
            <table class="table table-bordered " id="ot_table" data-segment="<?php echo $pageSegment; ?>">
                <thead>
                  <th width="50">ลำดับ</th>
                  <th>กลุ่มเวลาล่วงเวลา</th>
                  <th width='100'>ตัวเลือก</th>
                </thead>
                <tbody id='ot_body'>

                </tbody>
            </table>
          </div>
        </div>
      </div>
      <div id="grant" class="tab-pane">
        <div class="row">
          <div class="col-md-12">
            <h4>ผู้ดูแลและอนุมัติ
              <a class='btn btn-success pull-right m-l-20 btn-outline hidden-xs hidden-sm waves-effect waves-light btn-clear-approve' data-toggle="modal" data-target="#myModal"><span class='fa fa-plus'></span> เพิ่มผู้ดูแลและอนุมัติ</a>
            </h4>
            <div id="myModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                              <div class="modal-dialog">
                                  <div class="modal-content">
                                      <div class="modal-header">
                                          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                          <h4 class="modal-title" id="myModalLabel">เพิ่มผู้ดูแลและผู้อนุมัติ</h4>
                                      </div>
                                      <div class="modal-body">
                                          <label for="advanced-demo">ผู้ดูแล</label>
                                          <form onsubmit="$('#advanced-demo').blur();return false;" class="pure-form" style="padding-bottom:10px;">
                                            <input id="advanced-demo" type="text" name="q" placeholder="ชื่อ หรือนามสกุล" class="form-control">
                                          </form>
                                          <span id="txt_label"></span>
                                          <div class="" style="padding-top:10px;">
                                            <span id="emp-chip">


                                            </span>
                                          </div>
                                      </div>
                                      <div class="modal-footer">
                                          <button type="button" class="btn btn-success waves-effect btn-grant-creategroup">Save</button>
                                          <button type="button" id="btn_close_approve" class="btn btn-danger waves-effect" data-dismiss="modal">Close</button>
                                      </div>
                                  </div>
                                  <!-- /.modal-content -->
                              </div>
                              <!-- /.modal-dialog -->
                          </div>
            <p>คุณสามารถกำหนดค่าผู้ดูแลและอนุมัติได้</p>
          </div>
        </div>
        <div class="row">
          <div class="col-md-12">
            <hr>
            <table class="table table-bordered " id="grant_table" data-segment="<?php echo $pageSegment; ?>">
                <thead>
                  <th width="50">ลำดับ</th>
                  <th>ชื่อ - นามสกุล</th>
                  <th width='100'>ตัวเลือก</th>
                </thead>
                <tbody id='approve_body'>

                </tbody>
            </table>
          </div>
        </div>
      </div>
      <div id="leave" class="tab-pane">
        <div class="row">
          <div class="col-md-12">
            <h4>วันลา
            </h4>
            <p>คุณสามารถกำหนดวันลาได้</p>
          </div>
        </div>
        <hr>
        <div class="row">
          <div class="col-md-12">
            <div class="form-group">
              <label for="selectLev">เลือกกลุ่มวันลา</label>
              <select class="form-control" name="" id="selectLev">

              </select>
            </div>
          </div>
          <div class="col-md-12">
            <hr>
              <div class="Lev_detail"></div>
              </div>
          </div>
        </div>
    </div>
    </div>
</div>
