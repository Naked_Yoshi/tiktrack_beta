
<div id="brancheCreate" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="brancheCreateLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="clearModals();">×</button>
          <h4 class="modal-title" id="brancheCreateLabel"> เพิ่มสาขาประจำไซต์ : <?php echo $site["data"][0]["siteName"]; ?></h4>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-md-12">
            <div class="sttabs tabs-style-circle">
            <nav>
            <ul class='site_tabs'>
                <li class='btn-step tab-current' data-phase="branche_detail" data-persent="0%" data-progress="danger"  id="sbranche_detail_tab"  data-toggle='site_detail'><a class="sticon ti-home" disabled="disabled"><span class='tabs-txt'>สาขา</span></a></li>
                <li class='btn-step' data-phase="branche_location" data-persent="33.33%"  data-progress="danger"  id="branche_location_tab" data-toggle='site_location'><a class="sticon ti-location-pin"><span class='tabs-txt'>สถานที่</span></a></li>
                <li class='btn-step' data-phase="branche_working" data-persent="66.66%"  data-progress="danger" id="branche_working_tab" data-toggle='site_working'><a class="sticon ti-calendar"><span class='tabs-txt'>วันทำการ</span></a></li>
                <li class='btn-step'  data-phase="branche_submit" data-persent="100%" data-progress="success"  id="branche_submit_tab" data-toggle='site_ot'><a class="sticon ti-save"><span class='tabs-txt'>บันทึกข้อมูล</span></a></li>
            </ul>
            </nav>
            <div class="site_tabs_content content-wrap text-center">
                <div class="row tabs-progress">
                  <div class="col-md-12">
                    <div class="progress">
                       <div class="progress-bar progress-bar-danger" style="width: 0%;" role="progressbar"></div>
                     </div>
                  </div>
                </div>

                <section id="branche_detail" class='content-current'>
                    <div class="row">
                      <div class="form-group has-feedback" id="branchecreateName_group">
                          <label for="branchename">ชื่อสาขา</label>
                          <input type="text" class="form-control validate" name="branchename" id="branchecreateName" placeholder="กรอกชื่อสาขา">
                          <span class="form-control-feedback" id="branchecreateName_feedback"></span>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-12" align='right'>
                          <button class="fcbtn btn btn-outline btn-info btn-1d btn-step" data-phase="branche_location" data-persent="33.33%">ถัดไป</button>
                      </div>
                    </div>
                </section>
                <!-- detail -->

                <section id="branche_location">
                    <div class="form-group" id="sitecreateLoname_group">
                          <input type="text" class="form-control" id="loname" placeholder="ค้นหาสถานที่ ... " value="">
                    </div>
                    <div class="row">
                      <div class="col-md-12">
                          <div class='map' id="branchMap"></div>
                      </div>
                    </div>
                    <br>
                    <div class="row">
                      <div class="col-md-12">
                        <div class="form-group has-feedback" id="branchecreateArea_group">
                            <label for="sitecreateArea">พื้นที่รัศมี : </label> <span id="txt_area"></span><span> เมตร</span>
                            <input type="range" min="10" max="200" name="area" value="<?php echo $site["data"][0]["area"]; ?>" id="CircleArea_branche">
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="form-group has-feedback" id="branchecreateLat_group">
                            <label for="branchecreateName">ละติจูด</label>
                            <input type="text" class="form-control" name="lat" id="branchecreateLat" placeholder="กรอกชื่อไซต์" value="<?php echo $site["data"][0]["lat"]; ?>" readonly>
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="form-group has-feedback" id="branchecreateLng_group">
                            <label for="branchecreateName">ลองติจูด</label>
                            <input type="text" class="form-control" name="lng" id="branchecreateLng" placeholder="กรอกชื่อไซต์" value="<?php echo $site["data"][0]["lng"]; ?>" readonly>
                        </div>
                      </div>

                    </div>
                    <br>
                    <div class="row">
                      <div class="col-md-12"  align='right'>
                          <button class="fcbtn btn btn-outline btn-default btn-1d btn-step" data-phase="branche_detail" data-persent="0%">ย้อนกลับ</button>
                          <button class="fcbtn btn btn-outline btn-info btn-1d btn-step" data-phase="branche_working" data-persent="66.66%">ถัดไป</button>
                      </div>
                    </div>
                </section>
                <!-- location -->

                <section id="branche_working">
                    <div class="row">
                      <div class="col-md-12 text-left">
                        <h4><span class='fa fa-list-alt'></span> วันทำการประจำสาขา</h4>
                        <table class='table'>
                          <thead>
                              <th class='text-left' ><b>รายการวันทำการประจำสาขา</b></th>
                              <th class='text-right' colspan="2">
                                <!-- <a data-toggle="tab" href="#createnew"  class='btn btn-outline btn-success wk-new-create' aria-expanded="false"><span class='fa fa-file'></span> สร้างใหม่ </a>
                                <a data-toggle="tab" href="#list_wk"  class='btn btn-outline btn-info wk-search' aria-expanded="false"><span class='fa fa-search'></span> ค้นหา </a> -->
                              </th>
                          </thead>
                          <tbody id='wk_select_table'>
                            <tr class='noSelect'>
                              <td colspan="3" class='text-center '><b> --- เลือกวันทำการ --- </b></td>
                            </tr>
                          </tbody>
                        </table>
                      </div>
                    </div>

                    <div class="row">
                      <div class='col-md-12' id="list_wk">
                        <div class="row">
                          <div class="col-md-12 text-left">
                            <h4><span class='fa fa-calendar'></span> รายการวันทำการ
                                <!-- <a class="close close-wk-list"><span class="fa fa-remove"></span></a> -->
                            </h4>
                            <hr>
                          </div>
                        </div>
                        <div class="row">
                          <div class="col-md-12">
                            <div class="vtabs">
                                    <ul class="nav tabs-vertical tabs-working-list"></ul>
                                    <div class="tab-content tabs-working-content"></div>
                                </div>
                          </div>
                        </div>
                      </div>
                      <div class="col-md-12" id="create_new_wk">
                        <!-- create form -->
                        <form align='left' id='create_form_wk'>
                          <div class="row">
                            <div class="col-md-12">
                              <h4>
                                สร้างใหม่
                                <a class="close close-new"><span class="fa fa-remove"></span></a>
                              </h4>
                              <hr>
                            </div>
                          </div>
                          <div class="row">
                            <div class="col-md-12">
                              <div class="form-group has-feedback" id="wk_name_group">
                                  <label for="wk_name">ชื่อวันทำการ</label>
                                  <input type="text" class="form-control" name="wk_name" id="wk_name" placeholder="เวลาเข้างาน">
                                  <span class="form-control-feedback" id="wk_name_feedback"></span>
                              </div>
                              <input type="hidden" name="segment" id="segment" value="<?php echo $site["segment"]; ?>">
                            </div>
                          </div>

                          <div class="row">
                            <div class="col-md-12">
                                <label for="monday_group">วันจันทร์</label>
                            </div>
                            <div class="col-md-6">
                              <div class="form-group has-feedback" id="monday_start_group">
                                  <div class="input-group">
                                    <input type="text" class="form-control clockpicker" name="monday_start" id="monday_start" placeholder="เวลาเข้างาน">
                                    <span class="form-control-feedback" id="monday_start_feedback"></span>
                                    <span class="input-group-btn">
                                      <button class="btn btn-default btn-outline btn-cancel-time" data-id="monday_start" type="button"><span class='fa fa-remove'></span></button>
                                    </span>
                                  </div>
                              </div>

                            </div>
                            <div class="col-md-6">
                              <div class="form-group has-feedback clockpicker" id="monday_end_group">
                                  <div class="input-group">
                                    <input type="text" class="form-control" name="monday_end" id="monday_end" placeholder="เวลาออกงาน">
                                    <span class="form-control-feedback" id="monday_end_feedback"></span>
                                    <span class="input-group-btn">
                                      <button class="btn btn-default btn-outline btn-cancel-time" data-id="monday_end" type="button"><span class='fa fa-remove'></span></button>
                                    </span>
                                  </div>
                              </div>
                            </div>
                            <div class="col-md-12">
                                <a class='apply-all'><span class='fa fa-check-circle'></span> นำไปใช้ทั้งหมด</a>
                            </div>
                          </div>
                          <br>
                          <div class="row">
                            <div class="col-md-12">
                              <label for="tuesday_group">วันอังคาร</label>
                            </div>
                            <div class="col-md-6">
                              <div class="form-group has-feedback clockpicker" id="tuesday_start_group">
                                <div class="input-group">
                                  <input type="text" class="form-control" name="tuesday_start" id="tuesday_start" placeholder="เวลาเข้างาน">
                                  <span class="form-control-feedback" id="tuesday_start_feedback"></span>
                                  <span class="input-group-btn">
                                    <button class="btn btn-default btn-outline btn-cancel-time" data-id="tuesday_start" type="button"><span class='fa fa-remove'></span></button>
                                  </span>
                                </div>
                              </div>
                            </div>
                            <div class="col-md-6">
                              <div class="form-group has-feedback clockpicker" id="tuesday_end_group">
                                <div class="input-group">
                                  <input type="text" class="form-control" name="tuesday_end" id="tuesday_end" placeholder="เวลาออกงาน">
                                  <span class="form-control-feedback" id="tuesday_end_feedback"></span>
                                  <span class="input-group-btn">
                                    <button class="btn btn-default btn-outline btn-cancel-time" data-id="tuesday_end" type="button"><span class='fa fa-remove'></span></button>
                                  </span>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div class="row">
                            <div class="col-md-12">
                              <label for="wednesday_group">วันพุธ</label>
                            </div>
                            <div class="col-md-6">
                              <div class="form-group has-feedback clockpicker" id="wednesday_start_group">
                                <div class="input-group">
                                  <input type="text" class="form-control" name="wednesday_start" id="wednesday_start" placeholder="เวลาเข้างาน">
                                  <span class="form-control-feedback" id="wednesday_start_feedback"></span>
                                  <span class="input-group-btn">
                                    <button class="btn btn-default btn-outline btn-cancel-time" data-id="wednesday_start" type="button"><span class='fa fa-remove'></span></button>
                                  </span>
                                </div>
                              </div>
                            </div>
                            <div class="col-md-6">
                              <div class="form-group has-feedback clockpicker" id="wednesday_end_group">
                                <div class="input-group">
                                  <input type="text" class="form-control" name="wednesday_end" id="wednesday_end" placeholder="เวลาออกงาน">
                                  <span class="form-control-feedback" id="wednesday_end_feedback"></span>
                                  <span class="input-group-btn">
                                    <button class="btn btn-default btn-outline btn-cancel-time" data-id="wednesday_end" type="button"><span class='fa fa-remove'></span></button>
                                  </span>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div class="row">
                            <div class="col-md-12">
                              <label for="thursday_group">วันพฤหัสบดี</label>
                            </div>
                            <div class="col-md-6">
                              <div class="form-group has-feedback clockpicker" id="thursday_start_group">
                                <div class="input-group">
                                  <input type="text" class="form-control" name="thursday_start" id="thursday_start" placeholder="เวลาเข้างาน">
                                  <span class="form-control-feedback" id="thursday_start_feedback"></span>
                                  <span class="input-group-btn">
                                    <button class="btn btn-default btn-outline btn-cancel-time" data-id="thursday_start" type="button"><span class='fa fa-remove'></span></button>
                                  </span>
                                </div>
                              </div>
                            </div>
                            <div class="col-md-6">
                              <div class="form-group has-feedback clockpicker" id="thursday_end_group">
                                <div class="input-group">
                                  <input type="text" class="form-control" name="thursday_end" id="thursday_end" placeholder="เวลาออกงาน">
                                  <span class="form-control-feedback" id="thursday_end_feedback"></span>
                                  <span class="input-group-btn">
                                    <button class="btn btn-default btn-outline btn-cancel-time" data-id="thursday_end" type="button"><span class='fa fa-remove'></span></button>
                                  </span>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div class="row">
                            <div class="col-md-12">
                              <label for="friday_group">วันศุกร์</label>
                            </div>
                            <div class="col-md-6">
                              <div class="form-group has-feedback clockpicker" id="friday_start_group">
                                <div class="input-group">
                                  <input type="text" class="form-control" name="friday_start" id="friday_start" placeholder="เวลาเข้างาน">
                                  <span class="form-control-feedback" id="friday_start_feedback"></span>
                                  <span class="input-group-btn">
                                    <button class="btn btn-default btn-outline btn-cancel-time" data-id="friday_start" type="button"><span class='fa fa-remove'></span></button>
                                  </span>
                                </div>
                              </div>
                            </div>
                            <div class="col-md-6">
                              <div class="form-group has-feedback clockpicker" id="friday_end_group">
                                <div class="input-group">
                                  <input type="text" class="form-control" name="friday_end" id="friday_end" placeholder="เวลาออกงาน">
                                  <span class="form-control-feedback" id="friday_end_feedback"></span>
                                  <span class="input-group-btn">
                                    <button class="btn btn-default btn-outline btn-cancel-time" data-id="friday_end" type="button"><span class='fa fa-remove'></span></button>
                                  </span>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div class="row">
                            <div class="col-md-12">
                              <label for="saturday_group">วันเสาร์</label>
                            </div>
                            <div class="col-md-6">
                              <div class="form-group has-feedback clockpicker" id="saturday_start_group">
                                  <div class="input-group">
                                  <input type="text" class="form-control" name="saturday_start" id="saturday_start" placeholder="เวลาเข้างาน">
                                  <span class="form-control-feedback" id="saturday_start_feedback"></span>
                                  <span class="input-group-btn">
                                    <button class="btn btn-default btn-outline btn-cancel-time" data-id="saturday_start" type="button"><span class='fa fa-remove'></span></button>
                                  </span>
                                </div>
                              </div>
                            </div>
                            <div class="col-md-6">
                              <div class="form-group has-feedback clockpicker" id="saturday_end_group">
                                <div class="input-group">
                                  <input type="text" class="form-control" name="saturday_end" id="saturday_end" placeholder="เวลาออกงาน">
                                  <span class="form-control-feedback" id="saturday_end_feedback"></span>
                                  <span class="input-group-btn">
                                    <button class="btn btn-default btn-outline btn-cancel-time" data-id="saturday_end" type="button"><span class='fa fa-remove'></span></button>
                                  </span>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div class="row">
                            <div class="col-md-12">
                              <label for="sunday_group">วันอาทิตย์</label>
                            </div>
                            <div class="col-md-6">
                              <div class="form-group has-feedback clockpicker" id="sunday_start_group">
                                <div class="input-group">
                                  <input type="text" class="form-control" name="sunday_start" id="sunday_start" placeholder="เวลาเข้างาน">
                                  <span class="form-control-feedback" id="sunday_start_feedback"></span>
                                  <span class="input-group-btn">
                                    <button class="btn btn-default btn-outline btn-cancel-time" data-id="sunday_start" type="button"><span class='fa fa-remove'></span></button>
                                  </span>
                                </div>
                              </div>
                            </div>
                            <div class="col-md-6">
                              <div class="form-group has-feedback clockpicker" id="sunday_end_group">
                                <div class="input-group">
                                  <input type="text" class="form-control" name="sunday_end" id="sunday_end" placeholder="เวลาออกงาน">
                                  <span class="form-control-feedback" id="sunday_end_feedback"></span>
                                  <span class="input-group-btn">
                                    <button class="btn btn-default btn-outline btn-cancel-time" data-id="sunday_end" type="button"><span class='fa fa-remove'></span></button>
                                  </span>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div class="row">
                            <div class="col-md-12">
                              <button type="button" class="fcbtn btn btn-outline btn-success btn-1d btn-create-wk-branche">บันทึก</button>
                              <button type="reset" class="fcbtn btn btn-outline btn-default btn-1d">เคลียร์</button>
                            </div>
                          </div>
                        </form>
                        <!-- end form -->
                      </div>
                    </div>
                    <div class="row">
                      <hr>
                      <div class="col-md-12" align='right'>
                          <button class="fcbtn btn btn-outline btn-default btn-1d btn-step" data-phase="branche_location" data-persent="33.33%">ย้อนกลับ</button>
                          <button class="fcbtn btn btn-outline btn-info btn-1d btn-step" data-phase="branche_submit" data-persent="100%" data-progress="success">ถัดไป</button>
                      </div>
                    </div>
                </section>
                <section id="branche_submit">
                  <div class="row">
                    <div class="col-md-12">
                      <p class='text-left'>
                        <label for="mapSubmit"><span class='fa fa-sitemap'></span> ชื่อสาขา</label>
                        <br>
                        <span class='text-left' id="nameSubmit"></span>
                      </p>
                    </div>
                    <div class="col-md-12">
                        <p class='text-left'>
                          <label for="mapSubmit"><span class='ti-location-pin'></span> สถานที่</label>
                        </p>
                        <div class='map' id="branchmapSubmit"></div>
                        <hr>
                    </div>
                    <div class="col-md-12">
                        <p class='text-left'>
                          <label for="tableTime"><span class='fa fa-list-alt'></span> เวลาทำการ </label>
                        </p>
                        <div id="tableTime">
                            <table class='table table-striped'>
                              <thead>
                                  <th class='text-center' width='50'>ลำดับ</th>
                                  <th class="text-left"> รายการเวลาทำการ</th>
                                  <th></th>
                              </thead>
                              <tbody id='tableTime_body'></tbody>
                            </table>
                        </div>
                    </div>
                  </div>
                  <br />
                    <div class="row">
                      <div class="col-md-12 text-center">
                          <button class="fcbtn btn btn-outline btn-default btn-1d btn-step" data-phase="branche_working" data-persent="66.66%"  data-progress="danger">ย้อนกลับ</button>
                          <button class="fcbtn btn btn-outline btn-success btn-1d btn-branche-submit" data-persent="100%">บันทึกข้อมูล</button>
                      </div>
                    </div>
                </section>
            </div>
            <!-- /content -->
            </div>
          </div>

        </div>

      </div>
    </div>
  </div>
</div>
