<div id="tat_detail_modal" class="tat-modal tat-modal-sm">
  <div class="tat-modal-container">
      <div class="tat-modal-header">
        <h6><span class='fa fa-list-alt'></span> รายละเอียดการบันทีกการลงเวลา</h6>
      </div>
      <div class="row">
          <ul class="tabs tabs-fixed-width">
            <li class="tab col s3"><a class="map-in active"><span class='fa fa-angle-double-up'></span> สถานที่การลงเวลาเข้า</a></li>
            <li class="tab col s3"><a class='map-out'><span class='fa fa-angle-double-down'></span> สถานที่การลงเวลาออก</a></li>
          </ul>
        <div class='col s12' id="map_in" style="height:250px;">

        </div>
        <div class='col s12' id="map_out" style="height:250px; display:none;">

        </div>
      </div>

      <div class="tat-modal-content">
        <?php
        $checkin = "-";
        $checkout = "-";
        if ($tat["checkinTimeClient"] != "") {
            $checkin =  $this->functions->chk_datetotimeformat($tat["checkinTimeClient"]);
        }
        if ($tat["checkoutTimeClient"] != "") {
            $checkout = $this->functions->chk_datetotimeformat($tat["checkoutTimeClient"]);
        }
        if ($tat["usedTimeSite"] == 0) {
            $late = $this->functions->chk_timeNegative($this->functions->chk_totimeformat($tat["branche_chkLate"]));
            $bf_ot = $this->functions->chk_timeNegative($this->functions->chk_totimeformat($tat["branche_OT_before"]));
            $af_ot = $this->functions->chk_timeNegative($this->functions->chk_totimeformat($tat["branche_OT_after"]));
        }else{
            $late = $this->functions->chk_timeNegative($this->functions->chk_totimeformat($tat["site_chkLate"]));
            $bf_ot = $this->functions->chk_timeNegative($this->functions->chk_totimeformat($tat["site_OT_before"]));
            $af_ot = $this->functions->chk_timeNegative($this->functions->chk_totimeformat($tat["site_OT_after"]));
        }

        $route_checkin = "-";
        $route_checkout = "-";
        if ($tat["route_checkin"] != "") {
            $route_checkin = "<span class='fa fa-hand-o-up'></span> คลิกลงเวลา";
            if ($tat["route_checkin"] != 1) {
              $route_checkin = "<span class='fa fa-qrcode'></span> แสกน qr code";
            }
        }
        if ($tat["route_checkout"] != "") {
            $route_checkout = "<span class='fa fa-hand-o-up'></span> คลิกลงเวลา";
            if ($tat["route_checkout"] != 1) {
              $route_checkout = "<span class='fa fa-qrcode'></span> แสกน Qr code";
            }
        }
        if ($tat["approveStatus"] == "" || $tat["approveStatus"] == 3) {
            $status = array("color"=>"grey-text","msg"=>"<span class='fa fa-times-circle'></span>  ไม่ตอบสนอง");
            $str_cant = "resp";
        }else if($tat["approveStatus"]== 2){
            $status = array("color"=>"red-text","msg"=>"<span class='fa fa-times-circle'></span> ไม่อนุมัติ");
        }else if($tat["approveStatus"]== 1){
            $status = array("color"=>"green-text","msg"=>"<span class='fa fa-check-circle'></span> อนุมัติแล้ว");
        }

         ?>
          <div class="row">
            <div class="col s12">
              <p><span class='fa fa-user-circle'></span> ข้อมูล</p>
              <li class="divider"></li>
            </div>
            <div class="col s12">
              <label>ชื่อ - นามสกุล </label>
              <p><?php echo $tat["firstname"]." ".$tat["lastname"]; ?> </p>
            </div>
            <div class="col s12 m6">
              <label>ไซต์ </label>
              <p><?php echo $tat["siteName"] ?></p>
            </div>
            <div class="col s12 m6">
              <label>สาขา </label>
              <p><?php echo $tat["brancheName"]; ?></p>
            </div>
            <div class="col s12">
              <p><span class='fa fa-clock-o'></span> การลงเวลา</p>
              <li class="divider"></li><br>
            </div>
            <div class="col s12">
              <label>วันที่  </label>
              <p><?php echo $this->functions->convertDateToStr($tat["checkinTimeClient"]); ?></p>
            </div>
            <?php
              $typecheck_in = "";
              if ($tat["checkInType"] == 3) {
                  $typecheck_in = "<span class='fa fa-exclamation-circle tooltipped yellow-text text-darken-3' data-position='top' data-delay='50' data-tooltip='ลงเวลาย้อนหลัง'></span>";
              }
              $typecheck_out = "";
              if ($tat["checkOutType"] == 4) {
                  $typecheck_out = "<span class='fa fa-exclamation-circle tooltipped yellow-text text-darken-3' data-position='top' data-delay='50' data-tooltip='ลงเวลาย้อนหลัง'></span>";
              }
             ?>
            <div class="col s12 m6">
              <label>เวลาเข้า </label>
              <p><?php echo $checkin." ".$typecheck_in; ?> </p>
            </div>
            <div class="col s12 m6">
              <label>เวลาออก </label>
              <p><?php echo $checkout." ".$typecheck_out; ?> </p>
            </div>

            <div class="col s12 m6">
              <label>สาย </label>
              <p><?php echo $late." ชั่วโมง"; ?></p>
            </div>
            <div class="col s12 m6">
              <label>ล่วงเวลา </label>
              <p><?php echo ($bf_ot+$af_ot)." ชั่วโมง"; ?></p>
            </div>
            <br>
            <div class="col s12">
              <li class="divider"></li><br>
            </div>
            <div class="col s12 m6">
              <label>ช่องทางการลงเวลาเข้า </label>
              <p><?php echo $route_checkin; ?> </p>
            </div>
            <div class="col s12 m6">
              <label>ช่องทางการลงเวลาออก </label>
              <p><?php echo $route_checkout; ?> </p>

            </div>
          </div>
          <div class="col s12">
            <li class="divider"></li>
            <p>การอนุมัติ</p>

          </div>
          <div class="col s12">
            <p class='<?php echo $status["color"]; ?>'><?php echo $status["msg"]; ?></p>
          </div>
          <div class="col s12">
            <li class="divider"></li>
            <p>หมายเหตุ</p>

          </div>
          <div class="col s12">
            <p>
              <?php if ($tat["messageEmp_in"] == "" && $tat["messageEmp_out"] == "") {
                    echo " - ";
              }else if ($tat["messageEmp_in"] != ""){
                    echo " <p> <label>ลงเวลาเข้าย้อนหลังเนื่องด้วย  </label> <br /> ".$tat["messageEmp_in"]."</p>";
              }else if ($tat["messageEmp_out"] != "") {
                echo " <p> <label>ลงเวลาออกย้อนหลังเนื่องด้วย  </label> <br /> ".$tat["messageEmp_out"]."</p>";
              }?>
            </p>
          </div>
        </div>
      </div>
  </div>
</div>
<!--  AIzaSyCZKhEPx14ken6ISjjnQT0K4tZ7nlNaJjU -->
