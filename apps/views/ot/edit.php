<div id="otEdit" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="otEditLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="clearModals();">×</button>
          <h4 class="modal-title" id="otCreateLabel"> แก้ไขกลุ่มล่วงเวลา</h4>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-md-12">
            <div class="sttabs tabs-style-circle">
            <nav>
            <ul class='ot_tabs'>
                <li class='btn-step tab-current' data-phase="ot_detail" data-persent="0%" data-progress="danger"  id="ot_detail_tab"  data-toggle='site_detail'><a class="sticon fa fa-list" disabled="disabled"><span class='tabs-txt'>รายละเอียด</span></a></li>
                <li class='btn-step' data-phase="ot_excess" data-persent="50%"  data-progress="danger"  id="ot_excess_tab" data-toggle='site_location'><a class="sticon fa fa-plus-square"><span class='tabs-txt'>กำหนดส่วนเกิน</span></a></li>
                <li class='btn-step' data-phase="ot_multiple" data-persent="100%"  data-progress="success" id="ot_multiple_tab" data-toggle='site_working'><a class="sticon fa fa-list-alt"><span class='tabs-txt'>ล่วงเวลา</span></a></li>
                <!-- <li class='btn-step'  data-phase="ot_submit" data-persent="100%" data-progress="success"  id="ot_submit_tab" data-toggle='site_ot'><a class="sticon ti-save"><span class='tabs-txt'>บันทึกข้อมูล</span></a></li> -->
            </ul>
            </nav>
            <div class="ot_tabs_content content-wrap text-center">
                <div class="row tabs-progress">
                  <div class="col-md-12">
                    <div class="progress">
                       <div class="progress-bar progress-bar-danger" style="width: 0%;" role="progressbar"></div>
                     </div>
                  </div>
                </div>

                <section id="ot_detail" class='content-current'>
                    <div class="row">
                      <div class="col-md-12">
                        <div class="form-group text-left has-feedback" id="groupOteName_group">
                            <label for="groupOteName">ชื่อกลุ่มล่วงเวลา</label>
                            <input type="text" class="form-control validate" name="groupOteName" id="groupOteName" placeholder="กรอกชื่อกลุ่มล่วงเวลา">
                            <span class="form-control-feedback" id="groupOteName_feedback"></span>
                        </div>
                      </div>
                      <div class="col-md-12">
                        <hr>
                      </div>
                    </div>
                    <div class="row" align='left'>
                      <div class="col-md-12">
                          <h4>กำหนดเวลาคำนวนค่าล่วงเวลา</h4> <hr>
                      </div>
                      <div class="col-md-6">
                        <div class="form-group text-left has-feedback" id="groupOtBefore_group">
                            <label for="groupOteBefore">ก่อนเวลาทำงาน (นาที)</label>
                            <input type="text" class="form-control validate" name="groupOtBefore" id="groupOtBefore" placeholder="กรอกจำนวนนาที" value="0" onkeypress="return numberOnly(event);">
                            <span class="form-control-feedback" id="groupOtBefore_feedback"></span>
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="form-group text-left has-feedback" id="groupOtAfter_group">
                            <label for="groupOteAfter">หลังเวลาทำงาน (นาที)</label>
                            <input type="text" class="form-control validate" name="groupOtAfter" id="groupOtAfter" placeholder="กรอกจำนวนนาที" value="0" onkeypress="return numberOnly(event);">
                            <span class="form-control-feedback" id="groupOtAfter_feedback"></span>
                        </div>
                      </div>
                      <div class="col-md-12">
                          <div class="progress progress-lg">
                              <div role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 20%;" class="progress-bar progress-bar-success wow animated progress-animated"></div>
                              <div role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 10%;" class="progress-bar progress-bar-inverse"></div>
                              <div role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 40%;" class="progress-bar progress-bar-info"></div>
                              <div role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 10%;" class="progress-bar progress-bar-inverse"></div>
                              <div role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 20%;" class="progress-bar progress-bar-success"></div>
                          </div>
                      </div>
                      <br>
                      <div class="col-md-12 block-note">
                          <p><b><u>หมายเหตุ</u></b> <br>
                              <small>กำหนดการคำนวนโอทีก่อนและหลังเลิกงาน</small>
                              <ul>
                                <li><small>กรอกได้เฉพาะตัวเลขเท่านั้น</small> </li>
                                <li><small>หากต้องการให้คำนวนค่าล่วงเวลาทันทีหลังจากเวลางานให้กรอกหมายเลข ' 0 ' </small> </li>
                                <li><small>หากไม่มีการคำนวนค่าล่วงเวลาให้กรอกเครื่องหมาย ' - '</small> </li>
                              </ul>
                          </p>
                      </div>
                    </div>
                    <!-- <div class="row" align='left'>
                      <div class="col-md-12">
                          <h4>จำนวนการขอล่วงเวลา</h4> <hr>
                      </div>
                      <div class="col-md-6">
                        <div class="form-group text-left has-feedback" id="dateOtLimit_group">
                            <label for="groupOteBefore">จำนวนที่ขอได้ต่อวัน (ชม)</label>
                            <input type="text" class="form-control validate" name="dateOtLimit" id="dateOtLimit" placeholder="กรอกจำนวนนาที" value="0" onkeypress="return numberOnly(event);">
                            <span class="form-control-feedback" id="dateOtLimit_feedback"></span>
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="form-group text-left has-feedback" id="monthOtLimit_group">
                            <label for="groupOteAfter">จำนวนที่ขอได้ต่อเดือน (ชม)</label>
                            <input type="text" class="form-control validate" name="monthOtLimit" id="monthOtLimit" placeholder="กรอกจำนวนนาที" value="0" onkeypress="return numberOnly(event);">
                            <span class="form-control-feedback" id="monthOtLimit_feedback"></span>
                        </div>
                      </div>
                      <div class="col-md-12 block-note">
                          <p><b><u>หมายเหตุ</u></b> <br>
                              <small>กำหนดการกรอกจำนวนจำกัดของการขอค่าล่วงเวลา</small>
                              <ul>
                                <li><small>กรอกได้เฉพาะตัวเลขเท่านั้น</small> </li>
                                <li><small>หากต้องการกำหนดการขอได้ไม่จำกัดให้กรอกหมายเลข ' 0 ' </small> </li>
                              </ul>
                          </p>
                      </div>

                      <div class="col-md-12">
                        <hr>
                      </div>
                    </div> -->
                    <div class="row">
                      <div class="col-md-12 text-left">
                        <div class="checkbox checkbox-success">
                          <input id="check_beforeRequest" type="checkbox" name="check_beforeRequest">
                          <label for="check_beforeRequest">กำหนดขอล่วงหน้า</label>
                        </div>
                        <br>
                      </div>
                      <div class='beRequest' id="request_before">
                          <div class="col-md-12">
                            <div class="form-group text-left has-feedback" id="timeBeforeRequest_group">
                                <label for="timeBeforeRequest">ขอล่วงเวลาก่อน (นาที)</label>
                                <input type="text" class="form-control" name="timeBeforeRequest" id="timeBeforeRequest" placeholder="นาที"  onkeypress="return numberOnly(event);">
                                <span class="form-control-feedback" id="timeBeforeRequest_feedback"></span>
                            </div>
                          </div>
                          <div class="col-md-12 text-left">
                            <div class="checkbox checkbox-success">
                              <input id="urgent" type="checkbox">
                              <label for="urgent">สามารถขอเร่งด่วน</label>
                            </div>
                            <br>
                          </div>
                      </div>
                      <div class="col-md-12">
                        <hr>
                      </div>
                    </div>
                    <!-- <div class="row">
                      <div class="col-md-12 text-left">
                        <div class="checkbox checkbox-success">
                          <input id="check_approveRequest" type="checkbox" name="check_approveRequest">
                          <label for="check_approveRequest">มีการอนุมัติจ่าย</label>
                        </div>
                        <br>
                      </div> -->

                    <!-- class='beRequest' -->
                      <!-- <div class='beRequest' id="request_approve">
                        <div class="col-md-12">
                          <div class="form-group text-left has-feedback" id="paidDate_group">
                              <label for="paidDate">กำหนดวันจ่าย </label>
                              <input type="text" class="form-control" name="paidDate" id="paidDate" placeholder="กรอกวันที่จ่าย"  onkeypress="return numberOnly(event);">
                              <span class="form-control-feedback" id="paidDate_feedback"></span>
                          </div>
                        </div>
                      </div>
                      <div class="col-md-12">
                        <hr>
                      </div>
                    </div> -->

                    <div class="row">
                      <div class="col-md-12 text-left">
                        <div class="checkbox checkbox-success">
                          <input id="check_packageRequest" type="checkbox" name="check_packageRequest">
                          <label for="check_packageRequest">อนุญาตคำนวนค่าล่วงเวลาแบบเหมาจ่าย</label>
                        </div>
                        <br>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-12" align='right'>
                          <button class="fcbtn btn btn-outline btn-info btn-1d btn-step" data-phase="ot_excess" data-persent="50%">ถัดไป</button>
                      </div>
                    </div>
                </section>
                <!-- detail -->

                <section id="ot_excess" class='text-left'>
                    <h4>การกำหนดเศษนาที</h4>
                    <p>กำหนดเงื่อนไขในการคำนวนเศษนาที</p>
                    <table class='table' id="ExTable">
                      <thead>
                          <th width="200">เงื่อนไข</th>
                          <th width="125">เศษ (นาที)</th>
                          <th width="125">ปัดเป็น (นาที)</th>
                          <th width='50'>
                            <a class='btn btn-success pull-right m-l-20  btn-outline waves-effect waves-light btn-create-exc-row'><span class='fa fa-plus'></span></a>
                          </th>
                      </thead>
                      <tbody id="ExcBody">
                        <tr  class='text-center no-list' data-row='0'>
                        <td  colspan='4'>
                          <h5>ไม่พบรายการส่วนเกิน</h5>
                        </td>
                        </tr>
                      </tbody>
                    </table>
                    <br>
                    <div class="row">
                      <div class="col-md-12"  align='right'>
                          <button class="fcbtn btn btn-outline btn-default btn-1d btn-step" data-phase="ot_detail" data-persent="0%">ย้อนกลับ</button>
                          <button class="fcbtn btn btn-outline btn-info btn-1d btn-step" data-phase="ot_multiple" data-persent="100%" data-progress="success">ถัดไป</button>
                      </div>
                    </div>
                </section>
                <!-- excess -->
                <section id="ot_multiple"  class='text-left'>
                    <h4>กำหนดค่าล่วงเวลา</h4>
                    <p>ตัวคูณตามช่วงเวลาการทำงาน</p>
                    <div class="row">
                      <div class="col-md-12">
                        <table class='table'>
                          <tbody class='ExcBody'>
                              <tr>
                                <td width='150'>
                                  <p>วันทำงานปกติ</p>
                                </td>
                                <td>
                                  <div class="row">
                                    <div class="col-md-12">
                                      <div class='form-group has-feedback' id="positionWorking_text_group_ex">
                                        <label for="positionWorking_text_ex">เวลาทำงาน (นอกเวลา)</label>
                                        <input type="text" class='form-control' id='positionWorking_text_ex' name="positionWorking_text_ex" data-id='positionWorking_text_ex' value="1.0" placeholder="กรอกจำนวนคูณ (เท่า)" onkeypress="return numDecimal(event);">
                                        <span class="form-control-feedback" id="positionWorking_text_ex_feedback"></span>
                                      </div>
                                    </div>
                                  </div>
                                  <div class="form-group has-feedback" id="positionWorking_group">
                                    <label for="positionWorking">ตำแหน่งการอนุมัติ</label>
                                    <select class="form-control position_ot validate" name="positionWorking" id="positionWorking" data-id="positionWorking"></select>
                                    <span class="form-control-feedback" id="positionWorking_feedback"></span>
                                  </div>
                                  <div class="tag-position" id="positionWorking_tag"></div>

                                  <div class="form-group has-feedback" id="positionWorking_name">
                                    <label for="positionWorking">ผู้อนุมัติ</label>
                                    <select class="form-control emp_ot validate" name="positionWorkingName" id="positionWorkingName" data-id="positionWorkingName"></select>
                                    <span class="form-control-feedback" id="positionWorkingName_feedback"></span>
                                  </div>
                                  <div class="tag-position" id="positionWorkingName_tag"></div>
                                </td>
                              </tr>
                              <tr>
                                <td>
                                  <p>วันหยุดประจำสัปดาห์</p>
                                </td>
                                <td>
                                  <div class="row">
                                    <div class="col-md-6">
                                      <div class='form-group has-feedback' id="positionweekend_text_group">
                                        <label for="positionweekend_text">เวลาทำงาน (ปกติ)</label>
                                        <input type="text" class='form-control' name="positionweekend_text" id="positionweekend_text" data-id='positionweekend_text' value="1.0" placeholder="กรอกจำนวนคูณ (เท่า)" onkeypress="return numDecimal(event);">
                                        <span class="form-control-feedback" id="positionweekend_text_feedback"></span>
                                      </div>
                                    </div>
                                    <div class="col-md-6">
                                      <div class='form-group has-feedback' id="positionweekend_text_group_ex">
                                        <label for="positionweekend_text_ex">เวลาทำงาน (นอกเวลา)</label>
                                        <input type="text" class='form-control' name="positionweekend_text_ex" id="positionweekend_text_ex" data-id='positionweekend_text_ex' value="1.0" placeholder="กรอกจำนวนคูณ (เท่า)" onkeypress="return numDecimal(event);">
                                        <span class="form-control-feedback" id="positionweekend_text _ex_feedback"></span>
                                      </div>
                                    </div>
                                  </div>

                                  <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group has-feedback clockpicker" id="weekend_ot_start">
                                          <label for="weekend_ot_started">เวลาเริ่มงาน</label>
                                          <div class="input-group">
                                            <input type="text" class="form-control" name="weekend_ot_started" id="weekend_ot_started" value='' placeholder="เวลาเข้างานวันหยุด">
                                            <span class="form-control-feedback" id="weekend_ot_started_feedback"></span>
                                            <span class="input-group-btn">
                                              <button class="btn btn-default btn-outline btn-cancel-ot-time" data-id="weekend_ot_started" type="button"><span class='fa fa-remove'></span></button>
                                            </span>
                                          </div>
                                        </div>
                                      </div>
                                      <div class="col-md-6">
                                          <div class="form-group has-feedback clockpicker" id="weekend_ot_end">
                                            <label for="weekend_ot_started">เวลาเลิกงาน</label>
                                            <div class="input-group">
                                              <input type="text" class="form-control" name="weekend_ot_end" id="weekend_ot_ended" value='' placeholder="เวลาออกงานวันหยุด">
                                              <span class="form-control-feedback" id="weekend_ot_end_feedback"></span>
                                              <span class="input-group-btn">
                                                <button class="btn btn-default btn-outline btn-cancel-ot-time" data-id="weekend_ot_ended" type="button"><span class='fa fa-remove'></span></button>
                                              </span>
                                            </div>
                                          </div>
                                        </div>
                                  </div>

                                  <div class="form-group has-feedback" id="positionweekend_group">
                                    <label for="positionweekend">ตำแหน่งการอนุมัติ</label>
                                    <select class="form-control position_ot validate" name="positionweekend" id="positionweekend" data-id="positionweekend"></select>
                                    <span class="form-control-feedback" id="positionweekend_feedback"></span>
                                  </div>
                                  <div class="tag-position" id="positionweekend_tag"></div>

                                  <div class="form-group has-feedback" id="positionweekend_name">
                                    <label for="positionweekend">ผู้อนุมัติ</label>
                                    <select class="form-control emp_ot validate" name="positionweekendname" id="positionweekendName" data-id="positionweekendName"></select>
                                    <span class="form-control-feedback" id="positionweekendname_feedback"></span>
                                  </div>
                                  <div class="tag-position" id="positionweekendName_tag"></div>
                                </td>
                              </tr>
                              <tr>
                                <td>
                                  <p>วันหยุดประจำปี</p>
                                </td>
                                <td>
                                  <div class="row">
                                    <div class="col-md-6">
                                      <div class='form-group  has-feedback' id="positionHoliday_text_group">
                                        <label for="positionHoliday_text">เวลาทำงาน (ปกติ)</label>
                                        <input type="text" class='form-control' id="positionHoliday_text" name="positionHoliday_text" value="1.0" placeholder="กรอกจำนวนคูณ (เท่า)" data-id='positionHoliday_text' onkeypress="return numDecimal(event);">
                                        <span class="form-control-feedback" id="positionHoliday_text_feedback"></span>
                                      </div>
                                    </div>
                                    <div class="col-md-6">
                                      <div class='form-group  has-feedback' id="positionHoliday_text_group_ex">
                                        <label for="positionHoliday_text_ex">เวลาทำงาน (นอกเวลา)</label>
                                        <input type="text" class='form-control' id="positionHoliday_text_ex" name="positionHoliday_text_ex" value="1.0" placeholder="กรอกจำนวนคูณ (เท่า)" data-id='positionHoliday_text_ex' onkeypress="return numDecimal(event);">
                                        <span class="form-control-feedback" id="positionHoliday_text _ex_feedback"></span>
                                      </div>
                                    </div>
                                  </div>
                                  <div class="form-group has-feedback" id="positionHoliday_group">
                                    <label for="positionHoliday">ตำแหน่งการอนุมัติ</label>
                                    <select class="form-control position_ot validate" name="positionHoliday" id="positionHoliday" data-id="positionHoliday">

                                    </select>
                                    <span class="form-control-feedback" id="positionHoliday_feedback"></span>
                                  </div>
                                  <div class="tag-position" id="positionHoliday_tag"></div>

                                  <div class="form-group has-feedback" id="positionHoliday_name">
                                    <label for="positionHoliday">ผู้อนุมัติ</label>
                                    <select class="form-control emp_ot validate" name="positionHolidayName" id="positionHolidayName" data-id="positionHolidayName">

                                    </select>
                                    <span class="form-control-feedback" id="positionHolidayName_feedback"></span>
                                  </div>
                                  <div class="tag-position" id="positionHolidayName_tag"></div>

                                </td>
                              </tr>
                          </tbody>
                        </table>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-12"  align='center'>
                          <button class="fcbtn btn btn-outline btn-default btn-1d btn-step" data-phase="ot_excess" data-persent="50%" data-progress="danger">ย้อนกลับ</button>
                          <button class="fcbtn btn btn-outline btn-success btn-1d btn-ot-submit" data-persent="100%">บันทึกข้อมูล</button>
                      </div>
                    </div>
                </section>
                <!-- multiple -->
            </div>
            <!-- /content -->
            </div>
          </div>

        </div>

      </div>
    </div>
  </div>
</div>
