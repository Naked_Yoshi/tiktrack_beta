<div class='white-box'>
    <section>
        <div class="row">
            <div class="col-md-12">
                <!-- <a class='btn btn-success pull-right m-l-20 btn-rounded btn-outline hidden-xs hidden-sm waves-effect waves-light btn-create-site'>เพิ่มไซต์งาน</a> -->
                <h4>รายงาน : บันทึกลงเวลา</h4>
                <hr>
            </div>
        </div>


        <div class="row">

            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <table class='table table-bordered  table-striped report_attendance_table'>
                    <thead>
                        <tr>
                        <th width='50' class='text-center'>ลำดับ</th>
                        <th>ชื่อรายงาน</th>
                        <th width='150' class='text-center'>ตัวเลือก</th>
                        </tr>
                    </thead>
                    <tbody id='report_attendance_body'>
                        <tr>
                            <td class='text-center'>1</td>
                            <td>รายงานสรุปการลงเวลา</td>
                            <td class='text-center'><button class="btn btn-info" data-toggle="modal" data-target="#reportModal"><span class='fa fa-print'></span> ปริ้นรายงาน</button></td>
                        </tr>
                    </tbody>
                </table>
            </div>

        </div>

    </section>
</div>


<div class="modal fade" id="reportModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1">

<div class="modal-dialog" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="exampleModalLabel1">พิมพ์รายงาน</h4>
        </div>
        <div class="modal-body">
          <!-- <form class='report_form' method="post" action='<?php echo base_url('attendance/previewReport'); ?>'> -->
            <form class='report_form' method="post" target="_blank" action='<?php echo base_url('attendance/previewReport'); ?>'>
            <input type="hidden" name="company_id" id="company_id" value="">
            <input type="hidden" name="index_emp" id="index_emp" value="">
            <input type="hidden" name="index_site" id="index_site" value="">
            <input type="hidden" name="index_branche" id="index_branche" value="">
            <input type="hidden" name="index_dep" id="index_dep" value="">
              <div class="row">
                <?php $month = array("มกราคม","กุมภาพันธ์","มีนาคม","เมษายน","พฤษภาคม","มิถุนายน","กรกฎาคม","สิงหาคม","กันยายน","ตุลาคม","พฤศจิกายน","ธันวาคม"); ?>
                <div class='form-group col-md-6' id="report_month_type">
                <label>เดือน</label>
                <div class="input-field custom sm" >
                <select class="form-control" name="report_searchmonth" id="report_selectsearchmonth">
                  <?php for ($i=0; $i < count($month); $i++) { $num = $i+1; ?>
                    <option value="<?php echo $num ?>"><?php echo $month[$i]; ?></option>
                  <?php } ?>
                </select>
                </div>
                </div>
                <div class='form-group col-md-6' >
                  <label>ปี</label>
                  <div class="input-field custom sm"  id="report_searchyear">
                  <select class="form-control" name="report_searchyear" id="report_selectsearchyear">
                    <?php for ($i=0; $i < 10; $i++) { $year = 2018+$i; ?>
                      <option value="<?php echo $year; ?>"><?php echo $year; ?></option>
                    <?php } ?>
                  </select>
                  </div>
                </div>

              </div>

              <div class="row">
                <div class="col s12">
                  <hr>
                </div>
                <div class="col s6">
                  <p><span class='fa fa-file-o'></span> ประเภทการพิมพ์รายงาน : </p>
                </div>
                <div class='form-group col-md-6' id="report_year_type">
                  <div class="input-field custom sm"  id="report_selectType">
                  <select class="form-control" name="report_selectType" id="report_Type">
                      <option value="0">พิมพ์รายบุคคล</option>
                      <option value="1">พิมพ์ตามไซต์งาน</option>
                      <!-- <option value="2">พิมพ์ตามแผนก</option> -->
                  </select>
                  </div>
                </div>
              </div>

              <div class="row" id="by_person">
                <div class="col s12">
                   <label for="search_user_report">ค้นหาชื่อพนักงาน</label>
                   <form onsubmit="$('#search_user_report').blur();return false;" class="pure-form" style="padding-bottom:10px;">
                     <input id="search_user_report" type="text" name="q" placeholder="ชื่อ หรือนามสกุล" class="form-control">
                   </form>
                </div>
                <!-- <div class="col s12">
                    <br>
                    <label>ข้อมูลที่เลือก</label>
                    <p id='getname'> - </p>
                    <label>ไซต์</label>
                    <p id='getsite'> - </p>
                    <label>สาขา</label>
                    <p id='getbranch'> - </p>
                    <hr>
                </div> -->
                <input type="hidden" id='index' name="index" value="">
              </div>

              <div class="row" id="by_site" style="display:none;">
                <div class="col-md-6">
                  <label>ไซต์ *</label>
                  <div class="input-field custom sm"  id="report_site">
                      <select class="form-control" name="r_site" id="r_site">
                      </select>
                  </div>
                  <label class='feedback' id="feedback-site"></label>
                </div>
                <div class="col-md-6">
                  <label>สาขา *</label>
                  <div class="input-field custom sm"  id="report_branch">
                    <select class="form-control" name="r_branche" id="r_branche">
                      <option value="" disabled selected>เลือกรายการ </option>
                    </select>
                  </div>
                  <label class='feedback' id="feedback-branch"></label>
                </div>
              </div>

              <div class="row" id="by_dep" style="display:none;">
                <div class="col s12">
                  <div class="input-field custom sm"  id="report_dep">
                  <select class="form-control" name="r_dep" id="r_dep">
                      <option value="">เลือกแผนก</option>
                  </select>
                  </div>
                </div>
              </div>
        </form>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-primary btn-report-attendance">พิมพ์ข้อมูล</button>
        </div>
    </div>
</div>

</div>