<div class="white-box">
    <div class="vtabs">
        <ul class="nav tabs-vertical">
            <li class="tab active">
                <a data-toggle="tab" href="#common" aria-expanded="true"> <span class="visible-xs"><i class="ti-home"></i></span> <span class="hidden-xs">โมดูลทั่วไป</span> </a>
            </li>
            <li class="tab">
                <a data-toggle="tab" href="#profile3" aria-expanded="false"> <span class="visible-xs"><i class="ti-user"></i></span> <span class="hidden-xs">ขาด ลา สาย</span> </a>
            </li>
            <!-- <li class="tab">
                <a aria-expanded="false" data-toggle="tab" href="#messages3"> <span class="visible-xs"><i class="ti-email"></i></span> <span class="hidden-xs">Messages</span> </a>
            </li> -->
        </ul>
        <div class="tab-content">
            <div id="common" class="tab-pane active">
              <div class="row">
                <a class='btn btn-success pull-right m-l-20 btn-outline hidden-xs hidden-sm waves-effect waves-light btn-save-setting'>บันทึกการตั้งค่า</a>
              </div>
              <br>
              <div class="row">
                <div class="col-md-6">
                  <label for="eslip">อนุญาติเข้าถึง E-slip
                    <span class="fa fa-info-circle" title="เปิดฟังก์ชัน E-Slip สามารถดูสลิปเงินเดือนได้ผ่านมือถือ" data-toggle="tooltip"></span>
                    </label>

                </div>
                <div class="col-md-6 text-right">
                  ปิด <input type="checkbox" id="cb_eslip"  name="cb_eslip" class="js-switch" data-color="#99d683" /> เปิด
                </div>
              </div>
              <hr>
              <div class="row">
                <div class="col-md-6">
                  <label for="allow_ot">Allow OT</label>
                  <span class="fa fa-info-circle" title="อนุญาตืให้เปิดโมดูลล่วงเวลา" data-toggle="tooltip"></span>
                </div>
                <div class="col-md-6 text-right">
                  ปิด <input type="checkbox" id="allow_ot" name="allow_ot" class="js-switch" data-color="#99d683" /> เปิด
                </div>
              </div>
              <hr>
              <div class="row">
                <div class="col-md-6">
                  <label for="auto_replace">Auto Replace</label>
                  <span class="fa fa-info-circle" title="อนุญาตืให้เปิดใบขอคนแทนทันที" data-toggle="tooltip"></span>
                </div>
                <div class="col-md-6 text-right">
                  ปิด <input type="checkbox" id="auto_replace" name="auto_replace" class="js-switch" data-color="#99d683" /> เปิด
                </div>
              </div>
              <hr>
                <div class="clearfix"></div>
            </div>
            <div id="profile3" class="tab-pane">
                <p>Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt.Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim.</p>
            </div>
            <div id="messages3" class="tab-pane">
                <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim.</p>
            </div>
        </div>
    </div>
</div>
