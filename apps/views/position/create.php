<div id="positionCreate" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="positionCreateLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="clearModals();">×</button>
          <h4 class="modal-title" id="positionCreateLabel">  เพิ่มตำแหน่ง</h4>
      </div>
      <div class="modal-body">
        <form id="positionCreateForm">
          <div class='row'>
            <div class="col-md-12">
              <div class="form-group has-feedback" id="positionName_group">
                <label for="positionName">ชื่อตำแหน่ง</label>
                <input type="text" class="form-control validate" name="positionName" id="positionName" placeholder="กรอกชื่อตำแหน่ง">
                <span class="form-control-feedback" id="positionName_feedback"></span>
              </div>
            </div>
            <div class="col-md-12">
              <div class="form-group has-feedback" id="positionDep_group">
                <label for="positionDep">แผนก</label>
                <select class="form-control validate" name="dep" id="positionDep">

                </select>
                <span class="form-control-feedback" id="positionDep_feedback"></span>
              </div>
            </div>
            <div class="col-md-12">
              <div class="form-group has-feedback" id="positionRoot_group">
                <label for="positionRoot">ตำแหน่งสูงกว่า</label>
                <select class="form-control validate" name="root" id="positionRoot">
                  <option>Default select</option>
                </select>
                <span class="form-control-feedback" id="positionRoot_feedback"></span>
              </div>
            </div>
            <div class="col-md-12">
                <table class='table table-bordered table-striped'>
                  <thead>
                      <th>สิทธิ์การเข้าถึง</th>
                      <th class='text-center' width='75'>เพิ่ม</th>
                      <th class='text-center' width='75'>แก้ไข</th>
                      <th class='text-center' width='75'>ลบ</th>
                      <th class='text-center' width='75'>อนุมัติ</th>
                  </thead>
                  <tbody>
                    <tr>
                      <td>ไซต์งาน</td>
                      <td  class='text-center'>
                        <div class="checkbox checkbox-danger">
                            <input id="siteAdd" name="siteAdd" type="checkbox" class="fxsdr">
                            <label for="siteAdd"></label>
                        </div>
                      </td>
                      <td  class='text-center'>
                        <div class="checkbox checkbox-danger">
                            <input id="siteEdit"  name="siteEdit" type="checkbox" class="fxsdr">
                            <label for="siteEdit"></label>
                        </div>
                      </td>
                      <td  class='text-center'>
                        <div class="checkbox checkbox-danger">
                            <input id="siteDel" name="siteDel" type="checkbox" class="fxsdr">
                            <label for="siteDel"></label>
                        </div>
                      </td>
                      <td  class='text-center'>
                        <div class="checkbox checkbox-danger">
                            <input id="siteApprove" name="siteApprove" type="checkbox" class="fxsdr">
                            <label for="siteApprove"></label>
                        </div>
                      </td>
                    </tr>
                    <tr>
                      <td>สาขา</td>
                      <td  class='text-center'>
                        <div class="checkbox checkbox-danger">
                            <input id="brancheAdd"  name="brancheAdd" type="checkbox" class="fxsdr">
                            <label for="brancheAdd"></label>
                        </div>
                      </td>
                      <td  class='text-center'>
                        <div class="checkbox checkbox-danger">
                            <input id="brancheEdit" name="brancheEdit" type="checkbox" class="fxsdr">
                            <label for="brancheEdit"></label>
                        </div>
                      </td>
                      <td  class='text-center'>
                        <div class="checkbox checkbox-danger">
                            <input id="brancheDel" name="brancheDel" type="checkbox" class="fxsdr">
                            <label for="brancheDel"></label>
                        </div>
                      </td>
                      <td  class='text-center'>
                        <div class="checkbox checkbox-danger">
                            <input id="brancheApprove" name="brancheApprove" type="checkbox" class="fxsdr">
                            <label for="brancheApprove"></label>
                        </div>
                      </td>
                    </tr>
                    <tr>
                      <td>OT</td>
                      <td  class='text-center'>
                        <div class="checkbox checkbox-danger">
                            <input id="otAdd"  name="otAdd" type="checkbox" class="fxsdr">
                            <label for="otAdd"></label>
                        </div>
                      </td>
                      <td  class='text-center'>
                        <div class="checkbox checkbox-danger">
                            <input id="otEdit" name="otEdit" type="checkbox" class="fxsdr">
                            <label for="otEdit"></label>
                        </div>
                      </td>
                      <td  class='text-center'>
                        <div class="checkbox checkbox-danger">
                            <input id="otDel" name="otDel" type="checkbox" class="fxsdr">
                            <label for="otDel"></label>
                        </div>
                      </td>
                      <td  class='text-center'>
                        <div class="checkbox checkbox-danger">
                            <input id="otApprove" name="otApprove" type="checkbox"  class="fxsdr">
                            <label for="otApprove"></label>
                        </div>
                      </td>
                    </tr>
                    <tr>
                      <td>พนักงาน</td>
                      <td  class='text-center'>
                        <div class="checkbox checkbox-danger">
                            <input id="userAdd"  name="userAdd" type="checkbox" class="fxsdr">
                            <label for="userAdd"></label>
                        </div>
                      </td>
                      <td  class='text-center'>
                        <div class="checkbox checkbox-danger">
                            <input id="userEdit" name="userEdit" type="checkbox" class="fxsdr">
                            <label for="userEdit"></label>
                        </div>
                      </td>
                      <td  class='text-center'>
                        <div class="checkbox checkbox-danger">
                            <input id="userDel" name="userDel" type="checkbox" class="fxsdr">
                            <label for="userDel"></label>
                        </div>
                      </td>
                      <!-- <td  class='text-center'>
                        <div class="checkbox checkbox-danger">
                            <input id="brancheApprove" name="brancheApprove" type="checkbox"  class="fxsdr">
                            <label for="brancheApprove"></label>
                        </div>
                      </td> -->
                    </tr>
                    <tr>
                    <td>ลงเวลา</td>
                    <td  class='text-center'>
                        <div class="checkbox checkbox-danger">
                            <input id="checkinout"  name="checkinout" type="checkbox" class="fxsdr">
                            <label for="checkinout"></label>
                        </div>
                      </td>
                    </tr>
                  </tbody>
                </table>
            </div>
          </div>
        </form>
      </div>
      <div class="modal-footer">
      <button type="button" class="btn btn-success waves-effect submit-create-position">บันทึกข้อมูล</button>
      <button type="button" class="btn btn-info waves-effect" data-dismiss="modal" onclick="clearModals();">ปิดหน้าต่าง</button>
      </div>
    </div>
  </div>
</div>
