		<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<div class="row">
    <div class="col-lg-12">

          <div class="sttabs tabs-style-iconbox">
              <nav>
                  <ul>
                      <li><a href="#section-iconbox-1" class="sticon fa fa-sitemap"><span class='sticon-text'>ผังองค์กร</span></a></li>
                      <li><a href="#section-iconbox-2" class="sticon fa fa-building"><span class='sticon-text'>แผนก</span></a></li>
                      <li><a href="#section-iconbox-3" class="sticon fa fa-user"><span class='sticon-text'>ตำแหน่ง</span></a></li>
                  </ul>
              </nav>
              <div class="white-box">
                <div class="content-wrap">
                    <section id="section-iconbox-1">
                        <!-- <center><h4 class="modal-title ">แสดงแผนผัง</span></h4></center> -->
                      <!-- แสดงแผนผัง -->
                              <div class=''  id="org" >
                                <div class="row">
                                  <div class="col-md-12">
                                    <div class="container-chart">
																			<center>
                                      <div class="" id="chart_div"></div>
																		</center>
                                    </div>
                                  </div>
                                </div>
                              </div>
                              <!-- แสดงแผนผัง -->
                    </section>
                    <section id="section-iconbox-2">
                      <div class="row">
                        <div class="col-md-12">
                          <a class='btn btn-success pull-right m-l-20 btn-rounded btn-outline hidden-xs hidden-sm waves-effect waves-light btn-create-dep'>เพิ่มแผนก</a>
                            <!-- <a class='btn btn-success pull-right m-l-20 btn-rounded btn-outline hidden-xs hidden-sm waves-effect waves-light btn-edit-dep'>แก้ไขแผนก</a> -->
                          <h4>ข้อมูลแผนก</h4>
                          <hr>
                        </div>
                      </div>
                      <div class="row">
                          <div class="col-md-12">
                              <div class="table-responsive">
                                <table class='table table-bordered   table-striped departments_table'>
                                  <thead>
                                    <th  class='text-center' >ลำดับ</th>
                                    <th>ชื่อแผนก</th>
                                    <th>คำอธิบาย</th>
                                    <th>จำนวนพนักงาน</th>
                                    <th>ตัวเลือก</th>
                                  </thead>
                                  <tbody id='departments_body'>

                                  </tbody>
                                </table>
                              </div>
                          </div>
                      </div>
                    </section>
                    <section id="section-iconbox-3">
                      <div class="row">
                        <div class="col-md-12">
                          <a class='btn btn-success pull-right m-l-20 btn-rounded btn-outline hidden-xs hidden-sm waves-effect waves-light btn-create-position'>เพิ่มตำแหน่ง</a>
                          <h4>ข้อมูลตำแหน่ง</h4>
                          <hr>
                        </div>
                      </div>
                      <div class="row">
                          <div class="col-md-12">
                              <div class="table-responsive">
                                <table class='table table-bordered  table-striped position_table' id='position_table'>
                                  <thead>
                                    <th  class='text-center' >ลำดับ</th>
                                    <th>ชื่อตำแหน่ง</th>
                                    <th>แผนก</th>
                                    <th>หัวหน้า</th>
                                    <th>ตัวเลือก</th>
                                  </thead>
                                  <tbody id='position_body'>

                                  </tbody>
                                </table>
                              </div>
                              <!-- new -->


<!-- /new -->


                          </div>
                      </div>
                      </section>
                </div>
              </div>


              <!-- /content -->

          </div>
          <!-- /tabs -->
    </div>
</div>
