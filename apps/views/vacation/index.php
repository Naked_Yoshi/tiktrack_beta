<div class="white-box">
      <section>
        <div class="row">
          <div class="col-md-12">
            <a class='btn btn-success pull-right m-l-20 btn-rounded btn-outline hidden-xs hidden-sm waves-effect waves-light btn-create-vcation'>เพิ่มแบบการลา</a>
            <a class='btn btn-info pull-right m-l-20 btn-rounded btn-outline hidden-xs hidden-sm waves-effect waves-light btn-create-vcation_type'>ประเภทการลา</a>
            <h4>ข้อมูลแบบการลา</h4>
            <hr>
          </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <!-- <div class="table-responsive"> -->
                  <table class='table table-bordered  table-striped vacation_table'>
                    <thead>
                      <th  width='50' class='text-center' >ลำดับ</th>
                      <th>ชื่อแบบ</th>
                      <th width='150'>ตัวเลือก</th>
                    </thead>
                    <tbody id='vacation_body'>

                    </tbody>
                  </table>
                <!-- </div> -->
            </div>
        </div>
        </section>
</div>
