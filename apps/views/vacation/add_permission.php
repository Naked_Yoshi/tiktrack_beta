<div id="permission" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                  <div class="modal-dialog">
                      <div class="modal-content">
                          <div class="modal-header">
                              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                              <h4 class="modal-title" id="myModalLabel">เพิ่มผู้อนุมัติ <span id="lv"></span></h4>
                          </div>
                          <div class="modal-body">
                            <label for="list-approve">ผู้อนุมัติ</label>
                            <form onsubmit="$('#list-approve').blur();return false;" class="pure-form" style="padding-bottom:10px;">
                              <input id="list-approve" type="text" name="q" placeholder="ชื่อ หรือนามสกุล" class="form-control">
                            </form>
                          </div>
                          <div class="modal-footer">
                              <button type="button" id="btn_approve" class="btn btn-success waves-effect">Add</button>
                              <button type="button" class="btn btn-danger waves-effect" data-dismiss="modal">Close</button>
                          </div>
                      </div>
                      <!-- /.modal-content -->
                  </div>
                  <!-- /.modal-dialog -->
              </div>
