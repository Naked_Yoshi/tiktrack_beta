<div id="VacationTypeModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title" id="myModalLabel">เพิ่มประเภทการลา</h4>
            </div>
            <div class="modal-body">
                <div class="row" style="padding:20px">
                  <table class='table' id="ExTable">
                    <thead>
                        <th width="200">เงื่อนไข</th>
                        <th width='50'>
                          <a class='btn btn-success pull-right m-l-20  btn-outline waves-effect waves-light btn-create-type-vac-row'><span class='fa fa-plus'></span></a>
                        </th>
                    </thead>
                    <tbody id="ExcBody">
                      <tr  class='text-center no-list' data-row='0'>
                      <td  colspan='2'>
                        <h5>ไม่พบรายการ</h5>
                      </td>
                      </tr>
                    </tbody>
                  </table>
                </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-success waves-effect btn-save-type-leave" >Save</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
