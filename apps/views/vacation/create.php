<div id="CVacationModal" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title" id="myModalLabel">เพิ่มแบบการลา</h4>
            </div>
            <div class="modal-body">
                <div class="row" style="padding:20px">
                  <div class="form-group">
                      <label for="va_name">ชื่อแบบการลา</label>
                      <input type="text" class="form-control" id="va_name" placeholder="ชื่อแบบการลา">
                  </div>
                </div>
                <div class="row" style="padding:20px">
                  <table class='table' id="ExTable">
                    <thead>
                        <th width="200">เงื่อนไข</th>
                        <th width="50" class="text-center">แจ้งล่วงหน้า (ชั่วโมง)</th>
                        <th width="120" class="text-center">จำกัดเพศ</th>
                        <th width="50" class="text-center">หักเงิน</th>
                        <!-- <th width="50" class="text-center">ใช้สิทธิ์ได้<br>1 ครั้ง</th> -->
                        <th width="50" class="text-center">ลาฉุกเฉิน</th>
                        <th width="50" class="text-center">ย้อนหลัง</th>
                        <!-- <th width="50" class="text-center"></th> -->
                        <th width='50'>
                          <a class='btn btn-success pull-right m-l-20  btn-outline waves-effect waves-light btn-create-vac-row'><span class='fa fa-plus'></span></a>
                        </th>
                    </thead>
                    <tbody id="ExcBody">
                      <tr  class='text-center no-list' data-row='0'>
                      <td  colspan='7'>
                        <h5>ไม่พบรายการ</h5>
                      </td>
                      </tr>
                    </tbody>
                  </table>
                </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-success waves-effect btn-save-leave">Save</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
