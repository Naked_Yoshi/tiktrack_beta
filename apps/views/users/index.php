<div class="white-box">
      <section>
        <div class="row">
          <div class="col-md-12">
            <a class='btn btn-success pull-right m-l-20 btn-rounded btn-outline hidden-xs hidden-sm waves-effect waves-light btn-create-user'>เพิ่มพนักงาน</a>
            <h4>จัดการพนักงาน</h4>
            <hr>
          </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <!-- <div class="table-responsive"> -->
                  <table class='table table-bordered  table-striped site_table' id="users_table">
                    <thead>
                      <th  width='50' class='text-center' >ลำดับ</th>
                      <th>ชื่อ - นามสกุล</th>
                      <th>เบอร์โทรศัพท์</th>
                      <th>ไซต์งาน</th>
                      <th>แผนก</th>
                      <th>ตำแหน่ง</th>
                      <th width='150'>ตัวเลือก</th>
                    </thead>
                    <tbody id='user_body'>

                    </tbody>
                  </table>
                <!-- </div> -->
            </div>
        </div>
        </section>
</div>
