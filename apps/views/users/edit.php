<div class="white-box">
  <ul class="nav customtab nav-tabs" role="tablist">
      <li role="presentation" class="active"><a href="#home1" aria-controls="home" role="tab" data-toggle="tab" aria-expanded="true"><span class="visible-xs"><i class="ti-home"></i></span><span class="hidden-xs"> ข้อมูลพนักงาน</span></a></li>
      <!-- <li role="presentation" class="disabled"><a href="#profile1" aria-controls="profile" role="tab" data-toggle="tab" aria-expanded="false"><span class="visible-xs"><i class="ti-user"></i></span> <span class="hidden-xs">สัญญาจ้าง</span></a></li>
      <li role="presentation" class="disabled"><a href="#messages1" aria-controls="messages" role="tab" data-toggle="tab" aria-expanded="false"><span class="visible-xs"><i class="ti-email"></i></span> <span class="hidden-xs">ประวัติการใช้งาน</span></a></li>
      <li role="presentation" class="disabled"><a href="#settings1" aria-controls="settings" role="tab" data-toggle="tab" aria-expanded="false"><span class="visible-xs"><i class="ti-settings"></i></span> <span class="hidden-xs">เอกสาร</span></a></li> -->
  </ul>

  <div class="tab-content">
      <div role="tabpanel" class="tab-pane fade active in" id="home1">
          <div class="col-md-4">
            <div class="text-center"><img src="<?php echo base_url("assets/plugins/images/users/varun.jpg"); ?>" alt="user-img" class="img-circle"></div>
            <div class="user-btm-box">
                <!-- .row -->
                <div class="row text-center m-t-10">
                    <div class="col-md-6 b-r"><strong>ชื่อพนักงาน</strong>
                        <p class="emp_name">Genelia Deshmukh</p>
                    </div>
                    <div class="col-md-6"><strong>ตำแหน่ง</strong>
                        <p class="emp_position">Designer</p>
                    </div>
                </div>
                <!-- /.row -->
                <hr>
                <!-- .row -->
                <div class="row text-center m-t-10">
                    <div class="col-md-6 b-r"><strong>Email</strong>
                        <p class="emp_email">genelia@gmail.com</p>
                    </div>
                    <div class="col-md-6"><strong>เบอร์โทรศัพท์</strong>
                        <p class="emp_phone">+123 456 789</p>
                    </div>
                </div>
                <!-- /.row -->
                <hr>
                <!-- .row -->
                <div class="row text-center m-t-10">
                      <button  class="btn btn-block btn-primary btn-rounded btn-change-phone">ปลดล๊อคอุปกรณ์</button>
                </div>
                <!-- /.row -->

            </div>
          </div>
          <div class="col-md-8 pull-right">
            <div class="row">
              <div class="col-md-12">
                <h4>ข้อมูลส่วนตัว</h4>
                <hr>
              </div>
            </div>

            <!-- // row -->
            <div class="row">
              <div class="col-md-4">
                <div class="form-group">
                  <label for="usrcreateTitle">คำนำหน้า</label>
                  <select class="form-control" name="title" id="usrcreateTitle"></select>
                </div>
              </div>
              <div class="col-md-4">
                <div class="form-group">
                  <label for="usrcreateFname ">ชื่อจริง</label>
                  <input type="text" class="form-control validate" name="fname" id="usrcreateFname" placeholder="กรอกชื่อจริง">
                </div>
              </div>
              <div class="col-md-4">
                <div class="form-group">
                  <label for="usrcreateLname">นามสกุล</label>
                  <input type="text" class="form-control validate" name="lname" id="usrcreateLname" placeholder="กรอกนามสกุล">
                </div>
              </div>
            </div>
            <!-- // row -->
            <div class="row">
              <div class="col-md-4">
                <div class="form-group">
                  <label for="usrcreateTitle_en">คำนำหน้า (ภาษาอังกฤษ)</label>
                  <input type="text" class="form-control" name="title_en" id="usrcreateTitle_en" placeholder="คำนำหน้าภาษาอังกฤษ" readonly>
                </div>
              </div>
              <div class="col-md-4">
                <div class="form-group">
                  <label for="usrcreateFname_en">ชื่อจริง (ภาษาอังกฤษ)</label>
                  <input type="text" class="form-control validate" name="fname_en" id="usrcreateFname_en" placeholder="กรอกชื่อจริงภาษาอังกฤษ">
                </div>
              </div>
              <div class="col-md-4">
                <div class="form-group">
                  <label for="usrcreateLname_en">นามสกุล (ภาษาอังกฤษ)</label>
                  <input type="text" class="form-control validate" name="lname_en" id="usrcreateLname_en" placeholder="กรอกสกุลภาษาอังกฤษ">
                </div>
              </div>
            </div>
            <!-- // row -->
            <div class="row">
              <div class="col-md-4">
                <div class="form-group">
                  <label for="usrcreateSex">เพศ</label>
                  <select class="form-control" name="sex" id="usrcreateSex">
                      <option value="M">ชาย</option>
                      <option value="F">หญิง</option>
                   </select>
                </div>
              </div>
              <div class="col-md-4">
                <div class="form-group">
                  <label for="usrcreateBdate">วันเกิด</label>
                  <input type="text" class="form-control validate" placeholder="dd/mm/yyyy" name="bdate" id="usrcreateBdate" data-mask="99/99/9999">
                </div>
              </div>
              <div class="col-md-4">
                <div class="form-group">
                  <label for="usrcreatePersonId">รหัสบัตรประชาชน</label>
                  <input type="text" class="form-control validate" name="personId" id="usrcreatePersonId" placeholder="กรอกรหัสบัตรประชาชน" data-mask="9-9999-99999-99-9">
                </div>
              </div>
            </div>
            <!-- // row -->
            <div class="row">
              <div class="col-md-4">
                <div class="form-group">
                  <label for="usrcreateMarried">สถานะภาพการสมรส</label>
                  <select class="form-control" name="married" id="usrcreateMarried">
                    <option value="0">Default select</option>
                    <option value="1">โสด</option>
                    <option value="2">สมรส</option>
                    <option value="3">หย่าร้าง</option>
                   </select>
                </div>
              </div>
              <div class="col-md-4">
                <div class="form-group">
                  <label for="usrcreateNation">สัญชาติ</label>
                  <select class="form-control" name="nation" id="usrcreateNation">
                    <!-- <option>Default select</option> -->
                   </select>
                </div>
              </div>
              <div class="col-md-4">
                <div class="form-group">
                  <label for="usrcreatePersonId">เบอร์โทรศัพท์</label>
                  <input type="text" class="form-control validate" name="phone" id="usrcreatephone" placeholder="เบอร์โทรศัพท์" data-mask="999-999-9999">
                </div>
              </div>
            </div>

            <div class="row">
              <div class="col-md-12">
                <h4>การทำงาน</h4>
                <hr>
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <label for="usrcreateDep">เลือกแผนก</label>
                <select class="form-control" name="dep" id="usrcreateDep">

                 </select>
              </div>
            </div>
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label for="usrcreatePosition">เลือกตำแหน่ง</label>
                  <select class="form-control" name="position" id="usrcreatePosition">
                    <!-- <option>Default select</option> -->
                   </select>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-4">
                <div class="form-group">
                  <label for="usrcreateSalary">เงินเดือน</label>
                  <input type="text" class="form-control validate" name="salary" id="usrcreateSalary" placeholder="กรอกเงินเดือน">
                </div>
              </div>
              <div class="col-md-4">
                <div class="form-group">
                  <label for="usrcreateStart">วันที่เริ่มงาน</label>
                  <input type="text" class="form-control mydatepicker validate" placeholder="dd/mm/yyyy" name="start" id="usrcreateStart" data-mask="dd/mm/yyyy">
                </div>
              </div>
              <div class="col-md-4">
                <div class="form-group">
                  <label for="usrcreateHoliday">วันหยุดพนักงาน</label>
                  <input type="text" class="form-control" name="holiday" id="usrcreateHoliday" placeholder="กรอกเงินวันหยุดพนักงาน">
                </div>
              </div>
            </div>
            <div class="col-xs-12" style="padding:0px">
                <button class="btn btn-block btn-success btn-update-emp">บันทึก</button>
            </div>
          </div>
          <div class="clearfix"></div>
      </div>
      <div role="tabpanel" class="tab-pane fade" id="profile1">
          <div class="col-md-6">
              <h3>Lets check profile</h3>
              <h4>you can use it with the small code</h4>
          </div>
          <div class="col-md-5 pull-right">
              <p>Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a.</p>
          </div>
          <div class="clearfix"></div>
      </div>
      <div role="tabpanel" class="tab-pane fade" id="messages1">
          <div class="col-md-6">
              <h3>Come on you have a lot message</h3>
              <h4>you can use it with the small code</h4>
          </div>
          <div class="col-md-5 pull-right">
              <p>Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a.</p>
          </div>
          <div class="clearfix"></div>
      </div>
      <div role="tabpanel" class="tab-pane fade" id="settings1">
          <div class="col-md-6">
              <h3>Just do Settings</h3>
              <h4>you can use it with the small code</h4>
          </div>
          <div class="col-md-5 pull-right">
              <p>Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a.</p>
          </div>
          <div class="clearfix"></div>
      </div>
  </div>
</div>
