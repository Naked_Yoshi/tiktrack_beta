<div id="userCreate" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="userCreateLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="clearModals();">×</button>
          <h4 class="modal-title" id="userCreateLabel"><i class='icon-user-follow'></i> เพิ่มพนักงาน</h4>
      </div>
      <div class="modal-body">
            <form class="usrCreateForm">
              <div class='row'>
                <div class="col-md-12">
                  <div class="form-group" id="form_email" >
                    <label for="usrcreateEmail">Email address</label>
                    <input type="email" class="form-control validate" name="email" id="usrcreateEmail" placeholder="กรอกอีเมล">
                    <small class="form-text text-muted" id="lbl_email_message" style="color:red;display:none;"><span class="fa fa-ban"></span>  อีเมล์นี้ถูกใช้งานแล้ว โปรดตรวจสอบ</small>
                  </div>
                </div>

                <div class="col-md-12">
                  <div class="form-group" id="form_phone">
                    <label for="usrcreatePhone">เบอร์โทรศัพท์</label>
                    <input type="text" class="form-control validate" id="usrcreatePhone" aria-describedby="phoneHelp" data-mask="999-999-9999"  placeholder="กรอกเบอร์โทรศัพท์">
                    <small id="phoneHelp" class="form-text text-muted">เบอร์โทรติดต่อจริง ใช้ในการเข้าระบบแอพพลิเคชั่น</small><br>
                    <small class="form-text text-muted" id="lbl_phone_message" style="color:red;display:none;"><span class="fa fa-ban"></span>  เบอร์โทรศัพท์นี้ถูกใช้งานแล้ว โปรดตรวจสอบ</small>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-12">
                  <h4>ข้อมูลส่วนตัว</h4>
                  <hr>
                </div>
              </div>
              <!-- // row -->
              <div class="row">
                <div class="col-md-4">
                  <div class="form-group">
                    <label for="usrcreateTitle">คำนำหน้า</label>
                    <select class="form-control" name="title" id="usrcreateTitle"></select>
                  </div>
                </div>
                <div class="col-md-4">
                  <div class="form-group">
                    <label for="usrcreateFname ">ชื่อจริง</label>
                    <input type="text" class="form-control validate" name="fname" id="usrcreateFname" placeholder="กรอกชื่อจริง">
                  </div>
                </div>
                <div class="col-md-4">
                  <div class="form-group">
                    <label for="usrcreateLname">นามสกุล</label>
                    <input type="text" class="form-control validate" name="lname" id="usrcreateLname" placeholder="กรอกนามสกุล">
                  </div>
                </div>
              </div>
              <!-- // row -->
              <div class="row">
                <div class="col-md-4">
                  <div class="form-group">
                    <label for="usrcreateTitle_en">คำนำหน้า (ภาษาอังกฤษ)</label>
                    <input type="text" class="form-control" name="title_en" id="usrcreateTitle_en" placeholder="คำนำหน้าภาษาอังกฤษ" readonly>
                  </div>
                </div>
                <div class="col-md-4">
                  <div class="form-group">
                    <label for="usrcreateFname_en">ชื่อจริง (ภาษาอังกฤษ)</label>
                    <input type="text" class="form-control validate" name="fname_en" id="usrcreateFname_en" placeholder="กรอกชื่อจริงภาษาอังกฤษ">
                  </div>
                </div>
                <div class="col-md-4">
                  <div class="form-group">
                    <label for="usrcreateLname_en">นามสกุล (ภาษาอังกฤษ)</label>
                    <input type="text" class="form-control validate" name="lname_en" id="usrcreateLname_en" placeholder="กรอกสกุลภาษาอังกฤษ">
                  </div>
                </div>
              </div>
              <!-- // row -->
              <div class="row">
                <div class="col-md-4">
                  <div class="form-group">
                    <label for="usrcreateSex">เพศ</label>
                    <select class="form-control" name="sex" id="usrcreateSex">
                        <option value="M">ชาย</option>
                        <option value="F">หญิง</option>
                     </select>
                  </div>
                </div>
                <div class="col-md-4">
                  <div class="form-group">
                    <label for="usrcreateBdate">วันเกิด</label>
                    <input type="text" class="form-control validate" placeholder="dd/mm/yyyy" name="bdate" id="usrcreateBdate" data-mask="99/99/9999">
                  </div>
                </div>
                <div class="col-md-4">
                  <div class="form-group">
                    <label for="usrcreatePersonId">รหัสบัตรประชาชน</label>
                    <input type="text" class="form-control validate" name="personId" id="usrcreatePersonId" placeholder="กรอกรหัสบัตรประชาชน" data-mask="9-9999-99999-99-9">
                  </div>
                </div>
              </div>
              <!-- // row -->
              <div class="row">
                <div class="col-md-4">
                  <div class="form-group">
                    <label for="usrcreateMarried">สถานะภาพการสมรส</label>
                    <select class="form-control" name="married" id="usrcreateMarried">
                      <option value="0">Default select</option>
                      <option value="1">โสด</option>
                      <option value="2">สมรส</option>
                      <option value="3">หย่าร้าง</option>
                     </select>
                  </div>
                </div>
                <div class="col-md-4">
                  <div class="form-group">
                    <label for="usrcreateNation">สัญชาติ</label>
                    <select class="form-control" name="nation" id="usrcreateNation">
                      <!-- <option>Default select</option> -->
                     </select>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-12">
                  <h4>การทำงาน</h4>
                  <hr>
                </div>
              </div>
              <!-- <div class="row">
                <div class="col-md-4">
                  <div class="form-group">
                    <label for="usrcreateSite">เลือกไซต์</label>
                    <select class="form-control" name="site" id="usrcreateSite">
                      <option>Default select</option>
                     </select>
                  </div>
                </div>
                <div class="col-md-4">
                  <div class="form-group">
                    <label for="usrcreateBranche">เลือกสาขา</label>
                    <select class="form-control" name="branche" id="usrcreateBranche">
                      <option>Default select</option>
                     </select>
                  </div>
                </div>
                <div class="col-md-4">
                  <div class="form-group">
                    <label for="usrcreateWorking">เลือกวันทำงาน</label>
                    <select class="form-control" name="working" id="usrcreateWorking">
                      <option>Default select</option>
                     </select>
                  </div>
                </div>
              </div> -->
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                    <label for="usrcreateDep">เลือกแผนก</label>
                    <select class="form-control" name="dep" id="usrcreateDep">

                     </select>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label for="usrcreatePosition">เลือกตำแหน่ง</label>
                    <select class="form-control" name="position" id="usrcreatePosition">
                      <!-- <option>Default select</option> -->
                     </select>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-4">
                  <div class="form-group">
                    <label for="usrcreateSalary">เงินเดือน</label>
                    <input type="text" class="form-control validate" name="salary" id="usrcreateSalary" placeholder="กรอกเงินเดือน">
                  </div>
                </div>
                <div class="col-md-4">
                  <div class="form-group">
                    <label for="usrcreateStart">วันที่เริ่มงาน</label>
                    <input type="text" class="form-control mydatepicker validate" placeholder="dd/mm/yyyy" name="start" id="usrcreateStart" data-mask="dd/mm/yyyy">
                  </div>
                </div>
                <!-- <div class="col-md-4">
                  <div class="form-group">
                    <label for="usrcreateProbation">วันสิ้นสุดการทดลองงาน</label>
                    <input type="text" class="form-control mydatepicker" placeholder="dd/mm/yyyy" name="probation" id="usrcreateProba" data-mask="dd/mm/yyyy">
                  </div>
                </div> -->
                <div class="col-md-4">
                  <div class="form-group">
                    <label for="usrcreateHoliday">วันหยุดพนักงาน</label>
                    <input type="text" class="form-control" name="holiday" id="usrcreateHoliday" placeholder="กรอกวันหยุดพนักงาน">
                  </div>
                </div>
              </div>
            </form>
      </div>
      <div class="modal-footer">
      <button type="button" class="btn btn-success waves-effect  btn-create">บันทึกข้อมูล</button>
      <button type="button" class="btn btn-info waves-effect" data-dismiss="modal" onclick="clearModals();">ปิดหน้าต่าง</button>
      </div>
    </div>
  </div>
</div>
