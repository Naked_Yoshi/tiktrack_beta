<?php

  if (!defined("BASEPATH")) exit('No direct script access allowed');
  date_default_timezone_set("Asia/Bangkok");

  class MY_Controller extends CI_Controller{
        var $template = array();
        var $data = array();

        public function __construct(){
           parent::__construct();
        }

        public function layouts(){
            $this->template["navbar"] = $this->load->view("layouts/navbar",$this->data,true);
            $this->template["sidebar"] = $this->load->view("layouts/sidebar",$this->data,true);
            $this->template["bread"] = $this->load->view("layouts/bread",$this->data,true);
            $this->template["middle"] = $this->load->view($this->middle,$this->data,true);
            $this->template["footer"] = $this->load->view("layouts/footer",$this->data,true);
            $this->load->view("layouts/index",$this->template);
        }
  }

?>
