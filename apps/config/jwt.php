<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

$config['jwt_key'] = 'Si@mr@j@th@nee1652Thailand';

/*Generated token will expire in 30 minute for sample code
* Increase this value as per requirement for production
*/
$config['token_timeout'] = 30;

/* End of file jwt.php */
/* Location: ./application/config/jwt.php */
