<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once 'vendor/autoload.php';

class Attendance extends MY_Controller {
	private $month  = array("มกราคม","กุมภาพันธ์","มีนาคม","เมษายน","พฤษภาคม","มิถุนายน","กรกฎาคม","สิงหาคม","กันยายน","ตุลาคม","พฤศจิกายน","ธันวาคม");
	private $models;
	public function __construct(){
			 	 parent::__construct();
				 // $this->models = $this->Branche;
	}

	public function index()
	{
		// $this->data["pageName"] = "ไซต์และสาขา";
		// $this->data["pageactive"] = "<li><a href='".base_url("sites")."' class='active'>ไซต์และสาขา</a></li>";
		// $this->middle = 'sites/index';
		// $this->data["CSS_asset"]  = "<link href='".base_url("assets/plugins/bower_components/clockpicker/dist/jquery-clockpicker.min.css")."'   rel='stylesheet' type='text/css'  />";
		// $this->data["JS_asset"]   = "<script src='".base_url("assets/js/cbpFWTabs.js")."'></script>";
		// $this->data["JS_asset"]  .= "<script src='".base_url("assets/js/apps/JSite.js")."'></script>";
		// $this->data["JS_asset"]  .= "<script src='".base_url("assets/js/apps/JSiteHelpers.js")."'></script>";
		// $this->data["JS_asset"]  .= "<script src='".base_url("assets/plugins/bower_components/moment/moment.js")."'></script>";
		// $this->data["JS_asset"]  .= "<script src='".base_url("assets/plugins/bower_components/clockpicker/dist/jquery-clockpicker.min.js")."'></script>";
		// $this->data["JS_asset"]  .= "<script src='https://maps.googleapis.com/maps/api/js?key=AIzaSyCZKhEPx14ken6ISjjnQT0K4tZ7nlNaJjU&libraries=places' async defer></script>";
		// $this->layouts();
	}

  public function daily(){
    $this->data["pageName"] = "บันทึกลงเวลา (รายวัน)";
    $this->data["pageactive"] = "<li><a href='".base_url("attendance/daily")."' class='active'>บันทึกลงเวลา (รายวัน)</a></li>";
    $this->middle = 'attendance/daily';
    $this->data["CSS_asset"]  = "<link href='".base_url("assets/plugins/bower_components/clockpicker/dist/jquery-clockpicker.min.css")."'   rel='stylesheet' type='text/css'  />";
    $this->data["CSS_asset"]  .= "<link href='".base_url("assets/plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css")."'   rel='stylesheet' type='text/css'  />";
		$this->data["CSS_asset"]  .= "<link href='".base_url("assets/plugins/bower_components/tablesaw-master/dist/tablesaw.css")."'   rel='stylesheet' type='text/css'  />";
		$this->data["CSS_asset"]  .= "<link href='".base_url("assets/plugins/bower_components/custom-select/custom-select.css")."'   rel='stylesheet' type='text/css'  />";
		$this->data["CSS_asset"]  .= "<link href='".base_url("assets/plugins/bower_components/datatables/jquery.dataTables.min.css")."'   rel='stylesheet' type='text/css'  />";

		$this->data["JS_asset"]   = "<script src='".base_url("assets/js/cbpFWTabs.js")."'></script>";
    $this->data["JS_asset"]  .= "<script src='".base_url("assets/js/apps/JDaily.js")."'></script>";
    $this->data["JS_asset"]  .= "<script src='".base_url("assets/plugins/bower_components/moment/moment.js")."'></script>";
    $this->data["JS_asset"]  .= "<script src='".base_url("assets/plugins/bower_components/clockpicker/dist/jquery-clockpicker.min.js")."'></script>";
    $this->data["JS_asset"]  .= "<script src='".base_url("assets/plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js")."'></script>";
		$this->data["JS_asset"]  .= "<script src='".base_url("assets/plugins/bower_components/tablesaw-master/dist/tablesaw.js")."'></script>";
		$this->data["JS_asset"]  .= "<script src='".base_url("assets/plugins/bower_components/tablesaw-master/dist/tablesaw-init.js")."'></script>";
		$this->data["JS_asset"]  .= "<script src='".base_url("assets/plugins/bower_components/bootstrap-select/bootstrap-select.min.js")."'></script>";
    $this->data["JS_asset"]  .= "<script src='https://maps.googleapis.com/maps/api/js?key=AIzaSyCZKhEPx14ken6ISjjnQT0K4tZ7nlNaJjU&libraries=places' async defer></script>";


    $this->layouts();
  }

	public function users(){
		$this->data["pageName"] = "บันทึกลงเวลา";
		$this->data["pageactive"] = "<li><a href='".base_url("attendance/users")."' class='active'>บันทึกลงเวลา</a></li>";
		$this->middle = 'attendance/person';

		//CSS
		$this->data["CSS_asset"]  = "<link href='".base_url("assets/plugins/bower_components/clockpicker/dist/jquery-clockpicker.min.css")."'   rel='stylesheet' type='text/css'  />";
		$this->data["CSS_asset"]  .= "<link href='".base_url("assets/plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css")."'   rel='stylesheet' type='text/css'  />";
		$this->data["CSS_asset"]  .= "<link href='".base_url("assets/plugins/bower_components/tablesaw-master/dist/tablesaw.css")."'   rel='stylesheet' type='text/css'  />";
		$this->data["CSS_asset"]  .= "<link href='".base_url("assets/plugins/bower_components/custom-select/custom-select.css")."'   rel='stylesheet' type='text/css'  />";
		$this->data["CSS_asset"] .= "<link href='".base_url("assets/css/jquery.auto-complete.css")."'   rel='stylesheet' type='text/css'  />";

		//JS
		//$this->data["JS_asset"] = "<script src='https://maps.googleapis.com/maps/api/js?key=AIzaSyCZKhEPx14ken6ISjjnQT0K4tZ7nlNaJjU' async defer></script>";
		$this->data["JS_asset"]   = "<script src='".base_url("assets/js/cbpFWTabs.js")."'></script>";
		$this->data["JS_asset"]  .= "<script src='".base_url("assets/js/apps/JAttUser.js")."'></script>";
		// $this->data["JS_asset"]  .= "<script src='".base_url("assets/js/apps/JPerson.js")."'></script>";
		$this->data["JS_asset"]  .= "<script src='".base_url("assets/plugins/bower_components/moment/moment.js")."'></script>";
		$this->data["JS_asset"]  .= "<script src='".base_url("assets/plugins/bower_components/clockpicker/dist/jquery-clockpicker.min.js")."'></script>";
		$this->data["JS_asset"]  .= "<script src='".base_url("assets/plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js")."'></script>";
		$this->data["JS_asset"]  .= "<script src='".base_url("assets/plugins/bower_components/tablesaw-master/dist/tablesaw.js")."'></script>";
		$this->data["JS_asset"]  .= "<script src='".base_url("assets/plugins/bower_components/tablesaw-master/dist/tablesaw-init.js")."'></script>";
		$this->data["JS_asset"]  .= "<script src='".base_url("assets/plugins/bower_components/bootstrap-select/bootstrap-select.min.js")."'></script>";
		$this->data["JS_asset"]  .= "<script src='https://maps.googleapis.com/maps/api/js?key=AIzaSyCZKhEPx14ken6ISjjnQT0K4tZ7nlNaJjU&libraries=places' async defer></script>";
		$this->data["JS_asset"]  .= "<script src='".base_url("assets/js/jquery.auto-complete.js")."'></script>";
		$this->layouts();
	}


	public function showdetailuser(){
		$this->load->view("attendance/detail.php");
	}

	public function showadjust(){
		$this->load->view("attendance/adjust.php");
	}

	private function getrow($dd,$tat){
			$callback = array();
			$dd = doubleval($dd);
			for ($i=0; $i < count($tat); $i++) {
					$row_day = doubleval($tat[$i]["tat_day"]);
					if ($row_day == $dd) {
							$callback = $tat[$i];
					}
			}
			return $callback;
	}

	private function gethd($dd,$hd_data){
			$callback = array();
			$dd = doubleval($dd);
			for ($i=0; $i < count($hd_data); $i++) {
					$row_day = doubleval($hd_data[$i]["hd_day"]);
					if ($row_day == $dd) {
							$callback = $hd_data[$i];
					}
			}
			return $callback;
	}

	private function getlev($dd,$lev_data,$month,$year){
			$callback = array();
			// $dd = doubleval($dd);

			$timenow = date($year.'-'.$month.'-'.$dd);
			$timenow = date('Y-m-d', strtotime($timenow));
			for ($i=0; $i < count($lev_data); $i++) {
					$sdate = date('Y-m-d', strtotime($lev_data[$i]["lev_data_date_start"]));
					$edate = date('Y-m-d', strtotime($lev_data[$i]["lev_data_date_end"]));
					if (($timenow >= $sdate) && ($timenow <= $edate)) {
						$callback = $lev_data[$i];
						 break;
					}
			}
			return $callback;
	}

	private function getot($dd,$ot_data){
		$callback = array();
		$dd = doubleval($dd);
		for ($i=0; $i < count($ot_data); $i++) {
				$row_day = doubleval(date("j", strtotime($ot_data[$i]["otd_date_start"])));
				if ($row_day == $dd) {
						$callback = $ot_data[$i];
				}
		}
		return $callback;
	}

	public function previewReport(){
		$emp_id = "";
		$tempsite = "";
		$tempbranche = "";
		$tempdep = "";

		$tempyear = $this->input->post("report_searchyear");
		$tempmonth = $this->input->post("report_searchmonth");
		$tempcompany = $this->input->post("company_id");

		$tat_month = substr($tempyear,2,2).sprintf("%02d", $tempmonth);;
		// print_r($tat_month);
		$type = $this->input->post("report_selectType");

		if ($type == "0") {
			$emp_id = $this->input->post("index_emp");
		}else if($type == "1"){
				$tempsite = $this->input->post("index_site");
			$tempbranche = $this->input->post("index_branche");
		}else if($type == "2"){
				$tempdep = $this->input->post("index_dep");
		}

		// $leave_request = $this->input->post("leave_request");
		// $auth = $this->input->post("auth");
		// $company = $this->input->post("company_id");
		//
		// $data = $this->ReportModels->getLeaveReport($leave_request,$company,$auth,$tempyear,$tempmonth);
		$company_detail = $this->Helpers->get_company($tempcompany);

		$defaultConfig = (new Mpdf\Config\ConfigVariables())->getDefaults();
 			$fontDirs = $defaultConfig['fontDir'];

 			$defaultFontConfig = (new Mpdf\Config\FontVariables())->getDefaults();
 			$fontData = $defaultFontConfig['fontdata'];
			 $mpdf = new \Mpdf\Mpdf([
 			    'fontDir' => array_merge($fontDirs, [
 			        __DIR__ . '/custom/font/th',
 			    ]),
 			    'fontdata' => $fontData + [
 			        'frutiger' => [
 			            'R' => 'THSarabunNew.ttf',
 			            'I' => 'THSarabunNew Italic.ttf',
 									'B' => 'THSarabunNew Bold.ttf',
 			        ]
 			    ],
 			    'default_font' => 'frutiger',
					'format' => 'A4'
 			]);
			// $fullname = $data["data"][0]["fullName"];
			$row = $this->User->getAllUser($type,$emp_id,$tempsite,$tempbranche,$tempdep);
			for ($i=0; $i < count($row) ; $i++) {
				$emp_id = $row[$i]["employee_id"];
				$hd_id = $row[$i]["hd_id"];
				$wk_id = $row[$i]["wk_id"];
				$wk_emp = $this->Helpers->chkDayoff($wk_id);
				$hd_emp = $this->Helpers->getHolidayEmp($hd_id,$tempmonth);
				$lev_emp = $this->Helpers->getLeaveEmp($emp_id,$tempmonth,$tempyear);
				$ot_emp = $this->Helpers->getOtEmp($emp_id,$tempmonth,$tempyear);
				$tat = $this->Helpers->getAtt($company_detail[0]["company_id"],$tat_month,$emp_id);
				$mpdf->AddPage('','','','','',5,5,10,0,0,0);
				$html = $this->htmlbody($tempmonth,$tempyear,$company_detail,$emp_id,$tat,$hd_emp,$lev_emp,$ot_emp,$wk_emp); //$data,$tempmonth,$tempyear,$leave_request
				$mpdf->WriteHTML($html);
			}
			$mpdf->Output();
	}

	private function htmlbody($month,$year,$company,$emp,$tat,$hd_data,$lev_emp,$ot_emp,$wk_emp){ //$tat,$index,$month,$year

			$lastday_of_month = cal_days_in_month(CAL_GREGORIAN,$month,$year);

			// $company = $this->company[0]["company_id"];
			$emp_data = $this->User->getBy($emp);
			$title  = $this->Helpers->get_title($emp_data["data"][0]["title_id"]);
			$startworkdate = $emp_data["data"][0]['startworkdate'];
			// // print_r($title);

			$bu_year = $year+543;
			// $month = doubleval($month)-1;
			$body   = "<style>
								.header{text-align:center; padding-bottom:3px; font-size:18px; font-weight:bold;}
								.sub-header{text-align:center; padding-bottom:3px; font-size:16px; font-weight:bold;}
								.header-bold{ text-decoration: underline;!important;}
								.input-form{ font-weight:normal; text-decoration:underline;}
								 table{ text-align:center; width:100%; font-size:14px; border-collapse: collapse; margin-top:10px;}
								 table tr { margin:0px !important;}
								 table tr td{ border:solid 1 px !important; margin:0px !important;}
								 table tr td.none-border{ border:none !important;}
								 table tr td.border-form { border-bottom:dotted 1px #666 !important;}
								 table tr td.td-header {padding-bottom:-5px;}
								 table tr td.td-padding {padding:10px;}
							   </style>";
			  $body .= "<table>";
				$body .= "<tr>";
				$body .= "<td class='header none-border' colspan='15'>". $company[0]["company_name"] ."</td>"; //".$this->company[0]["company_name"]."
				$body .= "</tr>";
				$body .= "<tr>";
				$body .= "<td class='none-border' colspan='15'>&nbsp;</td>";
				$body .= "</tr>";
				$body .= "<tr>";
				$body .= "<td class='td-header none-border' width='50' align='left'><b>ตั้งแต่วันที่</b></td>";
				$body .= "<td class='td-header none-border border-form' width='20'> 1 </td>";
				$body .= "<td class='td-header none-border'  width='50' align='center'><b>ถึงวันที่</b></td>";
				$body .= "<td class='td-header none-border border-form' width='20'> ".$lastday_of_month." </td>";
				$body .= "<td class='td-header none-border' width='50' align='center' ><b>เดือน</b></td>";
				$body .= "<td  class='td-header none-border border-form' colspan='3' > ".$this->month[doubleval($month)-1]." </td>"; //
				$body .= "<td class='td-header none-border' width='20' align='center' ><b>พ.ศ.</b></td>";
				$body .= "<td  class='td-header none-border border-form' width='50'>".$bu_year." </td>"; //
				$body .= "<td class='td-header none-border' width='50' align='center' ><b>หน่วยงาน</b></td>";
				$body .= "<td  class='td-header none-border border-form' colspan='5'>  ". $emp_data["data"][0]["siteName"] ."(".$emp_data["data"][0]["brancheName"].")</td>"; //
				$body .= "</tr>";

				$body .= "<tr>";
				$body .= "<td class='none-border' colspan='15'></td>";
				$body .= "</tr>";

			  $body .= "<tr>";
			  $body .= "<td class='td-header none-border' colspan='2'  width='120' align='left'><b>สำหรับพนักงานชื่อ-นามสกุล</b></td>";
				$body .= "<td class='td-header none-border border-form' colspan='6' > ".$title[0]["title_th"]. $emp_data["data"][0]["firstname"]." ".$emp_data["data"][0]['lastname']." </td>";
				$body .= "<td class='td-header none-border' colspan='1' align='center' ><b>ตำแหน่ง</b></td>";
			  $body .= "<td  class='td-header none-border border-form' colspan='6' > ". $emp_data["data"][0]['position_name'] ." </td>"; //".$emp[0]['positionName']."
			  $body .= "</tr>";

				$body .= "<tr>";
				$body .= "<td class='none-border' colspan='15'>&nbsp;</td>";
				$body .= "</tr>";

				$body .= "<tr>";
				$body .= "<td colspan='2'>วันที่</td>";
				$body .= "<td colspan='5'>สำหรับการลงเวลาทำงานในวันปกติ</td>";

				$body .= "<td colspan='4'>การลา</td>";
				$body .= "<td rowspan='2' colspan='6' width='70px'>ผู้อนุมัติ</td>";
				$body .= "</tr>";

				$body .= "<tr>";
				$body .= "<td width='80' colspan='2'>วันปกติ / วัันหยุด</td>";
	
				$body .= "<td width='72'>เข้า</td>";
				$body .= "<td width='72'>สาย</td>";
				$body .= "<td width='72'>ย้อนหลัง</td>";
				$body .= "<td width='72'>ออก</td>";
				$body .= "<td width='72'>ขาดงาน</td>";

				$body .= "<td width='48'>ประเภท</td>";
				$body .= "<td width='48'>วัน</td>";
				$body .= "<td width='48'>ชั่วโมง</td>";
				$body .= "<td width='48'>นาที</td>";
				$body .= "</tr>";

				$sun_work = 0;
				$sum_late_time = "0";
				$sum_leave_before_time = "0";
				// $sum_ot1 = "00:00:00";
				// $sum_ot2 = "00:00:00";
				// $sum_ot3 = "00:00:00";
				$sum_lev_hour = 0;
				$sum_lev_day = 0;
				$sum_lev_minute = 0;

				$wk_hour = $this->Helpers->getwkEmp($emp);

				for ($i=1; $i <= $lastday_of_month; $i++) {
				$day_of_week = date('w',strtotime($year."-".$month."-".$i));
				$rowtable = $this->getrow($i,$tat);
				$row_hd = $this->gethd($i,$hd_data);
				$row_lev = $this->getlev($i,$lev_emp,$month,$year);
				// $row_ot = $this->getot($i,$ot_emp);
				$checkIn = "-";
				$checkOut = "-";
				$lateTime = "-";
				$chkDelay = "-";
				$hdd_caption = "";
				$approveByName = "";

				$late_hour = 0;
				$late_minute = 0;

				$lev_type = "-";
				$lev_used_day = "-";
				$lev_used_hour = "-";
				$lev_used_minute = "-";

				$ot_start = "-";
				$ot_end = "-";
				$ot_f1 = "-";
				$ot_f2 = "-";
				$ot_f3 = "-";

				$chkholiday = 0;
				$chkdaywork = 0;


				$chkdaywork = $wk_emp[0][$day_of_week];							
				if (!empty($rowtable)) {
					$breakTime = explode("-",$rowtable["breakStr"]);
					$breakTime_start = date("H:i", strtotime($breakTime[0]));
					$breakTime_end = date("H:i", strtotime($breakTime[1]));
					
					if ($rowtable["checkin_time"] != "") {
								$checkIn =  date('H:i', strtotime($rowtable["checkin_time"]));
								$sun_work++;
						}
						if ($rowtable["checkout_time"] != "") {
								$checkOut = date('H:i', strtotime($rowtable["checkout_time"]));
						}
					if ($rowtable["lateTime"] != "") {
						if (strpos($rowtable["lateTime"], '-') !== false) {
							$lateTime = "-";
						}else{
							
							if ($chkdaywork == "0") {
								$lateTime = "-";
							}else {
									$lateTime = $rowtable["lateTime"];
									$lateTime = date('H:i', strtotime($rowtable["lateTime"]));
									if ($lateTime == "00:00") {
										$lateTime = "-";
									}else{
										$secs = strtotime($lateTime)-strtotime("00:00");
										$late_hour = intval(substr($lateTime,0,2)) * 60;
										$late_minute =  intval(substr($lateTime,3,2));
										$sum_late_time += ($late_hour + $late_minute);
										// $sum_late_time = date("H:i",strtotime($sum_late_time)+$secs);
									}
							}
						}
					}
					if ($rowtable["approveBy"] != "") {
						$approveByName = $rowtable["approveBy"];
					}

					$chkDelay = $rowtable["isCheckInDelay"];
				}

				if ($approveByName == "") {
					$approveByName = '-';
				}

				if(!empty($row_hd)){
					$chkholiday = 1;
						$hdd_caption = "(". $row_hd["hdd_caption"]. ")";
						$sum_late_time -= ($late_hour + $late_minute);
						$lateTime = "-";
				}else{
					if ($day_of_week == 6 || $day_of_week == 0) {
						if ($day_of_week == 6) {
							$hdd_caption = "เสาร์";
						}else if($day_of_week == 0){
							$hdd_caption = "อาทิตย์";
						}
					}else{
						switch ($day_of_week) {
							case 1:
								$hdd_caption = "จันทร์";
								break;
							case 2:
									$hdd_caption = "อังคาร";
									break;
							case 3:
									$hdd_caption = "พุธ";
									break;
							case 4:
									$hdd_caption = "พฤหัสบดี";
									break;
							case 5:
									$hdd_caption = "ศุกร์";
									break;
							default:
								// code...
								break;
						}
					}
				}

				if(!empty($row_lev)){
						$lev_start_time = date("H:i",strtotime($row_lev["lev_data_date_start"]));
						$lev_end_time = date("H:i",strtotime($row_lev["lev_data_date_end"]));

						$lev_type = $row_lev["leave_type_name"];

						$lev_used_minute = $row_lev["lev_data_d_minute"];
						$sum_lev_minute += $row_lev["lev_data_d_minute"];
						if ($sum_lev_minute >= 60) {
							$sum_lev_hour += 1;
							$sum_lev_minute -= 60;
						}

						$lev_used_hour = $row_lev["lev_data_d_hour"];
						$sum_lev_hour += $row_lev["lev_data_d_hour"];
						if ($sum_lev_hour >= $wk_hour) {
							$sum_lev_day += 1;
							$sum_lev_hour -= $wk_hour;
						}

						if ($row_lev["lev_data_d_day"] > 0) {
							$lev_used_day = 1;
							$sum_lev_day += 1;
						}else{
							$lev_used_day = $row_lev["lev_data_d_day"];
						}
						// $lev_used_day = $row_lev["lev_data_d_day"];

						if ($lev_start_time == "08:00") {
							$sum_late_time -= ($late_hour + $late_minute); //ลบของเก่าออกก่อน
							if ($lev_end_time == "12:00") {
								$diff = strtotime($checkIn) - strtotime($breakTime_end);
							}else{
								$diff = strtotime($checkIn) - strtotime($lev_end_time);
							}
							// $diff = strtotime($checkIn) - strtotime($lev_end_time);

							if (strpos($diff, '-') !== false || $diff == 0) {							
								$lateTime = "-";
							}else{								
								$lateTime = sprintf('%02d', floor($diff / (60*60))).":".sprintf('%02d', ($diff / 60));
								$late_hour = intval(substr($lateTime,0,2)) * 60;
								$late_minute =  intval(substr($lateTime,3,2));
								$sum_late_time += ($late_hour + $late_minute);
								// $sum_late_time = date("H:i",strtotime($sum_late_time)+$secs);
							}
							// $temp_late_time =
						}

							// if (strpos($diff, '-') !== false || $diff == 0) {
							// 	// $secs = strtotime($lateTime)-strtotime("00:00");
							// 	$sum_late_time -= ($late_hour + $late_minute);
							// 	// $sum_late_time = date("H:i",strtotime($sum_late_time)-$secs);
								
							// 	$lateTime = "-";
							// }
				}

				// if ($lateTime == "0:0") {
				// 	$lateTime = "-";
				// }

				// if(!empty($row_ot)){
				// 	$ot_start = $row_ot["otd_time_start"];
				// 	$ot_end = $row_ot["otd_time_end"];
				// 	if ($row_ot["otd_multiple"] == 1.50) {
				// 		$ot_f1 = $row_ot["reqTotalHours"];
				// 		$secs = strtotime($ot_f1)-strtotime("00:00:00");
				// 		$sum_ot1 = date("H:i:s",strtotime($sum_ot1)+$secs);
				// 	}else if($row_ot["otd_multiple"] == 1.00){
				// 		$ot_f2 = $row_ot["reqTotalHours"];
				// 		$secs = strtotime($ot_f2)-strtotime("00:00:00");
				// 		$sum_ot2 = date("H:i:s",strtotime($sum_ot2)+$secs);
				// 	}else if($row_ot["otd_multiple"] == 3.00){
				// 		$ot_f3 = $row_ot["reqTotalHours"];
				// 		$secs = strtotime($ot_f3)-strtotime("00:00:00");
				// 		$sum_ot3 = date("H:i:s",strtotime($sum_ot3)+$secs);
				// 	}
				// }

				if ($chkholiday == 1) {
					$body .= "<tr bgcolor='#e6e6e6'>";
				}else{
					$body .= "<tr>";
				}

				$body .= "<td width='10'>". $i ."</td>"; //$i

				$body .= "<td width='60'>$hdd_caption</td>";

				if (empty($row_hd) && $chkdaywork == "1" && $checkIn == "-" && $lev_type == "-" && strtotime($year."-".$month."-".$i) <= strtotime(date("Y-m-d"))  && strtotime($year."-".$month."-".$i) >= strtotime($startworkdate)) {
					$body .= "<td bgcolor='#e6e6e6' colspan='9' class='text-center'>ขาดงาน</td>";
				}else{

						$body .= "<td>$checkIn</td>";
						$body .= "<td>$lateTime</td>"; //
						$body .= "<td>$chkDelay</td>"; //
						$body .= "<td>$checkOut</td>"; //
						$body .= "<td></td>"; // ออกก่อนเวลา

						$body .= "<td>$lev_type</td>";
						$body .= "<td>$lev_used_day</td>";
						$body .= "<td>$lev_used_hour</td>";
						$body .= "<td>$lev_used_minute</td>";					
					}	
					$body .= "<td colspan='6'>$approveByName</td>";
						// $body .= "<td></td>";
					$body .= "</tr>";

				}
				$body .= "<tr>";
				$body .= "<td class='none-border' colspan='2'>รวมวันทำงานปกติ</td>";
				$body .= "<td>$sun_work วัน</td>"; //
				$body .= "<td>". floor(floor($sum_late_time / 60) / $wk_hour) ." / ". sprintf('%02d', floor($sum_late_time / 60) % $wk_hour)  .":". sprintf('%02d', ($sum_late_time % 60)) ."</td>"; //floor(floor($sum_late_time / 60) / $wk_hour) ." / ". sprintf('%02d', floor($sum_late_time / 60) % $wk_hour)  .":". sprintf('%02d', ($sum_late_time % 60))

				$body .= "<td class='none-border'></td>";
				$body .= "<td class='none-border'></td>";
				$body .= "<td></td>";
				$body .= "<td class='none-border'></td>";
				$body .= "<td>$sum_lev_day วัน</td>";
				$body .= "<td>$sum_lev_hour ชม.</td>";
				$body .= "<td>". ($sum_lev_minute % 60) ." นาที</td>";
				$body .= "<td class='none-border'></td>";
				$body .= "</tr>";

				$body .= "<tr>";
				$body .= "<td class='none-border' colspan='13'>&nbsp;</td>";
				$body .= "</tr>";

				$body .= "<tr>";
				$body .= "<td class='none-border' colspan='1' width='50' align='left'><b>หมายเหตุ</b></td>";
				$body .= "<td class='none-border' colspan='14' align='left'> 1. ข้อมูลการลาที่ปรากฏในช่องการลานั้น ได้รับการอนุมัติเรียบร้อยแล้ว</td>";
				$body .= "</tr>";
				$body .= "<tr>";
				$body .= "<td class='none-border' colspan='1' width='50' align='center'><b></b></td>";
				$body .= "<td class='none-border' colspan='14' align='left'>&nbsp;</td>";
				$body .= "</tr>";
				$body .= "<tr>";
				$body .= "<td class='none-border' colspan='1' width='50' align='center'><b></b></td>";
				// $body .= "<td class='none-border' colspan='14' align='left'> 3. ใบลงเวลาทำงานนั้นจะถูกต้องสมบูรณ์  เมื่อ<span class='input-form'>มีลายมือชื่อพนักงานตามช่องลายเซ็นต์ด้านบนในวันที่มาปฏิบัติงานและด้านล่างซ้าย</span> และ <span class='input-form'>ลายมือชื่อผู้บริหารหน่วยงานตามช่องด้านล่างขวา</span> แล้วเท่านั้น</td>";
				$body .= "</tr>";

				$body .= "<tr>";
				$body .= "<td class='none-border' colspan='14'>&nbsp;</td>";
				$body .= "</tr>";


			$body .= "</table>";
			$body .= "";
			return $body;
	}

}
