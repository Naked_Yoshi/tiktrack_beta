<?php
defined('BASEPATH') OR exit('No direct script access allowed');
date_default_timezone_set("Asia/Bangkok");

require_once 'vendor/autoload.php';

class ReportForm extends MY_Controller {
	private $auth ;
	private $th_month  = array("มกราคม","กุมภาพันธ์","มีนาคม","เมษายน","พฤษภาคม","มิถุนายน","กรกฎาคม","สิงหาคม","กันยายน","ตุลาคม","พฤศจิกายน","ธันวาคม");
	public function __construct(){
		 parent::__construct();
    }

    public function report_attendance(){
        $this->data["pageName"] = "รายงาน : บันทึกลงเวลา";
        $this->data["pageactive"] = "<li><a href='".base_url("attendance/daily")."' class='active'>บันทึกลงเวลา (รายวัน)</a></li>";       
        $this->middle = 'reportform/report_attendance';

        $this->data["JS_asset"]   = "<script src='".base_url("assets/js/cbpFWTabs.js")."'></script>";
        $this->data["JS_asset"]  .= "<script src='".base_url("assets/js/apps/ReportForm/report_attendance.js")."'></script>";

        $this->layouts();
    }
}