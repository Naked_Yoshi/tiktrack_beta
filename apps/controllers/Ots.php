<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ots extends MY_Controller {

	private $models;
	public function __construct(){
			 parent::__construct();
			 $this->models = $this->OT;
	}


	public function modalCreate(){
			$this->load->view("ot/create");
	}

	public function modalEdit(){
			$this->load->view("ot/edit");
	}

	public function getlinkot(){
		$this->load->view("working/link_ot");
	}

	public function load(){
		$this->models->company = 3;
		$this->models->auth = "20170000001";
		$ot = $this->models->getAll($_POST);
		$ot_table = $this->generateTable($ot);
		echo json_encode($ot_table);
		return;
	}

 	public function getDetail(){
			$ot = $_POST["ot"];
		  $config = $this->models->getBy($ot);
			if (count($config) > 0) {
					$config["Has"] = true;
					$config["data"] = $config[0];
			}else{
					$config["Has"] = false;
			}
			$excess = $this->models->getExcess($ot);
			$approve = $this->models->getApprove($ot);
			$container = array(
				"config" => $config,
				"excess" => $excess,
				"approve" => $approve
			);
			$data["detail"] = $container;
			$this->load->view("working/link_ot",$data);
	}

	public function add(){
			$this->models->company = 3;
			$this->models->auth = "20170000001";
			$add = $this->models->add($_POST);
			$callback = array(
				"success" => false,
				"title" => "ผิดพลาด",
				"msg" => "ไม่สามารถบันทึกข้อมูลได้ กรุณาลองใหม่ในภายหลัง"
			);
			if($add){
				$callback = array(
					"success" => true,
					"title" => "สำเร็จ",
					"msg" => "บันทึกข้อมูลค่าล่วงเวลาสำเร็จ"
				);
			}
			echo json_encode($callback);
			return;
	}


	private function generateTable($data){
	    $tables = array();
	    $tables["table"] = array();
	    $tables["data"] = array();
	    $tables["Has"] = $data["Has"];
	    if ($data["Has"]) {
	        $num = 1;
	        for ($i=0; $i < count($data["data"]); $i++) {
							 $otCode = base64_encode(base64_encode(base64_encode($data["data"][$i]["ot_name"])));
	             $tr  = "";
	             $tr .= "<tr>";
	             $tr .= "<td  class='text-center'>";
	             $tr .= $num;
	             $tr .= "</td>";
	             $tr .= "<td>";
	             $tr .= $data["data"][$i]["ot_name"];
	             $tr .= "</td>";
	             $tr .= "<td class='text-center'>";
	             $tr .= "<div class='btn-group'>";
	             $tr .= "<button class='btn btn-sm btn-default btn-outline waves-effect btn-site-edit' data-edit='".$otCode."'><span class='fa fa-edit'></span></button>";
	             $tr .= "<button class='btn btn-sm btn-default btn-outline waves-effect'><span class='fa fa-trash'></span></button>";
	             $tr .= "</div>";
	             $tr .= "</td>";
	             $tr .= "</tr>";
	             array_push($tables["table"],$tr);
	             array_push($tables["data"],
	                array(
	                  "num"=>$num,
	                  "id"=>$data["data"][$i]["ot_id"],
	                  "name"=>$data["data"][$i]["ot_name"],
	              ));
	              $num ++;
	        }

	    }
	    return $tables;
	}
}
