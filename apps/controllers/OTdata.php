<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once 'vendor/autoload.php';

class OTdata extends MY_Controller {

	private $models;
	public function __construct(){
			 	 parent::__construct();
				 // $this->models = $this->Branche;
	}

  public function requestLoad()
  {
    $this->data["pageName"] = "ล่วงเวลา OT";
    $this->data["pageactive"] = "<li><a href='".base_url("otdata/requestLoad")."' class='active'>ขออนุมัติ OT</a></li>";
    $this->middle = 'otdata/request';
    $this->data["CSS_asset"]  = "<link href='".base_url("assets/plugins/bower_components/clockpicker/dist/jquery-clockpicker.min.css")."'   rel='stylesheet' type='text/css'  />";
    $this->data["CSS_asset"]  .= "<link href='".base_url("assets/plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css")."'   rel='stylesheet' type='text/css'  />";
		$this->data["CSS_asset"]  .= "<link href='".base_url("assets/plugins/bower_components/tablesaw-master/dist/tablesaw.css")."'   rel='stylesheet' type='text/css'  />";
		$this->data["CSS_asset"]  .= "<link href='".base_url("assets/plugins/bower_components/custom-select/custom-select.css")."'   rel='stylesheet' type='text/css'  />";
		$this->data["CSS_asset"]  .= "<link href='".base_url("assets/plugins/bower_components/datatables/jquery.dataTables.min.css")."'   rel='stylesheet' type='text/css'  />";
		$this->data["CSS_asset"] .= "<link href='".base_url("assets/css/jquery.auto-complete.css")."'   rel='stylesheet' type='text/css'  />";

		$this->data["JS_asset"]   = "<script src='".base_url("assets/js/cbpFWTabs.js")."'></script>";
    $this->data["JS_asset"]  .= "<script src='".base_url("assets/js/apps/JOTData/request.js")."'></script>";
    $this->data["JS_asset"]  .= "<script src='".base_url("assets/plugins/bower_components/moment/moment.js")."'></script>";
    $this->data["JS_asset"]  .= "<script src='".base_url("assets/plugins/bower_components/clockpicker/dist/jquery-clockpicker.min.js")."'></script>";
    $this->data["JS_asset"]  .= "<script src='".base_url("assets/plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js")."'></script>";
		$this->data["JS_asset"]  .= "<script src='".base_url("assets/plugins/bower_components/tablesaw-master/dist/tablesaw.js")."'></script>";
		$this->data["JS_asset"]  .= "<script src='".base_url("assets/plugins/bower_components/tablesaw-master/dist/tablesaw-init.js")."'></script>";
		$this->data["JS_asset"]  .= "<script src='".base_url("assets/plugins/bower_components/bootstrap-select/bootstrap-select.min.js")."'></script>";
    $this->data["JS_asset"]  .= "<script src='https://maps.googleapis.com/maps/api/js?key=AIzaSyCZKhEPx14ken6ISjjnQT0K4tZ7nlNaJjU&libraries=places' async defer></script>";
		$this->data["JS_asset"]  .= "<script src='".base_url("assets/js/jquery.auto-complete.js")."'></script>";
    $this->layouts();
  }

	public function completeLoad()
	{
		$this->data["pageName"] = "ล่วงเวลา OT";
    $this->data["pageactive"] = "<li><a href='".base_url("otdata/completeLoad")."' class='active'>อนุมัติ</a></li>";
    $this->middle = 'otdata/complete';
    $this->data["CSS_asset"]  = "<link href='".base_url("assets/plugins/bower_components/clockpicker/dist/jquery-clockpicker.min.css")."'   rel='stylesheet' type='text/css'  />";
    $this->data["CSS_asset"]  .= "<link href='".base_url("assets/plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css")."'   rel='stylesheet' type='text/css'  />";
		$this->data["CSS_asset"]  .= "<link href='".base_url("assets/plugins/bower_components/tablesaw-master/dist/tablesaw.css")."'   rel='stylesheet' type='text/css'  />";
		$this->data["CSS_asset"]  .= "<link href='".base_url("assets/plugins/bower_components/custom-select/custom-select.css")."'   rel='stylesheet' type='text/css'  />";
		$this->data["CSS_asset"]  .= "<link href='".base_url("assets/plugins/bower_components/datatables/jquery.dataTables.min.css")."'   rel='stylesheet' type='text/css'  />";
		$this->data["CSS_asset"] .= "<link href='".base_url("assets/css/jquery.auto-complete.css")."'   rel='stylesheet' type='text/css'  />";

		$this->data["JS_asset"]   = "<script src='".base_url("assets/js/cbpFWTabs.js")."'></script>";
    $this->data["JS_asset"]  .= "<script src='".base_url("assets/js/apps/JOTData/complete.js")."'></script>";
    $this->data["JS_asset"]  .= "<script src='".base_url("assets/plugins/bower_components/moment/moment.js")."'></script>";
    $this->data["JS_asset"]  .= "<script src='".base_url("assets/plugins/bower_components/clockpicker/dist/jquery-clockpicker.min.js")."'></script>";
    $this->data["JS_asset"]  .= "<script src='".base_url("assets/plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js")."'></script>";
		$this->data["JS_asset"]  .= "<script src='".base_url("assets/plugins/bower_components/tablesaw-master/dist/tablesaw.js")."'></script>";
		$this->data["JS_asset"]  .= "<script src='".base_url("assets/plugins/bower_components/tablesaw-master/dist/tablesaw-init.js")."'></script>";
		$this->data["JS_asset"]  .= "<script src='".base_url("assets/plugins/bower_components/bootstrap-select/bootstrap-select.min.js")."'></script>";
    $this->data["JS_asset"]  .= "<script src='https://maps.googleapis.com/maps/api/js?key=AIzaSyCZKhEPx14ken6ISjjnQT0K4tZ7nlNaJjU&libraries=places' async defer></script>";
		$this->data["JS_asset"]  .= "<script src='".base_url("assets/js/jquery.auto-complete.js")."'></script>";
    $this->layouts();
	}

	public function rejectLoad()
	{
		$this->data["pageName"] = "ล่วงเวลา OT";
    $this->data["pageactive"] = "<li><a href='".base_url("otdata/completeLoad")."' class='active'>ยกเลิกอนุมัติ</a></li>";
    $this->middle = 'otdata/reject';
    $this->data["CSS_asset"]  = "<link href='".base_url("assets/plugins/bower_components/clockpicker/dist/jquery-clockpicker.min.css")."'   rel='stylesheet' type='text/css'  />";
    $this->data["CSS_asset"]  .= "<link href='".base_url("assets/plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css")."'   rel='stylesheet' type='text/css'  />";
		$this->data["CSS_asset"]  .= "<link href='".base_url("assets/plugins/bower_components/tablesaw-master/dist/tablesaw.css")."'   rel='stylesheet' type='text/css'  />";
		$this->data["CSS_asset"]  .= "<link href='".base_url("assets/plugins/bower_components/custom-select/custom-select.css")."'   rel='stylesheet' type='text/css'  />";
		$this->data["CSS_asset"]  .= "<link href='".base_url("assets/plugins/bower_components/datatables/jquery.dataTables.min.css")."'   rel='stylesheet' type='text/css'  />";

		$this->data["JS_asset"]   = "<script src='".base_url("assets/js/cbpFWTabs.js")."'></script>";
    $this->data["JS_asset"]  .= "<script src='".base_url("assets/js/apps/JOTData/reject.js")."'></script>";
    $this->data["JS_asset"]  .= "<script src='".base_url("assets/plugins/bower_components/moment/moment.js")."'></script>";
    $this->data["JS_asset"]  .= "<script src='".base_url("assets/plugins/bower_components/clockpicker/dist/jquery-clockpicker.min.js")."'></script>";
    $this->data["JS_asset"]  .= "<script src='".base_url("assets/plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js")."'></script>";
		$this->data["JS_asset"]  .= "<script src='".base_url("assets/plugins/bower_components/tablesaw-master/dist/tablesaw.js")."'></script>";
		$this->data["JS_asset"]  .= "<script src='".base_url("assets/plugins/bower_components/tablesaw-master/dist/tablesaw-init.js")."'></script>";
		$this->data["JS_asset"]  .= "<script src='".base_url("assets/plugins/bower_components/bootstrap-select/bootstrap-select.min.js")."'></script>";
    $this->data["JS_asset"]  .= "<script src='https://maps.googleapis.com/maps/api/js?key=AIzaSyCZKhEPx14ken6ISjjnQT0K4tZ7nlNaJjU&libraries=places' async defer></script>";
    $this->layouts();
	}

	public function paidLoad()
	{
		$this->data["pageName"] = "ล่วงเวลา OT";
		$this->data["pageactive"] = "<li><a href='".base_url("otdata/paidLoad")."' class='active'>อนุมัติเบิก</a></li>";
		$this->middle = 'otdata/paid';
		$this->data["CSS_asset"]  = "<link href='".base_url("assets/plugins/bower_components/clockpicker/dist/jquery-clockpicker.min.css")."'   rel='stylesheet' type='text/css'  />";
		$this->data["CSS_asset"]  .= "<link href='".base_url("assets/plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css")."'   rel='stylesheet' type='text/css'  />";
		$this->data["CSS_asset"]  .= "<link href='".base_url("assets/plugins/bower_components/tablesaw-master/dist/tablesaw.css")."'   rel='stylesheet' type='text/css'  />";
		$this->data["CSS_asset"]  .= "<link href='".base_url("assets/plugins/bower_components/custom-select/custom-select.css")."'   rel='stylesheet' type='text/css'  />";
		$this->data["CSS_asset"]  .= "<link href='".base_url("assets/plugins/bower_components/datatables/jquery.dataTables.min.css")."'   rel='stylesheet' type='text/css'  />";
		$this->data["CSS_asset"] .= "<link href='".base_url("assets/css/jquery.auto-complete.css")."'   rel='stylesheet' type='text/css'  />";

		$this->data["JS_asset"]   = "<script src='".base_url("assets/js/cbpFWTabs.js")."'></script>";
		$this->data["JS_asset"]  .= "<script src='".base_url("assets/js/apps/JOTData/paid.js")."'></script>";
		$this->data["JS_asset"]  .= "<script src='".base_url("assets/plugins/bower_components/moment/moment.js")."'></script>";
		$this->data["JS_asset"]  .= "<script src='".base_url("assets/plugins/bower_components/clockpicker/dist/jquery-clockpicker.min.js")."'></script>";
		$this->data["JS_asset"]  .= "<script src='".base_url("assets/plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js")."'></script>";
		$this->data["JS_asset"]  .= "<script src='".base_url("assets/plugins/bower_components/tablesaw-master/dist/tablesaw.js")."'></script>";
		$this->data["JS_asset"]  .= "<script src='".base_url("assets/plugins/bower_components/tablesaw-master/dist/tablesaw-init.js")."'></script>";
		$this->data["JS_asset"]  .= "<script src='".base_url("assets/plugins/bower_components/bootstrap-select/bootstrap-select.min.js")."'></script>";
		$this->data["JS_asset"]  .= "<script src='https://maps.googleapis.com/maps/api/js?key=AIzaSyCZKhEPx14ken6ISjjnQT0K4tZ7nlNaJjU&libraries=places' async defer></script>";
		$this->data["JS_asset"]  .= "<script src='".base_url("assets/js/jquery.auto-complete.js")."'></script>";
		$this->layouts();
	}


	public function ReportComplete(){
			$this->load->view("otdata/ReportComplete");
	}
	public function ReportRequest(){
			$this->load->view("otdata/ReportRequest");
	}
	public function AdjustOT(){
		$this->load->view("otdata/adjustOT");
	}
	public function AdjustExpenses(){
		$this->load->view("otdata/adjustExpenses");
	}
	public function FilterLeaveRequest(){
		$this->load->view("otdata/filter");
	}

	// public function preview(){
	// 	$data = $_GET['dataset'];
	// 	print_r($data);
	// 	 previewReport();
	// }

	public function previewReport(){
		// $data = json_decode($_GET["dataset"]);
		// $dataRequest = base64_decode($data);

		$defaultConfig = (new Mpdf\Config\ConfigVariables())->getDefaults();
 			$fontDirs = $defaultConfig['fontDir'];

 			$defaultFontConfig = (new Mpdf\Config\FontVariables())->getDefaults();
 			$fontData = $defaultFontConfig['fontdata'];
			 $mpdf = new \Mpdf\Mpdf([
 			    'fontDir' => array_merge($fontDirs, [
 			        __DIR__ . '/custom/font/th',
 			    ]),
 			    'fontdata' => $fontData + [
 			        'frutiger' => [
 			            'R' => 'THSarabunNew.ttf',
 			            'I' => 'THSarabunNew Italic.ttf',
 									'B' => 'THSarabunNew Bold.ttf',
 			        ]
 			    ],
 			    'default_font' => 'frutiger',
					'format' => 'A4-L'
 			]);
			// $fullname = $data["data"][0]["fullName"];
			//TODO : call HelperModel
			//mock data
			// $date_start = '2019-01-01';
			// $date_end = '2019-01-31';
			// $employee_id = '20181105112134VI';
			$type = $this->input->post("selectType");

			$tempsite = $this->input->post("site");
			$tempbranche = $this->input->post("branche");

			$date_start = $this->input->post("date_start");
			$conv_date_start = str_replace('/', '-', $date_start);
			$date_started = date("Y-m-d", strtotime($conv_date_start));

			$date_end = $this->input->post("date_ended");
			$conv_date_end = str_replace('/', '-', $date_end);
			$date_ended = date("Y-m-d", strtotime($conv_date_end));

			$employee_id = $this->input->post("index_emp");

			// echo "site ". $tempsite ." branche ".$tempbranche;
			$tempdep = "";

			if ($type == "0") {
				$response_user_data = $this->Helpers->GetUserHeaderOT($employee_id);
				// print_r($response_user_data);
				$response = $this->Helpers->OTSummaryPerson($date_started,$date_ended,$employee_id,"","","");
				$response['Header'] = $response_user_data['data'];
				// print_r($response);
				if($response['Has'])
				{
					$html = $this->htmlbodys($response);
					$mpdf->WriteHTML($html);
						// $mpdf->WriteHTML("test : ");
					// $mpdf->Output();
				}else{
					$html = $this->notFoundBody();
					$mpdf->WriteHTML($html);
						// $mpdf->WriteHTML("test : ");

				}
			}else{

				$row = $this->User->getAllUser($type,$employee_id,$tempsite,$tempbranche,$tempdep);
				for ($i=0; $i < count($row) ; $i++) {
					$emp_id = $row[$i]["employee_id"];
					$response_user_data = $this->Helpers->GetUserHeaderOT($emp_id);
					// print_r($response_user_data);
					$response = $this->Helpers->OTSummaryPerson($date_started,$date_ended,$emp_id);
					$response['Header'] = $response_user_data['data'];


						if($response['Has'])
						{
							$mpdf->AddPage('','','','','',5,5,10,0,0,0);
							$html = $this->htmlbodys($response);
							$mpdf->WriteHTML($html);
								// $mpdf->WriteHTML("test : ");
							// $mpdf->Output();
						}
				}
			}

				$mpdf->Output();

			// echo $date_started." ".$date_ended." ".$employee_id;

			// $date_start = '2019-01-01';
			// $date_end = '2019-01-31';
			// $employee_id = '20181105112134VI';



	}

	function htmlbodys($data){
		$header = $data['Header'];
		$body   = "<style>
						.header{text-align:center; padding-bottom:3px; font-size:18px; font-weight:bold;}
						.sub-header{text-align:center; padding-bottom:3px; font-size:16px; font-weight:bold;}
						.header-bold{ text-decoration: underline;!important;}
						.input-form{ font-weight:normal; text-decoration:underline;}
						.text-left{ text-align:left;}
						.text-right{ text-align:right;}
						.text-center{ text-align:center;}
						 table{ text-align:center; width:100%; font-size:14px; border-collapse: collapse; margin-top:10px;}
						 table tr { margin:0px !important;}
						 table tr td{ border:solid 1 px !important; margin:0px !important;}
						 table tr td.none-border{ border:none !important;}
						 table tr td.border-form { border-bottom:dotted 1px #666 !important;}
						 table tr td.td-header {padding-bottom:-5px;}
						 table tr td.td-padding {padding:10px;}
						 </style>";
		$body .= "<table>";
		$body .= "<tr>";
		$body .= "<td colspan='17' class='none-border header'>บริษัท สยามราชธานี จำกัด</td>";
		$body .= "</tr>";
		$body .= "<tr>";
		$body .= "<td colspan='17' class='none-border header'>แบบฟอร์มขออนุมัติ การทำงานล่วงเวลา / ทำงานวันหยุด</td>";
		$body .= "</tr>";
		$body .= "<tr>";
		$body .= "<td colspan='1' width='6%' class='none-border text-left'>ชื่อ - สกุล</td><td colspan='1'  width='6%' class='none-border border-form'></td><td colspan='4' class='none-border border-form'>".$header['fullname']."</td>";
		$body .= "<td colspan='1' class='none-border text-left'>แผนก</td><td colspan='4' class='none-border border-form'>".$header['dep_name']."</td>";
		$body .= "<td colspan='1' class='none-border text-left'>ธุรกิจ</td><td colspan='7' class='none-border border-form'>"."-"."</td>";
		$body .= "</tr>";

		$body .= "<tr>";
		$body .= "<td class='none-border' colspan='17'>&nbsp;</td>";
		$body .= "</tr>";

		//headers
		$body .= "<tr>";
		$body .= "<td rowspan='2' width = '30%'>ว/ด/ป<br>ที่ขอ</td>";
		$body .= "<td rowspan='2'>ว/ด/ป<br>ที่ทำ</td>";
		$body .= "<td rowspan='2'>ระบุวัน<br>จ-อา</td>";
		$body .= "<td colspan='2'>เวลาทำงาน</td>";
		$body .= "<td colspan='2'>เวลาที่ทำ</td>";
		$body .= "<td rowspan='2' width='15%'>เหตุผลที่ขอทำ</td>";
		$body .= "<td rowspan='2' width='10%'>ค่าใช้จ่าย (รหัส)<br>แผนก / Site</td>";
		$body .= "<td colspan='3'>OT (ชม.)</td>";
		$body .= "<td rowspan='2'>OT (เหมาจ่าย)<br>จำนวนเงิน</td>";
		$body .= "<td rowspan='2'>ลายเซ็น<br>พนักงาน</td>";
		$body .= "<td rowspan='2'>หน.แผนก<br>อนุมัติทำ</td>";
		$body .= "<td rowspan='2'>ผจก.แผนก<br>อนุมัติทำ</td>";
		$body .= "<td rowspan='2'>หน.แผนก<br>อนุมัติเบิก</td>";
		$body .= "<td rowspan='2'>ผจก.แผนก<br>อนุมัติเบิก</td>";
		$body .= "<td rowspan='2' width='12%'>หมายเหตุ</td>";
		$body .= "</tr>";

		$body .= "<tr>";
		$body .= "<td width='3%'>เข้า</td><td width='3%'>ออก</td>";
		$body .= "<td width='3%'>เริ่ม</td><td width='3%'>ถึง</td>";
		$body .= "<td width='3%'>1</td><td width='3%'>1.5</td><td width='3%'>3</td>";
		$body .= "</tr>";
		//body
		$total_ot100 = 0.00;
		$total_ot150 = 0.00;
		$total_ot300 = 0.00;

		$total_expenses_paid = 0.00;

		foreach ($data['data'] as $ot_item) {
			$body .= "<tr>";
			$body .= "<td>".$ot_item['request_date']."</td>";
			$body .= "<td>".$ot_item['req_todo_date']."</td>";
			$body .= "<td>".$this->GetStringWeekDay($ot_item['weekday'])."</td>";
			$body .= "<td>".$ot_item['checkin_time']."</td>";
			$body .= "<td>".$ot_item['checkout_time']."</td>";
			$body .= "<td>".$ot_item['time_start']."</td>";
			$body .= "<td>".$ot_item['time_end']."</td>";
			$body .= "<td>".$ot_item['detail']."</td>";
			if($ot_item['code_site'] != null )
			{
				$body .= "<td>".$ot_item['code_site']."</td>";
			}else{
				$body .= "<td>". $header['dep_name'] ."</td>";
			}

			$req_f = floatval($ot_item['req_hour'].".".$ot_item['req_min']);
			$req_fh = intval($ot_item['req_hour'])*60;
			$req_fm = intval($ot_item['req_min']);
			$req = sprintf("%.2f", $req_f) ;

			if ($ot_item['expenses_paid'] <> null || $ot_item['expenses_paid'] <> '') {
					$body .= "<td>&nbsp;</td>";
					$body .= "<td>&nbsp;</td>";
					$body .= "<td>&nbsp;</td>";
			}else{
				if($ot_item['multiple'] == '1.00')
				{
					$body .= "<td>".$req."</td>";
					$body .= "<td>&nbsp;</td>";
					$body .= "<td>&nbsp;</td>";
					$total_ot100 += ($req_fh + $req_fm);
				}else if($ot_item['multiple'] == '1.50')
				{
					$body .= "<td>&nbsp;</td>";
					$body .= "<td>".$req."</td>";
					$body .= "<td>&nbsp;</td>";
					$total_ot150 += ($req_fh + $req_fm);
				}else if ($ot_item['multiple'] == '3.00'){
					$body .= "<td>&nbsp;</td>";
					$body .= "<td>&nbsp;</td>";
					$body .= "<td>".$req."</td>";
					$total_ot300 += ($req_fh + $req_fm);
				}
			}

			$body .= "<td>".$ot_item['expenses_paid']."</td>"; //เหมาจ่าย
			$total_expenses_paid += $ot_item['expenses_paid'];
			
			$body .= "<td>".$ot_item['sign_emp']."</td>";

			if($ot_item['sign_super'] != null)
			{
				if ($ot_item['multiple'] == '3.00') {
					$body .= "<td>&nbsp;</td>";
				}else{
					$body .= "<td>".$ot_item['sign_super']."</td>";
				}			
			}else{
				$body .= "<td>&nbsp;</td>";
			}

			if ($ot_item['multiple'] == '3.00') {
				$body .= "<td>".$ot_item['sign_super']."</td>";
			}else{
				$body .= "<td>&nbsp;</td>";
			}
			
			$body .= "<td>".$ot_item['sign_paid']."</td>";
			if ($ot_item['multiple'] == '3.00') {
				$body .= "<td>". $ot_item['sign_paid'] ."</td>";
			}else{
				$body .= "<td>&nbsp;</td>";
			}
			
			$body .= "<td>&nbsp;</td>";
			$body .= "</tr>";
		}

		//body summary
		$body .= "<tr>";
		$body .= "<td colspan='7' class='text-right'>รวม</td>";
		// $body .= "<td>&nbsp;</td>";
		// $body .= "<td>&nbsp;</td>";
		// $body .= "<td>&nbsp;</td>";
		// $body .= "<td>&nbsp;</td>";
		// $body .= "<td>&nbsp;</td>";		
		// $body .= "<td>&nbsp;</td>";
		$body .= "<td>&nbsp;</td>";
		$body .= "<td>&nbsp;</td>";
		$body .= "<td>".floor($total_ot100/60).".".($total_ot100 % 60)."</td>";//1.0
		$body .= "<td>".floor($total_ot150/60).".".($total_ot150 % 60)."</td>";//1.50
		$body .= "<td>".floor($total_ot300/60).".".($total_ot300 % 60)."</td>";//3.0		
		$body .= "<td>". $total_expenses_paid ."</td>";
		$body .= "<td>&nbsp;</td>";
		$body .= "<td>&nbsp;</td>";
		$body .= "<td>&nbsp;</td>";
		$body .= "<td>&nbsp;</td>";
		$body .= "<td>&nbsp;</td>";
		$body .= "<td>&nbsp;</td>";
		$body .= "</tr>";

		$body .= "</table>";

		
		return $body;
	}

	private function notFoundBody()
	{
		$body = "";
		$body .= "<table>";
		$body .= "<tr>";
		$body .= "<td>";
		$body .= "ไม่พบข้อมูลการขอ OT ของพนักงาน";
		$body .= "</td>";
		$body .= "</tr>";
		$body .= "</table>";

		return $body;
	}

	private function GetStringWeekDay($day)
	{
		switch ($day) {
				case '0':
				return 'จ.';
				case '1':
				return 'อ.';
				case '2':
				return 'พ.';
				case '3':
				return 'พฤ.';
				case '4':
				return 'ศ.';
				case '5':
				return 'ส.';
				case '6':
				return 'อ.';

			default:
				return '-';
		}
	}


}
