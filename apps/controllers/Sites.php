<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sites extends MY_Controller {

	private $models;
	public function __construct(){
			 	 parent::__construct();
				 $this->models = $this->Site;
	}

	public function index()
	{
		$this->data["pageName"] = "ไซต์และสาขา";
		$this->data["pageactive"] = "<li><a href='".base_url("sites")."' class='active'>ไซต์และสาขา</a></li>";
		$this->middle = 'sites/index';
		$this->data["CSS_asset"]  = "<link href='".base_url("assets/plugins/bower_components/clockpicker/dist/jquery-clockpicker.min.css")."'   rel='stylesheet' type='text/css'  />";
		$this->data["JS_asset"]   = "<script src='".base_url("assets/js/cbpFWTabs.js")."'></script>";
		$this->data["JS_asset"]  .= "<script src='".base_url("assets/js/apps/JSite.js")."'></script>";
		$this->data["JS_asset"]  .= "<script src='".base_url("assets/js/apps/JSiteHelpers.js")."'></script>";
		$this->data["JS_asset"]  .= "<script src='".base_url("assets/plugins/bower_components/moment/moment.js")."'></script>";
		$this->data["JS_asset"]  .= "<script src='".base_url("assets/plugins/bower_components/clockpicker/dist/jquery-clockpicker.min.js")."'></script>";
		$this->data["JS_asset"]  .= "<script src='https://maps.googleapis.com/maps/api/js?key=AIzaSyCZKhEPx14ken6ISjjnQT0K4tZ7nlNaJjU&libraries=places' async defer></script>";
		$this->layouts();
	}

	public function load(){
			$arraysite = json_decode($_POST["obj"],true);
			print_r($arraysite);
			$this->models->company = 3;
			$this->models->auth = "20170000001";
			$load = $this->models->getAll();
			$sites = $this->generateTable($load);
		 	// echo json_encode($sites);
		 	return;
	}

	public function updateLocation(){
			$site =  base64_decode(base64_decode(base64_decode($_POST["segment"])));
			$this->models->lat = $_POST["lat"];
			$this->models->lng = $_POST["lng"];
			$this->models->area = $_POST["area"];
			$this->models->id = $site;
 		  $this->models->auth = "20170000001";
			$update = $this->models->updateLocation();
			$callback = array(
 		 		"success" => false,
 		 		"title" => "ผิดพลาด",
 		 		"msg" => "ไม่สามารถบันทึกข้อมูลได้ กรุณาลองใหม่ในภายหลัง"
 		 );
 		 if($update) {
 		 	 $callback = array(
 		 			"success" => true,
 		 			"title" => "สำเร็จ",
 		 			"msg" => "แก้ไขสถานที่สำเร็จ"
 		 	 );
 		 }
 		 echo json_encode($callback);
 		 return;
	}

	public function updateName(){
		 $site =  base64_decode(base64_decode(base64_decode($_POST["segment"])));
		 $name = $_POST["name"];
		 $this->models->name = $name;
		 $this->models->id = $site;
		 $this->models->auth = "20170000001";
		 $update = $this->models->updateName();
		 $callback = array(
		 		"success" => false,
		 		"title" => "ผิดพลาด",
		 		"msg" => "ไม่สามารถบันทึกข้อมูลได้ กรุณาลองใหม่ในภายหลัง"
		 );
		 if($update) {
		 	 $callback = array(
		 			"success" => true,
		 			"title" => "สำเร็จ",
		 			"msg" => "แก้ไขชื่อไซต์สำเร็จ"
		 	 );
		 }
		 echo json_encode($callback);
		 return;
	}

	public function Edit(){
		 $site =  base64_decode(base64_decode(base64_decode($this->uri->segments[3])));
		 $this->models->id = $site;
		 $detail = $this->models->getBy();
		 if (!$detail["Has"]) {
		 		 exit("<script>alert('ไม่พบไซต์ที่คุณเลือก'); history.back();</script>");
		 }else{
			 	 $this->data["site"] = $site;
			 	 $this->data["detail"] = $detail["data"][0];
				 $this->data["pageName"] = $detail["data"][0]["siteName"];
				 $this->data["pageSegment"] = $this->uri->segments[3];
			 	 $this->data["pageactive"] = "<li><a href='".base_url("sites")."' >ไซต์และสาขา</a></li> <li><a href='".base_url("sites/Edit/".$this->uri->segments[3])."' class='active'>".$detail["data"][0]["siteName"]."</a></li>";
			 	 $this->middle = 'sites/edit';
			 	 $this->data["CSS_asset"]  = "<link href='".base_url("assets/plugins/bower_components/clockpicker/dist/jquery-clockpicker.min.css")."'   rel='stylesheet' type='text/css'  />";
				 $this->data["CSS_asset"] .= "<link href='".base_url("assets/plugins/bower_components/switchery/dist/switchery.min.css")."'   rel='stylesheet' type='text/css'  />";
				  $this->data["CSS_asset"] .= "<link href='".base_url("assets/css/jquery.auto-complete.css")."'   rel='stylesheet' type='text/css'  />";

			 	 $this->data["JS_asset"]   = "<script src='".base_url("assets/js/cbpFWTabs.js")."'></script>";
			 	 $this->data["JS_asset"]  .= "<script src='".base_url("assets/plugins/bower_components/switchery/dist/switchery.min.js")."'></script>";
			 	 $this->data["JS_asset"]  .= "<script src='".base_url("assets/plugins/bower_components/moment/moment.js")."'></script>";
				 $this->data["JS_asset"]  .= "<script src='https://maps.googleapis.com/maps/api/js?key=AIzaSyCZKhEPx14ken6ISjjnQT0K4tZ7nlNaJjU&libraries=places' async defer></script>";
				 $this->data["JS_asset"]  .= "<script src='".base_url("assets/js/apps/JSiteHelpers.js")."'></script>";
				 $this->data["JS_asset"]  .= "<script src='".base_url("assets/js/apps/JSiteEdit.js")."'></script>";
				 $this->data["JS_asset"]  .= "<script src='".base_url("assets/js/apps/JHoliday.js")."'></script>";
				 $this->data["JS_asset"]  .= "<script src='".base_url("assets/js/apps/JOt.js")."'></script>";
				 $this->data["JS_asset"]  .= "<script src='".base_url("assets/js/apps/JBranche.js")."'></script>";
				 $this->data["JS_asset"]  .= "<script src='".base_url("assets/js/apps/JLeave.js")."'></script>";
 			 	 $this->data["JS_asset"]  .= "<script src='".base_url("assets/plugins/bower_components/clockpicker/dist/jquery-clockpicker.min.js")."'></script>";
				 $this->data["JS_asset"]  .= "<script src='".base_url("assets/js/jquery.auto-complete.js")."'></script>";
			 	 $this->layouts();
		 }
	}

	private function generateTable($data){
	    $tables = array();
	    $tables["table"] = array();
	    $tables["data"] = array();
	    $tables["Has"] = $data["Has"];
	    if ($data["Has"]) {
	        $num = 1;
	        for ($i=0; $i < count($data["data"]); $i++) {
							 $siteCode = base64_encode(base64_encode(base64_encode($data["data"][$i]["site_id"])));
	             $tr  = "";
	             $tr .= "<tr>";
	             $tr .= "<td  class='text-center'>";
	             $tr .= $num;
	             $tr .= "</td>";
	             $tr .= "<td>";
	             $tr .= $data["data"][$i]["siteName"];
	             $tr .= "</td>";
	             $tr .= "<td class='text-center'>";
	             $tr .= "<div class='btn-group'>";
	             $tr .= "<button class='btn btn-sm btn-default btn-outline waves-effect btn-site-edit' data-edit='".$siteCode."'><span class='fa fa-edit'></span></button>";
	             $tr .= "<button class='btn btn-sm btn-default btn-outline waves-effect'><span class='fa fa-trash'></span></button>";
	             $tr .= "</div>";
	             $tr .= "</td>";
	             $tr .= "</tr>";
	             array_push($tables["table"],$tr);
	             array_push($tables["data"],
	                array(
	                  "num"=>$num,
	                  "id"=>$data["data"][$i]["site_id"],
	                  "name"=>$data["data"][$i]["siteName"],
	              ));
	              $num ++;
	        }

	    }
	    return $tables;
	}

	public function addWorking(){
		$site =  base64_decode(base64_decode(base64_decode($_POST["segment"])));
		$this->models->id = $site;
		$this->models->company = $_POST["company_id"];
		$this->models->auth = "20170000001";
		$wk = $_POST["wk"];
		for ($i=0; $i < count($wk); $i++) {
					$getNo = $this->models->getLastNo();
					$newNo = intval($getNo[0]["no"]);
					$newNo ++;
					$data_setting = array(
						 "site_id" => $site,
						 "no" => $newNo,
						 "site_setting_status" => 1,
						 "wk_id" => $wk[$i],
						 "ot_id" => 0,
						 "hd_id" => 0
					);
					$add_setting = $this->db->insert('ms_site_setting',$data_setting);
		}
		$callback = array(
				"success" => false,
				"title" => "ผิดพลาด",
				"msg" => "ไม่สามารถบันทึกข้อมูลได้ กรุณาลองใหม่ในภายหลัง"
		);
		if($add_setting) {
			 $callback = array(
					"success" => true,
					"title" => "สำเร็จ",
					"msg" => "บันทึกข้อมูลวันทำการสำเร็จ"
			 );
		}
		echo json_encode($callback);
		return;
	}

	public function create(){
			$this->models->company = 3;
			$this->models->auth = "20170000001";
			$add = $this->models->add($_POST);
			$callback = array(
				"success" => false,
				"title" => "ผิดพลาด",
				"msg" => "ไม่สามารถบันทึกข้อมูลได้ กรุณาลองใหม่ในภายหลัง"
			);
			if($add){
				$callback = array(
					"success" => true,
					"title" => "สำเร็จ",
					"msg" => "บันทึกข้อมูลไซต์สำเร็จ"
				);
			}
			echo json_encode($callback);
	}

	public function ModalCreate(){
			$this->load->view("sites/create");
	}

	public function getchip(){
			$chip = array();
			$str_chip = "";
			if (isset($_POST["chip"])) {
					$chip = $_POST["chip"];
					$str_chip = "(".implode(",",$chip).")";
			}
			$person = $this->Persons;
			$person->company = $this->auth[0]["company_id"];
			$str = $_POST["str"];
			$array = explode(" ",$str);
			$count = count($array);
			$str_1 = "";
			$str_2 = "";
			if ($count > 1) {
					$str_1 = $array[0];
					$str_2 = $array[1];
					for ($i=2; $i < count($array); $i++) {
								 $str_2 .= " ".$array[$i];
					}
			}else{
				 $str_1 = $array[0];
			}
			$data = $person->getEmpChip($str_1,$str_2,$str_chip);
			echo json_encode($data);
			return;
	}

}
