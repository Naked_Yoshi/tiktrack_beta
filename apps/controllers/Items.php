<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Items extends MY_Controller {

	private $models;
	public function __construct(){
			 parent::__construct();
			 $this->models = $this->Item;
	}



	public function optionTitle(){
			$title = $this->models->getTitle();
			$option = array();
			for ($i=0; $i < count($title); $i++) {
					$op = "<option id='op_title$i' value='".$title[$i]["title_id"]."' data-index='$i'>".$title[$i]["title_th"]."</option>";
					array_push($option,$op);
			}
			$callback = array(
					"title" => $title,
					"options" => $option
			);
			echo json_encode($callback);
			return;
	}

}
