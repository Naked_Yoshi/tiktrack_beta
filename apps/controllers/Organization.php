<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Organization extends MY_Controller {
		private $auth ;
		private $company ;
		public $id;
		public $key;
			private $org = array();
			private $root;


	public function __construct(){
			 parent::__construct();
			 $this->models = $this->Position;
	}
	public function index()
	{
		// $data["auth"] =  $this->auth;
		// $data["company"] = $this->company;

			$this->data["pageName"] = "จัดการองค์กร";
			$this->data["pageactive"] = "<li><a href='".base_url("organization")."' class='active'>จัดการองค์กร</a></li>";
			$this->middle = 'organization/index';
			$this->data["JS_asset"]   = "<script src='".base_url("assets/js/cbpFWTabs.js")."'></script>";
			 // $this->data["JS_asset"]   = "<script src='".base_url("assets/js/organization.js")."'></script>";
			$this->data["JS_asset"]  .= "<script src='".base_url("assets/js/apps/JOrganization.js")."'></script>";
//   <script src="../plugins/bower_components/bootstrap-table/dist/bootstrap-table.min.js"></script>
    // <script src="../plugins/bower_components/bootstrap-table/dist/bootstrap-table.ints.js"></script>

			$this->layouts();
	}

	public function add(){
			$this->load->view("users/create");
	}

	public function getOrg(){
  $position = $this->Position;
	$company_name = $this->input->post("company_name");
	 $this->position = $this->auth[0]["company_id"];
	 $this->models->company = $this->input->post("company_id");
	 $position->firstRoot = 0;
	 if (isset($_POST["item"]) && $_POST["item"] != "") {
			$department = $this->Department;
			$department->company = $this->auth[0]["company_id"];
			$detail = $department->getDepartments("",$_POST["item"]);
			$company_obj = array(
										 array(
										 "v" => "company",
										 "f" => $detail["Department"][0]["dep_name"],
									 ),"",$detail["Department"][0]["dep_name"],
								 );
			array_push($this->org,$company_obj);
			$root = $position->getrootPosition($_POST["item"]);
	 }else{
			$company_obj = array(
										 array(
										 "v" => "company",
										 "f" => $company_name,
									 ),"", $company_name,
								 );
			 array_push($this->org,$company_obj);
			$root = $position->getrootPosition();
	 }
	 if ($root["hasRoot"]) {
			 for ($r=0; $r < count($root["Root"]); $r++) {
						$head = $root["Root"][$r];
						$approve = "";
						if ($head["approve_ot"] != 0) {
								$approve = "<span class='fa fa-check-circle tooltipped'  data-position='top' data-tooltip='มีสิทธิ์การอนุมัติลงเวลา'></span> ";
						}
						if ($head["position_root"] =='0'){
								$head_obj = array( array("v" => $head["position_id"],"f" => $head["position_name"]." $approve "),"company" , $head["position_name"]);
						}else {
									$head_obj = array( array("v" => $head["position_id"],"f" => $head["position_name"]." $approve "),$head["position_root"] , $head["position_name"]);
						}

						array_push($this->org,$head_obj);
						$this->getsubPosition($head["position_id"]);

			 }

	 }
	 echo json_encode($this->org);
	 return;
}
public function getsubPosition($root){
			$position = $this->Position;
			$position->company = $this->auth[0]["company_id"];
			$position->root = $root;
			$root_obj = $position->getsubPosition();
			if ($root_obj["hasPosition"]) {
				 for ($r=0; $r < count($root_obj["Position"]); $r++) {
							 $head = $root_obj["Position"][$r];
							 $approve = "";
	 							if ($head["approve_ot"] != 0) {
										$approve = "<span class='fa fa-check-circle tooltipped'  data-position='top' data-tooltip='มีสิทธิ์การอนุมัติลงเวลา'></span> ";
	 							}
							 $head_obj = array( array("v" => $head["position_id"],"f" => $head["position_name"]." $approve "),$roots, $head["position_name"]);
							 $this->getsubPosition($head["position_id"]);
							 array_push($this->org,$head_obj);
				 }
			}
			return;
	}
}
