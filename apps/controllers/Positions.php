<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Positions extends MY_Controller {

	private $models;
	public function __construct(){
			 parent::__construct();
			 $this->models = $this->Position;

	}
	public function index()
	{

	}

	public function load(){
			$this->models->company = 3;
			$load = $this->models->getAll();
			$table = $this->generateTable($load);
			echo json_encode($table);
			return;
	}

	public function ModalCreate(){
			$this->load->view("position/create");
	}
	public function ModalEdit(){
			$this->load->view("position/edit");
	}

	public function create(){
			$this->models->company = 3;
			$this->models->auth = "20170000001";
			$add = $this->models->add($_POST);
			$callback = array(
				"success" => false,
				"title" => "ผิดพลาด",
				"msg" => "ไม่สามารถบันทึกข้อมูลได้ กรุณาลองใหม่ในภายหลัง"
			);
			if($add){
				$callback = array(
					"success" => true,
					"title" => "สำเร็จ",
					"msg" => "บันทึกข้อมูลตำแหน่งสำเร็จ"
				);
			}
			echo json_encode($callback);
			return;
	}



	public function options(){
		$this->models->company = 3;
		$str = "";
		$root = "";
		 // print_r($_POST);
		if (isset($_POST["dep"])) {
						$str = $_POST["dep"];
		}
		if (isset($_POST["root"])) {
				$root = $_POST["root"];
		}

		$load = $this->models->getAll($str,$root);
 // print_r($_POST);
		$options = $this->generateOption($load,$str,$root);
	 	echo json_encode($options);
	 	return;
	}

	private function generateTable($data){
			$tables = array();
			$tables["table"] = array();
			$tables["data"] = array();
			$tables["Has"] = $data["Has"];
			if ($data["Has"]) {
					$num = 1;
					for ($i=0; $i < count($data["data"]); $i++) {
							 $boss = "";
							 $root = $data["data"][$i]["position_root"];
							 if ($root != 0) {
							 		$data_boss = $this->models->getBy($root);
									if ($data_boss["Has"]) {
											$boss = $data_boss["detail"][0]["position_name"];
									}else{
										  $boss = "-";
									}
							 }else{
								 		$boss = "<span class='boss-text'>หัวหน้าสูงสุด</span>";
							 }
							 $tr  = "";
							 $tr .= "<tr>";
							 $tr .= "<td  class='text-center'>";
							 $tr .= $num;
							 $tr .= "</td>";
							 $tr .= "<td>";
							 $tr .= $data["data"][$i]["position_name"];
							 $tr .= "</td>";
							 $tr .= "<td>";
							 $tr .= $data["data"][$i]["dep_name"];
							 $tr .= "</td>";
							 $tr .= "<td>";
							 $tr .= $boss;
							 $tr .= "</td>";
							 $tr .= "<td width='100'>";
							 $tr .= "<div class='btn-group'>";
							 $tr .= "<button class='btn btn-sm btn-default btn-outline waves-effect btn-edit-position' onclick='edit_position(this)' data-position_id='". $data["data"][$i]["position_id"] ."'><span class='fa fa-edit'></span></button>";
							 $tr .= "<button class='btn btn-sm btn-default btn-outline waves-effect btn-delete-position' data-position_id='". $data["data"][$i]["position_id"] ."'><span class='fa fa-trash'></span></button>";
							$tr .= "<button class='btn btn-sm btn-default btn-outline waves-effect btn-detail-position'><span class='fa fa-eye'></span></button>";
							 $tr .= "</div>";
							 $tr .= "</td>";
							 $tr .= "</tr>";
							 array_push($tables["table"],$tr);
							 array_push($tables["data"],
									array(
										"num"=>$num,
										"id"=>$data["data"][$i]["position_id"],
										"name"=>$data["data"][$i]["position_name"],
								));
								$num ++;
					}

			}
			return $tables;
	}
	private function generateOption($data,$str = "",$root = ""){
			$options = array();
			$options["option"] = array();
				if ($str != "getOT") {
						$op  = "<option value='0'>";
						$op .= "ระดับสูงสุด";
						$op .= "</option>";
				}else{
						$op  = "<option value='0'>";
						$op .= "เลือกตำแหน่ง";
						$op .= "</option>";
				}

				$options["Has"] = $data["Has"];
				if ($data["Has"]) {
						for ($i=0; $i < count($data["data"]); $i++) {
								 $op  = "";
								 $str_select = "";
								 if ($root ==  $data["data"][$i]["position_id"]) {
								 			$str_select = "selected";
								 }
								 $op .= "<option value='".$data["data"][$i]["position_id"]."'  $str_select>";
								 $op .= $data["data"][$i]["position_name"];
								 $op .= "</option>";
								 array_push($options["option"],$op);
						}
				}
			return $options;
	}


}
