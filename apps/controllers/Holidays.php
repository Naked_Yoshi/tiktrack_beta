<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Holidays extends MY_Controller {

	private $models;
	public function __construct(){
			 parent::__construct();
			  $this->models = $this->Holiday;
	}

	public function index(){


	}

	public function load(){
			$this->models->company = 3;
			$site = "";
			if (isset($_POST["segment"])) {
				  $site = $_POST["segment"];
			}
			$branche = "";
			if (isset($_POST["branche"])) {
				  $branche = $_POST["branche"];
			}
			$obj = array("segment"=>$site,"branche"=>$branche);
			$all = $this->models->getAll($obj);
			$holiday = $this->generateTable($all);
			echo json_encode($holiday);
	 	  return;

	}

	public function ModalCreate(){
			$this->load->view("holiday/create");
	}

	public function ModalEdit(){
			$this->load->view("holiday/edit");
	}


	  public function getlinkhd(){
	    $this->load->view("working/link_hd");
	  }

	public function add(){
			$this->models->company = 3;
			$this->models->auth = "20170000001";
			$add = $this->models->add($_POST);
			$callback = array(
					"success" => false,
					"title" => "ผิดพลาด",
					"msg" => "ไม่สามารถบันทึกข้อมูลได้ กรุณาลองใหม่ในภายหลัง"
			);
			if($add){
				$callback = array(
					"success" => true,
					"title" => "สำเร็จ",
					"msg" => "บันทึกข้อมูลวันหยุดประจำปีสำเร็จ"
				);
			}
			echo json_encode($callback);
			return;
	}
	public function getDetail(){
			$hd = $_POST["hd"];
			$config = $this->models->getDetailBy($hd);
			$data["detail"] = $config;
			$this->load->view("working/link_hd",$data);
	}
	private function generateTable($data){
			$tables = array();
			$tables["table"] = array();
			$tables["data"] = array();
			$tables["Has"] = $data["Has"];
			if ($data["Has"]) {
					$num = 1;
					for ($i=0; $i < count($data["data"]); $i++) {
							 $tr  = "";
							 $tr .= "<tr>";
							 $tr .= "<td class='text-center'>";
							 $tr .= $num;
							 $tr .= "</td>";
							 $tr .= "<td>";
							 $tr .= $data["data"][$i]["hd_name"];
							 $tr .= "</td>";
							 $tr .= "<td width='100'>";
							 $tr .= "<div class='btn-group'>";
							 $tr .= "<button class='btn btn-sm btn-default btn-outline waves-effect btn-edit-holiday' data-detail=''><span class='fa fa-edit'></span></button>";
							 $tr .= "<button class='btn btn-sm btn-default btn-outline waves-effect'><span class='fa fa-trash'></span></button>";
							 $tr .= "<button class='btn btn-sm btn-default btn-outline waves-effect'><span class='fa fa-search'></span></button>";
							 $tr .= "</div>";
							 $tr .= "</td>";
							 $tr .= "</tr>";
							 array_push($tables["table"],$tr);
							 array_push($tables["data"],
							 		array(
										"num"=>$num,
										"id"=>$data["data"][$i]["hd_id"],
										"name"=>$data["data"][$i]["hd_name"],
								));
								$num ++;
					}

			}
			return $tables;
	}

}
