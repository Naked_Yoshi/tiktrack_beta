<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Branches extends MY_Controller {

	private $models;
	public function __construct(){
			 	 parent::__construct();
				 $this->models = $this->Branche;
	}

	public function index()
	{
		// $this->data["pageName"] = "ไซต์และสาขา";
		// $this->data["pageactive"] = "<li><a href='".base_url("sites")."' class='active'>ไซต์และสาขา</a></li>";
		// $this->middle = 'sites/index';
		// $this->data["CSS_asset"]  = "<link href='".base_url("assets/plugins/bower_components/clockpicker/dist/jquery-clockpicker.min.css")."'   rel='stylesheet' type='text/css'  />";
		// $this->data["JS_asset"]   = "<script src='".base_url("assets/js/cbpFWTabs.js")."'></script>";
		// $this->data["JS_asset"]  .= "<script src='".base_url("assets/js/apps/JSite.js")."'></script>";
		// $this->data["JS_asset"]  .= "<script src='".base_url("assets/js/apps/JSiteHelpers.js")."'></script>";
		// $this->data["JS_asset"]  .= "<script src='".base_url("assets/plugins/bower_components/moment/moment.js")."'></script>";
		// $this->data["JS_asset"]  .= "<script src='".base_url("assets/plugins/bower_components/clockpicker/dist/jquery-clockpicker.min.js")."'></script>";
		// $this->data["JS_asset"]  .= "<script src='https://maps.googleapis.com/maps/api/js?key=AIzaSyCZKhEPx14ken6ISjjnQT0K4tZ7nlNaJjU&libraries=places' async defer></script>";
		// $this->layouts();
	}

	public function Edit(){
		 $site =  base64_decode(base64_decode(base64_decode($this->uri->segments[3])));
		 $branche = base64_decode(base64_decode(base64_decode($this->uri->segments[4])));
		 $this->models->id = $site;
		 $this->models->branche_id = $branche;
		 $detail = $this->models->getBy();
		 if (!$detail["Has"]) {
		 		 exit("<script>alert('ไม่พบไซต์ที่คุณเลือก'); history.back();</script>");
		 }else{
			 	 $this->data["site"] = $site;
				 $this->data["branche"] = $site;
			 	 $this->data["detail"] = $detail["data"][0];
				 $this->data["pageName"] = $detail["data"][0]["brancheName"];
				 $this->data["pageSegment"] = $this->uri->segments[3];
			 	 $this->data["pageactive"] = "<li><a href='".base_url("sites")."' >ไซต์และสาขา</a></li> <li><a href='".base_url("sites/Edit/".$this->uri->segments[3])."' >".$detail["data"][0]["siteName"]."</a></li> <li><a href='".base_url("branches/Edit/".$this->uri->segments[4])."' class='active'>".$detail["data"][0]["brancheName"]."</a></li>";
			 	 $this->middle = 'branche/edit';
			 	 $this->data["CSS_asset"]  = "<link href='".base_url("assets/plugins/bower_components/clockpicker/dist/jquery-clockpicker.min.css")."'   rel='stylesheet' type='text/css'  />";
				 $this->data["CSS_asset"] .= "<link href='".base_url("assets/plugins/bower_components/switchery/dist/switchery.min.css")."'   rel='stylesheet' type='text/css'  />";
				  $this->data["CSS_asset"] .= "<link href='".base_url("assets/css/jquery.auto-complete.css")."'   rel='stylesheet' type='text/css'  />";

			 	 $this->data["JS_asset"]   = "<script src='".base_url("assets/js/cbpFWTabs.js")."'></script>";
			 	 $this->data["JS_asset"]  .= "<script src='".base_url("assets/plugins/bower_components/switchery/dist/switchery.min.js")."'></script>";
			 	 $this->data["JS_asset"]  .= "<script src='".base_url("assets/plugins/bower_components/moment/moment.js")."'></script>";
				 $this->data["JS_asset"]  .= "<script src='https://maps.googleapis.com/maps/api/js?key=AIzaSyCZKhEPx14ken6ISjjnQT0K4tZ7nlNaJjU&libraries=places' async defer></script>";
				 $this->data["JS_asset"]  .= "<script src='".base_url("assets/js/apps/JBrancheHelpers.js")."'></script>";
				 $this->data["JS_asset"]  .= "<script src='".base_url("assets/js/apps/JBrancheEdit.js")."'></script>";
				 $this->data["JS_asset"]  .= "<script src='".base_url("assets/js/apps/JHoliday.js")."'></script>";
				 $this->data["JS_asset"]  .= "<script src='".base_url("assets/js/apps/JOt.js")."'></script>";
				 $this->data["JS_asset"]  .= "<script src='".base_url("assets/js/apps/JBranche.js")."'></script>";
				 $this->data["JS_asset"]  .= "<script src='".base_url("assets/js/apps/JLeaveBranche.js")."'></script>";
 			 	 $this->data["JS_asset"]  .= "<script src='".base_url("assets/plugins/bower_components/clockpicker/dist/jquery-clockpicker.min.js")."'></script>";
				 $this->data["JS_asset"]  .= "<script src='".base_url("assets/js/jquery.auto-complete.js")."'></script>";
			 	 $this->layouts();
		 }
	}

	public function load(){
			$segment = base64_decode(base64_decode(base64_decode($_POST["segment"])));
			$this->models->site = $segment;
			$branch = $this->models->getAll();
			$branche = $this->generateTable($branch);
			echo json_encode($branche);
			return;
	}

	public function ModalCreate(){
			$segment = base64_decode(base64_decode(base64_decode($_POST["segment"])));
			$site = $this->Site;
			$site->id = $segment;
			$data["site"] = $site->getBy();
			$data["site"]["segment"] = $_POST["segment"];
			$this->load->view("branche/create",$data);
	}

	public function create(){
			$this->models->company = 3;
			$this->models->auth = "20170000001";
			$add = $this->models->add($_POST);
			$callback = array(
					"success" => false,
					"title" => "ผิดพลาด",
					"msg" => "ไม่สามารถบันทึกข้อมูลได้ กรุณาลองใหม่ในภายหลัง"
			);
			if($add){
				$callback = array(
					"success" => true,
					"title" => "สำเร็จ",
					"msg" => "บันทึกข้อมูลสาขาสำเร็จ"
				);
			}
			echo json_encode($callback);
			return;
	}

	private function generateTable($data){
				$tables = array();
				$tables["table"] = array();
				$tables["data"] = array();
				$tables["Has"] = $data["Has"];
				if ($data["Has"]) {
						$num = 1;
						$rows = $data["data"];
						for ($i=0; $i < count($rows); $i++) {
							 $tr  = "";
							 $tr .= "<tr>";
							 $tr .= "<td  class='text-center'>";
							 $tr .= $num;
							 $tr .= "</td>";
							 $tr .= "<td>";
							 $tr .= $rows[$i]["brancheName"];
							 $tr .= "</td>";
							 $tr .= "<td class='text-center'>";
							 $tr .= "<div class='btn-group'>";
							 $tr .= "<button class='btn btn-sm btn-default btn-outline waves-effect btn-site-edit' data-edit=''><span class='fa fa-edit'></span></button>";
							 $tr .= "<button class='btn btn-sm btn-default btn-outline waves-effect'><span class='fa fa-trash'></span></button>";
							 $tr .= "</div>";
							 $tr .= "</td>";
							 $tr .= "</tr>";
							 array_push($tables["table"],$tr);
							 array_push($tables["data"],
									array(
										"num"=>$num,
										"id"=> $rows[$i]["branche_id"],
										"name"=> $rows[$i]["brancheName"]
								));
								$num ++;
						}
						# code...
				}
				return $tables;
	}
}
