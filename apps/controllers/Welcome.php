<?php
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Headers: Origin, Content-Type, X-Auth-Token,token');
header('Access-Control-Allow-Header: Origin, Content-Type, X-Auth-Token,token');

header('Access-Control-Allow-Methods: POST,GET');

defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends MY_Controller {

	public function __construct(){
			 parent::__construct();
	}

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	public function index()
	{
		// $this->load->view('welcome_message');
		$this->load->view('home');
	}

	public function resetpass(){
		$this->data["pageSegment"] = $this->uri->segments[1];
		$this->load->view('resetpass');
	}

	// public function Approve(){
	// 	$this->data["pageSegment"] = $this->uri->segments[1];
	// 	$this->load->view('approved');
	// }

	public function firstchangepass(){
		$this->data["pageSegment"] = $this->uri->segments[1];
		$this->load->view('firstlogin');
	}

	public function chartEmp()
	{
		$this->load->view('chartemp');
	}

}
