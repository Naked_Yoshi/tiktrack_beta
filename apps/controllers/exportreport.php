<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once 'vendor/autoload.php';

class Exportreport extends MY_Controller {

	private $models;
	public function __construct(){
			 	 parent::__construct();
				 // $this->models = $this->Site;
	}

  public function index()
  {
    $this->data["pageName"] = "รายงาน & ส่งออกข้อมูล";
    $this->data["pageactive"] = "<li><a href='".base_url("exportreport")."' class='active'>รายงาน & ส่งออกข้อมูล</a></li>";
    $this->middle = 'exportreport/index';
    // $this->data["CSS_asset"]  = "<link href='".base_url("assets/plugins/bower_components/clockpicker/dist/jquery-clockpicker.min.css")."'   rel='stylesheet' type='text/css'  />";
    $this->data["JS_asset"]   = "<script src='".base_url("assets/js/cbpFWTabs.js")."'></script>";
    $this->data["JS_asset"]  .= "<script src='".base_url("assets/js/apps/exportreport.js")."'></script>";
    // $this->data["JS_asset"]  .= "<script src='".base_url("assets/js/apps/JSiteHelpers.js")."'></script>";
    $this->data["JS_asset"]  .= "<script src='".base_url("assets/plugins/bower_components/moment/moment.js")."'></script>";
    // $this->data["JS_asset"]  .= "<script src='".base_url("assets/plugins/bower_components/clockpicker/dist/jquery-clockpicker.min.js")."'></script>";
    // $this->data["JS_asset"]  .= "<script src='https://maps.googleapis.com/maps/api/js?key=AIzaSyCZKhEPx14ken6ISjjnQT0K4tZ7nlNaJjU&libraries=places' async defer></script>";
    $this->layouts();
  }
}
