<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Users extends MY_Controller {

	public function __construct(){
			 parent::__construct();
			 $this->models = $this->User;
	}
	public function index()
	{
			$this->data["pageName"] = "จัดการพนักงาน";
			$this->data["pageactive"] = "<li><a href='".base_url("users")."' class='active'>จัดการพนักงาน</a></li>";
			$this->data["JS_asset"]  = "<script src='".base_url("assets/js/apps/JUsers.js")."'></script>";
			$this->data["JS_asset"] .= "<script src='".base_url("assets/js/mask.js")."'></script>";
			$this->middle = 'users/index';
			$this->layouts();
	}

	public function add(){
			$this->load->view("users/create");
	}

	public function c_getnations(){
		$rows = $this->Helpers->getnation();

		echo json_encode($rows);

		return;
	}

	public function Edit(){
		$emp_id =  base64_decode(base64_decode(base64_decode($this->uri->segments[3])));
		$this->models->id = $emp_id;
		$detail = $this->models->getBy($emp_id);
		if (!$detail["Has"]) {
				exit("<script>alert('ไม่พบพนักงานที่คุณเลือก'); history.back();</script>");
		}else{
				// $this->data["site"] = $site;
				// $this->data["detail"] = $detail["data"][0];
				$this->data["pageName"] = "แก้ไขข้อมูลพนักงาน";
				$this->data["pageSegment"] = $this->uri->segments[3];
				$this->data["pageactive"] = "<li><a href='".base_url("users")."' >จัดการพนักงาน</a></li>";
				$this->middle = 'users/edit';
				$this->data["CSS_asset"]  = "<link href='".base_url("assets/plugins/bower_components/clockpicker/dist/jquery-clockpicker.min.css")."'   rel='stylesheet' type='text/css'  />";
				$this->data["CSS_asset"] .= "<link href='".base_url("assets/plugins/bower_components/switchery/dist/switchery.min.css")."'   rel='stylesheet' type='text/css'  />";
				 $this->data["CSS_asset"] .= "<link href='".base_url("assets/css/jquery.auto-complete.css")."'   rel='stylesheet' type='text/css'  />";

				$this->data["JS_asset"]   = "<script src='".base_url("assets/js/cbpFWTabs.js")."'></script>";
				$this->data["JS_asset"]  .= "<script src='".base_url("assets/plugins/bower_components/switchery/dist/switchery.min.js")."'></script>";
				$this->data["JS_asset"]  .= "<script src='".base_url("assets/plugins/bower_components/moment/moment.js")."'></script>";
				$this->data["JS_asset"]  .= "<script src='https://maps.googleapis.com/maps/api/js?key=AIzaSyCZKhEPx14ken6ISjjnQT0K4tZ7nlNaJjU&libraries=places' async defer></script>";
				// $this->data["JS_asset"]  .= "<script src='".base_url("assets/js/apps/JSiteHelpers.js")."'></script>";
				$this->data["JS_asset"]  .= "<script src='".base_url("assets/js/apps/JUserEdit.js")."'></script>";
				// $this->data["JS_asset"]  .= "<script src='".base_url("assets/js/apps/JHoliday.js")."'></script>";
				// $this->data["JS_asset"]  .= "<script src='".base_url("assets/js/apps/JOt.js")."'></script>";
				// $this->data["JS_asset"]  .= "<script src='".base_url("assets/js/apps/JBranche.js")."'></script>";
				$this->data["JS_asset"] .= "<script src='".base_url("assets/js/mask.js")."'></script>";
				$this->data["JS_asset"]  .= "<script src='".base_url("assets/plugins/bower_components/clockpicker/dist/jquery-clockpicker.min.js")."'></script>";
				$this->data["JS_asset"]  .= "<script src='".base_url("assets/js/jquery.auto-complete.js")."'></script>";
				$this->layouts();
		}
	}


	public function changepass(){
		$this->load->view("changepass");
	}

	public function modal_wk(){
		$this->load->view("sites/change_wk");
	}
}
