<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Chelpers extends MY_Controller {

	private $models;
	public function __construct(){
			 	 parent::__construct();

	}


public function decodeJWT(){
  $token = $this->input->POST["token"];
  $decode = AUTHORIZATION::validateToken($token);

  $callback = array('status'=>TRUE,'token' => $decode);
  return $callback;
}

public function getdatediff(){
	$timestamp = $this->input->POST["timestamp"];
	$covtimestamp = date('Y-m-d H:i:s',$timestamp);
	$timenow = date("Y-m-d H:i:s");
	// $timenow = date("2018-04-24 17:00:00");

	$start = strtotime($timestamp);
	$end   = strtotime($timenow);
	$diff  = $end - $start;

	// $hours = floor($diff / (60 * 60));
	// $minutes = $diff - $hours * (60 * 60);
	$minute = round((strtotime(date("H:i",strtotime($timenow))) - strtotime(date("H:i",strtotime($covtimestamp)))) /60);
	if ($minute >= 600) {
		$callback = array("chk"=>TRUE,"minute"=>$minute);
	}else{
		$callback = array("chk"=>FALSE,"minute"=>$minute);
	}

	// $hourdiff = round((strtotime($covtimestamp) - strtotime($timenow))/3600);
	// $callback = array("minute"=>$minute);
	echo json_encode($callback);
	return;
}

}
  ?>
