<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once 'vendor/autoload.php';

class CompanySetting extends MY_Controller {

	private $models;
	public function __construct(){
			 	 parent::__construct();
				 // $this->models = $this->Site;
	}

  public function index()
  {
    $this->data["pageName"] = "ตั้งค่าทั่วไป";
    $this->data["pageactive"] = "<li><a href='".base_url("companysetting")."' class='active'>รายงาน & ส่งออกข้อมูล</a></li>";
    $this->middle = 'companysetting/common_setting';
    // $this->data["CSS_asset"]  = "<link href='".base_url("assets/plugins/bower_components/clockpicker/dist/jquery-clockpicker.min.css")."'   rel='stylesheet' type='text/css'  />";
    $this->data["JS_asset"]   = "<script src='".base_url("assets/js/cbpFWTabs.js")."'></script>";
    $this->data["JS_asset"]  .= "<script src='".base_url("assets/js/apps/companysetting.js")."'></script>";
    // $this->data["JS_asset"]  .= "<script src='".base_url("assets/js/apps/JSiteHelpers.js")."'></script>";
    $this->data["JS_asset"]  .= "<script src='".base_url("assets/plugins/bower_components/moment/moment.js")."'></script>";
    // $this->data["JS_asset"]  .= "<script src='".base_url("assets/plugins/bower_components/clockpicker/dist/jquery-clockpicker.min.js")."'></script>";
    // $this->data["JS_asset"]  .= "<script src='https://maps.googleapis.com/maps/api/js?key=AIzaSyCZKhEPx14ken6ISjjnQT0K4tZ7nlNaJjU&libraries=places' async defer></script>";
    $this->layouts();
  }

	public function working_setting()
	{
		$this->data["pageName"] = "ตั้งค่าทั่วไป";
		$this->data["pageactive"] = "<li><a href='".base_url("companysetting/working_setting")."' class='active'>รายงาน & ส่งออกข้อมูล</a></li>";
		$this->middle = 'working/index_setting';
		$this->data["CSS_asset"]  = "<link href='".base_url("assets/plugins/bower_components/clockpicker/dist/jquery-clockpicker.min.css")."'   rel='stylesheet' type='text/css'  />";
		$this->data["JS_asset"]   = "<script src='".base_url("assets/js/cbpFWTabs.js")."'></script>";
		$this->data["JS_asset"]  .= "<script src='".base_url("assets/js/apps/CompanySetting/working_setting.js")."'></script>";
		$this->data["JS_asset"]  .= "<script src='".base_url("assets/plugins/bower_components/moment/moment.js")."'></script>";
		$this->data["JS_asset"]  .= "<script src='".base_url("assets/plugins/bower_components/clockpicker/dist/jquery-clockpicker.min.js")."'></script>";
		// $this->data["JS_asset"]  .= "<script src='https://maps.googleapis.com/maps/api/js?key=AIzaSyCZKhEPx14ken6ISjjnQT0K4tZ7nlNaJjU&libraries=places' async defer></script>";
		$this->layouts();
	}

	public function holiday_setting()
	{
		$this->data["pageName"] = "ตั้งค่าทั่วไป";
		$this->data["pageactive"] = "<li><a href='".base_url("companysetting/holiday_setting")."' class='active'>วันหยุดประจำปี</a></li>";
		$this->middle = 'holiday/index_setting';
		$this->data["CSS_asset"]  = "<link href='".base_url("assets/plugins/bower_components/clockpicker/dist/jquery-clockpicker.min.css")."'   rel='stylesheet' type='text/css'  />";
		$this->data["JS_asset"]   = "<script src='".base_url("assets/js/cbpFWTabs.js")."'></script>";
		$this->data["JS_asset"]  .= "<script src='".base_url("assets/js/apps/CompanySetting/holiday_setting.js")."'></script>";
		$this->data["JS_asset"]  .= "<script src='".base_url("assets/plugins/bower_components/moment/moment.js")."'></script>";
		$this->data["JS_asset"]  .= "<script src='".base_url("assets/plugins/bower_components/clockpicker/dist/jquery-clockpicker.min.js")."'></script>";
		// $this->data["JS_asset"]  .= "<script src='https://maps.googleapis.com/maps/api/js?key=AIzaSyCZKhEPx14ken6ISjjnQT0K4tZ7nlNaJjU&libraries=places' async defer></script>";
		$this->layouts();
	}

	public function createWorking(){
		$this->load->view("working/create_working");
	}

	public function editworking(){
		$this->load->view("working/edit_working");
	}




}
