<?php
defined('BASEPATH') OR exit('No direct script access allowed');
date_default_timezone_set("Asia/Bangkok");

require_once 'vendor/autoload.php';

class Report extends CI_Controller {
	private $auth ;
	private $th_month  = array("มกราคม","กุมภาพันธ์","มีนาคม","เมษายน","พฤษภาคม","มิถุนายน","กรกฎาคม","สิงหาคม","กันยายน","ตุลาคม","พฤศจิกายน","ธันวาคม");
	public function __construct(){
		 parent::__construct();
		 // $this->load->library('session');
		 // $this->load->helper('cookie'); // ใช้งาน  Cookie
		 // $this->load->library('user_agent');
		 // $this->load->helper('url');
		 // $this->functions->chkactive();
		 // $this->auth = $this->tiktrack_api->calluser($_SESSION["email"]);
		 // $this->company = $this->tiktrack_api->getcompany_detail($this->auth[0]["company_id"]);
	}

	public function config(){
			$this->load->view('attandance/report_config');
	}

	public function preview(){
		   $type = $this->input->post("selectedType");
			 $site = "";
			 $branch = $this->input->post("dep");

			 $date_start = $this->input->post("date_start");
			 $conv_date_start = str_replace('/', '-', $date_start);
			 $date_started = date("Y-m-d", strtotime($conv_date_start));

			 $date_end = $this->input->post("date_ended");
			 $conv_date_end = str_replace('/', '-', $date_end);
			 $date_ended = date("Y-m-d", strtotime($conv_date_end));


			 $month = sprintf("%02d",date("m",strtotime($date_start)));
			 $year = date("Y",strtotime($date_start));
			 // $year = substr($year,2,2);
			 $index = "";

			$defaultConfig = (new Mpdf\Config\ConfigVariables())->getDefaults();
 			$fontDirs = $defaultConfig['fontDir'];

 			$defaultFontConfig = (new Mpdf\Config\FontVariables())->getDefaults();
 			$fontData = $defaultFontConfig['fontdata'];
			 $mpdf = new \Mpdf\Mpdf([
 			    'fontDir' => array_merge($fontDirs, [
 			        __DIR__ . '/custom/font/th',
 			    ]),
 			    'fontdata' => $fontData + [
 			        'frutiger' => [
 			            'R' => 'THSarabunNew.ttf',
 			            'I' => 'THSarabunNew Italic.ttf',
 									'B' => 'THSarabunNew Bold.ttf',
 			        ]
 			    ],
 			    'default_font' => 'frutiger',
					'format' => 'A4-L',
					'debug' => true,
    			'allow_output_buffering' => true
 			]);


			if ($type == "0") {
					 $index = $_POST["index"];
					 // $tat = $this->tiktrack_api->fillter_tat("",$year."".$month,$this->company[0]["company_id"],"","",$index);
					 $mpdf->AddPage('','','','','',5,5,10,0,0,0);
					 $html = $this->htmlbody($month,$year,$branch,$date_started,$date_ended);
					 $mpdf->WriteHTML($html);
			}else{
				// $row = $this->tiktrack_api->getEmployee_search($this->company[0]["company_id"],$site,$branch);
					// for ($i=0; $i < count($row); $i++) {
							// $empid = $row[$i]["employeeId"];
							// $tat = $this->tiktrack_api->fillter_tat("",$year."".$month,$this->company[0]["company_id"],"","",$empid);
							$mpdf->AddPage('','','','','',5,5,10,0,0,0);
							$html = $this->htmlbody($month,$year,$branch,$date_started,$date_ended);
							$mpdf->WriteHTML($html);
					// }
			}
			// $mpdf->AddPage('','','','','',5,5,10,0,0,0);
			// 				$html = $this->htmlbody();
			// 				$mpdf->WriteHTML($html);
			$mpdf->Output();

	}
	// private function getrow($dd,$tat){
	// 		$callback = array();
	// 		$dd = doubleval($dd);
	// 		for ($i=0; $i < count($tat); $i++) {
	// 				$row_day = doubleval($tat[$i]["date"]);
	// 				if ($row_day == $dd) {
	// 						$callback = $tat[$i];
	// 				}
	// 		}
	// 		return $callback;
	// }
	//
	// private function AddPlayTime($times) {
	// 	$minutes = 0;
  //   for ($i=0; $i< count($times);$i++) {
	// 			$minute = 0;
	// 			$arr = explode(':', $times[$i]);
	// 			$hour = $arr[0];
	// 			if (isset($arr[1])) {
	// 					$minute = $arr[1];
	// 			}
  //       $minutes += $hour * 60;
  //       $minutes += $minute;
  //   }
	//
  //   $hours = floor($minutes / 60);
  //   $minutes -= $hours * 60;
  //   return sprintf('%02d:%02d', $hours, $minutes);
	// }
	//
	private function htmlbody($month,$year,$site,$date_start,$date_end){

		  // $month = doubleval($month)-1;
			$this->Branche->branche_id = $site;
			$branche = $this->Branche->getBy();
			$brancheName = $branche["data"][0]["brancheName"];
			// $emp_data = $this->User->getBy($emp);
			// $title  = $this->Helpers->get_title($emp_data["data"][0]["title_id"]);


			$sum_day_deduct = 0;
			$sum_hour_deduct = 0;
			$sum_minute_deduct = 0;

			$sum_day_absence = 0;
			$sum_hour_absence = 0;
			$sum_minute_absence = 0;

			$sum_ot_site_hour_10 = 0;
			$sum_ot_site_minute_10 = 0;
			$sum_ot_site_hour_15 = 0;
			$sum_ot_site_minute_15 = 0;
			$sum_ot_site_hour_30 = 0;
			$sum_ot_site_minute_30 = 0;

			$sum_ot_hq_hour_10 = 0;
			$sum_ot_hq_minute_10 = 0;
			$sum_ot_hq_hour_15 = 0;
			$sum_ot_hq_minute_15 = 0;
			$sum_ot_hq_hour_30 = 0;
			$sum_ot_hq_minute_30 = 0;

			$temp_ot_all_hour_10 = 0;
			$temp_ot_all_minute_10 = 0;
			$temp_ot_all_hour_15 = 0;
			$temp_ot_all_minute_15 = 0;
			$temp_ot_all_hour_30 = 0;
			$temp_ot_all_minute_30 = 0;

			$sum_ot_all_10 = 0;
			$sum_ot_all_15 = 0;
			$sum_ot_all_30 = 0;

			$sum_expenses_day = 0;
			$sum_expenses_paid = 0;

			$data = $this->MonthlyHR1Siamraj($site, $date_start, $date_end);

			$month = explode("-",$date_end);
			$month = intval($month[1]);

			$bu_year = $year+543;
			$body   = "<style>
							.header{text-align:center; padding-bottom:3px; font-size:18px; font-weight:bold;}
							.sub-header{text-align:center; padding-bottom:3px; font-size:16px; font-weight:bold;}
							.header-bold{ text-decoration: underline;!important;}
							.input-form{ font-weight:normal; text-decoration:underline;}
							.text-left{ text-align:left;}
							.text-right{ text-align:right;}
							.text-center{ text-align:center;}
							 table{ text-align:center; width:100%; font-size:14px; border-collapse: collapse; margin-top:10px;}
							 table tr { margin:0px !important;}
							 table tr td{ border:solid 1 px !important; margin:0px !important;}
							 table tr td.none-border{ border:none !important;}
							 table tr td.border-form { border-bottom:dotted 1px #666 !important;}
							 table tr td.td-header {padding-bottom:-5px;}
							 table tr td.td-padding {padding:10px;}
						   </style>";
			$body .= "<table>";
			$body .= "<tr>";
			$body .= "<td colspan='12' class='none-border text-left'>บริษัท สยามราชธานี จำกัด</td><td colspan='15' class='none-border text-right'>หน้าที่ 1/1</td>";
			$body .= "</tr>";
			$body .= "<tr>";
			$body .= "<td colspan='12' class='none-border text-left'>รายงาน OT / ลากิจ / ขาดงาน รายเดือน - [". $brancheName ."]</td><td colspan='15' class='none-border text-right'>วันที่พิมพ์ ". date("d/m/Y H:i") ."</td>";
			$body .= "</tr>";
			$body .= "<tr>";
			$body .= "<td colspan='24' class='none-border text-left'>สำหรับเดือน  ". $this->th_month[$month-1] ."  ". $bu_year ." [ ". date("d/m/Y", strtotime($date_start)) ." - ". date("d/m/Y", strtotime($date_end)) ." ] </td>"; //$this->month[$month]
			$body .= "</tr>";
			$body .= "<tr>";
			$body .= "<td width='5%' rowspan='3'>#</td>";
			$body .= "<td width='15%' rowspan='3'>ชื่อ - นามสกุล</td>";
			$body .= "<td width='10%' rowspan='2' colspan='3'>หักขาดงาน</td>";
			$body .= "<td width='10%' rowspan='2' colspan='3'>หักลากิจ</td>";
			$body .= "<td width='10%' rowspan='2' colspan='2' >OT เหมาจ่าย</td>";
			$body .= "<td rowspan='2' colspan='3' bgcolor='#e6e6e6'>รวม OT (ชม.)</td>";
			$body .= "<td colspan='6'>สำนักงาน (OT ชม.)</td>";
			$body .= "<td width='10%' rowspan='3'>Site</td>";
			$body .= "<td colspan='6'>Site (OT ชม.)</td>";
			$body .= "<td width='8%' rowspan='3'>จำนวนเงิน</td>";
			$body .= "</tr>";

			$body .= "<tr>";
			$body .= "<td colspan='2'>1</td><td colspan='2'>1.5</td><td colspan='2'>3</td>";
			$body .= "<td colspan='2'>1</td><td colspan='2'>1.5</td><td colspan='2'>3</td>";
			$body .= "</tr>";

			$body .= "<tr>";
			$body .= "<td>วัน</td><td>ชม.</td><td>นาที</td>";
			$body .= "<td>วัน</td><td>ชม.</td><td>นาที</td>";
			$body .= "<td>วัน</td><td>บาท</td>";
			$body .= "<td bgcolor='#e6e6e6'>1</td><td bgcolor='#e6e6e6'>1.5</td><td bgcolor='#e6e6e6'>3</td>";
			$body .= "<td>ชม.</td><td>นาที</td>"; // สำนักงานใหญ่ 1
			$body .= "<td>ชม.</td><td>นาที</td>"; // สำนักงานใหญ่ 1.5
			$body .= "<td>ชม.</td><td>นาที</td>"; // สำนักงานใหญ่ 3
			$body .= "<td>ชม.</td><td>นาที</td>"; // site 1
			$body .= "<td>ชม.</td><td>นาที</td>"; // site 1.5
			$body .= "<td>ชม.</td><td>นาที</td>"; // site 3
			$body .= "</tr>";

// data body
			$no = 0;
			for ($i=0; $i <count($data) ; $i++) {
				$chkOTsite = count($data[$i]["overtime_site"]);


				if ($data[$i]["leave_deduct"][0]["day"] == null) {
					$leave_deduct_day = 0;
				}else{
					$leave_deduct_day = $data[$i]["leave_deduct"][0]["day"];
				}
				// $sum_day_deduct = $sum_day_deduct + ($data[$i]["absense_day"] + $leave_deduct_day);
				if (($data[$i]["absense_day"] + $leave_deduct_day) == 0) {
					// $leave_deduct_day = "";
				}else{
					$leave_absense_day = ($data[$i]["absense_day"]);
				}

				//deduct hour
				if ($data[$i]["leave_deduct"][0]["hour"] == null) {
					$leave_deduct_hour = 0;
				}else{
					$leave_deduct_hour = $data[$i]["leave_deduct"][0]["hour"];
				}
				// $sum_hour_deduct = $sum_hour_deduct + $leave_deduct_hour;

				//deduct minute
				if ($data[$i]["leave_deduct"][0]["minute"] == null) {
					$leave_deduct_minute = 0;
				}else{
					$leave_deduct_minute = $data[$i]["leave_deduct"][0]["minute"];
				}
				// $sum_minute_deduct = $sum_minute_deduct + $leave_deduct_minute;
				$sum_day_absence  += ($data[$i]["absense_day"]); //$leave_deduct_day

				$sum_day_deduct = $sum_day_deduct + $leave_deduct_day;
				$sum_hour_deduct = $sum_hour_deduct + $leave_deduct_hour;
				$sum_minute_deduct = $sum_minute_deduct + $leave_deduct_minute;

				// sum OT all

				if (isset($data[$i]["overtime_all"]["1.00"])) {
					$temp_ot_all_hour_10 = $temp_ot_all_hour_10 + (int)$data[$i]["overtime_all"]["1.00"]["hour"];
					$temp_ot_all_minute_10 = $temp_ot_all_minute_10 + (int)$data[$i]["overtime_all"]["1.00"]["minute"];
					// $ot_all_10 = $data[$i]["overtime_all"]["1.00"]["hour"].".".$data[$i]["overtime_all"]["1.00"]["minute"];
				}else{
					$temp_ot_all_hour_10 = $temp_ot_all_hour_10 + 0;
					$temp_ot_all_minute_10 = $temp_ot_all_minute_10 + 0;
				}

				if (isset($data[$i]["overtime_all"]["1.50"])) {
					$temp_ot_all_hour_15 = $temp_ot_all_hour_15 + (int)$data[$i]["overtime_all"]["1.50"]["hour"];
					$temp_ot_all_minute_15 = $temp_ot_all_minute_15 + (int)$data[$i]["overtime_all"]["1.50"]["minute"];
				}else{
					$temp_ot_all_hour_15 = $temp_ot_all_hour_15 + 0;
					$temp_ot_all_minute_15 = $temp_ot_all_minute_15 + 0;
				}

				if (isset($data[$i]["overtime_all"]["3.00"])) {
					$temp_ot_all_hour_30 = $temp_ot_all_hour_30 + (int)$data[$i]["overtime_all"]["3.00"]["hour"];
					$temp_ot_all_minute_30 = $temp_ot_all_minute_30 + (int)$data[$i]["overtime_all"]["3.00"]["minute"];
				}else{
					$temp_ot_all_hour_30 = $temp_ot_all_hour_30 + 0;
					$temp_ot_all_minute_30 = $temp_ot_all_minute_30 + 0;
				}



				////////////////////////////////////////////////////////////////////////

				// if (count($data[$i]["overtime_hq"]) == 0 && count($data[$i]["overtime_site"]) == 0 && $data[$i]["absense_day"] == 0 && $data[$i]["leave_deduct"][0]["day"] == null && $data[$i]["leave_deduct"][0]["hour"] == null && $data[$i]["leave_deduct"][0]["minute"] == null) {
				// 	continue;
				// }

				if (count($data[$i]["overtime_hq"]) == 0 && count($data[$i]["overtime_site"]) == 0 && $data[$i]["absense_day"] == 0) {
					continue;
				}

				if ($chkOTsite == 0) {
					$no++;
						$body .= $this->getDataBody($data,$i,$no,$chkOTsite,0,1);

						for ($j=0; $j < count($data[$i]["overtime_hq"]) ; $j++) {
							if ($data[$i]["overtime_hq"][$j]["multiple"] == "1.00") {
								$sum_ot_hq_hour_10 = $sum_ot_hq_hour_10 + (int)$data[$i]["overtime_hq"][$j]["hour"];
								$sum_ot_hq_minute_10 = $sum_ot_hq_minute_10 +  (int)$data[$i]["overtime_hq"][$j]["minute"];
							}elseif($data[$i]["overtime_hq"][$j]["multiple"] == "1.50"){
								$sum_ot_hq_hour_15 = $sum_ot_hq_hour_15 + (int)$data[$i]["overtime_hq"][$j]["hour"];
								$sum_ot_hq_minute_15 = $sum_ot_hq_minute_15 +  (int)$data[$i]["overtime_hq"][$j]["minute"];
							}elseif($data[$i]["overtime_hq"][$j]["multiple"] == "3.00"){
								$sum_ot_hq_hour_30 = $sum_ot_hq_hour_30 + (int)$data[$i]["overtime_hq"][$j]["hour"];
								$sum_ot_hq_minute_30 = $sum_ot_hq_minute_30 + (int)$data[$i]["overtime_hq"][$j]["minute"];
							}

							if (isset($data[$i]["overtime_hq"][$j]["expenses_paid"])) {
								$sum_expenses_paid += (int)$data[$i]["overtime_hq"][$j]["expenses_paid"];
							}else{
								$sum_expenses_paid += 0;
							}						

							if (isset($data[$i]["overtime_hq"][$j]["expenses_day"])) {
								$sum_expenses_day += (int)$data[$i]["overtime_hq"][$j]["expenses_day"];
							}
						}

				}else{
					$groupData = 1;
					$no++;
					for ($iOTsite=0; $iOTsite < $chkOTsite ; $iOTsite++) {
						$body .= $this->getDataBody($data,$i,$no,$chkOTsite,$iOTsite,$groupData);

						if ($data[$i]["overtime_site"][$iOTsite]["multiple"] == "1.00") {
							$sum_ot_site_hour_10 = $sum_ot_site_hour_10 + (int)$data[$i]["overtime_site"][$iOTsite]["hour"];
							$sum_ot_site_minute_10 = $sum_ot_site_minute_10 + (int)$data[$i]["overtime_site"][$iOTsite]["minute"];
						}elseif($data[$i]["overtime_site"][$iOTsite]["multiple"] == "1.50"){
							$sum_ot_site_hour_15 = $sum_ot_site_hour_15 + (int)$data[$i]["overtime_site"][$iOTsite]["hour"];
							$sum_ot_site_minute_15 = $sum_ot_site_minute_15 + (int)$data[$i]["overtime_site"][$iOTsite]["minute"];
						}elseif($data[$i]["overtime_site"][$iOTsite]["multiple"] == "3.00"){
							$sum_ot_site_hour_30 = $sum_ot_site_hour_30 + (int)$data[$i]["overtime_site"][$iOTsite]["hour"];
							$sum_ot_site_minute_30 = $sum_ot_site_minute_30 + (int)$data[$i]["overtime_site"][$iOTsite]["minute"];
						}

						$sum_expenses_paid = (int)$data[$i]["overtime_site"][$iOTsite]["expenses_paid"];

						if (isset($data[$i]["overtime_site"][$iOTsite]["expenses_paid"])) {
							$sum_expenses_day = $data[$i]["overtime_site"][$iOTsite]["expenses_day"];
						}

						$groupData ++;
					} // get record OT site
				}

			}

//summary

			$body .= "<tr>";
			$body .= "<td colspan='2' class='text-right'>รวม</td>";
			$body .= "<td>". $sum_day_absence ."</td>";
			$body .= "<td>0</td>"; //$sum_hour_deduct
			$body .= "<td>0</td>"; //$sum_minute_deduct
			$body .= "<td>$sum_day_deduct</td>";
			$body .= "<td>$sum_hour_deduct</td>";
			$body .= "<td>$sum_minute_deduct</td>";
			$body .= "<td>". $sum_expenses_day ."</td>";
			$body .= "<td>". number_format($sum_expenses_paid,2) ."</td>";
			$body .= "<td bgcolor='#e6e6e6'>". floor(($temp_ot_all_hour_10*60 + $temp_ot_all_minute_10)/60) .".". ($temp_ot_all_hour_10*60 + $temp_ot_all_minute_10) % 60 ."</td>";
			$body .= "<td bgcolor='#e6e6e6'>". floor(($temp_ot_all_hour_15*60 + $temp_ot_all_minute_15)/60) .".". ($temp_ot_all_hour_15*60 + $temp_ot_all_minute_15) % 60 ."</td>";
			$body .= "<td bgcolor='#e6e6e6'>". floor(($temp_ot_all_hour_30*60 + $temp_ot_all_minute_30)/60) .".". ($temp_ot_all_hour_30*60 + $temp_ot_all_minute_30) % 60 ."</td>";
			$body .= "<td>". $sum_ot_hq_hour_10 ."</td>";
			$body .= "<td>". $sum_ot_hq_minute_10 ."</td>";
			$body .= "<td>". $sum_ot_hq_hour_15 ."</td>";
			$body .= "<td>". $sum_ot_hq_minute_15 ."</td>";
			$body .= "<td>". $sum_ot_hq_hour_30 ."</td>";
			$body .= "<td>". $sum_ot_hq_minute_30 ."</td>";
			$body .= "<td></td>";
			$body .= "<td>". $sum_ot_site_hour_10 ."</td>";
			$body .= "<td>". $sum_ot_site_minute_10 ."</td>";
			$body .= "<td>". $sum_ot_site_hour_15 ."</td>";
			$body .= "<td>". $sum_ot_site_minute_15 ."</td>";
			$body .= "<td>". $sum_ot_site_hour_30 ."</td>";
			$body .= "<td>". $sum_ot_site_minute_30 ."</td>";
			$body .= "<td></td>";
			$body .= "</tr>";
		$body .= "</table>";

		$body .= "<table>";
		$body .= "<tr>";
		$body .= "<td class='none-border text-right'>ลงชื่อ :</td><td class='none-border border-form' width='15%'></td><td class='none-border text-left'> ผู้จัดทำ</td>";
		$body .= "<td class='none-border text-right'>ลงชื่อ :</td><td class='none-border border-form' width='15%'></td><td class='none-border text-left'> หัวหน้าแผนก/ผู้จัดการ</td>";
		$body .= "<td class='none-border text-right'>ลงชื่อ :</td><td class='none-border border-form' width='15%'></td><td class='none-border text-left'> ผู้อำนวยการ</td>";
		$body .= "</tr>";
		$body .= "<tr>";
		$body .= "<td class='none-border text-right'>วันที่ :</td><td colspan = '1' class='none-border border-form'></td><td class='none-border'> </td>";
		$body .= "<td class='none-border text-right'>วันที่ :</td><td class='none-border border-form'></td><td class='none-border'> </td>";
		$body .= "<td class='none-border text-right'>วันที่ :</td><td class='none-border border-form'></td><td class='none-border'> </td>";
		$body .= "</tr>";
		$body .= "</table>";

			 $body .= "";
			return $body;
	}

	public function getDataBody($data,$i,$no,$chkOTsite,$iOTsite,$groupData)
	{

		// if (count($data[$i]["overtime_hq"]) == 0 && count($data[$i]["overtime_site"]) == 0 && $data[$i]["absense_day"] == 0 && $data[$i]["leave_deduct"][0]["day"] == null && $data[$i]["leave_deduct"][0]["hour"] == null && $data[$i]["leave_deduct"][0]["minute"] == null) {
		// 	return;
		// }
		$body_leave_deduct_day = "";

		$body_absence_day = "";
		//deduct day
		if ($data[$i]["leave_deduct"][0]["day"] == null) {
			$body_leave_deduct_day = "";
		}else{
			$body_leave_deduct_day = $data[$i]["leave_deduct"][0]["day"];
		}
		// $sum_day_deduct = $sum_day_deduct + ($data[$i]["absense_day"] + $leave_deduct_day);
		if (($data[$i]["absense_day"] ) == 0) { //+ $body_leave_deduct_day
			$body_leave_deduct_day = "";
		}else{
			$body_absence_day = ($data[$i]["absense_day"]); //$body_leave_deduct_day
		}

		//deduct hour
		if ($data[$i]["leave_deduct"][0]["hour"] == null) {
			$body_leave_deduct_hour = 0;
		}else{
			$body_leave_deduct_hour = $data[$i]["leave_deduct"][0]["hour"];
		}
		// $sum_hour_deduct = $sum_hour_deduct + $leave_deduct_hour;
		if ($body_leave_deduct_hour == 0) {
			$body_leave_deduct_hour = "";
		}

		//deduct minute
		if ($data[$i]["leave_deduct"][0]["minute"] == null) {
			$body_leave_deduct_minute = 0;
		}else{
			$body_leave_deduct_minute = $data[$i]["leave_deduct"][0]["minute"];
		}
		// $sum_minute_deduct = $sum_minute_deduct + $leave_deduct_minute;
		if ($body_leave_deduct_minute == 0) {
			$body_leave_deduct_minute = "";
		}

		$multiple10_hour = "";
		$multiple10_minute = "";
		$multiple15_hour = "";
		$multiple15_minute = "";
		$multiple30_hour = "";
		$multiple30_minute = "";

		$multiple10_site_hour = "";
		$multiple10_site_minute = "";
		$multiple15_site_hour = "";
		$multiple15_site_minute = "";
		$multiple30_site_hour = "";
		$multiple30_site_minute = "";

		$expenses_day = "";
		$expenses_paid = "";
		
		for ($j=0; $j < count($data[$i]["overtime_hq"]) ; $j++) {
			if ($data[$i]["overtime_hq"][$j]["multiple"] == "1.00") {
				$multiple10_hour = (int)$data[$i]["overtime_hq"][$j]["hour"];
				$multiple10_minute = (int)$data[$i]["overtime_hq"][$j]["minute"];
			}elseif($data[$i]["overtime_hq"][$j]["multiple"] == "1.50"){
				$multiple15_hour = (int)$data[$i]["overtime_hq"][$j]["hour"];
				$multiple15_minute = (int)$data[$i]["overtime_hq"][$j]["minute"];
			}elseif($data[$i]["overtime_hq"][$j]["multiple"] == "3.00"){
				$multiple30_hour = (int)$data[$i]["overtime_hq"][$j]["hour"];
				$multiple30_minute = (int)$data[$i]["overtime_hq"][$j]["minute"];
			}

			if (isset($data[$i]["overtime_hq"][$j]["expenses_paid"])) {
				$expenses_paid = number_format((int)$data[$i]["overtime_hq"][$j]["expenses_paid"],2);
			}elseif ((int)$data[$i]["overtime_hq"][$j]["expenses_paid"] == 0) {
				$expenses_paid = "";
			}
			

			if (isset($data[$i]["overtime_hq"][$j]["expenses_paid"])) {
				$expenses_day = (int)$data[$i]["overtime_hq"][$j]["expenses_day"];
			}elseif ((int)$data[$i]["overtime_hq"][$j]["expenses_day"] == 0) {
				$expenses_day = "";
			}
			// $expenses_day = (int)$data[$i]["overtime_hq"][$j]["expenses_day"];
		}

		if (isset($data[$i]["overtime_all"]["1.00"])) {
			$ot_all_multiple10 = "1";
			$ot_all_10 = $data[$i]["overtime_all"]["1.00"]["hour"].".".$data[$i]["overtime_all"]["1.00"]["minute"];
		}else{
			$ot_all_10 = "";
		}

		if (isset($data[$i]["overtime_all"]["1.50"])) {
			$ot_all_multiple10 = "1.5";
			$ot_all_15 = (int)$data[$i]["overtime_all"]["1.50"]["hour"].".".$data[$i]["overtime_all"]["1.50"]["minute"];
		}else{
			$ot_all_15 = "";
		}

		if (isset($data[$i]["overtime_all"]["3.00"])) {
			$ot_all_multiple10 = "3";
			$ot_all_30 = $data[$i]["overtime_all"]["3.00"]["hour"].".".$data[$i]["overtime_all"]["3.00"]["minute"];
		}else{
			$ot_all_30 = "";
		}





// body ========================================
		$body = "";
		if ($groupData <> 1) {
			$body .= "<tr>";
		}else{
			$body .= "<tr>";
		}
		// $body .= "<tr>";
		if ($groupData <> 1) {
			$no = "";
			$fullname = "";
			$body_leave_deduct_day = "";
			$body_leave_deduct_hour = "";
			$body_leave_deduct_minute = "";
			$multiple10_hour = "";
			$multiple10_minute = "";
			$multiple15_hour = "";
			$multiple15_minute = "";
			$multiple30_hour = "";
			$multiple30_minute = "";
			$ot_all_10 = "";
			$ot_all_15 = "";
			$ot_all_30 = "";
		}else{
			$no = $no;
			$fullname = $data[$i]["fullname"];
		}
		$body .= "<td>". $no ."</td>";
		$body .= "<td class='text-left'>". $fullname ."</td>";
		$body .= "<td>". $body_absence_day ."</td>";
		$body .= "<td></td>"; // 
		$body .= "<td></td>"; // 
		$body .= "<td>". $body_leave_deduct_day ."</td>";
		$body .= "<td>". $body_leave_deduct_hour. "</td>";
		$body .= "<td>". $body_leave_deduct_minute ."</td>";

		if (isset($data[$i]["overtime_site"][$iOTsite]["expenses_paid"])) {
			$expenses_day = $data[$i]["overtime_site"][$iOTsite]["expenses_day"];
		}
		
		$body .= "<td>". $expenses_day ."</td>";
		// if ($expenses_paid == 0) {
		// 	$body .= "<td>". $expenses_day ."</td>";
		// }else{
		// 	$body .= "<td>". (int)$data[$i]["overtime_site"][$iOTsite]["expenses_day"] ."</td>";
		// }
		
		if ($chkOTsite == 0) {
			$body .= "<td>". $expenses_paid ."</td>";
		}else{
			$body .= "<td>". number_format((int)$data[$i]["overtime_site"][$iOTsite]["expenses_paid"],2) ."</td>";
		}	
		$body .= "<td bgcolor='#e6e6e6'>". $ot_all_10 ."</td>";
		$body .= "<td bgcolor='#e6e6e6'>". $ot_all_15 ."</td>";
		$body .= "<td bgcolor='#e6e6e6'>". $ot_all_30."</td>";

		// if ($multiple10_hour == 0) {
		// 	$multiple10_hour = "";
		// }
		// if ($multiple10_minute == 0) {
		// 	$multiple10_minute = "";
		// }
		// if ($multiple15_hour == 0) {
		// 	$multiple15_hour = "";
		// }
		// if ($multiple15_minute == 0) {
		// 	$multiple15_minute = "";
		// }
		// if ($multiple30_hour == 0) {
		// 	$multiple30_hour = "";
		// }
		// if ($multiple30_minute == 0) {
		// 	$multiple30_minute = "";
		// }
		$body .= "<td>". $multiple10_hour ."</td>";
		$body .= "<td>". $multiple10_minute ."</td>";
		$body .= "<td>". $multiple15_hour ."</td>";
		$body .= "<td>". $multiple15_minute ."</td>";
		$body .= "<td>". $multiple30_hour ."</td>";
		$body .= "<td>". $multiple30_minute ."</td>";

		if ($chkOTsite == 0) {
			$body .= "<td></td>";
		}else{
			$body .= "<td>". $data[$i]["overtime_site"][$iOTsite]["code_site"] ."</td>";

			if ($data[$i]["overtime_site"][$iOTsite]["multiple"] == "1.00") {
				$multiple10_site_hour = (int)$data[$i]["overtime_site"][$iOTsite]["hour"];
				$multiple10_site_minute = (int)$data[$i]["overtime_site"][$iOTsite]["minute"];
			}elseif($data[$i]["overtime_site"][$iOTsite]["multiple"] == "1.50"){
				$multiple15_site_hour = (int)$data[$i]["overtime_site"][$iOTsite]["hour"];
				$multiple15_site_minute = (int)$data[$i]["overtime_site"][$iOTsite]["minute"];
			}elseif($data[$i]["overtime_site"][$iOTsite]["multiple"] == "3.00"){
				$multiple30_site_hour = (int)$data[$i]["overtime_site"][$iOTsite]["hour"];
				$multiple30_site_minute = (int)$data[$i]["overtime_site"][$iOTsite]["minute"];
			}
		}


		$body .= "<td>". $multiple10_site_hour ."</td>";
		$body .= "<td>". $multiple10_site_minute ."</td>";
		$body .= "<td>". $multiple15_site_hour ."</td>";
		$body .= "<td>". $multiple15_site_minute ."</td>";
		$body .= "<td>". $multiple30_site_hour ."</td>";
		$body .= "<td>". $multiple30_site_minute ."</td>";
		$body .= "<td></td>";

		$body .= "</tr>";
		return $body;
	}

	public function MonthlyHR1Siamraj($branche_id,$start_date,$end_date)
	{
		//get
		// if($this->isAuthorised)
		// {
			// $report = $this->ReportModel;
			// $branche_id = $this->input->get('branche_id');
			// $start_date = $this->input->get('start_date');
			// $end_date = $this->input->get('end_date');

			//form
			// data : [ {employee_id : "xxxx",
			// "leave_deduct": {day : x,hour:y,minute:z}}]
			// echo $branche_id."||".$start_date."||".$end_date;
			$userModel = $this->User;
			// $userModel->company = $this->company_id;
			$user_list_response = $this->User->getUserBranche($branche_id);
			$user_list = array();
			if($user_list_response['Has'])
			{
				$user_list = $user_list_response['data'];
			}
			// print_r($user_list_response);

			if(count($user_list) > 0)
			{
				//found user in this branches
				$callback = array();
				foreach ($user_list as $user) {
					$employee_id = $user['employee_id'];
					$hd_id = $user['hd_id'];
					$startworkdate = $user['startworkdate'];
					$overtime_temp = $this->Helpers->OvertimeMonthlySummaryByPerson($start_date,$end_date,$employee_id);
					$absense_temp = $this->Helpers->AbsenseMonthlySummaryByPerson($start_date,$end_date,$employee_id,$hd_id,$startworkdate);
					$leave_temp = $this->Helpers->TimeoffDeductMonthlySummaryByPerson($start_date,$end_date,$employee_id);

					$output = array('employee_id' => $user['employee_id'] ,
					'fullname' => $user['fullname'],'overtime_hq'=>  array(),'overtime_site'=>  array());
					if($overtime_temp['Has'])
					{
						$output['overtime_hq'] = $overtime_temp['data']['overtime_hq'];
						$output['overtime_site'] = $overtime_temp['data']['overtime_site'];
						$output['overtime_all'] = $overtime_temp['data']['overtime_all'];
					}

					if($absense_temp['Has'])
					{
						$output['absense_day'] = $absense_temp['data'];
					}

					if($leave_temp['Has'])
					{
						$output['leave_deduct'] = $leave_temp['data'];
					}

					array_push($callback,$output);
				}
				return $callback;
				// $this->set_response($callback, REST_Controller::HTTP_OK);
			// }else{
			//   $this->set_response("Permission not allow",REST_Controller::HTTP_NOT_ACCEPTABLE);
			// }

		}
	}


	// public function MonthlyHR1Siamraj($branche_id,$start_date,$end_date)
	public function MonthlyHR1Siamraj_test()
	{
		//get
		// if($this->isAuthorised)
		// {
			// $report = $this->ReportModel;
			$branche_id = $this->input->get('branche_id');
			$start_date = $this->input->get('start_date');
			$end_date = $this->input->get('end_date');

			//form
			// data : [ {employee_id : "xxxx",
			// "leave_deduct": {day : x,hour:y,minute:z}}]
			// echo $branche_id."||".$start_date."||".$end_date;
			$userModel = $this->User;
			// $userModel->company = $this->company_id;
			$user_list_response = $this->User->getUserBranche($branche_id);
			$user_list = array();
			if($user_list_response['Has'])
			{
				$user_list = $user_list_response['data'];
			}
			// print_r($user_list_response);

			if(count($user_list) > 0)
			{
				//found user in this branches
				$callback = array();
				foreach ($user_list as $user) {
					$employee_id = $user['employee_id'];
					$hd_id = $user['hd_id'];
					$startworkdate = $user['startworkdate'];
					$overtime_temp = $this->Helpers->OvertimeMonthlySummaryByPerson($start_date,$end_date,$employee_id);
					$absense_temp = $this->Helpers->AbsenseMonthlySummaryByPerson($start_date,$end_date,$employee_id,$hd_id,$startworkdate);
					$leave_temp = $this->Helpers->TimeoffDeductMonthlySummaryByPerson($start_date,$end_date,$employee_id);

					$output = array('employee_id' => $user['employee_id'] ,
					'fullname' => $user['fullname'],'overtime_hq'=>  array(),'overtime_site'=>  array());
					if($overtime_temp['Has'])
					{
						$output['overtime_hq'] = $overtime_temp['data']['overtime_hq'];
						$output['overtime_site'] = $overtime_temp['data']['overtime_site'];
						$output['overtime_all'] = $overtime_temp['data']['overtime_all'];
					}

					if($absense_temp['Has'])
					{
						$output['absense_day'] = $absense_temp['data'];
					}

					if($leave_temp['Has'])
					{
						$output['leave_deduct'] = $leave_temp['data'];
					}

					array_push($callback,$output);
				}
				echo json_encode($callback);
				// return $callback;
				return;
				// $this->set_response($callback, REST_Controller::HTTP_OK);
			// }else{
			//   $this->set_response("Permission not allow",REST_Controller::HTTP_NOT_ACCEPTABLE);
			// }

		}
	}

}
