<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Departments extends MY_Controller {

	private $models;
	public function __construct(){
			 	 parent::__construct();
				 $this->models = $this->Department;
	}

	public function index()
	{
		 $department= $this->Departments;
		 $department->company = $this->auth[0]["company_id"];
		 $search = "";
		 if (isset($_POST["key"]) || $_POST["key"] != "") {
			  $search = $_POST["key"];
		 }
		 $dep = $department->getDepartments($search);
		 $department->dep = $dep;
		 $table = $department->genDepTables();
		 $callback = array(
			  "data" => $table["data"],
				"table" => $table["table"]
		 );
		 echo json_encode($callback);
		 return;
	}


 public function options(){
	 $str = "";
	 if (isset($_POST["selected"])) {
	 	 	 $str  = $_POST["selected"];
	 }
	 $this->models->company = 3;
	 $load = $this->models->getAll();
	 $options = $this->generateOption($load,$str);
	 echo json_encode($options);
	 return;
	 print_r($load);
 }

	public function create(){
			$this->models->company = 3;
			$this->models->auth = "20170000001";
		  $add = $this->models->add($_POST);
			$callback = array(
				"success" => false,
				"title" => "ผิดพลาด",
				"msg" => "ไม่สามารถบันทึกข้อมูลได้ กรุณาลองใหม่ในภายหลัง"
			);
			if($add){
				$callback = array(
					"success" => true,
					"title" => "สำเร็จ",
					"msg" => "บันทึกข้อมูลแผนกเรียบร้อย"
				);
			}
			echo json_encode($callback);
			return;
	}
	public function edit(){
		$id = $_POST["id"];
		$department = $this->Departments;
		$department->company = $this->auth[0]["company_id"];
		$dep = $department->getDepartments("",$id);
		$data["department"] = ($dep["Department"][0]);
		$this->load->view('department/edit',$data);

}
public function delete(){
	// $id = $_POST["id"];
	// $department = $this->Departments;
	// $department->company = $this->auth[0]["company_id"];
	// $dep = $department->getDepartments("",$id);
	// $data["department"] = ($dep["Department"][0]);
	// $this->load->view('department/delete',$data);

}

	public function load(){
			$this->models->company = 3;
		  $load = $this->models->getAll();
			$table = $this->generateTable($load);
			echo json_encode($table);
			return;

	}

	public function ModalCreate(){
			$this->load->view("department/create");
	}
	public function ModalEdit(){
			$this->load->view("department/edit");
	}
	public function ModalDetail(){
			$this->load->view("department/detail");
	}

	private function generateOption($data,$str = ""){
			$options = array();
			$options["option"] = array();
			$options["Has"] = $data["Has"];
			$op  = "<option value=''>";
			$op .= "เลือกแผนก";
			$op .= "</option>";
			array_push($options["option"],$op);
			if ($data["Has"]) {
					for ($i=0; $i < count($data["data"]); $i++) {
							 $select = "";
							 if ($str == $data["data"][$i]["dep_id"]) {
								 		$select = "selected";
							 }
							 $op  = "";
							 $op .= "<option value='".$data["data"][$i]["dep_id"]."' ".$select.">";
							 $op .= $data["data"][$i]["dep_name"];
							 $op .= "</option>";
							 array_push($options["option"],$op);
					}
			}
			return $options;
	}

	private function generateTable($data){
			$tables = array();
			$tables["table"] = array();
			$tables["data"] = array();
			$tables["Has"] = $data["Has"];
			if ($data["Has"]) {
					$num = 1;
					for ($i=0; $i < count($data["data"]); $i++) {
							 $tr  = "";
							 $tr .= "<tr>";
							 $tr .= "<td class='text-center'>";
							 $tr .= $num;
							 $tr .= "</td>";
							 $tr .= "<td>";
							 $tr .= $data["data"][$i]["dep_name"];
							 $tr .= "</td>";
							 $tr .= "<td>";
							 $tr .= $data["data"][$i]["dep_caption"];
							 $tr .= "</td>";
							 $tr .= "<td>";
							 $tr .= "</td>";
							 $tr .= "<td width='100'>";
							 $tr .= "<div class='btn-group'>";
							 $tr .= "<button class='btn btn-sm btn-default btn-outline waves-effect btn-edit-dep' data-dep_id='". $data["data"][$i]["dep_id"] ."'><span class='fa fa-edit'></span></button>";
							 $tr .= "<button class='btn btn-sm btn-default btn-outline waves-effect btn-delete-dep' data-dep_id='". $data["data"][$i]["dep_id"] ."' ><span class='fa fa-trash'></span></button>";
							 $tr .= "<button class='btn btn-sm btn-default btn-outline waves-effect btn-detail-dep'  data-dep_id='". $data["data"][$i]["dep_id"] ."'><span class='fa fa-eye'></span></button>";
							 $tr .= "</div>";
							 $tr .= "</td>";
							 $tr .= "</tr>";
							 array_push($tables["table"],$tr);
							 array_push($tables["data"],
							 		array(
										"num"=>$num,
										"id"=>$data["data"][$i]["dep_id"],
										"name"=>$data["data"][$i]["dep_name"],
										"caption" => $data["data"][$i]["dep_caption"]
								));
								$num ++;
					}

			}
			return $tables;
	}


}
