<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Workings extends MY_Controller {

	private $models;
	private $auth;

	public function __construct(){
			 parent::__construct();
			 $this->models = $this->Working;
			 $this->auth["auth"] = "20170000001";
			 $this->auth["company"] = 3;

	}

	public function index()
	{

	}

	public function getModalLink(){
		$this->load->view("working/link");
	}


	public function load(){
		$segment = base64_decode(base64_decode(base64_decode($_POST["segment"])));
		$this->models->site = $segment;
		$this->models->company =  $this->auth["company"];
		$this->models->auth =  $this->auth["auth"];
		$helpers = $this->Helpers;
		$wk_table = $this->models->getAll();
		$tables = $this->generateTable($wk_table);
		echo json_encode($tables);
		return;
	}

	public function modalCreate(){
			$this->load->view("working/create");
	}

	public function create(){
			$this->models->company = $this->input->post("company_id");
			$this->models->auth = $this->input->post("createBy");
			$add = $this->models->add($_POST);
			$callback = array(
				"success" => false,
				"title" => "ผิดพลาด",
				"msg" => "ไม่สามารถบันทึกข้อมูลได้ กรุณาลองใหม่ในภายหลัง"
			);
			if($add){
				$callback = array(
					"success" => true,
					"title" => "สำเร็จ",
					"msg" => "บันทึกข้อมูลวันทำการสำเร็จ"
				);
			}
			echo json_encode($callback);
			return;
	}

	public function openLink(){
			$site = "";
			if (isset($_POST["segment"])) {
				  $site = base64_decode(base64_decode(base64_decode($_POST["segment"])));
			}
			$branche = "";
			if (isset($_POST["branche"])) {
				  $branche = $_POST["branche"];
			}
			$obj = array("segment"=>$site,"branche"=>$branche);
			$id = $_POST["id"];
			$wk = $this->models->getBy($id,$obj);
			$wk_setting = $this->models->getSetting($id,$obj);
			$this->OT->company = 3;
			$getOT = $this->OT->getBy($wk_setting[0]["ot_id"]);
			// echo $wk_setting[0]["hd_id"];
			$getHD = $this->Holiday->getBy($wk_setting[0]["hd_id"]);

			$ot_all = $this->OT->getAll(array("segment" => base64_encode(base64_encode(base64_encode($site))),"branche" => $branche));
			$ot = array("Has" => false,"All" => $ot_all,);
			// holiday
			$this->Holiday->company = 3;
			$hd_all = $this->Holiday->getAll(array("segment" => base64_encode(base64_encode(base64_encode($site))),"branche" => $branche));
			$hd = array("Has" => false,"All" => $hd_all,);
			if ($getHD["Has"]) {
					$hd["Has"] = true;
					$hd["data"] = $getHD["data"][0];
			}
			if (count($getOT) > 0) {
					$ot["Has"] = true;
					$ot["data"] = $getOT[0];
			}
			$data["wk"] = $this->getTimeContent($wk);
			$data["ot"] = $ot;
			$data["hd"] = $hd;

			$openlink = $this->load->view("working/link",$data,true);
			$callback = array(
				"openLink" => $openlink,
				"working" => $wk[0],
				"ot" => $ot,
			);
			echo  json_encode($callback);
			return;
	}

	public function linkOT(){
		  $link = $this->models->updateLinkOT($_POST);
			$callback = array(
				"success" => false,
				"title" => "ผิดพลาด",
				"msg" => "ไม่สามารถบันทึกข้อมูลได้ กรุณาลองใหม่ในภายหลัง"
			);
			if($link){
			$site = "";
			 if (isset($_POST["segment"])) {
					 $site = base64_decode(base64_decode(base64_decode($_POST["segment"])));
			 }
			 $branche = "";
			 if (isset($_POST["branche"])) {
					 $branche = $_POST["branche"];
			 }
			 $id = $_POST["id"];
			 $obj = array("segment"=>$site,"branche"=>$branche);
			 $wk_setting = $this->models->getSetting($id,$obj);
			 $getOT = $this->OT->getBy($wk_setting[0]["ot_id"]);
			 $id = $_POST["id"];
				 $callback = array(
						"success" => true,
						"title" => "สำเร็จ",
						"msg" => "เชื่อมต่อกลุ่มล่วงเวลาสำเร็จ",
						"setting" => "เชื่อมต่อกับ กลุ่มล่วงเวลา : ".$getOT[0]["ot_name"]
					);
			}
			echo json_encode($callback);
			return;
	}

	public function linkHD(){
		  $link = $this->models->updateLinkHD($_POST);
			$callback = array(
				"success" => false,
				"title" => "ผิดพลาด",
				"msg" => "ไม่สามารถบันทึกข้อมูลได้ กรุณาลองใหม่ในภายหลัง"
			);
			if($link){
			$site = "";
			 if (isset($_POST["segment"])) {
					 $site = base64_decode(base64_decode(base64_decode($_POST["segment"])));
			 }
			 $branche = "";
			 if (isset($_POST["branche"])) {
					 $branche = $_POST["branche"];
			 }
			 $id = $_POST["id"];
			 $obj = array("segment"=>$site,"branche"=>$branche);
			 $wk_setting = $this->models->getSetting($id,$obj);
			 $getHD = $this->Holiday->getBy($wk_setting[0]["hd_id"]);
			 // print_r($getHD);
			 $id = $_POST["id"];
				 $callback = array(
						"success" => true,
						"title" => "สำเร็จ",
						"msg" => "เชื่อมต่อกลุ่มล่วงเวลาสำเร็จ",
						"setting" => "เชื่อมต่อกับ กลุ่มล่วงเวลา : ".$getHD["data"][0]["hd_name"]
					);
			}
			echo json_encode($callback);
			return;
	}


	public function getTimeContent($data){
			$wk = $data[0];
			$h = $this->Helpers;
			$time_content = array(
					"mon_start" => $h->genDisplayTime($wk["wk_mon_start"],true),
					"mon_end" => $h->genDisplayTime($wk["wk_mon_end"],true),
					"tue_start" => $h->genDisplayTime($wk["wk_tue_start"],true),
					"tue_end" => $h->genDisplayTime($wk["wk_tue_end"],true),
					"wed_start" => $h->genDisplayTime($wk["wk_wed_start"],true),
					"wed_end" => $h->genDisplayTime($wk["wk_wed_end"],true),
					"thu_start" => $h->genDisplayTime($wk["wk_thu_start"],true),
					"thu_end" => $h->genDisplayTime($wk["wk_thu_end"],true),
					"fri_start" => $h->genDisplayTime($wk["wk_fri_start"],true),
					"fri_end" => $h->genDisplayTime($wk["wk_fri_end"],true),
					"sat_start" => $h->genDisplayTime($wk["wk_sat_start"],true),
					"sat_end" => $h->genDisplayTime($wk["wk_sat_end"],true),
					"sun_start" => $h->genDisplayTime($wk["wk_sun_start"],true),
					"sun_end" => $h->genDisplayTime($wk["wk_sun_end"],true),
			);
			return $time_content;
	}

	private function generateTable($data){
			$callback = array();
			$callback["Has"] = false;
			if ($data["Has"]) {
					$callback["Has"] = true;
					$callback["table"] = array();
					$num = 1;
					for ($i=0; $i < count($data["data"]); $i++) {
							 $list = $data["data"][$i];
							 $owner = "disabled";
							 if ($list["wk_createdBy"] == $this->auth["auth"]) {
								 	 $owner = "";
							 }
							 $tr  = "<tr>";
							 $tr .= "<td class='text-center'>";
							 $tr .= $num;
							 $tr .= "</td>";
							 $tr .= "<td>";
							 $tr .= $list["wk_name"];
							 $tr .= "</td>";
							 $tr .= "<td class='text-center'>";
	             $tr .= "<div class='btn-group'>";
							 $tr .= "<button class='btn btn-sm btn-default btn-outline waves-effect btn-wk-link' data-id='".$list["wk_id"]."'><span class='fa fa-link'></span></button>";
	             $tr .= "<button class='btn btn-sm btn-default btn-outline waves-effect btn-wk-edit' $owner><span class='fa fa-edit'></span></button>";
	             $tr .= "<button class='btn btn-sm btn-default btn-outline waves-effect'><span class='fa fa-trash'></span></button>";
	             $tr .= "</div>";
	             $tr .= "</td>";
							 $tr .= "</tr>";
							 array_push($callback["table"],$tr);
							 $num++;
					}
			}
			return $callback;
	}


	public function wkList(){
		$segment = "";
		$insite = "";

		$auth = $this->input->post("auth");
		$company = $this->input->post("company_id");

		if (isset($_POST["segment"])) {
				 $segment = base64_decode(base64_decode(base64_decode($_POST["segment"])));
				 $this->models->notRep = $segment;
		}

		if (isset($_POST["insite"])) {
				 $insite = base64_decode(base64_decode(base64_decode($_POST["insite"])));
				 $this->models->insite = $insite;
		}

		$this->models->company = $company;
		$this->models->auth = $auth;
		$helpers = $this->Helpers;
		$wk_list = $this->models->getAll();
		$callback = array();
		$callback["success"] = FALSE;
		if ($wk_list["Has"]) {
				$wk_row = $wk_list["data"];
				$callback["list"] = array();
				$callback["content"] = array();
				$callback["data"] = array();
				for ($i=0; $i < count($wk_row); $i++) {
						 $active = "";
						 if ($i == 0) {
						 		 $active = "active";
						 }
						 $li  = "";
								 $li .= "<li class='tab $active'>";
								 $li .= "<a data-toggle='tab' href='#wk$i' aria-expanded='false'>";
								 $li .= "<span class='visible-xs'></span><i></i>".$wk_row[$i]["wk_name"];
								 $li .= "</a>";
								 $li .= "</li>";
								 $cont = "";
 								 $cont .= "<div id='wk$i' class='tab-pane $active'>";
								 		 $cont .= "<div class='row'>";
										 $cont .= "<div class='col-md-6 text-left'>";
										 $cont .= "<h4>".$wk_row[$i]["wk_name"]."</h4>";
										 $cont .= "</div>";
										 $cont .= "<div class='col-md-6 text-right'>";
										 $cont .= "<button class='btn btn-outline btn-success btn-select-wk' id='btn_select_wk$i' data-index='".$i."'><span class='fa fa-plus'></span> เพิ่มรายการนี้</button>";
										 $cont .= "</div>";
										 $cont .= "</div> <br />";

										 $cont .= "<div class='row text-left'>";
												$cont .= "<div class='col-md-6'>";
												$cont .= "<div class='form-group'>";
												$cont .= "<label for='monStart$i'>วันจันทร์</label>";
												$cont .= "<input class='form-control' id='monStart$i' value='".$helpers->genDisplayTime($wk_row[$i]["wk_mon_start"])."' readonly>";
												$cont .= "</div>";
												$cont .= "</div>";
												$cont .= "<div class='col-md-6'>";
												$cont .= "<div class='form-group'>";
												$cont .= "<label class='monEnd$i'>&nbsp;</label>";
												$cont .= "<input class='form-control' id='monEnd$i' value='".$helpers->genDisplayTime($wk_row[$i]["wk_mon_end"])."' readonly>";
												$cont .= "</div>";
												$cont .= "</div>";
												 //
												$cont .= "<div class='col-md-6'>";
											  $cont .= "<div class='form-group'>";
												$cont .= "<label for='tueStart$i'>วันอังคาร</label>";
												$cont .= "<input class='form-control' id='tueStart$i' value='".$helpers->genDisplayTime($wk_row[$i]["wk_tue_start"])."' readonly>";
												$cont .= "</div>";
												$cont .= "</div>";
												$cont .= "<div class='col-md-6'>";
												$cont .= "<div class='form-group'>";
												$cont .= "<label class='tueEnd$i'>&nbsp;</label>";
												$cont .= "<input class='form-control' id='tueEnd$i' value='".$helpers->genDisplayTime($wk_row[$i]["wk_tue_end"])."' readonly>";
												$cont .= "</div>";
												$cont .= "</div>";
												 //
												$cont .= "<div class='col-md-6'>";
												$cont .= "<div class='form-group'>";
												$cont .= "<label for='wedStart$i'>วันพุธ</label>";
												$cont .= "<input class='form-control' id='wedStart$i' value='".$helpers->genDisplayTime($wk_row[$i]["wk_wed_start"])."' readonly>";
												$cont .= "</div>";
												$cont .= "</div>";
												$cont .= "<div class='col-md-6'>";
												$cont .= "<div class='form-group'>";
												$cont .= "<label class='wedEnd$i'>&nbsp;</label>";
												$cont .= "<input class='form-control' id='wedEnd$i' value='".$helpers->genDisplayTime($wk_row[$i]["wk_wed_end"])."' readonly>";
												$cont .= "</div>";
												$cont .= "</div>";
												//
												$cont .= "<div class='col-md-6'>";
												$cont .= "<div class='form-group'>";
												$cont .= "<label for='thuStart$i'>วันพฤหัสบดี</label>";
												$cont .= "<input class='form-control' id='thuStart$i' value='".$helpers->genDisplayTime($wk_row[$i]["wk_thu_start"])."' readonly>";
												$cont .= "</div>";
												$cont .= "</div>";
												$cont .= "<div class='col-md-6'>";
												$cont .= "<div class='form-group'>";
												$cont .= "<label class='thuEnd$i'>&nbsp;</label>";
												$cont .= "<input class='form-control' id='thuEnd$i' value='".$helpers->genDisplayTime($wk_row[$i]["wk_thu_end"])."' readonly>";
												$cont .= "</div>";
												$cont .= "</div>";
												//
												$cont .= "<div class='col-md-6'>";
												$cont .= "<div class='form-group'>";
												$cont .= "<label for='friStart$i'>วันศุกร์</label>";
												$cont .= "<input class='form-control' id='friStart$i' value='".$helpers->genDisplayTime($wk_row[$i]["wk_fri_start"])."' readonly>";
												$cont .= "</div>";
												$cont .= "</div>";
												$cont .= "<div class='col-md-6'>";
												$cont .= "<div class='form-group'>";
												$cont .= "<label class='friEnd$i'>&nbsp;</label>";
												$cont .= "<input class='form-control' id='friEnd$i' value='".$helpers->genDisplayTime($wk_row[$i]["wk_fri_end"])."' readonly>";
												$cont .= "</div>";
												$cont .= "</div>";
												//
												$cont .= "<div class='col-md-6'>";
												$cont .= "<div class='form-group'>";
												$cont .= "<label for='satStart$i'>วันเสาร์</label>";
												$cont .= "<input class='form-control' id='satStart$i' value='".$helpers->genDisplayTime($wk_row[$i]["wk_sat_start"])."' readonly>";
												$cont .= "</div>";
												$cont .= "</div>";
												$cont .= "<div class='col-md-6'>";
												$cont .= "<div class='form-group'>";
												$cont .= "<label class='satEnd$i'>&nbsp;</label>";
												$cont .= "<input class='form-control' id='satEnd$i' value='".$helpers->genDisplayTime($wk_row[$i]["wk_sat_end"])."' readonly>";
												$cont .= "</div>";
												$cont .= "</div>";
												//
												$cont .= "<div class='col-md-6'>";
												$cont .= "<div class='form-group'>";
												$cont .= "<label for='sunStart$i'>วันอาทิตย์</label>";
												$cont .= "<input class='form-control' id='sunStart$i' value='".$helpers->genDisplayTime($wk_row[$i]["wk_sun_start"])."' readonly>";
												$cont .= "</div>";
												$cont .= "</div>";
												$cont .= "<div class='col-md-6'>";
												$cont .= "<div class='form-group'>";
												$cont .= "<label class='sunEnd$i'>&nbsp;</label>";
												$cont .= "<input class='form-control' id='sunEnd$i' value='".$helpers->genDisplayTime($wk_row[$i]["wk_sun_end"])."' readonly>";
												$cont .= "</div>";
												$cont .= "</div>";
										 $cont .= "</div>";
								 $cont .= "</div>";

						 array_push($callback["list"],$li);
						 array_push($callback["content"],$cont);
						 array_push($callback["data"],$wk_row[$i]);
				}
				$callback["success"] = TRUE;
		}
		echo json_encode($callback);
		return;
	}

}
// <li class="tab active">
// 		<a data-toggle="tab" href="#home3" aria-expanded="true"> <span class="visible-xs"><i class="ti-home"></i></span> <span class="hidden-xs">Default</span> </a>
// </li>
