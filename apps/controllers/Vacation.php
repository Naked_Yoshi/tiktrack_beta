<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once 'vendor/autoload.php';

class Vacation extends MY_Controller {

	private $models;
	public function __construct(){
			 	 parent::__construct();
				 // $this->models = $this->Site;
	}

  public function index()
  {
    $this->data["pageName"] = "จัดการวันลา";
    $this->data["pageactive"] = "<li><a href='".base_url("vacation")."' class='active'>จัดการวันลา</a></li>";
    $this->middle = 'vacation/index';
    $this->data["CSS_asset"]  = "<link href='".base_url("assets/plugins/bower_components/clockpicker/dist/jquery-clockpicker.min.css")."'   rel='stylesheet' type='text/css'  />";
    $this->data["JS_asset"]   = "<script src='".base_url("assets/js/cbpFWTabs.js")."'></script>";
    $this->data["JS_asset"]  .= "<script src='".base_url("assets/js/apps/JVacation.js")."'></script>";
    // $this->data["JS_asset"]  .= "<script src='".base_url("assets/js/apps/JSiteHelpers.js")."'></script>";
    $this->data["JS_asset"]  .= "<script src='".base_url("assets/plugins/bower_components/moment/moment.js")."'></script>";
    $this->data["JS_asset"]  .= "<script src='".base_url("assets/plugins/bower_components/clockpicker/dist/jquery-clockpicker.min.js")."'></script>";
    // $this->data["JS_asset"]  .= "<script src='https://maps.googleapis.com/maps/api/js?key=AIzaSyCZKhEPx14ken6ISjjnQT0K4tZ7nlNaJjU&libraries=places' async defer></script>";
    $this->layouts();
  }

  public function EditModal(){
    $this->load->view("vacation/edit");
  }

  public function ModalCreate(){
    $this->load->view("vacation/create");
  }

  public function getlinklev(){
    $this->load->view("working/link_lev");
  }

  public function typeModal(){
    $this->load->view("vacation/type");
  }

  public function approveModal(){
    $this->load->view("vacation/add_permission");
  }

	public function getApproveModal(){
		$this->load->view("leave/approveModel");
	}

	public function getReportLeaveRequest(){
		$this->load->view("leave/ReportRequest");
	}

	public function getConfig(){
		$this->load->view("vacation/config");
	}

	public function FilterLeaveRequest(){
		$this->load->view("leave/filter");
	}

	public function LeaveRequest(){
		$this->data["pageName"] = "ขออนุมัติการลา";
    $this->data["pageactive"] = "<li><a href='".base_url("vacation/LeaveRequest")."' class='active'>ขออนุมัติการลา</a></li>";
    $this->middle = 'leave/request';
    $this->data["CSS_asset"]  = "<link href='".base_url("assets/plugins/bower_components/clockpicker/dist/jquery-clockpicker.min.css")."'   rel='stylesheet' type='text/css'  />";
    $this->data["JS_asset"]   = "<script src='".base_url("assets/js/cbpFWTabs.js")."'></script>";
    $this->data["JS_asset"]  .= "<script src='".base_url("assets/js/apps/JLeave/JRequest.js")."'></script>";
    // $this->data["JS_asset"]  .= "<script src='".base_url("assets/js/apps/JHelpers.js")."'></script>";
    $this->data["JS_asset"]  .= "<script src='".base_url("assets/plugins/bower_components/moment/moment.js")."'></script>";
    $this->data["JS_asset"]  .= "<script src='".base_url("assets/plugins/bower_components/clockpicker/dist/jquery-clockpicker.min.js")."'></script>";
		$this->layouts();
	}

	public function LeaveApprove(){
		$this->data["pageName"] = "ขออนุมัติการลา";
    $this->data["pageactive"] = "<li><a href='".base_url("vacation/LeaveApprove")."' class='active'>อนุมัติแล้ว</a></li>";
    $this->middle = 'leave/approve';
    $this->data["CSS_asset"]  = "<link href='".base_url("assets/plugins/bower_components/clockpicker/dist/jquery-clockpicker.min.css")."'   rel='stylesheet' type='text/css'  />";
    $this->data["JS_asset"]   = "<script src='".base_url("assets/js/cbpFWTabs.js")."'></script>";
    $this->data["JS_asset"]  .= "<script src='".base_url("assets/js/apps/JLeave/JApprove.js")."'></script>";
    // $this->data["JS_asset"]  .= "<script src='".base_url("assets/js/apps/JHelpers.js")."'></script>";
    $this->data["JS_asset"]  .= "<script src='".base_url("assets/plugins/bower_components/moment/moment.js")."'></script>";
    $this->data["JS_asset"]  .= "<script src='".base_url("assets/plugins/bower_components/clockpicker/dist/jquery-clockpicker.min.js")."'></script>";
		$this->layouts();
	}

	public function LeaveReject(){
		$this->data["pageName"] = "ขออนุมัติการลา";
    $this->data["pageactive"] = "<li><a href='".base_url("vacation/LeaveReject")."' class='active'>ไม่อนุมัติ</a></li>";
    $this->middle = 'leave/reject';
    $this->data["CSS_asset"]  = "<link href='".base_url("assets/plugins/bower_components/clockpicker/dist/jquery-clockpicker.min.css")."'   rel='stylesheet' type='text/css'  />";
    $this->data["JS_asset"]   = "<script src='".base_url("assets/js/cbpFWTabs.js")."'></script>";
    $this->data["JS_asset"]  .= "<script src='".base_url("assets/js/apps/JLeave/JReject.js")."'></script>";
    // $this->data["JS_asset"]  .= "<script src='".base_url("assets/js/apps/JHelpers.js")."'></script>";
    $this->data["JS_asset"]  .= "<script src='".base_url("assets/plugins/bower_components/moment/moment.js")."'></script>";
    $this->data["JS_asset"]  .= "<script src='".base_url("assets/plugins/bower_components/clockpicker/dist/jquery-clockpicker.min.js")."'></script>";
		$this->layouts();
	}

	public function previewReport(){
		// $data = json_decode($_GET["dataset"]);
		// $dataRequest = base64_decode($data);
		$tempyear = $this->input->post("searchyear");
		$tempmonth = $this->input->post("searchmonth");
		$leave_request = $this->input->post("leave_request");
		$auth = $this->input->post("auth");
		$company = $this->input->post("company_id");

		$data = $this->ReportModels->getLeaveReport($leave_request,$company,$auth,$tempyear,$tempmonth);


		$defaultConfig = (new Mpdf\Config\ConfigVariables())->getDefaults();
 			$fontDirs = $defaultConfig['fontDir'];

 			$defaultFontConfig = (new Mpdf\Config\FontVariables())->getDefaults();
 			$fontData = $defaultFontConfig['fontdata'];
			 $mpdf = new \Mpdf\Mpdf([
 			    'fontDir' => array_merge($fontDirs, [
 			        __DIR__ . '/custom/font/th',
 			    ]),
 			    'fontdata' => $fontData + [
 			        'frutiger' => [
 			            'R' => 'THSarabunNew.ttf',
 			            'I' => 'THSarabunNew Italic.ttf',
 									'B' => 'THSarabunNew Bold.ttf',
 			        ]
 			    ],
 			    'default_font' => 'frutiger',
					'format' => 'A4-L'
 			]);
			// $fullname = $data["data"][0]["fullName"];
			$html = $this->htmlbodys($data,$tempmonth,$tempyear,$leave_request);

			$mpdf->WriteHTML($html);
				// $mpdf->WriteHTML("test : ");
			$mpdf->Output();
	}

	function htmlbodys($data,$tempmonth,$tempyear,$leave_request){
		if ($leave_request == "1") {
			$title = "รายงานการอนุมัติวันลา";
		}else if($leave_request == "3"){
			$title = "รายงานการขอวันลา";
		}

		$thaimonth = $this->thaimonth($tempmonth);
		$tempyear = $tempyear+543;
		$html="<style>
						table{ text-align:center; width:100%; font-size:18px; border-collapse: collapse; margin-top:10px;}
					</style>";
		$html.="<div style='text-align:center; padding-bottom:5px;'>บริษัท สยามราชธานี จำกัด</div>";
		$html.="<div style='text-align:center; padding-bottom:5px;'>". $title ." ประจำเดือน ". $thaimonth ." ปี ". $tempyear ."</div>";
		$html.="<table style='text-align:center; width:100%; border: 1px solid black; border-collapse: collapse;'>";
		$html.="<thead>";
		$html.="<tr>";
		$html.="<th scope='col' data-tablesaw-sortable-col data-tablesaw-priority='4' class='text-center'>no</th>";
		$html.="<th scope='col' data-tablesaw-sortable-col data-tablesaw-sortable-default-col data-tablesaw-priority='persist'>ชื่อ - นามสกุล</th>";
		$html.="<th scope='col' data-tablesaw-sortable-col data-tablesaw-priority='4' class='text-center'>วันที่ขอ</th>";
		$html.="<th scope='col' data-tablesaw-sortable-col data-tablesaw-priority='4' class='text-center'>รายละเอียด</th>";
		$html.="<th scope='col' data-tablesaw-sortable-col data-tablesaw-priority='2' class='text-center'>เริ่ม</th>";
		$html.="<th scope='col' data-tablesaw-sortable-col data-tablesaw-priority='2' class='text-center'>สิ้นสุด</th>";
		$html.="<th scope='col' data-tablesaw-sortable-col data-tablesaw-priority='4' class='text-center'>ไซต์</th>";
		$html.="<th scope='col' data-tablesaw-sortable-col data-tablesaw-priority='4' class='text-center'>สาขา</th>";
		$html.="<th scope='col' data-tablesaw-sortable-col data-tablesaw-priority='4' class='text-center'>รวม</th>";
		$html.="</tr>";
		$html.="</thead>";
		$html.="<tbody id='leavedata_body'>";
		$no = 1;
		for ($i=0; $i < count($data) ; $i++) {
			$html.="<tr>";
			$html.="<td>".$no."</td>";
			$html.="<td>". $data[$i]["fullname"] ."</td>";
			$html.="<td class='text-center'>";
			$html.= $data[$i]["createAt"];
			$html.="</td>";
			$html.="<td class='text-center'>";
			$html.= $data[$i]["detail"];
			$html.="</td>";
			$html.="<td class='text-center'>";
			$html.=$data[$i]["lev_data_date_start"];
			$html.="</td>";
			$html.="<td class='text-center'>";
			$html.=$data[$i]["lev_data_date_end"];
			$html.="</td>";
			//
			$html.="<td class='text-center'>";
			$html.=$data[$i]["siteName"];
			$html.="</td>";
			$html.="<td class='text-center'>";
			$html.=$data[$i]["brancheName"];
			$html.="</td>";
			$html.="<td class='text-center'> <div class='label label-table label-success'>";
			$html.= $data[$i]["lev_data_d_day"]." วัน ".$data[$i]["lev_data_d_hour"]." ชั่วโมง";
			$html.="</td>";
			//

			$html.="</tr>";
		}

		$html.="</tbody>";
		$html.="</table>";
		return $html;
	}

	public function thaimonth($tempmonth){
		$thaimonth = Array("มกราคม","กุมภาพันธ์","มีนาคม","เมษายน","พฤษภาคม","มิถุนายน","กรกฎาคม","สิงหาคม","กันยายน","ตุลาคม","พฤศจิกายน","ธันวาคม");
		$month = $thaimonth[$tempmonth - 1];
		return $month;
	}

}
