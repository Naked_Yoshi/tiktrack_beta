<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Working extends CI_Model{

  public  $company;
  public  $auth;
  public  $site;
  public  $notRep;
  public  $insite;
  private $now;


  function __construct()
  {
      parent::__construct();
      $this->now = date("Y-m-d H:i:s");
  }

// OR site_id = '".$this->site."'
  public function getAll(){
      $site_qry = "";
      if ($this->site != "") {
          $site_qry = " AND wk_id IN ( SELECT wk_id FROM ms_site_setting WHERE site_id = '".$this->site."' ) ";
      }
      $insite_qry = "";
      if ($this->insite != "") {
          $insite_qry = " AND wk_id IN ( SELECT wk_id FROM ms_site_setting WHERE site_id = '".$this->site."' ) OR site_id = '".$this->insite."' ";
      }
      $notRep_qry = "";
      if ($this->notRep != "") {
          $notRep_qry = " AND wk_id NOT IN ( SELECT wk_id FROM ms_site_setting WHERE site_id = '".$this->notRep."' )";
      }
      $SQL = "SELECT * FROM working WHERE company_id IN ('0','".$this->company."') AND wk_status = '1' ".$site_qry." ".$notRep_qry;
      $QRY = $this->db->query($SQL);
      $callback = array();
      $callback["Has"] = FALSE;
      if ($QRY->num_rows() > 0) {
          $callback["Has"] = TRUE;
          $callback["data"] = $ROW = $QRY->result_array();
      }
      return $callback;
 }

 public function getBy($id,$obj){
    $site = "";
    if ($obj["segment"] != "") {
        $site = " AND wk_id IN (SELECT wk_id FROM ms_site_setting WHERE site_id = '".$obj["segment"]."' ) ";
    }
    $SQL = "SELECT * FROM working WHERE wk_id = '".$id."' ".$site;
    $QRY = $this->db->query($SQL);
    $ROW = $QRY->result_array();
    return $ROW;
 }

 public function getSetting($id,$obj){
     if ($obj["segment"] != "") {
         $SQL = " SELECT * FROM ms_site_setting WHERE wk_id = '".$id."' AND site_id = '".$obj["segment"]."' ";
     }
     if ($obj["branche"] != "") {
         $SQL = " SELECT * FROM ms_branche_setting WHERE wk_id = '".$id."' AND branche_id = '".$obj["branche"]."' ";
     }
     $QRY = $this->db->query($SQL);
     $ROW = $QRY->result_array();
     return $ROW;
 }

 public function updateLinkOT($req){
      if ($req["segment"] != "") {
          $site_id =  base64_decode(base64_decode(base64_decode($req["segment"])));
          $this->db->where(array("site_id" => $site_id ,"wk_id" => $req["id"]));
          $update = $this->db->update("ms_site_setting",array("ot_id" => $req["ot"]));
      }
      if ($req["branche"] != "") {
          $branche_id = base64_decode(base64_decode(base64_decode($req["branche"])));
          $this->db->where(array("branche_id"=>$branche_id,"wk_id" => $req["id"]));
          $update = $this->db->update("ms_branche_setting",array("ot_id" => $req["ot"]));
      }
      if ($update) {
          return true;
      }else{
          return false;
      }
      //
 }
 public function updateLinkHD($req){
      if ($req["segment"] != "") {
          $site_id =  base64_decode(base64_decode(base64_decode($req["segment"])));
          $this->db->where(array("site_id" => $site_id ,"wk_id" => $req["id"]));
          $update = $this->db->update("ms_site_setting",array("hd_id" => $req["hd"]));
      }
      if ($req["branche"] != "") {
          $branche_id = base64_decode(base64_decode(base64_decode($req["branche"])));
          $this->db->where(array("branche_id"=>$branche_id,"wk_id" => $req["id"]));
          $update = $this->db->update("ms_branche_setting",array("ot_id" => $req["hd"]));
      }
      if ($update) {
          return true;
      }else{
          return false;
      }
      //
 }
  public function add($request){
     $helpers = $this->Helpers;
     $wk_name = $request["wk_name"];
     $monStart = $helpers->setDefaultTime($request["monday_start"]);
     $monEnd = $helpers->setDefaultTime($request["monday_end"]);

     $tueStart = $helpers->setDefaultTime($request["tuesday_start"]);
     $tueEnd = $helpers->setDefaultTime($request["tuesday_end"]);

     $wedStart = $helpers->setDefaultTime($request["wednesday_start"]);
     $wedEnd = $helpers->setDefaultTime($request["wednesday_end"]);

     $thuStart = $helpers->setDefaultTime($request["thursday_start"]);
     $thuEnd = $helpers->setDefaultTime($request["thursday_end"]);

     $friStart = $helpers->setDefaultTime($request["friday_start"]);
     $friEnd = $helpers->setDefaultTime($request["friday_end"]);

     $satStart = $helpers->setDefaultTime($request["saturday_start"]);
     $satEnd = $helpers->setDefaultTime($request["saturday_end"]);

     $sunStart = $helpers->setDefaultTime($request["sunday_start"]);
     $sunEnd = $helpers->setDefaultTime($request["sunday_end"]);
     $site = 0;
     if (isset($request["segment"])) {
        $site = base64_decode(base64_decode(base64_decode($request["segment"])));
     }
     $data = array(
           "wk_id" => "",
           "wk_name" => $wk_name,
           "wk_mon_start" => $monStart,
           "wk_mon_end" => $monEnd,
           "wk_tue_start" => $tueStart,
           "wk_tue_end" => $tueEnd,
           "wk_wed_start" => $wedStart,
           "wk_wed_end" => $wedEnd,
           "wk_thu_start" => $thuStart,
           "wk_thu_end" => $thuEnd,
           "wk_fri_start" => $friStart,
           "wk_fri_end" => $friEnd,
           "wk_sat_start" => $satStart,
           "wk_sat_end" => $satEnd,
           "wk_sun_start" => $sunStart,
           "wk_sun_end" => $sunEnd,
           "wk_createdBy" => $this->auth,
           "wk_createdAt" => $this->now,
           "wk_updatedBy" => $this->auth,
           "wk_updatedAt" => $this->now,
           "wk_approvedBy" => "",
           "wk_approvedAt" => "",
           "wk_status" => 1,
           "wk_public" => 0,
           "site_id" => $site,
           "branche_id" => 0,
           "company_id" => $this->company,
           "ot_id" => 0

     );
     $add = $this->db->insert('working',$data);
     if ($add) {
         return true;
     }else{
         return false;
     }

  }

}
 // company_id IN ('0','3') AND wk_status = '1'
