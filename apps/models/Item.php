<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Item extends CI_Model{


  function __construct()
  {
      parent::__construct();
      $this->now = date("Y-m-d H:i:s");
  }

  public function getTitle(){
     $SQL = "SELECT * FROM ms_title_name";
     $QRY = $this->db->query($SQL);
     $ROW = $QRY->result_array();
     return $ROW;
  }

}
 // company_id IN ('0','3') AND wk_status = '1'
