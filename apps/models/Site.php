<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Site extends CI_Model{

  public  $company;
  public  $auth;
  private $now;
  public  $id;
  public  $name;

  public  $lat;
  public  $lng;
  public  $area;

  function __construct()
  {
      parent::__construct();
      $this->now = date("Y-m-d H:i:s");
  }

public function getAll(){
      $SQL = "SELECT * FROM sites WHERE company_id = '".$this->company."' AND createdBy = '".$this->auth."' ";
      $QRY = $this->db->query($SQL);
      $ROW["Has"] = false;
      if ($QRY->num_rows() > 0) {
          $ROW["Has"] = true;
          $ROW["data"] = $QRY->result_array();
      }
      return $ROW;
}

public function getBy(){
      $SQL = "SELECT * FROM sites WHERE site_id = '".$this->id."' ";
      $QRY = $this->db->query($SQL);
      $ROW["Has"] = false;
      if ($QRY->num_rows() > 0) {
          $ROW["Has"] = true;
          $ROW["data"] = $QRY->result_array();
      }
      return $ROW;
}

public function updateName(){
     $data = array(
        "siteName" => $this->name,
        "updatedBy" => $this->auth,
        "updatedAt" => $this->now
     );
     $this->db->where(array(
       "site_id" => $this->id
     ));
     $update = $this->db->update("sites",$data);
     if($update){
        return true;
     }else{
        return false;
     }
}

public function updateLocation(){
    $data = array(
       "lat" => $this->lat,
       "lng" => $this->lng,
       "area" => $this->area,
       "updatedBy" => $this->auth,
       "updatedAt" => $this->now
    );
    $this->db->where(array(
      "site_id" => $this->id
    ));
    $update = $this->db->update("sites",$data);
    if($update){
       return true;
    }else{
       return false;
    }
}


public function add($request){
      $name = $request["name"];
      $lat = $request["lat"];
      $lng = $request["lng"];
      $area = $request["area"];
      $wk = $request["wk"];
      $last = $this->getLastId();
      $new_id = 1;
      if (count($last) > 0) {
          $new_id = intval($last[0]["site_id"]);
          $new_id ++;
      }

      $data = array(
          "site_id" => $new_id,
          "siteRef" => "",
          "siteName" => $name,
          "date_close_job" => 0,
          "lat" => $lat,
          "lng" => $lng,
          "area" => $area,
          "createdBy" => $this->auth,
          "createdAt" => $this->now,
          "updatedBy" => $this->auth,
          "updatedAt" => $this->now,
          "company_id" => $this->company
      );
      for ($i=0; $i < count($wk); $i++) {
           $num = $i+1;
           $data_setting = array(
              "site_id" => $new_id,
              "no" => $num,
              "site_setting_status" => 1,
              "wk_id" => $wk[$i],
              "ot_id" => 0
           );
           $add_setting = $this->db->insert('ms_site_setting',$data_setting);
      }
      $add = $this->db->insert('sites',$data);
      if ($add) {
          return true;
      }else{
          return false;
      }
 }

 public function getLastId(){
      $SQL = "SELECT site_id FROM sites ORDER BY site_id DESC LIMIT 1";
      $QRY = $this->db->query($SQL);
      $ROW = $QRY->result_array();
      return $ROW;
 }

 public function getLastNo(){
     $SQL = "SELECT no FROM ms_site_setting WHERE site_id = '".$this->id."' ORDER BY no  DESC LIMIT 1";
     $QRY = $this->db->query($SQL);
     $ROW = $QRY->result_array();
     return $ROW;
 }

}
