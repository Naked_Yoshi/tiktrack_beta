<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Department extends CI_Model{

  public $company;
  public $auth;
  private $now;

  function __construct()
  {
      parent::__construct();
      $this->now = date("Y-m-d H:i:s");
  }


  public function getAll(){
    $SQL = "SELECT * FROM department WHERE company_id = '3'";
    $QRY = $this->db->query($SQL);
    $ROW["Has"] = false;
    if ($QRY->num_rows() > 0) {
        $ROW["data"] = $QRY->result_array();
        $ROW["Has"] = true;
    }
    return $ROW;
  }

  public function getDepartments($search = "" , $id = ""){
     $callback = array();
     $company = $this->company;
     $str = " AND dep_name LIKE '%$search%' ";
     $str_id = "";
     if ($id != "") {
         $str_id = " AND dep_id = '$id' ";
     }
     $SQL = "SELECT * FROM department  WHERE company_id = '3' AND dep_status = '1' ".$str.$str_id;
      // ECHO $SQL;
     $QRY = $this->db->query($SQL);
     $NUM = $QRY->num_rows();
     $callback["hasDepartment"] = FALSE;
     if ($NUM > 0) {
       $callback["hasDepartment"] = TRUE;
       $callback["Department"] = $QRY->result_array();
    }
    return $callback;
}

  public function add($request){
     $depName = $request["dep_name"];
     $depDesc = $request["dep_caption"];
     $data = array(
        "dep_id" => "",
        "dep_name" => $dep_name,
        "dep_caption" => $depDesc,
        "dep_status" => 1,
        "createdBy" => $this->auth,
        "createdAt" => $this->now,
        "updatedBy" => $this->auth,
        "updatedAt" => "",
        "company_id" => $this->company
     );
     $add = $this->db->insert('department',$data);
     if ($add) {
         return true;
     }else{
         return false;
     }
  }

}
