<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Helpers extends CI_Model{

  public $company;
  public $auth;
  private $now;

  function __construct()
  {
      parent::__construct();
      $this->now = date("Y-m-d H:i:s");
  }



  public function genDisplayTime($timeIn,$msg = false){
      $timeOut = "";
      if ($timeIn == "00:00:00") {
          $timeOut = "-";
          if ($msg) {
              $timeOut = "<i style='color:#ccc;'>วันหยุด</i>";
          }
      }else{
          $time = explode(":",$timeIn);
          if ($msg) {
              $timeOut = $time[0].":".$time[1]." น.";
          }else{
              $timeOut = $time[0].":".$time[1];
          }

      }
      return $timeOut;
  }

  public function setDefaultTime($timeIn = ""){
			if ($timeIn == "") {
					return "00:00:00";
			}else{
					return $timeIn;
			}
	}


  public function setDesplayDate($dateIn){
			 $date = explode("-",$dateIn);
       return $date[2]."/".$date[1]."/".$date[0];
	}

  public function getnation(){
    $sql = "SELECT * FROM ms_nation";
    $query = $this->db->query($sql);
    $row = $query->result_array();
    return $row;
  }

  public function get_company($company){
    $SQL = "SELECT * FROM tiktrack_beta.company WHERE company_id = '$company';";
    $ROW = $this->db->query($SQL)->result_array();
    return $ROW;
  }

  public function get_title($id){
    $SQL="SELECT * FROM tiktrack_beta.ms_title_name WHERE title_id = '$id';";
    $QRY = $this->db->query($SQL)->result_array();
    return $QRY;
  }

  public function chk_datetotimeformat($time){
    return $time = substr($time,11,5);
}

  public function getAtt($company,$month,$id){
    $SQL = $this->SQL_Att_Head($company,$month,$id);

    $QRY = $this->db->query($SQL)->result_array();
    return $QRY;
  }

  public function getHolidayEmp($hd_id,$month){
    $SQL = "SELECT *,DAY(hdd_date) as hd_day FROM tiktrack_beta.holiday_detail WHERE hd_id = '$hd_id' AND MONTH(hdd_date) = '$month';";

    $QRY = $this->db->query($SQL)->result_array();
    return $QRY;
  }

  public function getLeaveEmp($emp_id,$tempmonth,$tempyear){
    $SQL = "SELECT
    a.lev_data_date_start,
    max(a.lev_data_date_end) as lev_data_date_end,
    sum(a.lev_data_d_day) as lev_data_d_day,
    sum(a.lev_data_d_hour) as lev_data_d_hour,
    sum(a.lev_data_d_minute) as lev_data_d_minute,
    a.detail,
    a.lev_approve_status,
    a.employee_id,
    b.leave_type_name
FROM
  tiktrack_beta.leave_data a
      LEFT JOIN
  ms_leave_type b ON a.leave_type = b.id
WHERE
  employee_id = '$emp_id'
      AND (MONTH(lev_data_date_start) = '$tempmonth'
        OR MONTH(lev_data_date_end) = '$tempmonth')
      AND YEAR(lev_data_date_start) = '$tempyear'
      AND lev_approve_status = '1'
      group by date_format(lev_data_date_start,'%d')
      order by lev_data_date_start";

    $QRY = $this->db->query($SQL)->result_array();
    return $QRY;
  }

  public function getOtEmp($emp_id,$tempmonth,$tempyear){
    $SQL = "SELECT
    *,
    IF(TIMEDIFF(otd_date_end, otd_date_start) > 0,
        TIME_FORMAT(TIMEDIFF(DATE_ADD(otd_date_end,
                            INTERVAL TIME_TO_SEC(otd_time_end) SECOND),
                        DATE_ADD(otd_date_start,
                            INTERVAL TIME_TO_SEC(otd_time_start) SECOND)),
                '%H:%i'),
        TIME_FORMAT(TIMEDIFF(otd_time_end, otd_time_start),
                '%H:%i')) AS reqTotalHours
FROM
    tiktrack_beta.ot_data
WHERE
    employee_id = '$emp_id'
        AND MONTH(otd_date_start) = '$tempmonth'
        AND YEAR(otd_date_start) = '$tempyear'
        AND otd_status = '1'
        order by otd_date_start;";

    $QRY = $this->db->query($SQL)->result_array();
    return $QRY;
  }

  public function getwkEmp($emp_id){
    $SQL = "SELECT U.*,WK.wk_mon_start,WK.wk_mon_end,CONVERT(DATE_SUB(TIMEDIFF(WK.wk_mon_end,WK.wk_mon_start),interval 1 HOUR),UNSIGNED INTEGER)/10000  as workhour FROM tiktrack_beta.users U LEFT JOIN working WK ON U.wk_id = WK.wk_id where employee_id = '$emp_id' ;";

    $QRY = $this->db->query($SQL)->result_array();
    return $QRY[0]["workhour"];
  }

  public function SQL_Att_Head($company,$month,$id){
    $SQL = "SELECT
          T.tat_id,
          T.tat_day,
          weekday(T.checkin_time_client) as name_weekday,
          U.employee_id,
          DATE_FORMAT(T.checkin_time_server,'%Y-%m-%d') as checkin_date,
          if(DATE_FORMAT(T.checkout_time_server,'%Y-%m-%d') IS NULL,'-',DATE_FORMAT(T.checkout_time_server,'%Y-%m-%d')) as checkout_date,
          CONCAT(U.firstname,' ',U.lastname) AS fullname,
          W.wk_name,
          IF(T.checkin_type_id = '1',
            DATE_FORMAT(T.checkin_time_server,'%H:%i:%s'),
            DATE_FORMAT(T.checkin_time_client,'%H:%i:%s') )AS checkin_time,
          IF(T.checkout_type_id = '3',
            DATE_FORMAT(T.checkout_time_server,'%H:%i:%s'),
            DATE_FORMAT(T.checkout_time_client,'%H:%i:%s') )AS checkout_time,
            IF(weekday(T.checkin_time_client) = '0',  CONCAT(W.wk_mon_start, '-' , W.wk_mon_end),
                            IF(weekday(T.checkin_time_client) = '1' ,CONCAT(W.wk_tue_start, '-' , W.wk_tue_end),
                              IF(weekday(T.checkin_time_client) = '2' ,CONCAT(W.wk_wed_start,'-',W.wk_wed_end),
                                IF(weekday(T.checkin_time_client) = '3', CONCAT(W.wk_thu_start,'-',W.wk_thu_end),
                                  IF(weekday(T.checkin_time_client) = '4', CONCAT(W.wk_fri_start,'-',W.wk_fri_end),
                                    IF(weekday(T.checkin_time_client) = '5', CONCAT(W.wk_sat_start,'-',W.wk_sat_end),
                                      IF(weekday(T.checkin_time_client) = '6', CONCAT(W.wk_sun_start,'-',W.wk_sun_end),'ไม่พบข้อมูล' ))))))) AS wokingStr,
                                      IF(weekday(T.checkin_time_client) = '0',  CONCAT(W.wk_mon_break_start, '-' , W.wk_mon_break_end),
                                          IF(weekday(T.checkin_time_client) = '1' ,CONCAT(W.wk_tue_break_start, '-' , W.wk_tue_break_end),
                                            IF(weekday(T.checkin_time_client) = '2' ,CONCAT(W.wk_wed_break_start,'-',W.wk_wed_break_end),
                                              IF(weekday(T.checkin_time_client) = '3', CONCAT(W.wk_thu_break_start,'-',W.wk_thu_break_end),
                                                IF(weekday(T.checkin_time_client) = '4', CONCAT(W.wk_fri_break_start,'-',W.wk_fri_break_end),
                                                  IF(weekday(T.checkin_time_client) = '5', CONCAT(W.wk_sat_break_start,'-',W.wk_sat_break_end),
                                                    IF(weekday(T.checkin_time_client) = '6', CONCAT(W.wk_sun_break_start,'-',W.wk_sun_break_end),'ไม่พบข้อมูล' ))))))) AS breakStr,
                                      IF(T.checkin_type_id = '1',
                                        IF(weekday(T.checkin_time_client) = '0',  TIMEDIFF(DATE_FORMAT(T.checkin_time_server,'%H:%i:%s'),W.wk_mon_start),
                                                      IF(weekday(T.checkin_time_client) = '1' ,TIMEDIFF(DATE_FORMAT(T.checkin_time_server,'%H:%i:%s'),W.wk_tue_start),
                                                        IF(weekday(T.checkin_time_client) = '2' ,TIMEDIFF(DATE_FORMAT(T.checkin_time_server,'%H:%i:%s'),W.wk_wed_start),
                                                          IF(weekday(T.checkin_time_client) = '3', TIMEDIFF(DATE_FORMAT(T.checkin_time_server,'%H:%i:%s'),W.wk_thu_start),
                                                            IF(weekday(T.checkin_time_client) = '4', TIMEDIFF(DATE_FORMAT(T.checkin_time_server,'%H:%i:%s'),W.wk_fri_start),
                                                              IF(weekday(T.checkin_time_client) = '5', TIMEDIFF(DATE_FORMAT(T.checkin_time_server,'%H:%i:%s'),W.wk_sat_start),
                                                                IF(weekday(T.checkin_time_client) = '6', TIMEDIFF(DATE_FORMAT(T.checkin_time_server,'%H:%i:%s'),W.wk_sun_start),'ไม่พบข้อมูล' ))))))) ,
                                        IF(weekday(T.checkin_time_client) = '0',  TIMEDIFF(DATE_FORMAT(T.checkin_time_client,'%H:%i:%s'),W.wk_mon_start),
                                                      IF(weekday(T.checkin_time_client) = '1' ,TIMEDIFF(DATE_FORMAT(T.checkin_time_client,'%H:%i:%s'),W.wk_tue_start),
                                                        IF(weekday(T.checkin_time_client) = '2' ,TIMEDIFF(DATE_FORMAT(T.checkin_time_client,'%H:%i:%s'),W.wk_wed_start),
                                                          IF(weekday(T.checkin_time_client) = '3', TIMEDIFF(DATE_FORMAT(T.checkin_time_client,'%H:%i:%s'),W.wk_thu_start),
                                                            IF(weekday(T.checkin_time_client) = '4', TIMEDIFF(DATE_FORMAT(T.checkin_time_client,'%H:%i:%s'),W.wk_fri_start),
                                                              IF(weekday(T.checkin_time_client) = '5', TIMEDIFF(DATE_FORMAT(T.checkin_time_client,'%H:%i:%s'),W.wk_sat_start),
                                                                IF(weekday(T.checkin_time_client) = '6', TIMEDIFF(DATE_FORMAT(T.checkin_time_client,'%H:%i:%s'),W.wk_sun_start),'ไม่พบข้อมูล' ))))))))AS lateTime,
                                                                IF(T.checkout_type_id = '2',
        IF(WEEKDAY(T.checkout_time_client) = '0',
            TIMEDIFF(W.wk_mon_end,
                    DATE_FORMAT(T.checkout_time_server, '%H:%i:%s')),
            IF(WEEKDAY(T.checkout_time_client) = '1',
                TIMEDIFF(W.wk_tue_end,
                        DATE_FORMAT(T.checkout_time_server, '%H:%i:%s')),
                IF(WEEKDAY(T.checkout_time_client) = '2',
                    TIMEDIFF(W.wk_wed_end,
                            DATE_FORMAT(T.checkout_time_server, '%H:%i:%s')),
                    IF(WEEKDAY(T.checkout_time_client) = '3',
                        TIMEDIFF(W.wk_thu_end,
                                DATE_FORMAT(T.checkout_time_server, '%H:%i:%s')),
                        IF(WEEKDAY(T.checkout_time_client) = '4',
                            TIMEDIFF(W.wk_fri_end,
                                    DATE_FORMAT(T.checkout_time_server, '%H:%i:%s')),
                            IF(WEEKDAY(T.checkout_time_client) = '5',
                                TIMEDIFF(W.wk_sat_end,
                                        DATE_FORMAT(T.checkout_time_server, '%H:%i:%s')),
                                IF(WEEKDAY(T.checkout_time_client) = '6',
                                    TIMEDIFF(W.wk_sun_end,
                                            DATE_FORMAT(T.checkout_time_server, '%H:%i:%s')),
                                    'ไม่พบข้อมูล'))))))),
        IF(WEEKDAY(T.checkout_time_client) = '0',
            TIMEDIFF(W.wk_mon_end,
                    DATE_FORMAT(T.checkout_time_client, '%H:%i:%s')),
            IF(WEEKDAY(T.checkout_time_client) = '1',
                TIMEDIFF(W.wk_tue_end,
                        DATE_FORMAT(T.checkout_time_client, '%H:%i:%s')),
                IF(WEEKDAY(T.checkout_time_client) = '2',
                    TIMEDIFF(W.wk_wed_end,
                            DATE_FORMAT(T.checkout_time_client, '%H:%i:%s')),
                    IF(WEEKDAY(T.checkout_time_client) = '3',
                        TIMEDIFF(W.wk_thu_end,
                                DATE_FORMAT(T.checkout_time_client, '%H:%i:%s')),
                        IF(WEEKDAY(T.checkout_time_client) = '4',
                            TIMEDIFF(W.wk_fri_end,
                                    DATE_FORMAT(T.checkout_time_client, '%H:%i:%s')),
                            IF(WEEKDAY(T.checkout_time_client) = '5',
                                TIMEDIFF(W.wk_sat_end,
                                        DATE_FORMAT(T.checkout_time_client, '%H:%i:%s')),
                                IF(WEEKDAY(T.checkout_time_client) = '6',
                                    TIMEDIFF(W.wk_sun_end,
                                            DATE_FORMAT(T.checkout_time_client, '%H:%i:%s')),
                                    'ไม่พบข้อมูล')))))))) AS outBeforeEnd,
                            										if(T.checkin_type_id = '3','delay','-') AS isCheckInDelay,
          IF(T.checkin_type_id = '1' OR T.checkout_type_id = '3',
            TIMEDIFF(DATE_FORMAT(T.checkout_time_server,'%H:%i:%s'),
                      DATE_FORMAT(T.checkin_time_server,'%H:%i:%s')),
              TIMEDIFF(DATE_FORMAT(T.checkout_time_client,'%H:%i:%s'),
                      DATE_FORMAT(T.checkin_time_client,'%H:%i:%s'))) AS total_wk_time,
          T.approveStatus,
          T.checkin_type_id,
          T.checkout_type_id,
          IF(to_seconds(TIMEDIFF(T.checkin_time_server,T.checkin_time_client )) > 60
            OR to_seconds(TIMEDIFF(T.checkin_time_client ,T.checkin_time_server )) > 600  ,'1', '0') AS alert_in,
          IF(to_seconds(TIMEDIFF(T.checkout_time_client,T.checkout_time_server )) > 60
            OR to_seconds(TIMEDIFF(T.checkout_time_client ,T.checkout_time_server )) > 600,'1', '0') AS alert_out,
          T.checkin_route_id,
          T.checkout_route_id,
          IF(S.siteName IS NULL OR S.siteName = '0' ,
              'ไม่มีไซต์', S.siteName ) as siteName,
            IF(B.brancheName IS NULL OR B.brancheName = '0',
              'ไม่มีสาขา', B.brancheName) as brancheName,
          UU.firstname as approveBy
          FROM tat_data T
          LEFT JOIN users U ON T.employee_id = U.employee_id
          LEFT JOIN sites S ON T.site_id = S.site_id
          LEFT JOIN branches B ON T.branche_id = B.branche_id
          LEFT JOIN working W ON T.wk_id = W.wk_id
          LEFT JOIN users UU ON T.approvedBy = UU.employee_id
          WHERE T.company_id = '$company' AND T.tat_month = '$month'
          AND T.employee_id = '$id'
          ORDER BY CAST(T.tat_day as unsigned);";
          return $SQL;
  }

  public function chkDayoff($wk){
    $SQL = "SELECT if(wk_mon_start = '00:00:00',0,1) as '1',
            if(wk_tue_start = '00:00:00',0,1) as '2',
            if(wk_wed_start = '00:00:00',0,1) as '3',
            if(wk_thu_start = '00:00:00',0,1) as '4',
            if(wk_fri_start = '00:00:00',0,1) as '5',
            if(wk_sat_start = '00:00:00',0,1) as '6',
            if(wk_sun_start = '00:00:00',0,1) as '0'
            FROM tiktrack_beta.working where wk_id = '". $wk ."';";
    $QRY = $this->db->query($SQL)->result_array();
    return $QRY;
  }

  public function OvertimeMonthlySummaryByPerson($date_start,$date_end,$employee_id)
  {
    $SQL = "SELECT
                    O.employee_id,
                    O.code_site,
                    O.otd_multiple AS multiple,
                    sum(IF(O.adjust_hour <> 0 OR O.adjust_hour = null OR O.expenses_paid <> 0 OR O.expenses_paid = '' OR O.expenses_paid = null, LPAD(O.adjust_hour, 2, '0'),
                    IF(TIMEDIFF(O.otd_date_end, O.otd_date_start) > 0,
                        TIME_FORMAT(TIMEDIFF(DATE_ADD(O.otd_date_end,
                                            INTERVAL TIME_TO_SEC(O.otd_time_end) SECOND),
                                        DATE_ADD(O.otd_date_start,
                                            INTERVAL TIME_TO_SEC(O.otd_time_start) SECOND)),
                                '%H'),
                        TIME_FORMAT(TIMEDIFF(O.otd_time_end, O.otd_time_start),
                                '%H')))) AS req_hour,
                    sum(IF(O.adjust_minute <> 0 OR O.adjust_minute = null OR O.expenses_paid <> 0 OR O.expenses_paid = '' OR O.expenses_paid = null, LPAD(O.adjust_minute, 2, '0'),
                    IF(TIMEDIFF(O.otd_date_end, O.otd_date_start) > 0,
                        TIME_FORMAT(TIMEDIFF(DATE_ADD(O.otd_date_end,
                                            INTERVAL TIME_TO_SEC(O.otd_time_end) SECOND),
                                        DATE_ADD(O.otd_date_start,
                                            INTERVAL TIME_TO_SEC(O.otd_time_start) SECOND)),
                                '%i'),
                        TIME_FORMAT(TIMEDIFF(O.otd_time_end, O.otd_time_start),
                                '%i')))) AS req_min,
                    TD.tat_id,
                    IF(DATE_FORMAT(TD.checkout_time_client, '%d') >= DATE_FORMAT(O.otd_date_end, '%d'),
                        DATE_FORMAT(TD.checkout_time_client, '%d/%m/%Y'),
                        'ยังไม่มีข้อมูล') AS actualOutDate,
                    IF(DATE_FORMAT(TD.checkout_time_client, '%d') >= DATE_FORMAT(O.otd_date_end, '%d'),
                        DATE_FORMAT(TD.checkout_time_client, '%H:%i'),
                        'ยังไม่มีข้อมูล') AS actualOutTime,
                    IF(TD.checkout_time_client IS NULL,
                        '0',
                        TIME_FORMAT(TIMEDIFF(TD.checkout_time_client,
                                        DATE_ADD(O.otd_date_start,
                                            INTERVAL TIME_TO_SEC(O.otd_time_start) SECOND)),
                                '%H')) AS actual_hour,
                    IF(TD.checkout_time_client IS NULL,
                        '0',
                        TIME_FORMAT(TIMEDIFF(TD.checkout_time_client,
                                        DATE_ADD(O.otd_date_start,
                                            INTERVAL TIME_TO_SEC(O.otd_time_start) SECOND)),
                                '%i')) AS actual_min,
                    O.adjust_hour,
                    O.adjust_minute,
                    count(DISTINCT(O.otd_date_start))  as expenses_day,
                    sum(O.expenses_paid) as expenses_paid
                FROM
                    ot_data AS O
                        LEFT JOIN
                    tat_data AS TD ON O.employee_id = TD.employee_id
                        AND DATE_FORMAT(O.otd_date_start, '%d/%m/%Y') = DATE_FORMAT(TD.checkin_time_client, '%d/%m/%Y')
                WHERE
                O.otd_status in ('1','7')
                AND DATE_FORMAT(O.otd_date_start, '%Y-%m-%d') >= '$date_start'
                AND DATE_FORMAT(O.otd_date_start, '%Y-%m-%d') <= '$date_end'
                AND O.employee_id = '$employee_id'
         GROUP BY O.otd_multiple";
    $QRY = $this->db->query($SQL);
    $ROW["Has"] = false;
    if ($QRY->num_rows() > 0) {

        //rearrange Overtime
        $output_data_hq  = array();
        $output_data_site  = array();
        $output_data_all  = array();
        foreach ($QRY->result_array() as $item) {
          $select_hour = 0;
          $select_minute = 0;

          // if($item['adjust_hour'] != 0 && $item['adjust_minute'] != 0)
          // {
          //   //supervisor is adjusted
          //   $select_hour = $item['adjust_hour'];
          //   $select_minute = $item['adjust_minute'];
          // }else{
          //   //check actual and request
          //   if($item['actual_hour'] >= 0 && $item['actual_min'] >= 0
          //   && $item['actual_hour'] <= $item['req_hour'] && $item['actual_min'] <= $item['req_min'])
          //   {
          //     $select_hour = $item['actual_hour'];
          //     $select_minute = $item['actual_min'];
          //   }else{
          //     $select_hour = $item['req_hour'];
          //     $select_minute = $item['req_min'];
          //   }
          // }

          $select_hour = $item['req_hour'];
          $select_minute = $item['req_min'];

          $select_hour += floor($select_minute/60);
          $select_minute = ($select_minute % 60);

          $for_push = array('multiple' => $item['multiple'],'hour'=>$select_hour,'minute'=>$select_minute,'code_site' => '' );
          $for_push['expenses_day'] = $item['expenses_day'];
          $for_push['expenses_paid'] = $item['expenses_paid'];
          if($item['code_site'] != NULL)
          {
            $for_push['code_site'] = $item['code_site'];            
            array_push($output_data_site,$for_push);
          }else{
            array_push($output_data_hq,$for_push);
          }

          if(array_key_exists($item['multiple'],$output_data_all))
          {
            $old_hour = $output_data_all[$item['multiple']]['hour'];
            $old_minute = $output_data_all[$item['multiple']]['miute'];
            $new_calculate_minute = $select_minute + $old_minute;
            $new_calculate_hour = $old_hour + $select_hour;
            if ($new_calculate_minute >= 60)
            {
              $new_calculate_hour += 1;
              $new_calculate_minute = $new_calculate_minute - 60 ;
            }

            $output_data_all[$item['multiple']]  = array('hour' =>  $new_calculate_hour, 'minute'=> $new_calculate_minute);

          }else{
            $output_data_all[$item['multiple']]  = array('hour' =>  $select_hour, 'minute'=> $select_minute);
          }
        }//end for each

        $ROW["data"] = array('overtime_hq' => $output_data_hq, 'overtime_site' => $output_data_site , 'overtime_all' => $output_data_all);
        $ROW["Has"] = true;
    }
    return $ROW;


  }

  public function AbsenseMonthlySummaryByPerson($date_start,$date_end,$employee_id,$hd_id,$startworkdate)
  {
    $SQL = "SELECT
                calendar.*,
                WEEKDAY(calendar.calendar_date),
                tat_temp.check_in,
                leave_temp.*
            FROM
                tiktrack_beta.calendar
                    LEFT JOIN
                (SELECT
                    DATE_FORMAT(tat_data.checkin_time_client, '%Y-%m-%d') AS check_in
                FROM
                    tat_data
                WHERE
            		approveStatus = '1' AND
                    tat_data.employee_id = '$employee_id') AS tat_temp ON calendar.calendar_date = tat_temp.check_in
                    LEFT JOIN
                (SELECT
                lev_data_id ,
                    DATE_FORMAT(lev_data_date_start, '%Y-%m-%d') AS date_leave,
                        DATE_FORMAT(lev_data_date_end, '%Y-%m-%d') AS date_leave_end,
                        lev_data_d_day AS `day`,
                        lev_data_d_hour AS `hour`,
                        lev_data_d_minute AS `minute`
                FROM
                    leave_data
                WHERE
            		lev_approve_status = '1' AND
                    employee_id = '$employee_id'
                        AND DATE_FORMAT(lev_data_date_start, '%Y') >= '2019') AS leave_temp ON calendar.calendar_date = leave_temp.date_leave
                    OR IF(calendar.calendar_date >= leave_temp.date_leave AND  calendar.calendar_date <= leave_temp.date_leave_end, leave_temp.date_leave,NULL)
            WHERE
                WEEKDAY(calendar.calendar_date) IN ('0' , '1', '2', '3', '4')
                    AND (calendar.calendar_date) NOT IN (SELECT hdd_date FROM tiktrack_beta.holiday_detail where hd_id = '16' and hdd_status = '1' and hdd_date between '$date_start' and '$date_end')
                    AND (calendar.calendar_date) >= '$startworkdate'
                    AND STR_TO_DATE(calendar.calendar_date,'%Y-%m-%d')  >= '$date_start'
                    AND STR_TO_DATE(calendar.calendar_date,'%Y-%m-%d')  < '$date_end'
            ORDER BY calendar.calendar_date";
    $QRY = $this->db->query($SQL);
    $ROW["Has"] = false;
    if ($QRY->num_rows() > 0) {
      $absense = 0;
        foreach ($QRY->result_array() as $item) {
          if($item['check_in'] == NULL && $item['lev_data_id'] == NULL)
          {
            //not check in and no data at leave_data
            $absense++;
          }
        }

        $ROW["data"] = $absense;
        $ROW["Has"] = true;
    }
    return $ROW;
  }

  public function TimeoffDeductMonthlySummaryByPerson($date_start,$date_end,$employee_id)
  {
    $SQL = "SELECT
                SUM(L.lev_data_d_day) AS `day`,
                SUM(L.lev_data_d_hour) AS `hour`,
                SUM(L.lev_data_d_minute) AS `minute`
            FROM
                tiktrack_beta.leave_data AS L
            WHERE
                L.leave_type IN (SELECT
                        levd_type
                    FROM
                        tiktrack_beta.leave_detail
                    WHERE
                        levd_is_deduct_money = '1'
                            AND lev_id = '5'
                            AND lev_status = '1')
                    AND L.lev_approve_status = '1'
                    AND DATE_FORMAT(L.lev_data_date_start, '%Y-%m-%d') >= '$date_start'
                    AND DATE_FORMAT(L.lev_data_date_end, '%Y-%m-%d') <= '$date_end'
                    AND L.employee_id = '$employee_id'";
    $QRY = $this->db->query($SQL);

    $ROW["Has"] = false;
    if ($QRY->num_rows() > 0) {
        $ROW["data"] = $QRY->result_array();
        $ROW["Has"] = true;
    }
    return $ROW;

  }
  public function GetUserHeaderOT($employee_id)
  {
    $SQL = "SELECT CONCAT(firstname, ' ',lastname) as fullname, department.dep_name
            FROM users  LEFT JOIN department ON department.dep_id = users.dep_id
            WHERE employee_id = '$employee_id'";
    $rows = $QRY = $this->db->query($SQL)->result_array();
    $ROW["Has"] = false;
    if (count($rows)> 0) {
        $ROW["data"] = $rows[0];
        $ROW["Has"] = true;
    }
    return $ROW;

  }
  public function OTSummaryPerson($date_start,$date_end,$employee_id)
  {
    $SQL = "SELECT
              DATE_FORMAT(O.createdAt, '%d/%m/%Y') AS request_date,
              DATE_FORMAT(O.otd_date_start, '%d/%m/%Y') AS req_todo_date,
              WEEKDAY(O.otd_date_start) AS weekday,
              DATE_FORMAT(O.otd_time_start, '%H:%i') AS time_start,
              DATE_FORMAT(O.otd_time_end, '%H:%i') AS time_end,
              O.otd_detail AS detail,
              O.code_site,
              O.expenses_paid,
              O.otd_multiple AS multiple,
              IF(O.adjust_hour <> 0 OR O.adjust_hour = null,LPAD(O.adjust_hour, 2, '0'),
              IF(TIMEDIFF(O.otd_date_end, O.otd_date_start) > 0,
                  TIME_FORMAT(TIMEDIFF(DATE_ADD(O.otd_date_end,
                                      INTERVAL TIME_TO_SEC(O.otd_time_end) SECOND),
                                  DATE_ADD(O.otd_date_start,
                                      INTERVAL TIME_TO_SEC(O.otd_time_start) SECOND)),
                          '%H'),
                  TIME_FORMAT(TIMEDIFF(O.otd_time_end, O.otd_time_start),
                          '%H')))
               AS req_hour,
              IF(O.adjust_minute <> 0 OR O.adjust_minute = null,LPAD(O.adjust_minute, 2, '0'),
				      IF(TIMEDIFF(O.otd_date_end, O.otd_date_start) > 0,
                  TIME_FORMAT(TIMEDIFF(DATE_ADD(O.otd_date_end,
                                      INTERVAL TIME_TO_SEC(O.otd_time_end) SECOND),
                                  DATE_ADD(O.otd_date_start,
                                      INTERVAL TIME_TO_SEC(O.otd_time_start) SECOND)),
                          '%i'),
                  TIME_FORMAT(TIMEDIFF(O.otd_time_end, O.otd_time_start),
                          '%i')))
                          AS req_min,
              U.firstname AS sign_emp,
              UA.firstname AS sign_super,
              UAA.firstname AS sign_paid,
              IF(T.checkin_type_id = '1',
                  DATE_FORMAT(T.checkin_time_server, '%H:%i'),
                  DATE_FORMAT(T.checkin_time_client, '%H:%i')) AS checkin_time,
              IF(T.checkin_type_id = '2',
                  DATE_FORMAT(T.checkout_time_server, '%H:%i'),
                  DATE_FORMAT(T.checkout_time_client, '%H:%i')) AS checkout_time
          FROM
              ot_data AS O
                  LEFT JOIN
              users AS U ON O.employee_id = U.employee_id
                  LEFT JOIN
              users AS UA ON O.approveBy = UA.employee_id
                  LEFT JOIN
              users AS UAA ON O.approvePaidBy = UAA.employee_id
                  LEFT JOIN
              tat_data AS T ON  O.employee_id = T.employee_id AND DATE_FORMAT(O.otd_date_start, '%Y-%m-%d') = DATE_FORMAT(T.checkin_time_client, '%Y-%m-%d')
          WHERE
              O.otd_status in ('1','7')
                  AND DATE_FORMAT(O.otd_date_start, '%Y-%m-%d') >= '$date_start'
                  AND DATE_FORMAT(O.otd_date_start, '%Y-%m-%d') <= '$date_end'
                  AND O.employee_id = '$employee_id' 
                  ORDER BY DATE_FORMAT(O.otd_date_start, '%Y-%m-%d') asc";
    $QRY = $this->db->query($SQL);
    $rows = $QRY->result_array();

    $ROW["Has"] = false;
    if (count($rows) > 0) {
        $ROW["data"] = $rows;
        $ROW["Has"] = true;

    }
    return $ROW;

  }


  // public function AbsenseMonthlySummaryByPerson_test($date_start,$date_end,$hd_id,$startworkdate)
  // {
  //   $SQL = "SELECT
  //               calendar.*,
  //               WEEKDAY(calendar.calendar_date),
  //               tat_temp.check_in,
  //               leave_temp.*
  //           FROM
  //               tiktrack_beta.calendar
  //                   LEFT JOIN
  //               (SELECT
  //                   DATE_FORMAT(tat_data.checkin_time_client, '%Y-%m-%d') AS check_in
  //               FROM
  //                   tat_data
  //               WHERE
  //           		approveStatus = '1' AND
  //                   tat_data.employee_id = '$employee_id') AS tat_temp ON calendar.calendar_date = tat_temp.check_in
  //                   LEFT JOIN
  //               (SELECT
  //               lev_data_id ,
  //                   DATE_FORMAT(lev_data_date_start, '%Y-%m-%d') AS date_leave,
  //                       DATE_FORMAT(lev_data_date_end, '%Y-%m-%d') AS date_leave_end,
  //                       lev_data_d_day AS `day`,
  //                       lev_data_d_hour AS `hour`,
  //                       lev_data_d_minute AS `minute`
  //               FROM
  //                   leave_data
  //               WHERE
  //           		lev_approve_status = '1' AND
  //                   employee_id = '$employee_id'
  //                       AND DATE_FORMAT(lev_data_date_start, '%Y') >= '2019') AS leave_temp ON calendar.calendar_date = leave_temp.date_leave
  //                   OR IF(calendar.calendar_date >= leave_temp.date_leave AND  calendar.calendar_date <= leave_temp.date_leave_end, leave_temp.date_leave,NULL)
  //           WHERE
  //               WEEKDAY(calendar.calendar_date) IN ('0' , '1', '2', '3', '4')
  //                   AND (calendar.calendar_date) NOT IN (SELECT hdd_date FROM tiktrack_beta.holiday_detail where hd_id = '16' and hdd_status = '1' and hdd_date between '$date_start' and '$date_end')
  //                   AND (calendar.calendar_date) >= '$startworkdate'
  //                   AND STR_TO_DATE(calendar.calendar_date,'%Y-%m-%d')  >= '$date_start'
  //                   AND STR_TO_DATE(calendar.calendar_date,'%Y-%m-%d')  < '$date_end'
  //           ORDER BY calendar.calendar_date";
  //   $QRY = $this->db->query($SQL);
  //   $ROW["Has"] = false;
  //   if ($QRY->num_rows() > 0) {
  //     $absense = 0;
  //       foreach ($QRY->result_array() as $item) {
  //         if($item['check_in'] == NULL && $item['lev_data_id'] == NULL)
  //         {
  //           //not check in and no data at leave_data
  //           $absense++;
  //         }
  //       }

  //       $ROW["data"] = $absense;
  //       $ROW["Has"] = true;
  //   }
  //   return $ROW;
  // }

}
