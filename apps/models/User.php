<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Model{

  public  $company;
  public  $auth;
  private $now;
  public  $id;
  public  $name;
  public  $site;
  public  $branche;
  public  $dep;
  public  $type;

  public  $lat;
  public  $lng;
  public  $area;

  function __construct()
  {
      parent::__construct();
      $this->now = date("Y-m-d H:i:s");
  }

  public function getBy($id){
        $SQL = "SELECT a.*,b.siteName,if(c.brancheName IS NULL,'-',c.brancheName) as brancheName,d.position_name FROM tiktrack_beta.users a LEFT JOIN sites b ON a.site_id = b.site_id
        LEFT JOIN branches c ON a.branche_id = c.branche_id
        LEFT JOIN positions d ON a.position_id = d.position_id
        where a.employee_id = '$id';";
        $QRY = $this->db->query($SQL);
        $ROW["Has"] = false;
        if ($QRY->num_rows() > 0) {
            $ROW["Has"] = true;
            $ROW["data"] = $QRY->result_array();
        }
        return $ROW;
  }

  public function getAllUser($type,$emp_id,$tempsite,$tempbranche,$tempdep){
    $SQL = "SELECT * FROM users ";
    $CONDITION = "";
    if ($type == "0") {
      $CONDITION = " WHERE employee_id = '$emp_id'";
    }else if($type == "1"){
      $CONDITION = " WHERE site_id = '$tempsite' AND branche_id = '$tempbranche'";
    }else if($type == "2"){
      $CONDITION = " WHERE dep_id = '". $tempdep ."' ";
    }
    $SQL = $SQL.$CONDITION." AND userStatus = '1'";
    $RSL = $this->db->query($SQL)->result_array();
    return $RSL;
  }

  public function getUserBranche($branche_id)
{
  $SQL = "SELECT U.employee_id,
          CONCAT(U.firstname,' ',U.lastname) AS fullname,
          B.brancheName,
          if(W.wk_name IS NULL,'ยังไม่เชื่อมต่อวันทำการ',W.wk_name) as wk_name,
          U.hd_id,
          U.startworkdate
          FROM tiktrack_beta.users  U
          LEFT JOIN branches B ON U.branche_id = B.branche_id
          LEFT JOIN working W ON U.wk_id = W.wk_id
          WHERE U.company_id = '1' AND U.branche_id = '".$branche_id."' AND U.userStatus ='1' ";
  $QRY = $this->db->query($SQL);
  $ROW = $QRY->result_array();
  $Callback  = array('Has' =>FALSE );
  if(count($ROW)>0)
  {
    $Callback  = array('Has' =>TRUE,'data' => $ROW );
  }
  return $Callback;
}
}
