<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ReportModels extends CI_Model{

  public  $company;
  public  $auth;
  public  $site;
  public  $notRep;
  public  $insite;
  private $now;


  function __construct()
  {
      parent::__construct();
      $this->now = date("Y-m-d H:i:s");
  }

  public function getLeaveReport($status,$company,$auth,$year,$month){
    $SQL = "SELECT leave_data.*,concat(users.firstname,' ',users.lastname) as fullname, sites.siteName,if(leave_data.branche_id = 0,'ไม่มีสาขา',branches.brancheName) as brancheName FROM tiktrack_beta.leave_data
    LEFT JOIN users ON leave_data.employee_id = users.employee_id
    LEFT JOIN sites ON leave_data.site_id = sites.site_id
    LEFT JOIN branches ON leave_data.branche_id = branches.branche_id
    WHERE leave_data.lev_approve_status = '$status' AND YEAR(createAt) = '". $year ."' AND month(createAt) = '". $month ."'   AND leave_data.site_id IN (SELECT  DISTINCT sites.site_id FROM sites
           RIGHT JOIN site_permission ON sites.site_id = site_permission.site_id
           WHERE sites.company_id = '". $company ."' AND sites.site_status = '1'  AND site_permission.employee_id = from_base64('". $auth ."') AND site_permission.site_perm_status = '1'
           OR sites.site_id IN
           (SELECT branches.site_id FROM branches
           RIGHT JOIN branches_permission ON branches.branche_id = branches_permission.branche_id
           WHERE branches.company_id = '". $company ."' AND branches_permission.employee_id = from_base64('". $auth ."')  AND branches_permission.branche_perm_status = '1')) order by leave_data.lev_data_id;";
    $QRY = $this->db->query($SQL)->result_array();;
    return $QRY;
  }

}
