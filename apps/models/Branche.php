<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Branche extends CI_Model{

  public  $company;
  public  $auth;
  private $now;
  public  $id;
  public  $name;
  public  $site;
  public  $lat;
  public  $lng;
  public  $area;
  public  $branche_id;

  function __construct()
  {
      parent::__construct();
      $this->now = date("Y-m-d H:i:s");
  }

  public function getAll(){
      $SQL = "SELECT * FROM branches WHERE site_id = '".$this->site."' ";
      $QRY = $this->db->query($SQL);
      $callback = array();
      $callback["Has"] = false;
      if ($QRY->num_rows() > 0) {
          $callback["Has"] = TRUE;
          $callback["data"] = $QRY->result_array();
      }
      return $callback;
  }

  public function getBy(){
        $SQL = "SELECT a.*,b.siteName FROM branches a LEFT JOIN sites b ON a.site_id = b.site_id WHERE a.branche_id = '$this->branche_id' ";
        $QRY = $this->db->query($SQL);
        $ROW["Has"] = false;
        if ($QRY->num_rows() > 0) {
            $ROW["Has"] = true;
            $ROW["data"] = $QRY->result_array();
        }
        return $ROW;
  }

  public function add($request){
      $name = $request["name"];
      $lat = $request["lat"];
      $lng = $request["lng"];
      $area = $request["area"];
      $segment = base64_decode(base64_decode(base64_decode($request["segment"])));
      $last = $this->getLastId();
      $new_id = 1;
      if (count($last) > 0) {
          $new_id = intval($last[0]["branche_id"]);
          $new_id ++;
      }
      $wk = $request["wk"];
      $data = array(
          "branche_id" => $new_id,
          "brancheName" => $name,
          "brancheStatus" => 1,
          "usedTimeSite" => 0,
          "lat" => $lat,
          "lng" => $lng,
          "area" => $area,
          "siteId" => $segment,
          "company_Id" => $this->company,
          "createdBy" => $this->auth,
          "createdAt" => $this->now,
          "updatedBy" => $this->auth,
          "updatedAt" => $this->now,
          "brancheDefault" => 0,
      );
      for ($i=0; $i < count($wk); $i++) {
           $num = $i+1;
           $data_setting = array(
              "brancheId" => $new_id,
              "t_no" => $num,
              "branche_setting_status" => 1,
              "wk_id" => $wk[$i],
              "ot_id" => 0
           );
           // print_r($data_setting);
           $add_setting = $this->db->insert('ms_branche_setting',$data_setting);
      }
      $add = $this->db->insert('branches',$data);
      if ($add) {
          return true;
      }else{
          return false;
      }
  }

  public function getLastId(){
      $SQL = "SELECT branche_id FROM branches ORDER BY branche_id DESC LIMIT 1";
      $QRY = $this->db->query($SQL);
      $ROW = $QRY->result_array();
      return $ROW;
  }

}
