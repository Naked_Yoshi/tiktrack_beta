<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Position extends CI_Model{

  // public  $company;
  // public  $auth;
  // private $now;
public $root;
public $company;
public $firstRoot = "";
public $post;
public $id = "";
public $dep = "";
public $key = "";
public  $auth;
private $now;
private $container =  array();


  function __construct()
  {
      parent::__construct();
      $this->now = date("Y-m-d H:i:s");
  }

 public function add($request){
   $name = $request["positionName"];
   $dep = $request["dep"];
   $root = $request["root"];
   $siteAdd = 0;
   $siteEdit = 0;
   $siteDel = 0;
   $siteApprove = 0;
   $brancheAdd = 0;
   $brancheEdit = 0;
   $brancheDel = 0;
   $brancheApprove = 0;
   $otAdd=0;
   $otEdit=0;
   $otDel=0;
   $otApprove=0;
   $userAdd=0;
   $userEdit=0;
   $userDel=0;
   if (isset($request["siteAdd"])) {
   		$siteAdd = 1;
   }
   if (isset($request["siteEdit"])) {
   		$siteEdit = 1;
   }
   if (isset($request["siteDel"])) {
   		$siteDel = 1;
   }
   if (isset($request["siteApprove"])) {
   		$siteApprove = 1;
   }
   if (isset($request["brancheAdd"])) {
   		$brancheAdd = 1;
   }
   if (isset($request["brancheEdit"])) {
   		$brancheEdit = 1;
   }
   if (isset($request["brancheDel"])) {
   		$brancheDel = 1;
   }
   if (isset($request["brancheApprove"])) {
   		$brancheApprove = 1;
   }
   if (isset($request["otAdd"])) {
       $otAdd = 1;
   }
   if (isset($request["otEdit"])) {
       $otEdit = 1;
   }
   if (isset($request["otDel"])) {
       $otDel = 1;
   }
   if (isset($request["otApprove"])) {
       $otApprove = 1;
   }
   if (isset($request["userAdd"])) {
       $userAdd = 1;
   }
   if (isset($request["userEdit"])) {
       $userEdit = 1;
   }
   if (isset($request["userDel"])) {
       $userDel = 1;
   }

   $data = array(
      "position_id" => "",
      "dep_id" => $dep,
      "position_name" => $name,
      "position_root" => $root,
      "add_site" => $siteAdd,
      "edit_site" => $siteEdit,
      "delete_site" => $siteDel,
      "approve_site" => $siteApprove,
      "add_branche" => $brancheAdd,
      "edit_branche" => $brancheEdit,
      "delete_branche" => $brancheDel,
      "approve_branche" => $brancheApprove,
      "add_ot" =>$otAdd,
      "edit_ot" => $otEdit,
      "delete_ot" => $otDel,
    "approve_ot" =>  $otApprove,
    "add_user" =>  $userAdd,
    "edit_user" =>  $userEdit,
    "delete_user" =>  $userDel,
      "position_status" => "1",
      "createdBy" => $this->auth,
      "createdAt" => $this->now,
      "updatedBy" => $this->auth,
      "updatedAt" => $this->now,
      "company_id" => $this->company
   );

   $add = $this->db->insert('positions',$data);
   if ($add) {
       return true;
   }else{
       return false;
   }

 }

 public function getAll($DEP = "",$ROOT = ""){
   $STR_SEARCHBY_DEP = "";
   $STR_SEARCHBY_ROOT = "";

   if ($DEP != "") {
       $STR_SEARCHBY_DEP = "AND a.dep_id = '$DEP' ";
       // echo  $STR_SEARCHBY_DEP;

   }
   if ( $DEP == "" and $ROOT != "") {
       $STR_SEARCHBY_ROOT = "AND a.dep_id = '$ROOT' ";

   }
   $SQL = "SELECT * FROM positions as a
           INNER JOIN department as b
           ON a.dep_id = b.dep_id

               WHERE a.company_id = '".$this->company."' ".$STR_SEARCHBY_DEP." ".$STR_SEARCHBY_ROOT;
// ECHO $SQL;
   $QRY = $this->db->query($SQL);


   $ROW["Has"] = false;
   if ($QRY->num_rows() > 0) {
       $ROW["data"] = $QRY->result_array();
       $ROW["Has"] = true;
   }
   return $ROW;

 }

 public function getBy($root){
    $SQL = "SELECT * FROM positions WHERE position_id = '$root' ";
    $QRY = $this->db->query($SQL);

    $callback = array();
    $callback["Has"] = false;
    if ($QRY->num_rows() > 0) {
        $callback["Has"] = true;
        $callback["detail"] = $QRY->result_array();
    }
    return $callback;

 }

 public function getrootPosition($department = ""){
      $str = "";
      if ($department != "") {
          $str .= " AND positions.dep_id = '$department' ";
      }
      if ($this->firstRoot != "") {
          $str .= "AND positions.position_root = '0'";

      }
      if ($this->id != "") {
          $str .= "AND positions.position_id = '".$this->id."'";
      }
      if ($this->key != "") {
          $str .= "AND positions.position_name LIKE '%".$this->key."%'";
      }

      // $company = $this->company;
      $callback = array();
      $SQL = "SELECT * FROM positions INNER JOIN department ON positions.dep_id = department.dep_id   WHERE  positions.company_id = '". $this->company ."'   AND positions.position_status = '1' AND positions.is_display_org = '1' ".$str;
       // AND positions.is_display_org = '1';

      $QRY = $this->db->query($SQL);
      $NUM = $QRY->num_rows();
      $callback["hasRoot"] = FALSE;
      if ($NUM > 0) {
         $callback["hasRoot"] = TRUE;
         $callback["Root"] = $QRY->result_array();
      }

      return $callback;
}
public function getsubPosition(){
			 $root = $this->root;
			 $company = $this->company;
			 $callback = array();
			 $SQL = "SELECT * FROM positions WHERE  company_id = '$company' AND position_root = '$root' AND position_status = '1' AND positions.is_display_org = '1' ";
          // AND positions.is_display_org = '1';
			 $QRY = $this->db->query($SQL);
			 $NUM = $QRY->num_rows();
			 $callback["hasPosition"] = FALSE;
			 if ($NUM > 0) {
					$callback["hasPosition"] = TRUE;
					$callback["Position"] = $QRY->result_array();
			 }
			 return $callback;
}

}
