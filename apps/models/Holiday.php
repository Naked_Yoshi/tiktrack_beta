<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Holiday extends CI_Model{

  public  $company;
  public  $auth;
  private $now;
  public  $id;
  public  $name;
  public  $site;

  function __construct()
  {
      parent::__construct();
      $this->now = date("Y-m-d H:i:s");
  }


  public function getAll($req){

      $site_str = "";
      $branche_str = "";
      if ($req["segment"] != "") {
          $site_str = " AND site_id = '".base64_decode(base64_decode(base64_decode($req["segment"])))."' ";
      }
      if ($req["branche"] != "") {
          $branche_str = "  AND branche_id = '".base64_decode(base64_decode(base64_decode($req["branche"])))."'  ";
      }
      $SQL = "SELECT * FROM holiday WHERE company_id = '".$this->company."'".$site_str.$branche_str;
      $QRY = $this->db->query($SQL);
      $ROW["Has"] = false;
      if ($QRY->num_rows() > 0) {
          $ROW["Has"] = true;
          $ROW["data"] = $QRY->result_array();
      }
      return $ROW;
  }

  public function getBy($id){
      $SQL = "SELECT * FROM holiday WHERE hd_id = '$id' ";
      $QRY = $this->db->query($SQL);
      $ROW["Has"] = false;
      if ($QRY->num_rows() > 0) {
          $ROW["Has"] = true;
          $ROW["data"] = $QRY->result_array();
      }
      return $ROW;
  }
  public function getDetailBy($id){
      $SQL = "SELECT * FROM holiday as h
              INNER JOIN holiday_detail as hd
              ON h.hd_id = hd.hd_id
              WHERE h.hd_id = '$id' ";
      $QRY = $this->db->query($SQL);
      $ROW["Has"] = false;
      if ($QRY->num_rows() > 0) {
          $ROW["Has"] = true;
          $ROWS = $QRY->result_array();
          $data = array();
          for ($i=0; $i < count($ROWS); $i++) {
               $data_array = array(
                 "hdd_date" => $this->Helpers->setDesplayDate($ROWS[$i]["hdd_date"]),
                 "hdd_caption" => $ROWS[$i]["hdd_caption"],
               );
               array_push($data,$data_array);
          }
          $ROW["data"] = $data;
      }
      return $ROW;
  }

  public function add($request){
      $name = $request["name"];
      $list = $request["list"];
      $segment = base64_decode(base64_decode(base64_decode($request["segment"])));
      $last = $this->getLastId();
      $new_id = 1;
      if (count($last) > 0) {
          $new_id = intval($last[0]["hd_id"]);
          $new_id ++;
      }
      $data = array(
          "hd_id" => $new_id,
          "hd_name" => $name,
          "hd_status" => 1,
          "createdBy" => $this->auth,
          "createdAt" => $this->now,
          "updatedBy" => $this->auth,
          "updatedAt" => $this->now,
          "company_id" => $this->company,
          "site_id" => $segment,
          "branche_id" => 0,
      );
      $num = 1;
      for ($i=0; $i < count($list); $i++) {
        $this->addDetail($new_id,$num,$list[$i]);
        $num ++;
      }
      $add = $this->db->insert('holiday',$data);
      if ($add) {
          return true;
      }else{
          return false;
      }
  }
  public function addDetail($id,$no,$arr){
          $data = array(
            "hd_id" => $id,
            "hdd_no" => $no,
            "hdd_date" => $this->convertFormatToDatabase($arr["date"]),
            "hdd_caption" => $arr["caption"],
            "hdd_status" => 1
          );
          $add = $this->db->insert('holiday_detail',$data);
          if ($add) {
              return true;
          }else{
              return false;
          }
  }
  public function getLastId(){
      $SQL = "SELECT hd_id FROM holiday ORDER BY hd_id DESC LIMIT 1";
      $QRY = $this->db->query($SQL);
      $ROW = $QRY->result_array();
      return $ROW;
  }
  public function convertFormatToDatabase($dateIn){
      $arr = explode("/",$dateIn);
      return $arr[2]."-".$arr[1]."-".$arr[0];
  }
}
