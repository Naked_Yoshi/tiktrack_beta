<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class OT extends CI_Model{

  public  $company;
  public  $auth;
  public  $site;
  public  $notRep;
  public  $insite;
  private $now;


  function __construct()
  {
      parent::__construct();
      $this->now = date("Y-m-d H:i:s");
  }


  public function getAll($req){
      $site_str = "";
      $branche_str = "";
      if ($req["segment"] != "") {
          $site_str = " OR (company_id = '".$this->company."' AND site_id = '".base64_decode(base64_decode(base64_decode($req["segment"])))."' ) ";
      }
      if ($req["branche"] != "") {
          $branche_str = " OR (company_id = '".$this->company."' AND branche_id = '".base64_decode(base64_decode(base64_decode($req["branche"])))."' ) ";
      }
      $SQL = "SELECT * FROM ot_config WHERE ot_status = '1' AND (company_id = '".$this->company."' AND site_id = '0' AND ot_public = '1') ".$site_str.$branche_str;
      $QRY = $this->db->query($SQL);
      $ROW["Has"] = false;
      if ($QRY->num_rows() > 0) {
          $ROW["data"] = $QRY->result_array();
          $ROW["Has"] = true;
      }
      return $ROW;
  }

  public function getNormalState($company_id,$employee_id,$status)
  {
    $SQL = "SELECT O.otd_id, O.is_urgent, CONCAT(U.firstname, ' ' , U.lastname) as fullName,
            DATE_FORMAT(O.otd_date_start,'%d/%m/%Y') as date_start,
            DATE_FORMAT(O.otd_date_end,'%d/%m/%Y') as date_end,
            DATE_FORMAT(O.otd_time_start,'%H:%i') as time_start,
            DATE_FORMAT(O.otd_time_end,'%H:%i') as time_end,
            O.otd_multiple as multiple,
            IF(O.otd_multiple = OC.ot_working,
            	'วันธรรมดา',
                IF(O.otd_multiple = OC.ot_weekend,
            		'วันหยุด',
                    'วันหยุดนักขัตฤกษ์')) as multiple_str,
            O.otd_detail as detail,
            O.otd_status as status,
            T.otda_name as ot_status_str,
            IF(S.siteName IS NULL OR S.siteName = '0' ,'ไม่มีไซต์', S.siteName ) as siteName,
            IF(B.brancheName IS NULL OR B.brancheName = '0', '-', B.brancheName) as brancheName,
            DATE_FORMAT(O.createdAt,'%d/%m/%Y') as requestDate,
            DATE_FORMAT(O.createdAt,'%H:%i') as requestTime,
            DATE_FORMAT(O.updatedAt,'%d/%m/%Y') as updateDate,
            DATE_FORMAT(O.updatedAt,'%H:%i') as updateTime,
            CONCAT(UA.firstname, ' ' , UA.lastname) as updateUser,
            IF(TIMEDIFF(O.otd_date_end,O.otd_date_start) > 0 ,
            	TIME_FORMAT(TIMEDIFF(DATE_ADD(O.otd_date_end,INTERVAL TIME_TO_SEC(O.otd_time_end) SECOND ) , DATE_ADD(O.otd_date_start,INTERVAL TIME_TO_SEC(O.otd_time_start) SECOND )),'%H:%i' ),
            	TIME_FORMAT(TIMEDIFF(O.otd_time_end,O.otd_time_start),'%H:%i')) as reqTotalHours
            FROM ot_data as O
              LEFT JOIN users as U                ON O.employee_id = U.employee_id
              LEFT JOIN users as UA               ON O.updatedBy = UA.employee_id
              LEFT JOIN ot_data_approve_type as T ON O.otd_status = T.otda_type
              LEFT JOIN sites as S                ON O.site_id = S.site_id
              LEFT JOIN branches as B             ON O.branche_id = B.branche_id
              LEFT JOIN ot_config as OC        ON O.ot_id = OC.ot_id
            WHERE O.company_id = '$company_id'
              AND O.otd_status IN $status
              AND O.site_id IN ( SELECT DISTINCT M.site_id FROM ot_config as M
                               RIGHT JOIN ot_position_approve as P ON M.ot_id = P.ot_id
                               WHERE M.company_id = '$company_id' AND P.employee_id = '$employee_id' )";

    $QRY = $this->db->query($SQL);
    $ROW["Has"] = false;
    if ($QRY->num_rows() > 0) {
        $ROW["data"] = $QRY->result_array();
        $ROW["Has"] = true;
    }
    return $ROW;
  }

  public function add($req){
      $new_id = $this->getLastId();
      $groupName = $req["groupName"];
      $groupBefore = $req["groupBefore"];
      $groupAfter = $req["groupAfter"];
      // $dateOtLimit = $req["dateOtLimit"];
      // $monthOtLimit = $req["monthOtLimit"];
      $check_beforeRequest = $req["check_beforeRequest"];
      $timeBeforeRequest = $req["timeBeforeRequest"];
      $urgent = $req["urgent"];
      $check_approveRequest = $req["check_approveRequest"];
      $paidDate = $req["paidDate"];
      $check_packageRequest = $req["check_packageRequest"];
      // print_r($req);
      // return;
      $working = $req["working"];
      $working_tag = $req["working_tag"];
      $this->addPosition($new_id,0,$working_tag);

      $weekend = $req["weekend"];
      $weekend_tag = $req["weekend_tag"];
      $this->addPosition($new_id,1,$weekend_tag);

      $holiday = $req["holiday"];
      $holiday_tag = $req["holiday_tag"];
      $this->addPosition($new_id,2,$holiday_tag);

      $excess = $req["excess"];
      $num_excess = 1;
      $site = 0;
      if (isset($req["segment"])) {
          $site = base64_decode(base64_decode(base64_decode($req["segment"])));
      }
      $branche = 0;
      if (isset($req["branche"])) {
          $branche = base64_decode(base64_decode(base64_decode($req["branche"])));
      }

      for ($i=0; $i < count($excess); $i++) {
          $this->addExcess($new_id,$num_excess,$excess[$i]);
          $num_excess++;
      }


      $ot_data = array(
        "ot_id" => $new_id,
        "ot_name" => $groupName,
        "ot_before_minute" => $groupBefore,
        "ot_after_minute" => $groupAfter,
        "ot_avaliable_time_before" => $timeBeforeRequest,
        "ot_check_follow" => 0,
        "ot_urgent_request" => $urgent,
        "ot_createdBy" => $this->auth,
        "ot_createdAt" => $this->now,
        "ot_updatedBy" => $this->auth,
        "ot_updatedAt" => $this->now,
        "ot_approveBy" => $this->auth,
        "ot_approveAt" => $this->now,
        "ot_status" => 1,
        "ot_request" => $check_beforeRequest,
        "ot_approve_paid" => $check_approveRequest,
        "ot_paid_date" => $paidDate,
        "ot_level" => 0,
        "ot_package" => $check_packageRequest,
        // "ot_limit_date" => $dateOtLimit,
        // "ot_limit_month" => $monthOtLimit,
        "ot_working" => $working,
        "ot_weekend" => $weekend,
        "ot_holiday" => $holiday,
        "company_id" => $this->company,
        "site_id" => $site,
        "branche_id" => $branche,
        "ot_public" => 0,
      );

      $add = $this->db->insert('ot_config',$ot_data);
      if ($add) {
          return true;
      }else{
          return false;
      }
  }

  public function getLastId(){
    $SQL = "SELECT ot_id FROM ot_config ORDER BY ot_id DESC LIMIT 1";
    $QRY = $this->db->query($SQL);
    $ROW = $QRY->result_array();
    $new_id = 1;
    if (count($ROW) > 0) {
        $new_id = intval($ROW[0]["ot_id"]);
        $new_id ++;
    }
    return $new_id;
  }
  public function getLastPositionNo($ot){
    $SQL = "SELECT otp_no FROM ot_position_approve WHERE ot_id = '$ot' ORDER BY otp_no DESC LIMIT 1";
    $QRY = $this->db->query($SQL);
    $ROW = $QRY->result_array();
    $new_id = 1;
    if (count($ROW) > 0) {
        $new_id = intval($ROW[0]["otp_no"]);
        $new_id ++;
    }
    return $new_id;
  }
  public function addPosition($ot,$type,$data){
      for ($i=0; $i < count($data); $i++) {
          $no = $this->getLastPositionNo($ot);
          $position = array(
            "ot_id" => $ot,
            "otp_no" => $no,
            "otp_type" => $type,
            "position_id" => $data[$i][0],
            "employee_id" => $data[$i][1],
            "updatedBy" => $this->auth,
            "updatedAt" => $this->now,
            "otp_status" => 1,
          );
          $add = $this->db->insert('ot_position_approve',$position);
          if (!$add) {
              return false;
          }
      }
      return true;
  }


  public function getBy($id){
      $SQL = "SELECT * FROM ot_config WHERE ot_id = '$id' ";
      $QRY = $this->db->query($SQL);
      $ROW = $QRY->result_array();
      return $ROW;
  }

  public function getExcess($id){
      $SQL = "SELECT * FROM ot_excess as ex
              INNER JOIN ot_type as et
              ON  ex.ote_condition = et.ot_type_id
              WHERE ex.ot_id = '$id' ";
      $QRY = $this->db->query($SQL);
      $ROW = $QRY->result_array();
      $callback = array(
        "Has" => false,
      );
      if (count($ROW) > 0) {
          $callback = array(
            "Has" => true,
            "data" => $ROW
          );
      }
      return $callback;
  }

  public function getApprove($id){
      $SQL = "SELECT * FROM ot_position_approve as op
              INNER JOIN positions as p
              ON  op.position_id = p.position_id
              WHERE op.ot_id = '$id' ";
      $QRY = $this->db->query($SQL);
      $ROW = $QRY->result_array();
      $callback = array(
        "Has" => false,
      );
      if (count($ROW) > 0) {
          $callback = array(
            "Has" => true,
            "data" => $ROW
          );
      }
      return $callback;
  }

  public function getfrom_Working($wk){
      $SQL = "SELECT * FROM ot_config WHERE ot_id IN ( SELECT ot_id FROM working WHERE wk_id = '$wk' )";
      $QRY = $this->db->query($SQL);
      $ROW = $QRY->result_array();
      return $ROW;
  }

  public function addExcess($ot,$no,$data){
    // print_r($data);
      $excess = array(
        "ot_id" => $ot,
        "ote_no" => $no,
        "ote_condition" => $data["condition"],
        "ote_excess" => $data["excess"],
        "ote_result" => $data["result"],
        "ote_status" => 1,
        "updatedBy" => $this->auth,
        "updatedAt" => $this->now
      );
      $add = $this->db->insert('ot_excess',$excess);
      if ($add) {
          return true;
      }else{
          return false;
      }

  }

}
 // company_id IN ('0','3') AND wk_status = '1'
