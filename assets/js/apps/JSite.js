var wk_list = [];

$(document).ready(function() {
  $("#site_body").Sites();
});


$(".btn-create-site").click(function(){
    var url = base_url('sites/ModalCreate');
    $.ajax({
      url: url,
      type: "POST",
    })
    .done(function(data) {
        $(".modal-area").html(data);
        $("#siteCreate").modal("toggle");
        $(".btn-step").click(function(){
           var content = $(this).data("phase");
           var persent = $(this).data("persent");
           var locate = [];
           if (typeof $(this).data("progress") !== "undefined") {
              if ( $(this).data("progress") == "success") {
                  $(".tabs-progress .progress-bar ").removeClass("progress-bar-danger").addClass("progress-bar-"+$(this).data("progress"));
              }else{
                  $(".tabs-progress .progress-bar ").removeClass("progress-bar-success").addClass("progress-bar-"+$(this).data("progress"));
              }
           }
           if (content == "site_location") {
               var input_sitename = $("#sitecreateName").validate_blank();
               if (input_sitename) {
                   var lat = parseFloat($("#sitecreateLat").val());
                   var lng = parseFloat($("#sitecreateLng").val());
                   if (lat == 0 && lng == 0) {
                      initMap({lat: lat,lng: lng},"map","site");
                      geolocate("site");
                   }else{
                      initMap({lat: lat,lng: lng},"map","site");
                   }
                   initAutocomplete();
                   $("#txt_area").html($("#CircleArea_site").val());
                   $("#CircleArea_site").change(function(){
                     $("#txt_area").html($("#CircleArea_site").val());
                   });
               }else{
                   return;
               }
           }else if(content == "site_working"){
                  var input_sitename = $("#sitecreateName").validate_blank();
                  if (input_sitename) {
                      $(".tabs-working-list").workingList();
                      $("#list_wk").show();
                      $("#create_new_wk").hide();
                  }else{
                      return;
                  }
           }else if (content == "site_submit"){
               var input_sitename = $("#sitecreateName").validate_blank();
               if (input_sitename) {
                   if($("#wk_select_table tr").not(".noSelect").length < 1){
                       $.toast({heading: "ผิดพลาด !",text: "คุณยังไม่ได้กำหนดวันทำการ",position: 'top-right',loaderBg:'#ff6849',icon: 'error',hideAfter: 2000,stack: 6});
                       return;
                   }else{

                       var sitename = $("#sitecreateName").val();
                       $("#nameSubmit").html("<h4>"+sitename+"</h4>");
                       var lat = parseFloat($("#sitecreateLat").val());
                       var lng = parseFloat($("#sitecreateLng").val());
                       var area = parseFloat($("#CircleArea").val());
                       var tr_select = $("#wk_select_table tr").not(".noSelect");
                       initMap({"lat": lat,"lng":lng }, "mapSubmit","site");
                       var num = 1;
                       $("#tableTime_body").html("");
                       for (var i = 0; i < tr_select.length; i++) {
                            var dx = wk_list[tr_select[i].dataset["index"]];
                            var tr  = "";
                                tr += "<tr>";
                                tr += "<td class='text-center'>";
                                tr += num;
                                tr += "</td>";
                                tr += "<td class='text-left'>";
                                tr += dx["wk_name"]
                                tr += "</td>";
                                tr += "<td class='text-right'>";
                                tr += "<span class='date-working "+getDayActive(dx["wk_mon_start"])+" '>จ.</span> ";
                                tr += "<span class='date-working "+getDayActive(dx["wk_tue_start"])+" '>อ.</span> ";
                                tr += "<span class='date-working "+getDayActive(dx["wk_wed_start"])+" '>พ.</span> ";
                                tr += "<span class='date-working tw "+getDayActive(dx["wk_thu_start"])+" '>พฤ.</span> ";
                                tr += "<span class='date-working "+getDayActive(dx["wk_fri_start"])+" '>ศ.</span> ";
                                tr += "<span class='date-working "+getDayActive(dx["wk_sat_start"])+" '>ส.</span> ";
                                tr += "<span class='date-working tw "+getDayActive(dx["wk_sun_start"])+" '>อา.</span> ";
                                tr += "</td>";
                                tr += "</tr>";
                            num ++;
                            $("#tableTime_body").append(tr);
                       }
                   }
             }else{
                return;
              }
           }
           $(".site_tabs_content section").removeClass('content-current');
           $("#"+content).addClass('content-current');
           $(".tabs-progress .progress-bar ").css({"width":persent});
           $(".site_tabs li").removeClass('tab-current')
           $("#"+content+"_tab").addClass('tab-current');

        });

        $(".btn-site-submit").click(function(){
            var sitename = $("#sitecreateName").val();
            var lat = parseFloat($("#sitecreateLat").val());
            var lng = parseFloat($("#sitecreateLng").val());
            var area = parseFloat($("#CircleArea_site").val());
            var tr_select = $("#wk_select_table tr").not(".noSelect");
            var wk = [];
            for (var i = 0; i < tr_select.length; i++) {
                 wk.push(tr_select[i].dataset["id"])
            }
            var options = {"name" : sitename,"lat" : lat,"lng" : lng,"area" : area,"wk" : wk}
            $(".site_tabs_content").siteCreate(options);
        });

        $(".wk-new-create").click(function(){
            $("#list_wk").hide();
            $("#create_new_wk").show();
            var decode = jwt_decode(token);
            $("#company_id").val(decode["company_id"]);
            $("#createBy").val(window.atob(decode["employee_id"]));
            // console.log($("#company_id").val()+" "+$("#createBy").val());
        });
        $(".wk-search").click(function(){
            $("#list_wk").show();
            $("#create_new_wk").hide();
        });

        $(".apply-all").click(function(event) {
            var input = $("#create_form_wk input").not("#wk_name");
            var input_start = $("#monday_start").val();
            var input_end = $("#monday_end").val();
            for (var i = 2; i < input.length; i++) {
                 if (i%2 == 0) {
                    input[i].value = input_start;
                 }else{
                    input[i].value = input_end;
                 }
            }
        });

        $(".btn-create-wk").click(function(event) {
            var blank = $("#wk_name").validate_blank();
            var input = $("#create_form_wk input").not("#wk_name");
            var odd = [];
            var even = [];
            for (var i = 0; i < input.length; i++) {
                 if (i%2 == 0) {
                     even.push({"value":input[i].value,"key":i });
                 }else{
                     odd.push({"value":input[i].value,"key":i });
                 }
            }
            var number = 0;
            for (var a = 0; a < even.length; a++) {
                if (even[a]["value"] == "" && odd[a]["value"] != "") {
                    $("#"+input[even[a]["key"]].id).validate_blank();
                    $.toast({heading: "ผิดพลาด !",text: "กรุณากำหนดเวลาเข้างาน",position: 'top-right',loaderBg:'#ff6849',icon: 'error',hideAfter: 2000,stack: 6});
                    return;
                }
                if (even[a]["value"] != "" && odd[a]["value"] == "") {
                   $("#"+input[odd[a]["key"]].id).validate_blank();
                   $.toast({heading: "ผิดพลาด !",text: "กรุณากำหนดเวลาออกงาน",position: 'top-right',loaderBg:'#ff6849',icon: 'error',hideAfter: 2000,stack: 6});
                    return;
                }
                if (even[a]["value"] != "" && odd[a]["value"] != "") {
                    number ++;
                }
            }
            if (blank) {
                if (number < 1) {
                  $.toast({heading: "ผิดพลาด !",text: "คุณยังไม่ได้กำหนดวันทำการ",position: 'top-right',loaderBg:'#ff6849',icon: 'error',hideAfter: 2000,stack: 6});
              }else{
                  $("#create_form_wk").workingCreate();
              }
              }else{
                $.toast({heading: "ผิดพลาด !",text: "กรุณากรอกชื่อวันทำการ",position: 'top-right',loaderBg:'#ff6849',icon: 'error',hideAfter: 2000,stack: 6});
              }

        });

        $('.clockpicker').clockpicker({placement: 'right',align: 'right',autoclose: true,'default': 'now',clear:true}).keypress(function(event) {return false;});

        $(".btn-cancel-time").click(function(){
          var id = $(this).data("id");
          $("#"+id).val("");
        })

        $(".close-new").click(function(){
            $("#list_wk").hide();
            $("#create_new_wk").hide();
        });

        $(".close-wk-list").click(function(){
            $("#list_wk").hide();
            $("#create_new_wk").hide();
        })

    });

});
