$(".form-login").keypress(function(event) {
  if(event.keyCode == 13){
    $(".btn-login").trigger('click');
  }
});

$(".btn-login").click(function(){
  var form = $(".form-login").serialize();
  var username = $('#username').val();
  var password = $('#password').val();

  var url = getApi("login");
  $.ajax({
    type:'POST',
    url: url,
    data: {"username" : username, "password" : window.btoa(window.btoa(password))}
  })
  .done(function(data) {
    console.log(data);
    var token = data["token"];
    // console.log(token);
    var decode = jwt_decode(token);
    // console.log(decode["firstname"]+' '+decode["lastname"]);

    if (data["status"]) {
      // console.log("test1");
      if (data["password_changed"] == "Y") {
        window.localStorage.setItem('token', data["token"]);
        window.location = base_url('attendance/users');
      }else if(data["password_changed"] == "N"){
        window.localStorage.setItem('token', data["token"]);
        window.location = base_url('welcome/firstchangepass/'+ window.btoa(window.btoa(window.btoa(username))));
      }
    }else{
      if (data["error"] == "Incomplete password") {
        $('#txt_response').html("รหัสผ่านไม่ถูกต้อง");
      }else if(data["error"] == "user not found."){
        $('#txt_response').html("ไม่พบบัญชีผู้ใช้");
      }
    }
  });
});

$(".btn-resetpass").click(function(){
  var segments      = location.pathname.split('/');
  var email = segments[segments.length - 1];

  var urlresetpass = getApi("login/updateforgetpassword");
  var chkpass = checkduppass();

  console.log(window.atob(window.atob(window.atob(email)))+" "+window.btoa(window.btoa(password)));

  if (chkpass) {
    var password = $("#new_password").val();
    $("#labelchk").html("");
    $.ajax({
      type:'POST',
      url: urlresetpass,
      data: {"email" : window.atob(window.atob(window.atob(email))), "password" : window.btoa(window.btoa(password))}
    })
    .done(function(data) {
      if (data) {
        swal(
         'SUCCESS',
         'รีเซต Password เรียบร้อยแล้ว',
         'success'
        )
      }else{
        swal(
         'FAILED',
         'คำขอล้มเหลว โปรดลองใหม่อีกครั้ง',
         'error'
        )
      }
    });
  }else{
    $("#labelchk").html("<span style='color:red'>Password ไม่ตรงกัน โปรดตรวจสอบ</span>");
  }
});

$(".btn-create-new-pass").click(function(){
  var chkpass = checkduppass();
  var old_pass = $("#generate_password").val();
  var new_pass = $("#new_password").val();
  var segments      = location.pathname.split('/');
  var email = segments[segments.length - 1];
  var urlcreatepass = getApi("login/updatePasswordNewUser");

  console.log(window.atob(window.atob(window.atob(email)))+" "+window.btoa(window.btoa(new_pass)));

  if (chkpass) {
    $.ajax({
      type:'POST',
      url: urlcreatepass,
      data: {"email" : window.atob(window.atob(window.atob(email))), "password" : window.btoa(window.btoa(new_pass)), "random_password" : window.btoa(window.btoa(old_pass))}
    })
    .done(function(data) {
      if (data) {
        swal({
            title: 'SUCCESS',
            text: "เปลี่ยนพาสเวิดเรียบร้อยแล้ว",
            type: 'success',
            showCancelButton: false,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'ตกลง'
          }).then((result) => {
            if (result.value) {
              window.location = base_url('attendance/users');
            }
          })
      }else{
        $("#labelchk").html("<span style='color:red'>Password เดิมไม่ถูกต้อง โปรดตรวจสอบ</span>");
      }

    });
  }else{
      $("#labelchk").html("<span style='color:red'>Password ไม่ตรงกัน โปรดตรวจสอบ</span>");
  }
});



function checkduppass(){
  var n_pass = $("#new_password").val();
  var re_pass = $("#renew_password").val();

  if (n_pass == re_pass) {
    return true;
  }else{
    return false;
  }
}

$(".btn-forget-password").click(function(){
  var path = "";
  var email = $("#email").val();
  var urlsendmail = getApi("login/forgetpassword");

   var mailbody = "<div style=' font-family:Verdana, Geneva, sans-serif; padding:10px; box-shadow:0px 1px 3px #ccc; height:400px;' align='center'>";
   mailbody += "<p><h1 style='color:#f44336;'>Tik Track</h1></p>";
   mailbody += "<p><h4>Best service for time attendance solution.</h1></h4>";
   mailbody += "<hr />";
   mailbody += "<div align='left' style='padding:0px 20px;'>";
   mailbody += "<p><b>สวัสดีค่ะ</b></p>";
   mailbody += "<p>โปรดคลิกลิงค์ด้านล่าง เพื่อรีเซตพาสเวิดค่ะ</p>";
   mailbody += "<p align='center'>";
   mailbody += "<a href='"+base_url(path)+"/resetpass/"+window.btoa(window.btoa(window.btoa(email)))+"'";
   mailbody += "style='text-decoration:none;";
   mailbody += "background:#ef5350;";
   mailbody += "color:#fff;";
   mailbody += "border:none;";
   mailbody += "transition: .5s;";
   mailbody += "cursor:pointer;";
   mailbody += "border-bottom: solid 2px #ef5350;";
   mailbody += "padding:25px 15px;border-radius:10px;width:100%;'>";
   mailbody += "RESET PASSWORD";
   mailbody += "</a>";
   mailbody += "</p>";
   mailbody += "</div>";

   $.ajax({
     url: urlsendmail,
     type: 'POST',
     data: {"email":email,"body":mailbody,"name":"","title":"TIK TRACK"}
   })
   .done(function(data) {
     if (data) {
       swal(
        'SUCCESS',
        'คำขอเสร็จสิ้น โปรดเช็คที่ E-mail ของท่าน',
        'success'
       )
     }else{
       swal(
        'FAILED',
        'คำขอล้มเหลว โปรดลองใหม่อีกครั้ง',
        'error'
       )
     }

   });
});

function base_url(path){
  var config = 'tiktrack_beta';
    // var config = 'tiktrack_bt2';
    var origin = window.location.origin;
    return origin+"/"+config+"/"+path
}

function getApi(route){
  var origin = "";
  var segments      = location.href.split('/'),
  chkURL = segments[2];
  if (chkURL == "172.16.0.20") {
    origin = "http://172.16.0.21/w_tiktrack_api_beta/";
  }else{
    origin = "http://203.151.43.167/w_tiktrack_api_beta/";
  }
  return origin+route;
}
