var token = window.localStorage.getItem('token');
var branche_chip_emp = [];
var branche_approved_list = [];
var add_branche_chip_emp = [];
var branche_emp_list = [];

$(document).ready(function() {
   $("#branche_table").branche();

});

(function ($){
  $.fn.branche = function (){
     var table = $(this);
     var brancheBody = $("#branche_body");
     var segments      = location.pathname.split('/'),
     secondLastSegment = segments[segments.length - 1];
     var url = getApi("branches/getall");
     // console.log(window.atob(window.atob(window.atob(secondLastSegment))));
     $.ajax({
       url: url,
       type: 'GET',
       headers: {'Authorization': token},
       data: {"site_id":window.atob(window.atob(window.atob(secondLastSegment)))}
     })
     .done(function(data) {
       var no = 1;
       if (data["Has"] == false) {
         // brancheBody.html("");
         var tablesbranche = "<tr>";
         tablesbranche += "<td colspan='3' class='text-center'>ไม่พบข้อมูลที่ค้นหา";
         tablesbranche += "</td>";
         tablesbranche += "</tr>";
         brancheBody.append(tablesbranche);
       }else{
           brancheBody.html("");
           for (var i = 0; i < data["data"].length; i++) {
             var tablesbranche = "<tr>";
                tablesbranche += "<td  class='text-center'>";
                tablesbranche += no;
                tablesbranche += "</td>";
                tablesbranche += "<td>";
                tablesbranche += data["data"][i]["brancheName"];
                tablesbranche += "</td>";
                tablesbranche += "<td class='text-center'>";
                tablesbranche += "<div class='btn-group'>";
                tablesbranche += "<button class='btn btn-sm btn-default btn-outline waves-effect btn-branche-edit' onclick='branche_edit(this)' data-edit='"+ data["data"][i]["branche_id"] +"' title='แก้ไขสาขา'><span class='fa fa-edit'></span></button>";
                tablesbranche += "<button class='btn btn-sm btn-default btn-outline waves-effect'onclick='branche_del(this)' data-del='"+ data["data"][i]["branche_id"] +"'><span class='fa fa-trash' title='ลบสาขา'></span></button>";
                tablesbranche += "</div>";
                tablesbranche += "</td>";
                tablesbranche += "</tr>";
                brancheBody.append(tablesbranche);
                no ++;
           }
      }
      table.DataTable(dataTableTh);

     })
     .fail(function() {
       console.log("error");
     })
     .always(function() {
       console.log("complete");
     });

  }
})(jQuery);




$(".btn-create-branche").click(function(){
    var url = base_url("branches/ModalCreate");
    var segment = $("#working_table").data("segment");
    $.ajax({
      url: url,
      type: 'POST',
      data: {"segment":segment}
    })
    .done(function(data) {
      $(".modal-area").html(data);
      $("#brancheCreate").modal("toggle");
      $(".btn-step").click(function(){
         var content = $(this).data("phase");
         var persent = $(this).data("persent");
         var locate = [];
         if (typeof $(this).data("progress") !== "undefined") {
            if ( $(this).data("progress") == "success") {
                $(".tabs-progress .progress-bar ").removeClass("progress-bar-danger").addClass("progress-bar-"+$(this).data("progress"));
            }else{
                $(".tabs-progress .progress-bar ").removeClass("progress-bar-success").addClass("progress-bar-"+$(this).data("progress"));
            }
         }

         $("#list_wk").show();
         $("#create_new_wk").hide();

         if (content == "branche_location") {
             var input_sitename = $("#branchecreateName").validate_blank();
             if (input_sitename) {
                 var lat = parseFloat($("#branchecreateLat").val());
                 var lng = parseFloat($("#branchecreateLng").val());
                 if (lat == 0 && lng == 0) {
                    initMap({lat: lat,lng: lng},"branchMap","branche");
                    geolocate("site");
                 }else{
                    initMap({lat: lat,lng: lng},"branchMap","branche");
                 }
                 initAutocomplete();
                 $("#txt_area").html($("#CircleArea_branche").val());
                 $("#CircleArea_branche").change(function(){
                   $("#txt_area").html($("#CircleArea_branche").val());
                 });
             }else{
                 return;
             }
         }else if(content == "branche_working"){
                var input_sitename = $("#branchecreateName").validate_blank();
                if (input_sitename) {
                    $(".tabs-working-list").workingList({"insite":segment});
                }else{
                    return;
                }
         }else if (content == "branche_submit"){
             var input_sitename = $("#branchecreateName").validate_blank();
             if (input_sitename) {
                 if($("#wk_select_table tr").not(".noSelect").length < 1){
                     $.toast({heading: "ผิดพลาด !",text: "คุณยังไม่ได้กำหนดวันทำการ",position: 'top-right',loaderBg:'#ff6849',icon: 'error',hideAfter: 2000,stack: 6});
                     return;
                 }else{
                     var branche = $("#branchecreateName").val();
                     $("#nameSubmit").html("<h4>"+branche+"</h4>");
                     var lat = parseFloat($("#branchecreateLat").val());
                     var lng = parseFloat($("#branchecreateLng").val());
                     var area = parseFloat($("#CircleArea_branche").val());
                     var tr_select = $("#wk_select_table tr").not(".noSelect");
                     initMap({"lat": lat,"lng":lng }, "branchmapSubmit","branche");
                     var num = 1;
                     $("#tableTime_body").html("");
                     for (var i = 0; i < tr_select.length; i++) {
                          var dx = wk_list[tr_select[i].dataset["index"]];
                          var tr  = "";
                              tr += "<tr>";
                              tr += "<td class='text-center'>";
                              tr += num;
                              tr += "</td>";
                              tr += "<td class='text-left'>";
                              tr += dx["wk_name"]
                              tr += "</td>";
                              tr += "<td class='text-right'>";
                              tr += "<span class='date-working "+getDayActive(dx["wk_mon_start"])+" '>จ.</span> ";
                              tr += "<span class='date-working "+getDayActive(dx["wk_tue_start"])+" '>อ.</span> ";
                              tr += "<span class='date-working "+getDayActive(dx["wk_wed_start"])+" '>พ.</span> ";
                              tr += "<span class='date-working tw "+getDayActive(dx["wk_thu_start"])+" '>พฤ.</span> ";
                              tr += "<span class='date-working "+getDayActive(dx["wk_fri_start"])+" '>ศ.</span> ";
                              tr += "<span class='date-working "+getDayActive(dx["wk_sat_start"])+" '>ส.</span> ";
                              tr += "<span class='date-working tw "+getDayActive(dx["wk_sun_start"])+" '>อา.</span> ";
                              tr += "</td>";
                              tr += "</tr>";
                          num ++;
                          $("#tableTime_body").append(tr);
                     }
                 }
           }else{
              return;
            }
         }

         $('.clockpicker').clockpicker({placement: 'right',align: 'right',autoclose: true,'default': 'now',clear:true}).keypress(function(event) {return false;});
         $(".btn-cancel-time").click(function(){
           var id = $(this).data("id");
           $("#"+id).val("");
         })
         $(".wk-new-create").click(function(){
             $("#list_wk").hide();
             $("#create_new_wk").show();
         });
         $(".wk-search").click(function(){
             $("#list_wk").show();
             $("#create_new_wk").hide();
         });
         $(".close-new").click(function(){
             $("#list_wk").hide();
             $("#create_new_wk").hide();
         });
         $(".close-wk-list").click(function(){
             $("#list_wk").hide();
             $("#create_new_wk").hide();
         })


         $(".site_tabs_content section").removeClass('content-current');
         $("#"+content).addClass('content-current');
         $(".tabs-progress .progress-bar ").css({"width":persent});
         $(".site_tabs li").removeClass('tab-current')
         $("#"+content+"_tab").addClass('tab-current');

      });
      // step
      $(".apply-all").click(function(event) {
          var input = $("#create_form_wk input:text").not("#wk_name");
          var input_start = $("#monday_start").val();
          var input_end = $("#monday_end").val();
          for (var i = 2; i < input.length; i++) {
               if (i%2 == 0) {
                  input[i].value = input_start;
               }else{
                  input[i].value = input_end;
               }
          }
      });
      $(".btn-create-wk-branche").click(function(event) {
          var blank = $("#wk_name").validate_blank();
          var input = $("#create_form_wk input:text").not("#wk_name");
          var odd = [];
          var even = [];
          for (var i = 0; i < input.length; i++) {
               if (i%2 == 0) {
                   even.push({"value":input[i].value,"key":i });
               }else{
                   odd.push({"value":input[i].value,"key":i });
               }
          }
          var number = 0;
          for (var a = 0; a < even.length; a++) {
              if (even[a]["value"] == "" && odd[a]["value"] != "") {
                  $("#"+input[even[a]["key"]].id).validate_blank();
                  $.toast({heading: "ผิดพลาด !",text: "กรุณากำหนดเวลาเข้างาน",position: 'top-right',loaderBg:'#ff6849',icon: 'error',hideAfter: 2000,stack: 6});
                  return;
              }
              if (even[a]["value"] != "" && odd[a]["value"] == "") {
                 $("#"+input[odd[a]["key"]].id).validate_blank();
                 $.toast({heading: "ผิดพลาด !",text: "กรุณากำหนดเวลาออกงาน",position: 'top-right',loaderBg:'#ff6849',icon: 'error',hideAfter: 2000,stack: 6});
                  return;
              }
              if (even[a]["value"] != "" && odd[a]["value"] != "") {
                  number ++;
              }
          }
          if (blank) {
              if (number < 1) {
                $.toast({heading: "ผิดพลาด !",text: "คุณยังไม่ได้กำหนดวันทำการ",position: 'top-right',loaderBg:'#ff6849',icon: 'error',hideAfter: 2000,stack: 6});
            }else{
                $("#create_form_wk").workingCreate({"insite":segment});
            }
            }else{
              $.toast({heading: "ผิดพลาด !",text: "กรุณากรอกชื่อวันทำการ",position: 'top-right',loaderBg:'#ff6849',icon: 'error',hideAfter: 2000,stack: 6});
            }

      });
      $(".btn-branche-submit").click(function(){
          var BranchUrl = getApi("branches/add");
          var branchename = $("#branchecreateName").val();
          var lat = parseFloat($("#branchecreateLat").val());
          var lng = parseFloat($("#branchecreateLng").val());
          var area = parseFloat($("#CircleArea_branche").val());
          var tr_select = $("#wk_select_table tr").not(".noSelect");
          var wk = [];
          for (var i = 0; i < tr_select.length; i++) {
               wk.push(tr_select[i].dataset["id"])
          }
          var options = {"name" : branchename,"lat" : lat,"lng" : lng,"area" : area,"wk" : wk,"site_id":window.atob(window.atob(window.atob(segment)))};
          console.log(options);
          $.ajax({
            url: BranchUrl,
            type: 'POST',
            headers: {'Authorization': token},
            data: {"request" : options}
          })
          .done(function(data) {
            if (data) {
                $.toast({heading: "SUCCESS",text:"เพิ่มไซต์งานเรียบร้อยแล้ว",position: 'top-right',loaderBg:'#ff6849',icon: 'success',hideAfter: 3500,stack: 6});
                $("#brancheCreate").modal("toggle");
                $("#branche_table").dataTable().fnDestroy();
                $("#branche_table").branche();
            }else{
                $.toast({heading: "ERROR",text: "ไม่สามารถเพิ่มได้",position: 'top-right',loaderBg:'#ff6849',icon: 'error',hideAfter: 3500,stack: 6});
            }
          });

          // $(".site_tabs_content").siteCreate(options);
      });
    });
});

$(".tab-BrancheEditlocation").click(function(){
  var lat = parseFloat($("#sitecreateLat").val());
  var lng = parseFloat($("#sitecreateLng").val());
  initAutocomplete();
  initMap({"lat": lat,"lng":lng });
});

function branche_del(el){
  var del = $(el).data("del");
  var token = window.localStorage.getItem('token');
  var urlinactive = getApi("branches/inactive");
  swal({
      title: 'โปรดยืนยัน',
      text: "ต้องการยกเลิกสาขานี้หรือไม่",
      type: 'info',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'ตกลง',
      cancelButtonText: 'ยกเลิก'
    }).then((result) => {
      if (result.value) {
        $.ajax({
          url: urlinactive,
          type: 'POST',
          headers: {'Authorization': token},
          data: {"branche_id" : del}
        }).done(function(data){
          if (data) {
            $.toast({ heading: "SUCCESS",text: "ยกเลิกสาขาเรียบร้อยแล้ว",position: 'top-right',loaderBg:'#ff6849',icon: 'success',hideAfter: 3500,stack: 6});
            $("#branche_body").html("");
            $("#branche_table").dataTable().fnDestroy();
            $("#branche_table").branche();
          }else{
            $.toast({heading: "FAILED",text: "พบข้อผิดพลาดโปรดตรวจสอบ",position: 'top-right',loaderBg:'#ff6849',icon: 'error',hideAfter: 3500,stack: 6});
          }
        });
      }else{

      }
    })
}

function branche_edit(el){
  var edit = $(el).data("edit");
  var segments      = location.pathname.split('/'),
  secondLastSegment = segments[segments.length - 1];
  var urlEdit = base_url("branches/Edit/" + secondLastSegment + "/" + window.btoa(window.btoa(window.btoa(edit))));
  window.location = urlEdit;
}
