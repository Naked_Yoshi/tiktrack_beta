var day_value;
var month_value;
var year_value;
var site_value;
var search_type;
var branch_value;
var chk_search = 0;
var round_serch = 0;

var token = window.localStorage.getItem('token');



  function chkactive(el){
    var id = $(el).attr('id');
    console.log(id);
    console.log("เลือกจำนวน"+id.length+"คน");

    if($("#"+id).prop('checked')){

      if($("#"+id).prop('checked',true)){

          $("#"+id).addClass('active');



          $(".all-approve").removeClass('disabled');
          $(".all-approve").removeAttr('disabled');

          $(".all-not-approve").removeClass('disabled');
          $(".all-not-approve").removeAttr('disabled');
      }

    }else{
       $("#"+id).removeClass('active');

       $(".all-approve").addClass('disabled');
       $(".all-approve").attr('disabled');

       $(".all-not-approve").addClass('disabled');
        $(".all-not-approve").attr('disabled');
    }




  } //function


$(document).ready(function() {


  callpicker();
  $("#attendance_table").attendances(); //แสดงค่าแรก
  $(".select2").select2();
  $('.selectpicker').selectpicker();
  $("#site").sitegetall();





$(".click01").click(function(){ //คลิกปุ่มเข้างาน
    //$("#attendance_table").attendances1().unload();
    // $("#attendance_table").html("");

    $("#attendance_table").dataTable().fnDestroy();
    $("#attendance_table").attendances1();




}); //func click01

$(".click02").click(function(){ //คลิกปุ่มสาย
    //$("#attendance_table").attendances1().unload();

    // $("#attendance_table").html("");
    $("#attendance_table").attendances2();
      //$("#attendance_body").empty();

});

$(".click03").click(function(){ //คลิกปุ่มขาด
    //$("#attendance_table").attendances1().unload();
    // $("#attendance_table").html("");



    $("#attendance_table").dataTable().fnDestroy();
    $("#attendance_table").attendances3();



      //$("#attendance_body").empty();

});


  $("#site").change(function(event) {
      $("#branch").branchgetall();
  }); //function change #site





  $(".all-approve").click(function() {
    var url = getApi("attdata/approve");
      var myArray = [];
      var dataSelect = $(".att-item.active");


    for (var i = 0; i < dataSelect.length; i++) {
      var element = dataSelect[i];
      console.log("สิ่งที่เลือกออกมาที่จะอนุมัติ",dataSelect[i]);
      myArray.push(element.id);
    }


    console.log("คลิกฟังก์ชั่น all-approve ได้ และที่select ออกมาคือ", dataSelect);
    console.log("ค่าอาเรย์ที่จะส่งไป",myArray);
    console.log("จำนวน Array ที่จะส่งไป (นับจากค่าตัวแปร myArray)",myArray.length);

    swal({
          title:"คุณแน่ใจ ?",
          text: "ที่ต้องการอนุมัติ จำนวน "+myArray.length+" คน",
          type: "warning",
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: "ใช่, อนุมัติเลย!",
          cancelButtonText: "ไม่, ยกเลิก!",
          closeOnConfirm: false,
          closeOnCancel: true
      }).then((result) => {
        if (result.value) {
          console.log("result when click approve ");
          $.ajax({
            type:'POST',
            url: url,
            headers: {'Authorization': token},
            contentType : "application/x-www-form-urlencoded",
            data: {'arr_tid': myArray}

          }).done(function(data) {
            console.log("เมื่อdone แล้ว",data);

            if(data){

              if(chk_search==1){

                $("#attendance_table").dataTable().fnDestroy();
                after_approve_search();
                console.log("เช็คการค้นหา",chk_search);

              }

              if(chk_search==2){
                  //$("#attendance_table").dataTable().fnDestroy();

                  $("#attendance_table").dataTable().fnDestroy();
                  $("#attendance_table").attendances();

                  console.log("เช็คการค้นหา",chk_search);
              }

              if(chk_search==3){

                $("#attendance_table").dataTable().fnDestroy();
                $("#attendance_table").attendances1();

                  console.log("เช็คการค้นหา",chk_search);
              }

              if(chk_search==4){

                $("#attendance_table").dataTable().fnDestroy();
                $("#attendance_table").attendances2();

                  console.log("เช็คการค้นหา",chk_search);
              }

              if(chk_search==5){

                $("#attendance_table").dataTable().fnDestroy();
                $("#attendance_table").attendances3();

                  console.log("เช็คการค้นหา",chk_search);
              }


              /* ปุ่ม อนุมัติ และ ไม่อนุมัติ */

               // เพิ่มคลาส disabled ใน  Class=" all-approve disabled"
               $(".all-approve").addClass('disabled');
               // เพิ่มAttribute disabled ใน tag button โดยอ้างอิงจากคลาสที่ชื่อว่าall-approve  <button Class="all-approve" disabled>
               $(".all-approve").attr('disabled');

               // เพิ่มคลาส disabled ใน  Class=" all-not-approve disabled"
               $(".all-not-approve").addClass('disabled');
               // เพิ่มAttribute disabled ใน tag button โดยอ้างอิงจากคลาสที่ชื่อว่าall-not-approve  <button Class="all-not-approve" disabled>
               $(".all-not-approve").attr('disabled');



            } //if data


          }); //done func data
          // //end connect to api
        } // if result value
      }) //then result


  }); // func approve btn



  $(".all-not-approve").click(function() {
    var url = getApi("attdata/reject");
      var myArray = [];
      var dataSelect = $(".att-item.active");


    for (var i = 0; i < dataSelect.length; i++) {
      var element = dataSelect[i];
      console.log("สิ่งที่เลือกออกมาที่จะไม่อนุมัติ",dataSelect[i]);
      myArray.push(element.id);
    }

    console.log("คลิกฟังก์ชั่น all-not-approve ได้ และที่select ออกมาคือ", dataSelect);
    console.log("ค่าอาเรย์ที่จะส่งไป",myArray);
    console.log("จำนวน Array ที่จะส่งไป (นับจากค่าตัวแปร myArray)",myArray.length);

    swal({
          title:"คุณแน่ใจ ?",
          text: "ที่ต้องการไม่อนุมัติ จำนวน "+myArray.length+" คน",
          type: "warning",
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: "ใช่, ไม่อนุมัติ!",
          cancelButtonText: "ไม่, ยกเลิก!",
          closeOnConfirm: false,
          closeOnCancel: true
      }).then((result) => {
        if (result.value) {
          console.log("result when click not approve ");
          $.ajax({
            type:'POST',
            url: url,
            headers: {'Authorization': token},
            contentType : "application/x-www-form-urlencoded",
            data: {'arr_tid': myArray}

          }).done(function(data) {
            // console.log("เมื่อdone แล้ว",data);

            if(data){

              if(chk_search==1){

                $("#attendance_table").dataTable().fnDestroy();
                after_approve_search();
                console.log("เช็คการค้นหา",chk_search);

              }

              if(chk_search==2){
                  //$("#attendance_table").dataTable().fnDestroy();

                  $("#attendance_table").dataTable().fnDestroy();
                  $("#attendance_table").attendances();

                  console.log("เช็คการค้นหา",chk_search);
              }

              if(chk_search==3){

                $("#attendance_table").dataTable().fnDestroy();
                $("#attendance_table").attendances1();

                  console.log("เช็คการค้นหา",chk_search);
              }

              if(chk_search==4){

                $("#attendance_table").dataTable().fnDestroy();
                $("#attendance_table").attendances2();

                  console.log("เช็คการค้นหา",chk_search);
              }

              if(chk_search==5){

                $("#attendance_table").dataTable().fnDestroy();
                $("#attendance_table").attendances3();

                  console.log("เช็คการค้นหา",chk_search);
              }

              //$("#attendance_table").dataTable().fnDestroy();
              //$("#attendance_table").load();
              //console.log("โหลดใหม่ได้",data);

              /* ปุ่ม อนุมัติ และ ไม่อนุมัติ */

               // เพิ่มคลาส disabled ใน  Class=" all-approve disabled"
               $(".all-approve").addClass('disabled');
               // เพิ่มAttribute disabled ใน tag button โดยอ้างอิงจากคลาสที่ชื่อว่าall-approve  <button Class="all-approve" disabled>
               $(".all-approve").attr('disabled');

               // เพิ่มคลาส disabled ใน  Class=" all-not-approve disabled"
               $(".all-not-approve").addClass('disabled');
               // เพิ่มAttribute disabled ใน tag button โดยอ้างอิงจากคลาสที่ชื่อว่าall-not-approve  <button Class="all-not-approve" disabled>
               $(".all-not-approve").attr('disabled');

            }// if data


          }); //done(function(data) {
          // //end connect to api
        } // if result value
      }) //then result


  }); // func not approve btn



  $("#search_type").change(function(event){
    day_value = "";
    month_value = "";
    year_value = "";
    site_value = "";
    branch_value = "";
  });




$(".btnsearchall").click(function(){

  var table = $(this);
  var attendanceBody = $("#attendance_body");
  search_type = $("#search_type").val();
  var datepicker = $("#datepicker").val();
  //var selectsearchmonth = $("#selectsearchmonth").val();
  //var searchyear = $("#searchyear").val();

  chk_search = 1;
  console.log("สถานะการเช็คคือ",chk_search);

  $("#attendance_table").dataTable().fnDestroy();
  var url = getApi("attdata/getcheckin");

      //เลือกวัน

      if (search_type == "day") {
        //day_value = $("#datepicker").val();
        var n = datepicker.split('/');
          day_value = n[2]+"-"+n[1]+"-"+n[0];
        // day_value = n[0]."-";
        // month_value = n[1]."-";
        // year_value = n[2]."-";

        console.log("ประเภทการค้นหา",search_type);
        console.log("day:",day_value);
        console.log("Month:",month_value);
        console.log("Year",year_value);
      }else if(search_type == "month"){
        //เลือกเดือน
        var search_type = $("#search_type").val();
        var selectsearchmonth = $("#selectsearchmonth").val();
        var selectsearchyear = $("#selectsearchyear").val();
        var month_value = selectsearchmonth;
        var year_value = selectsearchyear;
        console.log("ประเภทการค้นหา",search_type);
        console.log("เดือนที่เลือกคือ",month_value);
        console.log("ปีที่เลือกคือ",year_value);
      }else if (search_type == "year"){
        //เลือกปี
        var search_type = $("#search_type").val();
        var selectsearchyear = $("#selectsearchyear").val();
        var year_value = selectsearchyear;
        console.log("ประเภทการค้นหา",search_type);
        console.log("ปีที่เลือกคือ",year_value);
       }
       //else if(search_type){
      //   //เลือกไซด์
      //    var site = $("#site").val();
      //      console.log("ค่าไซด์ = ",site);
      // }


      //เลือกไซด์
      site_value = $("#site").val();
      if(site_value != 0){

      }else{
        site_value = "";
      }
      console.log("ค่าไซด์ = ",site_value);



      //เลือกสาขา
      branch_value = $("#branch").val();
      if (!!branch_value) {
        //branch_value = "";
      }else{
        //branch_value = 0;
        branch_value = "";
      }
      console.log("ค่าสาขา = ",branch_value);





      // var segments      = location.pathname.split('/'),
      // secondLastSegment = segments[segments.length - 1];
       $.ajax({
         url: url,
         type: 'GET',
         data : {
           "day": day_value,
           "month": month_value,
           "year":year_value,
           "site_id": site_value,
           "branche_id": branch_value
         },
         headers: {'Authorization': token}
       }).done(function(data) {
         var no = 1;
         if (data["Has"] == false) {
            attendanceBody.html("");
           console.log("ข้อมูลการค้นหา",data);

           var attendanceHead = $("#attendance_head");
           attendanceHead.html("");
           var tablehead = "<tr id='headcolumn'>"
           tablehead += "<th><div class='checkbox'><input class='select-all' id='select-all' type='checkbox'/><label></label></div></th>";
           tablehead += "<th></th>";
           tablehead += "<th>ชื่อ-นามสกุล</th>";
           tablehead += "<th>ช่วงเวลา</th>";
           tablehead += "<th>เวลาเข้า</th>";
           tablehead += "<th>เวลาออก</th>";
           tablehead += "<th>สาย</th>";
           tablehead += "<th>เวลาทำงาน</th>";
           tablehead += "<th>สถานะ</th>";
           tablehead += "<th>ช่องทางเข้า</th>";
           tablehead += "<th>ช่องทางออก</th>";
           tablehead += "<th>ตัวเลือก</th>";
           tablehead += "</tr>";
           attendanceHead.append(tablehead);

         }else{
            attendanceBody.html("");
           console.log("ข้อมูลการค้นหา",data);

           var attendanceHead = $("#attendance_head");
           attendanceHead.html("");
           var tablehead = "<tr id='headcolumn'>"
           tablehead += "<th><div class='checkbox'><input class='select-all' id='select-all' type='checkbox'/><label></label></div></th>";
           tablehead += "<th></th>";
           tablehead += "<th>ชื่อ-นามสกุล</th>";
           tablehead += "<th>ช่วงเวลา</th>";
           tablehead += "<th>เวลาเข้า</th>";
           tablehead += "<th>เวลาออก</th>";
           tablehead += "<th>สาย</th>";
           tablehead += "<th>เวลาทำงาน</th>";
           tablehead += "<th>สถานะ</th>";
           tablehead += "<th>ช่องทางเข้า</th>";
           tablehead += "<th>ช่องทางออก</th>";
           tablehead += "<th>ตัวเลือก</th>";
           tablehead += "</tr>";
           attendanceHead.append(tablehead);


           //$("#attendance_table").dataTable().fnDestroy();
           console.log(data);
          $count = 0;
           for (var i = 0; i < data["data"].length; i++) {
            // attendanceBody.html("");
             for (var i = 0; i < data["data"].length; i++) {
               $count ++;
               var tablesapprove = "<tr>";
               tablesapprove += "<td  class='text-center'>";
               tablesapprove += "<div class='checkbox'><input class='att-item' id='"+data["data"][i]["tat_id"]+"' type='checkbox' onclick='chkactive(this)'/><label></label></div>";
               //tablesapprove += $count+"   "+"tat_id คือ"+data["data"][i]["tat_id"];
               tablesapprove += "</td>";

               tablesapprove += "<td  class='text-center'>";
               //tablesapprove += "<div class='checkbox'><input class='att-item' id='"+data["data"][i]["tat_id"]+"' type='checkbox'/><label></label></div>";
               tablesapprove += $count;
               tablesapprove += "</td>";

               tablesapprove += "<td>";
               tablesapprove += data["data"][i]["fullname"];
               tablesapprove += "</td>";

               tablesapprove += "<td>";
                 tablesapprove += data["data"][i]["wk_name"];
                 tablesapprove += "</br>";
                 tablesapprove += data["data"][i]["wokingStr"];
               tablesapprove += "</td>";

               tablesapprove += "<td>";
                 var checkintime = data["data"][i]["checkin_time"];

                 if(!checkintime){
                    tablesapprove += "<center><b>--</b></center>";
                 }else{
                   tablesapprove += data["data"][i]["checkin_time"];
                 }
               tablesapprove += "</td>";

               tablesapprove += "<td>";
               var checkouttime = data["data"][i]["checkout_time"];
               if(!checkouttime){
                 tablesapprove += "<center><b>--</b></center>";
               }else{
                 tablesapprove += data["data"][i]["checkout_time"];
               }
               tablesapprove += "</td>";

               tablesapprove += "<td>";
               tablesapprove += data["data"][i]["lateTime"];
               tablesapprove += "</td>";

               tablesapprove += "<td>";
                var totalworktime = data["data"][i]["total_wk_time"];
                if(!totalworktime){
                  tablesapprove +="<center><b>--</b></center>";
                }else{
                  tablesapprove += data["data"][i]["total_wk_time"];
                }
               tablesapprove += "</td>";

               tablesapprove += "<td>";
                 var status = data["data"][i]["approveStatus"];
                 if(status =='1'){
                 tablesapprove += "<button class='btn btn-success btn-rounded disabled' data-position='top' data-delay='50' data-tooltip='อนุมัติ' disabled><span class='fa fa-check'></span> อนุมัติ<span class='approve_num'></span> </button> ";
                 }
                 if(status =='2'){
                 tablesapprove += "<button class='btn btn-danger btn-rounded disabled' data-position='top' data-delay='50' data-tooltip='ไม่อนุมัติ' disabled><span class='fa fa-remove'></span> ไม่อนุมัติ<span class='reject_num'></span></button>";
                 }
                 if(status =='3'){
                 tablesapprove += "<button class='btn btn-default btn-rounded disabled' data-position='top' data-delay='50' data-tooltip='ไม่ตอบสนอง' disabled> ไม่ตอบสนอง<span class='reject_num'></span></button>";
                 }
               tablesapprove += "</td>";

               tablesapprove += "<td>";
                var checkinrouteid = data["data"][i]["checkin_route_id"];
                  if(checkinrouteid =='1'){
                   //tablesapprove += data["data"][i]["checkin_route_id"];
                   tablesapprove += "<center><button class='btn btn-info btn-rounded disabled' data-position='top' data-delay'50' data-tooltip='คลิกลงเวลา' data-tooltip-id=''><span class='fa fa-hand-o-up'></span></button></center>";
                  }
                  if(checkinrouteid =='2'){
                    //tablesapprove += data["data"][i]["checkin_route_id"];
                    tablesapprove += "<center><button class='btn btn-info btn-rounded disabled' data-position='top' data-delay'50' data-tooltip='คลิกลงเวลา' data-tooltip-id=''><span class='fa fa-qrcode'></span></button></center>";
                  }
                  if(!checkinrouteid){
                    tablesapprove += "<center><b>--</b></center>";
                  }
               tablesapprove += "</td>";

               tablesapprove += "<td>";
                var checkoutrouteid = data["data"][i]["checkout_route_id"];
                if(checkoutrouteid =='1'){
                //tablesapprove += data["data"][i]["checkout_route_id"];
                tablesapprove += "<center><button class='btn btn-info btn-rounded disabled' data-position='top' data-delay'50' data-tooltip='คลิกลงเวลา' data-tooltip-id=''><span class='fa fa-hand-o-up'></span></button></center>";
                }
                if(checkoutrouteid =='2'){
                 //tablesapprove += data["data"][i]["checkout_route_id"];
                 tablesapprove += "<center><button class='btn btn-info btn-rounded disabled' data-position='top' data-delay'50' data-tooltip='คลิกลงเวลา' data-tooltip-id=''><span class='fa fa-qrcode'></span></button></center>";
                }
                if(!checkoutrouteid){
                 //tablesapprove += data["data"][i]["checkout_route_id"];
                 tablesapprove += "<center><b>--</b></center>";
                }
               tablesapprove += "</td>";

               tablesapprove += "<td class='text-center'>";
                 tablesapprove += "<div class='btn-group'>";
                 tablesapprove += "<button class='btn btn-sm btn-default btn-outline waves-effect showdetail' onclick='showdetail(this)' id='"+data["data"][i]["tat_id"]+"'><i class='fa fa-navicon'></i></button>";
                 tablesapprove += "</div>";
               tablesapprove += "</td>";
               tablesapprove += "</tr>";
              attendanceBody.append(tablesapprove);
              // approved_list.push(data[i]["employee_id"]);
              no ++;
                } //loop
              } //loop

            } //else data true

            $("#attendance_table").DataTable(dataTableTh);


            $("#checkin").html(data["checkin_rows"]);
            $("#late").html(data["late_rows"]);
            $("#absense").html(data["skip_rows"]);



            $("#select-all").click(function(event) {

              console.log("คลิกปุ่มcheck box ทั้งหมดได้");

              if($("#select-all").prop('checked')){
                $("input:checkbox").not(this).prop('checked',true);
                //$(".all-approve").addClass('active');
                $("input:checkbox").addClass('active');

                /* ปุ่ม อนุมัติ และ ไม่อนุมัติ */

                // ลบคลาส disabled ใน  Class=" all-approve disabled"
                $(".all-approve").removeClass('disabled');
                // ลบAttribute disabled ใน tag button โดยอ้างอิงจากคลาสที่ชื่อว่าall-approve  <button Class="all-approve" disabled>
                $(".all-approve").removeAttr('disabled');

                // ลบคลาส disabled ใน  Class=" all-not-approve disabled"
                $(".all-not-approve").removeClass('disabled');
                // ลบAttribute disabled ใน tag button โดยอ้างอิงจากคลาสที่ชื่อว่าall-not-approve  <button Class="all-not-approve" disabled>
                $(".all-not-approve").removeAttr('disabled');




                console.log("ติ๊กหมดแล้วนะ");
              }else {
                $("input:checkbox").not(this).prop('checked',false);
                console.log("ยังติ๊กไม่หมด");
                //$(".all-approve").removeClass('active');
                $("input:checkbox").removeClass('active');


                /* ปุ่ม อนุมัติ และ ไม่อนุมัติ */

                // เพิ่มคลาส disabled ใน  Class=" all-approve disabled"
                $(".all-approve").addClass('disabled');
                // เพิ่ม Attribute disabled ใน tag button โดยอ้างอิงจากคลาสที่ชื่อว่าall-approve  <button Class="all-approve" disabled>
                $(".all-approve").attr('disabled');
                // เพิ่มคลาส disabled ใน  Class=" all-not-approve disabled"
                $(".all-not-approve").addClass('disabled');
                // เพิ่ม Attribute disabled ใน tag button โดยอ้างอิงจากคลาสที่ชื่อว่าall-not-approve  <button Class="all-not-approve" disabled>
                $(".all-not-approve").attr('disabled');


              }

            }); //function #select-all


            // $(".att-item").click(function(event) {
            //   $("")
            //   console.log("คลิกปุ่มcheck box ย่อยได้");
            //
            //   if($(".att-item").prop('checked',true)){
            //     $(".att-item").addClass('active');
            //     console.log("เพิ่มคลาส active ใน checkbox ย่อยได้แล้ว");
            //   }
            //
            //
            //
            // });


       });//done

     }); //btnsearchall function

 }); //ready function




function after_approve_search(){
  console.log("ฟังก์ชั่น after_approve_search ขึ้นมาได้แล้ว");
  var table = $(this);
  var attendanceBody = $("#attendance_body");
  search_type = $("#search_type").val();
  var datepicker = $("#datepicker").val();
  //var selectsearchmonth = $("#selectsearchmonth").val();
  //var searchyear = $("#searchyear").val();

  chk_search = 1;
  console.log("สถานะการเช็คคือ",chk_search);

  $("#attendance_table").dataTable().fnDestroy();
  var url = getApi("attdata/getcheckin");

      //เลือกวัน

      if (search_type == "day") {
        //day_value = $("#datepicker").val();
        var n = datepicker.split('/');
          day_value = n[2]+"-"+n[1]+"-"+n[0];
        // day_value = n[0]."-";
        // month_value = n[1]."-";
        // year_value = n[2]."-";

        console.log("ประเภทการค้นหา",search_type);
        console.log("day:",day_value);
        console.log("Month:",month_value);
        console.log("Year",year_value);
      }else if(search_type == "month"){
        //เลือกเดือน
        var search_type = $("#search_type").val();
        var selectsearchmonth = $("#selectsearchmonth").val();
        var selectsearchyear = $("#selectsearchyear").val();
        var month_value = selectsearchmonth;
        var year_value = selectsearchyear;
        console.log("ประเภทการค้นหา",search_type);
        console.log("เดือนที่เลือกคือ",month_value);
        console.log("ปีที่เลือกคือ",year_value);
      }else if (search_type == "year"){
        //เลือกปี
        var search_type = $("#search_type").val();
        var selectsearchyear = $("#selectsearchyear").val();
        var year_value = selectsearchyear;
        console.log("ประเภทการค้นหา",search_type);
        console.log("ปีที่เลือกคือ",year_value);
       }
       //else if(search_type){
      //   //เลือกไซด์
      //    var site = $("#site").val();
      //      console.log("ค่าไซด์ = ",site);
      // }


      //เลือกไซด์
      site_value = $("#site").val();
      if(site_value != 0){

      }else{
        site_value = "";
      }
      console.log("ค่าไซด์ = ",site_value);



      //เลือกสาขา
      branch_value = $("#branch").val();
      if (!!branch_value) {
        //branch_value = "";
      }else{
        //branch_value = 0;
        branch_value = "";
      }
      console.log("ค่าสาขา = ",branch_value);





      // var segments      = location.pathname.split('/'),
      // secondLastSegment = segments[segments.length - 1];
       $.ajax({
         url: url,
         type: 'GET',
         data : {
           "day": day_value,
           "month": month_value,
           "year":year_value,
           "site_id": site_value,
           "branche_id": branch_value
         },
         headers: {'Authorization': token}
       }).done(function(data) {
         var no = 1;
         if (data["Has"] == false) {
            attendanceBody.html("");
           console.log("ข้อมูลการค้นหา",data);

           var attendanceHead = $("#attendance_head");
           attendanceHead.html("");
           var tablehead = "<tr id='headcolumn'>"
           tablehead += "<th><div class='checkbox'><input class='select-all' id='select-all' type='checkbox'/><label></label></div></th>";
           tablehead += "<th></th>";
           tablehead += "<th>ชื่อ-นามสกุล</th>";
           tablehead += "<th>ช่วงเวลา</th>";
           tablehead += "<th>เวลาเข้า</th>";
           tablehead += "<th>เวลาออก</th>";
           tablehead += "<th>สาย</th>";
           tablehead += "<th>เวลาทำงาน</th>";
           tablehead += "<th>สถานะ</th>";
           tablehead += "<th>ช่องทางเข้า</th>";
           tablehead += "<th>ช่องทางออก</th>";
           tablehead += "<th>ตัวเลือก</th>";
           tablehead += "</tr>";
           attendanceHead.append(tablehead);

         }else{
            attendanceBody.html("");
           console.log("ข้อมูลการค้นหา",data);

           var attendanceHead = $("#attendance_head");
           attendanceHead.html("");
           var tablehead = "<tr id='headcolumn'>"
           tablehead += "<th><div class='checkbox'><input class='select-all' id='select-all' type='checkbox'/><label></label></div></th>";
           tablehead += "<th></th>";
           tablehead += "<th>ชื่อ-นามสกุล</th>";
           tablehead += "<th>ช่วงเวลา</th>";
           tablehead += "<th>เวลาเข้า</th>";
           tablehead += "<th>เวลาออก</th>";
           tablehead += "<th>สาย</th>";
           tablehead += "<th>เวลาทำงาน</th>";
           tablehead += "<th>สถานะ</th>";
           tablehead += "<th>ช่องทางเข้า</th>";
           tablehead += "<th>ช่องทางออก</th>";
           tablehead += "<th>ตัวเลือก</th>";
           tablehead += "</tr>";
           attendanceHead.append(tablehead);


           //$("#attendance_table").dataTable().fnDestroy();
           console.log(data);
          $count = 0;
           for (var i = 0; i < data["data"].length; i++) {
            // attendanceBody.html("");
             for (var i = 0; i < data["data"].length; i++) {
               $count ++;
               var tablesapprove = "<tr>";
               tablesapprove += "<td  class='text-center'>";
               tablesapprove += "<div class='checkbox'><input class='att-item' id='"+data["data"][i]["tat_id"]+"' type='checkbox' onclick='chkactive(this)'/><label></label></div>";
               //tablesapprove += $count+"   "+"tat_id คือ"+data["data"][i]["tat_id"];
               tablesapprove += "</td>";

               tablesapprove += "<td  class='text-center'>";
               //tablesapprove += "<div class='checkbox'><input class='att-item' id='"+data["data"][i]["tat_id"]+"' type='checkbox'/><label></label></div>";
               tablesapprove += $count;
               tablesapprove += "</td>";

               tablesapprove += "<td>";
               tablesapprove += data["data"][i]["fullname"];
               tablesapprove += "</td>";

               tablesapprove += "<td>";
                 tablesapprove += data["data"][i]["wk_name"];
                 tablesapprove += "</br>";
                 tablesapprove += data["data"][i]["wokingStr"];
               tablesapprove += "</td>";

               tablesapprove += "<td>";
                 var checkintime = data["data"][i]["checkin_time"];

                 if(!checkintime){
                    tablesapprove += "<center><b>--</b></center>";
                 }else{
                   tablesapprove += data["data"][i]["checkin_time"];
                 }
               tablesapprove += "</td>";

               tablesapprove += "<td>";
               var checkouttime = data["data"][i]["checkout_time"];
               if(!checkouttime){
                 tablesapprove += "<center><b>--</b></center>";
               }else{
                 tablesapprove += data["data"][i]["checkout_time"];
               }
               tablesapprove += "</td>";

               tablesapprove += "<td>";
               tablesapprove += data["data"][i]["lateTime"];
               tablesapprove += "</td>";

               tablesapprove += "<td>";
                var totalworktime = data["data"][i]["total_wk_time"];
                if(!totalworktime){
                  tablesapprove +="<center><b>--</b></center>";
                }else{
                  tablesapprove += data["data"][i]["total_wk_time"];
                }
               tablesapprove += "</td>";

               tablesapprove += "<td>";
                 var status = data["data"][i]["approveStatus"];
                 if(status =='1'){
                 tablesapprove += "<button class='btn btn-success btn-rounded disabled' data-position='top' data-delay='50' data-tooltip='อนุมัติ' disabled><span class='fa fa-check'></span> อนุมัติ<span class='approve_num'></span> </button> ";
                 }
                 if(status =='2'){
                 tablesapprove += "<button class='btn btn-danger btn-rounded disabled' data-position='top' data-delay='50' data-tooltip='ไม่อนุมัติ' disabled><span class='fa fa-remove'></span> ไม่อนุมัติ<span class='reject_num'></span></button>";
                 }
                 if(status =='3'){
                 tablesapprove += "<button class='btn btn-default btn-rounded disabled' data-position='top' data-delay='50' data-tooltip='ไม่ตอบสนอง' disabled> ไม่ตอบสนอง<span class='reject_num'></span></button>";
                 }
               tablesapprove += "</td>";

               tablesapprove += "<td>";
                var checkinrouteid = data["data"][i]["checkin_route_id"];
                  if(checkinrouteid =='1'){
                   //tablesapprove += data["data"][i]["checkin_route_id"];
                   tablesapprove += "<center><button class='btn btn-info btn-rounded disabled' data-position='top' data-delay'50' data-tooltip='คลิกลงเวลา' data-tooltip-id=''><span class='fa fa-hand-o-up'></span></button></center>";
                  }
                  if(checkinrouteid =='2'){
                    //tablesapprove += data["data"][i]["checkin_route_id"];
                    tablesapprove += "<center><button class='btn btn-info btn-rounded disabled' data-position='top' data-delay'50' data-tooltip='คลิกลงเวลา' data-tooltip-id=''><span class='fa fa-qrcode'></span></button></center>";
                  }
                  if(!checkinrouteid){
                    tablesapprove += "<center><b>--</b></center>";
                  }
               tablesapprove += "</td>";

               tablesapprove += "<td>";
                var checkoutrouteid = data["data"][i]["checkout_route_id"];
                if(checkoutrouteid =='1'){
                //tablesapprove += data["data"][i]["checkout_route_id"];
                tablesapprove += "<center><button class='btn btn-info btn-rounded disabled' data-position='top' data-delay'50' data-tooltip='คลิกลงเวลา' data-tooltip-id=''><span class='fa fa-hand-o-up'></span></button></center>";
                }
                if(checkoutrouteid =='2'){
                 //tablesapprove += data["data"][i]["checkout_route_id"];
                 tablesapprove += "<center><button class='btn btn-info btn-rounded disabled' data-position='top' data-delay'50' data-tooltip='คลิกลงเวลา' data-tooltip-id=''><span class='fa fa-qrcode'></span></button></center>";
                }
                if(!checkoutrouteid){
                 //tablesapprove += data["data"][i]["checkout_route_id"];
                 tablesapprove += "<center><b>--</b></center>";
                }
               tablesapprove += "</td>";

               tablesapprove += "<td class='text-center'>";
                 tablesapprove += "<div class='btn-group'>";
                 tablesapprove += "<button class='btn btn-sm btn-default btn-outline waves-effect btn-approve-delete'  onclick='showdetail(this)' id='"+data["data"][i]["tat_id"]+"'><i class='fa fa-navicon'></i></button>";
                 tablesapprove += "</div>";
               tablesapprove += "</td>";
               tablesapprove += "</tr>";
              attendanceBody.append(tablesapprove);
              // approved_list.push(data[i]["employee_id"]);
              no ++;
                } //loop
              } //loop

            } //else data true

            $("#attendance_table").DataTable(dataTableTh);


            $("#checkin").html(data["checkin_rows"]);
            $("#late").html(data["late_rows"]);
            $("#absense").html(data["skip_rows"]);



            $("#select-all").click(function(event) {

              console.log("คลิกปุ่มcheck box ทั้งหมดได้");

              if($("#select-all").prop('checked')){
                $("input:checkbox").not(this).prop('checked',true);
                //$(".all-approve").addClass('active');
                $("input:checkbox").addClass('active');
                console.log("ติ๊กหมดแล้วนะ");


              /* ปุ่ม อนุมัติ และ ไม่อนุมัติ */

             // ลบคลาส disabled ใน  Class=" all-approve disabled"
             $(".all-approve").removeClass('disabled');
             // ลบAttribute disabled ใน tag button โดยอ้างอิงจากคลาสที่ชื่อว่าall-approve  <button Class="all-approve" disabled>
             $(".all-approve").removeAttr('disabled');

             // ลบคลาส disabled ใน  Class=" all-not-approve disabled"
             $(".all-not-approve").removeClass('disabled');
             // ลบAttribute disabled ใน tag button โดยอ้างอิงจากคลาสที่ชื่อว่าall-not-approve  <button Class="all-not-approve" disabled>
             $(".all-not-approve").removeAttr('disabled');




              }else {
                $("input:checkbox").not(this).prop('checked',false);
                //$(".all-approve").removeClass('active');
                $("input:checkbox").removeClass('active');


                 console.log("ยังติ๊กไม่หมด");




                 /* ปุ่ม อนุมัติ และ ไม่อนุมัติ */

                // เพิ่มคลาส disabled ใน  Class=" all-approve disabled"
                $(".all-approve").addClass('disabled');
                // เพิ่มAttribute disabled ใน tag button โดยอ้างอิงจากคลาสที่ชื่อว่าall-approve  <button Class="all-approve" disabled>
                $(".all-approve").attr('disabled');

                // เพิ่มคลาส disabled ใน  Class=" all-not-approve disabled"
                $(".all-not-approve").addClass('disabled');
                // เพิ่มAttribute disabled ใน tag button โดยอ้างอิงจากคลาสที่ชื่อว่าall-not-approve  <button Class="all-not-approve" disabled>
                $(".all-not-approve").attr('disabled');

              }

            }); //function #select-all


            // $(".att-item").click(function(event) {
            //   $("")
            //   console.log("คลิกปุ่มcheck box ย่อยได้");
            //
            //   if($(".att-item").prop('checked',true)){
            //     $(".att-item").addClass('active');
            //     console.log("เพิ่มคลาส active ใน checkbox ย่อยได้แล้ว");
            //   }
            //
            //
            //
            // });


       });//done



} // after_approve_search




function print(){
  $("#reportModal").click({
    // $("#reportModal").show();
  });
}



function callpicker(){
  $('.mydatepicker, #datepicker').datepicker({
      autoclose: true,
      todayHighlight: true,
      clearBtn:true,
      endDate:"0d",
      language: 'th'
  });
}



(function ($){
  $.fn.branchgetall = function (){
    var branch = $(this);
    var siteval = $("#site").val();
    var url = getApi("branches/getall");

        $.ajax({
          url: url,
          type: 'GET',
          headers: {'Authorization': token},
          data: {"site_id" : siteval}
        }).done(function(data) {
          console.log(data);
          var no = 1;


          if (data["Has"] == false) {
            branch.html("");
            var r = "";
            r += "<option value='0'>";
            r += "ไม่มีสาขา";
            r += "</option>";
            branch.append(r);

          }else{
            branch.html("");
            var r = "";
             // r += "<option value='0'>";
             // r += "เลือกสาขา";
             // r += "</option>";


          for (var i = 0; i < data["data"].length; i++) {
              var r = "";
              r += "<option value='"+ data["data"][i]["branche_id"] +"'>";
              r += data["data"][i]["brancheName"];
              r += "</option>";

           }//loop
            branch.append(r);


          } //else
        });//done



  }
})(jQuery);




  function showdetail(el){

      var url = base_url('attendance/showdetailuser');

       $.ajax({
         url: url,
         type: 'POST',
       }).done(function(data) {

       $(".modal-area").html(data);
       $("#DetailModal").modal('toggle');



            var urldetail = getApi("attdata/getBy");
            var id = $(el).attr('id');


            console.log("Test and id =",id);
             $.ajax({
               url: urldetail,
               type: 'GET',
               data : {
                 'id' : id
               },
               headers: {'Authorization': token}
             }).done(function(data) {

               // console.log("ส่งค่า id ที่จะแสดงได้แล้ว ค่าเป็น =",data);

               if(data["Has"] == false){

               }else{

                 //id
                 var tat_id = data["data"][0]["tat_id"];

                 //ชื่อ-นามสกุล
                 var label_name = $("#label_name");
                 label_name.html("");
                 var fullname = data["data"][0]["fullname"];
                 label_name.append(fullname);

                 //ไซต์
                 var label_site = $("#label_site");
                 label_site.html("");
                 var siteName = data["data"][0]["siteName"];
                 label_site.append(siteName);

                 //สาขา
                 var label_branch = $("#label_branch");
                 label_branch.html("");
                 var brancheName = data["data"][0]["brancheName"];
                 label_branch.append(brancheName);

                 //วันที่
                 var label_check_date = $("#label_check_date");
                 label_check_date.html("");
                 var check_date = data["data"][0]["check_date"];
                 label_check_date.append(check_date);

                 //เวลาเช็คอิน
                 var label_checkin_time = $("#label_checkin_time");
                 label_checkin_time.html("");
                 var checkin_time = data["data"][0]["checkin_time"];
                 label_checkin_time.append(checkin_time);

                 //เวลาเช็คเอาท์
                 var label_checkout_time = $("#label_checkout_time");
                 label_checkout_time.html("");
                 var checkout_time = data["data"][0]["checkout_time"];
                 label_checkout_time.append(checkout_time);

                 //สาย
                 var label_late_time = $("#label_late_time");
                 label_late_time.html("");
                 var lateTime = data["data"][0]["lateTime"];
                 label_late_time.append(lateTime);

                 //ช่องทางการเข้า
                 var label_checkin_route_id = $("#label_checkin_route_id");
                 label_checkin_route_id.html("");
                 var checkin_route_id = data["data"][0]["checkin_route_id"];
                 label_checkin_route_id.append(checkin_route_id);

                 //ช่องทางการออก
                 var label_checkout_route_id = $("#label_checkout_route_id");
                 label_checkout_route_id.html("");
                 var checkout_route_id = data["data"][0]["checkout_route_id"];
                 label_checkout_route_id.append(checkout_route_id);

                 //การอนุมัติ
                 var label_approveStatus = $("#label_approveStatus");
                 label_approveStatus.html("");
                 var approveStatus = data["data"][0]["approveStatus"];
                 label_approveStatus.append(approveStatus);

                 var in_lat = data["data"][0]["in_lat_device"];
                 var in_long = data["data"][0]["in_long_device"];

                 var out_lat = data["data"][0]["out_lat_device"];
                 var out_long = data["data"][0]["out_long_device"];

                 initMap("map_in",in_lat,in_long);

                 initMap("map_out",out_lat,out_long);

                 // $(".map-in").click(function(){
                 //   console.log("testtt");
                 //         $("#map_in").show();
                 //         $("#map_out").hide();
                 //         initMap("map_in",in_lat,in_long);
                 //     });
                 //
                 //     $(".map-out").click(function(){
                 //       console.log(out_lat+" "+out_long);
                 //         $("#map_out").show();
                 //         $("#map_in").hide();
                 //         initMap("map_out",out_lat,out_long);
                 //     });


               //site.append(r);
               // console.log(tat_id,fullname,siteName,brancheName,check_date,checkin_time,checkout_time,lateTime,checkin_route_id,checkout_route_id,approveStatus,"in_lat",in_lat,"in_long",in_long,"out_lat",out_lat,"out_long",out_long);


               }


             }); //done function data


     }); //done function data (Open Modal Dialog)


  } //End of function showdetail



(function ($){
  $.fn.sitegetall = function (){
    var site = $(this);

    var url = getApi("sites/getall");
        $.ajax({
          url: url,
          type: 'GET',
          headers: {'Authorization': token}
        }).done(function(data) {
          console.log(data);
          var no = 1;

          if (data["Has"] == false) {
          // log("data (site)=", data);

          }else{
            // log("data (site)=", data);
               var r = "<option value='0'>";
               r += "เลือกไซต์";
               r += "</option>";
              for (var i = 0; i < data["data"].length; i++) {
                r += "<option value='"+ data["data"][i]["site_id"] +"'>";
                r += data["data"][i]["siteName"];
                r += "</option>";

             }//loop
            site.append(r);

          } //else
        });//done



    }
  })(jQuery);



          $("#searchtype select").change(function(event) {
                      var type = $(this).val();
                      $(".type_ofsearch").hide();
                      $("#"+type+"_type").show();
          });



          (function ($){
            $.fn.attendances = function (){
            // console.log("โหลดฟังก์ชั่น Default ที่จะแสดงตารางได้");
            var table = $(this);
            var attendanceBody = $("#attendance_body");
            var type = $("#search_type").val();
            var type2 = $("#datepicker").val();

            chk_search = 2;
            // console.log("สถานะการเช็คคือ",chk_search);

             var urlcheckin = getApi("attdata/getcheckin");
            // var segments      = location.pathname.split('/'),
            // secondLastSegment = segments[segments.length - 1];
             $.ajax({
               url: urlcheckin,
               type: 'GET',
               data : {
                 "day": day_value,
                 "month": month_value,
                 "year":year_value,
                 "site_id": site_value,
                 "branche_id": branch_value
               },
               headers: {'Authorization': token}
             }).done(function(data) {
               console.log(data);
               var no = 1;

               if (data["Has"] == false) {
                 // console.log("ข้อมูลของตารางDefault",data);
                 attendanceBody.html("");
                 var attendanceHead = $("#attendance_head");
                 attendanceHead.html("");
                 var tablehead = "<tr id='headcolumn'>"
                 tablehead += "<th><div class='checkbox'><input class='select-all' id='select-all' type='checkbox'/><label></label></div></th>";
                 tablehead += "<th></th>";
                 tablehead += "<th>ชื่อ-นามสกุล</th>";
                 tablehead += "<th>ช่วงเวลา</th>";
                 tablehead += "<th>เวลาเข้า</th>";
                 tablehead += "<th>เวลาออก</th>";
                 tablehead += "<th>สาย</th>";
                 tablehead += "<th>เวลาทำงาน</th>";
                 tablehead += "<th>สถานะ</th>";
                 tablehead += "<th>ช่องทางเข้า</th>";
                 tablehead += "<th>ช่องทางออก</th>";
                 tablehead += "<th>ตัวเลือก</th>";
                 tablehead += "</tr>";
                 attendanceHead.append(tablehead);
               }else{
                 // console.log("ข้อมูลของตารางDefault",data);
                 // $("#checkin").html(data["checkin_rows"])
                 var attendanceHead = $("#attendance_head");
                 attendanceHead.html("");
                 var tablehead = "<tr id='headcolumn'>"
                 tablehead += "<th><div class='checkbox'><input class='select-all' id='select-all' type='checkbox'/><label></label></div></th>";
                 tablehead += "<th></th>";
                 tablehead += "<th>ชื่อ-นามสกุล</th>";
                 tablehead += "<th>ช่วงเวลา</th>";
                 tablehead += "<th>เวลาเข้า</th>";
                 tablehead += "<th>เวลาออก</th>";
                 tablehead += "<th>สาย</th>";
                 tablehead += "<th>เวลาทำงาน</th>";
                 tablehead += "<th>สถานะ</th>";
                 tablehead += "<th>ช่องทางเข้า</th>";
                 tablehead += "<th>ช่องทางออก</th>";
                 tablehead += "<th>ตัวเลือก</th>";
                 tablehead += "</tr>";
                 attendanceHead.append(tablehead);




                $count = 0;
                  attendanceBody.html("");
                   for (var i = 0; i < data["data"].length; i++) {
                     $count ++;
                   var tablesapprove = "<tr>";

                     tablesapprove += "<td  class='text-center'>";
                     //tablesapprove += "<div class='checkbox'><input class='att-item' id='"+data["data"][i]["tat_id"]+"' type='checkbox'/><label></label></div>";
                     tablesapprove += "<div class='checkbox'><input class='att-item' id='"+data["data"][i]["tat_id"]+"' type='checkbox' onclick='chkactive(this)'/><label></label></div>";
                     //tablesapprove += $count+"   "+"tat_id คือ"+data["data"][i]["tat_id"];
                     tablesapprove += "</td>";

                     tablesapprove += "<td  class='text-center'>";
                      tablesapprove += $count;
                     tablesapprove += "</td>";

                     tablesapprove += "<td>";
                     tablesapprove += data["data"][i]["fullname"];
                     tablesapprove += "</td>";

                     tablesapprove += "<td>";
                       tablesapprove += data["data"][i]["wk_name"];
                       tablesapprove += "</br>";
                       tablesapprove += data["data"][i]["wokingStr"];
                     tablesapprove += "</td>";

                     tablesapprove += "<td>";
                       var checkintime = data["data"][i]["checkin_time"];

                       if(!checkintime){
                          tablesapprove += "<center><b>--</b></center>";
                       }else{
                         tablesapprove += data["data"][i]["checkin_time"];
                       }
                     tablesapprove += "</td>";

                     tablesapprove += "<td>";
                     var checkouttime = data["data"][i]["checkout_time"];
                     if(!checkouttime){
                       tablesapprove += "<center><b>--</b></center>";
                     }else{
                       tablesapprove += data["data"][i]["checkout_time"];
                     }
                     tablesapprove += "</td>";

                     tablesapprove += "<td>";
                     tablesapprove += data["data"][i]["lateTime"];
                     tablesapprove += "</td>";

                     tablesapprove += "<td>";
                      var totalworktime = data["data"][i]["total_wk_time"];
                      if(!totalworktime){
                        tablesapprove +="<center><b>--</b></center>";
                      }else{
                        tablesapprove += data["data"][i]["total_wk_time"];
                      }
                     tablesapprove += "</td>";

                     tablesapprove += "<td>";
                       var status = data["data"][i]["approveStatus"];
                       if(status =='1'){
                       tablesapprove += "<button class='btn btn-success btn-rounded disabled' data-position='top' data-delay='50' data-tooltip='อนุมัติ' disabled><span class='fa fa-check'></span> อนุมัติ<span class='approve_num'></span> </button> ";
                       }
                       if(status =='2'){
                       tablesapprove += "<button class='btn btn-danger btn-rounded disabled' data-position='top' data-delay='50' data-tooltip='ไม่อนุมัติ' disabled><span class='fa fa-remove'></span> ไม่อนุมัติ<span class='reject_num'></span></button>";
                       }
                       if(status =='3'){
                       tablesapprove += "<button class='btn btn-default btn-rounded disabled' data-position='top' data-delay='50' data-tooltip='ไม่ตอบสนอง' disabled> ไม่ตอบสนอง<span class='reject_num'></span></button>";
                       }
                     tablesapprove += "</td>";

                     tablesapprove += "<td>";
                      var checkinrouteid = data["data"][i]["checkin_route_id"];
                        if(checkinrouteid =='1'){
                         //tablesapprove += data["data"][i]["checkin_route_id"];
                         tablesapprove += "<center><button class='btn btn-info btn-rounded disabled' data-position='top' data-delay'50' data-tooltip='คลิกลงเวลา' data-tooltip-id=''><span class='fa fa-hand-o-up'></span></button></center>";
                        }
                        if(checkinrouteid =='2'){
                          //tablesapprove += data["data"][i]["checkin_route_id"];
                          tablesapprove += "<center><button class='btn btn-info btn-rounded disabled' data-position='top' data-delay'50' data-tooltip='คลิกลงเวลา' data-tooltip-id=''><span class='fa fa-qrcode'></span></button></center>";
                        }
                        if(!checkinrouteid){
                          tablesapprove += "<center><b>--</b></center>";
                        }
                     tablesapprove += "</td>";

                     tablesapprove += "<td>";
                      var checkoutrouteid = data["data"][i]["checkout_route_id"];
                      if(checkoutrouteid =='1'){
                      //tablesapprove += data["data"][i]["checkout_route_id"];
                      tablesapprove += "<center><button class='btn btn-info btn-rounded disabled' data-position='top' data-delay'50' data-tooltip='คลิกลงเวลา' data-tooltip-id=''><span class='fa fa-hand-o-up'></span></button></center>";
                      }
                      if(checkoutrouteid =='2'){
                       //tablesapprove += data["data"][i]["checkout_route_id"];
                       tablesapprove += "<center><button class='btn btn-info btn-rounded disabled' data-position='top' data-delay'50' data-tooltip='คลิกลงเวลา' data-tooltip-id=''><span class='fa fa-qrcode'></span></button></center>";
                      }
                      if(!checkoutrouteid){
                       //tablesapprove += data["data"][i]["checkout_route_id"];
                       tablesapprove += "<center><b>--</b></center>";
                      }
                     tablesapprove += "</td>";

                     tablesapprove += "<td class='text-center'>";
                       tablesapprove += "<div class='btn-group'>";
                       tablesapprove += "<button class='btn btn-sm btn-default btn-outline waves-effect btn-approve-delete'  onclick='showdetail(this)' id='"+data["data"][i]["tat_id"]+"'><i class='fa fa-navicon'></i></button>";
                       tablesapprove += "</div>";
                     tablesapprove += "</td>";

                    tablesapprove += "</tr>";
                      // approved_list.push(data[i]["employee_id"]);
                      attendanceBody.append(tablesapprove);
                      no ++;
                    } //loop
                } //else
                table.dataTable(dataTableTh);


                 $("#select-all").click(function(event) {

                   console.log("คลิกปุ่มcheck box ทั้งหมดได้");

                   if($("#select-all").prop('checked')){
                     $("input:checkbox").not(this).prop('checked',true);
                     //$(".all-approve").addClass('active');
                     $("input:checkbox").addClass('active');
                     // console.log("ติ๊กหมดแล้วนะ");

                     /* ปุ่ม อนุมัติ และ ไม่อนุมัติ */

                    // ลบคลาส disabled ใน  Class=" all-approve disabled"
                    $(".all-approve").removeClass('disabled');
                    // ลบAttribute disabled ใน tag button โดยอ้างอิงจากคลาสที่ชื่อว่าall-approve  <button Class="all-approve" disabled>
                    $(".all-approve").removeAttr('disabled');

                    // ลบคลาส disabled ใน  Class=" all-not-approve disabled"
                    $(".all-not-approve").removeClass('disabled');
                    // ลบAttribute disabled ใน tag button โดยอ้างอิงจากคลาสที่ชื่อว่าall-not-approve  <button Class="all-not-approve" disabled>
                    $(".all-not-approve").removeAttr('disabled');




                   }else {
                     $("input:checkbox").not(this).prop('checked',false);
                     // console.log("ยังติ๊กไม่หมด");
                     //$(".all-approve").removeClass('active');
                     $("input:checkbox").removeClass('active');


                /* ปุ่ม อนุมัติ และ ไม่อนุมัติ */

                // เพิ่มคลาส disabled ใน  Class=" all-approve disabled"
                $(".all-approve").addClass('disabled');
                // เพิ่มAttribute disabled ใน tag button โดยอ้างอิงจากคลาสที่ชื่อว่าall-approve  <button Class="all-approve" disabled>
                $(".all-approve").attr('disabled');

                // เพิ่มคลาส disabled ใน  Class=" all-not-approve disabled"
                $(".all-not-approve").addClass('disabled');
                // เพิ่มAttribute disabled ใน tag button โดยอ้างอิงจากคลาสที่ชื่อว่าall-not-approve  <button Class="all-not-approve" disabled>
                $(".all-not-approve").attr('disabled');


              }// else

                 }); //function #select-all
             }); //done

             var checkin = $("#checkin");
             var urlcheckin = getApi("attdata/getcheckin");
             $.ajax({
               url: urlcheckin,
               type: 'GET',
               data : {
                 day: 'day_value',
                 month: 'month_value',
                 year:'year_value',
                 site_id: 'site_value',
                 branche_id: 'brahch_value'
               },
               headers: {'Authorization': token}
             }).done(function(data) {
               console.log("จำนวนคนเข้างาน ",data);
               var checkinval = data["checkin_rows"];
               if(data["Has"] == false || !data["checkin_rows"]){
                  checkin.append("0");
               }else{
                 checkin.append(checkinval);
               }
             }); //done

             var late = $("#late");
             var urllate = getApi("attdata/getlate");
             $.ajax({
               url: urllate,
               type: 'GET',
               data : {
                 day: 'day_value',
                 month: 'month_value',
                 year:'year_value',
                 site_id: 'site_value',
                 branche_id: 'branch_value'
               },
               headers: {'Authorization': token}
             }).done(function(data) {
               console.log("จำนวนคนสาย",data);
               var lateval = data["late_rows"];
               if(data["Has"] == false || !data["late_rows"]){
                  late.append("0");
               }else{
                 late.append(lateval);
               }
             }); //done

             var absense = $("#absense");
             var urlabsense = getApi("attdata/getabsense");
             $.ajax({
               url: urlabsense,
               type: 'GET',
               data : {
                 day: 'day_value',
                 month: 'month_value',
                 year:'year_value',
                 site_id: 'site_value',
                 branche_id: 'branch_value'
               },
               headers: {'Authorization': token}
             }).done(function(data) {
               console.log("จำนวนคนขาดงาน",data);
               var absenseval = data["skip_rows"];
               if(data["has"] == false || !data["skip_rows"]){
                  absense.append("0");
               }else{
                 absense.append(absenseval);
               }
             }); //done


          } // attendances
        })(jQuery); //function


          (function ($){

            $.fn.attendances1 = function (){
            console.log("คลิกเข้างานได้");
            //$("#attendance_body").empty();
            var table = $(this);
            var attendanceBody = $("#attendance_body");
            var url = getApi("attdata/getcheckin");
            // var segments      = location.pathname.split('/'),
            // secondLastSegment = segments[segments.length - 1];
          chk_search = 3;

            if(month_value === undefined){
              // console.log("เข้าหรือเปล่า", month_value);
              month_value="";
            }

            if(year_value === undefined){
              // console.log("เข้าหรือเปล่า", year_value);
              year_value="";
            }

            // console.log("logตรงนีิ",day_value,month_value,year_value,site_value,branch_value);



             $.ajax({
               url: url,
               type: 'GET',
               data : {
                 "day": day_value,
                 "month": month_value,
                 "year":year_value,
                 "site_id": site_value,
                 "branche_id": branch_value
               },
               headers: {'Authorization': token}
             }).done(function(data) {
               var no = 1;

               //var checkinval = data["checkin_rows"];
               //checkin.append(checkinval);



               if (data["Has"] == false) {
                 console.log("ตารางเข้างาน",data);
                 attendanceBody.html("");
                 var attendanceHead = $("#attendance_head");
                 attendanceHead.html("");
                 var tablehead = "<tr id='headcolumn'>"
                 tablehead += "<th><div class='checkbox'><input class='select-all' id='select-all' type='checkbox'/><label></label></div></th>";
                 tablehead += "<th></th>";
                 tablehead += "<th>ชื่อ-นามสกุล</th>";
                 tablehead += "<th>ช่วงเวลา</th>";
                 tablehead += "<th>เวลาเข้า</th>";
                 tablehead += "<th>เวลาออก</th>";
                 tablehead += "<th>สาย</th>";
                 tablehead += "<th>เวลาทำงาน</th>";
                 tablehead += "<th>สถานะ</th>";
                 tablehead += "<th>ช่องทางเข้า</th>";
                 tablehead += "<th>ช่องทางออก</th>";
                 tablehead += "<th>ตัวเลือก</th>";
                 tablehead += "</tr>";
                 attendanceHead.append(tablehead);
               }else{
                 attendanceBody.html("");
                 // console.log("ตารางเข้างาน",data);

                 var attendanceHead = $("#attendance_head");
                 attendanceHead.html("");
                 var tablehead = "<tr id='headcolumn'>"
                 tablehead += "<th><div class='checkbox'><input class='select-all' id='select-all' type='checkbox'/><label></label></div></th>";
                 tablehead += "<th></th>";
                 tablehead += "<th>ชื่อ-นามสกุล</th>";
                 tablehead += "<th>ช่วงเวลา</th>";
                 tablehead += "<th>เวลาเข้า</th>";
                 tablehead += "<th>เวลาออก</th>";
                 tablehead += "<th>สาย</th>";
                 tablehead += "<th>เวลาทำงาน</th>";
                 tablehead += "<th>สถานะ</th>";
                 tablehead += "<th>ช่องทางเข้า</th>";
                 tablehead += "<th>ช่องทางออก</th>";
                 tablehead += "<th>ตัวเลือก</td>";
                 tablehead += "</tr>";
                 attendanceHead.append(tablehead);




                 $count = 0;
                 for (var i = 0; i < data["data"].length; i++) {
                  attendanceBody.html("");
                   for (var i = 0; i < data["data"].length; i++) {
                     $count ++;

                   var tablesapprove = "<tr>";

                   tablesapprove += "<td  class='text-center'>";
                   //tablesapprove += "<div class='checkbox'><input class='att-item' id='"+data["data"][i]["tat_id"]+"' type='checkbox'/><label></label></div>";
                   tablesapprove += "<div class='checkbox'><input class='att-item' id='"+data["data"][i]["tat_id"]+"' type='checkbox' onclick='chkactive(this)'/><label></label></div>";
                   //tablesapprove += $count+"   "+"tat_id คือ"+data["data"][i]["tat_id"];
                   tablesapprove += "</td>";


                   tablesapprove += "<td  class='text-center'>";
                    tablesapprove += $count;
                   tablesapprove += "</td>";

                   tablesapprove += "<td>";
                   tablesapprove += data["data"][i]["fullname"];
                   tablesapprove += "</td>";

                   tablesapprove += "<td>";
                     tablesapprove += data["data"][i]["wk_name"];
                     tablesapprove += "</br>";
                     tablesapprove += data["data"][i]["wokingStr"];
                   tablesapprove += "</td>";

                   tablesapprove += "<td>";
                     var checkintime = data["data"][i]["checkin_time"];

                     if(!checkintime){
                        tablesapprove += "<center><b>--</b></center>";
                     }else{
                       tablesapprove += data["data"][i]["checkin_time"];
                     }
                   tablesapprove += "</td>";

                   tablesapprove += "<td>";
                   var checkouttime = data["data"][i]["checkout_time"];
                   if(!checkouttime){
                     tablesapprove += "<center><b>--</b></center>";
                   }else{
                     tablesapprove += data["data"][i]["checkout_time"];
                   }
                   tablesapprove += "</td>";

                   tablesapprove += "<td>";
                   tablesapprove += data["data"][i]["lateTime"];
                   tablesapprove += "</td>";

                   tablesapprove += "<td>";
                    var totalworktime = data["data"][i]["total_wk_time"];
                    if(!totalworktime){
                      tablesapprove +="<center><b>--</b></center>";
                    }else{
                      tablesapprove += data["data"][i]["total_wk_time"];
                    }
                   tablesapprove += "</td>";

                   tablesapprove += "<td>";
                     var status = data["data"][i]["approveStatus"];
                     if(status =='1'){
                     tablesapprove += "<button class='btn btn-success btn-rounded disabled' data-position='top' data-delay='50' data-tooltip='อนุมัติ' disabled><span class='fa fa-check'></span> อนุมัติ<span class='approve_num'></span> </button> ";
                     }
                     if(status =='2'){
                     tablesapprove += "<button class='btn btn-danger btn-rounded disabled' data-position='top' data-delay='50' data-tooltip='ไม่อนุมัติ' disabled><span class='fa fa-remove'></span> ไม่อนุมัติ<span class='reject_num'></span></button>";
                     }
                     if(status =='3'){
                     tablesapprove += "<button class='btn btn-default btn-rounded disabled' data-position='top' data-delay='50' data-tooltip='ไม่ตอบสนอง' disabled> ไม่ตอบสนอง<span class='reject_num'></span></button>";
                     }
                   tablesapprove += "</td>";

                   tablesapprove += "<td>";
                    var checkinrouteid = data["data"][i]["checkin_route_id"];
                      if(checkinrouteid =='1'){
                       //tablesapprove += data["data"][i]["checkin_route_id"];
                       tablesapprove += "<center><button class='btn btn-info btn-rounded disabled' data-position='top' data-delay'50' data-tooltip='คลิกลงเวลา' data-tooltip-id=''><span class='fa fa-hand-o-up'></span></button></center>";
                      }
                      if(checkinrouteid =='2'){
                        //tablesapprove += data["data"][i]["checkin_route_id"];
                        tablesapprove += "<center><button class='btn btn-info btn-rounded disabled' data-position='top' data-delay'50' data-tooltip='คลิกลงเวลา' data-tooltip-id=''><span class='fa fa-qrcode'></span></button></center>";
                      }
                      if(!checkinrouteid){
                        tablesapprove += "<center><b>--</b></center>";
                      }
                   tablesapprove += "</td>";

                   tablesapprove += "<td>";
                    var checkoutrouteid = data["data"][i]["checkout_route_id"];
                    if(checkoutrouteid =='1'){
                    //tablesapprove += data["data"][i]["checkout_route_id"];
                    tablesapprove += "<center><button class='btn btn-info btn-rounded disabled' data-position='top' data-delay'50' data-tooltip='คลิกลงเวลา' data-tooltip-id=''><span class='fa fa-hand-o-up'></span></button></center>";
                    }
                    if(checkoutrouteid =='2'){
                     //tablesapprove += data["data"][i]["checkout_route_id"];
                     tablesapprove += "<center><button class='btn btn-info btn-rounded disabled' data-position='top' data-delay'50' data-tooltip='คลิกลงเวลา' data-tooltip-id=''><span class='fa fa-qrcode'></span></button></center>";
                    }
                    if(!checkoutrouteid){
                     //tablesapprove += data["data"][i]["checkout_route_id"];
                     tablesapprove += "<center><b>--</b></center>";
                    }
                   tablesapprove += "</td>";

                   tablesapprove += "<td class='text-center'>";
                     tablesapprove += "<div class='btn-group'>";
                     tablesapprove += "<button class='btn btn-sm btn-default btn-outline waves-effect btn-approve-delete'  onclick='showdetail(this)' id='"+data["data"][i]["tat_id"]+"'><i class='fa fa-navicon'></i></button>";
                     tablesapprove += "</div>";
                   tablesapprove += "</td>";
                    tablesapprove += "</tr>";
                    attendanceBody.append(tablesapprove);
                    // approved_list.push(data[i]["employee_id"]);
                    no ++;
                  } // for
                }// for
              }// else
              table.DataTable(dataTableTh);

              $("#select-all").click(function(event) {

                // console.log("คลิกปุ่มcheck box ทั้งหมดได้");

                if($("#select-all").prop('checked')){
                  $("input:checkbox").not(this).prop('checked',true);
                  //$(".all-approve").addClass('active');
                  $("input:checkbox").addClass('active');
                  // console.log("ติ๊กหมดแล้วนะ");


                  /* ปุ่ม อนุมัติ และ ไม่อนุมัติ */

               // ลบคลาส disabled ใน  Class=" all-approve disabled"
               $(".all-approve").removeClass('disabled');
               // ลบAttribute disabled ใน tag button โดยอ้างอิงจากคลาสที่ชื่อว่าall-approve  <button Class="all-approve" disabled>
               $(".all-approve").removeAttr('disabled');

               // ลบคลาส disabled ใน  Class=" all-not-approve disabled"
               $(".all-not-approve").removeClass('disabled');
               // ลบAttribute disabled ใน tag button โดยอ้างอิงจากคลาสที่ชื่อว่าall-not-approve  <button Class="all-not-approve" disabled>
               $(".all-not-approve").removeAttr('disabled');


                }else {
                  $("input:checkbox").not(this).prop('checked',false);
                  console.log("ยังติ๊กไม่หมด");
                  //$(".all-approve").removeClass('active');
                  $("input:checkbox").removeClass('active');

                  /* ปุ่ม อนุมัติ และ ไม่อนุมัติ */

               // เพิ่มคลาส disabled ใน  Class=" all-approve disabled"
               $(".all-approve").addClass('disabled');
               // เพิ่มAttribute disabled ใน tag button โดยอ้างอิงจากคลาสที่ชื่อว่าall-approve  <button Class="all-approve" disabled>
               $(".all-approve").attr('disabled');

               // เพิ่มคลาส disabled ใน  Class=" all-not-approve disabled"
               $(".all-not-approve").addClass('disabled');
               // เพิ่มAttribute disabled ใน tag button โดยอ้างอิงจากคลาสที่ชื่อว่าall-not-approve  <button Class="all-not-approve" disabled>
               $(".all-not-approve").attr('disabled');

                }// else

              }); //function #select-all





         });// done




      }// attandances1
    })(jQuery); //function


          (function ($){
            $.fn.attendances2 = function (){
                //$("#attendance_body").empty();
            console.log("คลิกสายได้");
            var table = $(this);
            var attendanceBody = $("#attendance_body");
            var late = $("#late");
             var urllate = getApi("attdata/getlate");
            // var segments      = location.pathname.split('/'),
            // secondLastSegment = segments[segments.length - 1];

            chk_search = 4;
            // console.log("สถานะการเช็คคือ",chk_search);

            if(month_value === undefined){
              // console.log("เข้าหรือเปล่า", month_value);
              month_value="";
            }

            if(year_value === undefined){
              // console.log("เข้าหรือเปล่า", year_value);
              year_value="";
            }

            // console.log("logตรงนีิ",day_value,month_value,year_value,site_value,branch_value);

             $.ajax({
               url: urllate,
               type: 'GET',
               data : {
                "day": day_value,
                "month": month_value,
                "year":year_value,
                "site_id": site_value,
                "branche_id": branch_value
               },
               headers: {'Authorization': token}
             }).done(function(data) {
              $("#attendance_table").dataTable().fnDestroy();
               //var lateval = data["late_rows"];
               //late.append(lateval);

               var no = 1;
               if (data["Has"] == false) {
                 console.log("ตารางสาย",data);
                 attendanceBody.html("");
                 var attendanceHead = $("#attendance_head");
                 attendanceHead.html("");
                 var tablehead = "<tr id='headcolumn'>"
                 tablehead += "<th><div class='checkbox'><input class='select-all' id='select-all' type='checkbox'/><label></label></div></th>";
                 tablehead += "<th></th>";
                 tablehead += "<th>ชื่อ-นามสกุล</th>";
                 tablehead += "<th>ช่วงเวลา</th>";
                 tablehead += "<th>เวลาเข้า</th>";
                 tablehead += "<th>เวลาออก</th>";
                 tablehead += "<th>สาย</th>";
                 tablehead += "<th>เวลาทำงาน</th>";
                 tablehead += "<th>สถานะ</th>";
                 tablehead += "<th>ช่องทางเข้า</th>";
                 tablehead += "<th>ช่องทางออก</th>";
                 tablehead += "<th>ตัวเลือก</th>";
                 tablehead += "</tr>";
                 attendanceHead.append(tablehead);
               }else{
                attendanceBody.html("");
                 console.log("ตารางสาย",data);

                 var attendanceHead = $("#attendance_head");
                 attendanceHead.html("");
                 var tablehead = "<tr id='headcolumn'>"
                 tablehead += "<th><div class='checkbox'><input class='select-all' id='select-all' type='checkbox'/><label></label></div></th>";
                 tablehead += "<th></th>";
                 tablehead += "<th>ชื่อ-นามสกุล</th>";
                 tablehead += "<th>ช่วงเวลา</th>";
                 tablehead += "<th>เวลาเข้า</th>";
                 tablehead += "<th>เวลาออก</th>";
                 tablehead += "<th>สาย</th>";
                 tablehead += "<th>เวลาทำงาน</th>";
                 tablehead += "<th>สถานะ</th>";
                 tablehead += "<th>ช่องทางเข้า</th>";
                 tablehead += "<th>ช่องทางออก</th>";
                 tablehead += "<th>ตัวเลือก</th>";
                 tablehead += "</tr>";
                 attendanceHead.append(tablehead);


                 $count = 0;
                 for (var i = 0; i < data["data"].length; i++) {
                  attendanceBody.html("");
                   for (var i = 0; i < data["data"].length; i++) {
                     $count ++;

                   var tablesapprove = "<tr>";

                   tablesapprove += "<td  class='text-center'>";
                   //tablesapprove += "<div class='checkbox'><input class='att-item' id='"+data["data"][i]["tat_id"]+"' type='checkbox'/><label></label></div>";
                   tablesapprove += "<div class='checkbox'><input class='att-item' id='"+data["data"][i]["tat_id"]+"' type='checkbox' onclick='chkactive(this)'/><label></label></div>";
                   //tablesapprove += $count+"   "+"tat_id คือ"+data["data"][i]["tat_id"];
                   tablesapprove += "</td>";

                   tablesapprove += "<td  class='text-center'>";
                    tablesapprove += $count;
                   tablesapprove += "</td>";

                   tablesapprove += "<td>";
                   tablesapprove += data["data"][i]["fullname"];
                   tablesapprove += "</td>";

                   tablesapprove += "<td>";
                     tablesapprove += data["data"][i]["wk_name"];
                     tablesapprove += "</br>";
                     tablesapprove += data["data"][i]["wokingStr"];
                   tablesapprove += "</td>";

                   tablesapprove += "<td>";
                     var checkintime = data["data"][i]["checkin_time"];

                     if(!checkintime){
                        tablesapprove += "<center><b>--</b></center>";
                     }else{
                       tablesapprove += data["data"][i]["checkin_time"];
                     }
                   tablesapprove += "</td>";

                   tablesapprove += "<td>";
                   var checkouttime = data["data"][i]["checkout_time"];
                   if(!checkouttime){
                     tablesapprove += "<center><b>--</b></center>";
                   }else{
                     tablesapprove += data["data"][i]["checkout_time"];
                   }
                   tablesapprove += "</td>";

                   tablesapprove += "<td>";
                   tablesapprove += data["data"][i]["lateTime"];
                   tablesapprove += "</td>";

                   tablesapprove += "<td>";
                    var totalworktime = data["data"][i]["total_wk_time"];
                    if(!totalworktime){
                      tablesapprove +="<center><b>--</b></center>";
                    }else{
                      tablesapprove += data["data"][i]["total_wk_time"];
                    }
                   tablesapprove += "</td>";

                   tablesapprove += "<td>";
                     var status = data["data"][i]["approveStatus"];
                     if(status =='1'){
                     tablesapprove += "<button class='btn btn-success btn-rounded disabled' data-position='top' data-delay='50' data-tooltip='อนุมัติ' disabled><span class='fa fa-check'></span> อนุมัติ<span class='approve_num'></span> </button> ";
                     }
                     if(status =='2'){
                     tablesapprove += "<button class='btn btn-danger btn-rounded disabled' data-position='top' data-delay='50' data-tooltip='ไม่อนุมัติ' disabled><span class='fa fa-remove'></span> ไม่อนุมัติ<span class='reject_num'></span></button>";
                     }
                     if(status =='3'){
                     tablesapprove += "<button class='btn btn-default btn-rounded disabled' data-position='top' data-delay='50' data-tooltip='ไม่ตอบสนอง' disabled> ไม่ตอบสนอง<span class='reject_num'></span></button>";
                     }
                   tablesapprove += "</td>";

                   tablesapprove += "<td>";
                    var checkinrouteid = data["data"][i]["checkin_route_id"];
                      if(checkinrouteid =='1'){
                       //tablesapprove += data["data"][i]["checkin_route_id"];
                       tablesapprove += "<center><button class='btn btn-info btn-rounded disabled' data-position='top' data-delay'50' data-tooltip='คลิกลงเวลา' data-tooltip-id=''><span class='fa fa-hand-o-up'></span></button></center>";
                      }
                      if(checkinrouteid =='2'){
                        //tablesapprove += data["data"][i]["checkin_route_id"];
                        tablesapprove += "<center><button class='btn btn-info btn-rounded disabled' data-position='top' data-delay'50' data-tooltip='คลิกลงเวลา' data-tooltip-id=''><span class='fa fa-qrcode'></span></button></center>";
                      }
                      if(!checkinrouteid){
                        tablesapprove += "<center><b>--</b></center>";
                      }
                   tablesapprove += "</td>";

                   tablesapprove += "<td>";
                    var checkoutrouteid = data["data"][i]["checkout_route_id"];
                    if(checkoutrouteid =='1'){
                    //tablesapprove += data["data"][i]["checkout_route_id"];
                    tablesapprove += "<center><button class='btn btn-info btn-rounded disabled' data-position='top' data-delay'50' data-tooltip='คลิกลงเวลา' data-tooltip-id=''><span class='fa fa-hand-o-up'></span></button></center>";
                    }
                    if(checkoutrouteid =='2'){
                     //tablesapprove += data["data"][i]["checkout_route_id"];
                     tablesapprove += "<center><button class='btn btn-info btn-rounded disabled' data-position='top' data-delay'50' data-tooltip='คลิกลงเวลา' data-tooltip-id=''><span class='fa fa-qrcode'></span></button></center>";
                    }
                    if(!checkoutrouteid){
                     //tablesapprove += data["data"][i]["checkout_route_id"];
                     tablesapprove += "<center><b>--</b></center>";
                    }
                   tablesapprove += "</td>";

                   tablesapprove += "<td class='text-center'>";
                     tablesapprove += "<div class='btn-group'>";
                     tablesapprove += "<button class='btn btn-sm btn-default btn-outline waves-effect btn-approve-delete'  onclick='showdetail(this)' id='"+data["data"][i]["tat_id"]+"'><i class='fa fa-navicon'></i></button>";
                     tablesapprove += "</div>";
                   tablesapprove += "</td>";
                 tablesapprove += "</tr>";
                      attendanceBody.append(tablesapprove);
                      // approved_list.push(data[i]["employee_id"]);
                      no ++;
                    }//loop
                  }// loop
                } //else
                table.DataTable(dataTableTh);

                $("#select-all").click(function(event) {

                  console.log("คลิกปุ่มcheck box ทั้งหมดได้");

                  if($("#select-all").prop('checked')){
                    $("input:checkbox").not(this).prop('checked',true);
                    //$(".all-approve").addClass('active');
                    $("input:checkbox").addClass('active');
                    console.log("ติ๊กหมดแล้วนะ");



                    /* ปุ่ม อนุมัติ และ ไม่อนุมัติ */

                   // ลบคลาส disabled ใน  Class=" all-approve disabled"
                   $(".all-approve").removeClass('disabled');
                   // ลบAttribute disabled ใน tag button โดยอ้างอิงจากคลาสที่ชื่อว่าall-approve  <button Class="all-approve" disabled>
                   $(".all-approve").removeAttr('disabled');

                   // ลบคลาส disabled ใน  Class=" all-not-approve disabled"
                   $(".all-not-approve").removeClass('disabled');
                   // ลบAttribute disabled ใน tag button โดยอ้างอิงจากคลาสที่ชื่อว่าall-not-approve  <button Class="all-not-approve" disabled>
                   $(".all-not-approve").removeAttr('disabled');


                  }else {
                    $("input:checkbox").not(this).prop('checked',false);
                    console.log("ยังติ๊กไม่หมด");
                    //$(".all-approve").removeClass('active');
                    $("input:checkbox").removeClass('active');


                    /* ปุ่ม อนุมัติ และ ไม่อนุมัติ */

                   // เพิ่มคลาส disabled ใน  Class=" all-approve disabled"
                   $(".all-approve").addClass('disabled');
                   // เพิ่มAttribute disabled ใน tag button โดยอ้างอิงจากคลาสที่ชื่อว่าall-approve  <button Class="all-approve" disabled>
                   $(".all-approve").attr('disabled');

                   // เพิ่มคลาส disabled ใน  Class=" all-not-approve disabled"
                   $(".all-not-approve").addClass('disabled');
                   // เพิ่มAttribute disabled ใน tag button โดยอ้างอิงจากคลาสที่ชื่อว่าall-not-approve  <button Class="all-not-approve" disabled>
                   $(".all-not-approve").attr('disabled');


                  }// else

                }); //function #select-all

           }); //done



        } //attendances2
      })(jQuery); //function

      (function ($){

        $.fn.attendances3 = function (){
            console.log("คลิกขาดงานได้");

            //$("#attendance_body").empty();
            // console.log("staa");
        var table = $(this);
        var attendanceBody = $("#attendance_body");
        //var attendanceHead = $("#attendance_head");
        var url = getApi("attdata/getabsense");
        // var segments      = location.pathname.split('/'),
        // secondLastSegment = segments[segments.length - 1];





        chk_search = 5;
        console.log("สถานะการเช็คคือ",chk_search);

        if(month_value === undefined){
          console.log("เข้าหรือเปล่า", month_value);
          month_value="";
        }

        if(year_value === undefined){
          console.log("เข้าหรือเปล่า", year_value);
          year_value="";
        }

        console.log("logตรงนีิ",day_value,month_value,year_value,site_value,branch_value);


         $.ajax({
           url: url,
           type: 'GET',
           data : {
             // day: 'day_value',
             // month: 'month_value',
             // year:'year_value',
             // site_id: 'site_value',
             // branche_id: 'branch_value'

             "day": day_value,
             "month": month_value,
             "year":year_value,
             "site_id": site_value,
             "branche_id": branch_value
           },
           headers: {'Authorization': token}
         }).done(function(data) {
           var no = 1;
           if (data["Has"] == false) {
             console.log("ตารางขาดงาน",data);

             var attendanceHead = $("#attendance_head");
             attendanceHead.html("");
             var tablehead = "<tr id='headcolumn'>"
             tablehead += "<th><div class='checkbox'><input class='select-all' id='select-all' type='checkbox'/><label></label></div></th>";
             tablehead += "<th></th>";
             tablehead += "<th>ชื่อ-นามสกุล</th>";
             tablehead += "<th>ช่วงเวลา</th>";
             tablehead += "<th>เบอร์โทรศัพท์</th>";
             tablehead += "<th>ไซด์</th>";
             tablehead += "<th>สาขา</th>";
             // tablehead += "<td></td>";
             // tablehead += "<td></td>";
             // tablehead += "<td></td>";
             // tablehead += "<td></td>";
             // tablehead += "<td></td>";
             tablehead += "</tr>";
             attendanceHead.append(tablehead);



             attendanceBody.html("");

           }else{
             console.log("ตารางขาดงาน",data);

            var attendanceHead = $("#attendance_head");
            attendanceHead.html("");
            var tablehead = "<tr id='headcolumn'>"
            //tablehead += "<th><div class='checkbox'><input class='select-all' id='select-all' type='checkbox'/><label></label></div></th>";
            tablehead += "<th></th>";
            tablehead += "<th>ชื่อ-นามสกุล</th>";
            tablehead += "<th>ช่วงเวลา</th>";
            tablehead += "<th>เบอร์โทรศัพท์</th>";
            tablehead += "<th>ไซด์</th>";
            tablehead += "<th>สาขา</th>";
            // tablehead += "<td></td>";
            // tablehead += "<td></td>";
            // tablehead += "<td></td>";
            // tablehead += "<td></td>";
            // tablehead += "<td></td>";
            tablehead += "</tr>";
            attendanceHead.append(tablehead);


             $count = 0;
             for (var i = 0; i < data["data"].length; i++) {
              attendanceBody.html("");
               for (var i = 0; i < data["data"].length; i++) {
                 $count ++;

                 var tablesapprove = "<tr>";

                 // tablesapprove += "<td  class='text-center'>";
                 // tablesapprove += "<div class='checkbox'><input class='att-item' id='"+data["data"][i]["tat_id"]+"' type='checkbox' onclick='chkactive(this)'/><label></label></div>";
                 // tablesapprove += "</td>";

                 tablesapprove += "<td  class='text-center'>";
                  tablesapprove += $count;
                 tablesapprove += "</td>";

                 tablesapprove += "<td>";
                 tablesapprove += data["data"][i]["fullname"];
                 tablesapprove += "</td>";

                 tablesapprove += "<td>";
                   tablesapprove += data["data"][i]["wk_name"];
                   tablesapprove += "</br>";
                   tablesapprove += data["data"][i]["wokingStr"];
                 tablesapprove += "</td>";

                 tablesapprove += "<td>";
                 tablesapprove += data["data"][i]["phoneNumber"];
                 tablesapprove += "</td>";

                 tablesapprove += "<td>";
                 tablesapprove += data["data"][i]["siteName"];
                 tablesapprove += "</td>";

                 tablesapprove += "<td>";
                 tablesapprove += data["data"][i]["brancheName"];
                 tablesapprove += "</td>";

                 // tablesapprove += "<td></td>";
                 // tablesapprove += "<td></td>";
                 // tablesapprove += "<td></td>";
                 // tablesapprove += "<td></td>";
                 // tablesapprove += "<td></td>";

                 tablesapprove += "</tr>";

                  attendanceBody.append(tablesapprove);
                    // approved_list.push(data[i]["employee_id"]);
                    no ++;
                  } //loop
                } //loop
              }//else
              table.DataTable(dataTableTh);



         });//done



      } //attendances3
    })(jQuery); //function



    function initMap(mapid,lat_in,long_in) {
    // Create a map object and specify the DOM element for display.
    lat_in = parseFloat(lat_in);
    long_in = parseFloat(long_in);
    var uluru = {lat:lat_in, lng:long_in};
    var map = new google.maps.Map(document.getElementById(mapid), {
      center: uluru,
      zoom: 15
    });
    var marker = new google.maps.Marker({
          position: uluru,
          map: map
        });
}
