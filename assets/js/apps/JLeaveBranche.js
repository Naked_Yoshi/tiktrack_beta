
var lv_row = {};


var i = 1;
var token = window.localStorage.getItem('token');
  $(document).ready(function() {
    $("#Lev_detail").getleave();
  });

  (function ($){
    $.fn.getleave = function (){
      var urlgetByLeave = getApi("vacation/getByLeaveBranche");
      var segments      = location.pathname.split('/'),
      site_id = segments[segments.length - 2];
      branche_id = segments[segments.length - 1];
      $.ajax({
        url: urlgetByLeave,
        type: 'GET',
        headers: {'Authorization': token},
        data: {'branche_id':window.atob(window.atob(window.atob(branche_id)))}
      }).done(function(data) {
        if (data["Has"] == false) {
          callformleave();
        }else{
          var lev_id = data["data"][0]["lev_id"];
          callformleave(lev_id);
        }

      });
    }
  })(jQuery);

  function callformleave(lev_id = ""){
    var lev = $("#selectLev");
    var urlgetlinklev_f = base_url("vacation/getlinklev");
    $.ajax({
      url: urlgetlinklev_f,
      type: 'GET'
    }).done(function(data) {
      $(".Lev_detail").html(data);

      var urlgetlinklev_head = getApi("vacation/getBy");
      $.ajax({
        url: urlgetlinklev_head,
        type: 'GET',
        headers: {'Authorization': token},
        data: {'va_id':lev_id}
      }).done(function(data) {

        var label;
        if (lev_id == 0 || lev_id===null) {
          label = "<p><span class='fa fa-info-circle'></span> กรุณาเลือกรายการวันหยุด</p>";
        }else{
          label = "<p><span class='text-left'>กลุ่มรายการวันลา : "+ data["data"][0]["lev_name"] +"</span></p>";

          var r_lev = "";
          for (var i = 0; i < data["data"].length; i++) {
            var txt_urgent="<span class='fa fa-square-o'></span>";
            var txt_delay="<span class='fa fa-square-o'></span>";
            var txt_deduct_money="<span class='fa fa-square-o'></span>";
            if (data["data"][i]["levd_is_urgent"] == 1) {
              txt_urgent = "<span class='fa fa-check-square'></span>";
            }
            if (data["data"][i]["levd_delay_request"] == 1) {
              txt_delay = "<span class='fa fa-check-square'></span>";
            }
            if (data["data"][i]["levd_is_deduct_money"] == 1) {
              txt_deduct_money = "<span class='fa fa-check-square'></span>";
            }
            r_lev += "<tr>";
            r_lev += "<td>";
            r_lev += data["data"][i]["leave_type_name"];
            r_lev += "</td>";
            r_lev += "<td>";
            r_lev += data["data"][i]["levd_request_date"]+" ชั่วโมง";
            r_lev += "</td>";
            r_lev += "<td>";
            r_lev += data["data"][i]["levd_master"]+" ชั่วโมง";
            r_lev += "</td>";
            r_lev += "<td align='center'>";
            r_lev += txt_deduct_money;
            r_lev += "</td>";
            r_lev += "<td align='center'>";
            r_lev += txt_urgent;
            r_lev += "</td>";
            r_lev += "<td align='center'>";
            r_lev += txt_delay;
            r_lev += "</td>";
            r_lev += "</tr>";
          }
          $("#link_lev_body").html("");
          $("#link_lev_body").append(r_lev);
        }
        $("#link_lev_head").append(label);

      });

      var urlapi = getApi("vacation/getAll");
      $.ajax({
        url: urlapi,
        type: 'GET',
        headers: {'Authorization': token}
      }).done(function(data){
        if (data["Has"] == false) {
          var r = "";
          r += "<option value='' disabled>";
          r += "คุณยังไม่ได้ตั้งค่าแบบการลา";
          r += "</option>";
          lev.append(r);
        }else{
          var r = "";
          r += "<option value='0'>";
          r += "โปรดเลือกรูปแบบการลา";
          r += "</option>";
          for (var i = 0; i < data["data"].length; i++) {
            r += "<option value='"+ data["data"][i]["lev_id"] +"'>";
            r += data["data"][i]["lev_name"];
            r += "</option>";
          }
            lev.append(r).select2();
        }
        $("#selectLev").val(lev_id);
        $("#selectLev").change(function(event) {
        $(this).getLevDetail($("#selectLev").val(),window.atob(window.atob(window.atob(site_id))));
        }).select2();
      });
      ////////////////////////////////////////

      $(".btn-add-lev-approve").click(function(){
        var row_approve = $(".lev-approve");
        $(".no-list-lev-approve").remove();
        var temp_i = i-1;
        if (temp_i == 0) {

        }else{
          $(".del-row"+temp_i).hide();
        }

        var id = 1;
        var r = "<div id='lev_row"+i+"' data-row='"+i+"' class='rw'>";
        r += "<div class='col-md-1'>";
        r += "<span class='label label-info'>Lv : " + i + "</span> "
        r += "</div>";
        r += "<div class='col-md-1'>";
        r += "<span class='label label-success' style='cursor:pointer' onclick='add_lev_lv(this);' data-lev_row='"+ i +"'> + </span> "
        r += "</div>";
        r += "<div class='col-md-9 tag-approve"+ i +"'>";
        r += "<div class='upper_org_"+ i +"'>อนุมัติสูงกว่าระดับตนเอง "+ i +" ระดับ</div>";
        // r += "<span class='label label-info'>" + "มนุษย์ ทดลอง" + " <a onclick='rm_approve_lv(this);' data-id=''><font color='black'><span class='fa fa-close'></font></span></a></span> "
        r += "</div>";
        r += "<div class='col-md-1' style='padding-top:3px;'>";
        r += "<a style='cursor:pointer' onclick='rm_lev_lv(this);'  class='del-row"+ i +"' data-del_row='"+ i +"'><span class='fa fa-minus-circle'></span></a>"
        r += "</div>";
        r += "<br><hr>";
        r += "</div>";
        row_approve.append(r);
        i++;
        });


        /////////////////////////////////////////////// link /////////////////////////////////
        $(".btn-link-lev").click(function(){

          var addLeavePermis = [];
          console.log(addLeavePermis);
          lv_row = {};
          var add_lev_id = $("#selectLev").val();
          var arow = $(".lev-approve > div").length; //row LEVEL
          var lv_s = 1;
          for (var x = 0; x < arow; x++) {
              var nrow = $(".tag-approve"+ lv_s +" > span").length;// ROW NODE
              if (nrow == 0) {
                // $.toast({heading: "FAILED",text: "คุณยังไม่เพิ่มผู้อนุมัติที่ Level "+lv_s,position: 'top-right',loaderBg:'#ff6849',icon: 'error',hideAfter: 3500,stack: 6});
                // return false;
                lv_row.levp_level = lv_s;
                lv_row.employee_id = "";
                // console.log(lv_row);
                addLeavePermis.push(lv_row);
                lv_row = {};
              }
              for (var j = 0; j < nrow; j++) {
                var tag = $(".tag-approve"+ lv_s +" > span")[j].id;
                var tempemp_id = tag.split("_");
                var emp_permis = tempemp_id[1];
                lv_row.levp_level = lv_s;
                lv_row.employee_id = emp_permis;
                // console.log(lv_row);
                addLeavePermis.push(lv_row);
                lv_row = {};
              }
              lv_s++;
          }

          var segments      = location.pathname.split('/'),
          branche_id = segments[segments.length - 1];
          console.log(branche_id);

          var req = {
            "lev_id" : add_lev_id,
            "site_id" : window.atob(window.atob(window.atob(site_id))),
            "branche_id" : window.atob(window.atob(window.atob(branche_id))),
            "level" : {
              "add" : addLeavePermis
            }
          };

          // console.log(req);

          var urlgetlinklev_head = getApi("vacation/addLeavePermissionBranche");
          $.ajax({
            url: urlgetlinklev_head,
            type: 'POST',
            headers: {'Authorization': token},
            data: {'request':req}
          }).done(function(data) {
            if (data) {
              swal(
                    'SUCCESS',
                    'เชื่อมต่อกลุ่มลากับพนักงานไซต์เรียบร้อยแล้ว',
                    'success'
                  )
            }else{
              swal(
                    'ERROR',
                    'พบข้อผิดพลาด กรุณาลองใหม่',
                    'error'
                  )
            }
          });

        });

        ///////////////////////////////// Edit Permission //////////////////////////////

        var urlgetLeavePositionApprove = getApi("vacation/getLeavePositionApprove");
        $.ajax({
          url: urlgetLeavePositionApprove,
          type: 'GET',
          headers: {'Authorization': token},
          data: {'site_id':window.atob(window.atob(window.atob(site_id))),'branche_id':window.atob(window.atob(window.atob(branche_id)))}
        }).done(function(data) {
          console.log(data);
          if (data["Has"] == false) {

          }else{
            var lv_max = parseInt(data["lv_max"]);
            console.log(lv_max);
            var row_approve = $(".lev-approve");
            $(".no-list-lev-approve").remove();

            for (var z = 0; z < lv_max; z++) {
              var temp_i = i-1;
              if (temp_i == 0) {

              }else{
                $(".del-row"+temp_i).hide();
              }

              var r = "<div id='lev_row"+i+"' data-row='"+i+"' class='rw'>";
              r += "<div class='col-md-1'>";
              r += "<span class='label label-info'>Lv : " + i + "</span> "
              r += "</div>";
              r += "<div class='col-md-1'>";
              r += "<span class='label label-success' style='cursor:pointer' onclick='add_lev_lv(this);' data-lev_row='"+ i +"'> + </span> "
              r += "</div>";
              r += "<div class='col-md-9 tag-approve"+ i +"'>";
              r += "<div class='upper_org_"+ i +"'>อนุมัติสูงกว่าระดับตนเอง "+ i +" ระดับ</div>";
              // r += "<span class='label label-info'>" + "มนุษย์ ทดลอง" + " <a onclick='rm_approve_lv(this);' data-id=''><font color='black'><span class='fa fa-close'></font></span></a></span> "
              r += "</div>";
              r += "<div class='col-md-1' style='padding-top:3px;'>";
              r += "<a style='cursor:pointer' onclick='rm_lev_lv(this);'  class='del-row"+ i +"' data-del_row='"+ i +"'><span class='fa fa-minus-circle'></span></a>"
              r += "</div>";
              r += "<br><hr>";
              r += "</div>";
              row_approve.append(r);
              i++;

            }
            if (data["data"].length > 0) {
              for (var z = 0; z < data["data"].length; z++) {
                $(".upper_org_"+data["data"][z]["lev_level"]).hide();
                if (data["data"][z]["employee_id"] === null) {
                  $(".upper_org_"+data["data"][z]["lev_level"]).show();
                }else{
                  var r_approve = "<span class='label label-info row-lv"+ data["data"][z]["lev_level"] +"' id='tag"+ data["data"][z]["lev_level"] +"_"+ data["data"][z]["employee_id"] +"' data-emp_id="+ data["data"][z]["employee_id"] +">" + data["data"][z]["fullname"] + " <a onclick='rm_approve_lv(this);' data-del_row='"+ data["data"][z]["lev_level"] +"' data-id='"+ data["data"][z]["employee_id"] +"'><font color='black'><span class='fa fa-close'></font></span></a></span> ";

                  $(".tag-approve"+data["data"][z]["lev_level"]).append(r_approve);
                }
              }
            }
          }
        });

      });
  }

  function add_lev_lv(el){
    var lv = $(el).data("lev_row");
    var urlapproveModal = base_url("vacation/approveModal");
    $.ajax({
      url: urlapproveModal,
      type: 'GET'
    }).done(function(data) {
      $(".modal-area").html(data);
      $("#permission").modal("toggle");

      $("#lv").html("Lv. "+lv);
      $("#btn_approve").addClass("btn-add-approve"+lv);

      var fullname = "";
      var emp_id = "";

      var url = getApi("Branches/getApprover");
      var segments      = location.pathname.split('/'),
      site_id = segments[segments.length - 1];
      $.ajax({
        url: url,
        type: 'GET',
        headers: {'Authorization': token},
        data: {"branche_id": window.atob(window.atob(window.atob(site_id))) }
      }).done(function(data) {
        // console.log("testttt");
        $('#list-approve').autoComplete({
            minChars: 0,
            source: function(term, suggest){
                term = term.toLowerCase();
                var choices = [];
                for (var i = 0; i < data.length; i++) {
                  choices.push([data[i]["approver_name"],data[i]["employee_id"]]);
                }
                var suggestions = [];
                for (i=0;i<choices.length;i++)
                    if (~(choices[i][0]+' '+choices[i][1]).toLowerCase().indexOf(term)) suggestions.push(choices[i]);
                suggest(suggestions);
            },
            renderItem: function (item, search){
                search = search.replace(/[-\/\\^$*+?.()|[\]{}]/g, '\\$&');
                var re = new RegExp("(" + search.split(' ').join('|') + ")", "gi");
                return '<div class="autocomplete-suggestion" data-langname="'+item[0]+'" data-lang="'+item[1]+'" data-val="'+search+'">'+item[0].replace(re, "<b>$1</b>")+'</div>';
            },
            onSelect: function(e, term, item){
                $('#txt_label').html("");
                // var id = window.atob(item.data('lang'))
                // console.log('Item "'+item.data('langname')+' ('+item.data('lang')+')" selected by '+(e.type == 'keydown' ? 'pressing enter or tab' : 'mouse click')+'.');
                $('#list-approve').val(item.data('langname'));
                fullname = item.data('langname');
                emp_id = item.data('lang');
            }
        });
      });

      $(".btn-add-approve"+lv).click(function(){


          // console.log($(".lev-approve > div"));
          var row = $(".tag-approve"+ lv +" > span").length;
          if (row == 0) {
            var r = "<span class='label label-info row-lv"+ lv +"' id='tag"+ lv +"_"+ emp_id +"' data-emp_id="+ emp_id +">" + fullname + " <a onclick='rm_approve_lv(this);' data-del_row='"+ lv +"' data-id='"+ emp_id +"'><font color='black'><span class='fa fa-close'></font></span></a></span> ";
            $(".upper_org_"+lv).hide();
            $(".tag-approve"+lv).append(r);
            $("#permission").modal("toggle");
          }else{
            var arr = [];
            for (var i = 0; i < row; i++) {
              var tag = $(".tag-approve"+ lv +" > span")[i].id;
              var tempemp_id = tag.split("_")
              arr.push(tempemp_id[1]);
            }
            emp_id = emp_id.toString();
              if(jQuery.inArray(emp_id, arr) !== -1){
                $.toast({heading: "FAILED",text: "ไม่สามารถเพิ่มผู้อนุมัติได้",position: 'top-right',loaderBg:'#ff6849',icon: 'error',hideAfter: 3500,stack: 6});
              }else{
                var r = "<span class='label label-info row-lv"+ lv +"' id='tag"+ lv +"_"+ emp_id +"' data-emp_id="+ emp_id +">" + fullname + " <a onclick='rm_approve_lv(this);' data-del_row='"+ lv +"' data-id='"+ emp_id +"'><font color='black'><span class='fa fa-close'></font></span></a></span> ";
                $(".upper_org_"+lv).hide();
                $(".tag-approve"+lv).append(r);
                $("#permission").modal("toggle");
              }
              // console.log($(".tag-approve"+ lv +" > span")[i].id);
          }
      });
    });
  }

  function rm_approve_lv(ev){
    var lv = ev.dataset["del_row"];
    var tempemp_id = ev.dataset["id"];
    var tag = 0;
    $("#tag"+lv+"_"+tempemp_id).remove();
    tag = $(".tag-approve"+lv+" > span").length;
    if (tag == 0) {
        $(".upper_org_"+lv).show();
    }
  }

  function rm_lev_lv(ev){
    var rm = ev.dataset["del_row"];
    $("#lev_row"+rm).remove();
    var row = $(".lev-approve > div").length;
    if (row < 1) {
      var r = "";
      r += "<span class='text-center no-list-lev-approve' data-row='0'>";
      r += "<h5>ไม่พบรายการ โปรดเพิ่มผู้อนุมัติ</h5>";
      r += "</span>";
      $(".lev-approve").append(r);
    }
    rm--;
    $(".del-row"+rm).show();
    i--;
  }
