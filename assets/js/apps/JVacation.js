var vacADD = [];
var vacEDIT = [];
var vacDEL = [];

var typeADD = [];
var typeEDIT = [];
var typeDEL = [];

var configADD = [];
var configEDIT = [];
var configDEL = [];

var tmpTabContent = 0;


var token = window.localStorage.getItem('token');
$(document).ready(function() {
  $("#vacation_body").vacations();
});

function cleararr(){
  vacADD = [];
  vacEDIT = [];
  vacDEL = [];
}

$(".btn-create-vcation_type").click(function(){
  typeDEL = [];
  var url = base_url('vacation/typeModal');
  $.ajax({
    url: url,
    type: "POST",
  })
  .done(function(data) {
    $(".modal-area").html(data);
    $("#VacationTypeModal").modal("toggle");

    var urlapi = getApi("vacation/getTypeAll");
    var id = 1;
    $.ajax({
      url: urlapi,
      type: 'GET',
      headers: {'Authorization': token}
    }).done(function(data){
      if (data["Has"] == false) {

      }else{
        $(".no-list").remove();
        for (var i = 0; i < data["data"].length; i++) {
          if (data["data"][i]["is_default"] == 1) {
            var r = "<tr class='defaultrow' id='type_row"+id+"' data-row='"+data["data"][i]["id"]+"'>";
          }else{
            var r = "<tr class='editrow' id='type_row"+id+"' data-row='"+data["data"][i]["id"]+"'>";
          }
          r += "<td>";
          if (data["data"][i]["is_default"] == 1) {
            r += data["data"][i]["leave_type_name"];
          }else{
            r += "<input type='text' class='form-control' id='txtleave_name_"+id+"' name='' value='" + data["data"][i]["leave_type_name"] + "' placeholder='ชื่อเงื่อนไข'>";
          }
          r += "</td>";
          r += "<td align='right'>";
          if (data["data"][i]["is_default"] == 1) {
            r += "ค่าเริ่มต้น";
          }else{
            r += "<button type='button' class='btn btn-danger btn-outline'  data-row='"+id+"' data-id='"+ data["data"][i]["id"] +"' onclick='rm_va_type_row(this);'><span class='fa fa-remove'></span></button>";
          }
          r += "</td>";
          r += "</tr>";
          $("#ExcBody").append(r);
          id++;
        }
      }

    });

    $(".btn-create-type-vac-row").click(function(){
      $("#ExTable").createVacTypeRows();
    });

    $(".btn-save-type-leave").click(function(){
      typeADD = [];
      typeEDIT = [];

      var typeBody = $("#ExcBody tr").not(".no-list");
      for (var i = 0; i < typeBody.length; i++) {
        var typeTr = typeBody[i];
        if ($(typeTr).hasClass("editrow")) {
          var typeTd = typeBody[i].childNodes;
          var typeRow = {};
          typeRow.leave_type_id = typeTr.dataset["row"];
          for (var td = 0; td < (typeTd.length-1); td++) {
               var inputValue = typeTd[td].children[0].value;
               if (td == 0) {
                   typeRow.leave_type_name = inputValue;
               }
          }
          typeEDIT.push(typeRow);
        }else if($(typeTr).hasClass("addrow")){
          var typeTd = typeBody[i].childNodes;
          var typeRow = {};
          for (var td = 0; td < (typeTd.length-1); td++) {
               var inputValue = typeTd[td].children[0].value;
               if (td == 0) {
                   typeRow.leave_type_name = inputValue;
               }
          }
          typeADD.push(typeRow);
        }
      }

      var req = {
        "leave_type" : {
            "add" : typeADD,
            "edit" : typeEDIT,
            "delete" : typeDEL
        }
      }

      // console.log(req);
      var urlupdateType = getApi("vacation/updateType");
      $.ajax({
        url: urlupdateType,
        type: 'POST',
        headers: {'Authorization': token},
        data : {"request" : req}
      }).done(function(data){
        if (data) {
          $("#VacationTypeModal").modal("toggle");
          $.toast({ heading: "SUCCESS",text: "แก้ไขเรียบร้อยแล้ว",position: 'top-right',loaderBg:'#ff6849',icon: 'success',hideAfter: 3500,stack: 6});
        }else{
          $.toast({heading: "FAILED",text: "พบข้อผิดพลาดโปรดตรวจสอบ",position: 'top-right',loaderBg:'#ff6849',icon: 'error',hideAfter: 3500,stack: 6});
        }
      });
    });

  });
})

$(".btn-create-vcation").click(function(){
  cleararr();
  var url = base_url('vacation/ModalCreate');
  $.ajax({
    url: url,
    type: "POST",
  })
  .done(function(data) {
    $(".modal-area").html(data);
    $("#CVacationModal").modal("toggle");

    $(".btn-create-vac-row").click(function(event) {
        $("#ExTable").createVacRows();
    });

    $(".btn-save-leave").click(function(){
      vacADD = [];
      var lev_name = $("#va_name").val();

      var vaBody = $("#ExcBody tr").not(".no-list");
      for (var i = 0; i < vaBody.length; i++) {
        var vaTd = vaBody[i].childNodes;
        var vaRow = {};
        for (var td = 0; td < (vaTd.length-1); td++) {
             var inputValue = vaTd[td].children[0].children[0].value;
             if (td == 0) {
               if (inputValue == "0") {
                 $.toast({heading: "FAILED",text: "โปรดเลือกประเภทการลาให้ถูกต้อง",position: 'top-right',loaderBg:'#ff6849',icon: 'warning',hideAfter: 3500,stack: 6});
                 return;
               }else{
                 vaRow.levd_type = inputValue;
               }
             }else if (td == 1) {
                 vaRow.levd_request_date = inputValue;
             }else if (td == 2) {
                 vaRow.levd_is_gender = inputValue;
             }else if(td == 3){
               var levd_is_deduct_money  = 0;
               if ($("#"+vaTd[td].children[0].children[0].id).is(":checked")) {
                   var  levd_is_deduct_money = 1;
                 }
                 vaRow.levd_is_deduct_money = levd_is_deduct_money;
             }else if (td == 4) {
               var levd_is_urgent  = 0;
               if ($("#"+vaTd[td].children[0].children[0].id).is(":checked")) {
                   var  levd_is_urgent = 1;
                 }
                 vaRow.levd_is_urgent = levd_is_urgent;
             }else if (td == 5) {
               var levd_delay_request  = 0;
               if ($("#"+vaTd[td].children[0].children[0].id).is(":checked")) {
                   var  levd_delay_request = 1;
                 }
                 vaRow.levd_delay_request = levd_delay_request;
             }

        }
        vacADD.push(vaRow);

      }

      var req = {
        "lev_name" : lev_name,
        "vacation" : {
          "add" : vacADD
        }
      };

      var urlapi = getApi("vacation/addNW");
      console.log(req);
      $.ajax({
        url: urlapi,
        type: 'POST',
        headers: {'Authorization': token},
        data : {"request" : req}
      }).done(function(data){
        if (data) {
          $.toast({ heading: "SUCCESS",text: "เพิ่มแบบลาเรียบร้อยแล้ว",position: 'top-right',loaderBg:'#ff6849',icon: 'success',hideAfter: 3500,stack: 6});
          $("#CVacationModal").modal("toggle");
          $(".vacation_table").dataTable().fnDestroy();
          $("#vacation_body").vacations();
        }else{
          $.toast({heading: "FAILED",text: "พบข้อผิดพลาดโปรดตรวจสอบ",position: 'top-right',loaderBg:'#ff6849',icon: 'error',hideAfter: 3500,stack: 6});
        }
      });

    });

  });
});


(function ($){
  $.fn.vacations = function(){
    var vacationBody = $(this);
    vacationBody.html("");
    var urlapi = getApi("vacation/getAll");
    $.ajax({
      url: urlapi,
      type: 'GET',
      headers: {'Authorization': token}
    }).done(function(data){
      if (data["Has"] == false) {
        vacationBody.html("");
      }else{
        var no = 1;
        for (var i = 0; i < data["data"].length; i++) {
          var r = "<tr>";
          r += "<td class='text-center' >";
           r += no;
           r += "</td>";
           r += "<td>";
           r += data["data"][i]["lev_name"];
           r += "</td>";
           r += "<td class='text-center'><div class='btn-group'>"
           r += "<button class='btn btn-sm btn-default btn-outline waves-effect btn-site-edit' data-edit='"+ data["data"][i]["lev_id"] +"'  onclick='vacation_edit(this)'><span class='fa fa-edit'></span></button>";
           r += "<button class='btn btn-sm btn-default btn-outline waves-effect btn-site-config' data-config='"+ data["data"][i]["lev_id"] +"'  onclick='vacation_config(this)'><span class='fa fa-gears'></span></button>";
           r += "<button class='btn btn-sm btn-default btn-outline waves-effect' data-delete='"+ data["data"][i]["lev_id"] +"' onclick='vacation_delete(this)'><span class='fa fa-trash'></span></button>";
           r += "</div></td>";
           r += "</tr>";
          no++;
          vacationBody.append(r);
        }
      }
      $(".vacation_table").DataTable(dataTableTh);
    });
  }
})(jQuery);

function vacation_edit(el){
  cleararr();
  var va_id = $(el).data("edit");
  var url = base_url('vacation/EditModal');
  $.ajax({
    url: url,
    type: "POST",
  })
  .done(function(data) {
    $(".modal-area").html(data);
    $("#VacationModal").modal("toggle");

    $(".btn-create-vac-row").click(function(event) {
        $("#ExTable").createVacRows();
    });

    var urlvacationdetail = getApi("vacation/getBy");
    $.ajax({
      url: urlvacationdetail,
      type: 'GET',
      headers: {'Authorization': token},
      data: {"va_id" : va_id}
    }).done(function(data){
      if (data["Has"] == false) {

      }else{
        var table = $("#ExTable");
        var tr = $("#ExcBody tr");
        $("#va_name").val(data["data"][0]["lev_name"]);
        $(".no-list").remove();
        var id = 1;

        for (var i = 0; i < data["data"].length; i++) {
          var is_urgent = data["data"][i]["levd_is_urgent"];
          var delay_request = data["data"][i]["levd_delay_request"];
          var fix_gender = data["data"][i]["levd_is_gender"];
          var is_one_time = data["data"][i]["levd_is_one_time"];
          var is_deduct_money = data["data"][i]["levd_is_deduct_money"];
          var levd_type = data["data"][i]["levd_type"];

          var r = "";
          r += "<tr class='editrow' id='va_row"+id+"' data-row='"+id+"' data-vad='"+ data["data"][i]["levd_no"] +"'>";
          r += "<td>";
          r += "<div class='form-group has-feedback' id='txtcaption_"+id+"_group'>";
          r += "<select class='form-control select-type-lev' name='' id='selectTypeLev"+ id +"'>";
          r += "</select>";
          r += "<span class='form-control-feedback' id='txtcaption_"+id+"_feedback'></span>";
          r += "</div>";
          r += "</td>";
          r += "<td>";
          r += "<div class='form-group has-feedback ' id='txtrequest_date_"+id+"_group'>";
          r += "<input type='text' class='form-control text-left' id='txtrequest_date_"+id+"' name='' value='" + data["data"][i]["levd_request_date"] + "' placeholder='ชั่วโมง' onkeypress='return numberOnly(event);'>";
          r += "<span class='form-control-feedback' id='txtrequest_date_"+id+"_feedback'></span>";
          r += "</div>";
          r += "</td>";
          r += "<td>";
          r += "<div class='form-group has-feedback' id='txtcaption_"+id+"_group'>";
          r += "<select class='form-control' name='select_fix_gender' id='select_fix_gender"+id+"'>";
          r += "<option value='A'>ทั้งหมด</option>";
          r += "<option value='M'>ชาย</option>";
          r += "<option value='F'>หญิง</option>";
          r += "</select>";
          r += "</div>";
          r += "</td>";
          r += "<td  class='text-center'>";
          r += "<div class='checkbox checkbox-success'>";
          r += "<input id='chkis_deduct_money_"+id+"'  name='chkis_deduct_money_"+id+"' type='checkbox'>";
          r += "<label for='chkis_deduct_money_"+id+"_label'></label>";
          r += "</div>";
          r += "</td>";
          // r += "<td  class='text-center'>";
          // r += "<div class='checkbox checkbox-success'>";
          // r += "<input id='chkis_one_time_"+id+"'  name='chkis_one_time_"+id+"' type='checkbox'>";
          // r += "<label for='chkis_one_time_"+id+"_label'></label>";
          // r += "</div>";
          // r += "</td>";
          r += "<td  class='text-center'>";
          r += "<div class='checkbox checkbox-success'>";
          r += "<input id='chkis_urgent_"+id+"'  name='chkis_urgent_"+id+"' type='checkbox'>";
          r += "<label for='chkis_urgent_"+id+"_label'></label>";
          r += "</div>";
          r += "</td>";
          r += "<td  class='text-center'>";
          r += "<div class='checkbox checkbox-success'>";
          r += "<input id='chkdelay_request_"+id+"'  name='chkdelay_request_"+id+"' type='checkbox'>";
          r += "<label for='chkdelay_request_"+id+"_label'></label>";
          r += "</div>";
          r += "</td>";
          r += "<td class='text-right'>";
          r += "<button type='button' class='btn btn-danger btn-outline'  data-row='"+id+"' data-levd_no='"+ data["data"][i]["levd_no"] +"' onclick='rm_va_row(this);'><span class='fa fa-remove'></span></button>";
          r += "</td>";
          r += "</tr>";
          table.append(r);
          if (is_urgent  == 1 ) {$("#chkis_urgent_"+id).attr('checked', true).val(is_urgent); }
          if (delay_request  == 1 ) {$('#chkdelay_request_'+id).attr('checked', true).val(delay_request); }
          if (is_one_time  == 1 ) {$('#chkis_one_time_'+id).attr('checked', true).val(is_one_time); }
          if (is_deduct_money  == 1 ) {$('#chkis_deduct_money_'+id).attr('checked', true).val(is_deduct_money); }
          $("#select_fix_gender"+id).val(fix_gender);
          gentype_lev(id,levd_type);
            id++;
        }
      }

      $(".btn-save-leave").click(function(){
        vacADD = [];
        vacEDIT = [];
        var lev_name = $("#va_name").val();

        var vaBody = $("#ExcBody tr").not(".no-list");
        for (var i = 0; i < vaBody.length; i++) {
          var vaTr = vaBody[i];
          if ($(vaTr).hasClass("editrow")) {
            var vaTd = vaBody[i].childNodes;
            var vaRow = {};
            vaRow.levd_no = vaTr.dataset["vad"];
            for (var td = 0; td < (vaTd.length-1); td++) {
                 var inputValue = vaTd[td].children[0].children[0].value;
                 if (td == 0) {
                   if (inputValue == "0") {
                     $.toast({heading: "FAILED",text: "โปรดเลือกประเภทการลาให้ถูกต้อง",position: 'top-right',loaderBg:'#ff6849',icon: 'warning',hideAfter: 3500,stack: 6});
                     return;
                   }else{
                     vaRow.levd_type = inputValue;
                   }
                 }else if (td == 1) {
                     vaRow.levd_request_date = inputValue;
                 }else if (td == 2) {
                     vaRow.levd_is_gender = inputValue;
                 }else if(td == 3){
                   var levd_is_deduct_money  = 0;
                   if ($("#"+vaTd[td].children[0].children[0].id).is(":checked")) {
                       var  levd_is_deduct_money = 1;
                     }
                     vaRow.levd_is_deduct_money = levd_is_deduct_money;
                 }else if (td == 4) {
                   var levd_is_urgent  = 0;
                   if ($("#"+vaTd[td].children[0].children[0].id).is(":checked")) {
                       var  levd_is_urgent = 1;
                     }
                     vaRow.levd_is_urgent = levd_is_urgent;
                 }else if (td == 5) {
                   var levd_delay_request  = 0;
                   if ($("#"+vaTd[td].children[0].children[0].id).is(":checked")) {
                       var  levd_delay_request = 1;
                     }
                     vaRow.levd_delay_request = levd_delay_request;
                 }

            }
            vacEDIT.push(vaRow);
          }else{
            var vaTd = vaBody[i].childNodes;
            var vaRow = {};
            for (var td = 0; td < (vaTd.length-1); td++) {
                 var inputValue = vaTd[td].children[0].children[0].value;
                 if (td == 0) {
                   if (inputValue == "0") {
                     $.toast({heading: "FAILED",text: "โปรดเลือกประเภทการลาให้ถูกต้อง",position: 'top-right',loaderBg:'#ff6849',icon: 'warning',hideAfter: 3500,stack: 6});
                     return;
                   }else{
                     vaRow.levd_type = inputValue;
                   }
                 }else if (td == 1) {
                     vaRow.levd_request_date = inputValue;
                 }else if (td == 2) {
                     vaRow.levd_is_gender = inputValue;
                 }else if(td == 3){
                   var levd_is_deduct_money  = 0;
                   if ($("#"+vaTd[td].children[0].children[0].id).is(":checked")) {
                       var  levd_is_deduct_money = 1;
                     }
                     vaRow.levd_is_deduct_money = levd_is_deduct_money;
                 }else if (td == 4) {
                   var levd_is_urgent  = 0;
                   if ($("#"+vaTd[td].children[0].children[0].id).is(":checked")) {
                       var  levd_is_urgent = 1;
                     }
                     vaRow.levd_is_urgent = levd_is_urgent;
                 }else if (td == 5) {
                   var levd_delay_request  = 0;
                   if ($("#"+vaTd[td].children[0].children[0].id).is(":checked")) {
                       var  levd_delay_request = 1;
                     }
                     vaRow.levd_delay_request = levd_delay_request;
                 }

            }
            vacADD.push(vaRow);
          };
        }

        var req = {
          "lev_id" : va_id,
          "lev_name" : lev_name,
          "vacation" : {
            "add" :	vacADD,
            "edit" :	vacEDIT,
            "delete" :	vacDEL
          }
        };

        var urlapi = getApi("vacation/updateNW");
        // console.log(req);
        $.ajax({
          url: urlapi,
          type: 'POST',
          headers: {'Authorization': token},
          data : {"request" : req}
        }).done(function(data){
          if (data) {
            $.toast({ heading: "SUCCESS",text: "แก้ไขเรียบร้อยแล้ว",position: 'top-right',loaderBg:'#ff6849',icon: 'success',hideAfter: 3500,stack: 6});
            $("#VacationModal").modal("toggle");
            $(".vacation_table").dataTable().fnDestroy();
            $("#vacation_body").vacations();
          }else{
            $.toast({heading: "FAILED",text: "พบข้อผิดพลาดโปรดตรวจสอบ",position: 'top-right',loaderBg:'#ff6849',icon: 'error',hideAfter: 3500,stack: 6});
          }
        });

      });
    });

  });
}


function vacation_delete(el){
  var lev_id = $(el).data("delete");

    swal({
    title: 'โปรดยืนยัน',
    text: "คุณต้องการยกเลิกแบบลาหรือไม่",
    type: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    confirmButtonText: 'Yes, delete it!'
  }).then((result) => {
    if (result.value) {
      var urlvacationinactive = getApi("vacation/inactiveLeave");
      $.ajax({
        url: urlvacationinactive,
        type: 'POST',
        headers: {'Authorization': token},
        data: {"lev_id" : lev_id}
      }).done(function(data){
        if (data) {
          $.toast({ heading: "SUCCESS",text: "ยกเลิกเรียบร้อยแล้ว",position: 'top-right',loaderBg:'#ff6849',icon: 'success',hideAfter: 3500,stack: 6});
          $(".vacation_table").dataTable().fnDestroy();
          $("#vacation_body").vacations();
        }else{
          $.toast({heading: "FAILED",text: "พบข้อผิดพลาดโปรดตรวจสอบ",position: 'top-right',loaderBg:'#ff6849',icon: 'error',hideAfter: 3500,stack: 6});
        }
      });
    }
  })



}

$.fn.createVacRows = function (){
      var table = $(this);
      var tr = $("#ExcBody tr");
      var num = tr.length;
      var id = 1;
      if (num > 0) {
          var id = parseFloat(tr[num-1].dataset["row"]);
          id += 1;
      }
      console.log(table);
      $(".no-list").remove();
      var r  = "";
          r += "<tr  id='va_row"+id+"' data-row='"+id+"' >";
          r += "<td>";
          r += "<div class='form-group has-feedback' id='txtcaption_"+id+"_group'>";
          r += "<select class='form-control' name='' id='selectTypeLev"+ id +"'>";
          r += "</select>";
          r += "<span class='form-control-feedback' id='txtcaption_"+id+"_feedback'></span>";
          r += "</div>";
          r += "</td>";
          r += "<td>";
          r += "<div class='form-group has-feedback ' id='txtrequest_date_"+id+"_group'>";
          r += "<input type='text' class='form-control text-left' id='txtrequest_date_"+id+"' name='' value='' placeholder='ชั่วโมง' onkeypress='return numberOnly(event);'>";
          r += "<span class='form-control-feedback' id='txtrequest_date_"+id+"_feedback'></span>";
          r += "</div>";
          r += "</td>";
          r += "<td>";
          r += "<div class='form-group has-feedback' id='txtcaption_"+id+"_group'>";
          r += "<select class='form-control' name='select_fix_gender' id='select_fix_gender"+id+"'>";
          r += "<option value='A'>ทั้งหมด</option>";
          r += "<option value='M'>ชาย</option>";
          r += "<option value='F'>หญิง</option>";
          r += "</select>";
          r += "</div>";
          r += "</td>";
          r += "<td  class='text-center'>";
          r += "<div class='checkbox checkbox-success'>";
          r += "<input id='chkis_deduct_money_"+id+"'  name='chkis_deduct_money_"+id+"' type='checkbox'>";
          r += "<label for='chkis_deduct_money_"+id+"_label'></label>";
          r += "</div>";
          r += "</td>";
          // r += "<td  class='text-center'>";
          // r += "<div class='checkbox checkbox-success'>";
          // r += "<input id='chkis_one_time_"+id+"'  name='chkis_one_time_"+id+"' type='checkbox'>";
          // r += "<label for='chkis_one_time_"+id+"_label'></label>";
          // r += "</div>";
          // r += "</td>";
          r += "<td  class='text-center'>";
          r += "<div class='checkbox checkbox-success'>";
          r += "<input id='chkis_urgent_"+id+"'  name='chkis_urgent_"+id+"' type='checkbox'>";
          r += "<label for='chkis_urgent_"+id+"_label'></label>";
          r += "</div>";
          r += "</td>";
          r += "<td  class='text-center'>";
          r += "<div class='checkbox checkbox-success'>";
          r += "<input id='chkdelay_request_"+id+"'  name='chkdelay_request_"+id+"' type='checkbox'>";
          r += "<label for='chkdelay_request_"+id+"_label'></label>";
          r += "</div>";
          r += "</td>";
          // r += "<td>";
          // r += "<button type='button' class='btn btn-info btn-outline'  data-row='"+id+"' onclick='rm_va_row(this);'>กำหนดเงื่อนไข</button>"
          // r += "</td>";
          r += "<td class='text-right'>";
          r += "<button type='button' class='btn btn-danger btn-outline'  data-row='"+id+"' onclick='rm_va_row(this);'><span class='fa fa-remove'></span></button>";
          r += "</td>";
          r += "</tr>";
          table.append(r);

          var urlgetTypeAll = getApi("vacation/getTypeAll");
          $.ajax({
            url: urlgetTypeAll,
            type: 'GET',
            headers: {'Authorization': token}
          }).done(function(data){
            if (data["Has"] == false) {

            }else{
              var r = "<option value='0'>";
              r += "โปรดเลือกประเภทการลา";
              r += "</option>";
              for (var i = 0; i < data["data"].length; i++) {
                r += "<option value='"+ data["data"][i]["id"] +"'>";
                r += data["data"][i]["leave_type_name"];
                r += "</option>"
              }
              $("#selectTypeLev"+id).append(r);
            }
          });
}

$.fn.createVacTypeRows = function (){
  var table = $(this);
  var tr = $("#ExcBody tr");
  var num = tr.length;
  var id = 1;
  if (num > 0) {
      var id = parseFloat(tr[num-1].dataset["row"]);
      id += 1;
  }
  $(".no-list").remove();
  var r = "";
  r += "<tr class='addrow'  id='type_row"+id+"' data-row='"+id+"' >";
  r += "<td>";
  r += "<input type='text' class='form-control' id='txtleave_name_"+id+"' name='' value='' placeholder='ชื่อเงื่อนไข'>";
  r += "</td>";
  r += "<td align='right'>";
  r += "<button type='button' class='btn btn-danger btn-outline' data-row='"+id+"' onclick='rm_va_type_row(this);'><span class='fa fa-remove'></span></button>";
  r += "</td>";
  r += "</tr>";
  table.append(r);
}

$.fn.createConfigRows = function (){
  var table = $(this);
  var tr = $("#configBody"+ tmpTabContent +" tr");
  var num = tr.length;
  var id = 1;
  if (num > 0) {
      var id = parseFloat(tr[num-1].dataset["row"]);
      id += 1;
  }

  $(".no-list-config").remove();
  var r = "";
  r += "<tr class='addrow'  id='type_config_row"+id+"' data-row='"+id+"' >";
  r += "<td width='100'>";
  r += "<input type='number' class='form-control ' id='txt_exp_start_"+id+"' name='' value='' placeholder='ปี' size='3' maxlength='4'>";
  r += "</td>";
  r += "<td td width='50'>";
  r += "<span class='col-sm-2 text-center' style='padding-top:10px;'> ถึง </span>";
  r += "</td>";
  r += "<td width='100'>";
  r += "<input type='number' class='form-control ' id='txt_exp_end_"+id+"' name='' value='' placeholder='ปี' size='3' maxlength='4'>";
  r += "</td>";
  r += "<td>";
  r += "<input type='number' class='form-control' id='txt_holiday_"+id+"' name='' value='' placeholder='จำนวนวันหยุด'>";
  r += "</td>";
  r += "<td align='right'>";
  r += "<button type='button' class='btn btn-danger btn-outline' data-row='"+id+"' onclick='rm_type_config_row(this);'><span class='fa fa-remove'></span></button>";
  r += "</td>";
  r += "</tr>";
  table.append(r);
}

function rm_va_row(ev){
    var row = ev.dataset["row"];
    var vac_id = ev.dataset["levd_no"];
    $("#va_row"+row).remove();
    var tr = $("#ExcBody tr");
    if (tr.length < 1) {
    var r  = "";
        r += "<tr  class='text-center no-list' data-row='0'>";
        r += "<td  colspan='4'>";
        r += "<h5>ไม่พบรายการ</h5>";
        r += "</td>";
        r += "</tr>";
        $("#ExcBody").append(r);
    }
    vacDEL.push(vac_id);
    console.log(vacDEL);
}

function rm_va_type_row(ev){
    var row = ev.dataset["row"];
    var id = ev.dataset["id"];
    $("#type_row"+row).remove();
    var tr = $("#ExcBody tr");
    if (tr.length < 1) {
    var r  = "";
        r += "<tr  class='text-center no-list' data-row='0'>";
        r += "<td  colspan='4'>";
        r += "<h5>ไม่พบรายการ</h5>";
        r += "</td>";
        r += "</tr>";
        $("#ExcBody").append(r);
    }
    typeDEL.push(id);
    console.log(typeDEL);
}

function rm_type_config_row(ev){
    var row = ev.dataset["row"];
    var levd_ab_no = ev.dataset["levd_ab_no"];
    $("#type_config_row"+row).remove();
    var tr = $("#ExcBody tr");
    if (tr.length < 1) {
    var r  = "";
        r += "<tr  class='text-center no-list' data-row='0'>";
        r += "<td  colspan='4'>";
        r += "<h5>ไม่พบรายการ</h5>";
        r += "</td>";
        r += "</tr>";
        $("#ExcBody").append(r);
    }
    console.log(levd_ab_no);
    if (levd_ab_no !== "undefined") {
      configDEL.push(levd_ab_no);
    }
    // console.log(vacDEL);
}

function numberOnly(ev){
  if (ev.keyCode < 48 || ev.keyCode > 57) {
      $.toast({ heading:"คำเตือน",text:"กรอกเฉพาะตัวเลข",position: 'top-right',loaderBg:'#ff6849',icon: 'warning',hideAfter: 3500,stack: 6});
      return false;
  }
}

function gentype_lev(id,levd_type){
  var urlgetTypeAll = getApi("vacation/getTypeAll");
  $.ajax({
    url: urlgetTypeAll,
    type: 'GET',
    headers: {'Authorization': token}
  }).done(function(data){
    if (data["Has"] == false) {

    }else{
      var r = "";
      r += "<option value='0'>";
      r += "โปรดเลือกประเภทการลา";
      r += "</option>";
      for (var i = 0; i < data["data"].length; i++) {
        r += "<option value='"+ data["data"][i]["id"] +"'>";
        r += data["data"][i]["leave_type_name"];
        r += "</option>"
      }

      $("#selectTypeLev"+id).append(r);
      $("#selectTypeLev"+id).val(levd_type);
    }
});
}

function vacation_config(el){
  tmpTabContent = 0;
  var tmpiContent = 0;
  var va_id = $(el).data("config");
  var url = base_url('Vacation/getConfig');
  $.ajax({
    url: url,
    type: "POST",
  })
  .done(function(data) {
    $(".modal-area").html(data);
    $("#ConfigModal").modal("toggle");
    configDEL = [];

    var urlvacationdetail = getApi("vacation/getBy");
    $.ajax({
      url: urlvacationdetail,
      type: 'GET',
      headers: {'Authorization': token},
      data: {"va_id" : va_id}
    }).done(function(data) {
      if (data["Has"]) {
        var r = "";
        // console.log(data["data"]);
        // r += "<ul class='nav tabs-vertical'>";

        for (var i = 0; i < data["data"].length; i++) {
          var active = "";
          if (i == 0) {
            active = "active";
          }
          r += "<li class='tab tab-lev-type "+ active +"' data-leave_type='"+ data["data"][i]["levd_type"] +"' data-tab-index='"+ i +"'>";
          r += "<a data-toggle='tab' href='#tab"+ i +"' aria-expanded='true'> <span class='visible-xs'><i class='ti-home'></i></span> <span class='hidden-xs'>";
          r += data["data"][i]["leave_type_name"];
          r += "</span> </a>";
          r += "</li>";
        }
        // r += "</ul>";
        $(".type_tab").append(r);

        $(".tab-lev-type").click(function(){
          tmpTabContent = $(this).data("tab-index");

          // console.log("type_config"+tmpTabContent);
        });

        r_content = "";
        for (var iContent = 0; iContent < data["data"].length; iContent++) {
          r_content = "";
          var active = "";
          if (iContent == 0) {
            active = "active";
          }
          r_content += "<div id='tab"+ iContent +"' class='tab-pane "+ active +"'>";
          r_content += "<div class='row' style='padding-left:0ox;'>";
          r_content += "<h4 class='col-md-6'>ชื่อแบบการลา: " + data["data"][iContent]["lev_name"] + "</h4>";
          r_content += "<h4 class='col-md-6'>ประเภทการลา: " + data["data"][iContent]["leave_type_name"] + "</h4>";
          r_content += "<input type='hidden' id='lev_id"+ iContent +"' value='"+ data["data"][iContent]["lev_id"] +"'>";
          r_content += "<input type='hidden' id='levd_type"+ iContent +"' value='"+ data["data"][iContent]["levd_type"] +"'>";
          r_content += "</div>";
          r_content += "<hr>";

          r_content += "<div class='row'>";
          r_content += "<div class='type_ofsearch form-group col-md-12' id='report_month_type'>";
          r_content += "<label>ประเภทสิทธิ์</label>";
          r_content += "<div class='input-field custom sm type_config'  id='type_config"+ iContent +"'>";
          r_content += "<select class='form-control' name='type_config' id='select_type_config"+ iContent +"'>";
          r_content += "<option value='0'>โปรดเลือกสิทธิ์</option>";
          r_content += "<option value='1'>รับสิทธิ์เดิมทุกปี</option>";
          r_content += "<option value='2'>รับสิทธิ์เดิมตามอายุงาน</option>";
          r_content += "<option value='3'>รับสิทธิตามอายุงานแบบมีอัตราที่ไม่เท่ากัน</option>";
          r_content += "<option value='4'>รับสิทธิ์ได้ 1 ครั้งตลอดอายุงาน</option>";
          r_content += "</select>";
          r_content += "</div>";

          r_content += "<br><div class='type_config_edit"+ iContent +"'>";
          r_content += "</div>";

          r_content += "</div>";
          r_content += "</div>";
          r_content += "</div>";

          $(".tabs-config-content").append(r_content);

          /////////////////////// edit config
          if (data["data"][iContent]["levd_type_config"] !== null) {
            var type_config = data["data"][iContent]["levd_type_config"];
            $("#select_type_config"+iContent).val(type_config);

            var r_config = "";
            if (type_config == "1") {
              console.log(data["data"][iContent]["levd_reset_by_date"]);
              if (data["data"][iContent]["levd_reset_by_date"] != null) {
                var month = parseInt(data["data"][iContent]["levd_reset_by_date"].substring(0, 2));
                var day = data["data"][iContent]["levd_reset_by_date"].substring(2, 4);
              }

              // console.log(month+" "+day);
              // console.log(data["data"][iContent]["levd_reset_by_date"]);
              // console.log("type_config_edit"+iContent);
              $(".type_config_edit"+iContent).html("");

              r_config += "<div class='row'>";
              r_config += "<div class='form-group has-feedback col-md-12' id='txtmaster_"+ iContent +"_group'>";
              r_config += "<label>สิทธิ์ตามกฏหมาย</label>";
              r_config += "<input type='text' class='form-control text-left' id='txtmaster"+ iContent +"' name='' value='"+ data["data"][iContent]["levd_master"] +"' placeholder='จำนวนวัน' onkeypress='return numberOnly(event);'>";
              r_config += "<span class='form-control-feedback' id='txtmaster_"+ iContent +"_feedback'></span>";
              r_config += "</div>";
              r_config += "</div>";

              r_config += "<div class='input-field custom sm col-md-9'   id='month' style='padding-left:0px;'>";
              r_config += "<select class='form-control' name='month' id='select_month"+ iContent +"'>";
              for (var iMonth = 1; iMonth <= arrMonth.length; iMonth++)
              {
                r_config += "<option value='"+ iMonth +"'>";
                r_config += arrMonth[iMonth-1];
                r_config += "</option>";
              }
              r_config += "</select>";
              r_config += "</div>";

              r_config += "<div class='input-field custom sm col-md-3'   id='day' style='padding:0px;'>";
              r_config += "<select class='form-control' name='day' id='select_day"+ iContent +"'>";
              for (var i = 1; i <= 31; i++) {
                r_config += "<option value='"+ i +"'>"+ i +"</option>";
              }
              r_config += "</select>";
              r_config += "</div>";

              $(".type_config_edit"+iContent).append(r_config);

              $("#select_month"+iContent).val(month);
              $("#select_day"+iContent).val(day);

              $("#month").change(function() {
                var sMonth = $("#select_month").val();
                var allDay = 31;
                var r_OptionDay = "";
                if (sMonth == '4' || sMonth == '6' || sMonth == '9' || sMonth == '11') {
                  allDay = 30;
                }else if (sMonth == '2'){
                  allDay = 28;
                }
                for (var i = 1; i <= allDay; i++) {
                  r_OptionDay += "<option value='"+ i +"'>"+ i +"</option>";
                }
                $("#select_day").html("");
                $("#select_day").append(r_OptionDay);
              });


            }else if(type_config == "2"){
              $(".type_config_edit"+iContent).html("");

              r_config += "<div class='row'>";
              r_config += "<div class='form-group has-feedback col-md-12' id='txtmaster_"+iContent+"_group'>";
              r_config += "<label>สิทธิ์ตามกฏหมาย</label>";
              r_config += "<input type='text' class='form-control text-left' id='txtmaster"+iContent+"' name='' value='"+ data["data"][iContent]["levd_master"] +"' placeholder='จำนวนวัน' onkeypress='return numberOnly(event);'>";
              r_config += "<span class='form-control-feedback' id='txtmaster_"+iContent+"_feedback'></span>";
              r_config += "</div>";
              r_config += "</div>";

              r_config += "<div class='type_ofsearch form-group col-md-12' id='report_month_type' style='padding:0px;'>";
              r_config += "<label>ประเภทอายุงาน</label>";
              r_config += "<div class='input-field custom sm'  id='type_exp_config"+iContent+"'>";
              r_config += "<select class='form-control' name='type_exp_config' id='select_type_exp_config"+iContent+"'>";
              r_config += "<option value='0'>โปรดเลือกประเภทอายุงาน</option>";
              r_config += "<option value='1'>เริ่มงาน</option>";
              r_config += "<option value='2'>เริ่มบรรจุ (ผ่านโปร ฯ)</option>";
              r_config += "</select>";

              $(".type_config_edit"+iContent).append(r_config);

              $("#select_type_exp_config"+iContent).val(data["data"][iContent]["levd_reset_by_exp"]);
            }else if(type_config == "3"){

                          $(".type_config_edit"+iContent).html("");
                          // console.log("configBody"+tmpTabContent);

                          r_config += "<div class='type_ofsearch form-group col-md-12' id='report_month_type' style='padding:0px;'>";
                          r_config += "<label>ประเภทอายุงาน</label>";
                          r_config += "<div class='input-field custom sm'  id='type_exp_config"+iContent+"'>";
                          r_config += "<select class='form-control' name='type_exp_config' id='select_type_exp_config"+iContent+"'>";
                          r_config += "<option value='0'>โปรดเลือกประเภทอายุงาน</option>";
                          r_config += "<option value='1'>เริ่มงาน</option>";
                          r_config += "<option value='2'>เริ่มบรรจุ (ผ่านโปร ฯ)</option>";
                          r_config += "</select>";

                          r_config += "<br>";

                          r_config += "<label>สะสมวันหยุด</label>";
                          r_config += "<div class='input-field custom sm'  id='type_store_leave"+iContent+"'>";
                          r_config += "<select class='form-control' name='type_store_leave' id='select_type_store_leave"+iContent+"'>";
                          r_config += "<option value='0'>โปรดเลือก</option>";
                          r_config += "<option value='1'>สะสมวันหยุด</option>";
                          r_config += "<option value='2'>เคลียวันหยุดคงเหลือทั้งหมด</option>";
                          r_config += "</select>";

                          r_config += "<br>";

                          r_config += "<table class='table' id='leave_config_table"+ iContent +"'>";
                          r_config += "<thead>";
                          r_config += "<th width='50' colspan='3' class='text-left'>อายุงานถึงเกณฑ์</th>";
                          r_config += "<th width='70' class='text-left'>จำนวนวันหยุด</th>";
                          r_config += "<th width='50'><a class='btn btn-success pull-right m-l-20  btn-outline waves-effect waves-light btn-create-config-row'><span class='fa fa-plus'></span></a></th>";
                          r_config += "</thead>";
                          r_config += "<tbody id='configBody"+ iContent +"'>";
                          r_config += "<tr  class='text-center no-list-config' data-row='0'>";
                          r_config += "</tbody>";
                          r_config += "</table>";

                          $(".type_config_edit"+iContent).append(r_config);

                          $(".btn-create-config-row").click(function(){
                            $("#leave_config_table"+tmpTabContent).createConfigRows();
                          });

                          $("#select_type_exp_config"+iContent).val(data["data"][iContent]["levd_reset_by_exp"]);
                          $("#select_type_store_leave"+iContent).val(data["data"][iContent]["levd_is_store_leave"]);
                          // console.log("configBody"+iContent);
                          var configbody = "configBody"+iContent;
                          var urlleave_ab_config = getApi("LeaveData/getByLeaveABConfig");
                          $.ajax({
                            url: urlleave_ab_config,
                            type: 'GET',
                            headers: {'Authorization': token},
                            data: {"levd_id" : data["data"][iContent]["lev_id"], "levd_type" : data["data"][iContent]["levd_type"]}
                          }).done(function(data) {
                            if (data["code"] == 100) {
                              $(".no-list-config").remove();
                              var id = 1;
                              for (var i = 0; i < data["data"].length; i++) {
                                var r = "";
                                r += "<tr class='editrow'  id='type_config_row"+id+"' data-row='"+data["data"][i]["levd_ab_no"]+"' >";
                                r += "<td width='100'>";
                                r += "<input type='number' class='form-control ' id='txt_exp_start_"+id+"' name='' value='"+ data["data"][i]["levd_low_condition"] +"' placeholder='ปี' size='3' maxlength='4'>";
                                r += "</td>";
                                r += "<td td width='50'>";
                                r += "<span class='col-sm-2 text-center' style='padding-top:10px;'> ถึง </span>";
                                r += "</td>";
                                r += "<td width='100'>";
                                r += "<input type='number' class='form-control ' id='txt_exp_end_"+id+"' name='' value='"+ data["data"][i]["levd_high_condition"] +"' placeholder='ปี' size='3' maxlength='4'>";
                                r += "</td>";
                                r += "<td>";
                                r += "<input type='number' class='form-control' id='txt_holiday_"+id+"' name='' value='"+ data["data"][i]["levd_ab_master"] +"' placeholder='จำนวนวันหยุด'>";
                                r += "</td>";
                                r += "<td align='right'>";
                                r += "<button type='button' class='btn btn-danger btn-outline' data-row='"+id+"' data-levd_ab_no='"+ data["data"][i]["levd_ab_no"] +"' onclick='rm_type_config_row(this);'><span class='fa fa-remove'></span></button>";
                                r += "</td>";
                                r += "</tr>";
                                id++;
                                $("#"+configbody).append(r);
                              }
                              // console.log(configbody);

                            }
                          });
            }
            else if(type_config == "4"){
              $(".type_config_edit"+iContent).html("");

              r_config += "<div class='row'>";
              r_config += "<div class='form-group has-feedback col-md-12' id='txtmaster_"+ iContent +"_group'>";
              r_config += "<label>สิทธิ์ตามกฏหมาย</label>";
              r_config += "<input type='text' class='form-control text-left' id='txtmaster"+ iContent +"' name='' value='"+ data["data"][iContent]["levd_master"] +"' placeholder='จำนวนวัน' onkeypress='return numberOnly(event);'>";
              r_config += "<span class='form-control-feedback' id='txtmaster_"+ iContent +"_feedback'></span>";
              r_config += "</div>";
              r_config += "</div>";

              $(".type_config_edit"+iContent).append(r_config);
            }
          }
        }




        $(".type_config").change(function() {
          // console.log(tmpTabContent);
          var type_config = $("#select_type_config"+tmpTabContent).val();
          var r_config = "";
          if (type_config == "1") {
            console.log("type_config_edit"+tmpTabContent);
            $(".type_config_edit"+tmpTabContent).html("");

            r_config += "<div class='row'>";
            r_config += "<div class='form-group has-feedback col-md-12' id='txtmaster_"+ tmpTabContent +"_group'>";
            r_config += "<label>สิทธิ์ตามกฏหมาย</label>";
            r_config += "<input type='text' class='form-control text-left' id='txtmaster"+ tmpTabContent +"' name='' value='' placeholder='จำนวนวัน' onkeypress='return numberOnly(event);'>";
            r_config += "<span class='form-control-feedback' id='txtmaster_"+ tmpTabContent +"_feedback'></span>";
            r_config += "</div>";
            r_config += "</div>";

            r_config += "<div class='input-field custom sm col-md-9'   id='month' style='padding-left:0px;'>";
            r_config += "<select class='form-control' name='month' id='select_month"+ tmpTabContent +"'>";
            for (var iMonth = 1; iMonth <= arrMonth.length; iMonth++)
            {
              r_config += "<option value='"+ iMonth +"'>";
              r_config += arrMonth[iMonth-1];
              r_config += "</option>";
            }
            r_config += "</select>";
            r_config += "</div>";

            r_config += "<div class='input-field custom sm col-md-3'   id='day' style='padding:0px;'>";
            r_config += "<select class='form-control' name='day' id='select_day"+ tmpTabContent +"'>";
            for (var i = 1; i <= 31; i++) {
              r_config += "<option value='"+ i +"'>"+ i +"</option>";
            }
            r_config += "</select>";
            r_config += "</div>";

            $(".type_config_edit"+tmpTabContent).append(r_config);

            $("#month").change(function() {
              var sMonth = $("#select_month").val();
              var allDay = 31;
              var r_OptionDay = "";
              if (sMonth == '4' || sMonth == '6' || sMonth == '9' || sMonth == '11') {
                allDay = 30;
              }else if (sMonth == '2'){
                allDay = 28;
              }
              for (var i = 1; i <= allDay; i++) {
                r_OptionDay += "<option value='"+ i +"'>"+ i +"</option>";
              }
              $("#select_day").html("");
              $("#select_day").append(r_OptionDay);
            });


          }else if(type_config == "2"){
            $(".type_config_edit"+tmpTabContent).html("");

            r_config += "<div class='row'>";
            r_config += "<div class='form-group has-feedback col-md-12' id='txtmaster_"+tmpTabContent+"_group'>";
            r_config += "<label>สิทธิ์ตามกฏหมาย</label>";
            r_config += "<input type='text' class='form-control text-left' id='txtmaster"+tmpTabContent+"' name='' value='' placeholder='จำนวนวัน' onkeypress='return numberOnly(event);'>";
            r_config += "<span class='form-control-feedback' id='txtmaster_"+tmpTabContent+"_feedback'></span>";
            r_config += "</div>";
            r_config += "</div>";

            r_config += "<div class='type_ofsearch form-group col-md-12' id='report_month_type' style='padding:0px;'>";
            r_config += "<label>ประเภทอายุงาน</label>";
            r_config += "<div class='input-field custom sm'  id='type_exp_config"+tmpTabContent+"'>";
            r_config += "<select class='form-control' name='type_exp_config' id='select_type_exp_config"+tmpTabContent+"'>";
            r_config += "<option value='0'>โปรดเลือกประเภทอายุงาน</option>";
            r_config += "<option value='1'>เริ่มงาน</option>";
            r_config += "<option value='2'>เริ่มบรรจุ (ผ่านโปร ฯ)</option>";
            r_config += "</select>";

            $(".type_config_edit"+tmpTabContent).append(r_config);
          }else if(type_config == "3"){

            $(".type_config_edit"+tmpTabContent).html("");
            // console.log("configBody"+tmpTabContent);

            r_config += "<div class='type_ofsearch form-group col-md-12' id='report_month_type' style='padding:0px;'>";
            r_config += "<label>ประเภทอายุงาน</label>";
            r_config += "<div class='input-field custom sm'  id='type_exp_config"+tmpTabContent+"'>";
            r_config += "<select class='form-control' name='type_exp_config' id='select_type_exp_config"+tmpTabContent+"'>";
            r_config += "<option value='0'>โปรดเลือกประเภทอายุงาน</option>";
            r_config += "<option value='1'>เริ่มงาน</option>";
            r_config += "<option value='2'>เริ่มบรรจุ (ผ่านโปร ฯ)</option>";
            r_config += "</select>";

            r_config += "<br>";

            r_config += "<label>อนุญาติเก็บวันหยุด</label>";
            r_config += "<div class='input-field custom sm'  id='type_store_leave"+tmpTabContent+"'>";
            r_config += "<select class='form-control' name='type_store_leave' id='select_type_store_leave"+tmpTabContent+"'>";
            r_config += "<option value='0'>โปรดเลือก</option>";
            r_config += "<option value='1'>เก็บวันหยุดคงเหลือใช้ต่อ</option>";
            r_config += "<option value='2'>เคลียวันหยุดคงเหลือทั้งหมด</option>";
            r_config += "</select>";
            r_config += "</div>";

            r_config += "<br>";

            r_config += "<table class='table' id='leave_config_table"+ tmpTabContent +"'>";
            r_config += "<thead>";
            r_config += "<th width='50' colspan='3' class='text-left'>อายุงานถึงเกณฑ์</th>";
            r_config += "<th width='70' class='text-left'>จำนวนวันหยุด</th>";
            r_config += "<th width='50'><a class='btn btn-success pull-right m-l-20  btn-outline waves-effect waves-light btn-create-config-row'><span class='fa fa-plus'></span></a></th>";
            r_config += "</thead>";
            r_config += "<tbody id='configBody"+ tmpTabContent +"'>";
            r_config += "<tr  class='text-center no-list-config' data-row='0'>";
            r_config += "</tbody>";
            r_config += "</table>";

            $(".type_config_edit"+tmpTabContent).append(r_config);

            $(".btn-create-config-row").click(function(){
              $("#leave_config_table"+tmpTabContent).createConfigRows();
            });
          }else if(type_config == "4"){
            $(".type_config_edit"+tmpTabContent).html("");

            r_config += "<div class='row'>";
            r_config += "<div class='form-group has-feedback col-md-12' id='txtmaster_"+ tmpTabContent +"_group'>";
            r_config += "<label>สิทธิ์ตามกฏหมาย</label>";
            r_config += "<input type='text' class='form-control text-left' id='txtmaster"+ tmpTabContent +"' name='' value='' placeholder='จำนวนวัน' onkeypress='return numberOnly(event);'>";
            r_config += "<span class='form-control-feedback' id='txtmaster_"+ tmpTabContent +"_feedback'></span>";
            r_config += "</div>";
            r_config += "</div>";

            $(".type_config_edit"+tmpTabContent).append(r_config);
          }

        });


      }

      $(".btn-save-config").click(function(){
        var type_config = $("#select_type_config"+tmpTabContent).val();
        var lev_id = $("#lev_id"+tmpTabContent).val();
        var levd_type = $("#levd_type"+tmpTabContent).val();
        var req;
        if (type_config == "1") {

          var leave_master = $("#txtmaster"+tmpTabContent).val();
          var month = $("#select_month"+tmpTabContent).val();
          var day = $("#select_day"+tmpTabContent).val();

          if (month.length == 1) {month = "0"+month;}
          if (day.length == 1) {day = "0"+day;}

          req = {
            "select_type" : "1",
            "lev_id" : lev_id,
            "levd_type" : levd_type,
            "leave_master" : leave_master,
            "levd_reset_by_date" : month+day
          }
        }else if(type_config == "2"){
          var leave_master = $("#txtmaster"+tmpTabContent).val();
          var leave_type_exp = $("#select_type_exp_config"+tmpTabContent).val();

          req = {
            "select_type" : "2",
            "lev_id" : lev_id,
            "levd_type" : levd_type,
            "leave_master" : leave_master,
            "leave_type_exp" :leave_type_exp
          }
        }else if(type_config == "3"){
          configADD = [];
          configEDIT = [];

          var typeBody = $("#configBody"+ tmpTabContent +" tr").not(".no-list-config");
          var leave_type_exp = $("#select_type_exp_config"+tmpTabContent).val();
          var levd_is_store_leave = $("#select_type_store_leave"+tmpTabContent).val();
          // console.log(typeBody);
          for (var i = 0; i < typeBody.length; i++) {
            var typeTr = typeBody[i];
            if ($(typeTr).hasClass("editrow")) {
              var typeTd = typeBody[i].childNodes;
              var typeRow = {};
              typeRow.levd_ab_no = typeTr.dataset["row"];
              for (var td = 0; td < (typeTd.length-1); td++) {
                   var inputValue = typeTd[td].children[0].value;
                   if (td == 0) {
                     if (inputValue < 0 || inputValue == '') {
                       $.toast({ heading: "FAILD",text: "ตัวเลขไม่ถูกต้อง โปรดตรวจสอบ",position: 'top-right',loaderBg:'#ff6849',icon: 'warning',hideAfter: 3500,stack: 6});
                       return;
                     }
                      typeRow.levd_low_condition = inputValue;
                   }else if(td == 2){
                     if (inputValue < 0 || inputValue == '') {
                       $.toast({ heading: "FAILD",text: "ตัวเลขไม่ถูกต้อง โปรดตรวจสอบ",position: 'top-right',loaderBg:'#ff6849',icon: 'warning',hideAfter: 3500,stack: 6});
                       return;
                     }
                      typeRow.levd_high_condition = inputValue;
                   }else if(td == 3){
                     if (inputValue < 0 || inputValue == '') {
                       $.toast({ heading: "FAILD",text: "ตัวเลขไม่ถูกต้อง โปรดตรวจสอบ",position: 'top-right',loaderBg:'#ff6849',icon: 'warning',hideAfter: 3500,stack: 6});
                       return;
                     }
                      typeRow.levd_ab_master = inputValue;
                   }
              }
              configEDIT.push(typeRow);
            }else if($(typeTr).hasClass("addrow")){
              var typeTd = typeBody[i].childNodes;
              var typeRow = {};
              for (var td = 0; td < (typeTd.length-1); td++) {
                   var inputValue = typeTd[td].children[0].value;
                   if (td == 0) {
                     if (inputValue < 0 || inputValue == '') {
                       $.toast({ heading: "FAILD",text: "ตัวเลขไม่ถูกต้อง โปรดตรวจสอบ",position: 'top-right',loaderBg:'#ff6849',icon: 'warning',hideAfter: 3500,stack: 6});
                       return;
                     }
                      typeRow.levd_low_condition = inputValue;
                   }else if(td == 2){
                     if (inputValue < 0 || inputValue == '') {
                       $.toast({ heading: "FAILD",text: "ตัวเลขไม่ถูกต้อง โปรดตรวจสอบ",position: 'top-right',loaderBg:'#ff6849',icon: 'warning',hideAfter: 3500,stack: 6});
                       return;
                     }
                      typeRow.levd_high_condition = inputValue;
                   }else if(td == 3){
                     if (inputValue < 0 || inputValue == '') {
                       $.toast({ heading: "FAILD",text: "ตัวเลขไม่ถูกต้อง โปรดตรวจสอบ",position: 'top-right',loaderBg:'#ff6849',icon: 'warning',hideAfter: 3500,stack: 6});
                       return;
                     }
                      typeRow.levd_ab_master = inputValue;
                   }
              }
              configADD.push(typeRow);
            }
          }

          var req = {
            "select_type" : "3",
            "lev_id" : lev_id,
            "levd_type" : levd_type,
            "leave_type_exp" :leave_type_exp,
            "levd_is_store_leave" : levd_is_store_leave,
            "config" : {
              "add" :	configADD,
              "edit" :	configEDIT,
              "delete" :	configDEL
            }
          }

          // console.log(req);
        }else if(type_config == "4"){
          var leave_master = $("#txtmaster"+tmpTabContent).val();

          req = {
            "select_type" : "4",
            "lev_id" : lev_id,
            "levd_type" : levd_type,
            "leave_master" : leave_master,
            "levd_is_one_time" : 1
          }
        }

        var urlUpdateConfig = getApi("LeaveData/saveLeaveConfig");
        // console.log(req);
        $.ajax({
          url: urlUpdateConfig,
          type: 'POST',
          headers: {'Authorization': token},
          data: {"request": req }
        }).done(function(data) {
          if (data["Has"] == true) {
            $.toast({ heading: "SUCCESS",text: data["message"],position: 'top-right',loaderBg:'#ff6849',icon: 'success',hideAfter: 3500,stack: 6});
            return;
          }else if(data["Has"] == false){
            $.toast({ heading: "FAILD",text: data["message"],position: 'top-right',loaderBg:'#ff6849',icon: 'warning',hideAfter: 3500,stack: 6});
            return;
          }

        });
      });
    });//end modal

  });
}
