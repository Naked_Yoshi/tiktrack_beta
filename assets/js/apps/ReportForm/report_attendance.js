$(document).ready(function () {
    $(".select2").select2();
    $("#site").sitegetall();
    $("#r_site").sitegetall();
});

$("#report_selectType select").change(function(event) {
    var type = $(this).val();
    if (type == 0) {
        $("#by_person").show();
        $("#by_site").hide();
        $("#by_dep").hide();
    }else if(type == 1){
         $("#by_person").hide();
         $("#by_site").show();
         $("#by_dep").hide();
    }
 });

 $("#r_site").change(function(){
    $("#index_site").val(this.value);
  
    $("#r_branche").branchgetall("r_site");
  });

    (function ($){
    $.fn.sitegetall = function (){
      var site = $(this);
  
      var url = getApi("sites/getall");
          $.ajax({
            url: url,
            type: 'GET',
            headers: {'Authorization': token}
          }).done(function(data) {
            var no = 1;
  
            if (data["Has"] == false) {
            // log("data (site)=", data);
  
            }else{
              // log("data (site)=", data);
                 var r = "<option value='0'>";
                 r += "เลือกไซต์";
                 r += "</option>";
                for (var i = 0; i < data["data"].length; i++) {
                  r += "<option value='"+ data["data"][i]["site_id"] +"'>";
                  r += data["data"][i]["siteName"];
                  r += "</option>";
  
               }//loop
              site.append(r);
  
            } //else
          });//done
  
      }
        })(jQuery);

    (function ($){
        $.fn.branchgetall = function (chk){
          var branch = $(this);
    
          if (chk == "r_site") {
            var siteval = $("#r_site").val();
          }else{
            var siteval = $("#site").val();
          }
          var url = getApi("Branches/getall");
    
              $.ajax({
                url: url,
                type: 'GET',
                headers: {'Authorization': token},
                data: {"site_id" : siteval}
              }).done(function(data) {
                console.log(data);
                var no = 1;
    
    
                if (data["Has"] == false) {
                  branch.html("");
                  var r = "";
                  r += "<option value='0'>";
                  r += "ไม่มีสาขา";
                  r += "</option>";
                  branch.append(r);
    
                }else{
                  branch.html("");
                  var r = "";
                  r += "<option value='A'>";
                  r += "ทั้งหมด";
                  r += "</option>";
                for (var i = 0; i < data["data"].length; i++) {
                    r += "<option value='"+ data["data"][i]["branche_id"] +"'>";
                    r += data["data"][i]["brancheName"];
                    r += "</option>";
    
                 }//loop
                  branch.append(r);
    
                } //else
              });//done
    
              $("#r_branche").change(function(){
                $("#index_branche").val(this.value);
              });
        }
        })(jQuery);