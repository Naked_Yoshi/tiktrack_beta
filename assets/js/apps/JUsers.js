var token = window.localStorage.getItem('token');
$(document).ready(function() {
  $("#users_table").users();

});	 //inactiveuser

function userEdit(el){
  var edit = $(el).data("empedit");
  var urlEdit = base_url("users/Edit/"+edit);
  window.location = urlEdit;
}

function userDelete(el){
  var del = $(el).data("empdelete");
  console.log(del);
  var urlinactive = getApi("users/inactiveuser");
  swal({
      title: 'Are you sure?',
      text: "ต้องการยกเลิกพนักงานหรือไม่",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'ตกลง',
      cancelButtonText: 'ยกเลิก'
    }).then((result) => {
      if (result.value) {
        $.ajax({
          url: urlinactive,
          type: 'POST',
          headers: {'Authorization': token},
          data: {"no":del}
        }).done(function(data) {
          if (data) {
            $.toast({ heading: "SUCCESS",text: "ยกเลิกข้อมูลเรียบร้อยแล้ว",position: 'top-right',loaderBg:'#ff6849',icon: 'success',hideAfter: 3500,stack: 6});
            $("#users_table").dataTable().fnDestroy();
            $("#users_table").users();
          }else{
            $.toast({heading: "FAILED",text: "ไม่สามารถเพิ่มได้ โปรดตรวจสอบ",position: 'top-right',loaderBg:'#ff6849',icon: 'error',hideAfter: 3500,stack: 6});
          }
        });
      }
    })
}

$(".btn-create-user").click(function(){
    var url = base_url('users/add');
    $.ajax({
      url: url,
      type: 'POST',
    })
    .done(function(data) {

      $(".modal-area").html(data);
      $("#userCreate").modal('toggle');
      $("#usrcreateTitle").getTiltle();
      $("#usrcreateNation").nation();
      $("#usrcreateDep").createdepartment();
      $("#usrcreateDep").change(function(){
          $("#usrcreatePosition").createpositions();
      })


      $('.mydatepicker, #datepicker').datepicker({
          autoclose: true,
          todayHighlight: true,
          clearBtn:true,
          endDate:"0d",
          language: 'th'
      });

      $("#usrcreateEmail").focusout(function(){
        var chk_email = $("#usrcreateEmail").val();
        $.ajax({
          url: getApi("users/chkDupEmail"),
          type: 'GET',
          data : {"email" : chk_email}
        }).done(function(data) {
          if (data == false) {
            $("#form_email").removeClass("has-success");
            $("#form_email").addClass("has-error");
            $("#lbl_email_message").show();
          }else{
            $("#form_email").removeClass("has-error");
            $("#form_email").addClass("has-success");
            $("#lbl_email_message").hide();
          }
        });
      });

      $("#usrcreatePhone").focusout(function(){
        var chk_phone = $("#usrcreatePhone").val().toString().replace(/-/g, '');
        $.ajax({
          url: getApi("users/chkDupPhone"),
          type: 'GET',
          data : {"phone" : chk_phone}
        }).done(function(data) {
          if (data == false) {
            $("#form_phone").removeClass("has-success");
            $("#form_phone").addClass("has-error");
            $("#lbl_phone_message").show();
          }else{
            $("#form_phone").removeClass("has-error");
            $("#form_phone").addClass("has-success");
            $("#lbl_phone_message").hide();
          }
        });
      });

      $(".btn-create").click(function(){
        $('.btn-create').addClass('disabled');
        // $('.btn-create').addAttr('disabled');
         var input = $(".usrCreateForm input.validate");
         input.validate_blank();
         var bs = $("#usrcreateBdate").val().split("/");
         var b_date = bs[2]+"-"+bs[1]+"-"+bs[0];

         var ws = $("#usrcreateStart").val().split("/");
         var ws_date = ws[2]+"-"+ws[1]+"-"+ws[0];

         var phone = $("#usrcreatePhone").val().toString().replace(/-/g, '');
         var ciz_id = $("#usrcreatePersonId").val().toString().replace(/-/g, '');


         req = {
          		"firstname":$("#usrcreateFname").val(),
          		"lastname":$("#usrcreateLname").val(),
          		"firstname_en" : $("#usrcreateFname_en").val(),
          		"lastname_en" : $("#usrcreateLname_en").val(),
          		"gender" : $("#usrcreateSex").val(),
          		"ciz_id" : ciz_id,
          		"birthday":b_date,
          		"phoneNumber" : phone,
          		"email" :$("#usrcreateEmail").val(),
          		"totalHolidays" : $("#usrcreateHoliday").val(),
          		"married":$("#usrcreateMarried").val(),
          		"salary" : $("#usrcreateSalary").val(),
          		"startworkdate" : ws_date,
          		"probation" :"0000-00-00",
          		"nation_id" :$("#usrcreateNation").val(),
          		"position_id" : $("#usrcreatePosition").val(),
          		"dep_id" : $("#usrcreateDep").val(),
          		"title_id" : $("#usrcreateTitle").val()
         };

         var fullname = $("#usrcreateFname_en").val()+" "+$("#usrcreateLname_en").val();
         // console.log(req);
         var urladduser = getApi("users/addnewuser");
         $.ajax({
           url: urladduser,
           type: 'POST',
           headers: {'Authorization': token},
           data: {"request":req}
         })
         .done(function(data) {
            var urlsendmail = getApiOld("users/sendconfirmemail");
           if (data["status"]) {

             var mailbody = "<div style=' font-family:Verdana, Geneva, sans-serif; padding:10px; box-shadow:0px 1px 3px #ccc; height:400px;' align='center'>";
             mailbody += "<p><h1 style='color:#f44336;'>Tik Track</h1></p>";
             mailbody += "<p><h4>Best service for time attendance solution.</h1></h4>";
             mailbody += "<hr />";
             mailbody += "<div align='left' style='padding:0px 20px;'>";
             mailbody += "<p><b>สวัสดีคุณ "+ $("#usrcreateFname_en").val() +"</b></p>";
             mailbody += "<p>คุณได้รับคำเชิญเข้าร่วมการเข้าใช้ระบบลงเวลา Tiktrack</p>";
             mailbody += "<p>Username : "+ data["email"] +" </p>";
             mailbody += "<p>Password : "+ data["password"] +" </p> <br />";
             mailbody += "<p align='center'>";
             mailbody += "<a href='"+base_url()+"'";
             mailbody += "style='text-decoration:none;";
             mailbody += "background:#ef5350;";
             mailbody += "color:#fff;";
             mailbody += "border:none;";
             mailbody += "transition: .5s;";
             mailbody += "cursor:pointer;";
             mailbody += "border-bottom: solid 2px #ef5350;";
             mailbody += "padding:25px 15px;border-radius:10px;width:100%;'>";
             mailbody += "เข้าสู่ระบบ";
             mailbody += "</a>";
             mailbody += "</p>";
             mailbody += "</div>";

             $.ajax({
               url: urlsendmail,
               type: 'POST',
               headers: {'Authorization': token},
               data: {"email":$("#usrcreateEmail").val(),"body":mailbody,"name":fullname,"title":"TIK TRACK"}
             })
             .done(function(data) {
               if (data) {
                 $.toast({ heading: "SUCCESS",text: "เพิ่มพนักงานเรียบร้อยแล้ว",position: 'top-right',loaderBg:'#ff6849',icon: 'success',hideAfter: 3500,stack: 6});
                 $('.btn-create').removeClass('disabled');
                 $('.btn-create').removeAttr('disabled');
                 $("#userCreate").modal("toggle");
                 $("#users_table").dataTable().fnDestroy();
                 $("#users_table").users();
               }else{
                 $.toast({heading: "FAILED",text: "ไม่สามารถส่งอีเมล์แจ้ง โปรดตรวจสอบ",position: 'top-right',loaderBg:'#ff6849',icon: 'error',hideAfter: 3500,stack: 6});
               }
             });
           }else{
             $.toast({heading: "FAILED",text: "ไม่สามารถเพิ่มได้ โปรดตรวจสอบ",position: 'top-right',loaderBg:'#ff6849',icon: 'error',hideAfter: 3500,stack: 6});
           }
         });
      });
    });
});

(function($){
  $.fn.nation = function(){
    var urlnation = base_url("users/c_getnations");
    $.ajax({
      url: urlnation,
      type: 'GET'
    }).done(function(data) {
      var obj = JSON.parse(data);
      console.log(obj);
      var r = "<option value='0'>";
      r += "Default select";
      r += "</option>";
      for (var i = 0; i < obj.length; i++) {
        r += "<option value='"+ obj[i]["nation_id"] +"'>";
        r += obj[i]["nation_name"];
        r += "</option>";
      }
    $("#usrcreateNation").append(r);
    });
  }
})(jQuery);

(function($){
  $.fn.createdepartment = function(){
    var urldep = getApi("department/getall");
    $.ajax({
      url: urldep,
      type: 'GET',
      headers: {'Authorization': token}
    }).done(function(data) {
      var r = "<option value='0'>";
      r += "Default select";
      r += "</option>";
      for (var i = 0; i < data["data"].length; i++) {
        r += "<option value='"+ data["data"][i]["dep_id"] +"'>";
        r += data["data"][i]["dep_name"];
        r += "</option>";
      }
    $("#usrcreateDep").append(r);
    });
  }
})(jQuery);

(function($){
  $.fn.createpositions = function(){
    var dep_id = $("#usrcreateDep").val();
    var urlposition = getApi("position/getall");
    $.ajax({
      url: urlposition,
      type: 'GET',
      headers: {'Authorization': token},
      data:{'dep_id':dep_id}
    }).done(function(data) {
      console.log(data);
      $("#usrcreatePosition").html("");
      var r = "<option value='0'>";
      r += "Default select";
      r += "</option>";
      for (var i = 0; i < data["data"].length; i++) {
        r += "<option value='"+ data["data"][i]["position_id"] +"'>";
        r += data["data"][i]["position_name"];
        r += "</option>";
      }
      $("#usrcreatePosition").append(r);
    });
  }
})(jQuery);

(function($){
  var title = [];
  $.fn.users = function(){
    var table = $(this);
    var userBody = $("#user_body");
    var url = getApi("users/getall");
    $.ajax({
      url: url,
      type: 'GET',
      headers: {'Authorization': token}
    }).done(function(data) {
      var no = 1;
      if (data["Has"] == false) {
        var tablesusers = "<tr>";
        tablesusers += "<td colspan='7' class='text-center'>ไม่พบข้อมูล";
        tablesusers += "</td>";
        tablesusers += "</tr>";
        userBody.append(tablesusers);
      }else{
        userBody.html("");
          for (var i = 0; i < data["data"].length; i++) {
            var tablesusers = "<tr>";
               tablesusers += "<td  class='text-center'>";
               tablesusers += no;
               tablesusers += "</td>";
               tablesusers += "<td>";
               tablesusers += data["data"][i]["fullname"];
               tablesusers += "</td>";
               tablesusers += "<td>";
               tablesusers += data["data"][i]["phoneNumber"];
               tablesusers += "</td>";
               tablesusers += "<td>";
               tablesusers += data["data"][i]["siteName"];
               tablesusers += "</td>";
               tablesusers += "<td>";
               tablesusers += data["data"][i]["departmentName"];
               tablesusers += "</td>";
               tablesusers += "<td>";
               tablesusers += data["data"][i]["positionName"];
               tablesusers += "</td>";
               tablesusers += "<td class='text-center'>";
               tablesusers += "<div class='btn-group'>";
               tablesusers += "<button class='btn btn-sm btn-default btn-outline waves-effect btn-emp-site-edit' onclick='userEdit(this);' data-empedit='"+ window.btoa(window.btoa(window.btoa(data["data"][i]["employee_id"]))) +"'><span class='fa fa-edit'></span></button>";
               tablesusers += "<button class='btn btn-sm btn-default btn-outline waves-effect btn-emp-site-delete' onclick='userDelete(this);' data-empdelete='"+ data["data"][i]["no"] +"'><span class='fa fa-trash'></span></button>";
               tablesusers += "</div>";
               tablesusers += "</td>";
               tablesusers += "</tr>";
               userBody.append(tablesusers);
               // emp_list.push(data["data"][i]["employee_id"]);
               no ++;

        }
        table.DataTable(dataTableTh);
        // $(".btn-emp-site-edit").click(function(){
        //
        // });
      }

    });
  }
})(jQuery);

(function($){
  var title = [];
  $.fn.getTiltle = function(){
     var itemTitle = $(this);
     var url = base_url("items/optionTitle");
     $.ajax({
       url: url,
       type: 'POST',
     })
     .done(function(data) {
       var obj = JSON.parse(data);
           title = obj["title"];
       for (var i = 0; i < obj["options"].length; i++) {
            itemTitle.append(obj["options"][i]);
       }
       $("#usrcreateTitle_en").setTitleEng(title[itemTitle[0].selectedIndex]);
     });
     itemTitle.change(function(event) {
        var index = $(this).context.selectedIndex;
        $("#usrcreateTitle_en").setTitleEng(title[index]);
     });
  }
  $.fn.setTitleEng = function(option){
      $(this).val(option["title_en"]);
  }
})(jQuery);
