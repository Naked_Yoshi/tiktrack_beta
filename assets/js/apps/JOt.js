var empOptions;
var positionWorkingDelOT = [];
var positionweekendDelOT = [];
var positionHolidayDelOT = [];

var excessADD = [];
var excessEDIT = [];
var excessDEL = [];
$(document).ready(function() {
    $("#ot_body").Ot();

});

$(".btn-ot-creategroup").click(function(){
    var url = base_url("ots/modalCreate");
    var segment = $("#working_table").data("segment");
    $.ajax({
      url: url,
      type: 'POST',
      data: {"segment" : segment}
    })
    .done(function(data) {
        $(".modal-area").html(data);
        $("#otCreate").modal("toggle");
        $(".position_ot").getpositions();
        empOptions = $(".emp_ot");
        //modal show
        $("#check_beforeRequest").change(function(event) {
            if ($(this).is(":checked")) {
                $("#request_before").show();
            }else{
                $("#request_before").hide();
            }
        });
        $("#check_approveRequest").change(function(event) {
            if ($(this).is(":checked")) {
                $("#request_approve").show();
            }else{
                $("#request_approve").hide();
            }
        });
        // checked
        $(".btn-create-exc-row").click(function(event) {
            $("#ExTable").createExcRows();
        });

        $('.clockpicker').clockpicker({placement: 'top',align: 'left',autoclose: true,'default': 'now',clear:true}).keypress(function(event) {return false;});

        $(".btn-cancel-ot-time").click(function(){
          var id = $(this).data("id");
          $("#"+id).val("");
        });

        $(".btn-step").click(function(){
           var content = $(this).data("phase");
           var persent = $(this).data("persent");
           var locate = [];

           if (typeof $(this).data("progress") !== "undefined") {
              if ( $(this).data("progress") == "success") {
                  $(".tabs-progress .progress-bar ").removeClass("progress-bar-danger").addClass("progress-bar-"+$(this).data("progress"));
              }else{
                  $(".tabs-progress .progress-bar ").removeClass("progress-bar-success").addClass("progress-bar-"+$(this).data("progress"));
              }
           }
           var detail = check_ot_detail();

           if(content == "ot_excess"){
                if (!detail) {
                    return;
                }

           }



           if (content == "ot_multiple") {
                 if (!detail) {
                     return;
                 }
                 var excess = checke_ot_excess();
                 if (!excess) {
                    return;
                 }
           }



           $(".ot_tabs_content section").removeClass('content-current');
           $("#"+content).addClass('content-current');
           $(".tabs-progress .progress-bar ").css({"width":persent});
           $(".ot_tabs li").removeClass('tab-current')
           $("#"+content+"_tab").addClass('tab-current');

         });
         $(".position_ot").change(function(){
             var id = $(this).data("id");
             var value = parseFloat($(this).val());
             // if (value != 0) {
             //    var hastag = $("#"+id+"_label_"+value).length;
             //    if (hastag < 1) {
             //        var text = $("#"+id+" :selected").text();
             //        var label = "<span class='label label-position-ot label-warning "+id+"-label' id='"+id+"_label_"+value+"' data-value='"+value+"'>"+text+" <a onclick='rm_position_ot(this);' data-id='"+id+"_label_"+value+"'><span class='fa fa-close'></span></a> </span>";
             //        // $("#"+id+"_tag").append(label);
             //    }
             // }

             urlempname = getApi("OT/getApproversFromPosition");

             // console.log(empOptions);
             $.ajax({
               url: urlempname,
               type: 'GET',
               headers: {'Authorization': token},
               data: {"position_id" : value}
             })
             .done(function(data) {
              empOptions = $("#"+id+"Name");
              empOptions.html("");
              var op = "<option value='0'>";
              op += "เลือกผู้อนุมัติ";
              op += "</option>";

             for (var i = 0; i < data["data"].length; i++) {
               op+= "<option value='"+ data["data"][i]["employee_id"] +"'>";
               op+= data["data"][i]["fullname"];
               op+= "</option>";
             }
              empOptions.append(op);
              empOptions.select2();
              console.log(op);
         });
       });

       $(".emp_ot").change(function(){
         var id = $(this).data("id");
         var value = $(this).val();
         if (value != 0) {
            var hastag = $("#"+id+"_label_"+value).length;
            if (hastag < 1) {
                var position_id = $("#"+id.substring(0,id.length - 4)+" :selected").val();
                var position = $("#"+id.substring(0,id.length - 4)+" :selected").text();
                var text = $("#"+id+" :selected").text();
                var label = "<span class='label label-position-ot label-warning "+id+"-label' id='"+id+"_label_"+value+"' data-value='"+value+"' data-position_id='"+position_id+"' style='display: inline-block;'>"+position+" : "+text+" <a onclick='rm_position_ot(this);' data-id='"+id+"_label_"+value+"'><span class='fa fa-close'></span></a> </span>";
                $("#"+id+"_tag").append(label);
            }
          }
       });


         $(".btn-ot-submit").click(function(){
             var groupName = $("#groupOteName").val();
             var groupBefore = $("#groupOtBefore").val();
             var groupAfter = $("#groupOtAfter").val();
             var dateOtLimit = $("#dateOtLimit").val();
             var monthOtLimit = $("#monthOtLimit").val();
             var weekend_ot_start = $("#weekend_ot_started").val();
             var weekend_ot_end = $("#weekend_ot_ended").val();
             var check_beforeRequest = 0;
             var timeBeforeRequest = 0;
             var urgent = 0;
             var check_approveRequest = 0;
             var paidDate = 0;
             var check_packageRequest = 0;
             if ($("#check_beforeRequest").is(":checked")) {
                 check_beforeRequest = 1;
                 timeBeforeRequest = $("#timeBeforeRequest").val();
             }
             if ($("#urgent").is(':checked')) {
                 urgent = 1;
             }
             if ($("#check_approveRequest").is(':checked')) {
                check_approveRequest = 1;
                paidDate = $("#paidDate").val();
             }
             if ($("#check_packageRequest").is(':checked')) {
                check_packageRequest = 1;
             }
             // var working = $("#positionWorking_text");
             var weekend = $("#positionweekend_text");
             var holiday = $("#positionHoliday_text");

             var working_ex = $("#positionWorking_text_ex");
             var weekend_ex = $("#positionweekend_text_ex");
             var holiday_ex = $("#positionHoliday_text_ex");
             // if(!working.validate_blank() || !working_ex.validate_blank()){
             //    $.toast({heading:"ผิดพลาด !",text: "กรอกอัตราคูณวันทำงานปกติ",position: 'top-right',loaderBg:'#ff6849',icon: 'error',hideAfter: 3500,stack: 6});
             //    return;
             // }
             if(!weekend.validate_blank() || !weekend_ex.validate_blank()){
                $.toast({heading:"ผิดพลาด !",text: "กรอกอัตราคูณวันหยุดประจำสัปดาห์",position: 'top-right',loaderBg:'#ff6849',icon: 'error',hideAfter: 3500,stack: 6});
                return;
             }
             if(!holiday.validate_blank() || !holiday.validate_blank()){
                $.toast({heading:"ผิดพลาด !",text: "กรอกอัตราคูณวันหยุดประจำปี",position: 'top-right',loaderBg:'#ff6849',icon: 'error',hideAfter: 3500,stack: 6});
                return;
             }
             var exBody = $("#ExcBody tr").not(".no-list");
                  var exJson = [];
                  for (var i = 0; i < exBody.length; i++) {
                       var exTd = exBody[i].childNodes;
                       var exRow = {};
                       for (var td = 0; td < (exTd.length-1); td++) {
                            var inputValue = exTd[td].children[0].children[0].value;
                            if (td == 0) {
                                exRow.condition = inputValue;
                            }else if (td == 1) {
                                exRow.excess = inputValue;
                            }else if (td == 2) {
                                exRow.result = inputValue;
                            }

                       }
                       exJson.push(exRow);
                  }
             var working_tag = $("#positionWorkingName_tag .label");
             if(working_tag.length < 1){
                $("#positionWorkingName").validate();
                $.toast({heading:"ผิดพลาด !",text: "กรุณาเพิ่มตำแหน่งการอนุมัติ",position: 'top-right',loaderBg:'#ff6849',icon: 'error',hideAfter: 3500,stack: 6});
                return;
             }
             var weekend_tag = $("#positionweekendName_tag .label");
             if(weekend_tag.length < 1){
                $("#positionweekendName").validate();
                $.toast({heading:"ผิดพลาด !",text: "กรุณาเพิ่มตำแหน่งการอนุมัติ",position: 'top-right',loaderBg:'#ff6849',icon: 'error',hideAfter: 3500,stack: 6});
                return;
             }
             var holiday_tag = $("#positionHolidayName_tag .label");
             if(holiday_tag.length < 1){
                $("#positionHolidayName").validate();
                $.toast({heading:"ผิดพลาด !",text: "กรุณาเพิ่มตำแหน่งการอนุมัติ",position: 'top-right',loaderBg:'#ff6849',icon: 'error',hideAfter: 3500,stack: 6});
                return;
             }
             var working_tag_json = [];
             var weekend_tag_json = [];
             var holiday_tag_json = [];
             for (var i = 0; i < working_tag.length; i++) {
                  working_tag_json.push({"position_id": working_tag[i].dataset["position_id"], "employee_id": working_tag[i].dataset["value"]});
             }
             for (var i = 0; i < weekend_tag.length; i++) {
                  weekend_tag_json.push({"position_id": weekend_tag[i].dataset["position_id"], "employee_id": weekend_tag[i].dataset["value"]});
             }
             for (var i = 0; i < holiday_tag.length; i++) {
                  holiday_tag_json.push({"position_id": holiday_tag[i].dataset["position_id"], "employee_id": holiday_tag[i].dataset["value"]});
             }

            var tempbranche = 0
             if (chk_oper_branche == 1) {
               tempbranche = window.atob(window.atob(window.atob(branche_id)));
             }

             var formData = {
                 "groupName":groupName,
                 "groupBefore":groupBefore,
                 "groupAfter":groupAfter,
                 "check_beforeRequest":check_beforeRequest,
                 "timeBeforeRequest":timeBeforeRequest,
                 "urgent":urgent,
                 "check_approveRequest":check_approveRequest,
                 "paidDate":paidDate,
                 "check_packageRequest":check_packageRequest,
                 "working" : 0,
                 "working_ex" : working_ex.val(),
                  "working_tag" : {
                           "add" :	working_tag_json
                         },
                 "weekend" :weekend.val(),
                 "weekend_ex" : weekend_ex.val(),
                 "ot_weekend_start" : weekend_ot_start,
                 "ot_weekend_end" : weekend_ot_end,
                 "weekend_tag" : {
                           "add" :	weekend_tag_json
                         },
                 "holiday" :holiday.val(),
                 "holiday_ex" : holiday_ex.val(),
                 "holiday_tag" : {
                           "add" :	holiday_tag_json
                         },
                 "excess": {
                            "add" :	exJson
                          },
                 "site_id" : window.atob(window.atob(window.atob(site_id))),
                 "branche_id" : tempbranche

             }
             console.log(formData);
             var url = getApi("OT/add");
             $.ajax({
               url: url,
               type: 'POST',
               data: {"request" : formData},
               headers: {'Authorization': token}
             })
             .done(function(data) {
               if (data) {
                   $.toast({ heading: "SUCCESS",text: "เพิ่มกลุ่มล่วงเวลาเรียบร้อยแล้ว",position: 'top-right',loaderBg:'#ff6849',icon: 'success',hideAfter: 3500,stack: 6});
                   $("#otCreate").modal("toggle");
                   $("#ot_table").dataTable().fnDestroy();
                   $("#ot_body").Ot();
               }else{
                   $.toast({heading: "FAILED",text: "ล้มเหลว โปรดตรวจสอบ",position: 'top-right',loaderBg:'#ff6849',icon: 'error',hideAfter: 3500,stack: 6});
               }
             });

         });

    });
    // ajax done
});

function rm_position_ot(ev){
         $("#"+ev.dataset["id"]).remove();
         var evStr = ev.dataset["otp_no"];
         var evId = ev.dataset["id"];
         var myarr = evId.split("_");

         if(typeof evStr !== "undefined"){
           if (myarr[0].slice(0,-4) == "positionWorking") {
             positionWorkingDelOT.push(evStr);
           }else if(myarr[0].slice(0,-4) == "positionweekend"){
             positionweekendDelOT.push(evStr);
           }else if(myarr[0].slice(0,-4) == "positionHoliday"){
             positionHolidayDelOT.push(evStr);
           }
         }

         // console.log(arrDelTag);
         console.log(positionHolidayDelOT);
         // arrDelDelOT
}

function check_ot_detail(){
  var input_detail = $("#ot_detail input.validate");
  var id_vb = input_detail.validate_blank();

  if (!id_vb) {
      $.toast({ heading:"ผิดพลาด",text:"กรุณากรอกรายละเอียดให้ครบถ้วน",position: 'top-right',loaderBg:'#ff6849',icon: 'error',hideAfter: 3500,stack: 6});
      return false;
  }else{
      if ($("#check_beforeRequest").is(":checked")) {
          if(!$("#timeBeforeRequest").validate_blank()){
              $.toast({ heading:"ผิดพลาด",text:"กรุณากรอกฟิลด์ขอล่วงเวลา",position: 'top-right',loaderBg:'#ff6849',icon: 'error',hideAfter: 3500,stack: 6});
                return false;
          }
      }
      if ($("#check_approveRequest").is(":checked")) {
          if(!$("#paidDate").validate_blank()){
              $.toast({ heading:"ผิดพลาด",text:"กรุณากรอกฟิลด์กำหนดวันจ่าย",position: 'top-right',loaderBg:'#ff6849',icon: 'error',hideAfter: 3500,stack: 6});
                return false;
          }
      }
      return true;
  }
}


function checke_ot_excess(){
    var body = $("#ExcBody tr").not(".no-list");
    if (body.length < 1) {
        return true;
    }else{
      for (var i = 0; i < body.length; i++) {
           var row = body[i].dataset["row"];
           var exc = $("#txtexcess_"+row);
           var round = $("#txtround_"+row);
           var result = $("#txtresult_"+row);
           if (exc.val() == "") {
               exc.validate();
               $.toast({ heading:"ผิดพลาด",text:"กรุณากรอกข้อมูลให้ครบถ้วน",position: 'top-right',loaderBg:'#ff6849',icon: 'error',hideAfter: 3500,stack: 6});
               return false;
           }
           if (result.val() == "") {
               result.validate();
               $.toast({ heading:"ผิดพลาด",text:"กรุณากรอกข้อมูลให้ครบถ้วน",position: 'top-right',loaderBg:'#ff6849',icon: 'error',hideAfter: 3500,stack: 6});
               return false;
           }
           var result_value = parseFloat(result.val());
           var exc_value  = parseFloat(exc.val());
           if(parseInt(round.val()) < 1){
               if(exc_value >= result_value){
                 $.toast({ heading:"ผิดพลาด",text:"ผลลัพท์ต้องมีมากกว่าเศษนาที",position: 'top-right',loaderBg:'#ff6849',icon: 'error',hideAfter: 3500,stack: 6});
                  result.validate();
                  return false;
               }
          }else if(parseInt(round.val()) < 2){
                if(exc_value <= result_value){
                  $.toast({ heading:"ผิดพลาด",text:"ผลลัพท์ต้องมีน้อยว่าเศษนาที",position: 'top-right',loaderBg:'#ff6849',icon: 'error',hideAfter: 3500,stack: 6});
                   result.validate();
                   return false;
                }
         }else if(parseInt(round.val()) < 3){
              if(exc_value > result_value){
                $.toast({ heading:"ผิดพลาด",text:"ผลลัพท์ต้องมีมากกว่าเท่ากับเศษนาที",position: 'top-right',loaderBg:'#ff6849',icon: 'error',hideAfter: 3500,stack: 6});
                 result.validate();
                 return false;
              }
         }else if(parseInt(round.val()) < 4){
               if(exc_value < result_value){
                 $.toast({ heading:"ผิดพลาด",text:"ผลลัพท์ต้องมีน้อยว่าเท่ากับเศษนาที",position: 'top-right',loaderBg:'#ff6849',icon: 'error',hideAfter: 3500,stack: 6});
                  result.validate();
                  return false;
               }
        }
    }
      return true;
    }

}

(function($){
  $.fn.getpositions = function(){
    var positionOption = $(this);
    var url = getApi("position/getall");

    $.ajax({
      url:url,
      type:"GET",
      headers: {'Authorization': token}
    })
    .done(function(data) {
      if (data["Has"] == false) {
        var op = "<option value='0'>";
        op += "ไม่พบ";
        op += "</option>";
        positionOption.append(op);
      }else{
        var op = "<option value='0'>";
        op += "เลือกผู้อนุมัติ";
        op += "</option>";

       for (var i = 0; i < data["data"].length; i++) {
         op+= "<option value='"+ data["data"][i]["position_id"] +"'>";
         op+= data["data"][i]["position_name"];
         op+= "</option>";
      }
      positionOption.append(op);
      positionOption.select2();
     }
   });
  }
})(jQuery);

(function($){
    $.fn.Ot = function(){
      var otBody = $(this);
      var segment = $("#ot_table").data("segment");
      var branche = $("#ot_table").data("branche");
      var segments      = location.pathname.split('/');
      if (chk_oper_branche == 1) {
        site_id = segments[segments.length - 2];
        branche_id = segments[segments.length - 1];
        branche_id = window.atob(window.atob(window.atob(branche_id)));
      }else{
        site_id = segments[segments.length - 1];
        branche_id = 0;
      }
      if (typeof branche === "undefined") {
          branche = "";
      }
      if (typeof segment === "undefined") {
          segment = "";
      }
      var url = getApi("OT/getall");
      $.ajax({
        url: url,
        type: 'GET',
        headers: {'Authorization': token},
        data: {"site_id": window.atob(window.atob(window.atob(site_id))),"branche_id": branche_id }
      })
      .done(function(data) {
        var no = 1;
        if (data["Has"] == false) {

        }else{
                otBody.html("");
              for (var i = 0; i < data["data"].length; i++) {
                var tablesot = "<tr>";
                   tablesot += "<td  class='text-center'>";
                   tablesot += no;
                   tablesot += "</td>";
                   tablesot += "<td>";
                   tablesot += data["data"][i]["ot_name"];
                   tablesot += "</td>";
                   tablesot += "<td class='text-center'>";
                   tablesot += "<div class='btn-group'>";
                   tablesot += "<button class='btn btn-sm btn-default btn-outline waves-effect btn-ot-edit' data-edit='"+ data["data"][i]["ot_id"] +"' onclick='ot_edit(this)'><span class='fa fa-edit'></span></button>";
                   // tablesot += "<button class='btn btn-sm btn-default btn-outline waves-effect btn-ot-delete' data-delete='"+ data["data"][i]["ot_id"] +"'><span class='fa fa-trash'></span></button>";
                   tablesot += "</div>";
                   tablesot += "</td>";
                   tablesot += "</tr>";
                   otBody.append(tablesot);
                   no++;
              }
         $("#ot_table").DataTable(dataTableTh);
       }
      })
      .fail(function() {
        console.log("error");
      })
      .always(function() {
        console.log("complete");
      });

    }
})(jQuery);

function ot_edit(el){
  var segments      = location.pathname.split('/'),
  site_id = segments[segments.length - 1];
  var edit = $(el).data("edit");
  var url = base_url("ots/modalEdit");
  var ot_id;
  $.ajax({
    url: url,
    type: 'POST',
    data: {"segment" : edit}
  })
  .done(function(data) {
    $(".modal-area").html(data);
    $("#otEdit").modal("toggle");
    $(".position_ot").getpositions();

    $("#check_beforeRequest").change(function(event) {
        if ($(this).is(":checked")) {
            $("#request_before").show();
        }else{
            $("#request_before").hide();
        }
    });

     var urledit = getApi("OT/getby");
     positionWorkingDelOT = [];
     positionweekendDelOT = [];
     positionHolidayDelOT = [];
     console.log(positionHolidayDelOT);
     excessDEL = [];
      $.ajax({
        url: urledit,
        type: 'GET',
        headers: {'Authorization': token},
        data: {"id" : edit}
      }).done(function(data) {
        ot_id = data["data"][0]["ot_id"];
        $("#groupOteName").val(data["data"][0]["ot_name"]);
        $("#groupOtBefore").val(data["data"][0]["ot_before_minute"]);
        $("#groupOtAfter").val(data["data"][0]["ot_after_minute"]);
        $("#positionWorking_text").val(data["data"][0]["ot_working"]);
        $("#positionweekend_text").val(data["data"][0]["ot_weekend"]);
        $("#positionHoliday_text").val(data["data"][0]["ot_holiday"]);
        $("#positionWorking_text_ex").val(data["data"][0]["ot_working_ex"]);
        $("#positionweekend_text_ex").val(data["data"][0]["ot_weekend_ex"]);
        $("#positionHoliday_text_ex").val(data["data"][0]["ot_holiday_ex"]);
        $("#weekend_ot_started").val(data["data"][0]["ot_weekend_start"]);
        $("#weekend_ot_ended").val(data["data"][0]["ot_weekend_end"]);

        if (data["data"][0]["ot_avaliable_time_before"] > 0) {
          $("#check_beforeRequest").prop('checked', true);
          $("#request_before").show();
          $("#timeBeforeRequest").val(data["data"][0]["ot_avaliable_time_before"]);

            if (data["data"][0]["ot_urgent_request"] == 1) {
              $("#urgent").prop('checked', true);
            }

        }

        if (data["data"][0]["ot_package"] == 1) {
          $("#check_packageRequest").prop('checked', true);
        }


        $(".btn-create-exc-row").click(function(event) {
            $("#ExTable").createExcRows();
        });

        $('.clockpicker').clockpicker({placement: 'top',align: 'left',autoclose: true,'default': 'now',clear:true}).keypress(function(event) {return false;});

        $(".btn-cancel-ot-time").click(function(){
          var id = $(this).data("id");
          $("#"+id).val("");
        });

        var urleditexce = getApi("OT/getExcess");
        $.ajax({
          url: urleditexce,
          type: 'GET',
          headers: {'Authorization': token},
          data: {"id" : edit}
        }).done(function(data) {
          var table = $("#ExTable");
          var tr = $("#ExcBody tr");
          // var num = tr.length;
          var id = 1;
          // if (num > 0) {
          //     var id = parseFloat(tr[num-1].dataset["row"]);
          //     id += 1;
          // }
          if (data["Has"] == false) {

          }else{
            $(".no-list").remove();
            for (var i = 0; i < data["data"].length; i++) {
              var r  = "";
                  r += "<tr class='editrow' id='ex_row"+id+"' data-row='"+id+"' data-ote='"+ data["data"][i]["ote_no"] +"'>";
                  r += "<td>";
                  r += "<div class='form-group  has-feedback' id='txtround_"+id+"_group'>";
                  r += "<select class='form-control' id='txtround_"+id+"'>";
                  r += "<option value='0'>มากกว่า > </option>";
                  r += "<option value='1'>น้อยกว่า < </option>";
                  r += "<option value='2'>มากกว่าเท่ากับ >= </option>";
                  r += "<option value='3'>น้อยกว่าเท่ากับ <= </option>";
                  r += "</select>";
                  r += "<span class='form-control-feedback' id='txtround_"+id+"_feedback'></span>";
                  r += "</div>";
                  r += "</td>";
                  r += "<td>";
                  r += "<div class='form-group has-feedback' id='txtexcess_"+id+"_group'>";
                  r += "<input type='text' class='form-control' id='txtexcess_"+id+"' name='' value='" + data["data"][i]["ote_excess"] + "' placeholder='นาที' onkeypress='return numberOnly(event);'>";
                  r += "<span class='form-control-feedback' id='txtexcess_"+id+"_feedback'></span>";
                  r += "</div>";
                  r += "</td>";
                  r += "<td>";
                  r += "<div class='form-group has-feedback' id='txtresult_"+id+"_group'>";
                  r += "<input type='text' class='form-control' id='txtresult_"+id+"' name='' value='" + data["data"][i]["ote_result"] + "' placeholder='นาที' onkeypress='return numberOnly(event);'>";
                  r += "<span class='form-control-feedback' id='txtresult_"+id+"_feedback'></span>";
                  r += "</div>";
                  r += "</td>";
                  r += "<td class='text-right'>";
                  r += "<button type='button' class='btn btn-danger btn-outline'  data-row='"+id+"' data-ote='"+ data["data"][i]["ote_no"] +"' onclick='rm_ex_row(this);'><span class='fa fa-remove'></span></button>";
                  r += "</td>";
                  r += "</tr>";
                  table.append(r);

                  $("#txtround_"+id).val(data["data"][i]["ote_condition"]);
                  excessEDIT.push(data["data"][i]["ote_no"]);
                  id++;
            }
          }
          // console.log(excessEDIT);
        });

        var urleditposi_approve = getApi("OT/getApprovePosition");
        console.log(edit);
        $.ajax({
          url: urleditposi_approve,
          type: 'GET',
          headers: {'Authorization': token},
          data: {"id" : edit}
        }).done(function(data) {
          for (var i = 0; i < data["data"].length; i++) {
            id = data["data"][i]["position_id"];
            value = data["data"][i]["position_id"];
            var emp_id = data["data"][i]["employee_id"];
            var otp_no = data["data"][i]["otp_no"];
            if (data["data"][i]["otp_type"] == 0) {
              var label = "<span class='label label-position-ot label-warning positionWorkingName-label' id='positionWorkingName_label_"+emp_id+"' data-value='"+emp_id+"' data-position_id='"+id+"' style='display: inline-block;' data-chk='not-add'>"+data["data"][i]["position_name"]+" : "+ data["data"][i]["fullname"] +" <a onclick='rm_position_ot(this);' data-id='positionWorkingName_label_"+emp_id+"' data-otp_no='"+ otp_no +"'><span class='fa fa-close'></span></a> </span>";
              $("#positionWorkingName_tag").append(label);
            }else if(data["data"][i]["otp_type"] == 1){
              var label = "<span class='label label-position-ot label-warning positionweekendName-label' id='positionweekendName_label_"+emp_id+"' data-value='"+emp_id+"' data-position_id='"+id+"' style='display: inline-block;' data-chk='not-add'>"+data["data"][i]["position_name"]+" : "+ data["data"][i]["fullname"] +" <a onclick='rm_position_ot(this);' data-id='positionweekendName_label_"+emp_id+"' data-otp_no='"+ otp_no +"'><span class='fa fa-close'></span></a> </span>";
              $("#positionweekendName_tag").append(label);
            }else if(data["data"][i]["otp_type"] == 2){
              var label = "<span class='label label-position-ot label-warning positionHolidayName-label' id='positionHolidayName_label_"+emp_id+"' data-value='"+emp_id+"' data-position_id='"+id+"' style='display: inline-block;' data-chk='not-add'>"+data["data"][i]["position_name"]+" : "+ data["data"][i]["fullname"] +" <a onclick='rm_position_ot(this);' data-id='positionHolidayName_label_"+emp_id+"' data-otp_no='"+ otp_no +"'><span class='fa fa-close'></span></a> </span>";
              $("#positionHolidayName_tag").append(label);
            }
          }

        });
       //EDIT
        $(".position_ot").change(function(){
            var id = $(this).data("id");
            var value = parseFloat($(this).val());
            urlempname = getApi("OT/getApproversFromPosition");

            // console.log(empOptions);
            $.ajax({
              url: urlempname,
              type: 'GET',
              headers: {'Authorization': token},
              data: {"position_id" : value}
            })
            .done(function(data) {
             empOptions = $("#"+id+"Name");
             empOptions.html("");
             var op = "<option value='0'>";
             op += "เลือกตำแหน่ง";
             op += "</option>";

            for (var i = 0; i < data["data"].length; i++) {
              op+= "<option value='"+ data["data"][i]["employee_id"] +"'>";
              op+= data["data"][i]["fullname"];
              op+= "</option>";
            }
             empOptions.append(op);
             empOptions.select2();
             console.log(op);
        });
      });
      //EDIT
      $(".emp_ot").change(function(){
        var id = $(this).data("id");
        var value = $(this).val();
        if (value != 0) {
           var hastag = $("#"+id+"_label_"+value).length;
           if (hastag < 1) {
               var position_id = $("#"+id.substring(0,id.length - 4)+" :selected").val();
               var position = $("#"+id.substring(0,id.length - 4)+" :selected").text();
               var text = $("#"+id+" :selected").text();
               var label = "<span class='label label-position-ot label-warning "+id+"-label' id='"+id+"_label_"+value+"' data-value='"+value+"' data-position_id='"+position_id+"' style='display: inline-block;'>"+position+" : "+text+" <a onclick='rm_position_ot(this);' data-id='"+id+"_label_"+value+"'><span class='fa fa-close'></span></a> </span>";
               $("#"+id+"_tag").append(label);
               console.log(id);
           }
         }
      });

          $(".btn-step").click(function(){
            var content = $(this).data("phase");
            var persent = $(this).data("persent");
            var locate = [];
            console.log("test2222");
            if (typeof $(this).data("progress") !== "undefined") {
               if ( $(this).data("progress") == "success") {
                   $(".tabs-progress .progress-bar ").removeClass("progress-bar-danger").addClass("progress-bar-"+$(this).data("progress"));
               }else{
                   $(".tabs-progress .progress-bar ").removeClass("progress-bar-success").addClass("progress-bar-"+$(this).data("progress"));
               }
            }
            var detail = check_ot_detail();

            if(content == "ot_excess"){
                 if (!detail) {
                     return;
                 }

            }

            if (content == "ot_multiple") {
                  if (!detail) {
                      return;
                  }
                  var excess = checke_ot_excess();
                  if (!excess) {
                     return;
                  }
            }



            $(".ot_tabs_content section").removeClass('content-current');
            $("#"+content).addClass('content-current');
            $(".tabs-progress .progress-bar ").css({"width":persent});
            $(".ot_tabs li").removeClass('tab-current')
            $("#"+content+"_tab").addClass('tab-current');
          });


      });

      $(".btn-ot-submit").click(function(){
          var groupName = $("#groupOteName").val();
          var groupBefore = $("#groupOtBefore").val();
          var groupAfter = $("#groupOtAfter").val();
          var dateOtLimit = $("#dateOtLimit").val();
          var monthOtLimit = $("#monthOtLimit").val();
          var weekend_ot_start = $("#weekend_ot_started").val();
          var weekend_ot_end = $("#weekend_ot_ended").val();
          var check_beforeRequest = 0;
          var timeBeforeRequest = 0;
          var urgent = 0;
          var check_approveRequest = 0;
          var paidDate = 0;
          var check_packageRequest = 0;
          if ($("#check_beforeRequest").is(":checked")) {
              check_beforeRequest = 1;
              timeBeforeRequest = $("#timeBeforeRequest").val();
          }
          if ($("#urgent").is(':checked')) {
              urgent = 1;
          }
          if ($("#check_approveRequest").is(':checked')) {
             check_approveRequest = 1;
             paidDate = $("#paidDate").val();
          }
          if ($("#check_packageRequest").is(':checked')) {
             check_packageRequest = 1;
          }
          // var working = $("#positionWorking_text");
          var weekend = $("#positionweekend_text");
          var holiday = $("#positionHoliday_text");
          var working_ex = $("#positionWorking_text_ex");
          var weekend_ex = $("#positionweekend_text_ex");
          var holiday_ex = $("#positionHoliday_text_ex");
          // if(!working.validate_blank()){
          //    $.toast({heading:"ผิดพลาด !",text: "กรอกอัตราคูณวันทำงานปกติ",position: 'top-right',loaderBg:'#ff6849',icon: 'error',hideAfter: 3500,stack: 6});
          //    return;
          // }
          if(!weekend.validate_blank()){
             $.toast({heading:"ผิดพลาด !",text: "กรอกอัตราคูณวันหยุดประจำสัปดาห์",position: 'top-right',loaderBg:'#ff6849',icon: 'error',hideAfter: 3500,stack: 6});
             return;
          }
          if(!holiday.validate_blank()){
             $.toast({heading:"ผิดพลาด !",text: "กรอกอัตราคูณวันหยุดประจำปี",position: 'top-right',loaderBg:'#ff6849',icon: 'error',hideAfter: 3500,stack: 6});
             return;
          }
          var exBody = $("#ExcBody tr").not(".no-list");
               var exJson = [];
               var exEditJson = [];
               for (var i = 0; i < exBody.length; i++) {
                    var exTr = exBody[i];
                    if ($(exTr).hasClass("editrow")) {
                      var exTd = exBody[i].childNodes;
                      var exRow = {};
                      exRow.ote_no = exTr.dataset["ote"];
                      for (var td = 0; td < (exTd.length-1); td++) {
                           var inputValue = exTd[td].children[0].children[0].value;
                           if (td == 0) {
                               exRow.condition = inputValue;
                           }else if (td == 1) {
                               exRow.excess = inputValue;
                           }else if (td == 2) {
                               exRow.result = inputValue;
                           }

                      }
                      exEditJson.push(exRow);
                    }else{
                      var exTd = exBody[i].childNodes;
                      var exRow = {};
                      for (var td = 0; td < (exTd.length-1); td++) {
                           var inputValue = exTd[td].children[0].children[0].value;
                           if (td == 0) {
                               exRow.condition = inputValue;
                           }else if (td == 1) {
                               exRow.excess = inputValue;
                           }else if (td == 2) {
                               exRow.result = inputValue;
                           }

                      }
                      exJson.push(exRow);
                    };
               }
          var working_tag = $("#positionWorkingName_tag .label");
          if(working_tag.length < 1){
             $("#positionWorkingName").validate();
             $.toast({heading:"ผิดพลาด !",text: "กรุณาเพิ่มตำแหน่งการอนุมัติ",position: 'top-right',loaderBg:'#ff6849',icon: 'error',hideAfter: 3500,stack: 6});
             return;
          }
          var weekend_tag = $("#positionweekendName_tag .label");
          if(weekend_tag.length < 1){
             $("#positionweekendName").validate();
             $.toast({heading:"ผิดพลาด !",text: "กรุณาเพิ่มตำแหน่งการอนุมัติ",position: 'top-right',loaderBg:'#ff6849',icon: 'error',hideAfter: 3500,stack: 6});
             return;
          }
          var holiday_tag = $("#positionHolidayName_tag .label");
          if(holiday_tag.length < 1){
             $("#positionHolidayName").validate();
             $.toast({heading:"ผิดพลาด !",text: "กรุณาเพิ่มตำแหน่งการอนุมัติ",position: 'top-right',loaderBg:'#ff6849',icon: 'error',hideAfter: 3500,stack: 6});
             return;
          }
          var working_tag_json = [];
          var weekend_tag_json = [];
          var holiday_tag_json = [];
          for (var i = 0; i < working_tag.length; i++) {
            if (working_tag[i].dataset["chk"] == "not-add") {
              //null
            }else{
              working_tag_json.push({"position_id": working_tag[i].dataset["position_id"], "employee_id": working_tag[i].dataset["value"]});
            }
          }
          for (var i = 0; i < weekend_tag.length; i++) {
            if (weekend_tag[i].dataset["chk"] == "not-add") {
              //null
            }else{
                weekend_tag_json.push({"position_id": weekend_tag[i].dataset["position_id"], "employee_id": weekend_tag[i].dataset["value"]});
            }
          }
          for (var i = 0; i < holiday_tag.length; i++) {
            if (holiday_tag[i].dataset["chk"] == "not-add") {
              //null
            }else{
              holiday_tag_json.push({"position_id": holiday_tag[i].dataset["position_id"], "employee_id": holiday_tag[i].dataset["value"]});
            }
          }
          // console.log(holiday_tag_json);
          var formData = {
              "ot_id" : ot_id,
              "groupName":groupName,
              "groupBefore":groupBefore,
              "groupAfter":groupAfter,
              "timeBeforeRequest":timeBeforeRequest,
              "urgent":urgent,
              "check_beforeRequest":check_beforeRequest,
              "check_approveRequest":check_approveRequest,
              "paidDate":paidDate,
              "check_packageRequest":check_packageRequest,
              "ot_weekend_start" : weekend_ot_start,
              "ot_weekend_end" : weekend_ot_end,
              "working_tag" : {
                       "add" :	working_tag_json,
                       "delete" : positionWorkingDelOT
                     },
             "weekend_tag" : {
                       "add" :	weekend_tag_json,
                       "delete" : positionweekendDelOT
                     },
             "holiday_tag" : {
                       "add" :	holiday_tag_json,
                       "delete" : positionHolidayDelOT
                     },
              "site_id" : window.atob(window.atob(window.atob(site_id))),
              "working" : 0,
              "weekend" : weekend.val(),
              "holiday" : holiday.val(),
              "working_ex" : working_ex.val(),
              "weekend_ex" : weekend_ex.val(),
              "holiday_ex" : holiday_ex.val(),
              "excess": {
                        "add" :	exJson,
                        "edit" : exEditJson,
                        "delete" : excessDEL
                      }

          }
          console.log(formData);
          var url = getApi("OT/update");
          $.ajax({
            url: url,
            type: 'POST',
            headers: {'Authorization': token},
            data: {"request" : formData}
          })
          .done(function(data) {
            if (data) {
                $.toast({ heading: "SUCCESS",text: "แก้ไขเรียบร้อยแล้ว",position: 'top-right',loaderBg:'#ff6849',icon: 'success',hideAfter: 3500,stack: 6});
                $("#otEdit").modal("toggle");
                $("#ot_table").dataTable().fnDestroy();
                $("#ot_body").Ot();
            }else{
                $.toast({heading: "FAILED",text: "พบข้อผิดพลาดโปรดตรวจสอบ",position: 'top-right',loaderBg:'#ff6849',icon: 'error',hideAfter: 3500,stack: 6});
            }
          });

      });
  });//done
}
