$(document).ready(function() {
  $("#working_body").working();
});

(function ($){
  $.fn.working = function(){
    var workingBody = $(this);
    $.ajax({
      url: getApi("workings/getall"),
      type: 'GET',
      headers: {'Authorization': token}
    }).done(function(data){
      if (data["Has"]) {
        workingBody.html("");
        var no = 1;
        for (var i = 0; i < data["data"].length; i++) {
          var r = "<tr>";
           r += "<td class='text-center' >";
           r += no;
           r += "</td>";
           r += "<td>";
           r += data["data"][i]["wk_name"];
           r += "</td>";
           r += "<td class='text-center'><div class='btn-group'>"
           r += "<button class='btn btn-sm btn-default btn-outline waves-effect btn-site-edit' data-edit='"+ data["data"][i]["wk_id"] +"'  onclick='wk_edit(this)'><span class='fa fa-edit'></span></button>";
           r += "<button class='btn btn-sm btn-default btn-outline waves-effect' data-delete='"+ data["data"][i]["wk_id"] +"' onclick='wk_delete(this)'><span class='fa fa-trash'></span></button>";
           r += "</div></td>";
          r += "</tr>";
          no++;
          workingBody.append(r);
        }
      }
      $(".working_table").DataTable(dataTableTh);
    });
  }
})(jQuery);


$(".btn-create-working").click(function(){
  $.ajax({
    url: base_url('CompanySetting/createWorking'),
    type: "POST",
  })
  .done(function(data) {

    $(".modal-area").html(data);
    $("#create_wk_Modal").modal("toggle");

    $("#wk_chk_every").click(function(){
      if ($("#wk_chk_every").is(":checked")) {
        $(".chk-day").prop('checked',true);
      }else{
        $(".chk-day").prop('checked',false);
      }
    });

    $(".chk-day").click(function(){
      // console.log($(this).val());
      var day = $(this).val();
      // console.log(day);
      if ($("#wk_chk_"+day).is(":checked")) {
        $(".chk-wk-"+day).show();
      }else{
        $(".chk-wk-"+day).hide();
      }
    });

    $("#wk_chk_every").click(function(){
      if ($("#wk_chk_every").is(":checked")) {
        $(".wk-all").show();
      }else{
        $(".wk-all").hide();
      }
    });


    $("#chk_time_used_everyday").click(function(){
    if ($("#chk_time_used_everyday").is(":checked")) {
      $(".chk-time-custom").hide();
      $(".chk-time-eve").show();
    }else{
      var work_start = $("#work_start").val();
      var break_start = $("#break_start").val();
      var break_end = $("#break_end").val();
      var work_end = $("#work_end").val();

      $(".chk-time-eve").hide();
      $(".chk-time-custom").show();

      if ($("#wk_chk_monday").is(":checked")) {
        $("#monday_work_start").val(work_start);
        $("#monday_break_start").val(break_start);
        $("#monday_break_end").val(break_end);
        $("#monday_work_end").val(work_end);
      }
      if ($("#wk_chk_tuesday").is(":checked")) {
        $("#tuesday_work_start").val(work_start);
        $("#tuesday_break_start").val(break_start);
        $("#tuesday_break_end").val(break_end);
        $("#tuesday_work_end").val(work_end);
      }
      if ($("#wk_chk_wednesday").is(":checked")) {
        $("#wednesday_work_start").val(work_start);
        $("#wednesday_break_start").val(break_start);
        $("#wednesday_break_end").val(break_end);
        $("#wednesday_work_end").val(work_end);
      }
      if ($("#wk_chk_thuesday").is(":checked")) {
        $("#thuesday_work_start").val(work_start);
        $("#thuesday_break_start").val(break_start);
        $("#thuesday_break_end").val(break_end);
        $("#thuesday_work_end").val(work_end);
      }
      if ($("#wk_chk_friday").is(":checked")) {
        $("#friday_work_start").val(work_start);
        $("#friday_break_start").val(break_start);
        $("#friday_break_end").val(break_end);
        $("#friday_work_end").val(work_end);
      }
      if ($("#wk_chk_saturday").is(":checked")) {
        $("#saturday_work_start").val(work_start);
        $("#saturday_break_start").val(break_start);
        $("#saturday_break_end").val(break_end);
        $("#saturday_work_end").val(work_end);
      }
      if ($("#wk_chk_sunday").is(":checked")) {
        $("#sunday_work_start").val(work_start);
        $("#sunday_break_start").val(break_start);
        $("#sunday_break_end").val(break_end);
        $("#sunday_work_end").val(work_end);
      }

      // if ($("#wk_chk_monday").is(":checked")) {$(".chk-wk-monday").show();}else{$(".chk-wk-monday").hide();}

    }
    });

    $('.clockpicker').clockpicker({placement: 'right',align: 'right',autoclose: true,'default': 'now',clear:true}).keypress(function(event) {return false;});

    $(".apply-all").click(function(event) {
        var input = $("#create_form_wk input").not("#wk_name");
        var input_start = $("#monday_start").val();
        var input_end = $("#monday_end").val();
        for (var i = 2; i < input.length; i++) {
             if (i%2 == 0) {
                input[i].value = input_start;
             }else{
                input[i].value = input_end;
             }
        }
    });

    $(".btn-cancel-time").click(function(){
      var id = $(this).data("id");
      $("#"+id).val("");
    });

    $(".btn-create-wk").click(function(){
      var wk_name = $("#wk_name").val();
      var monday_start = "00:00";
      var monday_break_start = "00:00";
      var monday_break_end = "00:00";
      var monday_end = "00:00";
      var tuesday_start = "00:00";
      var tuesday_break_start = "00:00";
      var tuesday_break_end = "00:00";
      var tuesday_end = "00:00";
      var wednesday_start = "00:00";
      var wednesday_break_start = "00:00";
      var wednesday_break_end = "00:00";
      var wednesday_end = "00:00";
      var thursday_start = "00:00";
      var thursday_break_start = "00:00";
      var thursday_break_end = "00:00";
      var thursday_end = "00:00";
      var friday_start = "00:00";
      var friday_break_start = "00:00";
      var friday_break_end = "00:00";
      var friday_end = "00:00";
      var saturday_start = "00:00";
      var saturday_break_start = "00:00";
      var saturday_break_end = "00:00";
      var saturday_end = "00:00";
      var sunday_start = "00:00";
      var sunday_break_start = "00:00";
      var sunday_break_end = "00:00";
      var sunday_end = "00:00";

      if ($("#chk_time_used_everyday").is(":checked")) {
        if ($("#wk_chk_monday").is(":checked")) {
          monday_start = $("#work_start").val();
          monday_break_start = $("#break_start").val();
          monday_break_end = $("#break_end").val();
          monday_end = $("#work_end").val();
        }
        if ($("#wk_chk_tuesday").is(":checked")) {
          tuesday_start = $("#work_start").val();
          tuesday_break_start = $("#break_start").val();
          tuesday_break_end = $("#break_end").val();
          tuesday_end = $("#work_end").val();
        }
        if ($("#wk_chk_wednesday").is(":checked")) {
          wednesday_start = $("#work_start").val();
          wednesday_break_start = $("#break_start").val();
          wednesday_break_end = $("#break_end").val();
          wednesday_end = $("#work_end").val();
        }
        if ($("#wk_chk_thuesday").is(":checked")) {
          thursday_start = $("#work_start").val();
          thursday_break_start = $("#break_start").val();
          thursday_break_end = $("#break_end").val();
          thursday_end = $("#work_end").val();
        }
        if ($("#wk_chk_friday").is(":checked")) {
          friday_start = $("#work_start").val();
          friday_break_start = $("#break_start").val();
          friday_break_end = $("#break_end").val();
          friday_end = $("#work_end").val();
        }
        if ($("#wk_chk_saturday").is(":checked")) {
          saturday_start = $("#work_start").val();
          saturday_break_start = $("#break_start").val();
          saturday_break_end = $("#break_end").val();
          saturday_end = $("#work_end").val();
        }
        if ($("#wk_chk_sunday").is(":checked")) {
          sunday_start = $("#work_start").val();
          sunday_break_start = $("#break_start").val();
          sunday_break_end = $("#break_end").val();
          sunday_end = $("#work_end").val();
        }
      }else{
        if ($("#wk_chk_monday").is(":checked")) {
          if ($("#monday_work_start").val() != '') {monday_start = $("#monday_work_start").val()};
          if ($("#monday_break_start").val() != '') {monday_break_start = $("#monday_break_start").val()};
          if ($("#monday_break_end").val() != '') {monday_break_end = $("#monday_break_end").val()};
          if ($("#monday_work_end").val() != '') {monday_end = $("#monday_work_end").val()};
        }
        if ($("#wk_chk_tuesday").is(":checked")) {
          if ($("#tuesday_work_start").val() != '') {tuesday_start = $("#tuesday_work_start").val()};
          if ($("#tuesday_break_start").val() != '') {tuesday_break_start = $("#tuesday_break_start").val()};
          if ($("#tuesday_break_end").val() != '') {tuesday_break_end = $("#tuesday_break_end").val()};
          if ($("#tuesday_work_end").val() != '') {tuesday_end = $("#tuesday_work_end").val()};
        }
        if ($("#wk_chk_wednesday").is(":checked")) {
          if ($("#wednesday_work_start").val() != '') {wednesday_start = $("#wednesday_work_start").val()};
          if ($("#wednesday_break_start").val() != '') {wednesday_break_start = $("#wednesday_break_start").val()};
          if ($("#wednesday_break_end").val() != '') {wednesday_break_end = $("#wednesday_break_end").val()};
          if ($("#wednesday_work_end").val() != '') {wednesday_end = $("#wednesday_work_end").val()};
        }
        if ($("#wk_chk_thuesday").is(":checked")) {
          if ($("#thuesday_work_start").val() != '') {thursday_start = $("#thuesday_work_start").val()};
          if ($("#thuesday_break_start").val() != '') {thursday_break_start = $("#thuesday_break_start").val()};
          if ($("#thuesday_break_end").val() != '') {thursday_break_end = $("#thuesday_break_end").val()};
          if ($("#thuesday_work_end").val() != '') {thursday_end = $("#thuesday_work_end").val()};
        }
        if ($("#wk_chk_friday").is(":checked")) {
          if ($("#friday_work_start").val() != '') {friday_start = $("#friday_work_start").val()};
          if ($("#friday_break_start").val() != '') {friday_break_start = $("#friday_break_start").val()};
          if ($("#friday_break_end").val() != '') {friday_break_end = $("#friday_break_end").val()};
          if ($("#friday_work_end").val() != '') {friday_end = $("#friday_work_end").val()};
        }
        if ($("#wk_chk_saturday").is(":checked")) {
          if ($("#saturday_work_start").val() != '') {saturday_start = $("#saturday_work_start").val()};
          if ($("#saturday_break_start").val() != '') {saturday_break_start = $("#saturday_break_start").val()};
          if ($("#saturday_break_end").val() != '') {saturday_break_end = $("#saturday_break_end").val()};
          if ($("#saturday_work_end").val() != '') {saturday_end = $("#saturday_work_end").val()};
        }
        if ($("#wk_chk_sunday").is(":checked")) {
          if ($("#sunday_work_start").val() != '') {sunday_start = $("#sunday_work_start").val()};
          if ($("#sunday_break_start").val() != '') {sunday_break_start = $("#sunday_break_start").val()};
          if ($("#sunday_break_end").val() != '') {sunday_break_end = $("#sunday_break_end").val()};
          if ($("#sunday_work_end").val() != '') {sunday_end = $("#sunday_work_end").val()};
        }
      }

      var req = {
        "wk_name" : wk_name,
        "monday_start" : monday_start,
        "monday_break_start" : monday_break_start,
        "monday_break_end" : monday_break_end,
        "monday_end" : monday_end,
        "tuesday_start" : tuesday_start,
        "tuesday_break_start" : tuesday_break_start,
        "tuesday_break_end" : tuesday_break_end,
        "tuesday_end" : tuesday_end,
        "wednesday_start" : wednesday_start,
        "wednesday_break_start" : wednesday_break_start,
        "wednesday_break_end" : wednesday_break_end,
        "wednesday_end" : wednesday_end,
        "thursday_start" : thursday_start,
        "thursday_break_start" : thursday_break_start,
        "thursday_break_end" : thursday_break_end,
        "thursday_end" : thursday_end,
        "friday_start" : friday_start,
        "friday_break_start" : friday_break_start,
        "friday_break_end" : friday_break_end,
        "friday_end" : friday_end,
        "saturday_start" : saturday_start,
        "saturday_break_start" : saturday_break_start,
        "saturday_break_end" : saturday_break_end,
        "saturday_end" : saturday_end,
        "sunday_start" : sunday_start,
        "sunday_break_start" : sunday_break_start,
        "sunday_break_end" : sunday_break_end,
        "sunday_end" : sunday_end,
      }

      // console.log(req);

      url_add_wk = getApi("workings/add");
      $.ajax({
        url: url_add_wk,
        type: 'POST',
        headers: {'Authorization': token},
        data: {'request' : req}
      }).done(function(data){
        if (data) {
          $.toast({ heading: "SUCCESS",text: "เพิ่มวันทำการเรียบร้อยแล้ว",position: 'top-right',loaderBg:'#ff6849',icon: 'success',hideAfter: 3500,stack: 6});
          $("#create_wk_Modal").modal("toggle");
          $(".working_table").dataTable().fnDestroy();
          $("#working_body").working();
        }else{
          $.toast({heading: "FAILED",text: "พบข้อผิดพลาดโปรดตรวจสอบ",position: 'top-right',loaderBg:'#ff6849',icon: 'error',hideAfter: 3500,stack: 6});
        }

      });
    });


  });
});


function wk_edit(el){
var wk_id = $(el).data("edit");
// console.log(wk_id);
    $.ajax({
      url: base_url('CompanySetting/editworking'),
      type: "POST",
    })
    .done(function(data) {
      $(".modal-area").html(data);
      $("#edit_wk_Modal").modal("toggle");

      $(".chk-time-eve").hide();
      $(".chk-time-custom").show();
      $(".wk-all").show();

      $('.clockpicker').clockpicker({placement: 'right',align: 'right',autoclose: true,'default': 'now',clear:true}).keypress(function(event) {return false;});

      $(".apply-all").click(function(event) {
          var input = $("#create_form_wk input").not("#wk_name");
          var input_start = $("#monday_start").val();
          var input_end = $("#monday_end").val();
          for (var i = 2; i < input.length; i++) {
               if (i%2 == 0) {
                  input[i].value = input_start;
               }else{
                  input[i].value = input_end;
               }
          }
      });

      $(".btn-cancel-time").click(function(){
        var id = $(this).data("id");
        $("#"+id).val("");
      });

      $.ajax({
        url: getApi("workings/getby"),
        type: 'GET',
        headers: {'Authorization': token},
        data: {'id': wk_id}
      }).done(function(data){
        if (data["Has"]) {
          // console.log(data["data"][0]["wk_mon_start"]);
          $("#wk_name").val(data["data"][0]["wk_name"]);
          $("#monday_work_start").val(data["data"][0]["wk_mon_start"].slice(0,-3));
          $("#monday_break_start").val(data["data"][0]["wk_mon_break_start"].slice(0,-3));
          $("#monday_break_end").val(data["data"][0]["wk_mon_break_end"].slice(0,-3));
          $("#monday_work_end").val(data["data"][0]["wk_mon_end"].slice(0,-3));

          $("#tuesday_work_start").val(data["data"][0]["wk_tue_start"].slice(0,-3));
          $("#tuesday_break_start").val(data["data"][0]["wk_tue_break_start"].slice(0,-3));
          $("#tuesday_break_end").val(data["data"][0]["wk_tue_break_end"].slice(0,-3));
          $("#tuesday_work_end").val(data["data"][0]["wk_tue_end"].slice(0,-3));

          $("#wednesday_work_start").val(data["data"][0]["wk_wed_start"].slice(0,-3));
          $("#wednesday_break_start").val(data["data"][0]["wk_wed_break_start"].slice(0,-3));
          $("#wednesday_break_end").val(data["data"][0]["wk_wed_break_end"].slice(0,-3));
          $("#wednesday_work_end").val(data["data"][0]["wk_wed_end"].slice(0,-3));

          $("#thuesday_work_start").val(data["data"][0]["wk_thu_start"].slice(0,-3));
          $("#thuesday_break_start").val(data["data"][0]["wk_thu_break_start"].slice(0,-3));
          $("#thuesday_break_end").val(data["data"][0]["wk_thu_break_end"].slice(0,-3));
          $("#thuesday_work_end").val(data["data"][0]["wk_thu_end"].slice(0,-3));

          $("#friday_work_start").val(data["data"][0]["wk_fri_start"].slice(0,-3));
          $("#friday_break_start").val(data["data"][0]["wk_fri_break_start"].slice(0,-3));
          $("#friday_break_end").val(data["data"][0]["wk_fri_break_end"].slice(0,-3));
          $("#friday_work_end").val(data["data"][0]["wk_fri_end"].slice(0,-3));

          $("#saturday_work_start").val(data["data"][0]["wk_sat_start"].slice(0,-3));
          $("#saturday_break_start").val(data["data"][0]["wk_sat_break_start"].slice(0,-3));
          $("#saturday_break_end").val(data["data"][0]["wk_sat_break_end"].slice(0,-3));
          $("#saturday_work_end").val(data["data"][0]["wk_sat_end"].slice(0,-3));

          $("#sunday_work_start").val(data["data"][0]["wk_sun_start"].slice(0,-3));
          $("#sunday_break_start").val(data["data"][0]["wk_sun_break_start"].slice(0,-3));
          $("#sunday_break_end").val(data["data"][0]["wk_sun_break_end"].slice(0,-3));
          $("#sunday_work_end").val(data["data"][0]["wk_sun_end"].slice(0,-3));
        }
      });

      $(".btn-create-wk").click(function(){

        var wk_name = $("#wk_name").val();
        var monday_start = "00:00";
        var monday_break_start = "00:00";
        var monday_break_end = "00:00";
        var monday_end = "00:00";
        var tuesday_start = "00:00";
        var tuesday_break_start = "00:00";
        var tuesday_break_end = "00:00";
        var tuesday_end = "00:00";
        var wednesday_start = "00:00";
        var wednesday_break_start = "00:00";
        var wednesday_break_end = "00:00";
        var wednesday_end = "00:00";
        var thursday_start = "00:00";
        var thursday_break_start = "00:00";
        var thursday_break_end = "00:00";
        var thursday_end = "00:00";
        var friday_start = "00:00";
        var friday_break_start = "00:00";
        var friday_break_end = "00:00";
        var friday_end = "00:00";
        var saturday_start = "00:00";
        var saturday_break_start = "00:00";
        var saturday_break_end = "00:00";
        var saturday_end = "00:00";
        var sunday_start = "00:00";
        var sunday_break_start = "00:00";
        var sunday_break_end = "00:00";
        var sunday_end = "00:00";

        if ($("#monday_work_start").val() != '') {monday_start = $("#monday_work_start").val()};
        if ($("#monday_break_start").val() != '') {monday_break_start = $("#monday_break_start").val()};
        if ($("#monday_break_end").val() != '') {monday_break_end = $("#monday_break_end").val()};
        if ($("#monday_work_end").val() != '') {monday_end = $("#monday_work_end").val()};

        if ($("#tuesday_work_start").val() != '') {tuesday_start = $("#tuesday_work_start").val()};
        if ($("#tuesday_break_start").val() != '') {tuesday_break_start = $("#tuesday_break_start").val()};
        if ($("#tuesday_break_end").val() != '') {tuesday_break_end = $("#tuesday_break_end").val()};
        if ($("#tuesday_work_end").val() != '') {tuesday_end = $("#tuesday_work_end").val()};

        if ($("#wednesday_work_start").val() != '') {wednesday_start = $("#wednesday_work_start").val()};
        if ($("#wednesday_break_start").val() != '') {wednesday_break_start = $("#wednesday_break_start").val()};
        if ($("#wednesday_break_end").val() != '') {wednesday_break_end = $("#wednesday_break_end").val()};
        if ($("#wednesday_work_end").val() != '') {wednesday_end = $("#wednesday_work_end").val()};

        if ($("#thuesday_work_start").val() != '') {thursday_start = $("#thuesday_work_start").val()};
        if ($("#thuesday_break_start").val() != '') {thursday_break_start = $("#thuesday_break_start").val()};
        if ($("#thuesday_break_end").val() != '') {thursday_break_end = $("#thuesday_break_end").val()};
        if ($("#thuesday_work_end").val() != '') {thursday_end = $("#thuesday_work_end").val()};

        if ($("#friday_work_start").val() != '') {friday_start = $("#friday_work_start").val()};
        if ($("#friday_break_start").val() != '') {friday_break_start = $("#friday_break_start").val()};
        if ($("#friday_break_end").val() != '') {friday_break_end = $("#friday_break_end").val()};
        if ($("#friday_work_end").val() != '') {friday_end = $("#friday_work_end").val()};

        if ($("#saturday_work_start").val() != '') {saturday_start = $("#saturday_work_start").val()};
        if ($("#saturday_break_start").val() != '') {saturday_break_start = $("#saturday_break_start").val()};
        if ($("#saturday_break_end").val() != '') {saturday_break_end = $("#saturday_break_end").val()};
        if ($("#saturday_work_end").val() != '') {saturday_end = $("#saturday_work_end").val()};

        if ($("#sunday_work_start").val() != '') {sunday_start = $("#sunday_work_start").val()};
        if ($("#sunday_break_start").val() != '') {sunday_break_start = $("#sunday_break_start").val()};
        if ($("#sunday_break_end").val() != '') {sunday_break_end = $("#sunday_break_end").val()};
        if ($("#sunday_work_end").val() != '') {sunday_end = $("#sunday_work_end").val()};

        var req = {
          "wk_id" : wk_id,
          "wk_name" : wk_name,
          "monday_start" : monday_start,
          "monday_break_start" : monday_break_start,
          "monday_break_end" : monday_break_end,
          "monday_end" : monday_end,
          "tuesday_start" : tuesday_start,
          "tuesday_break_start" : tuesday_break_start,
          "tuesday_break_end" : tuesday_break_end,
          "tuesday_end" : tuesday_end,
          "wednesday_start" : wednesday_start,
          "wednesday_break_start" : wednesday_break_start,
          "wednesday_break_end" : wednesday_break_end,
          "wednesday_end" : wednesday_end,
          "thursday_start" : thursday_start,
          "thursday_break_start" : thursday_break_start,
          "thursday_break_end" : thursday_break_end,
          "thursday_end" : thursday_end,
          "friday_start" : friday_start,
          "friday_break_start" : friday_break_start,
          "friday_break_end" : friday_break_end,
          "friday_end" : friday_end,
          "saturday_start" : saturday_start,
          "saturday_break_start" : saturday_break_start,
          "saturday_break_end" : saturday_break_end,
          "saturday_end" : saturday_end,
          "sunday_start" : sunday_start,
          "sunday_break_start" : sunday_break_start,
          "sunday_break_end" : sunday_break_end,
          "sunday_end" : sunday_end,
        }

        // console.log(req);

        url_add_wk = getApi("workings/edit");
        $.ajax({
          url: url_add_wk,
          type: 'POST',
          headers: {'Authorization': token},
          data: {'request' : req}
        }).done(function(data){
          if (data) {
            $.toast({ heading: "SUCCESS",text: "แก้ไขวันทำการเรียบร้อยแล้ว",position: 'top-right',loaderBg:'#ff6849',icon: 'success',hideAfter: 3500,stack: 6});
            $("#edit_wk_Modal").modal("toggle");
            $(".working_table").dataTable().fnDestroy();
            $("#working_body").working();
          }else{
            $.toast({heading: "FAILED",text: "พบข้อผิดพลาดโปรดตรวจสอบ",position: 'top-right',loaderBg:'#ff6849',icon: 'error',hideAfter: 3500,stack: 6});
          }

        });

      });
    });
}

function wk_delete(el){
  var wk_id = $(el).data("delete");

  swal({
  title: 'โปรดยืนยัน',
  text: "คุณต้องการยกเลิกวันทำการหรือไม่",
  type: 'warning',
  showCancelButton: true,
  confirmButtonColor: '#3085d6',
  cancelButtonColor: '#d33',
  confirmButtonText: 'Yes, delete it!'
}).then((result) => {
  if (result.value) {

    $.ajax({
      url: getApi("workings/del"),
      type: 'GET',
      headers: {'Authorization': token},
      data: {"id" : wk_id}
    }).done(function(data){
      if (data) {
        $.toast({ heading: "SUCCESS",text: "ยกเลิกเรียบร้อยแล้ว",position: 'top-right',loaderBg:'#ff6849',icon: 'success',hideAfter: 3500,stack: 6});
        $(".working_table").dataTable().fnDestroy();
        $("#working_body").working();
      }else{
        $.toast({heading: "FAILED",text: "พบข้อผิดพลาดโปรดตรวจสอบ",position: 'top-right',loaderBg:'#ff6849',icon: 'error',hideAfter: 3500,stack: 6});
      }
    });
  }
})
}
