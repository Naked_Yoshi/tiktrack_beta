var token = window.localStorage.getItem('token');
var company_name = "";

function edit_position (el){
    var position_id = $(el).data("position_id");
               console.log(position_id);
            var url = base_url("Positions/ModalEdit");
            $.ajax({
              url: url,
              type: 'POST',
            }).done(function(data) {
                $(".modal-area").html(data);
                $("#EditPos").modal("toggle");
                  // $("#positionRoot").positionOptions();
                   // data : $("#EditPosForm").serialize()
                var url = getApi("Position/getBy");
                $.ajax({
                  url: url,
                  type: 'GET',
                  headers: {'Authorization': token},
                  data: {"root": position_id}
                })
                .done(function(data) {

                    $("#positionDep").depOptions(data["detail"][0]["dep_id"]);

                   $("#positionRoot").positionOptions(data["detail"][0]["position_root"]);

                  $("#pos_name_label").html(data["detail"][0]["position_name"]);
                  $("#positionName").val(data["detail"][0]["position_name"]);
                  $("#positionDep_id").val(data["detail"][0]["dep_id"]);
                    $("#posdep").val(data["detail"][0]["dep_id"]);

                    $("#positionDep").click(function(event){
                      // console.log("test");
                       $("#positionRoot").positionOptions();
                       // $("#positionRoot").val("0");
                    });

                  var site_add = data["detail"][0]["add_site"];
                   if (site_add  == 1 ) {$('#siteAdd').attr('checked', true).val(site_add); }
                  var site_edit = data["detail"][0]["edit_site"] ;
                   if (site_edit  == 1 ) {  $('#siteEdit').attr('checked', true).val(site_edit);  }
                  var site_delete =  data["detail"][0]["delete_site"];
                  if (site_delete ==1 ){$("#siteDelete").attr('checked', 'checked').val(site_delete); }
                   var site_Approve = data ["detail"][0]["approve_site"];
                   if (site_Approve  == 1 ) {  $('#siteApprove').attr('checked', 'checked').val(site_Approve);  }
                var add_B  =  data["detail"][0]["add_branche"] ;
                   if (add_B  == 1 ) {  $("#brancheAdd").attr("checked", "checked").val(add_B);  }
                  var edit_B =  data["detail"][0]["edit_branche"] ;
              if (edit_B  == 1 ) {  $('#brancheEdit').attr('checked', 'checked').val(edit_B);  }
            var delete_B =  data["detail"][0]["delete_branche"];
           if (delete_B  == 1 ) {  $('#brancheDel').attr('checked', 'checked').val(delete_B);  }
             var approve_B  = data["detail"][0]["approve_branche"];
               if (approve_B  == 1 ) {  $('#brancheApprove').attr('checked', 'checked').val(approve_B);  }
               var ot_Add  = data["detail"][0]["add_ot"];
                if (ot_Add  == 1 ) {  $('#otAdd').attr('checked', 'checked').val(ot_Add);  }
                var ot_Edit  = data["detail"][0]["edit_ot"];
                if (ot_Edit  == 1 ) {  $('#otEdit').attr('checked', 'checked').val(ot_Edit);  }
                var ot_Del  = data["detail"][0]["delete_ot"];
                 if (ot_Del  == 1 ) {  $('#otDel').attr('checked', 'checked').val(ot_Del);  }
                 var ot_Approve  = data["detail"][0]["approve_ot"];
                  if (ot_Approve  == 1 ) {  $('#otApprove').attr('checked', 'checked').val(ot_Approve);  }
                  var add_user  = data["detail"][0]["add_user"];
                   if (add_user  == 1 ) {  $('#userAdd').attr('checked', 'checked').val(add_user);  }
                   var user_Edit  = data["detail"][0]["edit_user"];
                    if (user_Edit  == 1 ) {  $('#userEdit').attr('checked', 'checked').val(user_Edit);  }
                    var user_Del  = data["detail"][0]["delete_user"];
                     if (user_Del  == 1 ) {  $('#userDel').attr('checked', 'checked').val(user_Del);  }
                     var checkinout  = data["detail"][0]["checkinout"];
                     if (checkinout  == 1 ) {  $('#checkinout').attr('checked', 'checked').val(checkinout);  }
                });
                // การแก้ไข้ข้อมูลตำแหน่ง

                $(".submit-edit-position").click(function(){
                     var url = getApi("position/updatePermission");
                    var pos_name = $("#positionName").val();
                  var pos_dep = $("#positionDep").val();
                    var pos_root = $("#positionRoot").val();


                    var siteAdd = 0;
                    var siteEdit = 0;
                    var siteDel = 0;
                    var siteApprove = 0;
                    var brancheAdd = 0;
                    var brancheEdit = 0;
                    var brancheDel = 0;
                    var brancheApprove = 0;
                    var otAdd = 0;
                    var otEdit = 0;
                    var otDel = 0;
                    var otApprove = 0;
                    var userAdd = 0;
                    var userEdit = 0;
                    var userDel = 0;
                    var checkinout = 0;

                    if ($('#siteAdd').is(':checked')) {
                      siteAdd = 1;
                    }
                    if ($('#siteEdit').is(':checked')) {
                      siteEdit = 1;
                    }
                    if ($('#siteDelete').is(':checked')) {
                      siteDel = 1;
                    }
                    if ($('#siteApprove').is(':checked')) {
                      siteApprove = 1;
                    }
                    if ($('#brancheAdd').is(':checked')) {
                      brancheAdd = 1;
                    }
                    if ($('#brancheEdit').is(':checked')) {
                      brancheEdit = 1;
                    }
                    if ($('#brancheDel').is(':checked')) {
                      brancheDel = 1;
                    }
                    if ($('#brancheApprove').is(':checked')) {
                      brancheApprove = 1;
                    }
                    if ($('#otAdd').is(':checked')) {
                      otAdd = 1;
                    }
                    if ($('#otEdit').is(':checked')) {
                      otEdit = 1;
                    }
                    if ($('#otDel').is(':checked')) {
                      otDel = 1;
                    }
                    if ($('#otApprove').is(':checked')) {
                      otApprove = 1;
                    }
                    if ($('#userAdd').is(':checked')) {
                      userAdd = 1;
                    }
                    if ($('#userEdit').is(':checked')) {
                      userEdit = 1;
                    }
                    if ($('#userDel').is(':checked')) {
                      userDel = 1;
                    }
                    if ($('#checkinout').is(':checked')) {
                      checkinout = 1;
                    }

                    if ($("#positionDep").val() == "0") {
                      $.toast({heading: "WARNING!!!",text: "โปรดเลือกแผนก",position: 'top-right',loaderBg:'#ff6849',icon: 'warning',hideAfter: 3500,stack: 6});
                      return;
                    }

                    if ($("#positionName").val().charAt(0) == " ") {
                      console.log(dep_name);
                      $.toast({heading: "WARNING",text: "ไม่สามารถบันทึกได้ เนื่องจากชื่อไม่ถูกต้อง",position: 'top-right',loaderBg:'#ff6849',icon: 'warning',hideAfter: 3500,stack: 6});
                      return;
                    }

                    var  req_updatePermission = {
                              "position_id" :position_id ,
                              "dep":pos_dep,
                              "root" : pos_root,
                              "positionName": pos_name,
                              "siteAdd":  siteAdd,
                              "siteEdit": siteEdit,
                              "siteDelete": siteDel,
                              "siteApprove": siteApprove,
                              "brancheAdd": brancheAdd,
                              "brancheEdit": brancheEdit,
                              "brancheDel": brancheDel,
                              "brancheApprove": brancheApprove,
                              "otAdd": otAdd,
                              "otEdit": otEdit,
                              "otDel": otDel,
                              "otApprove": otApprove,
                              "userAdd" :userAdd,
                              "userEdit" :userEdit,
                              "userDelete" :userDel,
                              "checkinout" : checkinout
                    }


                        // console.log( req_updatePermission);
                    // console.log(dep_id+" "+dep_name+" "+dep_desc);

                    $.ajax({
                      url: url,
                      type: 'POST',
                      headers: {'Authorization': token},
                      data: {"request": req_updatePermission}

                    })
                    .done(function(data) {
                      if (data) {
                        $.toast({heading: "SUCCESS",text: "อัพเดทข้อมูลเรียบร้อยแล้ว",position: 'top-right',loaderBg:'#ff6849',icon: 'success',hideAfter: 3500,stack: 6});
                       $(".close").trigger("click");
                       $(".position_table").dataTable().fnDestroy();
                      $("#position_body").positions();

                      getOrg("","chart_div");
                     // (function() {
                     //   [].slice.call(document.querySelectorAll('.sttabs')).forEach(function(el) {
                     //       new CBPFWTabs(el);
                     //   });
                     // })();
                      }else{
                          swal("แก้ไขข้อมูลเรียบร้อย !", "", "success");
                      }

                    });
                });

  // จบการแก้ไขข้อมูลแผนก
            });
        // });
}

function inactive_position(el){
  var position_id = $(el).data("position_id");
  swal({title: 'คุณต้องการลบข้อมูลตำแหน่ง ?',  type: 'warning',showCancelButton: true,confirmButtonColor: '#3085d6',cancelButtonColor: '#d33',confirmButtonText: 'ตกลง !',cancelButtonText: 'ยกเลิก'})
  .then((result) => { if (result.value) {
  var url = getApi("position/inactive");
    $.ajax({
    url: url,
    type: 'POST',
    headers: {'Authorization': token},
    data: {"position_id":position_id}
    }).done(function(data) {
    if (data["Has"]) {
      if (data["code"] == 102) {
        $.toast({heading: "FAILED",text: data["message"],position: 'top-right',loaderBg:'#ff6849',icon: 'warning',hideAfter: 3500,stack: 6});
      }else{
        $.toast({heading: "SUCCESS",text: data["message"],position: 'top-right',loaderBg:'#ff6849',icon: 'success',hideAfter: 3500,stack: 6});
        $(".close").trigger("click");
         // $("#position_body").html("");
        $("#position_table").dataTable().fnDestroy();
        $("#position_body").positions();

        getOrg("","chart_div");
       // (function() {
       //   [].slice.call(document.querySelectorAll('.sttabs')).forEach(function(el) {
       //       new CBPFWTabs(el);
       //   });
       // })();
      }
    }else{
       swal("ไม่สามารถลบข้อมูลได้  !", "", "error");
    }

  });
  }
    });
}

function edit_dep(el){
  var dep_id = $(el).data("dep_id");
    var url = base_url('departments/ModalEdit');
    $.ajax({
      url: url,
      type: 'POST',
    }).done(function(data) {
        $(".modal-area").html(data);
        $("#EditDev").modal("toggle");
        console.log(dep_id);
          var url = getApi("department/getby");
          $.ajax({
            url: url,
            type: 'GET',
            headers: {'Authorization': token},
            data: {"dep_id":dep_id}
          })
          .done(function(data) {
            $("#dep_name_label").html(data["data"][0]["dep_name"]);
            $("#depcreateName").val(data["data"][0]["dep_name"]);
            $("#depcreateDesc").val(data["data"][0]["dep_caption"]);
          });

        $(".submit-edit-dep").click(function(){
            var url = getApi("department/update");
            var dep_name = $("#depcreateName").val();
            var dep_desc = $("#depcreateDesc").val();

            if (dep_name.charAt(0) == " " || dep_name == "") {
              console.log(dep_name);
              $.toast({heading: "WARNING",text: "ไม่สามารถบันทึกได้ เนื่องจากชื่อไม่ถูกต้อง",position: 'top-right',loaderBg:'#ff6849',icon: 'warning',hideAfter: 3500,stack: 6});
              return;
            }

            var  req = {
                      "dep_id":dep_id,
                      "depName" :dep_name ,
                       "depDesc" : dep_desc
            }
            console.log(req);
            console.log(dep_id+" "+dep_name+" "+dep_desc);
            $.ajax({
              url: url,
              type: 'POST',
              headers: {'Authorization': token},
              data: {"request": req}
            })
            .done(function(data) {
              if (data) {
                $.toast({heading: "SUCCESS",text: "แก้ไขข้อมูลเรียบร้อยแล้ว",position: 'top-right',loaderBg:'#ff6849',icon: 'success',hideAfter: 3500,stack: 6});
                $(".close").trigger("click");
                $(".departments_table").dataTable().fnDestroy();
                $("#departments_body").departments();

              }else{
                  swal("แก้ไขข้อมูลเรียบร้อย !", "", "success");
              }


            });
        });
    });
}

function delete_dep(el){
  var dep_id = $(el).data("dep_id");
    // console.log(dep_id);
  swal({title: 'ลบข้อมูล' ,text: "คุณต้องการลบข้อมูลแผนก ใช่หรือไม่ ?", type: 'warning',showCancelButton: true,confirmButtonColor: '#3085d6',cancelButtonColor: '#d33',confirmButtonText: 'ใช่ !',cancelButtonText: 'ไม่'})
  .then((result) => { if (result.value) {
  var url = getApi("department/inactive");
  $.ajax({
  url: url,
  type: 'POST',
  headers: {'Authorization': token},
  data: {"dep_id":dep_id}
  })
  // console.log(dep_id);
  .done(function(data) {
  // console.log(dep_id);
  if (data["Has"]) {
    if (data["code"] == 102 || data["code"] == 103) {
      $.toast({heading: "FAILED",text: data["message"],position: 'top-right',loaderBg:'#ff6849',icon: 'warning',hideAfter: 3500,stack: 6});
    }else{
      $.toast({heading: "SUCCESS",text: data["message"],position: 'top-right',loaderBg:'#ff6849',icon: 'success',hideAfter: 3500,stack: 6});
      $(".close").trigger("click");
      $(".departments_table").dataTable().fnDestroy();
      $("#departments_body").departments();
    }
  }else{
  swal("ไม่สามารถลบข้อมูลได้  !", "", "error");
  }

  });
  }
  });
}

$(document).ready(function() {
   getOrg("","chart_div");
  (function() {
    [].slice.call(document.querySelectorAll('.sttabs')).forEach(function(el) {
        new CBPFWTabs(el);
    });
  })();
  $("#departments_body").departments();
  $("#position_body").positions();

  // $("#chart_body").chart();
});
$(".btn-create-dep").click(function(event) {
    var url = base_url('departments/ModalCreate');
    $.ajax({
      url: url,
      type: 'POST',
    }).done(function(data) {
        $(".modal-area").html(data);
        $("#depCreate").modal("toggle");
        $(".submit-create-dep").click(function(){
            var in_validate = $("#depCreateForm .validate").validate_blank();
            if (in_validate) {
                // var created = $("#depCreateForm").createDepartment();
                var dep_name = $("#depcreateName").val();
                var dep_desc = $("#depcreateDesc").val();

                if (dep_name.charAt(0) == " ") {
                  console.log(dep_name);
                  $.toast({heading: "WARNING",text: "ไม่สามารถบันทึกได้ เนื่องจากชื่อไม่ถูกต้อง",position: 'top-right',loaderBg:'#ff6849',icon: 'warning',hideAfter: 3500,stack: 6});
                  return;
                }

                var req = {
                  "depName" : dep_name,
                  "depDesc" : dep_desc
                }

                var departmentAdd = getApi("department/add");
                $.ajax({
                  url: departmentAdd,
                  type: 'POST',
                  headers: {'Authorization': token},
                  data:{"request" : req}
                }).done(function(data) {
                  if (data) {
                    $("#depCreate").modal("toggle");
                    $.toast({heading: "SUCCESS",text: "เพิ่มแผนกเรียบร้อยแล้ว",position: 'top-right',loaderBg:'#ff6849',icon: 'success',hideAfter: 3500,stack: 6});
                    $(".departments_table").dataTable().fnDestroy();
                    $("#departments_body").departments();
                  }else{
                    swal("พบข้อผิดพลาด !", "", " error");
                  }
                });
            }

        });
    });
});



$(".btn-create-position").click(function(){
    var url = base_url('positions/ModalCreate');
    $.ajax({
      url: url,
      type: 'POST',
    }).done(function(data) {
        $(".modal-area").html(data);
        $("#positionCreate").modal("toggle");
        $("#positionDep").depOptions();

        $("#positionDep").click(function(event){
          // console.log("1111");
           $("#positionRoot").positionOptions();
             });
        $(".submit-create-position").click(function(){
            var in_validate =  $("#positionCreateForm .validate").validate_blank();
            if (in_validate) {
                var created = $("#positionCreateForm").createPosition();
            }
            // input.validate_blank();
        });
    });
});


(function ($){
  $.fn.createDepartment = function (){
    var url = base_url("departments/create");
    $.ajax({
      url: url,
      type: 'POST',
      data: $(this).serialize()
    })
    .done(function(data) {
         var obj = JSON.parse(data);
         if (obj["success"]) {
             $.toast({
               heading: obj["title"],
               text: obj["msg"],
               position: 'top-right',
               loaderBg:'#ff6849',
               icon: 'success',
               hideAfter: 3500,
               stack: 6
             });
              $("#depCreate").modal("toggle");
              $(".departments_table").dataTable().fnDestroy();
              $("#departments_body").departments();
         }else{
           $.toast({
             heading: obj["title"],
             text: obj["msg"],
             position: 'top-right',
             loaderBg:'#ff6849',
             icon: 'error',
             hideAfter: 3500,
             stack: 6
           });
         }
    });
  }

  $.fn.createPosition = function (){
    var siteAdd = 0;
    var siteEdit = 0;
    var siteDel = 0;
    var siteApprove = 0;
    var brancheAdd = 0;
    var brancheEdit = 0;
    var brancheDel = 0;
    var brancheApprove = 0;
    var otAdd = 0;
    var otEdit = 0;
    var otDel = 0;
    var otApprove = 0;
    var userAdd = 0;
    var userEdit = 0;
    var userDel = 0;
    var checkinout = 0;

    if ($('#siteAdd').is(':checked')) {
      siteAdd = 1;
    }
    if ($('#siteEdit').is(':checked')) {
      siteEdit = 1;
    }
    if ($('#siteDel').is(':checked')) {
      siteDel = 1;
    }
    if ($('#siteApprove').is(':checked')) {
      siteApprove = 1;
    }
    if ($('#brancheAdd').is(':checked')) {
      brancheAdd = 1;
    }
    if ($('#brancheEdit').is(':checked')) {
      brancheEdit = 1;
    }
    if ($('#brancheDel').is(':checked')) {
      brancheDel = 1;
    }
    if ($('#brancheApprove').is(':checked')) {
      brancheApprove = 1;
    }
    if ($('#otAdd').is(':checked')) {
      otAdd = 1;
    }
    if ($('#otEdit').is(':checked')) {
      otEdit = 1;
    }
    if ($('#otDel').is(':checked')) {
      otDel = 1;
    }
    if ($('#otApprove').is(':checked')) {
      otApprove = 1;
    }
    if ($('#userAdd').is(':checked')) {
      userAdd = 1;
    }
    if ($('#userEdit').is(':checked')) {
      userEdit = 1;
    }
    if ($('#userDel').is(':checked')) {
      userDel = 1;
    }
    if ($('#checkinout').is(':checked')) {
      checkinout = 1;
    }

    if ($("#positionDep").val() == "0") {
      $.toast({heading: "WARNING!!!",text: "โปรดเลือกแผนก",position: 'top-right',loaderBg:'#ff6849',icon: 'warning',hideAfter: 3500,stack: 6});
      return;
    }

    if ($("#positionName").val().charAt(0) == " ") {
      console.log(dep_name);
      $.toast({heading: "WARNING",text: "ไม่สามารถบันทึกได้ เนื่องจากชื่อไม่ถูกต้อง",position: 'top-right',loaderBg:'#ff6849',icon: 'warning',hideAfter: 3500,stack: 6});
      return;
    }

    var req = {
      "dep" : $("#positionDep").val(),
      "positionName" : $("#positionName").val(),
      "root" : $("#positionRoot").val(),
      "siteAdd" : siteAdd,
      "siteEdit": siteEdit,
      "siteDelete" : siteDel,
      "siteApprove" : siteApprove,
      "brancheAdd" : brancheAdd,
      "brancheEdit" : brancheEdit,
      "brancheDel" : brancheDel,
      "brancheApprove" : brancheApprove,
      "otAdd" : otAdd,
      "otEdit" : otEdit,
      "otDel" : otDel,
      "otApprove" : otApprove,
      "userAdd" : userAdd,
      "userEdit" : userEdit,
      "userDelete" : userDel,
      "checkinout" : checkinout
    };

    // console.log(req);

    var url = getApi("position/add");
    $.ajax({
      url: url,
      type: 'POST',
      headers: {'Authorization': token},
      data:{'request' : req}
      // data: $(this).serialize()
    })
    .done(function(data) {
      if (data) {
          $.toast({ heading: "SUCCESS",text: "เพิ่มเรียบร้อยแล้ว",position: 'top-right',loaderBg:'#ff6849',icon: 'success',hideAfter: 3500,stack: 6});
           $("#positionCreate").modal("toggle");
           $(".position_table").dataTable().fnDestroy();
           $("#position_body").positions();

          getOrg("","chart_div");
      }else{
          $.toast({heading: "FAILED",text: "พบข้อผิดพลาดโปรดตรวจสอบ",position: 'top-right',loaderBg:'#ff6849',icon: 'error',hideAfter: 3500,stack: 6});
      }
    });
  }
  // -- create --
  $.fn.departments = function(){
    var url = getApi("department/getall");
    var departmentBody = $(this);
    $.ajax({
      url: url,
      type: 'GET',
      headers: {'Authorization': token}
    }).done(function(data) {
      departmentBody.html("");
      if (data["Has"] == false) {

      }else{

        var num = 1;
        for (var i = 0; i < data["data"].length; i++) {
          var r = "";
          r += "<tr>";
          r += "<td class='text-center'>";
          r += num;
          r += "</td>";
          r += "<td>";
          r += data["data"][i]["dep_name"];
          r += "</td>";
          r += "<td width='40%'>";
          r += data["data"][i]["dep_caption"];
          r += "</td>";
          r += "<td>";
          r += data["data"][i]["members"];
          r += "</td>";
          r += "<td width='100'>";
          r += "<div class='btn-group'>";
          r += "<button class='btn btn-sm btn-default btn-outline waves-effect btn-edit-dep' onclick='edit_dep(this)' data-dep_id='"+ data["data"][i]["dep_id"] +"'><span class='fa fa-edit'></span></button>";
          r += "<button class='btn btn-sm btn-default btn-outline waves-effect btn-delete-dep' onclick='delete_dep(this)' data-dep_id='"+ data["data"][i]["dep_id"] +"' ><span class='fa fa-trash'></span></button>";
          // r += "<button class='btn btn-sm btn-default btn-outline waves-effect btn-detail-dep' onclick='detail_dep(this)'  data-dep_id='"+ data["data"][i]["dep_id"] +"'><span class='fa fa-eye'></span></button>";
          r += "</div>";
          r += "</td>";
          r += "</tr>";
          num++;
          departmentBody.append(r);
        }
      }
          // $(".departments_table").dataTable().fnDestroy();
          $(".departments_table").DataTable(dataTableTh);

          $(".btn-detail-dep").click(function(event) {
              var id = $(this).data("dep_id");
              var url = base_url('departments/ModalDetail');
                  $.ajax({
                    url: url,
                    type: 'POST',
                  }).done(function(data) {
                      $(".modal-area").html(data);
                      $("#DetailDev").modal("toggle");
                      console.log(id);
                        var url = getApi("department/getby");
                        $.ajax({
                          url: url,
                          type: 'GET',
                          headers: {'Authorization': token},
                          data: {"dep_id":id}
                        })
                        .done(function(data) {
                          $("#dep_name_label").html(data["data"][0]["dep_name"]);
                          $("#depcreateName").val(data["data"][0]["dep_name"]);
                          $("#depcreateDesc").val(data["data"][0]["dep_caption"]);

                        });
                        getOrg(id,"chart_detail")

                  });
              });

          // end จบการแสดงข้อมูล


    });
  }

  $.fn.positions = function(){
    var url = getApi("Position/getall");
    var positionBody = $(this);
    positionBody.html("");
    $.ajax({
      url:url,
      type: 'GET',
      headers: {'Authorization': token}
    }).done(function(data) {
      // console.log(data);
      var num = 1;
      if (data["Has"] == false) {

      }else{
        var r = "";
        // console.log(data["data"].length);
        for (var i = 0; i < data["data"].length; i++) {
          r += "<tr>";
          r += "<td  class='text-center'>";
          r += num;
          r += "</td>";
          r += "<td>";
          r += data["data"][i]["position_name"];
          r += "</td>";
          r += "<td>";
          r += data["data"][i]["dep_name"];
          r += "</td>";
          r += "<td>";
          if (data["data"][i]["position_root"] == 0) {
            r += "<span class='boss-text'>"+ data["data"][i]["position_name_root"] +"</span>";
          }else{
            r += data["data"][i]["position_name_root"];
          }
          r += "</td>";
          r += "<td width='100'>";
          r += "<div class='btn-group'>";
          r += "<button class='btn btn-sm btn-default btn-outline waves-effect btn-edit-position' onclick='edit_position(this)' data-position_id='"+ data["data"][i]["position_id"] +"'><span class='fa fa-edit'></span></button>";
          r += "<button class='btn btn-sm btn-default btn-outline waves-effect btn-delete-position' onclick='inactive_position(this)' data-position_id='"+ data["data"][i]["position_id"] +"'><span class='fa fa-trash'></span></button>";
          // r += "<button class='btn btn-sm btn-default btn-outline waves-effect btn-detail-position'><span class='fa fa-eye'></span></button>";
          r += "</div>"
          r += "</td>";
          r += "</tr>";
          num++;
        }
          positionBody.append(r);
      }
        $("#position_table").DataTable(dataTableTh);
        var position_id = $(this).data("position_id");
    });
  }
  // -- load table --

///   แผ่นผังองค์กร

})(jQuery)

function getOrg(detail,id){

var item = "";
if (typeof detail !== "undefined") {
item = detail;
}

$.ajax({
url:getApi("department/getcompanyid"),
type:"GET",
headers: {'Authorization': token}
}).done(function(obj) {
  company_name = obj["data"][0]["company_name"];
  var decode = jwt_decode(token);
  $.ajax({url:base_url("Organization/getOrg"),type:"POST",data:{"item":detail,"company_name":company_name,"company_id" : decode["company_id"]}}).done(function(obj) {
  var obj = JSON.parse(obj);
  // console.log(obj);
  google.charts.load('current', {packages:["orgchart"]});
  google.charts.setOnLoadCallback((function(){drawChart(obj,id);}));
  });
});

}
function drawChart(obj,id){
  // console.log(obj,id);
var data = new google.visualization.DataTable();
data.addColumn('string', 'Name');
data.addColumn('string', 'Manager');
data.addColumn('string', 'ToolTip');
data.addRows(obj);
var chart = new google.visualization.OrgChart(document.getElementById(id));
chart.draw(data, {allowHtml:true});
$('.tooltipped').tooltip();
}
