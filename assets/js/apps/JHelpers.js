var token = window.localStorage.getItem('token');
var dataTableTh = [];
var decode;
var company_id_ajax;
var arrMonth = ["มกราคม","กุมภาพันธ์","มีนาคม","เมษายน","พฤษภาคม","มิถุนายน","กรกฎาคม","สิงหาคม","กันยายน","ตุลาคม","พฤศจิกายน","ธันวาคม"];

$(document).ready(function() {
  chkToken();
   readTextFile(base_url("assets/js/Thai.json"), function(text){
       dataTableTh = JSON.parse(text);
  });
  getDecodeData();
  getcompany_id();

  // importScripts('https://www.gstatic.com/firebasejs/3.5.2/firebase-app.js');
  // importScripts('https://www.gstatic.com/firebasejs/3.5.2/firebase-messaging.js')

  // // Enable pusher logging - don't include this in production
  //     Pusher.logToConsole = true;
  //
  //     var pusher = new Pusher('2ecbf0c3ba0a3e400bf9', {
  //       cluster: 'ap1',
  //       forceTLS: true
  //     });
  //
  //     var channel = pusher.subscribe('tiktrack-channel');
  //     channel.bind('tiktrack-event', function(data) {
  //       alert(JSON.stringify(data));
  //     });

  // Initialize Firebase
  var config = {
    apiKey: "AIzaSyC0Ou5iKJL89cFfKN6f7YSjqSrqw4HSLwI",
    authDomain: "tiktrackmanager.firebaseapp.com",
    databaseURL: "https://tiktrackmanager.firebaseio.com",
    projectId: "tiktrackmanager",
    storageBucket: "tiktrackmanager.appspot.com",
    messagingSenderId: "574894075630"
  };
  firebase.initializeApp(config);

  const messaging = firebase.messaging();

  messaging.usePublicVapidKey("BKZ45OLFhPGh9HhRCl6D2thvPXcPuW8dzF6bC7EPjLpww0N2zRHLb2JHlcMpTPohCH6y5JxwTslYFIcvQE9pWP0");

  messaging.requestPermission().then(function() {
  console.log('Notification permission granted.');
  return messaging.getToken();
})
.then(function(token){
  console.log(token);
})
.catch(function(err) {
  console.log('Unable to get permission to notify.', err);
});

messaging.onMessage(function(payload){
  console.log('onMessage: ', payload);


});


});

function setTokenSentToServer(){
  console.log("setTokenSentToServer");
}



function base_url(path){

    if (typeof path === "undefined") {
      path = "";
    }

    var config = 'tiktrack_beta';
    // var config = 'tiktrack_bt2';
    var origin = window.location.origin;
    return origin+"/"+config+"/"+path
}

function getApi(route){
  var origin = "";
  var segments      = location.href.split('/'),
  chkURL = segments[2];
  if (chkURL == "172.16.0.20") {
    origin = "http://172.16.0.21/w_tiktrack_api_beta/";
  }else{
    origin = "http://203.151.43.167/w_tiktrack_api_beta/";
  }
  return origin+route;
}

function getApiOld(route){
  var origin = "";
  var segments      = location.href.split('/'),
  chkURL = segments[2];
  if (chkURL == "172.16.0.20") {
    origin = "http://172.16.0.21/w_tiktrack_api_beta/";
  }else{
    origin = "http://203.151.43.173/w_tiktrack_api_beta/";
  }
  return origin+route;
}

$(".btn-change-pass").click(function(){
  // window.location = base_url('users/changepass');
  var url = base_url('users/changepass');
  $.ajax({
    url: url,
    type: 'POST',
  })
  .done(function(data) {
    $(".modal-area").html(data);
    $("#changePass").modal('toggle');

    $(".btn-save-change-pass").click(function(){
      var chkpass = checkduppass();
      var old_pass = $("#generate_password").val();
      var new_pass = $("#new_password").val();

      if (chkpass) {
        var chkoldpass = getApi("setting/checkpassword");
        $.ajax({
          type:'POST',
          url: chkoldpass,
          headers: {'Authorization': token},
          data: {"password" : window.btoa(window.btoa(old_pass))}
        })
        .done(function(data) {
          if (data) {
            var updatepass = getApi("setting/updatepassword");
            $.ajax({
              type:'POST',
              url: updatepass,
              headers: {'Authorization': token},
              data: {"new_password" : window.btoa(window.btoa(new_pass)), "old_password" : window.btoa(window.btoa(old_pass))}
            })
            .done(function(data) {
              swal({
                  title: 'SUCCESS',
                  text: "เปลี่ยนพาสเวิดเรียบร้อยแล้ว",
                  type: 'success',
                  showCancelButton: false,
                  confirmButtonColor: '#3085d6',
                  cancelButtonColor: '#d33',
                  confirmButtonText: 'Yes, delete it!'
                }).then((result) => {
                  if (result.value) {
                    $("#changePass").modal('toggle');
                    location.reload();
                  }
                })
            });
          }else{
              $("#labelchk").html("<span style='color:red'>Password เดิมไม่ถูกต้อง โปรดตรวจสอบ</span>");
          }
        });
      }else{
        $("#labelchk").html("<span style='color:red'>Password ไม่ตรงกัน โปรดตรวจสอบ</span>");
      }
    });

  });
});


$(".btn-logout").click(function(){
  logout();
});

function getcompany_id(){
  var urlget_company_id = getApi("department/getcompanyid");
  $.ajax({
    url:urlget_company_id,
    type:"GET",
    headers: {'Authorization': token}
  }).done(function(data) {
    company_id_ajax = data;
    // console.log(company_id_ajax);
  });
}

function getDecodeData(){
  var token = localStorage.getItem('token');
  decode = jwt_decode(token);

  if (decode["isAdmin"] == "0") {
    $(".isAdmin-org").html("");
  }else if (decode["isAdmin"] == "2") {
    $(".isAdmin-org").html("");
    $(".isAdmin").html("");
  }

  $('#fullname').html(decode["firstname"]+' '+decode["lastname"]);
  var timeLogin = decode["timestamp"];
  var urldatediff = base_url("chelpers/getdatediff");
  $.ajax({
    url:urldatediff,
    type:"POST",
    data:{"timestamp":timeLogin}
  }).done(function(data) {
    var obj = JSON.parse(data);
    console.log(obj);
    if (obj["chk"]) {
      console.log("session timeout");
      // logout();
    }
  });

  console.log(timeLogin);
}

function chkToken(){
  if (localStorage.getItem("token") === null) {
    window.location = base_url("");
  }
}

function logout(){
  localStorage.removeItem("token");
  window.location = base_url("");
}

function readTextFile(file, callback) {
    var rawFile = new XMLHttpRequest();
    rawFile.overrideMimeType("application/json");
    rawFile.open("GET", file, true);
    rawFile.onreadystatechange = function() {
        if (rawFile.readyState === 4 && rawFile.status == "200") {
            callback(rawFile.responseText);
        }
    }
    rawFile.send(null);
}


function clearModals(){
     $('.modal').on('hidden.bs.modal', function (e) {
         $(".modal-area").html("");
      });
}

(function($){

  $.fn.positionOptions = function($position_id = "0"){

    var dep = "";
    // if (typeof depObj !== "undefined") {
    //     dep = depObj["keyValue"];
    // }

    dep = $("#positionDep").val();
    console.log(dep);

    var url = getApi("position/getall");
    var positionOptions = $(this);
    // console.log(this);
    $.ajax({
      url:url,
      type:"GET",
      headers: {'Authorization': token},
      data:{"dep_id":dep}
    })
    .done(function(data) {
      if (data["Has"] == false) {
        positionOptions.html("");
        var r = "<option value='0'>";
        r += "ระดับสูงสุด";
        r += "</option>";
        positionOptions.append(r);
      }else{
        positionOptions.html("");
        var r = "<option value='0'>";
        r += "ระดับสูงสุด";
        r += "</option>";
        for (var i = 0; i < data["data"].length; i++) {
          r += "<option value='"+ data["data"][i]["position_id"] +"'>";
          r += data["data"][i]["position_name"];
          r += "</option>";
        }
        positionOptions.append(r);
      }
      $("#positionRoot").val($position_id);
      positionOptions.select2();

    });

  }
  $.fn.depOptions = function ($dep_id = 0){

    var url = getApi("department/getall");
    var depOptions = $(this);
    $.ajax({
      url: url,
      type: 'GET',
      headers: {'Authorization': token}
    })
    .done(function(data) {
      if (data["Has"] == false) {

      }else{
        depOptions.html("");
        var r = "<option value='0'>";
        r += "เลือกแผนก";
        r += "</option>";
        for (var i = 0; i < data["data"].length; i++) {
          r += "<option value='"+ data["data"][i]["dep_id"] +"'>";
          r += data["data"][i]["dep_name"];
          r += "</option>";
        }
        depOptions.append(r);
      }
      $("#positionDep").val($dep_id);
      depOptions.select2();

    });

  }
  // -- options --
  $.fn.validate_blank = function(){
    var input = $(this);
    for (var i = 0; i < input.length; i++) {
         if (input[i].value == "") {
             var id = input[i].id;
             $("#"+id+"_group").addClass('has-warning');
             $("#"+id+"_feedback").addClass('glyphicon glyphicon-warning-sign');
             $("#"+id+"").focus(function(){
                        $(this).removeClass('has-warning');
                        $("#"+id+"_group").removeClass('has-warning');
                        $("#"+id+"_feedback").removeClass('glyphicon glyphicon-warning-sign');
             });
             $("#"+id+"").change(function(){
                        $(this).removeClass('has-warning');
                        $("#"+id+"_group").removeClass('has-warning');
                        $("#"+id+"_feedback").removeClass('glyphicon glyphicon-warning-sign');
             });
             return false;
         }
    }
    return true;
  }
  $.fn.validate = function(){
    var input = $(this);
    for (var i = 0; i < input.length; i++) {

             var id = input[i].id;
             $("#"+id+"_group").addClass('has-warning');
             $("#"+id+"_feedback").addClass('glyphicon glyphicon-warning-sign');
             $("#"+id+"").focus(function(){
                        $(this).removeClass('has-warning');
                        $("#"+id+"_group").removeClass('has-warning');
                        $("#"+id+"_feedback").removeClass('glyphicon glyphicon-warning-sign');
             });
             $("#"+id+"").change(function(){
                        $(this).removeClass('has-warning');
                        $("#"+id+"_group").removeClass('has-warning');
                        $("#"+id+"_feedback").removeClass('glyphicon glyphicon-warning-sign');
             });
             return false;

    }
    return true;
  }
})(jQuery);

function checkduppass(){
  var n_pass = $("#new_password").val();
  var re_pass = $("#renew_password").val();

  if (n_pass == re_pass) {
    return true;
  }else{
    return false;
  }
}

function convertDataFormat(tempdate){
  var arrDate = tempdate.split("-");
  var formatDate = arrDate[2]+"/"+arrDate[1]+"/"+arrDate[0];
  return formatDate;
}
