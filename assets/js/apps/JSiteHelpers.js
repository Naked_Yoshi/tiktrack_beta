$(document).ready(function() {
  var elems = Array.prototype.slice.call(document.querySelectorAll('.js-switch'));
  $('.js-switch').each(function() {
     new Switchery($(this)[0], $(this).data());
  });
});

function initAutocomplete() {
  in_autocomplete = new google.maps.places.Autocomplete(
      /** @type {!HTMLInputElement} */(document.getElementById('loname')),
      {types: ['geocode']});
  in_autocomplete.addListener('place_changed', fillInAddress);
}
function fillInAddress() {
  var place = in_autocomplete.getPlace();

  if(typeof place.geometry !== "undefined"){
      initMap({"lat":  place.geometry["location"].lat(),"lng":  place.geometry["location"].lng()})

  }else{
      alert("ไม่พบสถานที่แห่งนี้");
  }
}

function site_edit(el){
  var edit = $(el).data("edit");
  var urlEdit = base_url("sites/Edit/"+edit);
  window.location = urlEdit;
}

function site_delete(el){
  var site_id = $(el).data("delete");
  var token = window.localStorage.getItem('token');
  var urlinactive = getApi("sites/inactive");
  swal({
      title: 'โปรดยืนยัน',
      text: "ต้องการยกเลิกไซต์นี้หรือไม่",
      type: 'info',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'ตกลง',
      cancelButtonText: 'ยกเลิก'
    }).then((result) => {
      if (result.value) {
        $.ajax({
          url: urlinactive,
          type: 'POST',
          headers: {'Authorization': token},
          data: {"site_id" : window.atob(window.atob(window.atob(site_id)))}
        }).done(function(data){
          if (data) {
            $.toast({ heading: "SUCCESS",text: "ยกเลิกไซต์เรียบร้อยแล้ว",position: 'top-right',loaderBg:'#ff6849',icon: 'success',hideAfter: 3500,stack: 6});

            $("#site_body").html("");
            $(".site_table").dataTable().fnDestroy();
            $("#site_body").Sites();
          }else{
            $.toast({heading: "FAILED",text: "พบข้อผิดพลาดโปรดตรวจสอบ",position: 'top-right',loaderBg:'#ff6849',icon: 'error',hideAfter: 3500,stack: 6});
          }
        });
      }else{

      }
    })


}

function geolocate(id) {
      if (navigator.geolocation) {
          navigator.geolocation.getCurrentPosition(function(position) {
          var geolocation = {
              lat: position.coords.latitude,
              lng: position.coords.longitude
          };
          var circle = new google.maps.Circle({
                center: geolocation,
                radius: position.coords.accuracy
          });
          initMap(geolocation,"map",id);
          $("#"+id+"createLat").val(geolocation["lat"]);
          $("#"+id+"createLng").val(geolocation["lng"]);
        });
    }
}


function gentable (data,index){
  var tr  = "";
      tr += "<tr id='selectWk"+index+"' data-index='"+index+"' data-id='"+data["wk_id"]+"'>";
      tr += "<td class='text-left'>";
      tr += data["wk_name"];
      tr += "</td>";
      tr += "<td class='text-right'>";
      tr += "<span class='date-working "+getDayActive(data["wk_mon_start"])+" '>จ.</span> ";
      tr += "<span class='date-working "+getDayActive(data["wk_tue_start"])+" '>อ.</span> ";
      tr += "<span class='date-working "+getDayActive(data["wk_wed_start"])+" '>พ.</span> ";
      tr += "<span class='date-working tw "+getDayActive(data["wk_thu_start"])+" '>พฤ.</span> ";
      tr += "<span class='date-working "+getDayActive(data["wk_fri_start"])+" '>ศ.</span> ";
      tr += "<span class='date-working "+getDayActive(data["wk_sat_start"])+" '>ส.</span> ";
      tr += "<span class='date-working tw "+getDayActive(data["wk_sun_start"])+" '>อา.</span> ";
      tr += "</td>";
      tr += "<td class='text-right' width='50'>";
      tr += "<button class='btn btn-sm btn-outline btn-danger btn-rm-select' data-index='"+index+"' onclick='rm_select("+index+")'><span class='fa fa-remove'></span></button>";
      tr += "</td>";
      tr += "</tr>";
      return tr;
}

function rm_select(index){
     $("#selectWk"+index).remove();
     $("#btn_select_wk"+index).removeAttr('disabled');
     if ($("#wk_select_table tr").length < 1) {
        $("#wk_select_table").append("<tr class='noSelect'><td colspan='3' class='text-center '><b> --- เลือกวันทำการ --- </b></td></tr>");
     }
}


function initMap(geolocation,map,idinput){
  var id = "map"
  if (typeof map !== "undefined") {
      id = map;
  }
  var input = "site";
  if (typeof idinput !== "undefined") {
      input = idinput;
  }
  // console.log(idinput);
  $("#"+input+"createLat").val(geolocation["lat"]);
  $("#"+input+"createLng").val(geolocation["lng"]);
  var mapArea = $("#CircleArea_"+input);
  var map = new google.maps.Map(document.getElementById(id), {
      center: geolocation,
      zoom: 17
  });

  var marker = new google.maps.Marker({
          map: map,
          position:geolocation,
          draggable: true,
  });

  var circlemap = new google.maps.Circle({
          strokeColor: '#FF0000',
          strokeOpacity: 0.7,
          strokeWeight: 1,
          fillColor: '#FF0000',
          fillOpacity: 0.35,
          map: map,
          center: geolocation,
  });

  circlemap.setRadius(parseFloat(mapArea.val()));

  var start_move_marker = google.maps.event.addListener(marker, 'dragstart', function (event) {
       circlemap.setMap(null);
  });

  var end_move_marker = google.maps.event.addListener(marker, 'dragend', function (event) {
           var lat = this.getPosition().lat();
           var lng = this.getPosition().lng();
           $("#"+input+"createLat").val(lat);
           $("#"+input+"createLng").val(lng);
           circlemap.setMap(map);
           circlemap.setCenter({"lat":lat,"lng":lng});
           circlemap.setRadius(parseFloat(mapArea.val()));
  });

  mapArea.change(function(event) {
      var area = parseFloat($(this).val());
      circlemap.setRadius(area);
  });
}

function getDayActive(dayin){
    if (dayin == "00:00:00") {
        return "";
    }else{
        return "active";
    }
}

(function ($){
  $.fn.Sites = function(){
      var siteBody = $(this);
      // var url = base_url("sites/load");
      var urlapi = getApi("sites/getall");
      console.log(urlapi);
      var token = window.localStorage.getItem('token');
      $.ajax({
        url: urlapi,
        type: 'GET',
        headers: {'Authorization': token}
      }).done(function(data){
        console.log(data);
        if (data["Has"] == false) {
          siteBody.html("");
        }else{
          var no = 1;
                   siteBody.html("");
                   for (var i = 0; i < data["data"].length; i++) {
                        siteBody.append("<tr><td  class='text-center'>"+ no +"</td><td>"+ data['data'][i]['siteName'] +"</td><td class='text-center'><div class='btn-group'><button class='btn btn-sm btn-default btn-outline waves-effect btn-site-edit' data-edit='"+ window.btoa(window.btoa(window.btoa(data["data"][i]["site_id"]))) +"'  onclick='site_edit(this)'><span class='fa fa-edit'></span></button><button class='btn btn-sm btn-default btn-outline waves-effect' data-delete='"+ window.btoa(window.btoa(window.btoa(data["data"][i]["site_id"]))) +"' onclick='site_delete(this)'><span class='fa fa-trash'></span></button></div></td></tr>");
                        no ++;
                   }

              $(".site_table").DataTable(dataTableTh);
        }
        });
  }
  $.fn.workingCreate = function(options){
      var dataPost = [];
      if (typeof options) {
          dataPost = options
      }
      var form = $(this).serialize();
      var url = base_url("workings/create");
      $.ajax({
        url: url,
        type: 'POST',
        data : form
      })
      .done(function(data) {
        var obj = JSON.parse(data);
        if (obj["success"]) {
            $.toast({ heading: obj["title"],text: obj["msg"],position: 'top-right',loaderBg:'#ff6849',icon: 'success',hideAfter: 3500,stack: 6});
            $(".tabs-working-list").workingList(dataPost);
            $("#list_wk").show();
            $("#create_new_wk").hide();

        }else{
            $.toast({heading: obj["title"],text: obj["msg"],position: 'top-right',loaderBg:'#ff6849',icon: 'error',hideAfter: 3500,stack: 6});
        }
      });
  }
  $.fn.workingList = function(options){
      var dataPost = [];
      if (typeof options) {
          dataPost = options
      }
      var tab_list = $(this);
      var tab_content = $(".tabs-working-content");
      var url = base_url("workings/wkList");
      var decode = jwt_decode(token);

      $.ajax({
        url: url,
        type: 'POST',
        data : {"dataPost" : dataPost,"auth" : window.btoa(decode["employee_id"]) ,"company_id" : decode["company_id"]}
      })
      .done(function(data) {
          var obj = JSON.parse(data);
          tab_list.html("")
          tab_content.html("");
          if (obj["success"]) {
              wk_list = obj["data"];
              for (var i = 0; i < obj["list"].length; i++) {
                   tab_list.append(obj["list"][i]);
                   tab_content.append(obj["content"][i]);
              }
              //
              $(".btn-select-wk").click(function(event) {
                  var index = $(this).data("index");
                  var select_table = $("#wk_select_table");
                  var tr = gentable(obj["data"][index],index);
                  if ($("#wk_select_table tr.noSelect").length > 0) {
                      select_table.html("");
                  }
                  if($("#selectWk"+index+"").length < 1){
                      select_table.append(tr);
                  }

                  $(this).attr("disabled","disabled");
              });
          }
      });
  }
  $.fn.siteCreate = function(options){
    // console.log(options);

      var url = getApi("sites/add");
      $.ajax({
        url: url,
        type: 'POST',
        headers: {'Authorization': token},
        data:{"request" : options}
      })
      .done(function(data) {
        if (data) {
            $.toast({heading: "SUCCESS",text:"เพิ่มไซต์งานเรียบร้อยแล้ว",position: 'top-right',loaderBg:'#ff6849',icon: 'success',hideAfter: 3500,stack: 6});
            $("#siteCreate").modal("toggle");
            $(".site_table").dataTable().fnDestroy();
            $("#site_body").Sites();
        }else{
            $.toast({heading: "ERROR",text: "ไม่สามารถเพิ่มได้",position: 'top-right',loaderBg:'#ff6849',icon: 'error',hideAfter: 3500,stack: 6});
        }

      });
  }

})(jQuery);
