$(document).ready(function() {
  var elems = Array.prototype.slice.call(document.querySelectorAll('.js-switch'));
  $('.js-switch').each(function() {
      new Switchery($(this)[0], $(this).data());
  });

  getCompanySetting();
});

function getCompanySetting(){
  $.ajax({
    url: getApi("Setting/getCompanySetting"),
    type: 'GET',
    headers: {'Authorization': token}
  })
  .done(function(data) {
    if (data["Has"]) {
      var eslip_enable = data["data"][0]["module_eslip"];
      var ot_enable = data["data"][0]["module_ot"];
      var job_enable = data["data"][0]["module_job"];
      // console.log(eslip_enable);
      if (eslip_enable == "1") {
        $("#cb_eslip").attr('checked',true);
        $("#cb_eslip").trigger("click");
      }
      if (ot_enable == "1") {
        $("#allow_ot").attr('checked',true);
        $("#allow_ot").trigger("click");
      }
      if (job_enable == "1") {
        $("#auto_replace").attr('checked',true);
        $("#auto_replace").trigger("click");
      }
    }

  });

  $(".btn-save-setting").click(function(){
    var set_eslip = 0;
    var set_ot = 0;
    var set_job = 0;
    if ($("#cb_eslip").is(":checked")) {set_eslip = 1;}
    if ($("#allow_ot").is(":checked")) {set_ot = 1;}
    if ($("#auto_replace").is(":checked")) {set_job = 1;}
    var req = {
      "set_eslip" : set_eslip,
      "set_ot" : set_ot,
      "set_job" : set_job
    }

    $.ajax({
      url: getApi("Setting/saveCompanySetting"),
      type: 'POST',
      headers: {'Authorization': token},
      data: {"request":req}
    }).done(function(data) {
      if (data) {
        $.toast({ heading: "SUCCESS",text: "เปลี่ยนแปลงค่าเรียบร้อยแล้ว",position: 'top-right',loaderBg:'#ff6849',icon: 'success',hideAfter: 3500,stack: 6});
        location.reload();
      }else{
        $.toast({ heading: "FAILED",text: "พบข้อผิดพลาดโปรดตรวจสอบ",position: 'top-right',loaderBg:'#ff6849',icon: 'error',hideAfter: 3500,stack: 6});
      }
    });
  });

}
