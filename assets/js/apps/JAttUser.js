var d = new Date();

var keyFilter = "";
var pagechange = 0;

var month = d.getMonth()+1;
var day = d.getDate();
var year = d.getFullYear();

var output = d.getFullYear() + '-' +
    (month<10 ? '0' : '') + month + '-' +
    (day<10 ? '0' : '') + day;

var pageSelected = 1;

var day_value;
var month_value;
var year_value;
var site_value;
var search_type;
var branch_value;
// var chk_search = 0;
// var round_serch = 0;

var data_checkin_row = [];
var data_checkin_late = [];
var data_check_absence = [];

var token = window.localStorage.getItem('token');
// var decode = jwt_decode(token);


function chkactive(el){
  var id = $(el).attr('id');
  // console.log(id);
  // console.log("เลือกจำนวน"+id.length+"คน");

  if($("#"+id).prop('checked')){

    if($("#"+id).prop('checked',true)){

        $("#"+id).addClass('active');

        $(".all-approve").removeClass('disabled');
        $(".all-approve").removeAttr('disabled');

        $(".all-not-approve").removeClass('disabled');
        $(".all-not-approve").removeAttr('disabled');
    }

  }else{
     $("#"+id).removeClass('active');

     $(".all-approve").addClass('disabled');
     $(".all-approve").attr('disabled');

     $(".all-not-approve").addClass('disabled');
      $(".all-not-approve").attr('disabled');
  }
} //function

function chk_all(){
  // console.log("คลิกปุ่มcheck box ทั้งหมดได้");

  if($("#select-all").prop('checked')){
    $("input:checkbox").not('[disabled=disabled]').prop('checked',true);
    //$(".all-approve").addClass('active');
    $("input:checkbox").addClass('active');

    /* ปุ่ม อนุมัติ และ ไม่อนุมัติ */

    // ลบคลาส disabled ใน  Class=" all-approve disabled"
    $(".all-approve").removeClass('disabled');
    // ลบAttribute disabled ใน tag button โดยอ้างอิงจากคลาสที่ชื่อว่าall-approve  <button Class="all-approve" disabled>
    $(".all-approve").removeAttr('disabled');

    // ลบคลาส disabled ใน  Class=" all-not-approve disabled"
    $(".all-not-approve").removeClass('disabled');
    // ลบAttribute disabled ใน tag button โดยอ้างอิงจากคลาสที่ชื่อว่าall-not-approve  <button Class="all-not-approve" disabled>
    $(".all-not-approve").removeAttr('disabled');

    // console.log("ติ๊กหมดแล้วนะ");
  }else {
    $("input:checkbox").not(this).prop('checked',false);
    // console.log("ยังติ๊กไม่หมด");
    //$(".all-approve").removeClass('active');
    $("input:checkbox").removeClass('active');


    /* ปุ่ม อนุมัติ และ ไม่อนุมัติ */

    // เพิ่มคลาส disabled ใน  Class=" all-approve disabled"
    $(".all-approve").addClass('disabled');
    // เพิ่ม Attribute disabled ใน tag button โดยอ้างอิงจากคลาสที่ชื่อว่าall-approve  <button Class="all-approve" disabled>
    $(".all-approve").attr('disabled');
    // เพิ่มคลาส disabled ใน  Class=" all-not-approve disabled"
    $(".all-not-approve").addClass('disabled');
    // เพิ่ม Attribute disabled ใน tag button โดยอ้างอิงจากคลาสที่ชื่อว่าall-not-approve  <button Class="all-not-approve" disabled>
    $(".all-not-approve").attr('disabled');


  }
}


$(document).ready(function() {
  day_value = output;
  getcheckin(); //แสดงค่าเข้างาน
  $(".select2").select2();
  $('.selectpicker').selectpicker();
  callpicker();
  $("#site").sitegetall();
  $("#r_site").sitegetall();
  $("#r_dep").getdepAll();
  $("#report_selectsearchmonth").val(month);
  $("#report_selectsearchyear").val(year);
});

$("#r_site").change(function(){
  $("#index_site").val(this.value);

  $("#r_branche").branchgetall("r_site");
})

$("#searchtype select").change(function(event) {
            var type = $(this).val();
            $(".type_ofsearch").hide();
            $("#"+type+"_type").show();
});

$("#site").change(function(event) {
    $("#branch").branchgetall("site");
});

function callpicker(){
  $('.mydatepicker, #datepicker').datepicker({
      autoclose: true,
      todayHighlight: true,
      clearBtn:true,
      endDate:"0d",
      language: 'th'
  });
}

function getcheckin(){
  var table = $("#attendance_table");
  var attendanceBody = $("#attendance_body");

    getCheckInRow();

      getcheckinLate(); //แสดงคนสาย

      // $("#select-all").click(function(event) {
      //   console.log("checkALL");
      //   chk_all();
      // }); //function #select-all
};



$(".click01").on( "click", function() {
  getCheckInRow();
});

function getCheckInRow(){
  var urlcheckin = getApi("ATTData/getcheckin");
  var attendanceBody = $("#attendance_body");
  var table = $("#attendance_table");
  // console.log(day_value);
  $.ajax({
    url: urlcheckin,
    type: 'GET',
    data : {
      "day": day_value,
      "month": month_value,
      "year":year_value,
      "site_id": site_value,
      "branche_id": branch_value
    },
    headers: {'Authorization': token}
  }).done(function(data) {
    console.log(data);
    if (data["Has"] == false) {
      $("#checkin").html(0);
    }else{
      $("#checkin").html(data["checkin_rows"]);

      table.dataTable().fnDestroy();

      // console.log("Login");
      // attendanceBody.html("");
      var attendanceHead = $("#attendance_head");
      attendanceHead.html("");
      var tablehead = "<tr id='headcolumn'>"
      tablehead += "<th></th>";
      tablehead += "<th></th>";
      tablehead += "<th>ชื่อ-นามสกุลปกติ</th>";
      tablehead += "<th>ช่วงเวลา</th>";
      tablehead += "<th>วันที่ลงเวลา</th>";
      tablehead += "<th>เวลาเข้า</th>";
      tablehead += "<th>เวลาออก</th>";
      tablehead += "<th>สาย</th>";
      tablehead += "<th>เวลาทำงาน</th>";
      tablehead += "<th>สถานะ</th>";
      // tablehead += "<th>ช่องทางเข้า</th>";
      // tablehead += "<th>ช่องทางออก</th>";
      tablehead += "<th>ตัวเลือก</th>";
      tablehead += "</tr>";
      attendanceHead.append(tablehead);
      attendanceBody.html("");
        var no = 1;
        $count = 0;
           for (var i = 0; i < data["data"].length; i++) {
             $count ++;
           var tablesapprove = "<tr>";

             tablesapprove += "<td  class='text-center'>";
             tablesapprove += "<div class='checkbox'>";
             if (data["data"][i]["approveStatus"] == null) {
               tablesapprove += "<input class='att-item' id='"+data["data"][i]["tat_id"]+"' type='checkbox' onclick='chkactive(this)' disabled /><label></label></div>";
             }else{
               tablesapprove += "<input class='att-item' id='"+data["data"][i]["tat_id"]+"' type='checkbox' onclick='chkactive(this)' /><label></label></div>";
             }

             tablesapprove += "</td>";

             tablesapprove += "<td  class='text-center'>";
              tablesapprove += no;
             tablesapprove += "</td>";

             tablesapprove += "<td>";
             tablesapprove += data["data"][i]["fullname"];
             tablesapprove += "</td>";

             tablesapprove += "<td>";
             if (data["data"][i]["wk_name"] == null) {
               tablesapprove += "ยังไม่เชื่อมต่อกลุ่มเวลา";
             }else{
               tablesapprove += data["data"][i]["wk_name"];
             }

               tablesapprove += "</br>";
               if (data["data"][i]["wokingStr"] == null) {

               }else{
                 tablesapprove += data["data"][i]["wokingStr"];
               }

             tablesapprove += "</td>";

             tablesapprove += "<td class='text-center'>";
             tablesapprove += data["data"][i]["tat_day"];
             tablesapprove += "</td>";

             var tat_day = "00";
             if (data["data"][i]["tat_day"].length == 1) {
               tat_day = "0" + data["data"][i]["tat_day"];
             }else{
               tat_day = data["data"][i]["tat_day"];
             }

             tablesapprove += "<td class='text-center'>";
               var checkintime = data["data"][i]["checkin_time"];

               if(!checkintime){
                  tablesapprove += "<center><b>--</b></center>";
               }else{
                 tablesapprove += tat_day + "/" + data["data"][i]["checkin_date"].substr(3,8) + "<br>" + data["data"][i]["checkin_time"];

                 if (data["data"][i]["checkin_type_id"] == 3) {
                   tablesapprove += "&nbsp;<span style='color:orange' class='fa fa-rotate-left'></span>";
                 }
               }
             tablesapprove += "</td>";

             tablesapprove += "<td class='text-center'>";
             var checkouttime = data["data"][i]["checkout_time"];
             if(!checkouttime){
               tablesapprove += "<center><b>--</b></center>";
             }else{
               tablesapprove += data["data"][i]["checkout_date"] + "<br>" + data["data"][i]["checkout_time"];

               if (data["data"][i]["checkout_type_id"] == 4) {
                 tablesapprove += "&nbsp;<span style='color:orange' class='fa fa-rotate-left'></span>";
               }
             }
             tablesapprove += "</td>";

             tablesapprove += "<td style='text-align:center;'>";
             if (data["data"][i]["lateTime"] == null) {
               lateTime = "<center><b>--</b></center>";
             }else{
               if (data["data"][i]["lateTime"].indexOf('-') == -1) {
                 lateTime = data["data"][i]["lateTime"];
               }else{
                 lateTime = "<center><b>--</b></center>";
               }
             }
             tablesapprove += lateTime;
             tablesapprove += "</td>";

             tablesapprove += "<td class='text-center'>";
              var totalworktime = data["data"][i]["total_wk_time"];
              if(!totalworktime){
                tablesapprove +="<center><b>--</b></center>";
              }else{
                tablesapprove += data["data"][i]["total_wk_time"];
              }
             tablesapprove += "</td>";

             tablesapprove += "<td style='text-align:center;'>";
               var status = data["data"][i]["approveStatus"];
               if(status =='1'){
               tablesapprove += "<button class='btn btn-success btn-rounded disabled' data-position='top' data-delay='50' data-tooltip='อนุมัติ' disabled><span class='fa fa-check'></span> อนุมัติ<span class='approve_num'></span> </button> ";
               }
               if(status =='2'){
               tablesapprove += "<button class='btn btn-danger btn-rounded disabled' data-position='top' data-delay='50' data-tooltip='ไม่อนุมัติ' disabled><span class='fa fa-remove'></span> ไม่อนุมัติ<span class='reject_num'></span></button>";
               }
               if(status =='3'){
               tablesapprove += "<button class='btn btn-default btn-rounded disabled' data-position='top' data-delay='50' data-tooltip='ไม่ตอบสนอง' disabled> ไม่ตอบสนอง<span class='reject_num'></span></button>";
              }
              if(status == null){
               tablesapprove += "<span class='label label-default'>ยังไม่ลงเวลาออก</span>"
              }
             tablesapprove += "</td>";

             tablesapprove += "<td class='text-center'>";
               tablesapprove += "<div class='btn-group'>";
               tablesapprove += "<button class='btn btn-sm btn-default btn-outline waves-effect btn-approve-delete'  onclick='showdetail(this)' id='"+data["data"][i]["tat_id"]+"'><i class='fa fa-navicon'></i></button>";

               if (decode["isAdmin"] == "1") {
               tablesapprove += "<button class='isAdmin btn btn-sm btn-default btn-outline waves-effect  btn-approve-delete'  onclick='showadjust(this)' id='"+data["data"][i]["tat_id"]+"'><i class='fa fa-pencil'></i></button>";
                }

               tablesapprove += "</div>";
             tablesapprove += "</td>";

            tablesapprove += "</tr>";
              $("#attendance_body").append(tablesapprove);
              no ++;
            } //loop
      // var keyFilter = $("div#attendance_table_filter input").val();
      table.dataTable(dataTableTh);
      table.dataTable().fnPageChange(pagechange);
      // console.log(keyFilter);
      $("div#attendance_table_filter input").val(keyFilter);
      $('.dataTables_wrapper').on('mousedown', '.paginate_button:not(.previous):not(.next)', function() {
        // pageSelected = $(this).data("dt-idx");
        pageSelected = $(this).text();
        pagechange = parseInt(pageSelected) - 1;
        // console.log(pagechange);
      });
    }
  });
}



function getcheckinLate(){
  var table = $("#attendance_table");
  var attendanceBody = $("#attendance_body");

  var urllate = getApi("ATTData/getlate");
  $.ajax({
    url: urllate,
    type: 'GET',
    data : {
      "day": day_value,
      "month": month_value,
      "year":year_value,
      "site_id": site_value,
      "branche_id": branch_value
    },
    headers: {'Authorization': token}
  }).done(function(data) {
    if (data["Has"] == false) {
      $("#late").html("0");

    }else{
      var checkInLate_row = data["data"].length;
      $("#late").html(checkInLate_row);
    }

    $(".click02").click(function(){
      getCheckin_Late(data);
    })

  });

  getcheckinabsence(); //แสดงคนขาด

  // $("#select-all").click(function(event) {
  //   console.log("checkALL");
  //   chk_all();
  // }); //function #select-all
}

function getCheckin_Late(data){
  var table = $("#attendance_table");
  var attendanceBody = $("#attendance_body");

  if (data["Has"] == false) {
    attendanceBody.html("");
  }else{
    var no = 1;
    console.log("LATEEE");


    table.dataTable().fnDestroy();

    var attendanceHead = $("#attendance_head");

    var tablehead = "<tr id='headcolumn'>"
    tablehead += "<th></th>";
    tablehead += "<th></th>";
    tablehead += "<th>ชื่อ-นามสกุล</th>";
    tablehead += "<th>ช่วงเวลา</th>";
    tablehead += "<th>เวลาเข้า</th>";
    tablehead += "<th>เวลาออก</th>";
    tablehead += "<th>สาย</th>";
    tablehead += "<th>เวลาทำงาน</th>";
    tablehead += "<th>สถานะ</th>";
    tablehead += "<th>ช่องทางเข้า</th>";
    tablehead += "<th>ช่องทางออก</th>";
    tablehead += "<th>ตัวเลือก</th>";
    tablehead += "</tr>";

    attendanceHead.html("");
    attendanceHead.append(tablehead);

    attendanceBody.html("");
    for (var i = 0; i < data["data"].length; i++) {

    var tablesapprove = "<tr>";

    tablesapprove += "<td  class='text-center'>";
    if (data["data"][i]["approveStatus"] == null) {
      tablesapprove += "<div class='checkbox'><input class='att-item' id='"+data["data"][i]["tat_id"]+"' type='checkbox' onclick='chkactive(this)' disabled /><label></label></div>";
    }else{
      tablesapprove += "<div class='checkbox'><input class='att-item' id='"+data["data"][i]["tat_id"]+"' type='checkbox' onclick='chkactive(this)'/><label></label></div>";
    }

    tablesapprove += "</td>";

    tablesapprove += "<td  class='text-center'>";
     tablesapprove += no;
    tablesapprove += "</td>";

    tablesapprove += "<td>";
    tablesapprove += data["data"][i]["fullname"];
    tablesapprove += "</td>";

    tablesapprove += "<td>";
      tablesapprove += data["data"][i]["wk_name"];
      tablesapprove += "</br>";
      tablesapprove += data["data"][i]["wokingStr"];
    tablesapprove += "</td>";

    tablesapprove += "<td class='text-center'>";
      var checkintime = data["data"][i]["checkin_time"];

      if(!checkintime){
         tablesapprove += "<center><b>--</b></center>";
      }else{
        tablesapprove +=data["data"][i]["checkin_date"] + "<br>" + data["data"][i]["checkin_time"];

        if (data["data"][i]["checkin_type_id"] == 3) {
          tablesapprove += "&nbsp;<span style='color:orange' class='fa fa-rotate-left'></span>";
        }
      }
    tablesapprove += "</td>";

    tablesapprove += "<td class='text-center'>";
    var checkouttime = data["data"][i]["checkout_time"];
    if(!checkouttime){
      tablesapprove += "<center><b>--</b></center>";
    }else{
      tablesapprove += data["data"][i]["checkout_date"] + "<br>" + data["data"][i]["checkout_time"];

      if (data["data"][i]["checkout_type_id"] == 4) {
        tablesapprove += "&nbsp;<span style='color:orange' class='fa fa-rotate-left'></span>";
      }
    }
    tablesapprove += "</td>";

    tablesapprove += "<td class='text-center'>";
    tablesapprove += data["data"][i]["lateTime"];
    tablesapprove += "</td>";

    tablesapprove += "<td class='text-center'>";
     var totalworktime = data["data"][i]["total_wk_time"];
     if(!totalworktime){
       tablesapprove +="<center><b>--</b></center>";
     }else{
       tablesapprove += data["data"][i]["total_wk_time"];
     }
    tablesapprove += "</td>";

    tablesapprove += "<td class='text-center'>";
      var status = data["data"][i]["approveStatus"];
      if(status =='1'){
      tablesapprove += "<button class='btn btn-success btn-rounded disabled' data-position='top' data-delay='50' data-tooltip='อนุมัติ' disabled><span class='fa fa-check'></span> อนุมัติ<span class='approve_num'></span> </button> ";
      }
      if(status =='2'){
      tablesapprove += "<button class='btn btn-danger btn-rounded disabled' data-position='top' data-delay='50' data-tooltip='ไม่อนุมัติ' disabled><span class='fa fa-remove'></span> ไม่อนุมัติ<span class='reject_num'></span></button>";
      }
      if(status =='3'){
      tablesapprove += "<button class='btn btn-default btn-rounded disabled' data-position='top' data-delay='50' data-tooltip='ไม่ตอบสนอง' disabled> ไม่ตอบสนอง<span class='reject_num'></span></button>";
      }
      if(status == null){
       tablesapprove += "<span class='label label-default'>ยังไม่ลงเวลาออก</span>"
      }
    tablesapprove += "</td>";

    tablesapprove += "<td class='text-center'>";
     var checkinrouteid = data["data"][i]["checkin_route_id"];
      tablesapprove += "<span class='mytooltip tooltip-effect-1'><span class='tooltip-item2'>";
       if(checkinrouteid =='1'){
        tablesapprove += "<center><button class='btn btn-info btn-rounded disabled' data-position='top' data-delay'50' data-tooltip='คลิกลงเวลา' data-tooltip-id=''><span class='fa fa-hand-o-up'></span></button></center>";

        tablesapprove += "<span class='tooltip-content clearfix>'";
        tablesapprove += "<span class='tooltip-text text-center'>" + "<div class='text-center'>คลิกลงเวลา</div>";
        tablesapprove += "</span></span>";
       }
       if(checkinrouteid =='2'){
         tablesapprove += "<center><button class='btn btn-info btn-rounded disabled' data-position='top' data-delay'50' data-tooltip='QR CODE' data-tooltip-id=''><span class='fa fa-qrcode'></span></button></center>";

         tablesapprove += "<span class='tooltip-content clearfix>'";
         tablesapprove += "<span class='tooltip-text text-center'>" + "<div class='text-center'>QR CODE</div>";
         tablesapprove += "</span></span>";
       }
       tablesapprove += "</span>";
       if(!checkinrouteid){
         tablesapprove += "<center><b>--</b></center>";
       }
    tablesapprove += "</td>";

    tablesapprove += "<td class='text-center'>";
     var checkoutrouteid = data["data"][i]["checkout_route_id"];
     tablesapprove += "<span class='mytooltip tooltip-effect-1'><span class='tooltip-item2'>";
     if(checkoutrouteid =='1'){
     tablesapprove += "<center><button class='btn btn-info btn-rounded disabled' data-position='top' data-delay'50' data-tooltip='คลิกลงเวลา' data-tooltip-id=''><span class='fa fa-hand-o-up'></span></button></center>";

     tablesapprove += "<span class='tooltip-content clearfix>'";
     tablesapprove += "<span class='tooltip-text text-center'>" + "<div class='text-center'>คลิกลงเวลา</div>";
     tablesapprove += "</span></span>";
     }
     if(checkoutrouteid =='2'){
      tablesapprove += "<center><button class='btn btn-info btn-rounded disabled' data-position='top' data-delay'50' data-tooltip='คลิกลงเวลา' data-tooltip-id=''><span class='fa fa-qrcode'></span></button></center>";

      tablesapprove += "<span class='tooltip-content clearfix>'";
      tablesapprove += "<span class='tooltip-text text-center'>" + "<div class='text-center'>QR CODE</div>";
      tablesapprove += "</span></span>";
     }
     tablesapprove += "</span>";
     if(!checkoutrouteid){
      tablesapprove += "<center><b>--</b></center>";
     }
    tablesapprove += "</td>";

    tablesapprove += "<td class='text-center'>";
      tablesapprove += "<div class='btn-group'>";
      tablesapprove += "<button class='btn btn-sm btn-default btn-outline waves-effect btn-approve-delete'  onclick='showdetail(this)' id='"+data["data"][i]["tat_id"]+"'><i class='fa fa-navicon'></i></button>";
      tablesapprove += "</div>";
    tablesapprove += "</td>";
  tablesapprove += "</tr>";
       attendanceBody.append(tablesapprove);
       no ++;
     }//loop


       table.DataTable(dataTableTh);
  }

}

function getcheckinabsence(){
  var table = $("#attendance_table");
  var attendanceBody = $("#attendance_body");

  var urllate = getApi("ATTData/getAbsense");
  $.ajax({
    url: urllate,
    type: 'GET',
    data : {
      "day": day_value,
      "month": month_value,
      "year":year_value,
      "site_id": site_value,
      "branche_id": branch_value
    },
    headers: {'Authorization': token}
  }).done(function(data) {
    $("#absense").html(data["skip_rows"]);

    $(".click03").click(function(){
      // console.log("click03");
      $("#attendance_table").dataTable().fnDestroy();
      var no = 1;
      var attendanceHead = $("#attendance_head");
      attendanceHead.html("");
      var tablehead = "<tr id='headcolumn'>"
      // tablehead += "<th><div class='checkbox'><input class='select-all' id='select-all' type='checkbox'/><label></label></div></th>";
      tablehead += "<th></th>";
      tablehead += "<th>ชื่อ-นามสกุล</th>";
      tablehead += "<th>ช่วงเวลา</th>";
      tablehead += "<th>เบอร์โทรศัพท์</th>";
      tablehead += "<th>ไซด์</th>";
      tablehead += "<th>สาขา</th>";
      tablehead += "<th>เหตุผลขาดงาน</th>";
      tablehead += "</tr>";
      attendanceHead.append(tablehead);

      attendanceBody.html("");
       for (var i = 0; i < data["data"].length; i++) {
         // $count ++;

         var tablesapprove = "<tr>";
         tablesapprove += "<td  class='text-center'>";
          tablesapprove += no;
         tablesapprove += "</td>";

         tablesapprove += "<td>";
         tablesapprove += data["data"][i]["fullname"];
         tablesapprove += "</td>";

         tablesapprove += "<td>";
           tablesapprove += data["data"][i]["wk_name"];
           tablesapprove += "</br>";
           tablesapprove += data["data"][i]["wokingStr"];
         tablesapprove += "</td>";

         tablesapprove += "<td>";
         tablesapprove += data["data"][i]["phoneNumber"];
         tablesapprove += "</td>";

         tablesapprove += "<td>";
         tablesapprove += data["data"][i]["siteName"];
         tablesapprove += "</td>";

         tablesapprove += "<td>";
         tablesapprove += data["data"][i]["brancheName"];
         tablesapprove += "</td>";

         tablesapprove += "<td>";
         if (data["data"][i]["detail"] === null || data["data"][i]["detail"] === undefined) {
           tablesapprove += "ขาดงาน";
         }else{
           tablesapprove += data["data"][i]["detail"];
         }
         tablesapprove += "</td>";

         tablesapprove += "</tr>";

          attendanceBody.append(tablesapprove);
            no ++;
          } //loop

          table.DataTable(dataTableTh);
    });//click
    // $("#attendance_table").dataTable().fnDestroy();

  });
}

$(".btnsearchall").click(function(){
  search_type = $("#search_type").val();

  if (search_type == "day") {
      var n = $("#datepicker").val().split('/');
      day_value = n[2]+"-"+n[1]+"-"+n[0];
      month_value = "";
      year_value = "";
  }else if(search_type == "month"){
    day_value = "";
    month_value = $("#selectsearchmonth").val();
    year_value = d.getFullYear();
  }else if (search_type == "year"){
    day_value = "";
    month_value = "";
    year_value = $("#selectsearchyear").val();
   }

   site_value = $("#site").val();
   if(site_value != 0){

   }else{
     site_value = "";
   }

   //เลือกสาขา
   branch_value = $("#branch").val();
   if (!!branch_value) {
     //branch_value = "";
   }else{
     //branch_value = 0;
     branch_value = "";
   }


   getcheckin(); //แสดงค่าเข้างาน
   // getcheckinLate(); //แสดงคนสาย
   // getcheckinabsence(); //แสดงคนขาด

});

(function ($){
  $.fn.sitegetall = function (){
    var site = $(this);

    var url = getApi("sites/getall");
        $.ajax({
          url: url,
          type: 'GET',
          headers: {'Authorization': token}
        }).done(function(data) {
          var no = 1;

          if (data["Has"] == false) {
          // log("data (site)=", data);

          }else{
            // log("data (site)=", data);
               var r = "<option value='0'>";
               r += "เลือกไซต์";
               r += "</option>";
              for (var i = 0; i < data["data"].length; i++) {
                r += "<option value='"+ data["data"][i]["site_id"] +"'>";
                r += data["data"][i]["siteName"];
                r += "</option>";

             }//loop
            site.append(r);

          } //else
        });//done

    }
  })(jQuery);

  (function ($){
    $.fn.getdepAll = function (){
      var dep = $(this);
      $.ajax({
        url: getApi("Department/getall"),
        type: 'GET',
        headers: {'Authorization': token}
      }).done(function(data) {
        if (data["Has"] == false) {
          dep.html("");
          var r = "";
          r += "<option value='0'>";
          r += "ไม่มีสาขา";
          r += "</option>";
          dep.append(r);
        }else{
          dep.html("");
          var r = "";
          r += "<option value='0'>";
          r += "ไม่มีสาขา";
          r += "</option>";
        for (var i = 0; i < data["data"].length; i++) {
            r += "<option value='"+ data["data"][i]["dep_id"] +"'>";
            r += data["data"][i]["dep_name"];
            r += "</option>";
         }//loop
          dep.append(r);
        }
      });

      $('#r_dep').change(function(){
        $('#index_dep').val(this.value);
        // console.log(this.value);
      });

    }
  })(jQuery);

  (function ($){
    $.fn.branchgetall = function (chk){
      var branch = $(this);

      if (chk == "r_site") {
        var siteval = $("#r_site").val();
      }else{
        var siteval = $("#site").val();
      }
      var url = getApi("Branches/getall");

          $.ajax({
            url: url,
            type: 'GET',
            headers: {'Authorization': token},
            data: {"site_id" : siteval}
          }).done(function(data) {
            console.log(data);
            var no = 1;


            if (data["Has"] == false) {
              branch.html("");
              var r = "";
              r += "<option value='0'>";
              r += "ไม่มีสาขา";
              r += "</option>";
              branch.append(r);

            }else{
              branch.html("");
              var r = "";
              r += "<option value='A'>";
              r += "ทั้งหมด";
              r += "</option>";
            for (var i = 0; i < data["data"].length; i++) {
                r += "<option value='"+ data["data"][i]["branche_id"] +"'>";
                r += data["data"][i]["brancheName"];
                r += "</option>";

             }//loop
              branch.append(r);

            } //else
          });//done

          $("#r_branche").change(function(){
            $("#index_branche").val(this.value);
          });
    }
  })(jQuery);

  

    function showdetail(el){

        var url = base_url('attendance/showdetailuser');

         $.ajax({
           url: url,
           type: 'POST',
         }).done(function(data) {

         $(".modal-area").html(data);
         $("#DetailModal").modal('toggle');

              var urldetail = getApi("ATTData/getBy");
              var id = $(el).attr('id');

               $.ajax({
                 url: urldetail,
                 type: 'GET',
                 data : {
                   'id' : id
                 },
                 headers: {'Authorization': token}
               }).done(function(data) {

                 // console.log("ส่งค่า id ที่จะแสดงได้แล้ว ค่าเป็น =",data);

                 if(data["Has"] == false){

                 }else{

                   //id
                   var tat_id = data["data"][0]["tat_id"];

                   //ชื่อ-นามสกุล
                   var label_name = $("#label_name");
                   label_name.html("");
                   var fullname = data["data"][0]["fullname"];
                   label_name.append(fullname);

                   //ไซต์
                   var label_site = $("#label_site");
                   label_site.html("");
                   var siteName = data["data"][0]["siteName"];
                   label_site.append(siteName);

                   //สาขา
                   var label_branch = $("#label_branch");
                   label_branch.html("");
                   var brancheName = data["data"][0]["brancheName"];
                   label_branch.append(brancheName);

                   //วันที่
                   var label_check_date = $("#label_check_date");
                   label_check_date.html("");
                   var check_date = data["data"][0]["check_date"];
                   label_check_date.append(check_date);

                   //เวลาเช็คอิน
                   var label_checkin_time = $("#label_checkin_time");
                   label_checkin_time.html("");
                   var checkin_time = data["data"][0]["checkin_time"];
                   label_checkin_time.append(checkin_time);

                   //เวลาเช็คเอาท์
                   var label_checkout_time = $("#label_checkout_time");
                   label_checkout_time.html("");
                   var checkout_time = "";
                   if (data["data"][0]["checkout_time"] == null) {
                     checkout_time = "ยังไม่ลงเวลาออก";
                   }else{
                     checkout_time = data["data"][0]["checkout_time"];
                   }

                   label_checkout_time.append(checkout_time);

                   //สาย
                   var label_late_time = $("#label_late_time");
                   label_late_time.html("");
                   var lateTime = data["data"][0]["lateTime"];
                   if (data["data"][0]["lateTime"].indexOf('-') == -1) {
                     lateTime = data["data"][0]["lateTime"];
                   }else{
                     lateTime = "--";
                   }
                   label_late_time.append(lateTime);

                   //ช่องทางการเข้า
                   var label_checkin_route_id = $("#label_checkin_route_id");
                   label_checkin_route_id.html("");
                   var checkin_route_id = "";
                   if (data["data"][0]["checkin_type_id"] == 1) {
                     checkin_route_id = "ลงเวลาเข้าปกติ";
                   }else if(data["data"][0]["checkin_type_id"] == 3){
                     checkin_route_id = "ลงเวลาเข้าย้อนหล้ง";
                   }
                   label_checkin_route_id.append(checkin_route_id);

                   //ช่องทางการออก
                   var label_checkout_route_id = $("#label_checkout_route_id");
                   label_checkout_route_id.html("");
                   var checkout_route_id = "";
                   if(data["data"][0]["checkout_type_id"] == 2){
                     checkout_route_id = "ลงเวลาออกปกติ";
                   }else if(data["data"][0]["checkout_type_id"] == 4){
                     checkout_route_id = "ลงเวลาออกย้อนหลัง";
                   }else{
                     checkout_route_id = "ยังไม่ลงเวลาออก"
                   }
                   label_checkout_route_id.append(checkout_route_id);

                   //การอนุมัติ
                   var label_approveStatus = $("#label_approveStatus");
                   label_approveStatus.html("");
                   var approveStatus = "";
                   if(data["data"][0]["approveStatus"] == 1) {
                     approveStatus = "อนุมัติแล้ว"
                   }else if(data["data"][0]["approveStatus"] == 2){
                     approveStatus = "ไม่อนุมัติ"
                   }else if(data["data"][0]["approveStatus"] == 3){
                     approveStatus = "รอการอนุมัติ"
                   }else{
                     approveStatus = "ยังลงเวลาไม่เรียบร้อย"
                   }

                   label_approveStatus.append(approveStatus);

                   var in_lat = data["data"][0]["in_lat_device"];
                   var in_long = data["data"][0]["in_long_device"];

                   var out_lat = data["data"][0]["out_lat_device"];
                   var out_long = data["data"][0]["out_long_device"];

                   initMap("map_in",in_lat,in_long);

                   initMap("map_out",out_lat,out_long);



                   $("#tat_message_in").append(data["data"][0]["tat_message_in"]);
                   $("#tat_message_out").append(data["data"][0]["tat_message_out"])

                 }

               }); //done function data

       }); //done function data (Open Modal Dialog)

    } //End of function showdetail

    function showadjust(el){
      var url = base_url('attendance/showadjust');
      keyFilter = $("div#attendance_table_filter input").val();
      // $("div#attendance_table_filter input").val("ห้ามย้าย");
      // console.log(keyFilter);

       $.ajax({
         url: url,
         type: 'POST',
       }).done(function(data) {

       $(".modal-area").html(data);
       $("#AdjustModal").modal('toggle');

       $('.clockpicker').clockpicker({placement: 'right',align: 'right',autoclose: true,'default': 'now',clear:true}).keypress(function(event) {return false;});

       $(".btn-cancel-time").click(function(){
         var id = $(this).data("id");
         $("#"+id).val("");
       });

       var urlAdjust = getApi("ATTData/getAdjust");
       var id = $(el).attr('id');

        $.ajax({
          url: urlAdjust,
          type: 'GET',
          data : {
            'id' : id
          },
          headers: {'Authorization': token}
        }).done(function(data) {
          if (data["Has"]) {
            $("#adjust_emp_name").append(data["detail"][0]["fullname"]);
            $("#tat_date_checkin").val(data["detail"][0]["check_date"]);
            $("#tat_date_checkout").val(data["detail"][0]["check_date"]);
            $("#tat_time_checkin").val(data["detail"][0]["checkin_time"].slice(0,-3));
            $("#tat_time_checkout").val(data["detail"][0]["checkout_time"].slice(0,-3));

            $("#pre_change_check_in_time").val(data["detail"][0]["checkin_time"].slice(0,-3));
            $("#pre_change_check_out_time").val(data["detail"][0]["checkout_time"].slice(0,-3));

            var r = "";
            var no = 1;

            r += "<tr style='background-color:#FF0000;color:white;'>";
            r += "<td class='text-center'>";
            r += 0;
            r += "</td>";
            r += "<td>";
            r += data["adjust"][0]["changed_check_in_time"].slice(0,-3);
            r += "</td>";
            r += "<td>";
            r += data["adjust"][0]["changed_check_out_time"].slice(0,-3);
            r += "</td>";
            r += "<td>";
            r += data["adjust"][0]["fullname"];
            r += "</td>";
            r += "<td>";
            r += data["adjust"][0]["updateAt"];
            r += "</td>";
            r += "</tr>";

            for (var i = 0; i < data["adjust"].length; i++) {
              r += "<tr>";
              r += "<td class='text-center'>";
              r += no;
              r += "</td>";
              r += "<td>";
              r += data["adjust"][i]["pre_change_check_in_time"].slice(0,-3);
              r += "</td>";
              r += "<td>";
              r += data["adjust"][i]["pre_change_check_out_time"].slice(0,-3);
              r += "</td>";
              r += "<td>";
              r += data["adjust"][i]["fullname"];
              r += "</td>";
              r += "<td>";
              r += data["adjust"][i]["updateAt"];
              r += "</td>";
              r += "</tr>";
              no++;
            }
            $("#adjust_body").append(r);
            $("#adjust_table").dataTable(dataTableTh);
          }

        });

        $(".btn-save-adjust").click(function(){
          var tat_time_checkin = $("#tat_time_checkin").val();
          var tat_time_checkout = $("#tat_time_checkout").val();

          if (tat_time_checkin == "") {
            $.toast({heading: "FAILED",text: "โปรดกรอกเวลาเข้างานให้ถูกต้อง",position: 'top-right',loaderBg:'#ff6849',icon: 'warning',hideAfter: 3500,stack: 6});
            return;
          }
          if (tat_time_checkout == "") {
            $.toast({heading: "FAILED",text: "โปรดกรอกเวลาเลิกงานให้ถูกต้อง",position: 'top-right',loaderBg:'#ff6849',icon: 'warning',hideAfter: 3500,stack: 6});
            return;
          }
          if (tat_time_checkout < tat_time_checkin) {
            $.toast({heading: "FAILED",text: "โปรดกรอกเวลาเลิกงานให้ถูกต้อง",position: 'top-right',loaderBg:'#ff6849',icon: 'warning',hideAfter: 3500,stack: 6});
            return;
          }

          var req = {
            "tat_id" : id,
            "pre_change_check_in_time" : $("#pre_change_check_in_time").val(),
            "pre_change_check_out_time" : $("#pre_change_check_out_time").val(),
            "changed_check_in_time" : tat_time_checkin,
            "changed_check_out_time" : tat_time_checkout
          };

          // console.log(req);

          var url_save_adjust = getApi("ATTData/saveAdjust");
          $.ajax({
            url: url_save_adjust,
            type: 'POST',
            headers: {'Authorization': token},
            data: {'request' : req}
          }).done(function(data){
            if (data) {
              $.toast({ heading: "SUCCESS",text: "แก้ไขวันทำการเรียบร้อยแล้ว",position: 'top-right',loaderBg:'#ff6849',icon: 'success',hideAfter: 3500,stack: 6});
              $("#AdjustModal").modal("toggle");
              getcheckin();

            }else{
              $.toast({heading: "FAILED",text: "พบข้อผิดพลาดโปรดตรวจสอบ",position: 'top-right',loaderBg:'#ff6849',icon: 'error',hideAfter: 3500,stack: 6});
            }

          });

        });

     });
    }

    function initMap(mapid,lat_in,long_in) {
    // Create a map object and specify the DOM element for display.
    lat_in = parseFloat(lat_in);
    long_in = parseFloat(long_in);
    var uluru = {lat:lat_in, lng:long_in};
    var map = new google.maps.Map(document.getElementById(mapid), {
      center: uluru,
      zoom: 15
    });
    var marker = new google.maps.Marker({
          position: uluru,
          map: map
        });
}

$(".all-approve").click(function() {
  keyFilter = $("div#attendance_table_filter input").val();
  var url = getApi("ATTData/approve");
    var myArray = [];
    var dataSelect = $(".att-item.active").not('[disabled=disabled]');

  for (var i = 0; i < dataSelect.length; i++) {
    var element = dataSelect[i];
    console.log("สิ่งที่เลือกออกมาที่จะอนุมัติ",dataSelect[i]);
    myArray.push(element.id);
  }

  // console.log("คลิกฟังก์ชั่น all-approve ได้ และที่select ออกมาคือ", dataSelect);
  // console.log("ค่าอาเรย์ที่จะส่งไป",myArray);
  // console.log("จำนวน Array ที่จะส่งไป (นับจากค่าตัวแปร myArray)",myArray.length);

  swal({
        title:"คุณแน่ใจ ?",
        text: "ที่ต้องการอนุมัติ จำนวน "+myArray.length+" คน",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: "ใช่, อนุมัติเลย!",
        cancelButtonText: "ไม่, ยกเลิก!",
        closeOnConfirm: false,
        closeOnCancel: true
    }).then((result) => {
      if (result.value) {
        console.log("result when click approve ");
        $.ajax({
          type:'POST',
          url: url,
          headers: {'Authorization': token},
          contentType : "application/x-www-form-urlencoded",
          data: {'arr_tid': myArray}

        }).done(function(data) {
          console.log("เมื่อdone แล้ว",data);

          if(data){

            $("#select-all").prop('checked',false);

            $.toast({ heading: "APPROVED",text: "อนุมัติการลงเวลาเรียบร้อยแล้ว",position: 'top-right',loaderBg:'#ff6849',icon: 'success',hideAfter: 3500,stack: 6});

            getcheckin();

            // $('.dataTables_wrapper').data("dt-idx").val(3).trigger("click");

            /* ปุ่ม อนุมัติ และ ไม่อนุมัติ */

             // เพิ่มคลาส disabled ใน  Class=" all-approve disabled"
             $(".all-approve").addClass('disabled');
             // เพิ่มAttribute disabled ใน tag button โดยอ้างอิงจากคลาสที่ชื่อว่าall-approve  <button Class="all-approve" disabled>
             $(".all-approve").attr('disabled');

             // เพิ่มคลาส disabled ใน  Class=" all-not-approve disabled"
             $(".all-not-approve").addClass('disabled');
             // เพิ่มAttribute disabled ใน tag button โดยอ้างอิงจากคลาสที่ชื่อว่าall-not-approve  <button Class="all-not-approve" disabled>
             $(".all-not-approve").attr('disabled');



          } //if data


        }); //done func data
        // //end connect to api
      } // if result value
    }) //then result


}); // func approve btn

$(".all-not-approve").click(function() {
  var url = getApi("ATTData/reject");
    var myArray = [];
    var dataSelect = $(".att-item.active").not('[disabled=disabled]');


  for (var i = 0; i < dataSelect.length; i++) {
    var element = dataSelect[i];
    console.log("สิ่งที่เลือกออกมาที่จะไม่อนุมัติ",dataSelect[i]);
    myArray.push(element.id);
  }

  // console.log("คลิกฟังก์ชั่น all-not-approve ได้ และที่select ออกมาคือ", dataSelect);
  // console.log("ค่าอาเรย์ที่จะส่งไป",myArray);
  // console.log("จำนวน Array ที่จะส่งไป (นับจากค่าตัวแปร myArray)",myArray.length);

  swal({
        title:"คุณแน่ใจ ?",
        text: "ที่ต้องการไม่อนุมัติ จำนวน "+myArray.length+" คน",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: "ใช่, ไม่อนุมัติ!",
        cancelButtonText: "ไม่, ยกเลิก!",
        closeOnConfirm: false,
        closeOnCancel: true
    }).then((result) => {
      if (result.value) {
        console.log("result when click not approve ");
        $.ajax({
          type:'POST',
          url: url,
          headers: {'Authorization': token},
          contentType : "application/x-www-form-urlencoded",
          data: {'arr_tid': myArray}

        }).done(function(data) {
          // console.log("เมื่อdone แล้ว",data);

          if(data){

            $("#select-all").prop('checked',false);

            $.toast({ heading: "NOT APPROVED",text: "ไม่อนุมัติการลงเวลาเรียบร้อยแล้ว",position: 'top-right',loaderBg:'#ff6849',icon: 'info',hideAfter: 3500,stack: 6});

            getcheckin();


            /* ปุ่ม อนุมัติ และ ไม่อนุมัติ */

             // เพิ่มคลาส disabled ใน  Class=" all-approve disabled"
             $(".all-approve").addClass('disabled');
             // เพิ่มAttribute disabled ใน tag button โดยอ้างอิงจากคลาสที่ชื่อว่าall-approve  <button Class="all-approve" disabled>
             $(".all-approve").attr('disabled');

             // เพิ่มคลาส disabled ใน  Class=" all-not-approve disabled"
             $(".all-not-approve").addClass('disabled');
             // เพิ่มAttribute disabled ใน tag button โดยอ้างอิงจากคลาสที่ชื่อว่าall-not-approve  <button Class="all-not-approve" disabled>
             $(".all-not-approve").attr('disabled');

          }// if data


        }); //done(function(data) {
        // //end connect to api
      } // if result value
    }) //then result
}); // func not approve btn

$(".btn-report-attendance").click(function(){
  var decode = jwt_decode(token);
  var selectType = $("#report_Type").val();
  $("#company_id").val(decode["company_id"]);
  if (selectType == 0) {
    var empName = $("#search_user_report").val();
    if (empName == "") {
      $.toast({ heading: "WARNING",text: "โปรดใส่ชื่อพนักงานให้ถูกต้อง",position: 'top-right',loaderBg:'#ff6849',icon: 'warning',hideAfter: 3500,stack: 6});
    }else{
        $(".report_form").submit();
    }
  }else if (selectType == 1) {
    var selectSite = $("#r_site").val();
    if (selectSite == "0") {
      $.toast({ heading: "WARNING",text: "โปรดเลือกไซต์และสาขาให้ถูกต้อง",position: 'top-right',loaderBg:'#ff6849',icon: 'warning',hideAfter: 3500,stack: 6});
    }else{
      $(".report_form").submit();
    }
  }else if(selectType == 2){
    var selectDep = $("#r_dep").val();
    if (selectDep == "0") {
      $.toast({ heading: "WARNING",text: "โปรดเลือกแผนกให้ถูกต้อง",position: 'top-right',loaderBg:'#ff6849',icon: 'warning',hideAfter: 3500,stack: 6});
    }else{
      $(".report_form").submit();
    }
  }

})

  $("#report_selectType select").change(function(event) {
     var type = $(this).val();
     if (type == 0) {
         $("#by_person").show();
         $("#by_site").hide();
         $("#by_dep").hide();
     }else if(type == 1){
          $("#by_person").hide();
          $("#by_site").show();
          $("#by_dep").hide();
     }else if(type == 2){
       $("#by_person").hide();
       $("#by_site").hide();
       $("#by_dep").show();
     }
  });

$(function(){
  var url = getApi("users/getall");
  $.ajax({
    url: url,
    type: 'GET',
    headers: {'Authorization': token}
  }).done(function(data) {

    $('#search_user_report').autoComplete({
        minChars: 0,
        source: function(term, suggest){
            term = term.toLowerCase();
            var choices = [];
            for (var i = 0; i < data["data"].length; i++) {
              choices.push([data["data"][i]["fullname"],data["data"][i]["employee_id"]]);
            }
            var suggestions = [];
            for (i=0;i<choices.length;i++)
                if (~(choices[i][0]+' '+choices[i][1]).toLowerCase().indexOf(term)) suggestions.push(choices[i]);
            suggest(suggestions);
        },
        renderItem: function (item, search){
          search = search.replace(/[-\/\\^$*+?.()|[\]{}]/g, '\\$&');
          var re = new RegExp("(" + search.split(' ').join('|') + ")", "gi");
          return '<div class="autocomplete-suggestion" data-langname="'+item[0]+'" data-lang="'+item[1]+'" data-val="'+search+'">'+item[0].replace(re, "<b>$1</b>")+'</div>';
        },
        onSelect: function(e, term, item){
            // var id = window.atob(item.data('lang'))
            console.log('Item "'+item.data('langname')+' ('+item.data('lang')+')" selected by '+(e.type == 'keydown' ? 'pressing enter or tab' : 'mouse click')+'.');
            $('#search_user_report').val(item.data('langname'));
            $('#index_emp').val(item.data('lang'))
            // console.log(item.data('lang'));
        }
    });
  });
  });
