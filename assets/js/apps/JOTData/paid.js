var d = new Date();
var g_month = d.getMonth()+1;
var g_year = d.getFullYear();

var token = window.localStorage.getItem('token');
var otdata = [];
$(document).ready(function() {
  $("#otdata_body").load();
});

(function($){
  $.fn.load = function(){
    var table = $("#otdata_table");
    var otdata_body = $("#otdata_body");
    var url = getApi("OTData/getPaid");
    otdata_body.html("");
    $.ajax({
      type:'GET',
      url: url,
      headers: {'Authorization': token},
      data: {"month":"", "year":""}
    })
    .done(function(data) {
      //mapping data to table  and create html
      // console.log(data);
      if(data["Has"] )
      {
        // console.log(data["data"]);
        var no = 1;

        otdata  = data["data"];
        for (var i = 0; i < data["data"].length; i++) {
          var tableotdata = "";

            tableotdata += "<tr>";

             // tableotdata += "<td>";
             // tableotdata += "<div class='checkbox'><input class='ot-item' id='"+data["data"][i]["otd_id"]+"' type='checkbox'/><label></label></div>";
             // tableotdata += "</td>";

             tableotdata += "<td class='text-center'>";
             tableotdata += no;
             tableotdata += "</td>";

             tableotdata += "<td>";
             tableotdata += data["data"][i]["fullName"];
             tableotdata += "</td>";

             tableotdata += "<td class='text-center'>";
             tableotdata += data["data"][i]["requestDate"]+"<br/>"+data["data"][i]["requestTime"];
             tableotdata += "</td>";

             tableotdata += "<td>";
             tableotdata += data["data"][i]["detail"];
             tableotdata += "</td>";

             tableotdata += "<td class='text-center'>";
             tableotdata += data["data"][i]["date_start"]+"<br/>"+data["data"][i]["time_start"];
             tableotdata += "</td>";

             tableotdata += "<td class='text-center'>";
             tableotdata += data["data"][i]["date_end"]+"<br/>"+data["data"][i]["time_end"];
             tableotdata += "</td>";

             tableotdata += "<td class='text-center'>";
             tableotdata += "<span class='mytooltip tooltip-effect-1'><span class='tooltip-item'>";
             tableotdata += "("+data["data"][i]["multiple"] + ")";
             tableotdata += "</span>";
             tableotdata += "<span class='tooltip-content clearfix>'";
             tableotdata += "<span class='tooltip-text'>" + "<div class='text-center'>"+data["data"][i]["multiple_str"] + " </div></span>";
             tableotdata += "</td>";

             tableotdata += "<td class='text-center'> <div class='label label-table label-success'>";
             if (data["data"][i]["adjustOT"] == "-") {
               var a = data["data"][i]["reqTotalHours"].split(':');
               var b = data["data"][i]["actualWork"].split(':');
               var reqTotalHours = (+a[0]) * 60 * 60 + (+a[1]) * 60;
               var actualWork = (+b[0]) * 60 * 60 + (+b[1]) * 60;

               // console.log("reqTotalHours : " + reqTotalHours + "/ actualWork :" + actualWork);

               if (actualWork >= reqTotalHours) {
                 tableotdata += data["data"][i]["reqTotalHours"];
               }else{
                 tableotdata += data["data"][i]["actualWork"];
               }

             }else{
               tableotdata += data["data"][i]["adjustOT"];
             }
             tableotdata += "</div></td>";

             tableotdata += "<td class='text-center'>";
             if (data["data"][i]["code_site"] == null || data["data"][i]["siteName"] == "") {
               tableotdata += data["data"][i]["brancheName"];
             }else
             {
               tableotdata += data["data"][i]["code_site"];
             }
             tableotdata += "</td>";

             // tableotdata += "<td class='text-center'>";
             // tableotdata += data["data"][i]["brancheName"];
             // tableotdata += "</td>";

             // tableotdata += "<td class='text-center'> <div class='label label-table label-success'>";
             // // tableotdata += data["data"][i]["reqTotalHours"];
             // tableotdata += "</div></td>";

             tableotdata += "<td class='text-center'> <div class='label label-table label-info'>";
             tableotdata += data["data"][i]["approvePaidUser"];
             tableotdata += "</div></td>";

             tableotdata += "</td>";
             tableotdata += "<td class='text-center'>";
             tableotdata += "<div class='btn-group'>";
             // tableotdata += "<button class='btn btn-sm btn-default btn-outline waves-effect btn-ot-approve' onclick='ot_approve(this)' data-otindex='"+
             //                data["data"][i]["otd_id"] +"'><span class='fa fa-check-square'></span></button>" +
             //                "<button class='btn btn-sm btn-default btn-outline waves-effect btn-ot-reject' onclick='ot_reject(this)' data-otindex='"+
             //                data["data"][i]["otd_id"] +"'><span class='fa fa-times ' aria-hidden='false'></span></button>";
             tableotdata += "</div>";
             tableotdata += "</td>";
             tableotdata += "</tr>";

             otdata_body.append(tableotdata);
             no++;

        }

        table.DataTable(dataTableTh);

      }else {
        //no request for OT
        table.DataTable(dataTableTh);
      }


    });
    //end done load data
  }
  $(".btn-report-otrequest").click(function(){
      var url = base_url('OTdata/ReportRequest');
      $.ajax({
        url: url,
        type: 'POST',
      })
      .done(function(data) {
          $(".modal-area").html(data);
          $("#OTrequest").modal("toggle");

          getEmp();
          callpicker();

            $(".submit-report-print-ot-complete").click(function(){
              $(".report_form").submit();
            })
      });
  });
})(jQuery);

$(".btn_filter").click(function(){
  $.ajax({
    url: base_url("OTdata/FilterLeaveRequest"),
    type: 'POST'
  }).done(function(data) {
    $(".modal-area").html(data);
    $("#FilterLeaveRequest").modal("toggle");

    $("#select_searchmonth").val(g_month);
    $("#select_searchyear").val(g_year);

    $(".filter-Leave-Request").click(function(){
      var month = $("#select_searchmonth").val();
      var year = $("#select_searchyear").val();

      url = getApi("OTData/getPaid");

      $("#otdata_table").dataTable().fnDestroy();

      $.ajax({
        url: url,
        type: 'GET',
        headers: {'Authorization': token},
        data: {"month":month, "year":year}
      }).done(function(data) {

        if (data["Has"] == false) {
            $("#otdata_body").html("");
          $("#otdata_table").dataTable(dataTableTh);
        }else{
            $("#otdata_body").html("");
          no = 1;
          for (var i = 0; i < data["data"].length; i++) {
            var tableotdata = "";

              tableotdata += "<tr>";

               // tableotdata += "<td>";
               // tableotdata += "<div class='checkbox'><input class='ot-item' id='"+data["data"][i]["otd_id"]+"' type='checkbox'/><label></label></div>";
               // tableotdata += "</td>";

               tableotdata += "<td class='text-center'>";
               tableotdata += no;
               tableotdata += "</td>";

               tableotdata += "<td>";
               tableotdata += data["data"][i]["fullName"];
               tableotdata += "</td>";

               tableotdata += "<td class='text-center'>";
               tableotdata += data["data"][i]["requestDate"]+"<br/>"+data["data"][i]["requestTime"];
               tableotdata += "</td>";

               tableotdata += "<td>";
               tableotdata += data["data"][i]["detail"];
               tableotdata += "</td>";

               tableotdata += "<td class='text-center'>";
               tableotdata += data["data"][i]["date_start"]+"<br/>"+data["data"][i]["time_start"];
               tableotdata += "</td>";

               tableotdata += "<td class='text-center'>";
               tableotdata += data["data"][i]["date_end"]+"<br/>"+data["data"][i]["time_end"];
               tableotdata += "</td>";

               tableotdata += "<td class='text-center'>";
               tableotdata += "<span class='mytooltip tooltip-effect-1'><span class='tooltip-item'>";
               tableotdata += "("+data["data"][i]["multiple"] + ")";
               tableotdata += "</span>";
               tableotdata += "<span class='tooltip-content clearfix>'";
               tableotdata += "<span class='tooltip-text'>" + "<div class='text-center'>"+data["data"][i]["multiple_str"] + " </div></span>";
               tableotdata += "</td>";

               tableotdata += "<td class='text-center'> <div class='label label-table label-success'>";
               if (data["data"][i]["adjustOT"] == "-") {
                 var a = data["data"][i]["reqTotalHours"].split(':');
                 var b = data["data"][i]["actualWork"].split(':');
                 var reqTotalHours = (+a[0]) * 60 * 60 + (+a[1]) * 60;
                 var actualWork = (+b[0]) * 60 * 60 + (+b[1]) * 60;

                 // console.log("reqTotalHours : " + reqTotalHours + "/ actualWork :" + actualWork);

                 if (actualWork >= reqTotalHours) {
                   tableotdata += data["data"][i]["reqTotalHours"];
                 }else{
                   tableotdata += data["data"][i]["actualWork"];
                 }

               }else{
                 tableotdata += data["data"][i]["adjustOT"];
               }
               tableotdata += "</div></td>";

               tableotdata += "<td class='text-center'>";
               if (data["data"][i]["code_site"] == null || data["data"][i]["siteName"] == "") {
                 tableotdata += data["data"][i]["brancheName"];
               }else
               {
                 tableotdata += data["data"][i]["code_site"];
               }
               tableotdata += "</td>";

               // tableotdata += "<td class='text-center'>";
               // tableotdata += data["data"][i]["brancheName"];
               // tableotdata += "</td>";

               // tableotdata += "<td class='text-center'> <div class='label label-table label-success'>";
               // // tableotdata += data["data"][i]["reqTotalHours"];
               // tableotdata += "</div></td>";

               tableotdata += "<td class='text-center'> <div class='label label-table label-info'>";
               tableotdata += data["data"][i]["approvePaidUser"];
               tableotdata += "</div></td>";

               tableotdata += "</td>";
               tableotdata += "<td class='text-center'>";
               tableotdata += "<div class='btn-group'>";
               // tableotdata += "<button class='btn btn-sm btn-default btn-outline waves-effect btn-ot-approve' onclick='ot_approve(this)' data-otindex='"+
               //                data["data"][i]["otd_id"] +"'><span class='fa fa-check-square'></span></button>" +
               //                "<button class='btn btn-sm btn-default btn-outline waves-effect btn-ot-reject' onclick='ot_reject(this)' data-otindex='"+
               //                data["data"][i]["otd_id"] +"'><span class='fa fa-times ' aria-hidden='false'></span></button>";
               tableotdata += "</div>";
               tableotdata += "</td>";
               tableotdata += "</tr>";

            $("#otdata_body").append(tableotdata)
            no++;

          }
          $("#otdata_table").dataTable(dataTableTh);
        }
        $("#FilterLeaveRequest").modal("toggle");
      });//getrequest
    });// modal filter

  });
});
