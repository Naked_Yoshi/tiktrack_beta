var token = window.localStorage.getItem('token');

$(document).ready(function() {
  $("#otdata_body").load();
});

(function($){
  $.fn.load = function(){
    var table = $("#otdata_table");
    var otdata_body = $("#otdata_body");
    var url = getApi("OTData/getRejected");
    $.ajax({
      type:'GET',
      url: url,
      headers: {'Authorization': token},
    })
    .done(function(data) {
      //mapping data to table  and create html
      console.log(data);
      if(data["Has"] )
      {
        console.log(data["data"]);
        var no = 1;
        otdata_body.html("");
        for (var i = 0; i < data["data"].length; i++) {
          var tableotdata = "";
              if(data["data"][i]["is_urgent"] == 1)
              {
                tableotdata += "<tr class='danger'>";
              }else{
                tableotdata += "<tr>";
              }

             tableotdata += "<td class='text-center'>";
             tableotdata += no;
             tableotdata += "</td>";

             tableotdata += "<td>";
             tableotdata += data["data"][i]["fullName"];
             tableotdata += "</td>";

             tableotdata += "<td class='text-center'>";
             tableotdata += data["data"][i]["requestDate"]+"<br/>"+data["data"][i]["requestTime"];
             tableotdata += "</td>";

             tableotdata += "<td>";
             tableotdata += data["data"][i]["detail"];
             tableotdata += "</td>";

             tableotdata += "<td class='text-center'>";
             tableotdata += data["data"][i]["date_start"]+"<br/>"+data["data"][i]["time_start"];
             tableotdata += "</td>";

             tableotdata += "<td class='text-center'>";
             tableotdata += data["data"][i]["date_end"]+"<br/>"+data["data"][i]["time_end"];
             tableotdata += "</td>";

             tableotdata += "<td class='text-center'>";
             tableotdata += "<span class='mytooltip tooltip-effect-1'><span class='tooltip-item'>";
             tableotdata += "("+data["data"][i]["multiple"] + ")";
             tableotdata += "</span>";
             tableotdata += "<span class='tooltip-content clearfix>'";
             tableotdata += "<span class='tooltip-text'>" + "<div class='text-center'>"+data["data"][i]["multiple_str"] + " </div></span>";
             tableotdata += "</td>";

             tableotdata += "<td class='text-center'>";
             tableotdata += data["data"][i]["siteName"];
             tableotdata += "</td>";

             tableotdata += "<td class='text-center'>";
             tableotdata += data["data"][i]["brancheName"];
             tableotdata += "</td>";

             tableotdata += "<td class='text-center'> <div class='label label-table label-success'>";
             tableotdata += data["data"][i]["reqTotalHours"];
             tableotdata += "</div></td>";

             tableotdata += "</td>";
             tableotdata += "<td class='text-center'>";
             tableotdata += "<span class='mytooltip tooltip-effect-1'><span class='tooltip-item2'>";
             if(data["data"][i]["status"] == 2)
             {

               tableotdata += "<div class='label label-table label-danger'>";
               tableotdata += data["data"][i]["ot_status_str"];
             }else {
               tableotdata += "<div class='label label-table label-warning'>";
               tableotdata += data["data"][i]["ot_status_str"];
             }
             tableotdata += "</span>";
             tableotdata += "<span class='tooltip-content clearfix>'";
             tableotdata += "<span class='tooltip-text'>" + "<div class='text-center'>by : ";
             if(data["data"][i]["status"] == 2)
             {
               tableotdata += data["data"][i]["updateUser"];
             }else {
               tableotdata += data["data"][i]["fullName"];
             }
             tableotdata  += "<br/>  ( " + data["data"][i]["updateDate"]+" - " + data["data"][i]["updateTime"] +" )</div></span>";
             tableotdata += "</span></span>"
             tableotdata += "</div></td>";

             // tableotdata += "<td class='text-center'>";
             // tableotdata += data["data"][i]["updateDate"]+"<br/>"+data["data"][i]["updateTime"];
             // tableotdata += "</td>";

             tableotdata += "</tr>";
             otdata_body.append(tableotdata);
             no++;
        }

        table.DataTable(dataTableTh);

      }else {
        //no request for OT
        var tableNodata = "<tr>";
        tableNodata += "<td colspan='12' class='text-center'>ไม่พบข้อมูล";
        tableNodata += "</td>";
        tableNodata += "</tr>";
        otdata_body.append(tableNodata);
      }

    });
    //end done load data
  }
})(jQuery);
// $('.otdata_body').ready(function() {
//
// });
//end raedy event
