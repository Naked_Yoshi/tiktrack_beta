var d = new Date();
var g_month = d.getMonth()+1;
var g_year = d.getFullYear();

var token = window.localStorage.getItem('token');
var otdata = [];
$(document).ready(function() {
  $("#otdata_body").load();
});

(function($){
  $.fn.load = function(){
    var table = $("#otdata_table");
    var otdata_body = $("#otdata_body");
    var url = getApi("OTData/getWaitingApprove");
    otdata_body.html("");
    $.ajax({
      type:'GET',
      url: url,
      headers: {'Authorization': token},
      data: {"month":"", "year":""}
    })
    .done(function(data) {
      //mapping data to table  and create html
      // console.log(data);
      if(data["Has"] )
      {
        // console.log(data["data"]);
        var no = 1;

        otdata  = data["data"];
        for (var i = 0; i < data["data"].length; i++) {
          var tableotdata = "";
              if(data["data"][i]["is_urgent"] == 1)
              {
                tableotdata += "<tr class='danger'>";
              }else{
                tableotdata += "<tr>";
              }

             // tableotdata += "<td>";
             // tableotdata += "<div class='checkbox'><input class='ot-item' id='"+data["data"][i]["otd_id"]+"' type='checkbox'/><label></label></div>";
             // tableotdata += "</td>";

             tableotdata += "<td class='text-center'>";
             tableotdata += no;
             tableotdata += "</td>";

             tableotdata += "<td>";
             tableotdata += data["data"][i]["fullName"];
             tableotdata += "</td>";

             tableotdata += "<td class='text-center'>";
             tableotdata += data["data"][i]["requestDate"]+"<br/>"+data["data"][i]["requestTime"];
             tableotdata += "</td>";

             tableotdata += "<td>";
             tableotdata += data["data"][i]["detail"];
             tableotdata += "</td>";

             tableotdata += "<td class='text-center'>";
             tableotdata += data["data"][i]["date_start"]+"<br/>"+data["data"][i]["time_start"];
             tableotdata += "</td>";

             tableotdata += "<td class='text-center'>";
             tableotdata += data["data"][i]["date_end"]+"<br/>"+data["data"][i]["time_end"];
             tableotdata += "</td>";

             tableotdata += "<td class='text-center'>";
             tableotdata += "<span class='mytooltip tooltip-effect-1'><span class='tooltip-item'>";
             tableotdata += "("+data["data"][i]["multiple"] + ")";
             tableotdata += "</span>";
             tableotdata += "<span class='tooltip-content clearfix>'";
             tableotdata += "<span class='tooltip-text'>" + "<div class='text-center'>"+data["data"][i]["multiple_str"] + " </div></span>";
             tableotdata += "</td>";

             tableotdata += "<td class='text-center'>";
             if (data["data"][i]["code_site"] == null || data["data"][i]["siteName"] == "") {
              tableotdata += data["data"][i]["brancheName"];
            }else
            {
              tableotdata += data["data"][i]["code_site"];
            }
             tableotdata += "</td>";

             tableotdata += "<td class='text-center'>";
             if (data["data"][i]["expenses_paid"] == null) {
              tableotdata += "-";
             }else{
              tableotdata += data["data"][i]["expenses_paid"];
             }             
             tableotdata += "</td>";

             tableotdata += "<td class='text-center'> <div class='label label-table label-success'>";
             tableotdata += data["data"][i]["reqTotalHours"];
             tableotdata += "</div></td>";

             tableotdata += "</td>";
             tableotdata += "<td class='text-center'>";
             tableotdata += "<div class='btn-group'>";
             tableotdata += "<button class='btn btn-sm btn-default btn-outline waves-effect btn-ot-approve' onclick='ot_approve(this)' data-otindex='"+
                            data["data"][i]["otd_id"] +"'><span class='fa fa-check-square'></span></button>" +
                            "<button class='btn btn-sm btn-default btn-outline waves-effect btn-ot-reject' onclick='ot_reject(this)' data-otindex='"+
                            data["data"][i]["otd_id"] +"'><span class='fa fa-times ' aria-hidden='false'></span></button>";
             tableotdata += "</div>";
             tableotdata += "</td>";
             tableotdata += "</tr>";

             otdata_body.append(tableotdata);

             $(".ot-item").click(function(event) {
               $(".approve_num").html("");
               $(".reject_num").html("");

               var li = document.getElementById(this.id);
               if( li.checked)
               {
                 li.classList.add('active');
               }else {
                 li.classList.remove('active');
               }
               //get ot_id from all available item
               var num = $(".ot-item.active").length;
               if(num > 1)
               {
                 $(".ot-all-btn").removeClass('disabled');
                 $(".ot-all-btn").removeAttr('disabled');
                 $(".approve_num").html(" ( "+num+" )");
                 $(".reject_num").html(" ( "+num+" )");
               }else {
                 $(".ot-all-btn").addClass('disabled');
                 $(".ot-all-btn").attr('disabled');
                 $(".approve_num").html("");
                 $(".reject_num").html("");
               }

               if (num == otdata.length) {
                   $(".approve_num").html("ทั้งหมด");
                   $(".reject_num").html("ทั้งหมด");
                 }
             });

             no++;

        }

        table.DataTable(dataTableTh);

      }else {
        //no request for OT
        var tableNodata = "<tr>";
        tableNodata += "<td colspan='12' class='text-center'>ไม่พบข้อมูล";
        tableNodata += "</td>";
        tableNodata += "</tr>";
        otdata_body.append(tableNodata);
      }


    });
    //end done load data
  }
  $(".btn-report-otrequest").click(function(){
      var url = base_url('OTdata/ReportRequest');
      $.ajax({
        url: url,
        type: 'POST',
      })
      .done(function(data) {
          $(".modal-area").html(data);
          $("#OTrequest").modal("toggle");

          getEmp();
          callpicker();

          var url = getApi("sites/getall");
          $.ajax({
            type:'GET',
            url : url,
            headers: {'Authorization' : token}
          }).done(function(data){
            if (data["Has"] == false) {
              var r = "";
              r += "<option value='0'>";
              r += "เลือกไซต์";
              r += "</option>";
            }else{
              var r = "";
              r += "<option value='0'>";
              r += "เลือกไซต์";
              r += "</option>";
              for (var i = 0; i < data["data"].length; i++) {
                r += "<option value='"+ data["data"][i]["site_id"] +"'>";
                r += data["data"][i]["siteName"];
                r+= "</option>" ;
              }
              $("#cb_site").append(r);
            }

            $("#cb_site").change(function(event) {
              // console.log("1111");
              var site_id = $("#cb_site").val();
              $("#site").val(site_id);
              var url = getApi("branches/getall");
            $.ajax({
              type:'GET',
              url : url,
              headers: {'Authorization' : token},
              data :{"site_id" : site_id}
            }).done(function(data){
              if (data["Has"]  == false) {
                var op_branche = "<option value='0'>";
                op_branche += "ไม่มีสาขา";
                op_branche += "</option>";
              }else{
                var r = "<option value='0'>";
                op_branche += "เลือกสาขา";
                op_branche += "</option>";
                for (var i = 0; i < data["data"].length; i++)
                 {
                  op_branche += "<option value='"+ data["data"][i]["branche_id"] +"'>";
                  op_branche += data["data"][i]["brancheName"];
                  op_branche += "</option>" ;
                }
                  $("#cb_branche").append(op_branche);

                      }


                  });
                 });

                $("#cb_branche").change(function(event){
                  $("#branche").val($("#cb_branche").val());
                });


          $("#selectType").change(function(event) {
                var type = $(this).val();
                console.log(type);
                if (type == 0) {
                    $("#by_person").show();
                    $("#by_site").hide();
                }else{
                     $("#by_person").hide();
                     $("#by_site").show();
                } // end if type = 1

             });

             $(".submit-report-print-ot-complete").click(function(){
               $(".report_form").submit();
             });
      //     $("#positionDep").depOptions();
      //     // $("#positionDep").click(function(event){
      //     //    // console.log("positionDep");
      //     //    $("#positionRoot").positionOptions("", $("#positionDep").val());
      //     // });
      //     //$("#positionRoot").positionOptions();
      //     $("#positionDep").click(function(event){
      //       console.log("1111");
      //        $("#positionRoot").positionOptions("", $("#positionDep").val());
      //          });
      //     $(".submit-create-position").click(function(){
      //         var in_validate =  $("#positionCreateForm .validate").validate_blank();
      //         if (in_validate) {
      //             var created = $("#positionCreateForm").createPosition();
      //         }
      //         // input.validate_blank();
      //     });
      });
    });
  });
})(jQuery);




$("#select-all").click(function(event) {

  $(".approve_num").html("");
  $(".reject_num").html("");

  if($("#select-all").prop('checked'))
  {
    $("input:checkbox").not(this).prop('checked',true);
    $(".ot-item").addClass('active');
  }else {
    $("input:checkbox").not(this).prop('checked',false);
    $(".ot-item").removeClass('active');
  }
  //get ot_id from all available item
  var num = $(".ot-item.active").length;
  if(num > 1)
  {
    $(".ot-all-btn").removeClass('disabled');
    $(".ot-all-btn").removeAttr('disabled');
    $(".approve_num").html(" ( "+num+" )");
    $(".reject_num").html(" ( "+num+" )");
  }else {
    $(".ot-all-btn").addClass('disabled');
    $(".ot-all-btn").attr('disabled');
    $(".approve_num").html("");
    $(".reject_num").html("");
  }

  if (num == otdata.length) {
      $(".approve_num").html("ทั้งหมด");
      $(".reject_num").html("ทั้งหมด");
    }
});


$(".ot-all-approve").click(function() {
  var table = $("#otdata_table");
  var myArray = [];
  var dataSelect = $(".ot-item.active");
  for (var i = 0; i < dataSelect.length; i++) {
    var element = dataSelect[i];
    // console.log(element.id);
    myArray.push(element.id);
  }
  // console.log(myArray);
  var askStr;
  if (dataSelect.length == otdata.length) {
     askStr = "ทุกรายการ";
  }else {
     askStr = dataSelect.length + " รายการ";
  }
  var url_approve = getApi("OTData/approveot");
  swal({
        title:"คุณแน่ใจ ?",
        text: "ที่ต้องการอนุมัติ การขอล่วงเวลา " + askStr,
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: "ใช่, อนุมัติเลย!",
        cancelButtonText: "ไม่, ยกเลิก!",
        closeOnConfirm: false,
        closeOnCancel: true
    }).then((result) => {
      if (result.value) {
        $.ajax({
          type:'POST',
          url: url_approve,
          headers: {'Authorization': token},
          data: {'arr_users': myArray}
        }).done(function(data) {
          if(data)
          {
            //success update
            $.toast({ heading: "SUCCESS",text: "แก้ไขเรียบร้อยแล้ว",position: 'top-right',loaderBg:'#ff6849',icon: 'success',hideAfter: 3500,stack: 6});
            table.dataTable().fnDestroy();
            $("#otdata_body").load();
          }else {
            $.toast({ heading: "FAILED",text: "ไม่สามารถแก้ไขข้อมูลได้",position: 'top-right',loaderBg:'#ff6849',icon: 'error',hideAfter: 3500,stack: 6});
          }
        });
        //end connect to api
      }
    })
});

$(".ot-all-reject").click(function() {
  var table = $("#otdata_table");
  var myArray = [];
  var dataSelect = $(".ot-item.active");
  for (var i = 0; i < dataSelect.length; i++) {
    var element = dataSelect[i];
    // console.log(element.id);
    myArray.push(element.id);
  }
  // console.log(myArray);
  var askStr;
  if (dataSelect.length == otdata.length) {
     askStr = "ทุกรายการ";
  }else {
     askStr = dataSelect.length + " รายการ";
  }


  var url_approve = getApi("OTData/rejectot");
  // console.log(otData);
  swal({
        title:"คุณแน่ใจ ?",
        text: "ที่ต้องการยกเลิก การขอล่วงเวลา "+askStr,
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: "ใช่, ยกเลิกเลย!",
        cancelButtonText: "ไม่!"
    }).then((result) => {
      if (result.value) {
        $.ajax({
          type:'POST',
          url: url_approve,
          headers: {'Authorization': token},
          data: {'arr_users': myArray}
        }).done(function(data) {
          if(data)
          {
            //success update
            $.toast({ heading: "SUCCESS",text: "แก้ไขเรียบร้อยแล้ว",position: 'top-right',loaderBg:'#ff6849',icon: 'success',hideAfter: 3500,stack: 6});
            table.dataTable().fnDestroy();
            $("#otdata_body").load();
          }else {
            $.toast({ heading: "FAILED",text: "ไม่สามารถแก้ไขข้อมูลได้",position: 'top-right',loaderBg:'#ff6849',icon: 'error',hideAfter: 3500,stack: 6});
          }
        });
        //end connect to api
      }
    })

});

function ot_approve(el){
      var myArray = [];
      myArray.push($(el).data("otindex"));

      var url_approve = getApi("OTData/approveot");
      swal({
            title:"คุณแน่ใจ ?",
            text: "ที่ต้องการอนุมัติ การขอล่วงเวลารายการนี้",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: "ใช่, อนุมัติเลย!",
            cancelButtonText: "ไม่, ยกเลิก!",
            closeOnConfirm: false,
            closeOnCancel: true
        }).then((result) => {
          if (result.value) {
            $.ajax({
              type:'POST',
              url: url_approve,
              headers: {'Authorization': token},
              data: {'arr_users': myArray}
            }).done(function(data) {
              if(data)
              {
                //success update
                $.toast({ heading: "SUCCESS",text: "แก้ไขเรียบร้อยแล้ว",position: 'top-right',loaderBg:'#ff6849',icon: 'success',hideAfter: 3500,stack: 6});
                $("#otdata_table").dataTable().fnDestroy();
                $("#otdata_body").load();
              }else {
                $.toast({ heading: "FAILED",text: "ไม่สามารถแก้ไขข้อมูลได้",position: 'top-right',loaderBg:'#ff6849',icon: 'error',hideAfter: 3500,stack: 6});
              }
            });
            //end connect to api
          }
        })
     //end check alert
    }

    function ot_reject(el){
      var myArray = [];
      // var otData = $(this).data("otindex");
      myArray.push($(el).data("otindex"));
      var url_approve = getApi("OTData/rejectot");
      // console.log(otData);
      swal({
            title:"คุณแน่ใจ ?",
            text: "ที่ต้องการยกเลิก การขอล่วงเวลารายการนี้",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: "ใช่, ยกเลิกเลย!",
            cancelButtonText: "ไม่!"
        }).then((result) => {
          if (result.value) {
            $.ajax({
              type:'POST',
              url: url_approve,
              headers: {'Authorization': token},
              data: {'arr_users': myArray}
            }).done(function(data) {
              if(data)
              {
                //success update
                $.toast({ heading: "SUCCESS",text: "แก้ไขเรียบร้อยแล้ว",position: 'top-right',loaderBg:'#ff6849',icon: 'success',hideAfter: 3500,stack: 6});
                table.dataTable().fnDestroy();
                $("#otdata_body").load();
              }else {
                $.toast({ heading: "FAILED",text: "ไม่สามารถแก้ไขข้อมูลได้",position: 'top-right',loaderBg:'#ff6849',icon: 'error',hideAfter: 3500,stack: 6});
              }
            });
            //end connect to api
          }
        })
     //end check alert
    }

    $(".btn_filter").click(function(){
      $.ajax({
        url: base_url("OTdata/FilterLeaveRequest"),
        type: 'POST'
      }).done(function(data) {
        $(".modal-area").html(data);
        $("#FilterLeaveRequest").modal("toggle");

        $("#select_searchmonth").val(g_month);
        $("#select_searchyear").val(g_year);

        $(".filter-Leave-Request").click(function(){
          var month = $("#select_searchmonth").val();
          var year = $("#select_searchyear").val();

          console.log(month+" "+year);

          url = getApi("OTData/getWaitingApprove");

          $.ajax({
            url: url,
            type: 'GET',
            headers: {'Authorization': token},
            data: {"month":month, "year":year}
          }).done(function(data) {

            $("#otdata_table").dataTable().fnDestroy();
            // $("#otdata_body").html("");

            if (data["Has"] == false) {
              $("#otdata_body").html("");
              $("#otdata_table").dataTable(dataTableTh);
            }else{
              $("#otdata_body").html("");
              // console.log("testttt");
              no = 1;
              for (var i = 0; i < data["data"].length; i++) {
                var tableotdata = "";
                    if(data["data"][i]["is_urgent"] == 1)
                    {
                      tableotdata += "<tr class='danger'>";
                    }else{
                      tableotdata += "<tr>";
                    }

                   // tableotdata += "<td>";
                   // tableotdata += "<div class='checkbox'><input class='ot-item' id='"+data["data"][i]["otd_id"]+"' type='checkbox'/><label></label></div>";
                   // tableotdata += "</td>";

                   tableotdata += "<td class='text-center'>";
                    tableotdata += no;
                    tableotdata += "</td>";

                    tableotdata += "<td>";
                    tableotdata += data["data"][i]["fullName"];
                    tableotdata += "</td>";

                    tableotdata += "<td class='text-center'>";
                    tableotdata += data["data"][i]["requestDate"]+"<br/>"+data["data"][i]["requestTime"];
                    tableotdata += "</td>";

                    tableotdata += "<td>";
                    tableotdata += data["data"][i]["detail"];
                    tableotdata += "</td>";

                    tableotdata += "<td class='text-center'>";
                    tableotdata += data["data"][i]["date_start"]+"<br/>"+data["data"][i]["time_start"];
                    tableotdata += "</td>";

                    tableotdata += "<td class='text-center'>";
                    tableotdata += data["data"][i]["date_end"]+"<br/>"+data["data"][i]["time_end"];
                    tableotdata += "</td>";

                    tableotdata += "<td class='text-center'>";
                    tableotdata += "<span class='mytooltip tooltip-effect-1'><span class='tooltip-item'>";
                    tableotdata += "("+data["data"][i]["multiple"] + ")";
                    tableotdata += "</span>";
                    tableotdata += "<span class='tooltip-content clearfix>'";
                    tableotdata += "<span class='tooltip-text'>" + "<div class='text-center'>"+data["data"][i]["multiple_str"] + " </div></span>";
                    tableotdata += "</td>";

                    tableotdata += "<td class='text-center'>";
                    if (data["data"][i]["code_site"] == null || data["data"][i]["siteName"] == "") {
                      tableotdata += data["data"][i]["brancheName"];
                    }else
                    {
                      tableotdata += data["data"][i]["code_site"];
                    }
                    tableotdata += "</td>";

                    tableotdata += "<td class='text-center'>";
                    if (data["data"][i]["expenses_paid"] == null) {
                      tableotdata += "-";
                     }else{
                      tableotdata += data["data"][i]["expenses_paid"];
                     }             
                    tableotdata += "</td>";

                    tableotdata += "<td class='text-center'> <div class='label label-table label-success'>";
                    tableotdata += data["data"][i]["reqTotalHours"];
                    tableotdata += "</div></td>";

                    tableotdata += "</td>";
                    tableotdata += "<td class='text-center'>";
                    tableotdata += "<div class='btn-group'>";
                    tableotdata += "<button class='btn btn-sm btn-default btn-outline waves-effect btn-ot-approve' onclick='ot_approve(this)' data-otindex='"+
                                    data["data"][i]["otd_id"] +"'><span class='fa fa-check-square'></span></button>" +
                                    "<button class='btn btn-sm btn-default btn-outline waves-effect btn-ot-reject' onclick='ot_reject(this)' data-otindex='"+
                                    data["data"][i]["otd_id"] +"'><span class='fa fa-times ' aria-hidden='false'></span></button>";
                    tableotdata += "</div>";
                    tableotdata += "</td>";
                   tableotdata += "</tr>";
                no++;
                $("#otdata_body").append(tableotdata)
              }
              $("#otdata_table").dataTable(dataTableTh);
            }
              $("#FilterLeaveRequest").modal("toggle");
          });//getrequest
        });// modal filter

      });
    });

  function getEmp()
  {
    var url = getApi("users/getall");
    $.ajax({
      url: url,
      type: 'GET',
      headers: {'Authorization': token}
    }).done(function(data) {

      $('#search_user_report').autoComplete({
          minChars: 0,
          source: function(term, suggest){
              term = term.toLowerCase();
              var choices = [];
              for (var i = 0; i < data["data"].length; i++) {
                choices.push([data["data"][i]["fullname"],data["data"][i]["employee_id"]]);
              }
              var suggestions = [];
              for (i=0;i<choices.length;i++)
                  if (~(choices[i][0]+' '+choices[i][1]).toLowerCase().indexOf(term)) suggestions.push(choices[i]);
              suggest(suggestions);
          },
          renderItem: function (item, search){
            search = search.replace(/[-\/\\^$*+?.()|[\]{}]/g, '\\$&');
            var re = new RegExp("(" + search.split(' ').join('|') + ")", "gi");
            return '<div class="autocomplete-suggestion" data-langname="'+item[0]+'" data-lang="'+item[1]+'" data-val="'+search+'">'+item[0].replace(re, "<b>$1</b>")+'</div>';
          },
          onSelect: function(e, term, item){
              // var id = window.atob(item.data('lang'))
              console.log('Item "'+item.data('langname')+' ('+item.data('lang')+')" selected by '+(e.type == 'keydown' ? 'pressing enter or tab' : 'mouse click')+'.');
              $('#search_user_report').val(item.data('langname'));
              $('#index_emp').val(item.data('lang'))
              // console.log(item.data('lang'));
          }
      });
    });
  }

  function callpicker(){
    $('.mydatepicker, #datepicker').datepicker({
        autoclose: true,
        todayHighlight: true,
        clearBtn:true,
        endDate:"0d",
        language: 'th'
    });
  }
