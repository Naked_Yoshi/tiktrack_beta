var d = new Date();
var g_month = d.getMonth()+1;
var g_year = d.getFullYear();

var token = window.localStorage.getItem('token');

$(document).ready(function() {
  $("#otdata_body").load();

});

(function($){
  $.fn.load = function(){
    var table = $("#otdata_table");
    var otdata_body = $("#otdata_body");
    var url = getApi("OTData/getApprove");
    $.ajax({
      type:'GET',
      url: url,
      headers: {'Authorization': token},
      data: {'month':"", 'year':""}
    })
    .done(function(data) {
      //mapping data to table  and create html
      // console.log(data);
      if(data["Has"] )
      {
        // console.log(data["data"]);
        var no = 1;
        otdata_body.html("");
        for (var i = 0; i < data["data"].length; i++) {
          var tableotdata = "";
              if(data["data"][i]["is_urgent"] == 1)
              {
                tableotdata += "<tr class='danger'>";
              }else{
                tableotdata += "<tr>";
              }

            tableotdata += "<td>";
            if (data["data"][i]["actualOutTime"] == "ยังไม่มีข้อมูล") {
              tableotdata += "<div class='checkbox'><input class='ot-item' id='"+data["data"][i]["otd_id"]+"' onclick='chkactive(this)' type='checkbox' disabled /><label></label></div>";
            }else{
                tableotdata += "<div class='checkbox'><input class='ot-item' id='"+data["data"][i]["otd_id"]+"' onclick='chkactive(this)' type='checkbox'/><label></label></div>";
            }
            tableotdata += "</td>";

             tableotdata += "<td class='text-center'>";
             tableotdata += no;
             tableotdata += "</td>";

             tableotdata += "<td>";
             tableotdata += data["data"][i]["fullName"];
             tableotdata += "</td>";

             tableotdata += "<td class='text-center'>";
             tableotdata += data["data"][i]["requestDate"]+"<br/>"+data["data"][i]["requestTime"];
             tableotdata += "</td>";

             tableotdata += "<td>";
             tableotdata += data["data"][i]["detail"];
             tableotdata += "</td>";

             tableotdata += "<td class='text-center'>";
             tableotdata += data["data"][i]["date_start"]+" - "+ data["data"][i]["date_end"] +"<br/>"+data["data"][i]["time_start"]+" - "+data["data"][i]["time_end"];
             tableotdata += "</td>";

             // tableotdata += "<td class='text-center'>";
             // tableotdata += data["data"][i]["date_end"]+"<br/>"+data["data"][i]["time_end"];
             // tableotdata += "</td>";

             tableotdata += "<td class='text-center'>";
             tableotdata += "<span class='mytooltip tooltip-effect-1'><span class='tooltip-item'>";
             tableotdata += "("+data["data"][i]["multiple"] + ")";
             tableotdata += "</span>";
             tableotdata += "<span class='tooltip-content clearfix>'";
             tableotdata += "<span class='tooltip-text'>" + "<div class='text-center'>"+data["data"][i]["multiple_str"] + " </div></span>";
             tableotdata += "</td>";

             tableotdata += "<td class='text-center'>";
             if (data["data"][i]["code_site"] == null || data["data"][i]["siteName"] == "") {
               tableotdata += data["data"][i]["brancheName"];
             }else
             {
               tableotdata += data["data"][i]["code_site"];
             }
             tableotdata += "</td>";

             tableotdata += "<td class='text-center'> <div class='label label-table label-success'>";
             tableotdata += data["data"][i]["reqTotalHours"];
             tableotdata += "</div></td>";

             tableotdata += "</td>";
             tableotdata += "<td class='text-center'>";
             tableotdata += "<span class='mytooltip tooltip-effect-1'><span class='tooltip-item2'>";
             if(data["data"][i]["status"] == 1)
             {
               tableotdata += "<div class='label label-table label-success'>";
               tableotdata += "Done";
             }else if(data["data"][i]["status"] == 7)
             {
              tableotdata += "<div class='label label-table label-info'>";
              tableotdata += "Approve Paid";
             }
             else {
               tableotdata += "<div class='label label-table label-inverse'>";
               tableotdata += "Working..";
             }
             tableotdata += "</span>";
             tableotdata += "<span class='tooltip-content clearfix>'";
             tableotdata += "<span class='tooltip-text'>" + "<div class='text-center'>by : ";
             if(data["data"][i]["status"] == 1)
             {
               tableotdata += data["data"][i]["updateUser"];
             }else {
               tableotdata += "-"
             }

             tableotdata  += "<br/>  ( " + data["data"][i]["updateDate"]+" - " + data["data"][i]["updateTime"] +" )</div></span>";
             tableotdata += "</span></span>"
             tableotdata += "</div></td>";

             tableotdata += "<td class='text-center'>";
             if(data["data"][i]["tat_id"])
             {
               tableotdata += data["data"][i]["actualOutDate"]+"<br/>"+data["data"][i]["actualOutTime"];
             }else{
               tableotdata += "-";
             }
             tableotdata += "</td>";


             tableotdata += "<td class='text-center'>";
             if(data["data"][i]["tat_id"])
             {
               tableotdata += data["data"][i]["actualWork"];
             }else{
               tableotdata += "-";
             }
             tableotdata += "</td>";

             tableotdata += "<td class='text-center'>";
             tableotdata += data["data"][i]["adjustOT"];
             tableotdata += "</td>";

             tableotdata += "<td class='text-center'>";
             if (data["data"][i]["expenses_paid"] == null || data["data"][i]["expenses_paid"] == "") {
               tableotdata += "-";
             }else{
               tableotdata += data["data"][i]["expenses_paid"];
             }

             tableotdata += "</td>";

             tableotdata += "<td class='text-center'>";
             tableotdata += "<button title='ปรับเวลาเบิก' class='btn btn-sm btn-default btn-outline waves-effect btn-emp-site-edit' onclick='adjustOT(this)' data-edit='"+ data["data"][i]["otd_id"] +"'><span class='fa fa-clock-o'></span></button>";
             tableotdata += "<button title='ปรับค่าใช้จ่าย' class='btn btn-sm btn-default btn-outline waves-effect btn-emp-site-edit' onclick='adjustExpenses(this)' data-edit='"+ data["data"][i]["otd_id"] +"'><span class='fa fa-edit'></span></button>";
             tableotdata += "<button title='ยกเลิก' class='btn btn-sm btn-default btn-outline waves-effect btn-emp-site-cancel' onclick='cancelOT(this)' data-cancel='"+ data["data"][i]["otd_id"] +"'><span class='fa fa-trash-o'></span></button>";
             tableotdata += "</td>";


             tableotdata += "</tr>";
             otdata_body.append(tableotdata);
             no++;
        }

        table.DataTable(dataTableTh);

      }else {
        //no request for OT
        // var tableNodata = "<tr>";
        // tableNodata += "<td colspan='15' class='text-center'>ไม่พบข้อมูล";
        // tableNodata += "</td>";
        // tableNodata += "</tr>";
        // otdata_body.append(tableNodata);
        table.DataTable(dataTableTh);
      }

    });
    //end done load data
  }
  $(".submit-report-print-ot-complete").click(function(){
    console.log("type");
});

  $(".btn-report-otcomplete").click(function(){
      var url = base_url('OTdata/ReportComplete');
      $.ajax({
        url: url,
        type: 'POST',
      })
      .done(function(data) {
          $(".modal-area").html(data);
          $("#OTcomplete").modal("toggle");
          callpicker();

          var url = getApi("sites/getall");
          $.ajax({
            type:'GET',
            url : url,
            headers: {'Authorization' : token}
          }).done(function(data){
            if (data["Has"] == false) {
              var r = "";
              r += "<option value='0'>";
              r += "เลือกไซต์";
              r += "</option>";
            }else{
              var r = "";
              r += "<option value='0'>";
              r += "เลือกไซต์";
              r += "</option>";
              for (var i = 0; i < data["data"].length; i++) {
                r += "<option value='"+ data["data"][i]["site_id"] +"'>";
                r += data["data"][i]["siteName"];
                r+= "</option>" ;
              }
              $("#site").append(r);
            }


            // $(".submit-report-print-ot-complete").click(function(){
            //   console.log("type");
              $("#site").change(function(event) {
                console.log("1111");
                var site_id = $("#site").val();
                var url = getApi("branches/getall");
              $.ajax({
                type:'GET',
                url : url,
                headers: {'Authorization' : token},
                data :{"site_id" : site_id}
              }).done(function(data){
                if (data["Has"]  == false) {
                  var op_branche = "<option value='0'>";
                  op_branche += "ไม่มีสาขา";
                  op_branche += "</option>";
                }else{
                  var r = "<option value='0'>";
                  op_branche += "เลือกสาขา";
                  op_branche += "</option>";
                  for (var i = 0; i < data["data"].length; i++)
                   {
                    op_branche += "<option value='"+ data["data"][i]["branche_id"] +"'>";
                    op_branche += data["data"][i]["brancheName"];
                    op_branche += "</option>" ;
                  }
                    $("#Report_branche").append(op_branche);

                        }


                    });
                   });
                });

          $("#selectType select").change(function(event) {
                var type = $(this).val();
                console.log(type);
                if (type == 0) {
                    $("#by_person").show();
                    $("#by_site").hide();
                }else{
                     $("#by_person").hide();
                     $("#by_site").show();


                } // end if type = 1

             });

             $(".submit-report-print-ot-complete").click(function(){
               // console.log("type");
               var employee_name= $("#print_input input").val();
               // var month = $("#searchmonth select").val();
               //  var year = $("#searchyear select").val();
                 // var dataset = {"employee_name":employee_name,"month":month,"year":year};
                  var input = $(".tat-modal-content input:text").not(".select-dropdown").not(".norequire");
                  var type = $("#selectType select").val();
                  if (type == 0) {
                     input.focus(function(event) {
                        $("#"+$(this).data("id")).removeClass('has-warning');
                         $("#feedback-"+$(this).data("id")).removeClass('active').html("");
                       });
                           var blank = input.validate_blank_input({"className":".tat-modal","msg" : "กรุณากรอกฟิลด์นี้"});
                             var index = $("#index").val();
                               if(index == ""){
                                   $("#print_input").addClass('has-warning');
                                     $("#feedback-print_input").addClass('active').html("ไม่พบข้อมูล");
                                       return;
                                       }else{
                                          $(".report_form").submit();
                                          }
                                           }else{
                                                $(".report_form").submit();
                               }
                        });

      });
  });


})(jQuery);

function adjustOT(el){
  var otd_id = $(el).data("edit");
  $.ajax({
    url: base_url("OTdata/AdjustOT"),
    type: 'POST',
  })
  .done(function(data) {
      $(".modal-area").html(data);
      $("#AdjustOTModel").modal("toggle");

      $.ajax({
        url: getApi("OTData/getByAdjustOT"),
        type: 'GET',
        headers: {'Authorization': token},
        data:{"otd_id": otd_id}
      }).done(function(data) {
        if (data["Has"] == false) {

        }else{
          var reqTotalHours = data["data"][0]["reqTotalHours"].split(':');
          var actualWork = data["data"][0]["actualWork"].split(':');
          var adjustTime = data["adjustTime"].split(':');

          $("#reqTotalHours").html(reqTotalHours[0] + " ชั่วโมง " + reqTotalHours[1] + " นาที");
          $("#actualWork").html(actualWork[0] + " ชั่วโมง " + actualWork[1] + " นาที");
          $("#excessWork").html(adjustTime[0] + " ชั่วโมง " + adjustTime[1] + " นาที");

          $(".btn-adjust-ot").click(function(){
            var req = {
              "otd_id" : otd_id,
              "adjustHour" : pad($("#adjustHour").val(), 2),
              "adjustMinute" : pad($("#adjustMinute").val(), 2)
            }

            // console.log(req);

            $.ajax({
              url: getApi("OTData/adjustOT"),
              type: 'POST',
              headers: {'Authorization': token},
              data:{"request": req}
            }).done(function(data) {
              if (data) {
                $.toast({ heading: "SUCCESS",text: "แก้ไขเรียบร้อยแล้ว",position: 'top-right',loaderBg:'#ff6849',icon: 'success',hideAfter: 3500,stack: 6});
                $("#adjustExpenses").modal("toggle");
                $("#otdata_table").dataTable().fnDestroy();
                $("#otdata_body").load();
              }else{
                $.toast({heading: "FAILED",text: "พบข้อผิดพลาดโปรดตรวจสอบ",position: 'top-right',loaderBg:'#ff6849',icon: 'error',hideAfter: 3500,stack: 6});
              }
            });
          });
        }
      });
    });
}

function cancelOT(el){
  var otd_id = $(el).data("cancel");
  // console.log(otd_id);
    swal({
          title:"คุณแน่ใจ ?",
          text: "คุณต้องการยกเลิกรายการนี้หรือไม่",
          type: "warning",
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: "ใช่ ยกเลิก",
          cancelButtonText: "ไม่",
          closeOnConfirm: false,
          closeOnCancel: true
      }).then((result) => {
        if (result.value) {
          $.ajax({
            url: getApi("OTData/cancelOT"),
            type: 'GET',
            headers: {'Authorization': token},
            data:{"id": otd_id}
          }).done(function(data) {
            if (data) {
              $.toast({ heading: "SUCCESS",text: "ยกเลิกเรียบร้อยแล้ว",position: 'top-right',loaderBg:'#ff6849',icon: 'success',hideAfter: 3500,stack: 6});
              $("#AdjustOTModel").modal("toggle");
              $("#otdata_table").dataTable().fnDestroy();
              $("#otdata_body").load();
            }else{
                $.toast({heading: "FAILED",text: "พบข้อผิดพลาดโปรดตรวจสอบ",position: 'top-right',loaderBg:'#ff6849',icon: 'error',hideAfter: 3500,stack: 6});
            }
          })
        }
      })
}

function adjustExpenses(el){
  var otd_id = $(el).data("edit");
  $.ajax({
    url: base_url("OTdata/AdjustExpenses"),
    type: 'POST',
  })
  .done(function(data) {
      $(".modal-area").html(data);
      $("#AdjustExpenses").modal("toggle");

      $.ajax({
        url: getApi("OTData/getByAdjustOT"),
        type: 'GET',
        headers: {'Authorization': token},
        data:{"otd_id": otd_id}
      }).done(function(data) {
        if (data["Has"] == false) {

        }else{
          var expenses_paid = data["data"][0]["expenses_paid"];
          var code_site = data["data"][0]["code_site"];


          if (expenses_paid == null || expenses_paid == "") {
            $("#txt_expenses").html("0 บาท");
            $("#adjustExpenses").val("");
          }else{
            $("#txt_expenses").html(expenses_paid + " บาท");
            $("#adjustExpenses").val(expenses_paid);
          }

          if (code_site == null || code_site == "") {
            $("#txt_code_site").html(data["data"][0]["brancheName"]);
            $("#adjustCodeSite").val("");
          }else{
            $("#txt_code_site").html(code_site);
            $("#adjustCodeSite").val(code_site);
          }


          $(".btn-adjust-ot").click(function(){
            var req = {
              "otd_id" : otd_id,
              "expenses_paid" : $("#adjustExpenses").val(),
              "code_site" : $("#adjustCodeSite").val()
            }

            // console.log(req);

            $.ajax({
              url: getApi("OTData/adjustOTpaid"),
              type: 'POST',
              headers: {'Authorization': token},
              data:{"request": req}
            }).done(function(data) {
              if (data) {
                $.toast({ heading: "SUCCESS",text: "แก้ไขเรียบร้อยแล้ว",position: 'top-right',loaderBg:'#ff6849',icon: 'success',hideAfter: 3500,stack: 6});
                $("#AdjustExpenses").modal("toggle");
                $("#otdata_table").dataTable().fnDestroy();
                $("#otdata_body").load();
              }else{
                $.toast({heading: "FAILED",text: "พบข้อผิดพลาดโปรดตรวจสอบ",position: 'top-right',loaderBg:'#ff6849',icon: 'error',hideAfter: 3500,stack: 6});
              }
            });
          });
        }
      });

    });// modal
}

function pad (str, max) {
  str = str.toString();
  return str.length < max ? pad("0" + str, max) : str;
}

function callpicker(){
  $('.mydatepicker, #datepicker').datepicker({
      autoclose: true,
      todayHighlight: true,
      clearBtn:true,
      endDate:"0d",
      language: 'th'
  });
}

function chkactive(el){
  var id = $(el).attr('id');
  // console.log(id);
  // console.log("เลือกจำนวน"+id.length+"คน");

  if($("#"+id).prop('checked')){

    if($("#"+id).prop('checked',true)){

        $("#"+id).addClass('active');

        $(".ot-all-approve").removeClass('disabled');
        $(".ot-all-approve").removeAttr('disabled');

        $(".ot-all-reject").removeClass('disabled');
        $(".ot-all-reject").removeAttr('disabled');
    }

  }else{
     $("#"+id).removeClass('active');

     $(".ot-all-approve").addClass('disabled');
     $(".ot-all-approve").attr('disabled');

     $(".ot-all-reject").addClass('disabled');
      $(".ot-all-reject").attr('disabled');
  }
} //function

function chk_all(){
  // console.log("คลิกปุ่มcheck box ทั้งหมดได้");

  if($("#select-all").prop('checked')){
    $("input:checkbox").not('[disabled=disabled]').prop('checked',true);
    //$(".all-approve").addClass('active');
    $("input:checkbox").addClass('active');

    /* ปุ่ม อนุมัติ และ ไม่อนุมัติ */

    // ลบคลาส disabled ใน  Class=" all-approve disabled"
    $(".ot-all-approve").removeClass('disabled');
    // ลบAttribute disabled ใน tag button โดยอ้างอิงจากคลาสที่ชื่อว่าall-approve  <button Class="all-approve" disabled>
    $(".ot-all-approve").removeAttr('disabled');

    // ลบคลาส disabled ใน  Class=" all-not-approve disabled"
    $(".ot-all-reject").removeClass('disabled');
    // ลบAttribute disabled ใน tag button โดยอ้างอิงจากคลาสที่ชื่อว่าall-not-approve  <button Class="all-not-approve" disabled>
    $(".ot-all-reject").removeAttr('disabled');

    // console.log("ติ๊กหมดแล้วนะ");
  }else {
    $("input:checkbox").not(this).prop('checked',false);
    // console.log("ยังติ๊กไม่หมด");
    //$(".all-approve").removeClass('active');
    $("input:checkbox").removeClass('active');


    /* ปุ่ม อนุมัติ และ ไม่อนุมัติ */

    // เพิ่มคลาส disabled ใน  Class=" all-approve disabled"
    $(".ot-all-approve").addClass('disabled');
    // เพิ่ม Attribute disabled ใน tag button โดยอ้างอิงจากคลาสที่ชื่อว่าall-approve  <button Class="all-approve" disabled>
    $(".ot-all-approve").attr('disabled');
    // เพิ่มคลาส disabled ใน  Class=" all-not-approve disabled"
    $(".ot-all-reject").addClass('disabled');
    // เพิ่ม Attribute disabled ใน tag button โดยอ้างอิงจากคลาสที่ชื่อว่าall-not-approve  <button Class="all-not-approve" disabled>
    $(".ot-all-reject").attr('disabled');


  }
}

$(".ot-all-approve").click(function() {
  keyFilter = $("div#otdata_table_filter input").val();
  var url = getApi("OTData/approveOTpaid");
    var myArray = [];
    var dataSelect = $(".ot-item.active").not('[disabled=disabled]');

  for (var i = 0; i < dataSelect.length; i++) {
    var element = dataSelect[i];
    console.log("สิ่งที่เลือกออกมาที่จะอนุมัติ",dataSelect[i]);
    myArray.push(element.id);
  }

  // console.log("คลิกฟังก์ชั่น all-approve ได้ และที่select ออกมาคือ", dataSelect);
  // console.log("ค่าอาเรย์ที่จะส่งไป",myArray);
  // console.log("จำนวน Array ที่จะส่งไป (นับจากค่าตัวแปร myArray)",myArray.length);

  swal({
        title:"คุณแน่ใจ ?",
        text: "ที่ต้องการอนุมัติเบิก จำนวน "+myArray.length+" รายการ",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: "ใช่, อนุมัติ!",
        cancelButtonText: "ไม่, ยกเลิก!",
        closeOnConfirm: false,
        closeOnCancel: true
    }).then((result) => {
      if (result.value) {
        console.log("result when click approve ");
        $.ajax({
          type:'POST',
          url: url,
          headers: {'Authorization': token},
          contentType : "application/x-www-form-urlencoded",
          data: {'arr_otd': myArray}

        }).done(function(data) {
          // console.log("เมื่อdone แล้ว",data);

          if(data){

            $("#select-all").prop('checked',false);

            $.toast({ heading: "APPROVED",text: "อนุมัติการเบิกล่วงเวลาเรียบร้อยแล้ว",position: 'top-right',loaderBg:'#ff6849',icon: 'success',hideAfter: 3500,stack: 6});

            $("#otdata_table").dataTable().fnDestroy();
            $("#otdata_body").load();

            // $('.dataTables_wrapper').data("dt-idx").val(3).trigger("click");

            /* ปุ่ม อนุมัติ และ ไม่อนุมัติ */

             // เพิ่มคลาส disabled ใน  Class=" all-approve disabled"
             $(".ot-all-approve").addClass('disabled');
             // เพิ่มAttribute disabled ใน tag button โดยอ้างอิงจากคลาสที่ชื่อว่าall-approve  <button Class="all-approve" disabled>
             $(".ot-all-approve").attr('disabled');

             // เพิ่มคลาส disabled ใน  Class=" all-not-approve disabled"
             $(".ot-all-reject").addClass('disabled');
             // เพิ่มAttribute disabled ใน tag button โดยอ้างอิงจากคลาสที่ชื่อว่าall-not-approve  <button Class="all-not-approve" disabled>
             $(".ot-all-reject").attr('disabled');



          } //if data


        }); //done func data
        // //end connect to api
      } // if result value
    }) //then result


}); // func approve btn

$(".ot-all-reject").click(function() {
  keyFilter = $("div#otdata_table_filter input").val();
  var url = getApi("OTData/rejectOtpaid");
    var myArray = [];
    var dataSelect = $(".ot-item.active").not('[disabled=disabled]');


  for (var i = 0; i < dataSelect.length; i++) {
    var element = dataSelect[i];
    console.log("สิ่งที่เลือกออกมาที่จะไม่อนุมัติ",dataSelect[i]);
    myArray.push(element.id);
  }

  // console.log("คลิกฟังก์ชั่น all-not-approve ได้ และที่select ออกมาคือ", dataSelect);
  console.log("ค่าอาเรย์ที่จะส่งไป",myArray);
  // console.log("จำนวน Array ที่จะส่งไป (นับจากค่าตัวแปร myArray)",myArray.length);

  swal({
        title:"คุณแน่ใจ ?",
        text: "ที่ต้องการไม่อนุมัติ จำนวน "+myArray.length+" คน",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: "ใช่, ไม่อนุมัติ!",
        cancelButtonText: "ไม่, ยกเลิก!",
        closeOnConfirm: false,
        closeOnCancel: true
    }).then((result) => {
      if (result.value) {
        console.log("result when click not approve ");
        $.ajax({
          type:'POST',
          url: url,
          headers: {'Authorization': token},
          contentType : "application/x-www-form-urlencoded",
          data: {'arr_otd': myArray}

        }).done(function(data) {
          // console.log("เมื่อdone แล้ว",data);

          if(data){

            $("#select-all").prop('checked',false);

            $.toast({ heading: "NOT APPROVED",text: "ไม่อนุมัติการลงเวลาเรียบร้อยแล้ว",position: 'top-right',loaderBg:'#ff6849',icon: 'info',hideAfter: 3500,stack: 6});

            $("#otdata_table").dataTable().fnDestroy();
            $("#otdata_body").load();

            /* ปุ่ม อนุมัติ และ ไม่อนุมัติ */

             // เพิ่มคลาส disabled ใน  Class=" all-approve disabled"
             $(".ot-all-approve").addClass('disabled');
             // เพิ่มAttribute disabled ใน tag button โดยอ้างอิงจากคลาสที่ชื่อว่าall-approve  <button Class="all-approve" disabled>
             $(".ot-all-approve").attr('disabled');

             // เพิ่มคลาส disabled ใน  Class=" all-not-approve disabled"
             $(".ot-all-reject").addClass('disabled');
             // เพิ่มAttribute disabled ใน tag button โดยอ้างอิงจากคลาสที่ชื่อว่าall-not-approve  <button Class="all-not-approve" disabled>
             $(".ot-all-reject").attr('disabled');

          }// if data


        }); //done(function(data) {
        // //end connect to api
      } // if result value
    }) //then result
}); // func not approve btn

$(".btn_filter").click(function(){
  $.ajax({
    url: base_url("OTdata/FilterLeaveRequest"),
    type: 'POST'
  }).done(function(data) {
    $(".modal-area").html(data);
    $("#FilterLeaveRequest").modal("toggle");

    $("#select_searchmonth").val(g_month);
    $("#select_searchyear").val(g_year);

    $(".filter-Leave-Request").click(function(){
      var month = $("#select_searchmonth").val();
      var year = $("#select_searchyear").val();

      url = getApi("OTData/getApprove");

      $.ajax({
        url: url,
        type: 'GET',
        headers: {'Authorization': token},
        data: {"month":month, "year":year}
      }).done(function(data) {

        $("#otdata_table").dataTable().fnDestroy();
        // $("#otdata_body").html("");

        if (data["Has"] == false) {
          $("#otdata_body").html("");
          $("#otdata_table").dataTable(dataTableTh);
        }else{
          $("#otdata_body").html("");
          console.log("testttt");
          no = 1;
          for (var i = 0; i < data["data"].length; i++) {
            var tableotdata = "";
                if(data["data"][i]["is_urgent"] == 1)
                {
                  tableotdata += "<tr class='danger'>";
                }else{
                  tableotdata += "<tr>";
                }

              tableotdata += "<td>";
              if (data["data"][i]["actualOutTime"] == "ยังไม่มีข้อมูล") {
                tableotdata += "<div class='checkbox'><input class='ot-item' id='"+data["data"][i]["otd_id"]+"' onclick='chkactive(this)' type='checkbox' disabled /><label></label></div>";
              }else{
                  tableotdata += "<div class='checkbox'><input class='ot-item' id='"+data["data"][i]["otd_id"]+"' onclick='chkactive(this)' type='checkbox'/><label></label></div>";
              }
              tableotdata += "</td>";

               tableotdata += "<td class='text-center'>";
               tableotdata += no;
               tableotdata += "</td>";

               tableotdata += "<td>";
               tableotdata += data["data"][i]["fullName"];
               tableotdata += "</td>";

               tableotdata += "<td class='text-center'>";
               tableotdata += data["data"][i]["requestDate"]+"<br/>"+data["data"][i]["requestTime"];
               tableotdata += "</td>";

               tableotdata += "<td>";
               tableotdata += data["data"][i]["detail"];
               tableotdata += "</td>";

               tableotdata += "<td class='text-center'>";
               tableotdata += data["data"][i]["date_start"]+" - "+ data["data"][i]["date_end"] +"<br/>"+data["data"][i]["time_start"]+" - "+data["data"][i]["time_end"];
               tableotdata += "</td>";

               // tableotdata += "<td class='text-center'>";
               // tableotdata += data["data"][i]["date_end"]+"<br/>"+data["data"][i]["time_end"];
               // tableotdata += "</td>";

               tableotdata += "<td class='text-center'>";
               tableotdata += "<span class='mytooltip tooltip-effect-1'><span class='tooltip-item'>";
               tableotdata += "("+data["data"][i]["multiple"] + ")";
               tableotdata += "</span>";
               tableotdata += "<span class='tooltip-content clearfix>'";
               tableotdata += "<span class='tooltip-text'>" + "<div class='text-center'>"+data["data"][i]["multiple_str"] + " </div></span>";
               tableotdata += "</td>";

               tableotdata += "<td class='text-center'>";
               if (data["data"][i]["code_site"] == null || data["data"][i]["siteName"] == "") {
                 tableotdata += data["data"][i]["brancheName"];
               }else
               {
                 tableotdata += data["data"][i]["code_site"];
               }
               tableotdata += "</td>";

               tableotdata += "<td class='text-center'> <div class='label label-table label-success'>";
               tableotdata += data["data"][i]["reqTotalHours"];
               tableotdata += "</div></td>";

               tableotdata += "</td>";
               tableotdata += "<td class='text-center'>";
               tableotdata += "<span class='mytooltip tooltip-effect-1'><span class='tooltip-item2'>";
               if(data["data"][i]["status"] == 1)
               {
                 tableotdata += "<div class='label label-table label-success'>";
                 tableotdata += "Done";
               }else if(data["data"][i]["status"] == 7)
               {
                tableotdata += "<div class='label label-table label-info'>";
                tableotdata += "Approve Paid";
               }
               else {
                 tableotdata += "<div class='label label-table label-inverse'>";
                 tableotdata += "Working..";
               }
               tableotdata += "</span>";
               tableotdata += "<span class='tooltip-content clearfix>'";
               tableotdata += "<span class='tooltip-text'>" + "<div class='text-center'>by : ";
               if(data["data"][i]["status"] == 1)
               {
                 tableotdata += data["data"][i]["updateUser"];
               }else {
                 tableotdata += "-"
               }

               tableotdata  += "<br/>  ( " + data["data"][i]["updateDate"]+" - " + data["data"][i]["updateTime"] +" )</div></span>";
               tableotdata += "</span></span>"
               tableotdata += "</div></td>";

               tableotdata += "<td class='text-center'>";
               if(data["data"][i]["tat_id"])
               {
                 tableotdata += data["data"][i]["actualOutDate"]+"<br/>"+data["data"][i]["actualOutTime"];
               }else{
                 tableotdata += "-";
               }
               tableotdata += "</td>";


               tableotdata += "<td class='text-center'>";
               if(data["data"][i]["tat_id"])
               {
                 tableotdata += data["data"][i]["actualWork"];
               }else{
                 tableotdata += "-";
               }
               tableotdata += "</td>";

               tableotdata += "<td class='text-center'>";
               tableotdata += data["data"][i]["adjustOT"];
               tableotdata += "</td>";

               tableotdata += "<td class='text-center'>";
               if (data["data"][i]["expenses_paid"] == null || data["data"][i]["expenses_paid"] == "") {
                 tableotdata += "-";
               }else{
                 tableotdata += data["data"][i]["expenses_paid"];
               }

               tableotdata += "</td>";

               tableotdata += "<td class='text-center'>";
               tableotdata += "<button title='ปรับเวลาเบิก' class='btn btn-sm btn-default btn-outline waves-effect btn-emp-site-edit' onclick='adjustOT(this)' data-edit='"+ data["data"][i]["otd_id"] +"'><span class='fa fa-clock-o'></span></button>";
               tableotdata += "<button title='ปรับค่าใช้จ่าย' class='btn btn-sm btn-default btn-outline waves-effect btn-emp-site-edit' onclick='adjustExpenses(this)' data-edit='"+ data["data"][i]["otd_id"] +"'><span class='fa fa-edit'></span></button>";
               tableotdata += "<button title='ยกเลิก' class='btn btn-sm btn-default btn-outline waves-effect btn-emp-site-cancel' onclick='cancelOT(this)' data-cancel='"+ data["data"][i]["otd_id"] +"'><span class='fa fa-trash-o'></span></button>";
               tableotdata += "</td>";


               tableotdata += "</tr>";
            no++;
            $("#otdata_body").append(tableotdata)
          }
          $("#otdata_table").dataTable(dataTableTh);
        }
          $("#FilterLeaveRequest").modal("toggle");
      });//getrequest

    });
  });
});//end function

//
// $('.otdata_body').ready(function() {
//
// });
//end raedy event
