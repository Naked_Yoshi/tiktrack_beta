var d = new Date();
var g_month = d.getMonth()+1;
var g_year = d.getFullYear();

var token = window.localStorage.getItem('token');
  $(document).ready(function() {
    $("#leave_body").getleave();
  });

  (function ($){
    $.fn.getleave = function (){
      var body = $(this);
      var urlgetRequestLeave = getApi("LeaveData/getApproveLeave");
      var segments      = location.pathname.split('/'),
      site_id = segments[segments.length - 1];
      $.ajax({
        url: urlgetRequestLeave,
        type: 'GET',
        headers: {'Authorization': token},
        data: {"month":"", "year":""}
      }).done(function(data) {
        if (data["Has"] == false) {
          // var r = "";
          // r += "<tr>";
          // r += "<td colspan='12' class='text-center'>ไม่พบข้อมูลดังกล่าว</td>";
          // r += "</tr>";
          // body.append(r);
          $("#leave_table").dataTable(dataTableTh);
        }else{
          no = 1;
          for (var i = 0; i < data["data"].length; i++) {
            var r = "";
            r += "<tr>";
            // r += "<td>";
            // r += "";
            // r += "</td>";
            r += "<td style='text-align:center'>";
            r += no;
            r += "</td>";
            r += "<td>";
            r += data["data"][i]["fullname"];
            r += "</td>";
            r += "<td>";
            r += data["data"][i]["createAt"];
            r += "</td>";
            r += "<td>";
            r += data["data"][i]["leave_type_name"];
            r += "</td>";
            r += "<td>";
            r += data["data"][i]["detail"];
            r += "</td>";
            r += "<td style='text-align:center'>";
            var levd_date_start = data["data"][i]["lev_data_date_start"].split(" ");
            r += convertDataFormat(levd_date_start[0]);
            r += "</td>";
            r += "<td style='text-align:center'>";
            var levd_date_end = data["data"][i]["lev_data_date_end"].split(" ");
            r += convertDataFormat(levd_date_end[0]);
            r += "</td>";
            r += "<td>";
            r += data["data"][i]["siteName"];
            r += "</td>";
            r += "<td>";
            r += data["data"][i]["brancheName"];
            r += "</td>";
            r += "<td class='text-center'>";
            r += "<span class='label label-success'>อนุมัติ</span>";
            // r += "<span class='mytooltip tooltip-effect-1'><span class='tooltip-item2'>";
            // r += "<span class='label label-success'>อนุมัติ</span>";
            // r += "</span>";
            // r += "<span class='tooltip-content clearfix>'";
            // r += "<span class='tooltip-text'>";

            // var arrApprove = {};
            // // arrApprove = getApproveName(data["data"][i]["lev_data_id"]);
            // // r += "<div class='text-center'>Approve Lv. 1 by : </div>";
            // // console.log(arrApprove["data"]);
            //  // for (var j = 0; j < arrApprove["data"].length; j++) {
            //   // r += "<div class='text-center'>Approve Lv. " +  arrApprove["data"][j]["lev_level"] + " by : "+ arrApprove["data"][j]["fullname"] +" </div>";
            //   r += "<div class='text-center'>&nbsp;</div>";
            //  // }

            // r  += "</span>";
            // r += "</span></span></div>"
            r += "</td>";
            r += "<td style='text-align:center'>";
            r += data["data"][i]["lev_data_d_day"]+" วัน "+data["data"][i]["lev_data_d_hour"]+" ชั่วโมง";
            r += "</td>";
            r += "<td class='text-center'>";
            r += "<button class='btn btn-sm btn-default btn-outline waves-effect btn-emp-cancel-leave' onclick='cancelLeave(this)' data-cancel='"+ data["data"][i]["lev_data_id"] +"'><span class='fa fa-trash-o'></span></button>";
            r += "</td>";
            r += "</tr>";
            no++;
            body.append(r)
          }
          $("#leave_table").dataTable(dataTableTh);
        }
      });
    }
  })(jQuery);


  $(".btn_print").click(function(){
    $.ajax({
      url: base_url("vacation/getReportLeaveRequest"),
      type: 'POST'
    }).done(function(data) {
      $(".modal-area").html(data);
      $("#LeaveRequestReport").modal("toggle");

      $("#searchmonth").val(g_month);
      $("#searchyear").val(g_year);

      $("#leave_request").val("1");
      var decode = jwt_decode(token);
      $("#auth").val(decode["employee_id"]);
      $("#company_id").val(decode["company_id"]);

      $(".submit-report-print-leave").click(function(){
        $(".report_form").submit();
      })
    });
  });

  function cancelLeave(el){
    var id = $(el).data("cancel");

    swal({
          title:"คุณแน่ใจ ?",
          text: "คุณต้องการยกเลิกรายการนี้หรือไม่",
          type: "warning",
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: "ใช่ ยกเลิก",
          cancelButtonText: "ไม่",
          closeOnConfirm: false,
          closeOnCancel: true
      }).then((result) => {
        if (result.value) {
          $.ajax({
            url: getApi("LeaveData/cancelLeave"),
            type: 'GET',
            headers: {'Authorization': token},
            data:{"id": id}
          }).done(function(data) {
            if (data) {
              $.toast({ heading: "SUCCESS",text: "ยกเลิกการลาเรียบร้อยแล้ว",position: 'top-right',loaderBg:'#ff6849',icon: 'success',hideAfter: 3500,stack: 6});
              $("#leave_table").dataTable().fnDestroy();
              $("#leave_body").getleave();
            }else{
                $.toast({heading: "FAILED",text: "พบข้อผิดพลาดโปรดตรวจสอบ",position: 'top-right',loaderBg:'#ff6849',icon: 'error',hideAfter: 3500,stack: 6});
            }
          })
        }
      })
  }

  function getApproveName(lev_data_id){
    var dataApprove = $.ajax({
      url: getApi("LeaveData/getApproveName"),
      type: 'GET',
      data: {"lev_data_id" : lev_data_id},
      headers: {'Authorization': token},
      async: false
    }).responseJSON;
    // console.log(dataApprove);
    return dataApprove;
  }

  $(".btn_filter").click(function(){
    $.ajax({
      url: base_url("vacation/FilterLeaveRequest"),
      type: 'POST'
    }).done(function(data) {
      $(".modal-area").html(data);
      $("#FilterLeaveRequest").modal("toggle");

      $("#select_searchmonth").val(g_month);
      $("#select_searchyear").val(g_year);

      $(".filter-Leave-Request").click(function(){
        var month = $("#select_searchmonth").val();
        var year = $("#select_searchyear").val();

        url = getApi("LeaveData/getApproveLeave");
        $("#leave_table").dataTable().fnDestroy();
        $("#leave_body").html("");
        $.ajax({
          url: url,
          type: 'GET',
          headers: {'Authorization': token},
          data: {"month":month, "year":year},
          beforeSend: function() {
            $(".preloader").show();
          },
          success: function(data) {
            $(".preloader").hide();
          }
        }).done(function(data) {

          if (data["Has"] == false) {
            var r = "";
            $("#leave_body").append(r);
            $("#leave_table").dataTable(dataTableTh);
          }else{
            no = 1;
            for (var i = 0; i < data["data"].length; i++) {
              var r = "";
              r += "<tr>";
              // r += "<td>";
              // r += "";
              // r += "</td>";
              r += "<td style='text-align:center'>";
              r += no;
              r += "</td>";
              r += "<td>";
              r += data["data"][i]["fullname"];
              r += "</td>";
              r += "<td>";
              r += data["data"][i]["createAt"];
              r += "</td>";
              r += "<td>";
              r += data["data"][i]["leave_type_name"];
              r += "</td>";
              r += "<td>";
              r += data["data"][i]["detail"];
              r += "</td>";
              r += "<td style='text-align:center'>";
              var levd_date_start = data["data"][i]["lev_data_date_start"].split(" ");
              r += convertDataFormat(levd_date_start[0]);
              r += "</td>";
              r += "<td style='text-align:center'>";
              var levd_date_end = data["data"][i]["lev_data_date_end"].split(" ");
              r += convertDataFormat(levd_date_end[0]);
              r += "</td>";
              r += "<td>";
              r += data["data"][i]["siteName"];
              r += "</td>";
              r += "<td>";
              r += data["data"][i]["brancheName"];
              r += "</td>";
              r += "<td class='text-center'>";
              r += "<span class='label label-success'>อนุมัติ</span>";
              // r += "<span class='mytooltip tooltip-effect-1'><span class='tooltip-item2'>";
              // r += "<span class='label label-success'>อนุมัติ</span>";
              // r += "</span>";
              // r += "<span class='tooltip-content clearfix>'";
              // r += "<span class='tooltip-text'>";

              // var arrApprove = {};
              // arrApprove = getApproveName(data["data"][i]["lev_data_id"]);
              // // r += "<div class='text-center'>Approve Lv. 1 by : </div>";
              // // console.log(arrApprove["data"]);
              //  for (var j = 0; j < arrApprove["data"].length; j++) {
              //   r += "<div class='text-center'>Approve Lv. " +  arrApprove["data"][j]["lev_level"] + " by : "+ arrApprove["data"][j]["fullname"] +" </div>";
              //  }

              // r  += "</span>";
              // r += "</span></span></div>";
              r += "</td>";
              r += "<td style='text-align:center'>";
              r += data["data"][i]["lev_data_d_day"]+" วัน "+data["data"][i]["lev_data_d_hour"]+" ชั่วโมง";
              r += "</td>";
              r += "<td class='text-center'>";
              r += "<button class='btn btn-sm btn-default btn-outline waves-effect btn-emp-cancel-leave' onclick='cancelLeave(this)' data-cancel='"+ data["data"][i]["lev_data_id"] +"'><span class='fa fa-trash-o'></span></button>";
              r += "</td>";
              r += "</tr>";
              no++;
              $("#leave_body").append(r);
            }
            $("#leave_table").dataTable(dataTableTh);
            $("#FilterLeaveRequest").modal("toggle");
          }

        });//getrequest
      });
    });
  });
