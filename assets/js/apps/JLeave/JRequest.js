var d = new Date();
var g_month = d.getMonth()+1;
var g_year = d.getFullYear();

var arrLevel = [];
var token = window.localStorage.getItem('token');
  $(document).ready(function() {
    $("#leave_body").getleave();

  });

  (function ($){
    $.fn.getleave = function (){
      var body = $(this);
      var decode = jwt_decode(token);
      // console.log(decode);
      var isadmin = decode["isAdmin"];

      if (isadmin == "1") {
        var urlgetRequestLeave = getApi("LeaveData/getRequestLeave");
      }else{
        var urlgetRequestLeave = getApi("LeaveData/getRequestLeave2019");
      }
      
      var segments      = location.pathname.split('/'),
      site_id = segments[segments.length - 1];
      body.html("");
      $.ajax({
        url: urlgetRequestLeave,
        type: 'GET',
        headers: {'Authorization': token},
        data: {"month":"", "year":"", "site":"", "branche":""}
      }).done(function(data) {
        console.log(data);
        if (data["Has"] == false) {
          var r = "";
          body.append(r)
          $("#leave_table").dataTable(dataTableTh);
        }else{
          no = 1;
          for (var i = 0; i < data["data"].length; i++) {
            var r = "";
            r += "<tr>";
            // r += "<td>";
            // r += "";
            // r += "</td>";
            r += "<td style='text-align:center'>";
            r += no;
            r += "</td>";
            r += "<td>";
            r += data["data"][i]["fullname"];
            r += "</td>";
            var leave_start = data["data"][i]["lev_data_date_start"].split(" ");
            r += "<td class = 'text-center'>";
            r += convertDataFormat(leave_start[0]) + "<br>" + leave_start[1];
            r += "</td>";
            var leave_end = data["data"][i]["lev_data_date_end"].split(" ");
            r += "<td class = 'text-center'>";
            r += convertDataFormat(leave_end[0]) + "<br>" + leave_end[1];
            r += "</td>";
            var createdAtLeave = data["data"][i]["createAt"].split(" ");
            r += "<td>";
            r += convertDataFormat(createdAtLeave[0]) + " " + createdAtLeave[1];
            r += "</td>";
            r += "<td>";
            r += data["data"][i]["leave_type_name"];
            r += "</td>";
            r += "<td>";
            r += data["data"][i]["siteName"];
            r += "</td>";
            r += "<td>";
            r += data["data"][i]["brancheName"];
            r += "</td>";
            r += "<td style='text-align:center'>";
            r += "<label class='label label-default'>ท่านยังไม่ได้อนุมัติ</label>";
            r += "</td>";
            r += "<td>";
            r += "<div class='btn-group'>";
            r += "<button class='btn btn-sm btn-default btn-outline waves-effect btn-wk-link' data-id='"+ data["data"][i]["lev_data_id"] +"' onclick='getApproveDialog(this)'><span class='fa fa-link'></span></button>";
            // r += "<button class='btn btn-sm btn-default btn-outline waves-effect'><span class='fa fa-trash'></span></button>";
            r += "</div>";
            r += "</td>";
            r += "</tr>";
            no++;
            body.append(r)
          }
          $("#leave_table").dataTable(dataTableTh);
        }
      });
    }
  })(jQuery);

  function getApproveDialog(el){
    var lev_detail_id = $(el).data("id");
    var getModal = base_url("vacation/getApproveModal");
    var level_approve = 0;

    $.ajax({
      url: getModal,
      type: 'GET'
    }).done(function(data) {
      $(".modal-area").html(data);
      $("#LeaveApproveModel").modal("toggle");

      $.ajax({
        url: getApi("LeaveData/levelapprove"),
        type: 'GET',
        headers: {'Authorization': token},
        data: {'lev_data_id' : lev_detail_id}
      }).done(function(data) {
        if (data["Has"]) {
          level_approve = parseInt(data["data"][0]["max_level"]);

          $.ajax({
            url: getApi("LeaveData/getByRequestLeave"),
            type: 'GET',
            headers: {'Authorization': token},
            data: {'lev_data_id' : lev_detail_id}
          }).done(function(data) {
            if (data["Has"] == false) {

            }else{
              var levd_date_start = data["data"][0]["lev_data_date_start"].split(" ");
              var levd_date_end = data["data"][0]["lev_data_date_end"].split(" ");

              // console.log(data["data"]);
              var arrDataLevel = [];
              $("#txt_detail").html(" - "+data["data"][0]["detail"]);
              $("#txt_leave_type").html(" - "+data["data"][0]["leave_type_name"]);
              $("#txt_sdate_edate").html(" - "+convertDataFormat(levd_date_start[0])+" ถึง "+convertDataFormat(levd_date_end[0]));
              $("#txt_stime_etime").html(" - "+levd_date_start[1]+" ถึง "+levd_date_end[1]);
              $("#txt_sum_leave").html(" - "+data["data"][0]["lev_data_d_day"]+" วัน "+data["data"][0]["lev_data_d_hour"]+" ชั่วโมง "+data["data"][0]["lev_data_d_minute"]+" นาที");
              arrDataLevel = data["data"];

              if (data["data"][0]["lev_approve_status"] == 3) {
                $("#tag_approve_status").html("<span class='label label-warning'>รออนุมัติ</span>");
              }else if(data["data"][0]["lev_approve_status"] == 1){
                $("#tag_approve_status").html("<span class='label label-success'>อนุมัติ</span>");
              }else if(data["data"][0]["lev_approve_status"] == 2){
                $("#tag_approve_status").html("<span class='label label-danger'>ไม่อนุมัติ</span>");
              }

              var lv = 1;
              for (var i = 0; i < level_approve; i++) {

                var r = "";
                r += "<tr>";
                r += "<td>";
                r += "<span class='label label-info'>Lv : " + lv + "</span> ";
                r += "</td>";
                r += "<td>";
                r += "<div id='field_approve"+ lv +"'></div>";
                r += "</td>";
                r += "</tr>"
                $("#leave_approve_body").append(r);
                lv++;
              }

                var child_level = 1;
                for (var i = 0; i < level_approve; i++) {
                    arrLevel.push(child_level);
                    child_level++;
                  }

                  $.ajax({
                    url: getApi("LeaveData/directapprove"),
                    type: 'GET',
                    headers: {'Authorization': token},
                    data: {'lev_data_id' : lev_detail_id}
                  }).done(function(data) {
                    // console.log(data);
                    var decode = jwt_decode(token);
                    var employee_id = window.atob(decode["employee_id"]);
                    var isAdmin = decode["isAdmin"];
                    // console.log(decode);
                    // console.log(employee_id);
                    for (var lv = 0; lv < arrLevel.length; lv++) {/// count LV
                      // console.log(arrLevel[lv]);
                      for (var i = 0; i < data["data"].length; i++) {/// get direct approver
                        // console.log(arrLevel[lv] + " " + data["data"][i]["lev_level"]);
                        if (arrLevel[lv] == data["data"][i]["lev_level"]) {
                          // console.log(data["data"][i]["levd_is_approve"]);
                          if (data["data"][i]["levd_is_approve"] == 3) {
                            // console.log(employee_id + " " + data["data"][i]["direct_approver"]);
                            if (employee_id == data["data"][i]["direct_approver"]  || isAdmin == "1") {
                              var r = ""
                              r += "<button class='btn btn-success btn-rounded' data-is_approve='1' data-level='"+ arrDataLevel[i]["lev_level"] +"' data-lev_data_id='"+ arrDataLevel[i]["lev_data_id"] +"' onclick='btn_approve(this)' data-leva_id='"+ arrDataLevel[i]["leva_id"] +"' data-position='top' data-delay='50' data-tooltip='อนุมัติ'><span class='fa fa-check'></span> อนุมัติ </button>";
                              r += "&nbsp;&nbsp;&nbsp;";
                              r += "<button class='btn btn-danger btn-rounded' data-is_approve='2' data-level='"+ arrDataLevel[i]["lev_level"] +"' data-lev_data_id='"+ arrDataLevel[i]["lev_data_id"] +"' onclick='btn_reject(this)' data-leva_id='"+ arrDataLevel[i]["leva_id"] +"'  data-position='top' data-delay='50' data-tooltip='ไม่อนุมัติ'><span class='fa fa-remove'></span> ไม่อนุมัติ </button>";

                              $("#field_approve"+arrLevel[lv]).html(r);
                              break;
                            }else{
                              $("#field_approve"+arrLevel[lv]).html("คุณไม่สามารถอนุมัติในระดับนี้ได้");
                            }
                          }else{
                            // console.log(data["data"][i]["levd_is_approve"]);
                            // console.log(arrDataLevel);
                            if(data["data"][i]["levd_is_approve"] == 1){
                              $("#field_approve"+arrLevel[lv]).html("<span class='fa fa-check'></span><span>  อนุมัติแล้วโดย : "+ arrDataLevel[i]["approveFullName"] +"</span>");
                            }else if(data["data"][i]["levd_is_approve"] == 2){
                              console.log("#field_approve"+arrLevel[lv]);
                              $("#field_approve"+arrLevel[lv]).html("<span class='fa fa-remove'></span><span>  ไม่อนุมัติโดย : "+ arrDataLevel[i]["approveFullName"] +"</span>");
                            }
                          }
                        }else{
                          // console.log("error");
                        }
                      }
                    }

                  });

                  ///////ตัวอย่างเดิม
                  // for (var i = 0; i < arrDataLevel.length; i++) {
                  //
                  //   if (arrDataLevel[i]["levd_is_approve"] == 3) {
                  //       for (var j = 0; j < arrLevel.length; j++) {
                  //         console.log(arrDataLevel[i]["lev_level"]+" "+arrLevel[j]);
                  //         if (arrDataLevel[i]["lev_level"] == arrLevel[j]) {
                  //           var r = ""
                  //           r += "<button class='btn btn-success btn-rounded' data-is_approve='1' data-level='"+ arrDataLevel[i]["lev_level"] +"' data-lev_data_id='"+ arrDataLevel[i]["lev_data_id"] +"' onclick='btn_approve(this)' data-position='top' data-delay='50' data-tooltip='อนุมัติ'><span class='fa fa-check'></span> อนุมัติ </button>";
                  //           r += "&nbsp;&nbsp;&nbsp;";
                  //           r += "<button class='btn btn-danger btn-rounded' data-is_approve='2' data-level='"+ arrDataLevel[i]["lev_level"] +"' data-lev_data_id='"+ arrDataLevel[i]["lev_data_id"] +"' onclick='btn_reject(this)' data-position='top' data-delay='50' data-tooltip='ไม่อนุมัติ'><span class='fa fa-remove'></span> ไม่อนุมัติ </button>";
                  //           $("#field_approve"+arrDataLevel[i]["lev_level"]).html(r);
                  //           break;
                  //         }else{
                  //           $("#field_approve"+arrDataLevel[i]["lev_level"]).html("คุณไม่สามารถอนุมัติในระดับนี้ได้");
                  //         }
                  //       }
                  //
                  //   }else if(arrDataLevel[i]["levd_is_approve"] == 1){
                  //     $("#field_approve"+arrDataLevel[i]["lev_level"]).html("<span class='fa fa-check'></span><span>  อนุมัติแล้วโดย : "+ arrDataLevel[i]["approveFullName"] +"</span>");
                  //   }else if(arrDataLevel[i]["levd_is_approve"] == 2){
                  //     $("#field_approve"+arrDataLevel[i]["lev_level"]).html("<span class='fa fa-remove'></span><span>  ไม่อนุมัติโดย : "+ arrDataLevel[i]["approveFullName"] +"</span>");
                  //   }
                  // } /////// ของเก่า


                  //
                  // var req = {
                  //   "lev_id" : data["data"][0]["lev_id"],
                  //   "site_id" : data["data"][0]["site_id"],
                  //   "branche_id" : data["data"][0]["branche_id"],
                  //   "company_id" : data["data"][0]["company_id"]
                  // }
                  // console.log(req);
                  // $.ajax({
                  //   url: getApi("LeaveData/getLevelPermissionApprove"),
                  //   type: 'POST',
                  //   headers: {'Authorization': token},
                  //   data: {'request' : req}
                  // }).done(function(data) {
                  //   if (data["Has"] == false) {
                  //
                  //   }else{
                  //     for (var i = 0; i < data["data"].length; i++) {
                  //       arrLevel.push(data["data"][i]["lev_level"]);
                  //     }
                  //
                  //   for (var i = 0; i < arrDataLevel.length; i++) {
                  //     if (arrDataLevel[i]["levd_is_approve"] == 3) {
                  //         for (var j = 0; j < arrLevel.length; j++) {
                  //           console.log(arrDataLevel[i]["lev_level"]+" "+arrLevel[j]);
                  //           if (arrDataLevel[i]["lev_level"] == arrLevel[j]) {
                  //             var r = ""
                  //             r += "<button class='btn btn-success btn-rounded' data-is_approve='1' data-level='"+ arrDataLevel[i]["lev_level"] +"' data-lev_data_id='"+ arrDataLevel[i]["lev_data_id"] +"' onclick='btn_approve(this)' data-position='top' data-delay='50' data-tooltip='อนุมัติ'><span class='fa fa-check'></span> อนุมัติ </button>";
                  //             r += "&nbsp;&nbsp;&nbsp;";
                  //             r += "<button class='btn btn-danger btn-rounded' data-is_approve='2' data-level='"+ arrDataLevel[i]["lev_level"] +"' data-lev_data_id='"+ arrDataLevel[i]["lev_data_id"] +"' onclick='btn_reject(this)' data-position='top' data-delay='50' data-tooltip='ไม่อนุมัติ'><span class='fa fa-remove'></span> ไม่อนุมัติ </button>";
                  //             $("#field_approve"+arrDataLevel[i]["lev_level"]).html(r);
                  //             break;
                  //           }else{
                  //             $("#field_approve"+arrDataLevel[i]["lev_level"]).html("คุณไม่สามารถอนุมัติในระดับนี้ได้");
                  //           }
                  //         }
                  //
                  //     }else if(arrDataLevel[i]["levd_is_approve"] == 1){
                  //       $("#field_approve"+arrDataLevel[i]["lev_level"]).html("<span class='fa fa-check'></span><span>  อนุมัติแล้วโดย : "+ arrDataLevel[i]["approveFullName"] +"</span>");
                  //     }else if(arrDataLevel[i]["levd_is_approve"] == 2){
                  //       $("#field_approve"+arrDataLevel[i]["lev_level"]).html("<span class='fa fa-remove'></span><span>  ไม่อนุมัติโดย : "+ arrDataLevel[i]["approveFullName"] +"</span>");
                  //     }
                  //   }
                  //
                  //   }
                  // }); // call LeaveData/getLevelPermissionApprove
            }
          });//call LeaveData/getByRequestLeave
        }
      });
    });

  }

  function btn_approve(el){
    var is_approve = $(el).data("is_approve");
    var level = $(el).data("level");
    var lev_data_id = $(el).data("lev_data_id");
    var leva_id = $(el).data("leva_id");

    var req = {
      "is_approve" : is_approve,
  		"lev_data_id" : lev_data_id,
  		"lev_level" : level,
      "leva_id" : leva_id
    }

    // console.log(req);

    var approveLevel = getApi("LeaveData/approveLevelNw")
    $.ajax({
      url: approveLevel,
      type: 'POST',
      headers: {'Authorization': token},
      data: {'request' : req}
    }).done(function(data) {
      if (data["Has"]) {
        if (data["Mess_code"] == 101) {
          $("#LeaveApproveModel").modal("toggle");
          $.toast({ heading: "SUCCESS",text: "อนุมัติเรียบร้อยแล้ว",position: 'top-right',loaderBg:'#ff6849',icon: 'success',hideAfter: 3500,stack: 6});
          $("#leave_table").dataTable().fnDestroy();
          $("#leave_body").getleave();
        }else if(data["Mess_code"] == 102){
          $("#LeaveApproveModel").modal("toggle");
          $.toast({ heading: "SUCCESS",text: "ไม่อนุมัติเรียบร้อยแล้ว",position: 'top-right',loaderBg:'#ff6849',icon: 'success',hideAfter: 3500,stack: 6});
          $("#leave_table").dataTable().fnDestroy();
          $("#leave_body").getleave();
        }else if(data["Mess_code"] == 202){
          $("#LeaveApproveModel").modal("toggle");
          $.toast({ heading: "REJECT",text: "ไม่สามารถอนุมัติได้ เนื่องจากวันคงเหลือไม่พอ",position: 'top-right',loaderBg:'#ff6849',icon: 'warning',hideAfter: 6500,stack: 6});
          $("#leave_table").dataTable().fnDestroy();
          $("#leave_body").getleave();
        }
      }else{
        $.toast({heading: "FAILED",text: "พบข้อผิดพลาดโปรดตรวจสอบ",position: 'top-right',loaderBg:'#ff6849',icon: 'error',hideAfter: 3500,stack: 6});
      }
    });
  }

  function btn_reject(el){
    var is_approve = $(el).data("is_approve");
    var level = $(el).data("level");
    var lev_data_id = $(el).data("lev_data_id");
    var leva_id = $(el).data("leva_id");

    var req = {
      "is_approve" : is_approve,
  		"lev_data_id" : lev_data_id,
  		"lev_level" : level,
      "leva_id" : leva_id
    }

    var approveLevel = getApi("LeaveData/approveLevelNw")

    $.ajax({
      url: approveLevel,
      type: 'POST',
      headers: {'Authorization': token},
      data: {'request' : req}
    }).done(function(data) {
      if (data["Has"]) {
        if (data["Mess_code"] == 101) {
          $("#LeaveApproveModel").modal("toggle");
          $.toast({ heading: "SUCCESS",text: "อนุมัติเรียบร้อยแล้ว",position: 'top-right',loaderBg:'#ff6849',icon: 'success',hideAfter: 3500,stack: 6});
          $("#leave_table").dataTable().fnDestroy();
          $("#leave_body").getleave();
        }else if(data["Mess_code"] == 102){
          $("#LeaveApproveModel").modal("toggle");
          $.toast({ heading: "SUCCESS",text: "ไม่อนุมัติเรียบร้อยแล้ว",position: 'top-right',loaderBg:'#ff6849',icon: 'success',hideAfter: 3500,stack: 6});
          $("#leave_table").dataTable().fnDestroy();
          $("#leave_body").getleave();
        }else if(data["Mess_code"] == 202){
          $("#LeaveApproveModel").modal("toggle");
          $.toast({ heading: "SUCCESS",text: "ไม่สามารถอนุมัติได้ เนื่องจากวันคงเหลือไม่พอ",position: 'top-right',loaderBg:'#ff6849',icon: 'warning',hideAfter: 6500,stack: 6});
          $("#leave_table").dataTable().fnDestroy();
          $("#leave_body").getleave();
        }
      }else{
        $.toast({heading: "FAILED",text: "พบข้อผิดพลาดโปรดตรวจสอบ",position: 'top-right',loaderBg:'#ff6849',icon: 'error',hideAfter: 3500,stack: 6});
      }
    });

  }

  $(".btn_print").click(function(){
    $.ajax({
      url: base_url("vacation/getReportLeaveRequest"),
      type: 'POST'
    }).done(function(data) {
      $(".modal-area").html(data);
      $("#LeaveRequestReport").modal("toggle");

      $("#searchmonth").val(g_month);
      $("#searchyear").val(g_year);

      $("#leave_request").val("3");
      var decode = jwt_decode(token);
      $("#auth").val(decode["employee_id"]);
      $("#company_id").val(decode["company_id"]);

      $(".submit-report-print-leave").click(function(){
        $(".report_form").submit();
      })
    });
  });

  $(".btn_filter").click(function(){
    $.ajax({
      url: base_url("vacation/FilterLeaveRequest"),
      type: 'POST'
    }).done(function(data) {
      $(".modal-area").html(data);
      $("#FilterLeaveRequest").modal("toggle");

      $("#select_searchmonth").val(g_month);
      $("#select_searchyear").val(g_year);

      $("#site").sitegetall();

      $("#site").change(function(event) {
        $("#branch").branchgetall("site");
      });

      $(".filter-Leave-Request").click(function(){
        var month = $("#select_searchmonth").val();
        var year = $("#select_searchyear").val();

        url = getApi("LeaveData/getRequestLeave");

        $("#leave_table").dataTable().fnDestroy();
        $("#leave_body").html("");
        $.ajax({
          url: url,
          type: 'GET',
          headers: {'Authorization': token},
          data: {"month":month, "year":year}
        }).done(function(data) {

          if (data["Has"] == false) {
            var r = "";
            $("#leave_body").append(r);
            $("#leave_table").dataTable(dataTableTh);
          }else{
            no = 1;
            for (var i = 0; i < data["data"].length; i++) {
              var r = "";
              r += "<tr>";
              // r += "<td>";
              // r += "";
              // r += "</td>";
              r += "<td style='text-align:center'>";
              r += no;
              r += "</td>";
              r += "<td>";
              r += data["data"][i]["fullname"];
              var leave_start = data["data"][i]["lev_data_date_start"].split(" ");
              r += "<td class = 'text-center'>";
              r += convertDataFormat(leave_start[0]) + "<br>" + leave_start[1];
              r += "</td>";
              var leave_end = data["data"][i]["lev_data_date_end"].split(" ");
              r += "<td class = 'text-center'>";
              r += convertDataFormat(leave_end[0]) + "<br>" + leave_end[1];
              r += "</td>";
              var createdAtLeave = data["data"][i]["createAt"].split(" ");
              r += "<td>";
              r += convertDataFormat(createdAtLeave[0]) + " " + createdAtLeave[1];
              r += "</td>";
              r += "<td>";
              r += data["data"][i]["leave_type_name"];
              r += "</td>";
              r += "<td>";
              r += data["data"][i]["siteName"];
              r += "</td>";
              r += "<td>";
              r += data["data"][i]["brancheName"];
              r += "</td>";
              r += "<td style='text-align:center'>";
              r += "<label class='label label-default'>ท่านยังไม่ได้อนุมัติ</label>";
              r += "</td>";
              r += "<td>";
              r += "<div class='btn-group'>";
              r += "<button class='btn btn-sm btn-default btn-outline waves-effect btn-wk-link' data-id='"+ data["data"][i]["lev_data_id"] +"' onclick='getApproveDialog(this)'><span class='fa fa-link'></span></button>";
              // r += "<button class='btn btn-sm btn-default btn-outline waves-effect'><span class='fa fa-trash'></span></button>";
              r += "</div>";
              r += "</td>";
              r += "</tr>";
              no++;
              $("#leave_body").append(r)
            }
            $("#leave_table").dataTable(dataTableTh);
            $("#FilterLeaveRequest").modal("toggle");
          }

        });//getrequest
      });// modal filter

    });
  });

  (function ($){
    $.fn.sitegetall = function (){
      var site = $(this);
  
      var url = getApi("sites/getall");
          $.ajax({
            url: url,
            type: 'GET',
            headers: {'Authorization': token}
          }).done(function(data) {
            var no = 1;
  
            if (data["Has"] == false) {
            // log("data (site)=", data);
  
            }else{
              // log("data (site)=", data);
                 var r = "<option value='0'>";
                 r += "เลือกไซต์";
                 r += "</option>";
                for (var i = 0; i < data["data"].length; i++) {
                  r += "<option value='"+ data["data"][i]["site_id"] +"'>";
                  r += data["data"][i]["siteName"];
                  r += "</option>";
  
               }//loop
              site.append(r);
  
            } //else
          });//done
  
      }
    })(jQuery);

    (function ($){
      $.fn.branchgetall = function (chk){
        var branch = $(this);
  
        if (chk == "r_site") {
          var siteval = $("#r_site").val();
        }else{
          var siteval = $("#site").val();
        }
        var url = getApi("Branches/getall");
  
            $.ajax({
              url: url,
              type: 'GET',
              headers: {'Authorization': token},
              data: {"site_id" : siteval}
            }).done(function(data) {
              console.log(data);
              var no = 1;
  
  
              if (data["Has"] == false) {
                branch.html("");
                var r = "";
                r += "<option value='0'>";
                r += "ไม่มีสาขา";
                r += "</option>";
                branch.append(r);
  
              }else{
                branch.html("");
                var r = "";
                r += "<option value='A'>";
                r += "ทั้งหมด";
                r += "</option>";
              for (var i = 0; i < data["data"].length; i++) {
                  r += "<option value='"+ data["data"][i]["branche_id"] +"'>";
                  r += data["data"][i]["brancheName"];
                  r += "</option>";
  
               }//loop
                branch.append(r);
  
              } //else
            });//done
  
            $("#r_branche").change(function(){
              $("#index_branche").val(this.value);
            });
      }
    })(jQuery);
