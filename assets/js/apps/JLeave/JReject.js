var token = window.localStorage.getItem('token');
  $(document).ready(function() {
    $("#leave_body").getleave();
  });

  (function ($){
    $.fn.getleave = function (){
      var body = $(this);
      var urlgetRequestLeave = getApi("LeaveData/getRejectLeave");
      var segments      = location.pathname.split('/'),
      site_id = segments[segments.length - 1];
      $.ajax({
        url: urlgetRequestLeave,
        type: 'GET',
        headers: {'Authorization': token}
      }).done(function(data) {
        if (data["Has"] == false) {
          var r = "";
          body.append(r)
          $("#leave_table").dataTable(dataTableTh);
        }else{
          no = 1;
          for (var i = 0; i < data["data"].length; i++) {
            var r = "";
            r += "<tr>";
            // r += "<td>";
            // r += "";
            // r += "</td>";
            r += "<td style='text-align:center'>";
            r += no;
            r += "</td>";
            r += "<td>";
            r += data["data"][i]["fullname"];
            r += "</td>";
            r += "<td>";
            r += data["data"][i]["createAt"];
            r += "</td>";
            r += "<td>";
            r += data["data"][i]["detail"];
            r += "</td>";
            r += "<td style='text-align:center'>";
            var levd_date_start = data["data"][i]["lev_data_date_start"].split(" ");
            r += convertDataFormat(levd_date_start[0]);
            r += "</td>";
            r += "<td style='text-align:center'>";
            var levd_date_end = data["data"][i]["lev_data_date_end"].split(" ");
            r += convertDataFormat(levd_date_end[0]);
            r += "</td>";
            r += "<td>";
            r += data["data"][i]["siteName"];
            r += "</td>";
            r += "<td>";
            r += data["data"][i]["brancheName"];
            r += "</td>";
            r += "<td style='text-align:center'>";
            r += data["data"][i]["lev_data_d_day"]+" วัน "+data["data"][i]["lev_data_d_hour"]+" ชั่วโมง";
            r += "</td>";
            r += "<td class='text-center'>";
            if (data["data"][i]["lev_approve_status"] == 2) {
              r += "<span class='label label-danger'>ไม่อนุมัติ</span>";
            }else if(data["data"][i]["lev_approve_status"] == 6){
              r += "<span class='label label-warning'>ยกเลิกรายการเนื่องจากวันคงเหลือไม่พอ</span>";
            }
            r += "</td>";
            r += "</tr>";
            no++;
            body.append(r)
          }
          $("#leave_table").dataTable(dataTableTh);
        }
      });
    }
  })(jQuery);
