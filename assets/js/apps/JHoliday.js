var holidayDEL = [];
$(document).ready(function() {
  $("#holiday_table").Holiday();

});

$(".btn-create-holiday").click(function(event) {
    var url = base_url("holidays/ModalCreate");
    $.ajax({
      url:url,
      type:'POST',
    })
    .done(function(data) {
      $(".modal-area").html(data);
      $("#holidayCreate").modal("show");
      //
      $(".btn-create-hd-row").click(function(event) {
          $("#HolidayList_body").createHolidayRows();
          $('.mydatepicker').datepicker({
              autoclose: true,
              todayHighlight: true,
              clearBtn:true,
              language: 'th'
          });
      });

      $(".btn-save-hd").click(function(event) {
        var segments      = location.pathname.split('/');
        if (chk_oper_branche == 1) {
          site_id = segments[segments.length - 2];
          branche_id = segments[segments.length - 1];
        }else{
          site_id = segments[segments.length - 1];
        }
         var input = $("#form-holiday input");
         var table = $("#holiday_table");
         var validate = input.validate_blank();
         if (!validate) {
             $.toast({heading: "ผิดพลาด !",text: "กรุณากรอกข้อมูลให้ครบถ้วน",position: 'top-right',loaderBg:'#ff6849',icon: 'error',hideAfter: 2000,stack: 6});
             return;
         }
         var tr = $("#HolidayList_body tr").not(".no-list");
         if (tr.length < 1) {
             $.toast({heading: "ผิดพลาด !",text: "กรุณาเพิ่มรายการวันหยุด",position: 'top-right',loaderBg:'#ff6849',icon: 'error',hideAfter: 2000,stack: 6});
             return;
         }

         var groupName = $("#holyday_name").val();
         var groupList = [];
         for (var i = 0; i < tr.length; i++) {
              var Holiday_date = $("#textdate_row"+(i+1)).val();
              var Holiday_caption = $("#textcaption_row"+(i+1)).val();
              groupList.push({
                "date" : Holiday_date,
                "caption" : Holiday_caption
              })
         }
         // var formData = {"name":groupName,"list":groupList,"segment":table.data("segment")};
         if (chk_oper_branche == 1) {
           var req = {
             "name" : groupName,
             "list" : groupList,
             "site_id" : window.atob(window.atob(window.atob(site_id))),
             "branche_id" : window.atob(window.atob(window.atob(branche_id)))
           };
         }else{
           var req = {
             "name" : groupName,
             "list" : groupList,
             "site_id" : window.atob(window.atob(window.atob(site_id))),
           };
         }
         console.log(req);
         var holidate_add_url = getApi("holiday/add");
         $.ajax({
           url:holidate_add_url,
           type:'POST',
           headers: {'Authorization': token},
           data:{"request" :req}
         })
         .done(function(data) {
           if (data) {
               $.toast({ heading: "SUCCESS",text: "บันทึกกลุ่มวันหยุดเรียบร้อยแล้ว",position: 'top-right',loaderBg:'#ff6849',icon: 'success',hideAfter: 3500,stack: 6});
               $("#holidayCreate").modal("toggle");
               $("#holiday_table").dataTable().fnDestroy();
               $("#holiday_table").Holiday();
           }else{
               $.toast({heading: "FAILED",text: "พบข้อผิดพลาด กรุณาลองใหม่",position: 'top-right',loaderBg:'#ff6849',icon: 'error',hideAfter: 3500,stack: 6});
           }
          });

      });

    });

});

function rm_hd_row(ev){
  var row = ev.dataset["row"];
  var hdd_id = ev.dataset["hdd"];
  $("#hd_row"+row).remove();
  var tr = $("#HolidayList_body tr");
  if (tr.length < 1) {
  var r  = "";
      r += "<tr  class='text-center no-list' data-row='0'>";
      r += "<td  colspan='3'>";
      r += "<h5>ไม่พบรายการวันหยุด</h5>";
      r += "</td>";
      r += "</tr>";
      $("#HolidayList_body").append(r);
  }
 holidayDEL.push(hdd_id);
}

(function ($){
  $.fn.Holiday = function(){
    var table = $(this);
    var segments      = location.pathname.split('/'),
    site_id = segments[segments.length - 1];
    var root_site_id = "";
    chkEdittoken = segments[segments.length - 2];
    if (chkEdittoken != "Edit") {
      site_id = chkEdittoken;
    }
    console.log(window.atob(window.atob(window.atob(site_id))));
    var holidayBody = $("#holiday_body");
    var url = getApi("holiday/getall");
    $.ajax({
      url: url,
      type: 'GET',
      headers: {'Authorization': token},
      data: {"site_id":window.atob(window.atob(window.atob(site_id)))}
    })
    .done(function(data) {
      var no = 1;
      if (data["Has"] == false) {
        holidayBody.html("");
      }else{
        for (var i = 0; i < data["data"].length; i++) {
          holidayBody.html("");
          for (var i = 0; i < data["data"].length; i++) {
            var hd_body = "<tr>";
               hd_body += "<td  class='text-center'>";
               hd_body += no;
               hd_body += "</td>";
               hd_body += "<td>";
               hd_body += data["data"][i]["hd_name"];
               hd_body += "</td>";
               hd_body += "<td class='text-center'>";
               hd_body += "<div class='btn-group'>";
               hd_body += "<button class='btn btn-sm btn-default btn-outline waves-effect btn-edit-holiday'onclick='edit_holiday(this)' data-edit='"+ data["data"][i]["hd_id"] +"'><span class='fa fa-edit'></span></button>";
               hd_body += "<button class='btn btn-sm btn-default btn-outline waves-effect btn-del-holiday' onclick='del_holiday(this)' data-del='"+ data["data"][i]["hd_id"] +"'><span class='fa fa-trash'></span></button>";
               // hd_body += "<button class='btn btn-sm btn-default btn-outline waves-effect' btn-detail-holiday data-detail='"+ data["data"][i]["hd_id"] +"''><span class='fa fa-search'></span></button>";
               hd_body += "</div>";
               hd_body += "</td>";
               hd_body += "</tr>";
               holidayBody.append(hd_body);
               no ++;
          }
        }
      }
     table.DataTable(dataTableTh);

    });
    $.fn.createHolidayRows = function (){
        var table = $(this);
        var tr = $("#HolidayList_body tr");
        var num = tr.length;
        var id = 1;
        if (num > 0) {
            var id = parseFloat(tr[num-1].dataset["row"]);
            id += 1;
        }
        $(".no-list").remove();
        var r  = "";
            r += "<tr id='hd_row"+id+"' data-row='"+id+"'>";
            r += "<td>";
            r += "<div class='form-group has-feedback' id='textdate_row"+id+"_group'>";
            r += "<input type='text' class='form-control mydatepicker' id='textdate_row"+id+"' placeholder='วัน/เดือน/ปี' data-mask='dd/mm/yyyy'>";
            r += "<span class='form-control-feedback' id='textdate_row"+id+"_feedback'></span>";
            r += "</div>";
            r += "</td>";
            r += "<td>";
            r += "<div class='form-group  has-feedback' id='textcaption_row"+id+"_group'>";
            r += "<input type='text' class='form-control' placeholder='กรอกคำอธิบาย' id='textcaption_row"+id+"'>";
            r += "<span class='form-control-feedback' id='textcaption_row"+id+"_feedback'></span>";
            r += "</div>";
            r += "</td>";
            r += "<td class='text-right'>";
            r += "<button type='button' class='btn btn-danger btn-outline'  data-row='"+id+"' onclick='rm_hd_row(this);'><span class='fa fa-remove'></span></button>";
            r += "</td>";
            r += "</tr>";
            table.append(r);
    }
  }

})(jQuery);

function edit_holiday(el){

  var hd_id = $(el).data("edit");
    var url = base_url("holidays/ModalEdit");
    $.ajax({
      url:url,
      type:'POST',
    })
    .done(function(data) {
      // console.log(data);
      $(".modal-area").html(data);
      $("#holidayEdit").modal("show");
      holidayDEL = [];

       var urlgetDetailholiday = getApi("holiday/getby");
        $.ajax({
          url:urlgetDetailholiday,
          type:'GET',
          headers: {'Authorization': token},
          data: {"hd_id":hd_id}
        })
        .done(function(data) {
          $("#holyday_edit_name").val(data["data"][0]["hd_name"]);

          var urlgetdedail = getApi("holiday/getdetail");
          $.ajax({
            url:urlgetdedail,
            type:'GET',
            headers: {'Authorization': token},
            data: {"hd_id":hd_id}
          })
          .done(function(data) {
            var table = $("#hd_table");
            var tr = $("#HolidayList_body tr");
            // var num = tr.length;
            var id = 1;
            $(".no-list").remove();
            for (var i = 0; i < data["data"].length; i++) {
            var r  = "";
                r += "<tr class='editrow' id='hd_row"+id+"' data-row='"+id+"' data-hdd_id='"+ data["data"][i]["hdd_no"] +"'>";
                r += "<td>";
                r += "<div class='form-group has-feedback' id='textdate_row"+id+"_group'>";
                r += "<input type='text' class='form-control mydatepicker' id='textdate_row"+id+"' placeholder='วัน/เดือน/ปี' data-mask='dd/mm/yyyy' value='"+ data["data"][i]["hdd_date"] +"'>";
                r += "<span class='form-control-feedback' id='textdate_row"+id+"_feedback'></span>";
                r += "</div>";
                r += "</td>";
                r += "<td>";
                r += "<div class='form-group  has-feedback' id='textcaption_row"+id+"_group'>";
                r += "<input type='text' class='form-control' placeholder='กรอกคำอธิบาย' id='textcaption_row"+id+"' value='"+ data["data"][i]["hdd_caption"] +"'>";
                r += "<span class='form-control-feedback' id='textcaption_row"+id+"_feedback'></span>";
                r += "</div>";
                r += "</td>";
                r += "<td class='text-right'>";
                r += "<button type='button' class='btn btn-danger btn-outline'  data-row='"+id+"' data-hdd='"+ data["data"][i]["hdd_no"] +"' onclick='rm_hd_row(this);'><span class='fa fa-remove'></span></button>";
                r += "</td>";
                r += "</tr>";
                table.append(r);
              }
              $('.mydatepicker, #datepicker').datepicker({
                  autoclose: true,
                  todayHighlight: true,
                  clearBtn:true,
                  endDate:"0d",
                  language: 'th'
              });
          });
        });

      $(".btn-create-hd-row").click(function(event) {
          $("#HolidayList_body").createHolidayRows();
          $('.mydatepicker').datepicker({
              autoclose: true,
              todayHighlight: true,
              clearBtn:true,
              language: 'th'
          });
      });

      $(".btn-saveEdit-hd").click(function(event) {

        var hdBody = $("#HolidayList_body tr").not(".no-list");
             var hdJson = [];
             var hdEditJson = [];
             for (var i = 0; i < hdBody.length; i++) {
                  var hdTr = hdBody[i];
                  if ($(hdTr).hasClass("editrow")) {
                    var hdTd = hdBody[i].childNodes;
                    var hdRow = {};
                    hdRow.hdd_no = hdTr.dataset["hdd_id"];
                    for (var td = 0; td < (hdTd.length-1); td++) {
                         var inputValue = hdTd[td].children[0].children[0].value;
                         if (td == 0) {
                             hdRow.date = inputValue;
                         }else if (td == 1) {
                             hdRow.caption = inputValue;
                    }
                  }
                    hdEditJson.push(hdRow);
                  }else{
                    var hdTd = hdBody[i].childNodes;
                    var hdRow = {};
                    for (var td = 0; td < (hdTd.length-1); td++) {
                         var inputValue = hdTd[td].children[0].children[0].value;
                         if (td == 0) {
                             hdRow.date = inputValue;
                         }else if (td == 1) {
                             hdRow.caption = inputValue;
                    }
                  }
                    hdJson.push(hdRow);
                  };
             }

        var namegroup_hd = $("#holyday_edit_name").val();
        var req = {
          "name" : namegroup_hd,
          "details" : {
              "add" :	hdJson,
              "edit" : hdEditJson,
              "delete" : holidayDEL
          },
         "hd_id" : hd_id
        };

        console.log(req);
        var urlsave_hd = getApi("holiday/update");
        $.ajax({
          url:urlsave_hd,
          type:'POST',
          headers: {'Authorization': token},
          data: {"request":req}
        })
        .done(function(data) {
          if (data) {
            $.toast({ heading: "SUCCESS",text: "แก้ไขเรียบร้อยแล้ว",position: 'top-right',loaderBg:'#ff6849',icon: 'success',hideAfter: 3500,stack: 6});
            $("#holidayEdit").modal("toggle");
            $("#holiday_table").dataTable().fnDestroy();
            $("#holiday_table").Holiday();
          }else{
            $.toast({heading: "FAILED",text: "พบข้อผิดพลาดโปรดตรวจสอบ",position: 'top-right',loaderBg:'#ff6849',icon: 'error',hideAfter: 3500,stack: 6});
          }
        });
      });

    });
}

function del_holiday(el){
  var hd_id = $(el).data("del");
  swal({
        title: 'คุณแน่ใจ',
        text: "ต้องการลบวันหยุดนี้หรือไม่",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: "ตกลง",
        cancelButtonText: "ยกเลิก"
      }).then((result) => {
        if (result.value) {
          var url = getApi("holiday/inactive");
          $.ajax({
            url:url,
            type:'POST',
            headers: {'Authorization': token},
            data: {"hd_id" : hd_id}
          })
          .done(function(data) {
            if (data) {
              $.toast({ heading: "SUCCESS",text: "ยกเลิกวันหยุดเรียบร้อยแล้ว",position: 'top-right',loaderBg:'#ff6849',icon: 'success',hideAfter: 3500,stack: 6});
              $("#holidayEdit").modal("toggle");
              $("#holiday_table").dataTable().fnDestroy();
              $("#holiday_table").Holiday();
            }else{
               $.toast({heading: "FAILED",text: "พบข้อผิดพลาดโปรดตรวจสอบ",position: 'top-right',loaderBg:'#ff6849',icon: 'error',hideAfter: 3500,stack: 6});
            }
          });
        }
      })
}
