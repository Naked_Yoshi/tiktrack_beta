var token = window.localStorage.getItem('token');
var no_id;
var phone;
var email;
$(document).ready(function() {

  getuserdetail();

  $('.mydatepicker, #datepicker').datepicker({
      autoclose: true,
      todayHighlight: true,
      clearBtn:true,
      endDate:"0d",
      language: 'th'
  });
});

$("#usrcreateDep").change(function(){
  $("#usrcreatePosition").createpositions();
});

function getuserdetail(){

  // $("#usrcreateNation").nation();

  var segments      = location.pathname.split('/'),
  emp_id = segments[segments.length - 1];
  var urluserdetail = getApi("users/getby");
  $.ajax({
    url: urluserdetail,
    type: 'GET',
    headers: {'Authorization': token},
    data: {"employee_id":window.atob(window.atob(window.atob(emp_id)))}
  }).done(function(data) {

    no_id = data["data"][0]["no"];
    phone = data["data"][0]["phoneNumber"];
    email = data["data"][0]["email"];
    $("#usrcreateTitle").getTiltle(data["data"][0]["title_id"]);
    $("#usrcreateFname").val(data["data"][0]["firstname"]);
    $("#usrcreateLname").val(data["data"][0]["lastname"]);
    $("#usrcreateFname_en").val(data["data"][0]["firstname_en"]);
    $("#usrcreateLname_en").val(data["data"][0]["lastname_en"]);
    $("#usrcreateSex").val(data["data"][0]["gender"]);
    $("#usrcreateBdate").val(convertDataFormat(data["data"][0]["birthday"]));
    $("#usrcreatePersonId").val(data["data"][0]["ciz_id"]);
    $("#usrcreateMarried").val(data["data"][0]["married"]);
    $("#usrcreateNation").nation(data["data"][0]["nation_id"]);
    // $("#usrcreateNation").val(data["data"][0]["nation_id"]);
    $("#usrcreatePosition").createpositions(data["data"][0]["position_id"]);
    $("#usrcreateDep").createdepartment(data["data"][0]["dep_id"]);
    // $("#usrcreateDep").val(data["data"][0]["dep_id"]);
    // $("#usrcreatePosition").val(data["data"][0]["position_id"]);
    $("#usrcreateSalary").val(data["data"][0]["salary"]);
    $("#usrcreateStart").val(convertDataFormat(data["data"][0]["startworkdate"]));
    $("#usrcreateHoliday").val(data["data"][0]["totalHolidays"]);
    $("#usrcreatephone").val(data["data"][0]["phoneNumber"]);

    $(".emp_name").html(data["data"][0]["firstname"]+" "+data["data"][0]["lastname"]);
    // $(".emp_position").html($("#usrcreatePosition option:selected").text());
    $(".emp_email").html(data["data"][0]["email"]);
    $(".emp_phone").html(data["data"][0]["phoneNumber"]);
    // $("#usrcreateTitle").getTiltle();
  });
}

$(".btn-update-emp").click(function(event){
  console.log("testtt");
  var segments      = location.pathname.split('/'),
  emp_id = segments[segments.length - 1];

  var bs = $("#usrcreateBdate").val().split("/");
  var b_date = bs[2]+"-"+bs[1]+"-"+bs[0];

  var ws = $("#usrcreateStart").val().split("/");
  var ws_date = ws[2]+"-"+ws[1]+"-"+ws[0];

  // var phone = $("#usrcreatePhone").val().toString().replace(/-/g, '');
  var ciz_id = $("#usrcreatePersonId").val().toString().replace(/-/g, '');
  var phoneNumber = $("#usrcreatephone").val().toString().replace(/-/g, '');

  req = {
       "no":no_id,
       "title_id" : $("#usrcreateTitle").val(),
    	 "employee_id":window.atob(window.atob(window.atob(emp_id))),
       "firstname":$("#usrcreateFname").val(),
       "lastname":$("#usrcreateLname").val(),
       "firstname_en" : $("#usrcreateFname_en").val(),
       "lastname_en" : $("#usrcreateLname_en").val(),
       "gender" : $("#usrcreateSex").val(),
       "ciz_id" : ciz_id,
       "birthday":b_date,
       "phoneNumber" : phoneNumber,
       "email" : email,
       "totalHolidays" : $("#usrcreateHoliday").val(),
       "married":$("#usrcreateMarried").val(),
       "salary" : $("#usrcreateSalary").val(),
       "startworkdate" : ws_date,
       "probation" :"0000-00-00",
       "nation_id" :$("#usrcreateNation").val(),
       "position_id" : $("#usrcreatePosition").val(),
       "dep_id" : $("#usrcreateDep").val(),
  };

  var segments      = location.pathname.split('/'),
  emp_id = segments[segments.length - 1];
  var urluserupdate = getApi("users/update");
  // console.log(req);
  $.ajax({
    url: urluserupdate,
    type: 'POST',
    headers: {'Authorization': token},
    data: {"request":req}
  }).done(function(data) {
    console.log(data);
    if (data["status"]) {
      $.toast({ heading: "SUCCESS",text: "อัพเดทข้อมูลเรียบร้อยแล้ว",position: 'top-right',loaderBg:'#ff6849',icon: 'success',hideAfter: 3500,stack: 6});
    location.reload();
    }else{
      $.toast({heading: "FAILED",text: "ล้มเหลว โปรดวตรวจสอบ",position: 'top-right',loaderBg:'#ff6849',icon: 'error',hideAfter: 3500,stack: 6});
    }
  });
});

(function($){
  $.fn.nation = function(nation){
    var urlnation = base_url("users/c_getnations");
    $.ajax({
      url: urlnation,
      type: 'GET'
    }).done(function(data) {
      var obj = JSON.parse(data);
      // console.log(obj);
      var r = "<option value='0'>";
      r += "Default select";
      r += "</option>";
      for (var i = 0; i < obj.length; i++) {
        r += "<option value='"+ obj[i]["nation_id"] +"'>";
        r += obj[i]["nation_name"];
        r += "</option>";
      }
    $("#usrcreateNation").append(r);
    $("#usrcreateNation").val(nation);
    });
  }

})(jQuery);

(function($){
  $.fn.createdepartment = function(dep){
    var urldep = getApi("department/getall");
    console.log(dep);
    $.ajax({
      url: urldep,
      type: 'GET',
      headers: {'Authorization': token}
    }).done(function(data) {
      var r = "<option value='0'>";
      r += "Default select";
      r += "</option>";
      for (var i = 0; i < data["data"].length; i++) {
        r += "<option value='"+ data["data"][i]["dep_id"] +"'>";
        r += data["data"][i]["dep_name"];
        r += "</option>";
      }
    $("#usrcreateDep").append(r);
    $("#usrcreateDep").val(dep);
    });
    // getuserdetail();
  }
})(jQuery);

(function($){
  $.fn.createpositions = function(pos){
    var dep_id = $("#usrcreateDep").val();
    var urlposition = getApi("position/getall");
    $.ajax({
      url: urlposition,
      type: 'GET',
      headers: {'Authorization': token},
      data:{'dep_id':dep_id}
    }).done(function(data) {
      // console.log(data);
      $("#usrcreatePosition").html("");
      var r = "<option value='0'>";
      r += "Default select";
      r += "</option>";
      for (var i = 0; i < data["data"].length; i++) {
        r += "<option value='"+ data["data"][i]["position_id"] +"'>";
        r += data["data"][i]["position_name"];
        r += "</option>";
      }
      $("#usrcreatePosition").append(r);
      $("#usrcreatePosition").val(pos);
      $(".emp_position").html($("#usrcreatePosition option:selected").text());
    });

  }
})(jQuery);

(function($){
  var title = [];
  $.fn.getTiltle = function($titleThai){
     var itemTitle = $(this);
     var url = base_url("items/optionTitle");
     $.ajax({
       url: url,
       type: 'POST',
     })
     .done(function(data) {
       var obj = JSON.parse(data);
           title = obj["title"];
       for (var i = 0; i < obj["options"].length; i++) {
            itemTitle.append(obj["options"][i]);
       }
       // $("#usrcreateTitle_en").setTitleEng(title[itemTitle[0].selectedIndex]);
       $("#usrcreateTitle").val($titleThai);
        $("#usrcreateTitle_en").setTitleEng(title[$("#usrcreateTitle").prop('selectedIndex')]);
       // $("#usrcreateTitle_en").setTitleEng($("#usrcreateTitle").val($titleThai));

     });
     itemTitle.change(function(event) {
        var index = $(this).context.selectedIndex;
        $("#usrcreateTitle_en").setTitleEng(title[index]);
     });
  }
})(jQuery);

$.fn.setTitleEng = function(option){
  // console.log(option);
    $(this).val(option["title_en"]);
}

$(".btn-change-phone").click(function(){
  var segments      = location.pathname.split('/'),
  emp_id = segments[segments.length - 1];
  swal({title: 'ปลดล๊อคอุปกรณ์' ,text: "คุณต้องการปลดล๊อคอุปกรณ์ ใช่หรือไม่ ?", type: 'warning',showCancelButton: true,confirmButtonColor: '#3085d6',cancelButtonColor: '#d33',confirmButtonText: 'ใช่ !',cancelButtonText: 'ไม่'})
  .then((result) => { if (result.value) {
    $.ajax({
      url: getApi("users/changephone"),
      type: 'POST',
      headers: {'Authorization': token},
      data: {"employee_id":window.atob(window.atob(window.atob(emp_id)))}
    }).done(function(data) {
      if (data) {
        swal("ปลดล๊อคอุปกรณ์เรียบร้อยแล้ว", "", "success");
      }else{
        swal("ไม่สามารถปลดล๊อคอุปกรณ์ข้อมูลได้  !", "", "error");
      }
    });
    }
  });

})
