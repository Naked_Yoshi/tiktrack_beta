var token = window.localStorage.getItem('token');
var chip_emp = [];
var approved_list = [];
var add_chip_emp = [];
var emp_list = [];
var chk_oper_branche = 0;

$(document).ready(function() {
  showdetailsite();
  $("#working_table").Working();
  $("#grant_table").approve();
  $("#employee_table").employees();
  $("#usr_wk").getwk();
  $("#txt_area").html($("#CircleArea_site").val());
});

$("#CircleArea_site").change(function(){
  $("#txt_area").html($("#CircleArea_site").val());
});

$(".btn-grant-creategroup").click(function(){
  console.log("grant");
  if (chip_emp.length == 0) {
    $('#txt_label').html("<span style='color: red;'>กรุณาเลือกผู้อนุมัติ</span>");
  }else{
    $('#txt_label').html("");
    var myArray = chip_emp;
    var url = getApi("sites/addApprover");
    var segments      = location.pathname.split('/'),
    secondLastSegment = segments[segments.length - 1];
    $.ajax({
      url: url,
      type: 'POST',
      // datatype : "application/json",
      headers: {'Authorization': token},
      data: {"site_id":window.atob(window.atob(window.atob(secondLastSegment))),"arr_users":myArray}
    })
    .done(function(data) {
      if (data) {
        $.toast({ heading: "SUCCESS",text: "เพิ่มสิทธิ์เข้าใช้งานสาขาเรียบร้อยแล้ว",position: 'top-right',loaderBg:'#ff6849',icon: 'success',hideAfter: 3500,stack: 6});
        $("#btn_close_approve").trigger("click");
        $("#grant_table").dataTable().fnDestroy();
        $("#grant_table").approve();
      }else{
        $.toast({heading: "FAILED",text: "พบข้อผิดพลาดโปรดตรวจสอบ",position: 'top-right',loaderBg:'#ff6849',icon: 'error',hideAfter: 3500,stack: 6});
      }
    });
  }
});

$(".btn-adduser-onsite").click(function(){
  console.log("addsite");
  if (add_chip_emp.length == 0) {
    $('#emp_txt_label').html("<span style='color: red;'>กรุณาเลือกพนักงาน</span>");
  }else{
    $('#emp_txt_label').html("");
    var myArray = add_chip_emp;
    var url = getApi("users/adduser_onsite");
    var segments      = location.pathname.split('/'),
    secondLastSegment = segments[segments.length - 1];
    var wk_id = $("#usr_wk").val();
    $.ajax({
      url: url,
      type: 'POST',
      headers: {'Authorization': token},
      data: {"site_id":window.atob(window.atob(window.atob(secondLastSegment))),"arr_users":myArray, "wk_id":wk_id}
    })
    .done(function(data) {
      $("#employee_table").dataTable().fnDestroy();
      $("#employee_table").employees();
      $(".btn-adduser-close").trigger("click");
    });
  }
});

$(".btn-save-siteName").click(function(){
  var url = getApi("sites/updateName");
  var name = $("#sitecreateName").val();
  var segments      = location.pathname.split('/'),
  secondLastSegment = segments[segments.length - 1];
  $.ajax({
    url: url,
    type: 'POST',
    headers: {'Authorization': token},
    data: {"new_siteName":name,"site_id":window.atob(window.atob(window.atob(secondLastSegment)))}
  })
  .done(function(data) {
    if (data) {
       location.reload();
    }else{
      $.toast({heading: "ผิดพลาด",text: "ไม่สามารถบันทึกได้",position: 'top-right',loaderBg:'#ff6849',icon: 'error',hideAfter: 3500,stack: 6});
    }
  });
});

$(".btn-clear-approve").click(function(){
  $('#txt_label').html("");
  $('#emp-chip').html("");
  chip_emp = [];
});

$(".btn-clear-emp").click(function(){
  $('#emp_txt_label').html("");
  $('#add-emp-chip').html("");
  add_chip_emp = [];
});

function showdetailsite(){
  var url = getApi("sites/getby");
  var segments      = location.pathname.split('/'),
  secondLastSegment = segments[segments.length - 1];
  console.log(secondLastSegment);
  $.ajax({
    url: url,
    type: 'GET',
    headers: {'Authorization': token},
    data: {"site_id": window.atob(window.atob(window.atob(secondLastSegment))) }
  }).done(function(data) {
    $('#sitecreateName').val(data["data"][0]["siteName"]);
    $("#createName").html(data["data"][0]["createName"]);
    $("#updateName").html(data["data"][0]["updateName"]);
    $('#createAt').html(data["data"][0]["createdAt"]);
    $('#updateAt').html(data["data"][0]["updatedAt"]);
  });
}

(function ($){
  $.fn.getwk = function (){
    var wk = $(this);
    var url = getApi("workings/getall");
    var segments      = location.pathname.split('/'),
    site_id = segments[segments.length - 1];
    $.ajax({
      url: url,
      type: 'GET',
      headers: {'Authorization': token},
      data: {"site_id" : window.atob(window.atob(window.atob(site_id)))}
    }).done(function(data) {
      if (data["Has"]) {
        var r = "<option value='0' disabled>";
        r += "เลือกวันทำการ";
        r += "</option>";
        for (var i = 0; i < data["data"].length; i++) {
          r += "<option value='"+ data["data"][i]["wk_id"] +"'>";
          r += data["data"][i]["wk_name"];
          r += "</option>";
        }
        wk.append(r);
      }
    });
  }
})(jQuery);

(function ($){
  $.fn.approve = function (){
  var table = $(this);
  var approveBody = $("#approve_body");
  var url = getApi("sites/getApprover");
  var segments      = location.pathname.split('/'),
  secondLastSegment = segments[segments.length - 1];
  $.ajax({
    url: url,
    type: 'GET',
    headers: {'Authorization': token},
    data: {"site_id": window.atob(window.atob(window.atob(secondLastSegment))) }
  }).done(function(data) {
    var no = 1;
    if (data["Has"] == false) {
      var tablesapprove = "<tr>";
      tablesapprove += "<td colspan='3' class='text-center'>ไม่พบข้อมูล";
      tablesapprove += "</td>";
      tablesapprove += "</tr>";
      approveBody.append(tablesapprove);
    }else{
      for (var i = 0; i < data.length; i++) {
        approveBody.html("");
        for (var i = 0; i < data.length; i++) {
          var tablesapprove = "<tr>";
             tablesapprove += "<td  class='text-center'>";
             tablesapprove += no;
             tablesapprove += "</td>";
             tablesapprove += "<td>";
             tablesapprove += data[i]["approver_name"];
             tablesapprove += "</td>";
             tablesapprove += "<td class='text-center'>";
             tablesapprove += "<div class='btn-group'>";
             tablesapprove += "<button class='btn btn-sm btn-default btn-outline waves-effect btn-approve-delete' data-delete='"+ data[i]["site_perm_no"] +"' onclick='approve_delete(this)'><span class='fa fa-trash'></span></button>";
             tablesapprove += "</div>";
             tablesapprove += "</td>";
             tablesapprove += "</tr>";
             approveBody.append(tablesapprove);
             approved_list.push(data[i]["employee_id"]);
             no ++;
           }
         }
       }
      table.DataTable(dataTableTh);
  });
}
})(jQuery);

(function ($){
  $.fn.employees = function (){
    var table = $(this);
    console.log("employ");
    var employeeBody = $("#employees_body");
    var url = getApi("users/getuser_onsite");
    var segments      = location.pathname.split('/'),
    secondLastSegment = segments[segments.length - 1];
    $.ajax({
      url: url,
      type: 'GET',
      headers: {'Authorization': token},
      data: {"site_id": window.atob(window.atob(window.atob(secondLastSegment))) }
    }).done(function(data) {
      var no = 1;
      if (data["Has"] == false) {

      }else{
        for (var i = 0; i < data["data"].length; i++) {
          employeeBody.html("");
          for (var i = 0; i < data["data"].length; i++) {
            var tablesemployee = "<tr>";
               tablesemployee += "<td  class='text-center'>";
               tablesemployee += no;
               tablesemployee += "</td>";
               tablesemployee += "<td>";
               tablesemployee += data["data"][i]["fullname"];
               tablesemployee += "</td>";
               tablesemployee += "<td>";
               tablesemployee += data["data"][i]["brancheName"];
               tablesemployee += "</td>";
               tablesemployee += "<td>";
               tablesemployee += data["data"][i]["wk_name"];
               tablesemployee += "</td>";
               tablesemployee += "<td class='text-center'>";
               tablesemployee += "<div class='btn-group'>";
               tablesemployee += "<button class='btn btn-sm btn-default btn-outline waves-effect btn-emp-site-delete' data-fullname='"+ data["data"][i]["fullname"] +"' data-empwk='"+ data["data"][i]["employee_id"] +"' onclick='emp_site_edit_wk(this)'><span class='fa fa-edit'></span></button>";
               tablesemployee += "<button class='btn btn-sm btn-default btn-outline waves-effect btn-emp-site-delete' data-empdelete='"+ data["data"][i]["employee_id"] +"' onclick='emp_site_delete(this)'><span class='fa fa-trash'></span></button>";
               tablesemployee += "</div>";
               tablesemployee += "</td>";
               tablesemployee += "</tr>";
               employeeBody.append(tablesemployee);
               emp_list.push(data["data"][i]["employee_id"]);
               no ++;
             }
           }
           table.dataTable().fnDestroy();
           table.DataTable(dataTableTh);
         }

    });
  }
})(jQuery);

$(function(){
  var url = getApi("sites/getUserAllPermission");
  $.ajax({
    url: url,
    type: 'GET',
    headers: {'Authorization': token}
  }).done(function(data) {
    $('#advanced-demo').autoComplete({
        minChars: 0,
        source: function(term, suggest){
            term = term.toLowerCase();
            var choices = [];
            for (var i = 0; i < data.length; i++) {
              choices.push([data[i]["fullname"],data[i]["employee_id"]]);
            }
            var suggestions = [];
            for (i=0;i<choices.length;i++)
                if (~(choices[i][0]+' '+choices[i][1]).toLowerCase().indexOf(term)) suggestions.push(choices[i]);
            suggest(suggestions);
        },
        renderItem: function (item, search){
            search = search.replace(/[-\/\\^$*+?.()|[\]{}]/g, '\\$&');
            var re = new RegExp("(" + search.split(' ').join('|') + ")", "gi");
            return '<div class="autocomplete-suggestion" data-langname="'+item[0]+'" data-lang="'+item[1]+'" data-val="'+search+'">'+item[0].replace(re, "<b>$1</b>")+'</div>';
        },
        onSelect: function(e, term, item){
            $('#txt_label').html("");
            var id = window.atob(item.data('lang'))
            console.log('Item "'+item.data('langname')+' ('+item.data('lang')+')" selected by '+(e.type == 'keydown' ? 'pressing enter or tab' : 'mouse click')+'.');
            $('#advanced-demo').val("");

            if(jQuery.inArray(id, chip_emp) !== -1){
              alert("ไม่สามารถเลือกซ้ำได้");
            }else{
              var dup_approve = chkdup_remove(id,approved_list);
              if (dup_approve) {
                $('#emp-chip').append("<span class='label label-position-ot label-warning "+item.data('lang')+"-label' id='"+id+"_label_"+id+"' data-value='"+id+"'>"+item.data('langname')+" <a onclick='rm_position_approver(this);' data-id='"+id+"_label_"+id+"'><span class='fa fa-close'></span></a> </span>");
                chip_emp.push(id);
              }else{
                alert("ผู้ใช้นี้มีสิทธิ์อยู่แล้ว");
              }

            }
            console.log(chip_emp);
        }
    });
  });
  });

  $(function(){
    var url = getApi("users/getwaitinguser");
    $.ajax({
      url: url,
      type: 'GET',
      headers: {'Authorization': token}
    }).done(function(data) {
      $('#emp-waiting-list').autoComplete({
          minChars: 0,
          source: function(term, suggest){
              term = term.toLowerCase();
              var choices = [];
              for (var i = 0; i < data["data"].length; i++) {
                choices.push([data["data"][i]["fullname"],data["data"][i]["employee_id"]]);
              }
              var suggestions = [];
              for (i=0;i<choices.length;i++)
                  if (~(choices[i][0]+' '+choices[i][1]).toLowerCase().indexOf(term)) suggestions.push(choices[i]);
              suggest(suggestions);
          },
          renderItem: function (item, search){
              search = search.replace(/[-\/\\^$*+?.()|[\]{}]/g, '\\$&');
              var re = new RegExp("(" + search.split(' ').join('|') + ")", "gi");
              return '<div class="autocomplete-suggestion" data-empname="'+item[0]+'" data-empid="'+item[1]+'" data-val="'+search+'">'+item[0].replace(re, "<b>$1</b>")+'</div>';
          },
          onSelect: function(e, term, item){
              $('#emp_txt_label').html("");
              var id = item.data('empid');
              console.log('Item "'+item.data('empname')+' ('+item.data('empid')+')" selected by '+(e.type == 'keydown' ? 'pressing enter or tab' : 'mouse click')+'.');
              $('#emp-waiting-list').val("");

              if(jQuery.inArray(id, add_chip_emp) !== -1){
                alert("ไม่สามารถเลือกซ้ำได้");
              }else{
                var dup_emp = chkdup_remove(id,emp_list);
                if (dup_emp) {
                  $('#add-emp-chip').append("<span class='label label-position-ot label-warning "+item.data('empid')+"-label' id='"+id+"_label_"+id+"' data-value='"+id+"'>"+item.data('empname')+" <a onclick='rm_position_emp(this);' data-id='"+id+"_label_"+id+"'><span class='fa fa-close'></span></a> </span>");
                  add_chip_emp.push(id);
                }else{
                  alert("ผู้ใช้นี้อยู่ในไซต์แล้ว");
                }

              }
              console.log(add_chip_emp);
          }
      });
    });
    });

  function rm_position_approver(ev){
          var spit_id = [];
          var spit_id = ev.dataset["id"].split('_');
          console.log(spit_id[2]);
           $("#"+ev.dataset["id"]).remove();
           chip_emp = jQuery.grep(chip_emp, function(value) {
              return value != spit_id[2];
            });
           console.log(chip_emp);
  }

  function rm_position_emp(ev){
          var spit_id = [];
          var spit_id = ev.dataset["id"].split('_');
          console.log(spit_id[2]);
           $("#"+ev.dataset["id"]).remove();
           add_chip_emp = jQuery.grep(add_chip_emp, function(value) {
              return value != spit_id[2];
            });
           console.log(add_chip_emp);
  }

        function getUserChip(val){
          var chip = $(".emp-chip");
          var empChip = [];
          for (var i = 0; i < chip.length; i++) {
               empChip.push(chip[i].dataset["index"]);
          }
          $.ajax({
            url:  base_url+'person/getchip',
            type: 'POST',
            data: {"str":val,"chip":empChip}
          })
          .done(function(data) {
              var obj = JSON.parse(data);
              for (var i = 0; i < obj.length; i++) {
                $(".chip-container").append(obj[i]);
              }
              $('input.autocomplete').val("");
          });
        }

$(".tab-Editlocation").click(function(){
  var lat = parseFloat($("#sitecreateLat").val());
  var lng = parseFloat($("#sitecreateLng").val());
  initAutocomplete();
  initMap({"lat": lat,"lng":lng });
});
$(".btn-save-location").click(function(){
    var url = getApi("sites/updateLocation");
    var segments      = location.pathname.split('/'),
    site = segments[segments.length - 1];
    console.log(window.atob(window.atob(window.atob(site))));
    var area = $('#CircleArea_site').val();
    var lat = $('#sitecreateLat').val();
    var lng = $('#sitecreateLng').val();
    $.ajax({
      url: url,
      type: 'POST',
      headers: {'Authorization': token},
      data: {"site_id" : window.atob(window.atob(window.atob(site))), "area" : area, "lat" : lat, "lng" : lng}
    }).done(function(data) {
      if (data) {
          $.toast({ heading: "SUCCESS",text: "update สถานที่เรียบร้อยแล้ว",position: 'top-right',loaderBg:'#ff6849',icon: 'success',hideAfter: 3500,stack: 6});
      }else{
          $.toast({heading: "ERROR",text: "พบข้อผิดพลาด",position: 'top-right',loaderBg:'#ff6849',icon: 'error',hideAfter: 3500,stack: 6});
      }
    });
});
$(".btn-add-wk").click(function(){
  var url = base_url("workings/modalCreate");
  var segment = $("#working_table").data("segment");
  $.ajax({
    url: url,
    type: 'POST',
    data: {"segment" : segment}
  })
  .done(function(data) {
    $(".modal-area").html(data);
    $("#workingCreate").modal("toggle");
    $(".tabs-working-list").workingList({"segment":segment});
    $("#list_wk").show();
    $(".wk-new-create").click(function(){
        $("#list_wk").hide();
        $("#create_new_wk").show();
        var decode = jwt_decode(token);
        $("#company_id").val(decode["company_id"]);
        $("#createBy").val(window.atob(decode["employee_id"]));
    });
    // $(".wk-search").click(function(){
    //     $("#list_wk").show();
    //     $("#create_new_wk").hide();
    // });
    $(".close-new").click(function(){
        $("#list_wk").hide();
        $("#create_new_wk").hide();
    });
    $(".close-wk-list").click(function(){
        $("#list_wk").hide();
        $("#create_new_wk").hide();
    });
    $('.clockpicker').clockpicker({placement: 'top',align: 'left',autoclose: true,'default': 'now',clear:true}).keypress(function(event) {return false;});
    $(".btn-cancel-time").click(function(){
      var id = $(this).data("id");
      $("#"+id).val("");
    });
    $(".apply-all").click(function(event) {
        var input = $("#create_form_wk input").not("#wk_name");
        var input_start = $("#monday_start").val();
        var input_end = $("#monday_end").val();
        for (var i = 2; i < input.length; i++) {
             if (i%2 == 0) {
                input[i].value = input_start;
             }else{
                input[i].value = input_end;
             }
        }
    });
    $(".btn-create-wk").click(function(){
        var blank = $("#wk_name").validate_blank();
        var input = $("#create_form_wk input").not("#wk_name");
        var odd = [];
        var even = [];
        for (var i = 0; i < input.length; i++) {
             if (i%2 == 0) {
                 even.push({"value":input[i].value,"key":i });
             }else{
                 odd.push({"value":input[i].value,"key":i });
             }
        }
        var number = 0;
        for (var a = 0; a < even.length; a++) {
            if (even[a]["value"] == "" && odd[a]["value"] != "") {
                $("#"+input[even[a]["key"]].id).validate_blank();
                $.toast({heading: "ผิดพลาด !",text: "กรุณากำหนดเวลาเข้างาน",position: 'top-right',loaderBg:'#ff6849',icon: 'error',hideAfter: 2000,stack: 6});
                return;
            }
            if (even[a]["value"] != "" && odd[a]["value"] == "") {
               $("#"+input[odd[a]["key"]].id).validate_blank();
               $.toast({heading: "ผิดพลาด !",text: "กรุณากำหนดเวลาออกงาน",position: 'top-right',loaderBg:'#ff6849',icon: 'error',hideAfter: 2000,stack: 6});
                return;
            }
            if (even[a]["value"] != "" && odd[a]["value"] != "") {
                number ++;
            }
        }
        if (blank) {
            if (number < 1) {
              $.toast({heading: "ผิดพลาด !",text: "คุณยังไม่ได้กำหนดวันทำการ",position: 'top-right',loaderBg:'#ff6849',icon: 'error',hideAfter: 2000,stack: 6});
          }else{
              $("#create_form_wk").workingCreate();
          }
          }else{
            $.toast({heading: "ผิดพลาด !",text: "กรุณากรอกชื่อวันทำการ",position: 'top-right',loaderBg:'#ff6849',icon: 'error',hideAfter: 2000,stack: 6});
          }
     });
    $(".btn-save-wk").click(function(){
          var saveUrl = getApi("sites/addWorking");
          var tr_select = $("#wk_select_table tr").not(".noSelect");
          var wk = [];
          var segments      = location.pathname.split('/');
          site_id = segments[segments.length - 1];
          for (var i = 0; i < tr_select.length; i++) {
               wk.push(tr_select[i].dataset["id"])
          }
          var req = {
            "site_id" : window.atob(window.atob(window.atob(site_id))),
            "wk" : wk
          };
          // var options = {"segment":segment,"wk" : wk, "company_id" : company_id_ajax};
          $.ajax({
            url: saveUrl,
            type: 'POST',
            headers: {'Authorization': token},
            data: {'request':req }
          })
          .done(function(data) {
            if (data) {
                $.toast({ heading: "SUCCESS",text: "เพิ่มกลุ่มวันทำงานเรียบร้อยแล้ว",position: 'top-right',loaderBg:'#ff6849',icon: 'success',hideAfter: 3500,stack: 6});
                $("#workingCreate").modal("toggle");
                $("#working_table").dataTable().fnDestroy();
                $("#working_table").Working();
            }else{
              $.toast({heading: "FAILED",text: "พบข้อผิดพลาดโปรดตรวจสอบ",position: 'top-right',loaderBg:'#ff6849',icon: 'error',hideAfter: 3500,stack: 6});
            }
          });
    });
  });
});

(function($){

    $.fn.Working = function(){
        var table = $(this);
        var segments      = location.pathname.split('/'),
        site_id = segments[segments.length - 1];
        var body = $("#working_body");
        var url = getApi("workings/getall");
        $.ajax({
          url: url,
          type: 'GET',
          headers: {'Authorization': token},
          data: {"site_id" : window.atob(window.atob(window.atob(site_id)))}
        })
        .done(function(data) {
          var no=1;
            body.html("");
            if (data["data"] == false) {
              body.html("");
            }else{
              for (var i = 0; i < data["data"].length; i++) {
                var r = "<tr>";
                r += "<td class='text-center'>";
                r += no;
                r += "</td>";
                r += "<td>";
                r += data["data"][i]["wk_name"];
                r += "</td>";
                r += "<td class='text-center'>";
                r += "<div class='btn-group'>";
                r += "<button class='btn btn-sm btn-default btn-outline waves-effect btn-wk-link' data-id='"+ data["data"][i]["wk_id"] +"' onclick='wk_link(this)'><span class='fa fa-link'></span></button>";
                // r += "<button class='btn btn-sm btn-default btn-outline waves-effect'><span class='fa fa-trash'></span></button>";
                r += "</div>";
                r += "</td>";
                r += "</tr>";
                   body.append(r);
                   no++;
              }
            }
            table.DataTable(dataTableTh);
        });
    }

    $.fn.openLinkWorking = function (){
      var ot_id;
      var hd_id;
         var opLink = $(this);
         var id = $(this).data("id");
         var segments      = location.pathname.split('/'),
         site_id = segments[segments.length - 1];
         var url = base_url("workings/getModalLink");
         $.ajax({
           url: url,
           type: 'POST'
         })
         .done(function(data) {
            $(".modal-area").html(data);
            $("#wkLink").modal("toggle");
               (function() {[].slice.call(document.querySelectorAll('.sttabs')).forEach(function(el) {
                   new CBPFWTabs(el);
               });})();
              var urldetail = getApi("workings/getby");
              $.ajax({
                url: urldetail,
                type: 'GET',
                headers: {'Authorization': token},
                data: {'id':id,'segment':window.atob(window.atob(window.atob(site_id)))}
              })
              .done(function(data) {
                $("#mon_start").html(data["data"][0]["wk_mon_start"]);
                $("#mon_end").html(data["data"][0]["wk_mon_end"]);

                $("#tue_start").html(data["data"][0]["wk_tue_start"]);
                $("#tue_end").html(data["data"][0]["wk_tue_end"]);

                $("#wed_start").html(data["data"][0]["wk_wed_start"]);
                $("#wed_end").html(data["data"][0]["wk_wed_end"]);

                $("#thu_start").html(data["data"][0]["wk_thu_start"]);
                $("#thu_end").html(data["data"][0]["wk_thu_end"]);

                $("#fri_start").html(data["data"][0]["wk_fri_start"]);
                $("#fri_end").html(data["data"][0]["wk_fri_end"]);

                $("#sat_start").html(data["data"][0]["wk_sat_start"]);
                $("#sat_end").html(data["data"][0]["wk_sat_end"]);

                $("#sun_start").html(data["data"][0]["wk_sun_start"]);
                $("#sun_end").html(data["data"][0]["wk_sun_end"]);
              });

              var urlsetting = getApi("workings/getsetting");
              $.ajax({
                url: urlsetting,
                type: 'GET',
                headers: {'Authorization': token},
                data: {'id':id,'site_id':window.atob(window.atob(window.atob(site_id)))}
              }).done(function(data) {
                ot_id = data["data"][0]["ot_id"];
                hd_id = data["data"][0]["hd_id"];
                wk_id = data["data"][0]["wk_id"];
                lev_id = data["data"][0]["lev_id"];
                // console.log(hd_id);
                $("#text_wk_ot").html("เชื่อมต่อกับ กลุ่มล่วงเวลา : "+data["data"][0]["ot_name"]);
                $("#text_wk_hd").html("เชื่อมต่อกับ กลุ่มวันหยุดประจำปี : "+data["data"][0]["hd_name"]);
                $("#text_wk_lev").html("เชื่อมต่อกับ กลุ่มวันลา : "+data["data"][0]["lev_name"]);

                var urlgetlinkot_f = base_url("ots/getlinkot");
                $.ajax({
                  url: urlgetlinkot_f,
                  type: 'GET'
                }).done(function(data) {
                  $(".ot-detail-dialog").html(data);

                  var urlgetlinkot_head = getApi("OT/getby");
                  $.ajax({
                    url: urlgetlinkot_head,
                    type: 'GET',
                    headers: {'Authorization': token},
                    data: {'id':ot_id}
                  }).done(function(data) {
                    $("#title_ot_name").html(data["data"][0]["ot_name"]);
                    $("#before_minute").html(data["data"][0]["ot_before_minute"]);
                    $("#after_minute").html(data["data"][0]["ot_after_minute"]);

                    $("#ot_working_ex").html(data["data"][0]["ot_working_ex"]);

                    $("#ot_weekend").html(data["data"][0]["ot_weekend"]);
                    $("#ot_weekend_ex").html(data["data"][0]["ot_weekend_ex"]);

                    $("#ot_holiday").html(data["data"][0]["ot_holiday"]);
                    $("#ot_holiday_ex").html(data["data"][0]["ot_holiday_ex"]);
                  });

                  var urlgetlinkot_excess = getApi("OT/getExcess");
                  $.ajax({
                    url: urlgetlinkot_excess,
                    type: 'GET',
                    headers: {'Authorization': token},
                    data: {'id':ot_id}
                  }).done(function(data) {
                    if (data["Has"]) {
                      for (var i = 0; i < data["data"].length; i++) {
                        var r = "<p> - " + data["data"][i]["ot_type_name"] + " " + data["data"][i]["ote_excess"] + " " + "  นาที ปัดเป็น " + data["data"][i]["ote_result"] + " นาที </p>";
                        $("#excess_detail").append(r);
                      }
                    }
                  });
                });

                var urlgetlinkot_ot_approve = getApi("OT/getApprovePosition");
                $.ajax({
                  url: urlgetlinkot_ot_approve,
                  type: 'GET',
                  headers: {'Authorization': token},
                  data: {'id':ot_id}
                }).done(function(data) {
                  var r_weekday="";
                  var r_weekend="";
                  var r_holiday="";
                  for (var i = 0; i < data["data"].length; i++) {
                    $label = "<span class='label label-warning'>" + data["data"][i]["position_name"] + " : " + data["data"][i]["fullname"] + "</span> ";
                      if (data["data"][i]["otp_type"] == 0) {
                            r_weekday += $label;
                        }else if (data["data"][i]["otp_type"] == 1) {
                            r_weekend += $label;
                        }else if (data["data"][i]["otp_type"] == 2) {
                            r_holiday += $label;
                          }
                  }
                    $("#otp_working").html(r_weekday);
                    $("#otp_weekend").html(r_weekend);
                    $("#otp_holiday").html(r_holiday);
                });

                var urlgetlinkhd_f = base_url("holidays/getlinkhd");
                $.ajax({
                  url: urlgetlinkhd_f,
                  type: 'GET'
                }).done(function(data) {
                  $(".hd-detail-dialog").html(data);

                    var urlgetlinkhd_head = getApi("holiday/getby");
                    $.ajax({
                      url: urlgetlinkhd_head,
                      type: 'GET',
                      headers: {'Authorization': token},
                      data: {'hd_id':hd_id}
                    }).done(function(data) {
                      var label;
                      if (hd_id == 0) {
                        label = "<p><span class='fa fa-info-circle'></span> กรุณาเลือกรายการวันหยุด</p>";
                      }else{
                        label = "<p><span class='text-left'>กลุ่มรายการวันหยุด : "+ data["data"][0]["hd_name"] +"</span></p>";
                      }
                      $("#link_hd_head").append(label);
                    });

                    var urlgetlinkhd_d = getApi("holiday/getdetail");
                    $.ajax({
                      url: urlgetlinkhd_d,
                      type: 'GET',
                      headers: {'Authorization': token},
                      data: {'hd_id':hd_id}
                    }).done(function(data) {
                      for (var i = 0; i < data["data"].length; i++) {
                        var r = "<tr>";
                        r += "<td width='150'>";
                        r += data["data"][i]["hdd_date"];
                        r += "</td>";
                        r += "<td>";
                        r += data["data"][i]["hdd_caption"];
                        r += "</td>";
                        r += "</tr>";
                        $("#link_hd_body").append(r);
                      }
                    });
                });

                var ot = $("#selectOt");
                var urlgetall_ot = getApi("OT/getall");
                  $.ajax({
                    url: urlgetall_ot,
                    type: 'GET',
                    headers: {'Authorization': token},
                    data: {'site_id':window.atob(window.atob(window.atob(site_id)))}
                  }).done(function(data) {
                    var r_ot = "<option value='0'>";
                    r_ot += "เลือกตำแหน่ง";
                    r_ot += "</option>";

                   for (var i = 0; i < data["data"].length; i++) {
                     r_ot+= "<option value='"+ data["data"][i]["ot_id"] +"'>";
                     r_ot+= data["data"][i]["ot_name"];
                     r_ot+= "</option>";
                   }
                   ot.append(r_ot);
                   $("#selectOt").val(ot_id);
                   $("#selectOt").change(function(event) {
                     $(this).getOtDetail($("#selectOt").val(),window.atob(window.atob(window.atob(site_id))));
                   }).select2();
                  });
                var hd = $("#selectHd");
                var urlgetall_hd = getApi("holiday/getall");
                  $.ajax({
                    url: urlgetall_hd,
                    type: 'GET',
                    headers: {'Authorization': token}
                    // data: {'site_id':window.atob(window.atob(window.atob(site_id)))}
                  }).done(function(data) {
                    var r_hd = "<option value='0'>";
                    r_hd += "เลือกตำแหน่ง";
                    r_hd += "</option>";

                   for (var i = 0; i < data["data"].length; i++) {
                     r_hd+= "<option value='"+ data["data"][i]["hd_id"] +"'>";
                     r_hd+= data["data"][i]["hd_name"];
                     r_hd+= "</option>";
                   }
                   hd.append(r_hd);
                   $("#selectHd").val(hd_id);
                   $("#selectHd").change(function(event) {
                   $(this).getHdDetail($("#selectHd").val(),window.atob(window.atob(window.atob(site_id))));
                   }).select2();
                  });

              });
         }); //modal
       }
    $.fn.getLevDetail = function(lev_id,site_id){
      var urlgetlinklev_head = getApi("vacation/getBy");
      $.ajax({
        url: urlgetlinklev_head,
        type: 'GET',
        headers: {'Authorization': token},
        data: {"va_id" : lev_id}
      }).done(function(data) {
        var label;
        if (lev_id == 0) {
          label = "<p><span class='fa fa-info-circle'></span> กรุณาเลือกรายการวันหยุด</p>";
        }else{
          label = "<p><span class='text-left'>กลุ่มรายการวันลา : "+ data["data"][0]["lev_name"] +"</span></p>";
        }
        $("#link_lev_head").html("");
        $("#link_lev_head").html(label);
      });

      var urlgetlinklev_head = getApi("vacation/getBy");
      $.ajax({
        url: urlgetlinklev_head,
        type: 'GET',
        headers: {'Authorization': token},
        data: {'va_id':lev_id}
      }).done(function(data) {
        var label;
        if (lev_id == 0) {
          label = "<p><span class='fa fa-info-circle'></span> กรุณาเลือกรายการวันหยุด</p>";
        }else{
          label = "<p><span class='text-left'>กลุ่มรายการวันลา : "+ data["data"][0]["lev_name"] +"</span></p>";
        }
        $("#link_lev_head").html(label);

        var r_lev = "";
        if (lev_id == 0) {
          $("#link_lev_body").html("");
        }else{
          for (var i = 0; i < data["data"].length; i++) {
            var txt_urgent="<span class='fa fa-square-o'></span>";
            var txt_delay="<span class='fa fa-square-o'></span>";
            var txt_deduct_money="<span class='fa fa-square-o'></span>";
            if (data["data"][i]["levd_is_urgent"] == 1) {
              txt_urgent = "<span class='fa fa-check-square'></span>";
            }
            if (data["data"][i]["levd_delay_request"] == 1) {
              txt_delay = "<span class='fa fa-check-square'></span>";
            }
            if (data["data"][i]["levd_is_deduct_money"] == 1) {
              txt_deduct_money = "<span class='fa fa-check-square'></span>";
            }
            r_lev += "<tr>";
            r_lev += "<td>";
            r_lev += data["data"][i]["leave_type_name"];
            r_lev += "</td>";
            r_lev += "<td>";
            r_lev += data["data"][i]["levd_request_date"]+" ชั่วโมง";
            r_lev += "</td>";
            r_lev += "<td>";
            r_lev += data["data"][i]["levd_master"]+" ชั่วโมง";
            r_lev += "</td>";
            r_lev += "<td align='center'>";
            r_lev += txt_deduct_money;
            r_lev += "</td>";
            r_lev += "<td align='center'>";
            r_lev += txt_urgent;
            r_lev += "</td>";
            r_lev += "<td align='center'>";
            r_lev += txt_delay;
            r_lev += "</td>";
            r_lev += "</tr>";
          }
          $("#link_lev_body").html("");
          $("#link_lev_body").append(r_lev);
        }
      });

    }
    $.fn.getHdDetail = function(hd_id,site_id){
      var urlgetlinkhd_head = getApi("holiday/getby");
      $.ajax({
        url: urlgetlinkhd_head,
        type: 'GET',
        headers: {'Authorization': token},
        data: {'hd_id':hd_id}
      }).done(function(data) {
        var label;
        if (hd_id == 0) {
          label = "<p><span class='fa fa-info-circle'></span> กรุณาเลือกรายการวันหยุด</p>";
        }else{
          label = "<p><span class='text-left'>กลุ่มรายการวันหยุด : "+ data["data"][0]["hd_name"] +"</span></p>";
        }
        $("#link_hd_head").html("");
        $("#link_hd_head").html(label);

        var urlgetlinkhd_d = getApi("holiday/getdetail");
        $.ajax({
          url: urlgetlinkhd_d,
          type: 'GET',
          headers: {'Authorization': token},
          data: {'hd_id':hd_id}
        }).done(function(data) {
          $("#link_hd_body").html("");
          for (var i = 0; i < data["data"].length; i++) {
            var r = "<tr>";
            r += "<td width='150'>";
            r += data["data"][i]["hdd_date"];
            r += "</td>";
            r += "<td>";
            r += data["data"][i]["hdd_caption"];
            r += "</td>";
            r += "</tr>";
            $("#link_hd_body").append(r);
          }

          $(".btn-link-hd").click(function(event) {
            var req = {
              "id" : wk_id,
          		"site_id" : site_id,
          		"hd" : hd_id
            };

            swal({
                  title: 'คุณแน่ใจ',
                  text: "เมื่อเชื่อมต่อรายการวันหยุด การเข้างานจะคิดวันหยุดตามวันที่กำหนด",
                  type: 'warning',
                  showCancelButton: true,
                  confirmButtonColor: '#3085d6',
                  cancelButtonColor: '#d33',
                  confirmButtonText: "ใช่, เชื่อมต่อเลย !",
                  cancelButtonText: "ไม่, ยกเลิก!"
                }).then((result) => {
                  if (result.value) {
                    var updatelink_hd = getApi("workings/updateLinkHD_onsite");
                    $.ajax({
                      url: updatelink_hd,
                      type: 'POST',
                      headers: {'Authorization': token},
                      data: {'request':req }
                    }).done(function(data) {
                      if (data) {
                        $.toast({ heading: "SUCCESS",text: "เชื่อมต่อวันหยุดเรียบร้อยแล้ว",position: 'top-right',loaderBg:'#ff6849',icon: 'success',hideAfter: 3500,stack: 6});
                      }else{
                         $.toast({heading: "FAILED",text: "พบข้อผิดพลาดโปรดตรวจสอบ",position: 'top-right',loaderBg:'#ff6849',icon: 'error',hideAfter: 3500,stack: 6});
                      }
                    });
                  }
                })

          });
        });
      });
    }


    $.fn.getOtDetail = function(ot_id,site_id){
      var urlgetlinkot_head = getApi("OT/getby");
      $.ajax({
        url: urlgetlinkot_head,
        type: 'GET',
        headers: {'Authorization': token},
        data: {'id':ot_id}
      }).done(function(data) {
        $("#title_ot_name").html(data["data"][0]["ot_name"]);
        $("#before_minute").html(data["data"][0]["ot_before_minute"]);
        $("#after_minute").html(data["data"][0]["ot_after_minute"]);

        $("#ot_working_ex").html(data["data"][0]["ot_working_ex"]);

        $("#ot_weekend").html(data["data"][0]["ot_weekend"]);
        $("#ot_weekend_ex").html(data["data"][0]["ot_weekend_ex"]);

        $("#ot_holiday").html(data["data"][0]["ot_holiday"]);
        $("#ot_holiday_ex").html(data["data"][0]["ot_holiday_ex"]);
      });

      var urlgetlinkot_excess = getApi("OT/getExcess");
      $.ajax({
        url: urlgetlinkot_excess,
        type: 'GET',
        headers: {'Authorization': token},
        data: {'id':ot_id}
      }).done(function(data) {
        $("#excess_detail").html("");
        if (data["Has"] == false) {
          $("#excess_detail").append(" - จ่ายตามจริง");
        }else{
          for (var i = 0; i < data["data"].length; i++) {
            var r = "<p> - " + data["data"][i]["ot_type_name"] + " " + data["data"][i]["ote_excess"] + " " + "  นาที ปัดเป็น " + data["data"][i]["ote_result"] + " นาที </p>";
            $("#excess_detail").append(r);
          }
        }
      });

      var urlgetlinkot_ot_approve = getApi("OT/getApprovePosition");
      $.ajax({
        url: urlgetlinkot_ot_approve,
        type: 'GET',
        headers: {'Authorization': token},
        data: {'id':ot_id}
      }).done(function(data) {
        $("#otp_working").html("");
        $("#otp_weekend").html("");
        $("#otp_holiday").html("");
        var r_weekday="";
        var r_weekend="";
        var r_holiday="";
        for (var i = 0; i < data["data"].length; i++) {
          $label = "<span class='label label-warning'>" + data["data"][i]["position_name"] + " : " + data["data"][i]["fullname"] + "</span> ";
            if (data["data"][i]["otp_type"] == 0) {
                  r_weekday += $label;
              }else if (data["data"][i]["otp_type"] == 1) {
                  r_weekend += $label;
              }else if (data["data"][i]["otp_type"] == 2) {
                  r_holiday += $label;
                }
        }
          $("#otp_working").html(r_weekday);
          $("#otp_weekend").html(r_weekend);
          $("#otp_holiday").html(r_holiday);
      });

      $(".btn-link-ot").click(function(event) {
        var req = {
          "id" : wk_id,
          "site_id" : site_id,
          "ot" : ot_id
        };

        swal({
              title: 'คุณแน่ใจ',
              text: "เมื่อเชื่อมต่อรายการล่วงเวลา หรือไม่ ?",
              type: 'warning',
              showCancelButton: true,
              confirmButtonColor: '#3085d6',
              cancelButtonColor: '#d33',
              confirmButtonText: "ใช่, เชื่อมต่อเลย !",
              cancelButtonText: "ไม่, ยกเลิก!"
            }).then((result) => {
              if (result.value) {
                var updatelink_ot = getApi("workings/updateLinkOT_onsite");
                $.ajax({
                  url: updatelink_ot,
                  type: 'POST',
                  headers: {'Authorization': token},
                  data: {'request':req }
                }).done(function(data) {
                  if (data) {
                    $.toast({ heading: "SUCCESS",text: "เชื่อมต่อวันหยุดเรียบร้อยแล้ว",position: 'top-right',loaderBg:'#ff6849',icon: 'success',hideAfter: 3500,stack: 6});
                  }else{
                     $.toast({heading: "FAILED",text: "พบข้อผิดพลาดโปรดตรวจสอบ",position: 'top-right',loaderBg:'#ff6849',icon: 'error',hideAfter: 3500,stack: 6});
                  }
                });
              }
            })
      });
         // var otSelect = $(this).val();
         // var url = base_url("ots/getDetail");
         // $.ajax({
         //    url: url,
         //    type: 'POST',
         //    data: {'ot': otSelect}
         // }).done(function(data) {
         //    $(".ot-detail-dialog").html(data);
         //
         // });

    }
    $.fn.LinkHD = function(formData){
       var url = base_url("workings/linkHD");
       $.ajax({
         url: url,
         type: 'POST',
         data:formData
       })
       .done(function(data) {
            var obj = JSON.parse(data);
            if (obj["success"]) {
              swal(obj["title"],obj["msg"], "success");
              $("#td_hd_used").html(obj["setting"]);
            }
       });

    }

    $.fn.LinkOT = function(formData){
       var url = base_url("workings/linkOT");
       $.ajax({
         url: url,
         type: 'POST',
         data:formData
       })
       .done(function(data) {
            var obj = JSON.parse(data);
            if (obj["success"]) {
              swal(obj["title"],obj["msg"], "success");
              $("#td_ot_used").html(obj["setting"]);
            }
       });

    }
    $.fn.createExcRows = function (){
          var table = $(this);
          var tr = $("#ExcBody tr");
          var num = tr.length;
          var id = 1;
          if (num > 0) {
              var id = parseFloat(tr[num-1].dataset["row"]);
              id += 1;
          }
          console.log(table);
          $(".no-list").remove();
          var r  = "";
              r += "<tr id='ex_row"+id+"' data-row='"+id+"'>";
              r += "<td>";
              r += "<div class='form-group  has-feedback' id='txtround_"+id+"_group'>";
              r += "<select class='form-control' id='txtround_"+id+"'>";
              r += "<option value='0'>มากกว่า > </option>";
              r += "<option value='1'>น้อยกว่า < </option>";
              r += "<option value='2'>มากกว่าเท่ากับ >= </option>";
              r += "<option value='3'>น้อยกว่าเท่ากับ <= </option>";
              r += "</select>";
              r += "<span class='form-control-feedback' id='txtround_"+id+"_feedback'></span>";
              r += "</div>";
              r += "</td>";
              r += "<td>";
              r += "<div class='form-group has-feedback' id='txtexcess_"+id+"_group'>";
              r += "<input type='text' class='form-control' id='txtexcess_"+id+"' name='' value='' placeholder='นาที' onkeypress='return numberOnly(event);'>";
              r += "<span class='form-control-feedback' id='txtexcess_"+id+"_feedback'></span>";
              r += "</div>";
              r += "</td>";
              r += "<td>";
              r += "<div class='form-group has-feedback' id='txtresult_"+id+"_group'>";
              r += "<input type='text' class='form-control' id='txtresult_"+id+"' name='' value='' placeholder='นาที' onkeypress='return numberOnly(event);'>";
              r += "<span class='form-control-feedback' id='txtresult_"+id+"_feedback'></span>";
              r += "</div>";
              r += "</td>";
              r += "<td class='text-right'>";
              r += "<button type='button' class='btn btn-danger btn-outline'  data-row='"+id+"' onclick='rm_ex_row(this);'><span class='fa fa-remove'></span></button>";
              r += "</td>";
              r += "</tr>";
              table.append(r);
    }
})(jQuery);


function numberOnly(ev){
  if (ev.keyCode < 48 || ev.keyCode > 57) {
      $.toast({ heading:"คำเตือน",text:"กรอกเฉพาะตัวเลข",position: 'top-right',loaderBg:'#ff6849',icon: 'warning',hideAfter: 3500,stack: 6});
      return false;
  }
}

function numDecimal(ev){
  if ((ev.keyCode < 47 && ev.keyCode > 45) || (ev.keyCode > 47 && ev.keyCode < 58) ) {
      var target = ev.target.value;
      var arr = target.split(".");
      if (arr.length > 1) {
          if (ev.keyCode == 46){
              return false;
          }
      }
      return true;
  }else {
       $.toast({ heading:"คำเตือน",text:"กรอกเฉพาะตัวเลข",position: 'top-right',loaderBg:'#ff6849',icon: 'warning',hideAfter: 3500,stack: 6});
      return false;
  }
}

function rm_ex_row(ev){
    var row = ev.dataset["row"];
    var ote_id = ev.dataset["ote"];
    $("#ex_row"+row).remove();
    var tr = $("#ExcBody tr");
    if (tr.length < 1) {
    var r  = "";
        r += "<tr  class='text-center no-list' data-row='0'>";
        r += "<td  colspan='4'>";
        r += "<h5>ไม่พบรายการส่วนเกิน</h5>";
        r += "</td>";
        r += "</tr>";
        $("#ExcBody").append(r);
    }
    excessDEL.push(ote_id);
}

function chkdup_remove(id,list){
  for (var i = 0; i < list.length; i++) {
    if (list[i] == id) {
      return false;
      exit();
    }
  }
  return true;
}

function wk_link(el){
  $(el).openLinkWorking();
}

function emp_site_delete(el){
  var arr_emp = [];
  var emp_id = $(el).data("empdelete");
  arr_emp.push(emp_id);
  console.log(arr_emp);
  var segments      = location.pathname.split('/'),
  site_id = segments[segments.length - 1];
  var url = getApi("users/deleteuser_onsite");
  swal({
      title: 'Are you sure?',
      text: "ต้องการยกเลิกไซต์พนักงานคนนี้หรือไม่",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'ตกลง',
      cancelButtonText: 'ยกเลิก'
    }).then((result) => {
      if (result.value) {
        $.ajax({
          url: url,
          type: 'POST',
          headers: {'Authorization': token},
          data: {"site_id" : window.atob(window.atob(window.atob(site_id))), "arr_users" : arr_emp}
        }).done(function(data) {
          location.reload();
        });
      }
    })
}

function approve_delete(el){
  var perm_id = $(el).data("delete");
  var url = getApi("sites/inactiveApprover");
  swal({
      title: 'Are you sure?',
      text: "ต้องการยกเลิกสิทธิ์หรือไม่",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'ตกลง',
      cancelButtonText: 'ยกเลิก'
    }).then((result) => {
      if (result.value) {
        $.ajax({
          url: url,
          type: 'POST',
          headers: {'Authorization': token},
          data: {"perms_no" : perm_id}
        }).done(function(data) {
          location.reload();
        });
      }
    })
}

function emp_site_edit_wk(el){
  var emp_id = $(el).data("empwk");
  var fullname = $(el).data("fullname");
  var wk_id = "";
  var url = base_url("users/modal_wk");
  $.ajax({
    url:url,
    type:'POST',
  })
  .done(function(data) {
    $(".modal-area").html(data);
    $("#model_wk").modal("show");

    var urluserdetail = getApi("users/getby");
    $.ajax({
      url: urluserdetail,
      type: 'GET',
      headers: {'Authorization': token},
      data: {"employee_id":emp_id}
    }).done(function(data) {
      $("#wk_fullname").html(data["data"][0]["firstname"]+" "+data["data"][0]["lastname"]);
      wk_id = data["data"][0]["wk_id"];

      var wk = $("#change_usr_wk");
      var url = getApi("workings/getall");
      var segments      = location.pathname.split('/'),
      site_id = segments[segments.length - 1];
      $.ajax({
        url: url,
        type: 'GET',
        headers: {'Authorization': token},
        data: {"site_id" : window.atob(window.atob(window.atob(site_id)))}
      }).done(function(data) {
        if (data["Has"]) {
          var r = "<option value='0' disabled>";
          r += "เลือกวันทำการ";
          r += "</option>";
          for (var i = 0; i < data["data"].length; i++) {
            r += "<option value='"+ data["data"][i]["wk_id"] +"'>";
            r += data["data"][i]["wk_name"];
            r += "</option>";
          }
          wk.append(r);
          if (wk_id == "") {
            $("#change_usr_wk").val(0);
          }else{
              $("#change_usr_wk").val(wk_id);
          }
        }
      });
    });

    $(".btn-change-wk").click(function(){
      var wk_id = $("#change_usr_wk").val();
      var urlwk = getApi("users/updateWokingByUser");
      var req = {
        "employee_id":emp_id, "wk_id":wk_id, "site_id" : window.atob(window.atob(window.atob(site_id)))
      }
      $.ajax({
        url: urlwk,
        type: 'POST',
        headers: {'Authorization': token},
        data: {"request":req}
      }).done(function(data) {
        if (data) {
          $("#model_wk").modal("toggle");
          $.toast({ heading: "SUCCESS",text: "เปลี่ยนวันทำการพนักงานเรียบร้อยแล้ว",position: 'top-right',loaderBg:'#ff6849',icon: 'success',hideAfter: 3500,stack: 6});
        }else{
           $.toast({heading: "FAILED",text: "พบข้อผิดพลาดโปรดตรวจสอบ",position: 'top-right',loaderBg:'#ff6849',icon: 'error',hideAfter: 3500,stack: 6});
        }
      });
    });
  });
}


$(".btn-link_wk_all_site").click(function(){
  var wk = $("#usr_site_wk");
  var url = getApi("workings/getall");
  var segments      = location.pathname.split('/'),
  site_id = segments[segments.length - 1];
  $.ajax({
    url: url,
    type: 'GET',
    headers: {'Authorization': token},
    data: {"site_id" : window.atob(window.atob(window.atob(site_id)))}
  }).done(function(data) {
    if (data["Has"]) {
      var r = "<option value='0'>";
      r += "เลือกวันทำการ";
      r += "</option>";
      for (var i = 0; i < data["data"].length; i++) {
        r += "<option value='"+ data["data"][i]["wk_id"] +"'>";
        r += data["data"][i]["wk_name"];
        r += "</option>";
      }
      wk.append(r);
    }

    $(".btn-link_wk_all_site-save").click(function(){
      var wk_id = $("#usr_site_wk").val();
      var urlwk = getApi("users/updateWokingBygroup");
      var req = {
        "wk_id":wk_id, "site_id" : window.atob(window.atob(window.atob(site_id)))
      }
      $.ajax({
        url: urlwk,
        type: 'POST',
        headers: {'Authorization': token},
        data: {"request" : req}
      }).done(function(data) {
        if (data) {
          $(".btn-link_wk_all_site-close").trigger("click");
          $("#employee_table").dataTable().fnDestroy();
          $("#employee_table").employees();
          $.toast({ heading: "SUCCESS",text: "เชื่อมต่อวันทำการกับพนักงานทั้งหมดเรียบร้อยแล้ว",position: 'top-right',loaderBg:'#ff6849',icon: 'success',hideAfter: 3500,stack: 6});
        }else{
           $.toast({heading: "FAILED",text: "พบข้อผิดพลาดโปรดตรวจสอบ",position: 'top-right',loaderBg:'#ff6849',icon: 'error',hideAfter: 3500,stack: 6});
        }
      });
    });
  });
});
