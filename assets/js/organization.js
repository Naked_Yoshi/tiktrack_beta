// var orgObj = [];
var max = 10;
var depData = [];
var depTable = [];
var posData = [];
var posTable = [];
$(document).ready(function() {
    max = parseFloat($("#amount_view_dep").val());
    $('ul.tabs').tabs();
    getOrg("","chart_div");
    getDepartment(1);
    getPosition(1);
});

$("#searchPos").keyup(function(event) {
    var key = $(this).val();
    getPosition(1,key);
});

$("#searchDep").keyup(function(event) {
    var key = $(this).val();
    getDepartment(1,key);
});

$("#amount_view_dep").change(function(){
   var key = $("#searchDep").val();
   max = parseFloat($(this).val());
   getDepartment(1,key);
});

$("#amount_view_pos").change(function(){
   var key = $("#searchPos").val();
   max = parseFloat($(this).val());
   getPosition(1,key);
});

$(".btn-department").click(function(){
     $.ajax({url:  base_url+"department/add",}).done(function(data) {
             $(".modal-area").html(data);
             $(".tat-modal").tat_modal({"modal":"open"});
             $("#depSave").click(function(){
               var input = $("#depName input");
               var blank = input.validate_blank_input({"className":".tat-modal","msg" : "กรุณากรอกฟิลด์นี้"});
               if(blank){
                   $.ajax({
                       url: base_url+'department/create',
                       type: 'POST',
                       data: $("#department_form").serialize()
                   }).done(function(data) {
                      var obj = JSON.parse(data);
                      if (!obj["success"]) {
                          swal('ผิดพลาด!','กรุณาลองใหม่อีกครั้ง','error');
                      }else{
                        swal('สำเร็จ!','บันทึกข้อมูลสำเร็จ','success');
                        $(".tat-modal-close").trigger("click");
                        getDepartment(1);
                     }
                   });
               }
               input.focus(function(event) {
                   $("#"+$(this).data("id")).removeClass('has-warning');
                   $("#feedback-"+$(this).data("id")).removeClass('active').html("");
               });
             });
     });
});

$(".btn-position").click(function(){
    $.ajax({url:  base_url+"position/add",}).done(function(data) {
            $(".modal-area").html(data);
            $(".tat-modal").tat_modal({"modal":"open"});
            getDepOptions();
            $("#op_department select").change(function(){
               var dep = $(this).val();
               getPosOptions(dep);
            });
            $("#positionClear").click(function(){
                getDepOptions();
                getPosOptions();
            });
            $("#positionSave").click(function(){
                 var input = $("#positionName input");
                 var blank = input.validate_blank_input({"className":".tat-modal","msg" : "กรุณากรอกฟิลด์นี้"});
                 if (blank) {
                     var input_select = $(".tat-modal-content select");
                     var selected = input_select.validate_selected({"className":".tat-modal","msg" : "กรุณากรอกฟิลด์นี้"});
                     var input_fake = $(".tat-modal-content input.select-dropdown");
                     input_fake.focus(function(event) {
                         $("#"+$(this).offsetParent()[0].parentElement.id).removeClass('has-warning');
                         $("#feedback-"+$(this).offsetParent()[0].parentElement.id).removeClass('active').html("");
                     });
                     if (selected) {
                         $.ajax({
                             url: base_url+'position/create',
                             type: 'POST',
                             data: $("#position_form").serialize()
                         }).done(function(data) {
                            var obj = JSON.parse(data);
                            if (!obj["success"]) {
                                swal('ผิดพลาด!','กรุณาลองใหม่อีกครั้ง','error');
                            }else{
                              swal('สำเร็จ!','บันทึกข้อมูลสำเร็จ','success');
                              $(".tat-modal-close").trigger("click");
                              getOrg("","chart_div");
                              getPosition(1);
                           }

                     });
                 }
               }
            });
            $(".tat-modal-content input:text").focus(function(event) {
                $("#"+$(this).data("id")).removeClass('has-warning');
                $("#feedback-"+$(this).data("id")).removeClass('active').html("");
            });
    });
});



function getDepOptions(index){
    var selected = "";
    if (typeof index !== "underfined") {
        selected = index;
    }
    $.ajax({
      url:  base_url+'department/options',type: 'POST', data: {"selected":selected}
    }).done(function(data) {
       var obj = JSON.parse(data);
       $("#op_department select").html("");
       for (var i = 0; i < obj.length; i++) {
           $("#op_department select").append(obj[i]);
           callform();
       }

    });
}

function getPosOptions(dep,index){
  var item = "";
  if (typeof dep !== "underfined") {
      item = dep;
  }
  var selected = "";
  if (typeof index !== "underfined") {
      selected = index;
  }
  $.ajax({
    url:  base_url+'position/options',type: 'POST',data:{"item":item,"selected":selected}
  }).done(function(data) {
     var obj = JSON.parse(data);
     $("#op_positionBoss select").html("");
     for (var i = 0; i < obj.length; i++) {
         $("#op_positionBoss select").append(obj[i]);
         callform();
     }

  });
}

function getOrg(detail,id){
    var item = "";
    if (typeof detail !== "undefined") {
        item = detail;
    }
    $.ajax({url:base_url+"designation/getOrg",type:"POST",data:{"item":detail}}).done(function(obj) {
      var obj = JSON.parse(obj);
      google.charts.load('current', {packages:["orgchart"]});
      google.charts.setOnLoadCallback((function() { drawChart(obj,id);}));
    });
}
function drawChart(obj,id){
    var data = new google.visualization.DataTable();
    data.addColumn('string', 'Name');
    data.addColumn('string', 'Manager');
    data.addColumn('string', 'ToolTip');
    data.addRows(obj);
    var chart = new google.visualization.OrgChart(document.getElementById(id));
    chart.draw(data, {allowHtml:true});
    $('.tooltipped').tooltip();
}

function getDepartment(page,key){
  var searchKey = "";
  if (typeof key !== "undefined") {
      searchKey = key;
  }
  $.ajax({url:  base_url+"department",type:"POST",data:{"key":searchKey} }).done(function(data) {
          var obj = JSON.parse(data);
          depData = obj["data"];
          depTable = obj["table"];
          $(".department-body").department(depData,depTable,page);
  });
}

function getPosition(page,key){
  var searchKey = "";
  if (typeof key !== "undefined") {
      searchKey = key;
  }
  $.ajax({url:  base_url+"position",type:"POST",data:{"key":searchKey} }).done(function(data) {
          var obj = JSON.parse(data);
          posData = obj["data"];
          posTable = obj["table"];
          $(".department-body").getPosition(posData,posTable,page);
  });
}

function getindex(data,index){
          for (var i = 0; i < data.length; i++) {
                if (data[i]["index"] == index) {
                    return i;
                }
          }
}

(function($){
    $.fn.department = function (data,table,page){
      var num_rows = table.length;
      var start = (page-1)*max;
      var num = 0;
      $(".department-body").html("");
      if (data["hasDepartment"]) {
          for (var i = start; i < data["Department"].length; i++) {
               if (num < max) {
                   $(".department-body").append(table[data["Department"][i]["index"]]);
                   num++;
               }
               $(".pagination-dep").pagination({"funcname" : "getDepartment","perpage" : max,"num_rows" : num_rows,"page" : page});
          }
      }else{
          $(".department-body").append(table[0]);
          $(".pagination").pagination({"funcname" : "getDepartment","perpage" : max,"num_rows" : 1,"page" : page});
      }
      $('.tooltipped').tooltip();
      $(".btn-detail").click(function(){
          var index = getindex(data["Department"],$(this).data("index"));
          var id = data["Department"][index]["depId"];
          $.ajax({url:  base_url+"department/detail",type:"POST",data:{"id":id} }).done(function(data) {
                  $(".modal-area").html(data);
                  $(".tat-modal").tat_modal({"modal":"open"});
                  getOrg(id,"chart_detail")
          });
      });
      $(".btn-edit").click(function(){
        var index = getindex(data["Department"],$(this).data("index"));
        var id = data["Department"][index]["depId"];
        $.ajax({url:  base_url+"department/edit",type:"POST",data:{"id":id} }).done(function(data) {
                $(".modal-area").html(data);
                $(".tat-modal").tat_modal({"modal":"open"});
                $("#depUpdate").click(function(){
                    var input = $("#depName input");
                    var blank = input.validate_blank_input({"className":".tat-modal","msg" : "กรุณากรอกฟิลด์นี้"});
                    if(blank){
                        $.ajax({
                            url: base_url+'department/update',
                            type: 'POST',
                            data: $("#department_form").serialize()+"&index="+id
                        }).done(function(data) {
                           var obj = JSON.parse(data);
                           if (!obj["success"]) {
                               swal('ผิดพลาด!','กรุณาลองใหม่อีกครั้ง','error');
                           }else{
                             swal('สำเร็จ!','บันทึกข้อมูลสำเร็จ','success');
                             $(".tat-modal-close").trigger("click");
                             getOrg("","chart_div");
                             getDepartment(1);
                             getPosition(1);
                          }
                        });
                    }
                    input.focus(function(event) {
                        $("#"+$(this).data("id")).removeClass('has-warning');
                        $("#feedback-"+$(this).data("id")).removeClass('active').html("");
                    });
                });
        });
      });
      $(".btn-trash").click(function(){
          var index = getindex(data["Department"],$(this).data("index"));
          var id = data["Department"][index]["depId"];
          swal({title: 'คุณแน่ใจ ?',text: "คุณจะไม่สามารถย้อนกลับได้ !",  type: 'warning',showCancelButton: true,confirmButtonColor: '#3085d6',cancelButtonColor: '#d33',confirmButtonText: 'ใช่ ลบเลย !',cancelButtonText: 'ยกเลิก'})
         .then((result) => { if (result.value) {
                  $.ajax({url: base_url+"department/delete",type:"POST",data:{"id":id} }).done(function(data) {
                        var obj = JSON.parse(data);
                        if (obj["can"]) {
                            if (!obj["success"]) {
                                swal('ผิดพลาด!','กรุณาลองใหม่อีกครั้ง','error');
                            }else{
                                swal('สำเร็จ!','ลบข้อมูลสำเร็จ','success');
                                $(".tat-modal-close").trigger("click");
                                getOrg("","chart_div");
                                getDepartment(1);
                                getPosition(1);
                           }
                        }else{
                            swal('ไม่สามารถลบได้!','พบว่ามีพนักงานอยู่แผนกนี้','error');
                        }
                  });
          }
          });

      });
    }
    $.fn.getPosition = function (data,table,page){
      var num_rows = table.length;
      var start = (page-1)*max;
      var num = 0;
      $(".position-body").html("");
      if (data["hasRoot"]) {
          for (var i = start; i < data["Root"].length; i++) {
               if (num < max) {
                   $(".position-body").append(table[data["Root"][i]["index"]]);
                   num++;
               }
               $(".pagination-position").pagination({"funcname" : "getPosition","perpage" : max,"num_rows" : num_rows,"page" : page});
          }
      }else{
          $(".position-body").append(table[0]);
          $(".pagination-position").pagination({"funcname" : "getPosition","perpage" : max,"num_rows" : 1,"page" : page});
      }
      $('.tooltipped').tooltip();
      $(".btn-pos-edit").click(function(){
        var index = getindex(data["Root"],$(this).data("index"));
        var id = data["Root"][index]["positionId"];
        $.ajax({url:  base_url+"position/edit",type:"POST",data:{"id":id} }).done(function(data) {
                $(".modal-area").html(data);
                $(".tat-modal").tat_modal({"modal":"open"});
                callform();
                var dep = $("#op_department #dep").val();
                getDepOptions(dep);
                var root = $("#op_positionBoss #root").val();
                getPosOptions(dep,root);
                $("#positionSave").click(function(){
                     var input = $("#positionName input");
                     var blank = input.validate_blank_input({"className":".tat-modal","msg" : "กรุณากรอกฟิลด์นี้"});
                     if (blank) {
                         var input_select = $(".tat-modal-content select");
                         var selected = input_select.validate_selected({"className":".tat-modal","msg" : "กรุณากรอกฟิลด์นี้"});
                         var input_fake = $(".tat-modal-content input.select-dropdown");
                         input_fake.focus(function(event) {
                             $("#"+$(this).offsetParent()[0].parentElement.id).removeClass('has-warning');
                             $("#feedback-"+$(this).offsetParent()[0].parentElement.id).removeClass('active').html("");
                         });
                         if (selected) {
                             $.ajax({
                                 url: base_url+'position/update',
                                 type: 'POST',
                                 data: $("#position_form").serialize()
                             }).done(function(data) {
                                var obj = JSON.parse(data);
                                if (!obj["success"]) {
                                    swal('ผิดพลาด!','กรุณาลองใหม่อีกครั้ง','error');
                                }else{
                                  swal('สำเร็จ!','บันทึกข้อมูลสำเร็จ','success');
                                  $(".tat-modal-close").trigger("click");
                                  getOrg("","chart_div");
                                  getPosition(1);
                               }

                         });
                       }
                   }
                });
        });
      });
      $(".btn-pos-trash").click(function(){
        var index = getindex(data["Root"],$(this).data("index"));
        var id = data["Root"][index]["positionId"];
        swal({title: 'คุณแน่ใจ ?',text: "คุณจะไม่สามารถย้อนกลับได้ !",  type: 'warning',showCancelButton: true,confirmButtonColor: '#3085d6',cancelButtonColor: '#d33',confirmButtonText: 'ใช่ ลบเลย !',cancelButtonText: 'ยกเลิก'})
       .then((result) => { if (result.value) {
               $.ajax({url: base_url+"position/delete",type:"POST",data:{"id":id} }).done(function(data) {
                 var obj = JSON.parse(data);
                 if (obj["can"]) {
                     if (!obj["success"]) {
                         swal('ผิดพลาด!','กรุณาลองใหม่อีกครั้ง','error');
                     }else{
                         swal('สำเร็จ!','ลบข้อมูลสำเร็จ','success');
                         $(".tat-modal-close").trigger("click");
                         getOrg("","chart_div");
                         getDepartment(1);
                         getPosition(1);
                    }
                 }else{
                     swal('ไม่สามารถลบได้!','พบว่ามีพนักงานอยู่ตำแหน่งนี้','error');
                 }
               });
          }
        });
      });
    }
})(jQuery)
